create volatile table ZZMD00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntMedl	FLOAT, 
	AntAktivMedl	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD00 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Retail_Region_Cd  Retail_Region_Cd,
	a13.Store_Id  Store_Id,
	sum((Case when a11.Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntMedl,
	sum((Case when a11.Active_Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntAktivMedl
from	ITSemCMNVOUT.LOYALTY_MEMBER_WEEK_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a12.Home_Store_Seq_Num = a13.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201318)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (a11.Member_Ind > 0
 or a11.Active_Member_Ind > 0))
group by	a11.Calendar_Week_Id,
	a13.Retail_Region_Cd,
	a13.Store_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntKont	FLOAT, 
	AntAktivaKont	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD01 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Retail_Region_Cd  Retail_Region_Cd,
	a13.Store_Id  Store_Id,
	sum(a11.Contact_Sum_Cnt)  AntKont,
	sum((Case when a11.Active_Contact_Ind = 1 then a11.Contact_Sum_Cnt else NULL end))  AntAktivaKont
from	ITSemCMNVOUT.LOYALTY_CONTACT_WEEK_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a12.Home_Store_Seq_Num = a13.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201318)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a11.Calendar_Week_Id,
	a13.Retail_Region_Cd,
	a13.Store_Id

select	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	max(a16.Retail_Region_Name)  Retail_Region_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	max(a15.Store_Name)  Store_Name,
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	max(pa11.AntMedl)  AntMedl,
	max(pa12.AntKont)  AntKont,
	max(pa11.AntAktivMedl)  AntAktivMedl,
	max(pa12.AntAktivaKont)  AntAktivaKont
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id) = a15.Store_Id)
	join	ITSemCMNVOUT.RETAIL_REGION_D	a16
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = a16.Retail_Region_Cd)
group by	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id),
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01
