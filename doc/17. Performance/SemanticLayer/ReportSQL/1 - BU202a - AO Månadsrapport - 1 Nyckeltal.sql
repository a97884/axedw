SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=BU202a - AO Månadsrapport - 1 Nyckeltal; ClientUser=A20222;' For Session;



create volatile table T85C160WWMD000, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AntSaldoFgM INTEGER, 
WJXBFS1 INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into T85C160WWMD000 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
count(a11.Article_Seq_Num) AntSaldoFgM,
(Case when max((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then 1 else 0 end)) = 1 then count((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then a11.Article_Seq_Num else NULL end)) else NULL end) WJXBFS1
from PRSemCMNVOUT.PERPETUAL_INVENTORY_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Perpetual_Inv_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_Previous_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a14.Store_Id

create volatile table T0CSAWF5CMD001, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOManRbtFgM FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into T0CSAWF5CMD001 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
sum(a11.Discount_Amt) AOManRbtFgM
from PRSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Tran_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_Previous_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1)
and a11.Discount_Type_Cd in ('MP'))
group by a13.Calendar_Month_Id,
a14.Store_Id

create volatile table TF3VR5SNKMD002, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AntArtIAoSort INTEGER, 
AntSaldo INTEGER, 
WJXBFS1 INTEGER, 
WJXBFS2 INTEGER, 
WJXBFS3 INTEGER, 
TotalSortiment INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into TF3VR5SNKMD002 
select a13.Calendar_Month_Id Calendar_Month_Id,
a12.Store_Id Store_Id,
(Case when max((Case when a11.AO_Ind in (1) then 1 else 0 end)) = 1 then count(distinct (Case when a11.AO_Ind in (1) then a11.Article_Seq_Num else NULL end)) else NULL end) AntArtIAoSort,
(Case when max((Case when a11.AO_Ind in (1) then 1 else 0 end)) = 1 then count((Case when a11.AO_Ind in (1) then a11.Article_Seq_Num else NULL end)) else NULL end) AntSaldo,
(Case when max((Case when (a11.Empty_Shelf_Ind in (1) and a11.AO_Ind in (1)) then 1 else 0 end)) = 1 then count((Case when (a11.Empty_Shelf_Ind in (1) and a11.AO_Ind in (1)) then a11.Article_Seq_Num else NULL end)) else NULL end) WJXBFS1,
(Case when max((Case when (a11.Empty_Shelf_Ind in (1) and (a11.Supplier_Out_Of_Stock_Ind in (1) or a11.Incomplete_Delivery_Ind in (1)) and a11.AO_Ind in (1)) then 1 else 0 end)) = 1 then count((Case when (a11.Empty_Shelf_Ind in (1) and (a11.Supplier_Out_Of_Stock_Ind in (1) or a11.Incomplete_Delivery_Ind in (1)) and a11.AO_Ind in (1)) then a11.Article_Seq_Num else NULL end)) else NULL end) WJXBFS2,
(Case when max((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0) and a11.AO_Ind in (1)) then 1 else 0 end)) = 1 then count((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0) and a11.AO_Ind in (1)) then a11.Article_Seq_Num else NULL end)) else NULL end) WJXBFS3,
count(distinct a11.Article_Seq_Num) TotalSortiment
from PRSemCMNVOUT.PERPETUAL_INVENTORY_F a11
join PRSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join PRSemCMNVOUT.CALENDAR_DAY_D a13
on (a11.Perpetual_Inv_Dt = a13.Calendar_Dt)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a12.Concept_Cd in ('HEM', 'HEA', 'PRX'))
group by a13.Calendar_Month_Id,
a12.Store_Id

create volatile table T6RWJMFG0SP003, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOForsBelExMomsFgAr FLOAT, 
AOForsBelExBVFgA FLOAT, 
AOInkBelExFgA FLOAT, 
AOInkBelL09FgA FLOAT, 
AOForsBvL09FgA FLOAT, 
GODWFLAGd_1 INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into T6RWJMFG0SP003 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
sum(a11.Unit_Selling_Price_Amt) AOForsBelExMomsFgAr,
sum(a11.GP_Unit_Selling_Price_Amt) AOForsBelExBVFgA,
sum(a11.Unit_Cost_Amt) AOInkBelExFgA,
sum((Case when a11.Campaign_Sales_Type_Cd in ('L ') then a11.Unit_Cost_Amt else NULL end)) AOInkBelL09FgA,
sum((Case when a11.Campaign_Sales_Type_Cd in ('L ') then a11.GP_Unit_Selling_Price_Amt else NULL end)) AOForsBvL09FgA,
max((Case when a11.Campaign_Sales_Type_Cd in ('L ') then 1 else 0 end)) GODWFLAGd_1
from PRSemCMNVOUT.SALES_TRANSACTION_LINE_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Tran_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_PYS_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a14.Store_Id

create volatile table TNH1LSR1SSP004, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOSvinnKrFgA FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into TNH1LSR1SSP004 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
sum(a11.Total_Line_Value_Amt) AOSvinnKrFgA
from PRSemCMNVOUT.ARTICLE_KNOWN_LOSS_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Adjustment_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_PYS_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a14.Store_Id

create volatile table THYKW3IJKMD005, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOForsBelExMomsFgAr FLOAT, 
AOSvinnKrFgA FLOAT, 
AOForsBelExBVFgA FLOAT, 
AOInkBelExFgA FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into THYKW3IJKMD005 
select coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) Calendar_Month_Id,
coalesce(pa11.Store_Id, pa12.Store_Id) Store_Id,
pa11.AOForsBelExMomsFgAr AOForsBelExMomsFgAr,
pa12.AOSvinnKrFgA AOSvinnKrFgA,
pa11.AOForsBelExBVFgA AOForsBelExBVFgA,
pa11.AOInkBelExFgA AOInkBelExFgA
from T6RWJMFG0SP003 pa11
full outer join TNH1LSR1SSP004 pa12
on (pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
pa11.Store_Id = pa12.Store_Id)

create volatile table TSGH21PM8SP006, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOSvinnKr FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into TSGH21PM8SP006 
select a13.Calendar_Month_Id Calendar_Month_Id,
a12.Store_Id Store_Id,
sum(a11.Total_Line_Value_Amt) AOSvinnKr
from PRSemCMNVOUT.ARTICLE_KNOWN_LOSS_F a11
join PRSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join PRSemCMNVOUT.CALENDAR_DAY_D a13
on (a11.Adjustment_Dt = a13.Calendar_Dt)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a12.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a12.Store_Id

create volatile table TYR2R23E8SP007, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOForsBelExBVBer FLOAT, 
AOInkopsBelEx FLOAT, 
AOForsBelExMoms FLOAT, 
AOForsBvL09 FLOAT, 
AOInkBelL09 FLOAT, 
GODWFLAGc_1 INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into TYR2R23E8SP007 
select a13.Calendar_Month_Id Calendar_Month_Id,
a12.Store_Id Store_Id,
sum(a11.GP_Unit_Selling_Price_Amt) AOForsBelExBVBer,
sum(a11.Unit_Cost_Amt) AOInkopsBelEx,
sum(a11.Unit_Selling_Price_Amt) AOForsBelExMoms,
sum((Case when a11.Campaign_Sales_Type_Cd in ('L ') then a11.GP_Unit_Selling_Price_Amt else NULL end)) AOForsBvL09,
sum((Case when a11.Campaign_Sales_Type_Cd in ('L ') then a11.Unit_Cost_Amt else NULL end)) AOInkBelL09,
max((Case when a11.Campaign_Sales_Type_Cd in ('L ') then 1 else 0 end)) GODWFLAGc_1
from PRSemCMNVOUT.SALES_TRANSACTION_LINE_F a11
join PRSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join PRSemCMNVOUT.CALENDAR_DAY_D a13
on (a11.Tran_Dt = a13.Calendar_Dt)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a12.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a12.Store_Id

create volatile table T9Z0FUYN4MD008, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOSvinnKr FLOAT, 
AOForsBelExBVBer FLOAT, 
AOInkopsBelEx FLOAT, 
AOForsBelExMoms FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into T9Z0FUYN4MD008 
select coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) Calendar_Month_Id,
coalesce(pa11.Store_Id, pa12.Store_Id) Store_Id,
pa11.AOSvinnKr AOSvinnKr,
pa12.AOForsBelExBVBer AOForsBelExBVBer,
pa12.AOInkopsBelEx AOInkopsBelEx,
pa12.AOForsBelExMoms AOForsBelExMoms
from TSGH21PM8SP006 pa11
full outer join TYR2R23E8SP007 pa12
on (pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
pa11.Store_Id = pa12.Store_Id)

create volatile table THSQUVFLCOP009, no fallback, no log(
Store_Id INTEGER, 
Calendar_Month_Id INTEGER, 
WJXBFS1 FLOAT, 
WJXBFS2 FLOAT)
primary index (Store_Id, Calendar_Month_Id) on commit preserve rows

;insert into THSQUVFLCOP009 
select pa01.Store_Id Store_Id,
pa01.Calendar_Month_Id Calendar_Month_Id,
pa01.AOForsBvL09 WJXBFS1,
pa01.AOInkBelL09 WJXBFS2
from TYR2R23E8SP007 pa01
where pa01.GODWFLAGc_1 = 1

create volatile table TYYVGVQ74OP00A, no fallback, no log(
Store_Id INTEGER, 
Calendar_Month_Id INTEGER, 
WJXBFS1 FLOAT, 
WJXBFS2 FLOAT)
primary index (Store_Id, Calendar_Month_Id) on commit preserve rows

;insert into TYYVGVQ74OP00A 
select pa01.Store_Id Store_Id,
pa01.Calendar_Month_Id Calendar_Month_Id,
pa01.AOInkBelL09FgA WJXBFS1,
pa01.AOForsBvL09FgA WJXBFS2
from T6RWJMFG0SP003 pa01
where pa01.GODWFLAGd_1 = 1

create volatile table TNTZ1FXZKMD00B, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
HIHExklRestFgA INTEGER, 
AntSaldoFgA INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into TNTZ1FXZKMD00B 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
(Case when max((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then 1 else 0 end)) = 1 then count((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then a11.Article_Seq_Num else NULL end)) else NULL end) HIHExklRestFgA,
count(a11.Article_Seq_Num) AntSaldoFgA
from PRSemCMNVOUT.PERPETUAL_INVENTORY_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Perpetual_Inv_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_PYS_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a14.Store_Id

create volatile table ZZMD0C, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOInkBelL09FgM FLOAT, 
AOForsBvL09FgM FLOAT, 
GODWFLAGf_1 INTEGER, 
AOInkBelExFgM FLOAT, 
AOForsExMomsFgManad FLOAT, 
AOForsBelExBVFgM FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD0C 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
sum((Case when a11.Campaign_Sales_Type_Cd in ('L ') then a11.Unit_Cost_Amt else NULL end)) AOInkBelL09FgM,
sum((Case when a11.Campaign_Sales_Type_Cd in ('L ') then a11.GP_Unit_Selling_Price_Amt else NULL end)) AOForsBvL09FgM,
max((Case when a11.Campaign_Sales_Type_Cd in ('L ') then 1 else 0 end)) GODWFLAGf_1,
sum(a11.Unit_Cost_Amt) AOInkBelExFgM,
sum(a11.Unit_Selling_Price_Amt) AOForsExMomsFgManad,
sum(a11.GP_Unit_Selling_Price_Amt) AOForsBelExBVFgM
from PRSemCMNVOUT.SALES_TRANSACTION_LINE_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Tran_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_Previous_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a14.Store_Id

create volatile table ZZOP0D, no fallback, no log(
Store_Id INTEGER, 
Calendar_Month_Id INTEGER, 
WJXBFS1 FLOAT, 
WJXBFS2 FLOAT)
primary index (Store_Id, Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0D 
select pa01.Store_Id Store_Id,
pa01.Calendar_Month_Id Calendar_Month_Id,
pa01.AOInkBelL09FgM WJXBFS1,
pa01.AOForsBvL09FgM WJXBFS2
from ZZMD0C pa01
where pa01.GODWFLAGf_1 = 1

create volatile table ZZSP0E, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOSvinnKrFgM FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZSP0E 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
sum(a11.Total_Line_Value_Amt) AOSvinnKrFgM
from PRSemCMNVOUT.ARTICLE_KNOWN_LOSS_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Adjustment_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_Previous_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id,
a14.Store_Id

create volatile table ZZMD0F, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOInkBelExFgM FLOAT, 
AOForsExMomsFgManad FLOAT, 
AOSvinnKrFgM FLOAT, 
AOForsBelExBVFgM FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD0F 
select coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) Calendar_Month_Id,
coalesce(pa11.Store_Id, pa12.Store_Id) Store_Id,
pa11.AOInkBelExFgM AOInkBelExFgM,
pa11.AOForsExMomsFgManad AOForsExMomsFgManad,
pa12.AOSvinnKrFgM AOSvinnKrFgM,
pa11.AOForsBelExBVFgM AOForsBelExBVFgM
from ZZMD0C pa11
full outer join ZZSP0E pa12
on (pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD0G, no fallback, no log(
Calendar_Month_Id INTEGER, 
HIHTotKedja INTEGER, 
AntSaldoKedja INTEGER)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0G 
select a13.Calendar_Month_Id Calendar_Month_Id,
(Case when max((Case when a11.Empty_Shelf_Ind in (1) then 1 else 0 end)) = 1 then count((Case when a11.Empty_Shelf_Ind in (1) then a11.Article_Seq_Num else NULL end)) else NULL end) HIHTotKedja,
count(a11.Article_Seq_Num) AntSaldoKedja
from PRSemCMNVOUT.PERPETUAL_INVENTORY_F a11
join PRSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join PRSemCMNVOUT.CALENDAR_DAY_D a13
on (a11.Perpetual_Inv_Dt = a13.Calendar_Dt)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a12.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1))
group by a13.Calendar_Month_Id

create volatile table ZZMD0H, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOManRbt FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD0H 
select a13.Calendar_Month_Id Calendar_Month_Id,
a12.Store_Id Store_Id,
sum(a11.Discount_Amt) AOManRbt
from PRSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F a11
join PRSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join PRSemCMNVOUT.CALENDAR_DAY_D a13
on (a11.Tran_Dt = a13.Calendar_Dt)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a12.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1)
and a11.Discount_Type_Cd in ('MP'))
group by a13.Calendar_Month_Id,
a12.Store_Id

create volatile table ZZMD0I, no fallback, no log(
Calendar_Month_Id INTEGER, 
Store_Id INTEGER, 
AOManRbtFgA FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD0I 
select a13.Calendar_Month_Id Calendar_Month_Id,
a14.Store_Id Store_Id,
sum(a11.Discount_Amt) AOManRbtFgA
from PRSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F a11
join PRSemCMNVOUT.CALENDAR_DAY_D a12
on (a11.Tran_Dt = a12.Calendar_Dt)
join PRSemCMNVOUT.CALENDAR_MONTH_D a13
on (a12.Calendar_Month_Id = a13.Calendar_PYS_Month_Id)
join PRSemCMNVOUT.STORE_D a14
on (a11.Store_Seq_Num = a14.Store_Seq_Num)
where (a13.Calendar_Month_Id in (201212, 201211, 201210, 201209)
and a14.Concept_Cd in ('HEM', 'HEA', 'PRX')
and a11.AO_Ind in (1)
and a11.Discount_Type_Cd in ('MP'))
group by a13.Calendar_Month_Id,
a14.Store_Id

select coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa117.Calendar_Month_Id) Calendar_Month_Id,
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id, pa110.Store_Id, pa111.Store_Id, pa113.Store_Id, pa114.Store_Id, pa117.Store_Id) Store_Id,
max(a121.Store_Name) Store_Name,
max(pa11.AntSaldoFgM) AntSaldoFgM,
max(pa12.AOManRbtFgM) AOManRbtFgM,
max(pa13.AntArtIAoSort) AntArtIAoSort,
max(pa14.AOForsBelExMomsFgAr) AOForsBelExMomsFgAr,
max(pa15.AOSvinnKr) AOSvinnKr,
max(pa14.AOSvinnKrFgA) AOSvinnKrFgA,
max(pa13.WJXBFS1) HIHTot,
max(pa13.WJXBFS2) HIHPgaRest,
max(pa18.WJXBFS1) AOForsBvL09,
max(pa19.WJXBFS1) AOInkBelL09FgA,
max(pa110.HIHExklRestFgA) HIHExklRestFgA,
max(pa111.WJXBFS1) AOInkBelL09FgM,
max(pa13.WJXBFS3) HIHExklRest,
max(pa14.AOForsBelExBVFgA) AOForsBelExBVFgA,
max(pa113.AOInkBelExFgM) AOInkBelExFgM,
max(pa113.AOForsExMomsFgManad) AOForsExMomsFgManad,
max(pa113.AOSvinnKrFgM) AOSvinnKrFgM,
max(pa13.AntSaldo) AntSaldo,
max(pa119.HIHTotKedja) HIHTotKedja,
max(pa114.AOManRbt) AOManRbt,
max(pa15.AOForsBelExBVBer) AOForsBelExBVBer,
max(pa11.WJXBFS1) HIHExklRestFgM,
max(pa18.WJXBFS2) AOInkBelL09,
max(pa13.TotalSortiment) TotalSortiment,
max(pa117.AOManRbtFgA) AOManRbtFgA,
max(pa15.AOInkopsBelEx) AOInkopsBelEx,
max(pa111.WJXBFS2) AOForsBvL09FgM,
max(pa110.AntSaldoFgA) AntSaldoFgA,
max(pa15.AOForsBelExMoms) AOForsBelExMoms,
max(pa19.WJXBFS2) AOForsBvL09FgA,
max(pa119.AntSaldoKedja) AntSaldoKedja,
max(pa14.AOInkBelExFgA) AOInkBelExFgA,
max(pa113.AOForsBelExBVFgM) AOForsBelExBVFgM
from ZZMD00 pa11
full outer join ZZMD01 pa12
on (pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
pa11.Store_Id = pa12.Store_Id)
full outer join ZZMD02 pa13
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
full outer join ZZMD05 pa14
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id) = pa14.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
full outer join ZZMD08 pa15
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa15.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id) = pa15.Store_Id)
full outer join ZZOP09 pa18
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id) = pa18.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id) = pa18.Store_Id)
full outer join ZZOP0A pa19
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id) = pa19.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id) = pa19.Store_Id)
full outer join ZZMD0B pa110
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id) = pa110.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id) = pa110.Store_Id)
full outer join ZZOP0D pa111
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id) = pa111.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id, pa110.Store_Id) = pa111.Store_Id)
full outer join ZZMD0F pa113
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id) = pa113.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id, pa110.Store_Id, pa111.Store_Id) = pa113.Store_Id)
full outer join ZZMD0H pa114
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa113.Calendar_Month_Id) = pa114.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id, pa110.Store_Id, pa111.Store_Id, pa113.Store_Id) = pa114.Store_Id)
full outer join ZZMD0I pa117
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id) = pa117.Calendar_Month_Id and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id, pa110.Store_Id, pa111.Store_Id, pa113.Store_Id, pa114.Store_Id) = pa117.Store_Id)
left outer join ZZMD0G pa119
on (coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa117.Calendar_Month_Id) = pa119.Calendar_Month_Id)
join PRSemCMNVOUT.STORE_D a121
on (coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id, pa110.Store_Id, pa111.Store_Id, pa113.Store_Id, pa114.Store_Id, pa117.Store_Id) = a121.Store_Id)
group by coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa117.Calendar_Month_Id),
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa19.Store_Id, pa110.Store_Id, pa111.Store_Id, pa113.Store_Id, pa114.Store_Id, pa117.Store_Id)



SET QUERY_BAND = NONE For Session;
