SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=14 - MA206a - Statistik utbetalad bonus per butik; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AccrualAmt	FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD00 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	sum(a11.Accrual_Amt)  AccrualAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a14.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Store_Id not in (-1)
 and a11.Calendar_Month_Id in (201306))
group by	a11.Calendar_Month_Id,
	a12.Store_Id

create volatile table ZZMD01, no fallback, no log(
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Membership_Num) on commit preserve rows

;insert into ZZMD01 
select	a13.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num <> -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a14.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Store_Id not in (-1)
 and a11.Calendar_Month_Id in (201306))
group by	a13.Membership_Num

create volatile table ZZMD02, no fallback, no log(
	AccrualAmtButik	FLOAT) on commit preserve rows

;insert into ZZMD02 
select	sum(pa11.WJXBFS1)  AccrualAmtButik
from	ZZMD01	pa11

create volatile table ZZMD03, no fallback, no log(
	Utbetalad_bonus	FLOAT) on commit preserve rows

;insert into ZZMD03 
select	sum(a11.Remittance_Amt)  Utbetalad_bonus
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_Next_Month_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a14.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Calendar_Month_Id in (201306))

select	pa11.Store_Id  Store_Id,
	max(a14.Store_Name)  Store_Name,
	pa11.Calendar_Month_Id  Calendar_Month_Id,
	ZEROIFNULL(((max(pa11.AccrualAmt) / NULLIFZERO(max(pa12.AccrualAmtButik))) * max(pa13.Utbetalad_bonus)))  UtbetaladBonusButik
from	ZZMD00	pa11
	cross join	ZZMD02	pa12
	cross join	ZZMD03	pa13
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(pa11.Store_Id = a14.Store_Id)
group by	pa11.Store_Id,
	pa11.Calendar_Month_Id


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03
