SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=BU010b - Topp 10 artiklar (Butiks AP kr) - vecka; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD00 
select	a15.Calendar_Week_Id  Calendar_Week_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	(sum(a11.GP_Unit_Selling_Price_Amt) - sum(a11.Unit_Cost_Amt))  WJXBFS1,
	(sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end)) - sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end)))  WJXBFS2
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
	join	ITSemCMNVOUT.ARTICLE_D	a16
	  on 	(a14.Article_Seq_Num = a16.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Week_Id in (201318))
group by	a15.Calendar_Week_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

create volatile table ZZAM01, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Week_Id) on commit preserve rows

;insert into ZZAM01 
select	pa12.Concept_Cd  Concept_Cd,
	pa12.Article_Seq_Num  Article_Seq_Num,
	pa12.Calendar_Week_Id  Calendar_Week_Id,
	rank () over(partition by pa12.Concept_Cd order by ((ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.WJXBFS2)) - ZEROIFNULL(pa12.WJXBFS1)) desc)  WJXBFS1
from	ZZMD00	pa12

create volatile table ZZMQ02, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Week_Id	INTEGER)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ02 
select	pa11.Concept_Cd  Concept_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	pa11.Calendar_Week_Id  Calendar_Week_Id
from	ZZAM01	pa11
where	(pa11.WJXBFS1 <=  10.0)

create volatile table ZZMD03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	AntalBtkMedFsg	INTEGER, 
	WJXBFS1	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	WJXBFS2	FLOAT, 
	ForsBelExBVBerButKampanj	FLOAT, 
	InkopsBelExButKampanj	FLOAT)
primary index (Calendar_Week_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD03 
select	a15.Calendar_Week_Id  Calendar_Week_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg,
	(ZEROIFNULL(sum(a11.GP_Unit_Selling_Price_Amt)) - ZEROIFNULL(sum(a11.Unit_Cost_Amt)))  WJXBFS1,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	(ZEROIFNULL(sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))) - ZEROIFNULL(sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end))))  WJXBFS2,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBerButKampanj,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end))  InkopsBelExButKampanj
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
	join	ZZMQ02	pa16
	  on 	(a12.Concept_Cd = pa16.Concept_Cd and 
	a14.Article_Seq_Num = pa16.Article_Seq_Num and 
	a15.Calendar_Week_Id = pa16.Calendar_Week_Id)
	join	ITSemCMNVOUT.ARTICLE_D	a17
	  on 	(a14.Article_Seq_Num = a17.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Week_Id in (201318))
group by	a15.Calendar_Week_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

select	pa13.Concept_Cd  Concept_Cd,
	max(a14.Concept_Name)  Concept_Name,
	pa13.Article_Seq_Num  Article_Seq_Num,
	max(a15.Article_Id)  Article_Id,
	max(a15.Article_Desc)  Article_Desc,
	pa13.Calendar_Week_Id  Calendar_Week_Id,
	max(pa13.AntalBtkMedFsg)  AntalBtkMedFsg,
	rank () over(partition by pa13.Concept_Cd order by max(((ZEROIFNULL(pa13.WJXBFS1) - ZEROIFNULL(pa13.WJXBFS2)) - ZEROIFNULL(pa13.WJXBFS1))) desc)  WJXBFS1,
	max(pa13.ForsBelExBVBerButKampanj)  ForsBelExBVBerButKampanj,
	max(pa13.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa13.InkopsBelExButKampanj)  InkopsBelExButKampanj,
	max(pa13.InkopsBelEx)  InkopsBelEx
from	ZZMD03	pa13
	join	ITSemCMNVOUT.CONCEPT_D	a14
	  on 	(pa13.Concept_Cd = a14.Concept_Cd)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(pa13.Article_Seq_Num = a15.Article_Seq_Num)
group by	pa13.Concept_Cd,
	pa13.Article_Seq_Num,
	pa13.Calendar_Week_Id


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZAM01

drop table ZZMQ02

drop table ZZMD03
