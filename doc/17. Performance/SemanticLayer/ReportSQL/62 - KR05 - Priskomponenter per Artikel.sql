SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=KR05 - Priskomponenter per Artikel; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	ITSemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-10-29'
group by	a11.Calendar_Week_Id

create volatile table ZZNB01, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	KonsPrisExkl	FLOAT, 
	KonsPrisInkl	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB01 
select	a14.Cost_Price_List_Cd  Cost_Price_List_Cd,
	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg(a11.Retail_Price_Amt_Excl_VAT)  KonsPrisExkl,
	avg(a11.Retail_Price_Amt_Incl_VAT)  KonsPrisInkl
from	ITSemCMNVOUT.ARTICLE_RETAIL_PRICE_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Calendar_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a11.Location_Seq_Num = a14.Store_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a11.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a17.Art_Hier_Lvl_2_Id in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '21', '22', '23', '27', '28')
 and a14.Cost_Price_List_Cd in ('AA'))
group by	a14.Cost_Price_List_Cd,
	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB02, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB02 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB01	pc11
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC03, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC03 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.KonsPrisExkl)  WJXBFS1,
	max(pa11.KonsPrisInkl)  WJXBFS2
from	ZZNB01	pa11
	join	ZZMB02	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZNB04, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	Listpris	FLOAT, 
	GODWFLAG6_1	INTEGER, 
	Grundpris	FLOAT, 
	GODWFLAG9_1	INTEGER, 
	NettoNetPris	FLOAT, 
	GODWFLAGc_1	INTEGER, 
	PVServPOS	FLOAT, 
	GODWFLAGf_1	INTEGER, 
	PVServPOS1	FLOAT, 
	GODWFLAG12_1	INTEGER, 
	ServPlockSt	FLOAT, 
	GODWFLAG15_1	INTEGER, 
	ServPOSst	FLOAT, 
	GODWFLAG18_1	INTEGER, 
	ServEDIst	FLOAT, 
	GODWFLAG1b_1	INTEGER, 
	ServKrossSt	FLOAT, 
	GODWFLAG1e_1	INTEGER, 
	MarknFakt	FLOAT, 
	GODWFLAG21_1	INTEGER, 
	TransfPris	FLOAT, 
	GODWFLAG24_1	INTEGER, 
	ServSKst	FLOAT, 
	GODWFLAG27_1	INTEGER)
primary index (Cost_Price_List_Cd, Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB04 
select	a11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg((Case when a11.Price_Condition_Type_Cd in ('PB00') then a11.Price_Condition_Amt else NULL end))  Listpris,
	max((Case when a11.Price_Condition_Type_Cd in ('PB00') then 1 else 0 end))  GODWFLAG6_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then a11.Price_Condition_Amt else NULL end))  Grundpris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then 1 else 0 end))  GODWFLAG9_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then a11.Price_Condition_Amt else NULL end))  NettoNetPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then 1 else 0 end))  GODWFLAGc_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z260') then a11.Price_Condition_Amt else NULL end))  PVServPOS,
	max((Case when a11.Price_Condition_Type_Cd in ('Z260') then 1 else 0 end))  GODWFLAGf_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z261') then a11.Price_Condition_Amt else NULL end))  PVServPOS1,
	max((Case when a11.Price_Condition_Type_Cd in ('Z261') then 1 else 0 end))  GODWFLAG12_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z189') then a11.Price_Condition_Amt else NULL end))  ServPlockSt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z189') then 1 else 0 end))  GODWFLAG15_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z183') then a11.Price_Condition_Amt else NULL end))  ServPOSst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z183') then 1 else 0 end))  GODWFLAG18_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z180') then a11.Price_Condition_Amt else NULL end))  ServEDIst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z180') then 1 else 0 end))  GODWFLAG1b_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z185') then a11.Price_Condition_Amt else NULL end))  ServKrossSt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z185') then 1 else 0 end))  GODWFLAG1e_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z230') then a11.Price_Condition_Amt else NULL end))  MarknFakt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z230') then 1 else 0 end))  GODWFLAG21_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then a11.Price_Condition_Amt else NULL end))  TransfPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then 1 else 0 end))  GODWFLAG24_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z187') then a11.Price_Condition_Amt else NULL end))  ServSKst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z187') then 1 else 0 end))  GODWFLAG27_1
from	ITSemCMNVOUT.ARTICLE_COST_PRICE_COND_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Calendar_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a11.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a14.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
where	(a16.Art_Hier_Lvl_2_Id in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '21', '22', '23', '27', '28')
 and a11.Cost_Price_List_Cd in ('AA')
 and (a11.Price_Condition_Type_Cd in ('PB00')
 or a11.Price_Condition_Type_Cd in ('ZPB2')
 or a11.Price_Condition_Type_Cd in ('ZEKN')
 or a11.Price_Condition_Type_Cd in ('Z260')
 or a11.Price_Condition_Type_Cd in ('Z261')
 or a11.Price_Condition_Type_Cd in ('Z189')
 or a11.Price_Condition_Type_Cd in ('Z183')
 or a11.Price_Condition_Type_Cd in ('Z180')
 or a11.Price_Condition_Type_Cd in ('Z185')
 or a11.Price_Condition_Type_Cd in ('Z230')
 or a11.Price_Condition_Type_Cd in ('ZKP2')
 or a11.Price_Condition_Type_Cd in ('Z187')))
group by	a11.Cost_Price_List_Cd,
	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB05, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB05 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG6_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC06, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC06 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.Listpris)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB05	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG6_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB07, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB07 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG9_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC08, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC08 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.Grundpris)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB07	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG9_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB09, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB09 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAGc_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0A, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0A 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.NettoNetPris)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB09	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAGc_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0B, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0B 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAGf_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0C, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0C 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0B	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAGf_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0D, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0D 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG12_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0E, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0E 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS1)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0D	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG12_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0F, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0F 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG15_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0G, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0G 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPlockSt)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0F	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG15_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0H, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0H 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG18_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0I, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0I 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPOSst)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0H	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG18_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0J, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0J 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG1b_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0K, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0K 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServEDIst)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0J	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG1b_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0L, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0L 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG1e_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0M, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0M 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServKrossSt)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0L	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG1e_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0N, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0N 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG21_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0O, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0O 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.MarknFakt)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0N	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG21_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0P, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0P 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG24_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0Q, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0Q 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.TransfPris)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0P	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG24_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

create volatile table ZZMB0R, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB0R 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
where	pc11.GODWFLAG27_1 = 1
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num

create volatile table ZZNC0S, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZNC0S 
select	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServSKst)  WJXBFS1
from	ZZNB04	pa11
	join	ZZMB0R	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
where	pa11.GODWFLAG27_1 = 1
group by	pa11.Cost_Price_List_Cd,
	pa11.Article_Seq_Num

select	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num)  Article_Seq_Num,
	max(a114.Article_Id)  Article_Id,
	max(a114.Article_Desc)  Article_Desc,
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd, pa19.Cost_Price_List_Cd, pa110.Cost_Price_List_Cd, pa111.Cost_Price_List_Cd, pa112.Cost_Price_List_Cd, pa113.Cost_Price_List_Cd)  Cost_Price_List_Cd,
	max(a115.Price_List_Name)  Price_List_Name,
	max(pa11.WJXBFS1)  KonsPrisExkl,
	max(pa11.WJXBFS2)  KonsPrisInkl,
	max(pa12.WJXBFS1)  Listpris,
	max(pa13.WJXBFS1)  Grundpris,
	max(pa14.WJXBFS1)  NettoNetPris,
	(ZEROIFNULL(max(pa15.WJXBFS1)) + ZEROIFNULL(max(pa16.WJXBFS1)))  Kedjepaslag,
	max(pa17.WJXBFS1)  ServPlockSt,
	max(pa18.WJXBFS1)  ServPOSst,
	max(pa19.WJXBFS1)  ServEDIst,
	max(pa110.WJXBFS1)  ServKrossSt,
	max(pa111.WJXBFS1)  MarknFakt,
	max(pa112.WJXBFS1)  TransfPris,
	max(pa113.WJXBFS1)  ServSKst
from	ZZNC03	pa11
	full outer join	ZZNC06	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd)
	full outer join	ZZNC08	pa13
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = pa13.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd) = pa13.Cost_Price_List_Cd)
	full outer join	ZZNC0A	pa14
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num) = pa14.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd) = pa14.Cost_Price_List_Cd)
	full outer join	ZZNC0C	pa15
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num) = pa15.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd) = pa15.Cost_Price_List_Cd)
	full outer join	ZZNC0E	pa16
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num) = pa16.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd) = pa16.Cost_Price_List_Cd)
	full outer join	ZZNC0G	pa17
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num) = pa17.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd) = pa17.Cost_Price_List_Cd)
	full outer join	ZZNC0I	pa18
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num) = pa18.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd) = pa18.Cost_Price_List_Cd)
	full outer join	ZZNC0K	pa19
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num) = pa19.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd) = pa19.Cost_Price_List_Cd)
	full outer join	ZZNC0M	pa110
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num) = pa110.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd, pa19.Cost_Price_List_Cd) = pa110.Cost_Price_List_Cd)
	full outer join	ZZNC0O	pa111
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num) = pa111.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd, pa19.Cost_Price_List_Cd, pa110.Cost_Price_List_Cd) = pa111.Cost_Price_List_Cd)
	full outer join	ZZNC0Q	pa112
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num) = pa112.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd, pa19.Cost_Price_List_Cd, pa110.Cost_Price_List_Cd, pa111.Cost_Price_List_Cd) = pa112.Cost_Price_List_Cd)
	full outer join	ZZNC0S	pa113
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num) = pa113.Article_Seq_Num and 
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd, pa19.Cost_Price_List_Cd, pa110.Cost_Price_List_Cd, pa111.Cost_Price_List_Cd, pa112.Cost_Price_List_Cd) = pa113.Cost_Price_List_Cd)
	join	ITSemCMNVOUT.ARTICLE_D	a114
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num) = a114.Article_Seq_Num)
	join	ITSemCMNVOUT.PRICE_LIST_D	a115
	  on 	(coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd, pa19.Cost_Price_List_Cd, pa110.Cost_Price_List_Cd, pa111.Cost_Price_List_Cd, pa112.Cost_Price_List_Cd, pa113.Cost_Price_List_Cd) = a115.Price_List_Cd)
group by	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num),
	coalesce(pa11.Cost_Price_List_Cd, pa12.Cost_Price_List_Cd, pa13.Cost_Price_List_Cd, pa14.Cost_Price_List_Cd, pa15.Cost_Price_List_Cd, pa16.Cost_Price_List_Cd, pa17.Cost_Price_List_Cd, pa18.Cost_Price_List_Cd, pa19.Cost_Price_List_Cd, pa110.Cost_Price_List_Cd, pa111.Cost_Price_List_Cd, pa112.Cost_Price_List_Cd, pa113.Cost_Price_List_Cd)


SET QUERY_BAND = NONE For Session;


drop table ZZMQ00

drop table ZZNB01

drop table ZZMB02

drop table ZZNC03

drop table ZZNB04

drop table ZZMB05

drop table ZZNC06

drop table ZZMB07

drop table ZZNC08

drop table ZZMB09

drop table ZZNC0A

drop table ZZMB0B

drop table ZZNC0C

drop table ZZMB0D

drop table ZZNC0E

drop table ZZMB0F

drop table ZZNC0G

drop table ZZMB0H

drop table ZZNC0I

drop table ZZMB0J

drop table ZZNC0K

drop table ZZMB0L

drop table ZZNC0M

drop table ZZMB0N

drop table ZZNC0O

drop table ZZMB0P

drop table ZZNC0Q

drop table ZZMB0R

drop table ZZNC0S
