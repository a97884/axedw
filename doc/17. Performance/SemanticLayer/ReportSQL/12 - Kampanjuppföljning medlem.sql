SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=EDW3-IT; Action=Kampanjuppföljning medlem; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Campaign_Id	DECIMAL(10, 0), 
	Member_Account_Seq_Num	INTEGER, 
	Utilization_Ind	BYTEINT)
primary index (Campaign_Id, Member_Account_Seq_Num, Utilization_Ind) on commit preserve rows

;insert into ZZMQ00 
select	a11.Campaign_Id  Campaign_Id,
	a13.Member_Account_Seq_Num  Member_Account_Seq_Num,
	a13.Utilization_Ind  Utilization_Ind
from	ITSemCMNVOUT.CAMPAIGN_D	a11
	join	ITSemCMNVOUT.CAMPAIGN_LOYALTY_MEMBER_B	a13
	  on 	(a11.Campaign_Id = a13.Campaign_Id)
where	(a13.Utilization_Ind = 1
 and a11.Campaign_Num = '100062333')
group by	a11.Campaign_Id,
	a13.Member_Account_Seq_Num,
	a13.Utilization_Ind

create volatile table ZZRF01, no fallback, no log(
	Membership_Num	VARCHAR(80))
primary index (Membership_Num) on commit preserve rows

;insert into ZZRF01 
select	r13.Membership_Num  Membership_Num
from	ITSemCMNVOUT.CAMPAIGN_LOYALTY_MEMBER_B	r11
	join	ITSemCMNVOUT.CAMPAIGN_D	r12
	  on 	(r11.Campaign_Id = r12.Campaign_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	r13
	  on 	(r11.Member_Account_Seq_Num = r13.Member_Account_Seq_Num)
where	r12.Campaign_Num = '100062333'
group by	r13.Membership_Num

create volatile table ZZRF02, no fallback, no log(
	Membership_Num	VARCHAR(80))
primary index (Membership_Num) on commit preserve rows

;insert into ZZRF02 
select	r13.Membership_Num  Membership_Num
from	ITSemCMNVOUT.CAMPAIGN_LOYALTY_MEMBER_B	r11
	join	ZZMQ00	pr12
	  on 	(r11.Campaign_Id = pr12.Campaign_Id and 
	r11.Member_Account_Seq_Num = pr12.Member_Account_Seq_Num and 
	r11.Utilization_Ind = pr12.Utilization_Ind)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	r13
	  on 	(pr12.Member_Account_Seq_Num = r13.Member_Account_Seq_Num)
group by	r13.Membership_Num

create volatile table ZZMD03, no fallback, no log(
	Concept_Cd	CHAR(3), 
	kampanjstorlek	INTEGER, 
	WJXBFS1	INTEGER, 
	WJXBFS2	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZMD03 
select	a14.Concept_Cd  Concept_Cd,
	count(distinct a12.Membership_Num)  kampanjstorlek,
	(Case when max((Case when a11.Utilization_Ind = 1 then 1 else 0 end)) = 1 then count(distinct (Case when a11.Utilization_Ind = 1 then a12.Membership_Num else NULL end)) else NULL end)  WJXBFS1,
	(Case when max((Case when a11.Utilization_Ind = 1 then 1 else 0 end)) = 1 then ZEROIFNULL((count(distinct (Case when a11.Utilization_Ind = 1 then a12.Membership_Num else NULL end)) * 1.0)) else NULL end)  WJXBFS2
from	ITSemCMNVOUT.CAMPAIGN_LOYALTY_MEMBER_B	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.CAMPAIGN_D	a13
	  on 	(a11.Campaign_Id = a13.Campaign_Id)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a12.Home_Store_Seq_Num = a14.Store_Seq_Num)
where	a13.Campaign_Num = '100062333'
group by	a14.Concept_Cd

create volatile table ZZMD04, no fallback, no log(
	Concept_Cd	CHAR(3), 
	KampForsBelEx	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZMD04 
select	a16.Concept_Cd  Concept_Cd,
	sum(a11.Unit_Selling_Price_Amt)  KampForsBelEx
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a12
	  on 	(a11.Contact_Account_Seq_Num = a12.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a12.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ZZRF01	pa14
	  on 	(a13.Membership_Num = pa14.Membership_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a11.Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a16
	  on 	(a13.Home_Store_Seq_Num = a16.Store_Seq_Num)
where	(a11.Tran_Dt between DATE '2013-04-18' and DATE '2013-05-04'
 and a15.Concept_Cd in ('HEM', 'HEA')
 and ((a11.Tran_Dt)
 in	(select	c21.Calendar_Dt
	from	ITSemCMNVOUT.CALENDAR_DAY_D	c21
		cross join	ITSemCMNVOUT.CAMPAIGN_D	c22
	where	c21.Calendar_Dt between c22.Campaing_Start_Dt and c22.Campaing_End_Dt
	group by	c21.Calendar_Dt)))
group by	a16.Concept_Cd

create volatile table ZZMD05, no fallback, no log(
	Concept_Cd	CHAR(3), 
	KampInkopsBelEx	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZMD05 
select	a16.Concept_Cd  Concept_Cd,
	sum(a11.Unit_Cost_Amt)  KampInkopsBelEx
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a12
	  on 	(a11.Contact_Account_Seq_Num = a12.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a12.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ZZRF01	pa14
	  on 	(a13.Membership_Num = pa14.Membership_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a11.Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a16
	  on 	(a13.Home_Store_Seq_Num = a16.Store_Seq_Num)
where	(((a11.Tran_Dt)
 in	(select	c21.Calendar_Dt
	from	ITSemCMNVOUT.CALENDAR_DAY_D	c21
		cross join	ITSemCMNVOUT.CAMPAIGN_D	c22
	where	c21.Calendar_Dt between c22.Campaing_Start_Dt and c22.Campaing_End_Dt
	group by	c21.Calendar_Dt))
 and a15.Concept_Cd in ('HEM', 'HEA'))
group by	a16.Concept_Cd

create volatile table ZZMD06, no fallback, no log(
	Concept_Cd	CHAR(3), 
	KampBvUtnyttjande	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZMD06 
select	a16.Concept_Cd  Concept_Cd,
	(ZEROIFNULL(sum(a11.Unit_Selling_Price_Amt)) - ZEROIFNULL(sum(a11.Unit_Cost_Amt)))  KampBvUtnyttjande
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a12
	  on 	(a11.Contact_Account_Seq_Num = a12.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a12.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ZZRF02	pa14
	  on 	(a13.Membership_Num = pa14.Membership_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a11.Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a16
	  on 	(a13.Home_Store_Seq_Num = a16.Store_Seq_Num)
where	(((a12.Member_Account_Seq_Num)
 in	(select	pc21.Member_Account_Seq_Num
	from	ZZMQ00	pc21
	group by	pc21.Member_Account_Seq_Num))
 and a11.Tran_Dt between DATE '2013-04-18' and DATE '2013-05-04'
 and a15.Concept_Cd in ('HEM', 'HEA'))
group by	a16.Concept_Cd

create volatile table ZZSP07, no fallback, no log(
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZSP07 
select	a14.Concept_Cd  Concept_Cd,
	sum(a11.Accrual_Amt)  WJXBFS1,
	sum(a11.Purchase_Amt)  WJXBFS2,
	sum(a11.Downgraded_Amt)  WJXBFS3
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a13.Home_Store_Seq_Num = a14.Store_Seq_Num)
where	(a11.Loyalty_Transaction_Dt between DATE '2013-04-18' and DATE '2013-05-04'
 and a12.Concept_Cd in ('HEM', 'HEA'))
group by	a14.Concept_Cd

create volatile table ZZSP08, no fallback, no log(
	Concept_Cd	CHAR(3), 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	KampAntalSalda	FLOAT, 
	KampForsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT, 
	GODWFLAG13_1	INTEGER, 
	KampAntalSaldaMedl	FLOAT, 
	GODWFLAG14_1	INTEGER, 
	AntalSaldaMedl	FLOAT, 
	ForsBelExMedl	FLOAT, 
	GODWFLAG16_1	INTEGER)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZSP08 
select	a15.Concept_Cd  Concept_Cd,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Item_Qty else NULL end))  KampAntalSalda,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Selling_Price_Amt else NULL end))  KampForsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer,
	max((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then 1 else 0 end))  GODWFLAG13_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Item_Qty else NULL end))  KampAntalSaldaMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG14_1,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Item_Qty else NULL end))  AntalSaldaMedl,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG16_1
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a13
	  on 	(a11.Contact_Account_Seq_Num = a13.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a13.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a14.Home_Store_Seq_Num = a15.Store_Seq_Num)
where	(a11.Tran_Dt between DATE '2013-04-18' and DATE '2013-05-04'
 and a12.Concept_Cd in ('HEM', 'HEA'))
group by	a15.Concept_Cd

create volatile table ZZMD09, no fallback, no log(
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZMD09 
select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	((ZEROIFNULL(pa11.WJXBFS1) - ZEROIFNULL(pa11.WJXBFS2)) - ZEROIFNULL(pa11.WJXBFS3))  WJXBFS1,
	pa12.AntalSalda  AntalSalda,
	pa12.ForsBelEx  ForsBelEx
from	ZZSP07	pa11
	full outer join	ZZSP08	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd)

create volatile table ZZOP0A, no fallback, no log(
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT, 
	WJXBFS4	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZOP0A 
select	pa01.Concept_Cd  Concept_Cd,
	pa01.KampAntalSalda  WJXBFS1,
	pa01.KampForsBelEx  WJXBFS2,
	pa01.KampInkopsBelEx  WJXBFS3,
	pa01.KampForsBelExBVBer  WJXBFS4
from	ZZSP08	pa01
where	pa01.GODWFLAG13_1 = 1

create volatile table ZZOP0B, no fallback, no log(
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZOP0B 
select	pa01.Concept_Cd  Concept_Cd,
	pa01.KampAntalSaldaMedl  WJXBFS1
from	ZZSP08	pa01
where	pa01.GODWFLAG14_1 = 1

create volatile table ZZMD0C, no fallback, no log(
	Concept_Cd	CHAR(3), 
	KampAntalSalda	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZMD0C 
select	a16.Concept_Cd  Concept_Cd,
	sum(a11.Item_Qty)  KampAntalSalda
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a12
	  on 	(a11.Contact_Account_Seq_Num = a12.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a12.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ZZRF02	pa14
	  on 	(a13.Membership_Num = pa14.Membership_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a11.Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a16
	  on 	(a13.Home_Store_Seq_Num = a16.Store_Seq_Num)
where	(((a12.Member_Account_Seq_Num)
 in	(select	pc21.Member_Account_Seq_Num
	from	ZZMQ00	pc21
	group by	pc21.Member_Account_Seq_Num))
 and a11.Tran_Dt between DATE '2013-04-18' and DATE '2013-05-04'
 and a15.Concept_Cd in ('HEM', 'HEA')
 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  '))
group by	a16.Concept_Cd

create volatile table ZZOP0D, no fallback, no log(
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Concept_Cd) on commit preserve rows

;insert into ZZOP0D 
select	pa01.Concept_Cd  Concept_Cd,
	pa01.AntalSaldaMedl  WJXBFS1,
	pa01.ForsBelExMedl  WJXBFS2
from	ZZSP08	pa01
where	pa01.GODWFLAG16_1 = 1

select	coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa16.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd, pa19.Concept_Cd, pa110.Concept_Cd)  Concept_Cd,
	a111.Concept_Name  Concept_Name,
	pa12.kampanjstorlek  kampanjstorlek,
	pa12.WJXBFS1  Utnyttjande,
	ZEROIFNULL((pa12.WJXBFS2 / NULLIFZERO(pa12.kampanjstorlek)))  utnyttjandegrad,
	(ZEROIFNULL(pa13.KampForsBelEx) - ZEROIFNULL(pa14.KampInkopsBelEx))  KampBvTotalt,
	pa15.KampBvUtnyttjande  KampBvUtnyttjande,
	((ZEROIFNULL(pa13.KampForsBelEx) - ZEROIFNULL(pa14.KampInkopsBelEx)) - ZEROIFNULL(pa16.WJXBFS1))  KampBvInklBonus,
	pa17.WJXBFS1  KampAntalSalda,
	pa18.WJXBFS1  KampAntalSaldaMedl,
	pa19.KampAntalSalda  KampAntalSalda1,
	pa16.AntalSalda  AntalSalda,
	pa110.WJXBFS1  AntalSaldaMedl,
	ZEROIFNULL((pa17.WJXBFS1 / NULLIFZERO(pa12.kampanjstorlek)))  WJXBFS1,
	pa110.WJXBFS2  ForsBelExMedl,
	pa17.WJXBFS2  KampForsBelEx,
	pa16.ForsBelEx  ForsBelEx,
	pa17.WJXBFS3  KampInkopsBelEx,
	pa17.WJXBFS4  KampForsBelExBVBer
from	ZZMD03	pa12
	full outer join	ZZMD04	pa13
	  on 	(pa12.Concept_Cd = pa13.Concept_Cd)
	full outer join	ZZMD05	pa14
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd) = pa14.Concept_Cd)
	full outer join	ZZMD06	pa15
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd) = pa15.Concept_Cd)
	full outer join	ZZMD09	pa16
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd) = pa16.Concept_Cd)
	full outer join	ZZOP0A	pa17
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa16.Concept_Cd) = pa17.Concept_Cd)
	full outer join	ZZOP0B	pa18
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa16.Concept_Cd, pa17.Concept_Cd) = pa18.Concept_Cd)
	full outer join	ZZMD0C	pa19
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa16.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd) = pa19.Concept_Cd)
	full outer join	ZZOP0D	pa110
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa16.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd, pa19.Concept_Cd) = pa110.Concept_Cd)
	join	ITSemCMNVOUT.CONCEPT_D	a111
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa16.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd, pa19.Concept_Cd, pa110.Concept_Cd) = a111.Concept_Cd)


SET QUERY_BAND = NONE For Session;


drop table ZZMQ00

drop table ZZRF01

drop table ZZRF02

drop table ZZMD03

drop table ZZMD04

drop table ZZMD05

drop table ZZMD06

drop table ZZSP07

drop table ZZSP08

drop table ZZMD09

drop table ZZOP0A

drop table ZZOP0B

drop table ZZMD0C

drop table ZZOP0D
