SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=15 - MA205 - Uppföljning av SOS barnbyar per butik; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Membership_Num) on commit preserve rows

;insert into ZZMD00 
select	a13.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num <> -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a11.Calendar_Month_Id in (201306)
 and a12.Concept_Cd in ('HEM')
 and a14.Loyalty_Program_Id in ('1-MQ7V'))
group by	a13.Membership_Num

create volatile table ZZMD01, no fallback, no log(
	AccrualAmtButik	FLOAT) on commit preserve rows

;insert into ZZMD01 
select	sum(pa11.WJXBFS1)  AccrualAmtButik
from	ZZMD00	pa11

create volatile table ZZMD02, no fallback, no log(
	Utbetalad_bonus_SOS	FLOAT) on commit preserve rows

;insert into ZZMD02 
select	sum(a11.Remittance_Amt)  Utbetalad_bonus_SOS
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_Next_Month_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PARTNER_D	a15
	  on 	(a11.Partner_Seq_Num = a15.Partner_Seq_Num)
where	(a12.Calendar_Month_Id in (201306)
 and a14.Loyalty_Program_Id in ('1-MQ7V')
 and a15.Partner_Id in ('0001008556'))

create volatile table ZZMD03, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	AccrualAmt	FLOAT)
primary index (Store_Type_Cd, Calendar_Month_Id, Loyalty_Member_Type, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	a12.Store_Type_Cd  Store_Type_Cd,
	a11.Calendar_Month_Id  Calendar_Month_Id,
	a13.Loyalty_Member_Type  Loyalty_Member_Type,
	a12.Store_Id  Store_Id,
	sum(a11.Accrual_Amt)  AccrualAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a11.Calendar_Month_Id in (201306)
 and a12.Concept_Cd in ('HEM')
 and a14.Loyalty_Program_Id in ('1-MQ7V'))
group by	a12.Store_Type_Cd,
	a11.Calendar_Month_Id,
	a13.Loyalty_Member_Type,
	a12.Store_Id

select	pa11.Store_Type_Cd  Store_Type_Cd,
	max(a16.Store_Type_Desc)  Store_Type_Desc,
	pa11.Store_Id  Store_Id,
	max(a14.Store_Name)  Store_Name,
	pa11.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Loyalty_Member_Type  Loyalty_Member_Type,
	max(a15.Loyalty_Member_Type_Desc)  Loyalty_Member_Type_Desc,
	max(pa12.AccrualAmtButik)  AccrualAmtButik,
	max(pa13.Utbetalad_bonus_SOS)  Utbetalad_bonus_SOS,
	max(pa11.AccrualAmt)  AccrualAmt
from	ZZMD03	pa11
	cross join	ZZMD01	pa12
	cross join	ZZMD02	pa13
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(pa11.Store_Id = a14.Store_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TYPE_D	a15
	  on 	(pa11.Loyalty_Member_Type = a15.Loyalty_Member_Type)
	join	ITSemCMNVOUT.STORE_TYPE_D	a16
	  on 	(pa11.Store_Type_Cd = a16.Store_Type_Cd)
group by	pa11.Store_Type_Cd,
	pa11.Store_Id,
	pa11.Calendar_Month_Id,
	pa11.Loyalty_Member_Type


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03
