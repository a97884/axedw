Report: PS301 - Rapportguide Priss�ttning - vecka
Job: 197810
Report Cache Used: No

Query Engine Execution Start Time:		2013-05-28 14:52:16
Query Engine Execution Finish Time:		2013-05-28 14:54:11

Total Duration in Query Engine: 		0:01:55.23
SQL Generation:		0:01:41.00
SQL Execution:		0:01:54.26
Data Fetch Duration from Datasource(s):		0:00:00.00
Total Data Fetch Duration:		0:00:00.00
Analytical Duration:		0:00:00.00
Other Processing Duration in Query Engine:		0:00:00.97

Number of Rows Returned:		0
Number of Columns Returned:		0
Number of Temp Tables:		1

Total Number of Passes:		4
Number of SQL Passes:		4
Number of Analytical Passes:		0

DB User:		PR_MSTR_Admin
DB Instance:		EDWP

Tables Accessed:
ARTICLE_HIERARCHY_LVL_2_D	
ARTICLE_HIERARCHY_LVL_8_D	
CALENDAR_DAY_D	
ARTICLE_D	
ARTICLE_COST_PRICE_COND_F	


The following error occurs in pass	3:	Execute Query failed. 
Error type: Odbc error. Odbc operation attempted: SQLExecDirect. [HY008:-3110: on SQLHANDLE] [Teradata][ODBC Teradata Driver][Teradata Database] The transaction was aborted by the user.Unable to get catalog string. Connection String: DSN=edwp;UID=PR_MSTR_Admin;. SQL Statement: ;insert into T3XSBJ2C2NB000 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z115') then a11.Price_Condition_Amt else NULL end))  FUREfterskNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z115') then 1 else 0 end))  GODWFLAG1_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z105') then a11.Price_Condition_Amt else NULL end))  FURFakturaNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z105') then 1 else 0 end))  GODWFLAG4_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then a11.Price_Condition_Amt else NULL end))  TransfPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then 1 else 0 end))  GODWFLAG7_1,
	avg((Case when (a11.Price_Condition_Type_Cd in ('Z125') or a11.Price_Condition_Type_Cd = 'Z126') then a11.Price_Condition_Amt else NULL end))  EstPallrabatt,
	max((Case when (a11.Price_Condition_Type_Cd in ('Z125') or a11.Price_Condition_Type_Cd = 'Z126') then 1 else 0 end))  GODWFLAGd_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141', 'Z145') then a11.Price_Condition_Amt else NULL end))  MarknRab,
	max((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141', 'Z145') then 1 else 0 end))  GODWFLAG10_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('EKNN') then a11.Price_Condition_Amt else NULL end))  NettoNetPris,
	max((Case when a11.Price_Condition_Type_Cd in ('EKNN') then 1 else 0 end))  GODWFLAG13_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z260', 'Z261') then a11.Price_Condition_Amt else NULL end))  Kedjepaslag,
	max((Case when a11.Price_Condition_Type_Cd in ('Z260', 'Z261') then 1 else 0 end))  GODWFLAG16_1,
	avg((Case when (a11.Price_Condition_Type_Cd in ('Z180') or a11.Price_Condition_Type_Cd = 'Z181') then a11.Price_Condition_Amt else NULL end))  PVServiceEDI,
	max((Case when (a11.Price_Condition_Type_Cd in ('Z180') or a11.Price_Condition_Type_Cd = 'Z181') then 1 else 0 end))  GODWFLAG19_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z232') then a11.Price_Condition_Amt else NULL end))  EMVFaktor,
	max((Case when a11.Price_Condition_Type_Cd in ('Z232') then 1 else 0 end))  GODWFLAG1c_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z183') then a11.Price_Condition_Amt else NULL end))  PVServPOS,
	max((Case when a11.Price_Condition_Type_Cd in ('Z183') then 1 else 0 end))  GODWFLAG1f_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then a11.Price_Condition_Amt else NULL end))  Grundpris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then 1 else 0 end))  GODWFLAG22_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z187') then a11.Price_Condition_Amt else NULL end))  PVServSaljk,
	max((Case when a11.Price_Condition_Type_Cd in ('Z187') then 1 else 0 end))  GODWFLAG25_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z185') then a11.Price_Condition_Amt else NULL end))  PVServKross,
	max((Case when a11.Price_Condition_Type_Cd in ('Z185') then 1 else 0 end))  GODWFLAG28_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z189') then a11.Price_Condition_Amt else NULL end))  PVServPlock,
	max((Case when a11.Price_Condition_Type_Cd in ('Z189') then 1 else 0 end))  GODWFLAG2b_1
from	PRSemCMNVOUT.ARTICLE_COST_PRICE_COND_F	a11
	join	PRSemCMNVOUT.ARTICLE_D	a12
	  on 	(a11.Article_Seq_Num = a12.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a13
	  on 	(a12.Art_Hier_Lvl_8_Seq_Num = a13.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a14
	  on 	(a13.Art_Hier_Lvl_2_Seq_Num = a14.Art_Hier_Lvl_2_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Calendar_Dt = a15.Calendar_Dt)
where	(a15.Calendar_Week_Id in (201320)
 and a14.Art_Hier_Lvl_2_Id in ('13')
 and (a11.Price_Condition_Type_Cd in ('Z115')
 or a11.Price_Condition_Type_Cd in ('Z105')
 or a11.Price_Condition_Type_Cd in ('ZKP2')
 or (a11.Price_Condition_Type_Cd in ('Z125')
 or a11.Price_Condition_Type_Cd = 'Z126')
 or a11.Price_Condition_Type_Cd in ('Z140', 'Z141', 'Z145')
 or a11.Price_Condition_Type_Cd in ('EKNN')
 or a11.Price_Condition_Type_Cd in ('Z260', 'Z261')
 or (a11.Price_Condition_Type_Cd in ('Z180')
 or a11.Price_Condition_Type_Cd = 'Z181')
 or a11.Price_Condition_Type_Cd in ('Z232')
 or a11.Price_Condition_Type_Cd in ('Z183')
 or a11.Price_Condition_Type_Cd in ('ZPB2')
 or a11.Price_Condition_Type_Cd in ('Z187')
 or a11.Price_Condition_Type_Cd in ('Z185')
 or a11.Price_Condition_Type_Cd in ('Z189')))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt.

SQL Statements:

Pass0 - 	Execution Duration:	0:00:00.11
	Data Fetch Duration from Datasource(s):	0:00:00.00
	Total Data Fetch Duration:	0:00:00.00
	Other Processing Duration:	0:00:00.02
SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=PS301 - Rapportguide Priss�ttning - vecka; ClientUser=Administrator;' For Session;

Pass1 - 	Execution Duration:	0:00:00.13
	Data Fetch Duration from Datasource(s):	0:00:00.00
	Total Data Fetch Duration:	0:00:00.00
	Other Processing Duration:	0:00:00.02
create volatile table T3XSBJ2C2NB000, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	FUREfterskNet	FLOAT, 
	GODWFLAG1_1	INTEGER, 
	FURFakturaNet	FLOAT, 
	GODWFLAG4_1	INTEGER, 
	TransfPris	FLOAT, 
	GODWFLAG7_1	INTEGER, 
	EstPallrabatt	FLOAT, 
	GODWFLAGd_1	INTEGER, 
	MarknRab	FLOAT, 
	GODWFLAG10_1	INTEGER, 
	NettoNetPris	FLOAT, 
	GODWFLAG13_1	INTEGER, 
	Kedjepaslag	FLOAT, 
	GODWFLAG16_1	INTEGER, 
	PVServiceEDI	FLOAT, 
	GODWFLAG19_1	INTEGER, 
	EMVFaktor	FLOAT, 
	GODWFLAG1c_1	INTEGER, 
	PVServPOS	FLOAT, 
	GODWFLAG1f_1	INTEGER, 
	Grundpris	FLOAT, 
	GODWFLAG22_1	INTEGER, 
	PVServSaljk	FLOAT, 
	GODWFLAG25_1	INTEGER, 
	PVServKross	FLOAT, 
	GODWFLAG28_1	INTEGER, 
	PVServPlock	FLOAT, 
	GODWFLAG2b_1	INTEGER)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

Pass2 - 	Execution Duration:	0:01:53.85
	Data Fetch Duration from Datasource(s):	0:00:00.00
	Total Data Fetch Duration:	0:00:00.00
	Other Processing Duration:	0:00:00.02
;insert into T3XSBJ2C2NB000 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z115') then a11.Price_Condition_Amt else NULL end))  FUREfterskNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z115') then 1 else 0 end))  GODWFLAG1_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z105') then a11.Price_Condition_Amt else NULL end))  FURFakturaNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z105') then 1 else 0 end))  GODWFLAG4_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then a11.Price_Condition_Amt else NULL end))  TransfPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then 1 else 0 end))  GODWFLAG7_1,
	avg((Case when (a11.Price_Condition_Type_Cd in ('Z125') or a11.Price_Condition_Type_Cd = 'Z126') then a11.Price_Condition_Amt else NULL end))  EstPallrabatt,
	max((Case when (a11.Price_Condition_Type_Cd in ('Z125') or a11.Price_Condition_Type_Cd = 'Z126') then 1 else 0 end))  GODWFLAGd_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141', 'Z145') then a11.Price_Condition_Amt else NULL end))  MarknRab,
	max((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141', 'Z145') then 1 else 0 end))  GODWFLAG10_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('EKNN') then a11.Price_Condition_Amt else NULL end))  NettoNetPris,
	max((Case when a11.Price_Condition_Type_Cd in ('EKNN') then 1 else 0 end))  GODWFLAG13_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z260', 'Z261') then a11.Price_Condition_Amt else NULL end))  Kedjepaslag,
	max((Case when a11.Price_Condition_Type_Cd in ('Z260', 'Z261') then 1 else 0 end))  GODWFLAG16_1,
	avg((Case when (a11.Price_Condition_Type_Cd in ('Z180') or a11.Price_Condition_Type_Cd = 'Z181') then a11.Price_Condition_Amt else NULL end))  PVServiceEDI,
	max((Case when (a11.Price_Condition_Type_Cd in ('Z180') or a11.Price_Condition_Type_Cd = 'Z181') then 1 else 0 end))  GODWFLAG19_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z232') then a11.Price_Condition_Amt else NULL end))  EMVFaktor,
	max((Case when a11.Price_Condition_Type_Cd in ('Z232') then 1 else 0 end))  GODWFLAG1c_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z183') then a11.Price_Condition_Amt else NULL end))  PVServPOS,
	max((Case when a11.Price_Condition_Type_Cd in ('Z183') then 1 else 0 end))  GODWFLAG1f_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then a11.Price_Condition_Amt else NULL end))  Grundpris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then 1 else 0 end))  GODWFLAG22_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z187') then a11.Price_Condition_Amt else NULL end))  PVServSaljk,
	max((Case when a11.Price_Condition_Type_Cd in ('Z187') then 1 else 0 end))  GODWFLAG25_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z185') then a11.Price_Condition_Amt else NULL end))  PVServKross,
	max((Case when a11.Price_Condition_Type_Cd in ('Z185') then 1 else 0 end))  GODWFLAG28_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z189') then a11.Price_Condition_Amt else NULL end))  PVServPlock,
	max((Case when a11.Price_Condition_Type_Cd in ('Z189') then 1 else 0 end))  GODWFLAG2b_1
from	PRSemCMNVOUT.ARTICLE_COST_PRICE_COND_F	a11
	join	PRSemCMNVOUT.ARTICLE_D	a12
	  on 	(a11.Article_Seq_Num = a12.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a13
	  on 	(a12.Art_Hier_Lvl_8_Seq_Num = a13.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a14
	  on 	(a13.Art_Hier_Lvl_2_Seq_Num = a14.Art_Hier_Lvl_2_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Calendar_Dt = a15.Calendar_Dt)
where	(a15.Calendar_Week_Id in (201320)
 and a14.Art_Hier_Lvl_2_Id in ('13')
 and (a11.Price_Condition_Type_Cd in ('Z115')
 or a11.Price_Condition_Type_Cd in ('Z105')
 or a11.Price_Condition_Type_Cd in ('ZKP2')
 or (a11.Price_Condition_Type_Cd in ('Z125')
 or a11.Price_Condition_Type_Cd = 'Z126')
 or a11.Price_Condition_Type_Cd in ('Z140', 'Z141', 'Z145')
 or a11.Price_Condition_Type_Cd in ('EKNN')
 or a11.Price_Condition_Type_Cd in ('Z260', 'Z261')
 or (a11.Price_Condition_Type_Cd in ('Z180')
 or a11.Price_Condition_Type_Cd = 'Z181')
 or a11.Price_Condition_Type_Cd in ('Z232')
 or a11.Price_Condition_Type_Cd in ('Z183')
 or a11.Price_Condition_Type_Cd in ('ZPB2')
 or a11.Price_Condition_Type_Cd in ('Z187')
 or a11.Price_Condition_Type_Cd in ('Z185')
 or a11.Price_Condition_Type_Cd in ('Z189')))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

Pass3 - 	Execution Duration:	0:00:00.15
	Data Fetch Duration from Datasource(s):	0:00:00.00
	Total Data Fetch Duration:	0:00:00.00
	Other Processing Duration:	0:00:00.04
drop table T3XSBJ2C2NB000

[Analytical engine calculation steps:
	1.  Calculate metric: <Netto inkl service> at original data level
	2.  Calculate metric: <Tmarg Axfood Tot st> at original data level
	3.  Calculate metric: <Ink�pspris Kedja> at original data level
	4.  Calculate metric: <Tmarg Butik Tot st> at original data level
	5.  Calculate metric: <Tmarg Kedja Centr st> at original data level
	6.  Calculate metric: <Tmarg Kedja v�rde> at original data level
	7.  Calculate metric: <Tmarg Kedja v�rde kalkyl> at original data level
	8.  Calculate metric: <Kommersiell marginal kr/st> at original data level
	9.  Calculate subtotal: <Totalt> 
	10.  Calculate metric: <Netto inkl service> at subtotal levels
	11.  Calculate metric: <Tmarg Axfood Tot st> at subtotal levels
	12.  Calculate metric: <Ink�pspris Kedja> at subtotal levels
	13.  Calculate metric: <Tmarg Butik Tot st> at subtotal levels
	14.  Calculate metric: <Tmarg Kedja Centr st> at subtotal levels
	15.  Calculate metric: <Tmarg Kedja v�rde> at subtotal levels
	16.  Calculate metric: <Tmarg Kedja v�rde kalkyl> at subtotal levels
	17.  Calculate metric: <Kommersiell marginal kr/st> at subtotal levels
	18.  Perform cross-tabbing
]
