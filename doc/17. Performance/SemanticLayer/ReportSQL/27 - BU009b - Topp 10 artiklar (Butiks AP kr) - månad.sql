Report: BU009b - Topp 10 artiklar (Butiks AP kr) - m�nad
Job: 124613
Report Cache Used: No

Number of Columns Returned:		12
Number of Temp Tables:		4

Total Number of Passes:		15
Number of SQL Passes:		15
Number of Analytical Passes:		0

Tables Accessed:
ARTICLE_D
ARTICLE_HIERARCHY_LVL_2_D
ARTICLE_HIERARCHY_LVL_8_D
CALENDAR_DAY_D
CONCEPT_D
MEASURING_UNIT_D
SALES_TRANSACTION_LINE_F
SCAN_CODE_D
STORE_D


SQL Statements:

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=BU009b - Topp 10 artiklar (Butiks AP kr) - m�nad; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD00 
select	a15.Calendar_Month_Id  Calendar_Month_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	(sum(a11.GP_Unit_Selling_Price_Amt) - sum(a11.Unit_Cost_Amt))  WJXBFS1,
	(sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end)) - sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end)))  WJXBFS2
from	PRSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	PRSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
	join	PRSemCMNVOUT.ARTICLE_D	a16
	  on 	(a14.Article_Seq_Num = a16.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Month_Id in (201304))
group by	a15.Calendar_Month_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

create volatile table ZZAM01, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZAM01 
select	pa12.Concept_Cd  Concept_Cd,
	pa12.Article_Seq_Num  Article_Seq_Num,
	pa12.Calendar_Month_Id  Calendar_Month_Id,
	rank () over(partition by pa12.Concept_Cd order by ((ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.WJXBFS2)) - ZEROIFNULL(pa12.WJXBFS1)) desc)  WJXBFS1
from	ZZMD00	pa12

create volatile table ZZMQ02, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Month_Id	INTEGER)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZMQ02 
select	pa11.Concept_Cd  Concept_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	pa11.Calendar_Month_Id  Calendar_Month_Id
from	ZZAM01	pa11
where	(pa11.WJXBFS1 <=  10.0)

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	AntalBtkMedFsg	INTEGER, 
	ForsBelExBVBer	FLOAT, 
	WJXBFS1	FLOAT, 
	InkopsBelEx	FLOAT, 
	ForsBelExBVBerButKampanj	FLOAT, 
	WJXBFS2	FLOAT, 
	InkopsBelExButKampanj	FLOAT)
primary index (Calendar_Month_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD03 
select	a15.Calendar_Month_Id  Calendar_Month_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	(ZEROIFNULL(sum(a11.GP_Unit_Selling_Price_Amt)) - ZEROIFNULL(sum(a11.Unit_Cost_Amt)))  WJXBFS1,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBerButKampanj,
	(ZEROIFNULL(sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))) - ZEROIFNULL(sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end))))  WJXBFS2,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end))  InkopsBelExButKampanj
from	PRSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	PRSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
	join	ZZMQ02	pa16
	  on 	(a12.Concept_Cd = pa16.Concept_Cd and 
	a14.Article_Seq_Num = pa16.Article_Seq_Num and 
	a15.Calendar_Month_Id = pa16.Calendar_Month_Id)
	join	PRSemCMNVOUT.ARTICLE_D	a17
	  on 	(a14.Article_Seq_Num = a17.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Month_Id in (201304))
group by	a15.Calendar_Month_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

select	pa13.Concept_Cd  Concept_Cd,
	max(a14.Concept_Name)  Concept_Name,
	pa13.Article_Seq_Num  Article_Seq_Num,
	max(a15.Article_Id)  Article_Id,
	max(a15.Article_Desc)  Article_Desc,
	pa13.Calendar_Month_Id  Calendar_Month_Id,
	max(pa13.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa13.ForsBelExBVBerButKampanj)  ForsBelExBVBerButKampanj,
	max(pa13.ForsBelExBVBer)  ForsBelExBVBer,
	rank () over(partition by pa13.Concept_Cd order by max(((ZEROIFNULL(pa13.WJXBFS1) - ZEROIFNULL(pa13.WJXBFS2)) - ZEROIFNULL(pa13.WJXBFS1))) desc)  WJXBFS1,
	max(pa13.InkopsBelExButKampanj)  InkopsBelExButKampanj,
	max(pa13.InkopsBelEx)  InkopsBelEx
from	ZZMD03	pa13
	join	PRSemCMNVOUT.CONCEPT_D	a14
	  on 	(pa13.Concept_Cd = a14.Concept_Cd)
	join	PRSemCMNVOUT.ARTICLE_D	a15
	  on 	(pa13.Article_Seq_Num = a15.Article_Seq_Num)
group by	pa13.Concept_Cd,
	pa13.Article_Seq_Num,
	pa13.Calendar_Month_Id


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZAM01

drop table ZZMQ02

drop table ZZMD03


[Analytical engine calculation steps:
	1.  Calculate metric: <AP butikskampanj kr> in the dataset
	2.  Calculate metric: <Bruttovinst %> in the dataset
	3.  Calculate metric limit(s)
	4.  Perform dynamic aggregation over <M�nad>
	5.  Calculate metric: <AP butikskampanj kr> at original data level in the view
	6.  Calculate metric: <Bruttovinst %> at original data level in the view
	7.  Calculate subtotal: <Totalt #1> 
	8.  Calculate subtotal: <Totalt> 
	9.  Calculate metric: <AP butikskampanj kr> at subtotal levels in the view
	10.  Calculate metric: <Bruttovinst %> at subtotal levels in the view
	11.  Perform cross-tabbing
]
