SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=16 - MA204 - Ekonomisk uppföljning på kontoaktiviteter per butik; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	BonusgrBel	FLOAT)
primary index (Store_Type_Cd, Calendar_Month_Id, Loyalty_Member_Type, Store_Id) on commit preserve rows

;insert into ZZMD00 
select	a12.Store_Type_Cd  Store_Type_Cd,
	a15.Calendar_Month_Id  Calendar_Month_Id,
	a13.Loyalty_Member_Type  Loyalty_Member_Type,
	a12.Store_Id  Store_Id,
	sum(a11.Bonus_Applicable_Amt)  BonusgrBel
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Processing_Dt = a15.Calendar_Dt)
where	(a14.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Concept_Cd in ('HEM')
 and a15.Calendar_Month_Id in (201306)
 and a11.Loyalty_Trans_Sub_Type_Cd in ('Purchase'))
group by	a12.Store_Type_Cd,
	a15.Calendar_Month_Id,
	a13.Loyalty_Member_Type,
	a12.Store_Id

create volatile table ZZMD01, no fallback, no log(
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Membership_Num) on commit preserve rows

;insert into ZZMD01 
select	a13.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num <> -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a14.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Concept_Cd in ('HEM')
 and a11.Calendar_Month_Id in (201306))
group by	a13.Membership_Num

create volatile table ZZMD02, no fallback, no log(
	AccrualAmtButik	FLOAT) on commit preserve rows

;insert into ZZMD02 
select	sum(pa11.WJXBFS1)  AccrualAmtButik
from	ZZMD01	pa11

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Membership_Num) on commit preserve rows

;insert into ZZMD03 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a12.Loyalty_Member_Type  Loyalty_Member_Type,
	a12.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num = -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a11.Calendar_Month_Id in (201306))
group by	a11.Calendar_Month_Id,
	a12.Loyalty_Member_Type,
	a12.Membership_Num

create volatile table ZZMD04, no fallback, no log(
	AccrualAmtEjButik	FLOAT) on commit preserve rows

;insert into ZZMD04 
select	sum(pa11.WJXBFS1)  AccrualAmtEjButik
from	ZZMD03	pa11
where	pa11.Calendar_Month_Id in (201306)

create volatile table ZZMD05, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Store_Type_Cd, Calendar_Month_Id, Loyalty_Member_Type, Store_Id) on commit preserve rows

;insert into ZZMD05 
select	a12.Store_Type_Cd  Store_Type_Cd,
	a11.Calendar_Month_Id  Calendar_Month_Id,
	a13.Loyalty_Member_Type  Loyalty_Member_Type,
	a12.Store_Id  Store_Id,
	sum(a11.Accrual_Amt)  AccrualAmt,
	sum(a11.Purchase_Amt)  PurchaseAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a14.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Concept_Cd in ('HEM')
 and a11.Calendar_Month_Id in (201306))
group by	a12.Store_Type_Cd,
	a11.Calendar_Month_Id,
	a13.Loyalty_Member_Type,
	a12.Store_Id

create volatile table ZZMD06, no fallback, no log(
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZMD06 
select	sum(a11.Downgraded_Amt)  DowngradedAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a11.Calendar_Month_Id in (201306))

select	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd)  Store_Type_Cd,
	max(a18.Store_Type_Desc)  Store_Type_Desc,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	max(a16.Store_Name)  Store_Name,
	coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type)  Loyalty_Member_Type,
	max(a17.Loyalty_Member_Type_Desc)  Loyalty_Member_Type_Desc,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa11.BonusgrBel)  BonusgrBel,
	max(pa13.AccrualAmtButik)  AccrualAmtButik,
	max(pa14.AccrualAmtEjButik)  AccrualAmtEjButik,
	ZEROIFNULL(((max(pa12.AccrualAmt) / NULLIFZERO(max(pa13.AccrualAmtButik))) * max(pa15.DowngradedAmt)))  NedgraderingBonusButik,
	max(pa12.PurchaseAmt)  PurchaseAmt,
	max(pa12.AccrualAmt)  AccrualAmt
from	ZZMD00	pa11
	full outer join	ZZMD05	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Loyalty_Member_Type = pa12.Loyalty_Member_Type and 
	pa11.Store_Id = pa12.Store_Id and 
	pa11.Store_Type_Cd = pa12.Store_Type_Cd)
	cross join	ZZMD02	pa13
	cross join	ZZMD04	pa14
	cross join	ZZMD06	pa15
	join	ITSemCMNVOUT.STORE_D	a16
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id) = a16.Store_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TYPE_D	a17
	  on 	(coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type) = a17.Loyalty_Member_Type)
	join	ITSemCMNVOUT.STORE_TYPE_D	a18
	  on 	(coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd) = a18.Store_Type_Cd)
group by	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id),
	coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03

drop table ZZMD04

drop table ZZMD05

drop table ZZMD06

