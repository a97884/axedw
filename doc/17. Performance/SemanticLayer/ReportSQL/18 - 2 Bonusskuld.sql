SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=18 - MA201 - Bonusskuld; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Store_Type_Cd, Calendar_Month_Id, Loyalty_Member_Type) on commit preserve rows

;insert into ZZMD00 
select	a14.Store_Type_Cd  Store_Type_Cd,
	a11.Calendar_Month_Id  Calendar_Month_Id,
	a12.Loyalty_Member_Type  Loyalty_Member_Type,
	sum(a11.Accrual_Amt)  AccrualAmt,
	sum(a11.Purchase_Amt)  PurchaseAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a14.Store_Type_Cd not in ('-1  ')
 and a11.Calendar_Month_Id in (201306))
group by	a14.Store_Type_Cd,
	a11.Calendar_Month_Id,
	a12.Loyalty_Member_Type

create volatile table ZZMD01, no fallback, no log(
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Membership_Num) on commit preserve rows

;insert into ZZMD01 
select	a12.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num <> -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a14.Store_Type_Cd not in ('-1  ')
 and a11.Calendar_Month_Id in (201306))
group by	a12.Membership_Num

create volatile table ZZMD02, no fallback, no log(
	AccrualAmtButik	FLOAT) on commit preserve rows

;insert into ZZMD02 
select	sum(pa11.WJXBFS1)  AccrualAmtButik
from	ZZMD01	pa11

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Membership_Num) on commit preserve rows

;insert into ZZMD03 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a12.Loyalty_Member_Type  Loyalty_Member_Type,
	a12.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num = -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a11.Calendar_Month_Id in (201306))
group by	a11.Calendar_Month_Id,
	a12.Loyalty_Member_Type,
	a12.Membership_Num

create volatile table ZZMD04, no fallback, no log(
	AccrualAmtEjButik	FLOAT) on commit preserve rows

;insert into ZZMD04 
select	sum(pa11.WJXBFS1)  AccrualAmtEjButik
from	ZZMD03	pa11
where	pa11.Calendar_Month_Id in (201306)

create volatile table ZZSP05, no fallback, no log(
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZSP05 
select	sum(a11.Downgraded_Amt)  DowngradedAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a11.Calendar_Month_Id in (201306))

create volatile table ZZSP06, no fallback, no log(
	Forfallen_bonus	FLOAT) on commit preserve rows

;insert into ZZSP06 
select	sum(a11.Expired_Amt)  Forfallen_bonus
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Expiration_Dt = a14.Calendar_Dt)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a14.Calendar_Month_Id in (201306))

create volatile table ZZMD07, no fallback, no log(
	DowngradedAmt	FLOAT, 
	Forfallen_bonus	FLOAT) on commit preserve rows

;insert into ZZMD07 
select	pa11.DowngradedAmt  DowngradedAmt,
	pa12.Forfallen_bonus  Forfallen_bonus
from	ZZSP05	pa11
	cross join	ZZSP06	pa12

create volatile table ZZMD08, no fallback, no log(
	Utbetalad_bonus	FLOAT) on commit preserve rows

;insert into ZZMD08 
select	sum(a11.Remittance_Amt)  Utbetalad_bonus
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_Next_Month_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a14.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Calendar_Month_Id in (201306))

select	pa11.Store_Type_Cd  Store_Type_Cd,
	a17.Store_Type_Desc  Store_Type_Desc,
	pa11.Loyalty_Member_Type  Loyalty_Member_Type,
	a16.Loyalty_Member_Type_Desc  Loyalty_Member_Type_Desc,
	pa11.Calendar_Month_Id  Calendar_Month_Id,
	((((ZEROIFNULL(((pa11.AccrualAmt / NULLIFZERO(pa12.AccrualAmtButik)) * (ZEROIFNULL(pa12.AccrualAmtButik) + ZEROIFNULL(pa13.AccrualAmtEjButik)))) - ZEROIFNULL(((pa11.AccrualAmt / NULLIFZERO(pa12.AccrualAmtButik)) * pa14.DowngradedAmt))) - ZEROIFNULL(pa11.PurchaseAmt)) - ZEROIFNULL(((pa11.AccrualAmt / NULLIFZERO(pa12.AccrualAmtButik)) * pa14.Forfallen_bonus))) - ZEROIFNULL(((pa11.AccrualAmt / NULLIFZERO(pa12.AccrualAmtButik)) * pa15.Utbetalad_bonus)))  WJXBFS1,
	pa12.AccrualAmtButik  AccrualAmtButik,
	pa14.Forfallen_bonus  Forfallen_bonus,
	pa13.AccrualAmtEjButik  AccrualAmtEjButik,
	ZEROIFNULL(((pa11.AccrualAmt / NULLIFZERO(pa12.AccrualAmtButik)) * pa14.DowngradedAmt))  NedgraderingBonusButik,
	pa11.PurchaseAmt  PurchaseAmt,
	pa11.AccrualAmt  AccrualAmt
from	ZZMD00	pa11
	cross join	ZZMD02	pa12
	cross join	ZZMD04	pa13
	cross join	ZZMD07	pa14
	cross join	ZZMD08	pa15
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TYPE_D	a16
	  on 	(pa11.Loyalty_Member_Type = a16.Loyalty_Member_Type)
	join	ITSemCMNVOUT.STORE_TYPE_D	a17
	  on 	(pa11.Store_Type_Cd = a17.Store_Type_Cd)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03

drop table ZZMD04

drop table ZZSP05

drop table ZZSP06

drop table ZZMD07

drop table ZZMD08
