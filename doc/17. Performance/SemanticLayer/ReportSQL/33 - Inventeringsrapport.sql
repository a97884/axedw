SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=BU400 - Rapportguide Inventering; ClientUser=Administrator;' For Session;


select	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	max(a17.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	sum(a11.Unit_Cost_Value_Amt)  LVInpris,
	sum(a11.Unit_Retail_Value_Amt)  LVUtpris
from	ITSemCMNVOUT.ARTICLE_INVENTORY_EVENT_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a11.Item_Inv_Dt in (DATE '2013-05-02')
 and a12.Store_Id in (4247))
group by	a17.Art_Hier_Lvl_2_Id


SET QUERY_BAND = NONE For Session;