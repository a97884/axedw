/* Lojalitetsrapport 1 m�nad, 201212 alla Willys koncept */

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=MA002a - Lojalitetsrapport m�nad 1; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntMedl	INTEGER, 
	AntKont	INTEGER, 
	WJXBFS1	INTEGER, 
	WJXBFS2	INTEGER, 
	WJXBFS3	INTEGER, 
	WJXBFS4	INTEGER, 
	WJXBFS5	INTEGER)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD00 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a13.Loyalty_Program_Id  Loyalty_Program_Id,
	(Case when max((Case when a11.Member_Ind > 0 then 1 else 0 end)) = 1 then count(distinct (Case when a11.Member_Ind > 0 then a11.Member_Account_Seq_Num else NULL end)) else NULL end)  AntMedl,
	count(distinct a11.Contact_Account_Seq_Num)  AntKont,
	(Case when max((Case when a11.New_Member_Ind > 0 then 1 else 0 end)) = 1 then count(distinct (Case when a11.New_Member_Ind > 0 then a11.Member_Account_Seq_Num else NULL end)) else NULL end)  WJXBFS1,
	(Case when max((Case when a11.Active_Member_Ind > 0 then 1 else 0 end)) = 1 then count(distinct (Case when a11.Active_Member_Ind > 0 then a11.Member_Account_Seq_Num else NULL end)) else NULL end)  WJXBFS2,
	(Case when max((Case when (a11.Active_Member_Ind = 0 and a11.Member_Ind > 0) then 1 else 0 end)) = 1 then count(distinct (Case when (a11.Active_Member_Ind = 0 and a11.Member_Ind > 0) then a11.Member_Account_Seq_Num else NULL end)) else NULL end)  WJXBFS3,
	(Case when max((Case when a11.Lost_Member_Ind > 0 then 1 else 0 end)) = 1 then count(distinct (Case when a11.Lost_Member_Ind > 0 then a11.Member_Account_Seq_Num else NULL end)) else NULL end)  WJXBFS4,
	(Case when max((Case when a11.ReActived_Member_Ind > 0 then 1 else 0 end)) = 1 then count(distinct (Case when a11.ReActived_Member_Ind > 0 then a11.Member_Account_Seq_Num else NULL end)) else NULL end)  WJXBFS5
from	PRSemCMNVOUT.LOYALTY_MEMBER_MONTH_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Home_Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a11.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
where	(a11.Calendar_Month_Id in (201212)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a11.Calendar_Month_Id,
	a13.Loyalty_Program_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntAktiveradMedl	INTEGER)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD01 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a14.Loyalty_Program_Id  Loyalty_Program_Id,
	count(distinct a11.Member_Account_Seq_Num)  AntAktiveradMedl
from	PRSemCMNVOUT.LOY_MEMBER_ACTIVED_STATUS_F	a11
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Member_Enrollment_Dt = a12.Calendar_Dt)
	join	PRSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	PRSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a13.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a11.Activated_Member_Ind > 0
 and a12.Calendar_Month_Id in (201212))
group by	a12.Calendar_Month_Id,
	a14.Loyalty_Program_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntMedlMedKop	INTEGER)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD02 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a16.Loyalty_Program_Id  Loyalty_Program_Id,
	count(distinct a12.Member_Account_Seq_Num)  AntMedlMedKop
from	PRSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	PRSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a12
	  on 	(a11.Contact_Account_Seq_Num = a12.Contact_Account_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a12.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	PRSemCMNVOUT.STORE_D	a15
	  on 	(a14.Home_Store_Seq_Num = a15.Store_Seq_Num)
	join	PRSemCMNVOUT.LOYALTY_PROGRAM_D	a16
	  on 	(a14.Loyalty_Program_Seq_Num = a16.Loyalty_Program_Seq_Num)
where	(a13.Calendar_Month_Id in (201212)
 and a15.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id,
	a16.Loyalty_Program_Id

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntKvMedl	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD03 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a15.Loyalty_Program_Id  Loyalty_Program_Id,
	sum(a11.Receipt_Cnt)  AntKvMedl
from	PRSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	PRSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a13
	  on 	(a11.Contact_Account_Seq_Num = a13.Contact_Account_Seq_Num)
	join	PRSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a13.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	PRSemCMNVOUT.LOYALTY_PROGRAM_D	a15
	  on 	(a14.Loyalty_Program_Seq_Num = a15.Loyalty_Program_Seq_Num)
where	(a11.Contact_Account_Seq_Num <> -1
 and a12.Calendar_Month_Id in (201212))
group by	a12.Calendar_Month_Id,
	a15.Loyalty_Program_Id

select	coalesce(pa17.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id, pa110.Loyalty_Program_Id)  Loyalty_Program_Id,
	max(a111.Loyalty_Program_Name)  Loyalty_Program_Name,
	coalesce(pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa17.AntMedl)  AntMedl,
	max(pa17.AntKont)  AntKont,
	max(pa17.WJXBFS1)  AntNyaMedl,
	max(pa17.WJXBFS2)  AntAktivMedl,
	max(pa17.WJXBFS3)  AntPassivMedl,
	max(pa17.WJXBFS4)  AntForlMedl,
	max(pa17.WJXBFS5)  AntAteraktMedl,
	max(pa18.AntAktiveradMedl)  AntAktiveradMedl,
	max(pa19.AntMedlMedKop)  AntMedlMedKop,
	max(pa110.AntKvMedl)  AntKvMedl
from	ZZMD00	pa17
	full outer join	ZZMD01	pa18
	  on 	(pa17.Calendar_Month_Id = pa18.Calendar_Month_Id and 
	pa17.Loyalty_Program_Id = pa18.Loyalty_Program_Id)
	full outer join	ZZMD02	pa19
	  on 	(coalesce(pa17.Calendar_Month_Id, pa18.Calendar_Month_Id) = pa19.Calendar_Month_Id and 
	coalesce(pa17.Loyalty_Program_Id, pa18.Loyalty_Program_Id) = pa19.Loyalty_Program_Id)
	full outer join	ZZMD03	pa110
	  on 	(coalesce(pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id) = pa110.Calendar_Month_Id and 
	coalesce(pa17.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id) = pa110.Loyalty_Program_Id)
	join	PRSemCMNVOUT.LOYALTY_PROGRAM_D	a111
	  on 	(coalesce(pa17.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id, pa110.Loyalty_Program_Id) = a111.Loyalty_Program_Id)
group by	coalesce(pa17.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id, pa110.Loyalty_Program_Id),
	coalesce(pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03

