SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU009c - Topp 10 artiklar (Svinn kr) - månad; ClientUser=Administrator;' For Session;


create volatile table ZZAM00, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZAM00 
select	a12.Concept_Cd  Concept_Cd,
	a14.Article_Seq_Num  Article_Seq_Num,
	a15.Calendar_Month_Id  Calendar_Month_Id,
	rank () over(partition by a12.Concept_Cd order by sum(a11.Total_Line_Value_Amt) desc)  WJXBFS1
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Adjustment_Dt = a15.Calendar_Dt)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a14.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a11.Known_Loss_Adj_Reason_Cd in ('SV')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Month_Id in (201304))
group by	a12.Concept_Cd,
	a14.Article_Seq_Num,
	a15.Calendar_Month_Id

create volatile table ZZMQ01, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Month_Id	INTEGER)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZMQ01 
select	pa11.Concept_Cd  Concept_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	pa11.Calendar_Month_Id  Calendar_Month_Id
from	ZZAM00	pa11
where	(pa11.WJXBFS1 <=  10.0)

create volatile table ZZSP02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	SvinnKr	FLOAT, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZSP02 
select	a15.Calendar_Month_Id  Calendar_Month_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	sum(a11.Total_Line_Value_Amt)  SvinnKr,
	rank () over(partition by a12.Concept_Cd order by sum(a11.Total_Line_Value_Amt) desc)  WJXBFS1
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Adjustment_Dt = a15.Calendar_Dt)
	join	ZZMQ01	pa16
	  on 	(a12.Concept_Cd = pa16.Concept_Cd and 
	a14.Article_Seq_Num = pa16.Article_Seq_Num and 
	a15.Calendar_Month_Id = pa16.Calendar_Month_Id)
	join	UV2SemCMNVOUT.ARTICLE_D	a17
	  on 	(a14.Article_Seq_Num = a17.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a11.Known_Loss_Adj_Reason_Cd in ('SV')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Month_Id in (201304))
group by	a15.Calendar_Month_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

create volatile table ZZSP03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	AntalBtkMedFsg	INTEGER)
primary index (Calendar_Month_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZSP03 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_MONTH_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ZZMQ01	pa15
	  on 	(a11.Calendar_Month_Id = pa15.Calendar_Month_Id and 
	a12.Concept_Cd = pa15.Concept_Cd and 
	a14.Article_Seq_Num = pa15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a14.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a11.Calendar_Month_Id in (201304))
group by	a11.Calendar_Month_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

create volatile table ZZMD04, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	SvinnKr	FLOAT, 
	WJXBFS1	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Month_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD04 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num)  Article_Seq_Num,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	pa11.SvinnKr  SvinnKr,
	pa11.WJXBFS1  WJXBFS1,
	pa12.ForsBelExBVBer  ForsBelExBVBer,
	pa12.InkopsBelEx  InkopsBelEx
from	ZZSP02	pa11
	full outer join	ZZSP03	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd)

select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	max(a13.Concept_Name)  Concept_Name,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num)  Article_Seq_Num,
	max(a14.Article_Id)  Article_Id,
	max(a14.Article_Desc)  Article_Desc,
	max(pa11.SvinnKr)  SvinnKr,
	max(pa12.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa11.WJXBFS1)  WJXBFS1,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx
from	ZZMD04	pa11
	full outer join	ZZSP03	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd)
	join	UV2SemCMNVOUT.CONCEPT_D	a13
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = a13.Concept_Cd)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = a14.Article_Seq_Num)
group by	coalesce(pa11.Concept_Cd, pa12.Concept_Cd),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id),
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num)


SET QUERY_BAND = NONE For Session;


drop table ZZAM00

drop table ZZMQ01

drop table ZZSP02

drop table ZZSP03

drop table ZZMD04

