SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=BU004 - Huvudgrupp daglig rapport; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Calendar_Dt	DATE, 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Art_Hier_Lvl_2_Id, Calendar_Dt, Store_Id) on commit preserve rows

;insert into ZZMD00 
select	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a11.Tran_Dt  Calendar_Dt,
	a12.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a12.Concept_Cd in ('PRX')
 and (a11.Tran_Dt in (DATE '2013-05-02')
 or a11.Tran_Dt in (DATE '2012-05-03'))
 and a17.Art_Hier_Lvl_2_Id in ('-1', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '25', '27'))
group by	a17.Art_Hier_Lvl_2_Id,
	a11.Tran_Dt,
	a12.Store_Id

select	pa12.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	max(a14.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	pa12.Store_Id  Store_Id,
	max(a13.Store_Name)  Store_Name,
	pa12.Calendar_Dt  Calendar_Dt,
	max(pa12.ForsBelEx)  ForsBelEx,
	max(pa12.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa12.InkopsBelEx)  InkopsBelEx,
	max(pa12.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa12.KampForsBelExBVBer)  KampForsBelExBVBer
from	ZZMD00	pa12
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(pa12.Store_Id = a13.Store_Id)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a14
	  on 	(pa12.Art_Hier_Lvl_2_Id = a14.Art_Hier_Lvl_2_Id)
group by	pa12.Art_Hier_Lvl_2_Id,
	pa12.Store_Id,
	pa12.Calendar_Dt


SET QUERY_BAND = NONE For Session;


drop table ZZMD00
