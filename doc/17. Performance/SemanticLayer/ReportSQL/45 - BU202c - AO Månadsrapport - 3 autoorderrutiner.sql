Report: BU202c - AO Månadsrapport - 3 autoorderrutiner
Job: 182630
Report Cache Used: No

Number of Columns Returned:		12
Number of Temp Tables:		8

Total Number of Passes:		27
Number of SQL Passes:		27
Number of Analytical Passes:		0

Tables Accessed:
AUTO_REPL_CNTRL_PARAM_F
CALENDAR_DAY_D
CALENDAR_MONTH_D
ORDER_CONTROL_F
PERPETUAL_INVENTORY_F
STOCK_BALANCE_REQUEST_CNTRL_F
STORE_D


SQL Statements:

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=BU202c - AO Månadsrapport - 3 autoorderrutiner; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntArtIAoSort	INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD00 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	count(distinct a11.Article_Seq_Num)  AntArtIAoSort
from	PRSemCMNVOUT.PERPETUAL_INVENTORY_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Perpetual_Inv_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_D	a14
	  on 	(a13.Calendar_Month_Id = a14.Calendar_Month_Id)
where	(a11.AO_Ind in (1)
 and a14.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01')
group by	a13.Calendar_Month_Id,
	a12.Store_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	GenomfSK	FLOAT, 
	SpontanSK	FLOAT, 
	RiktadSK	FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD01 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	sum(a11.Performed_Cntrl_Cnt)  GenomfSK,
	sum(a11.Spontaneous_Cntrl_Cnt)  SpontanSK,
	sum(a11.Requested_Cntrl_Cnt)  RiktadSK
from	PRSemCMNVOUT.STOCK_BALANCE_REQUEST_CNTRL_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Stock_Balance_Request_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_D	a14
	  on 	(a13.Calendar_Month_Id = a14.Calendar_Month_Id)
where	(a11.AO_Ind in (1)
 and a14.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01')
group by	a13.Calendar_Month_Id,
	a12.Store_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD02 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a11.Month_Nbr_Of_Days  WJXBFS1
from	PRSemCMNVOUT.CALENDAR_MONTH_D	a11
where	a11.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01'

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntalOkadeORUtanAtgard	FLOAT, 
	AntalMinskadeORUtanAtgard	FLOAT, 
	GODWFLAG4_1	INTEGER, 
	AntalOR	FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	sum((Case when a11.Fixed_Order_Ind = 0 then a11.Increased_Cnt else NULL end))  AntalOkadeORUtanAtgard,
	sum((Case when a11.Fixed_Order_Ind = 0 then a11.Decreased_Cnt else NULL end))  AntalMinskadeORUtanAtgard,
	max((Case when a11.Fixed_Order_Ind = 0 then 1 else 0 end))  GODWFLAG4_1,
	sum(a11.Inv_Request_Cnt)  AntalOR
from	PRSemCMNVOUT.ORDER_CONTROL_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Order_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_D	a14
	  on 	(a13.Calendar_Month_Id = a14.Calendar_Month_Id)
where	a14.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01'
group by	a13.Calendar_Month_Id,
	a12.Store_Id

create volatile table ZZOP04, no fallback, no log(
	Store_Id	INTEGER, 
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Store_Id, Calendar_Month_Id) on commit preserve rows

;insert into ZZOP04 
select	pa01.Store_Id  Store_Id,
	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntalOkadeORUtanAtgard  WJXBFS1,
	pa01.AntalMinskadeORUtanAtgard  WJXBFS2
from	ZZMD03	pa01
where	pa01.GODWFLAG4_1 = 1

create volatile table ZZMD05, no fallback, no log(
	Calendar_Dt	DATE, 
	Store_Id	INTEGER, 
	AntalAndrParamDag	INTEGER)
primary index (Calendar_Dt, Store_Id) on commit preserve rows

;insert into ZZMD05 
select	a11.Change_Dt  Calendar_Dt,
	a12.Store_Id  Store_Id,
	count(distinct a11.Article_Seq_Num)  AntalAndrParamDag
from	PRSemCMNVOUT.AUTO_REPL_CNTRL_PARAM_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Change_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_D	a14
	  on 	(a13.Calendar_Month_Id = a14.Calendar_Month_Id)
where	(a11.Auto_Repl_Control_Type_Cd in ('MPL', 'TD ')
 and a11.AO_Ind in (1)
 and a14.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01')
group by	a11.Change_Dt,
	a12.Store_Id

create volatile table ZZSP06, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntalAndrParam	FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZSP06 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Store_Id  Store_Id,
	sum(pa11.AntalAndrParamDag)  AntalAndrParam
from	ZZMD05	pa11
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(pa11.Calendar_Dt = a12.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_D	a13
	  on 	(a12.Calendar_Month_Id = a13.Calendar_Month_Id)
where	a13.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01'
group by	a12.Calendar_Month_Id,
	pa11.Store_Id

create volatile table ZZMD07, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntalAndrParam	FLOAT, 
	AntalOR	FLOAT)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD07 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.AntalAndrParam  AntalAndrParam,
	pa12.AntalOR  AntalOR
from	ZZSP06	pa11
	full outer join	ZZMD03	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Store_Id = pa12.Store_Id)

select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id)  Store_Id,
	max(a16.Store_Name)  Store_Name,
	max(pa11.AntArtIAoSort)  AntArtIAoSort,
	max(pa12.GenomfSK)  GenomfSK,
	max(pa15.WJXBFS1)  WJXBFS1,
	max(pa13.WJXBFS1)  AntalOkadeORUtanAtgard,
	max(pa12.SpontanSK)  SpontanSK,
	max(pa14.AntalAndrParam)  AntalAndrParam,
	max(pa14.AntalOR)  AntalOR,
	max(pa12.RiktadSK)  RiktadSK,
	max(pa13.WJXBFS2)  AntalMinskadeORUtanAtgard
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZOP04	pa13
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	full outer join	ZZMD07	pa14
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id) = pa14.Calendar_Month_Id and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
	left outer join	ZZMD02	pa15
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa15.Calendar_Month_Id)
	join	PRSemCMNVOUT.STORE_D	a16
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id) = a16.Store_Id)
group by	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03

drop table ZZOP04

drop table ZZMD05

drop table ZZSP06

drop table ZZMD07

