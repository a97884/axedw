SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW)-Prestandaverifiering; Action=EKON05a - Veckorapport butik region; ClientUser=A15914;' For Session;



create volatile table TQSZFLE98MQ000, no fallback, no log(
    Calendar_Week_Id    INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into TQSZFLE98MQ000 
select    a11.Calendar_Week_Id Calendar_Week_Id
from    PRSemCMNVOUT.CALENDAR_DAY_D    a11
where    a11.Calendar_Dt = DATE '2013-11-01'
group by    a11.Calendar_Week_Id

create volatile table T1BHYMA18MD001, no fallback, no log(
    Store_Seq_Num    INTEGER, 
    ForsBelEx    FLOAT, 
    ForsBelExBVBer    FLOAT, 
    InkopsBelEx    FLOAT, 
    KampInkopsBelEx    FLOAT, 
    KampForsBelExBVBer    FLOAT)
primary index (Store_Seq_Num) on commit preserve rows

;insert into T1BHYMA18MD001 
select    a11.Store_Seq_Num Store_Seq_Num,
    sum(a11.Unit_Selling_Price_Amt) ForsBelEx,
    sum(a11.GP_Unit_Selling_Price_Amt) ForsBelExBVBer,
    sum(a11.Unit_Cost_Amt) InkopsBelEx,
    sum((Case when a11.Campaign_Sales_Type_Cd in ('C ', 'L ') then a11.Unit_Cost_Amt else NULL end)) KampInkopsBelEx,
    sum((Case when a11.Campaign_Sales_Type_Cd in ('C ', 'L ') then a11.GP_Unit_Selling_Price_Amt else NULL end)) KampForsBelExBVBer
from    PRSemCMNVOUT.SALES_TRAN_LINE_WEEK_F    a11
    join    TQSZFLE98MQ000    pa12
     on     (a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
    join    PRSemCMNVOUT.SCAN_CODE_D    a13
     on     (a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D    a14
     on     (a13.Art_Hier_Lvl_8_Seq_Num = a14.Art_Hier_Lvl_8_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D    a15
     on     (a13.Art_Hier_Lvl_2_Seq_Num = a15.Art_Hier_Lvl_2_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D    a16
     on     (a11.Contact_Account_Seq_Num = a16.Contact_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D    a17
     on     (a16.Member_Account_Seq_Num = a17.Member_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_PROGRAM_D    a18
     on     (a17.Loyalty_Program_Seq_Num = a18.Loyalty_Program_Seq_Num)
    join    PRSemCMNVOUT.STORE_D    a19
     on     (a11.Store_Seq_Num = a19.Store_Seq_Num)
    join    PRSemCMNVOUT.CONCEPT_D    a110
     on     (a19.Concept_Cd = a110.Concept_Cd)
where    (a19.Concept_Cd in ('HEA', 'HEM', 'PRX')
and a15.Art_Hier_Lvl_2_Id in ('-1', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '25', '26', '27', '31', '32', '33', '90', '91', '92', '93', '94', '95', '96', '97', '98')
and (a110.Concept_Top_Cd in ('HE')
or a18.Loyalty_Program_Id in ('1-MQ7V')
or a110.Concept_Top_Cd in ('PX')))
group by    a11.Store_Seq_Num

create volatile table TZ7QIPSQKMD002, no fallback, no log(
    Store_Seq_Num    INTEGER, 
    ForsBelExFg    FLOAT, 
    InkopsBelExFg    FLOAT, 
    ForsBelExBVBerFg    FLOAT, 
    WJXBFS1    FLOAT, 
    WJXBFS2    FLOAT)
primary index (Store_Seq_Num) on commit preserve rows

;insert into TZ7QIPSQKMD002 
select    a11.Store_Seq_Num Store_Seq_Num,
    sum(a11.Unit_Selling_Price_Amt) ForsBelExFg,
    sum(a11.Unit_Cost_Amt) InkopsBelExFg,
    sum(a11.GP_Unit_Selling_Price_Amt) ForsBelExBVBerFg,
    sum((Case when a11.Campaign_Sales_Type_Cd in ('C ', 'L ') then a11.GP_Unit_Selling_Price_Amt else NULL end)) WJXBFS1,
    sum((Case when a11.Campaign_Sales_Type_Cd in ('C ', 'L ') then a11.Unit_Cost_Amt else NULL end)) WJXBFS2
from    PRSemCMNVOUT.SALES_TRAN_LINE_WEEK_F    a11
    join    PRSemCMNVOUT.CALENDAR_WEEK_D    a12
     on     (a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
    join    TQSZFLE98MQ000    pa13
     on     (a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
    join    PRSemCMNVOUT.SCAN_CODE_D    a14
     on     (a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D    a15
     on     (a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D    a16
     on     (a14.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D    a17
     on     (a11.Contact_Account_Seq_Num = a17.Contact_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D    a18
     on     (a17.Member_Account_Seq_Num = a18.Member_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_PROGRAM_D    a19
     on     (a18.Loyalty_Program_Seq_Num = a19.Loyalty_Program_Seq_Num)
    join    PRSemCMNVOUT.STORE_D    a110
     on     (a11.Store_Seq_Num = a110.Store_Seq_Num)
    join    PRSemCMNVOUT.CONCEPT_D    a111
     on     (a110.Concept_Cd = a111.Concept_Cd)
where    (a110.Concept_Cd in ('HEA', 'HEM', 'PRX')
and a16.Art_Hier_Lvl_2_Id in ('-1', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '25', '26', '27', '31', '32', '33', '90', '91', '92', '93', '94', '95', '96', '97', '98')
and (a111.Concept_Top_Cd in ('HE')
or a19.Loyalty_Program_Id in ('1-MQ7V')
or a111.Concept_Top_Cd in ('PX')))
group by    a11.Store_Seq_Num

create volatile table TKKYJX73WMD003, no fallback, no log(
    Store_Seq_Num    INTEGER, 
    ForsBelExAckFg    FLOAT)
primary index (Store_Seq_Num) on commit preserve rows

;insert into TKKYJX73WMD003 
select    a11.Store_Seq_Num Store_Seq_Num,
    sum(a11.Unit_Selling_Price_Amt) ForsBelExAckFg
from    PRSemCMNVOUT.SALES_TRAN_LINE_WEEK_F    a11
    join    PRSemCMNVOUT.CALENDAR_WEEK_YTD_D    a12
     on     (a11.Calendar_Week_Id = a12.Calendar_Week_YTD_Id)
    join    PRSemCMNVOUT.CALENDAR_WEEK_D    a13
     on     (a12.Calendar_Week_Id = a13.Calendar_PYS_Week_Id)
    join    TQSZFLE98MQ000    pa14
     on     (a13.Calendar_Week_Id = pa14.Calendar_Week_Id)
    join    PRSemCMNVOUT.SCAN_CODE_D    a15
     on     (a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D    a16
     on     (a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D    a17
     on     (a15.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D    a18
     on     (a11.Contact_Account_Seq_Num = a18.Contact_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D    a19
     on     (a18.Member_Account_Seq_Num = a19.Member_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_PROGRAM_D    a110
     on     (a19.Loyalty_Program_Seq_Num = a110.Loyalty_Program_Seq_Num)
    join    PRSemCMNVOUT.STORE_D    a111
     on     (a11.Store_Seq_Num = a111.Store_Seq_Num)
    join    PRSemCMNVOUT.CONCEPT_D    a112
     on     (a111.Concept_Cd = a112.Concept_Cd)
where    (a111.Concept_Cd in ('HEA', 'HEM', 'PRX')
and a17.Art_Hier_Lvl_2_Id in ('-1', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '25', '26', '27', '31', '32', '33', '90', '91', '92', '93', '94', '95', '96', '97', '98')
and (a112.Concept_Top_Cd in ('HE')
or a110.Loyalty_Program_Id in ('1-MQ7V')
or a112.Concept_Top_Cd in ('PX')))
group by    a11.Store_Seq_Num

create volatile table ZZMD04, no fallback, no log(
    Store_Seq_Num    INTEGER, 
    ForsBelExAck    FLOAT)
primary index (Store_Seq_Num) on commit preserve rows

;insert into ZZMD04 
select    a11.Store_Seq_Num Store_Seq_Num,
    sum(a11.Unit_Selling_Price_Amt) ForsBelExAck
from    PRSemCMNVOUT.SALES_TRAN_LINE_WEEK_F    a11
    join    PRSemCMNVOUT.CALENDAR_WEEK_YTD_D    a12
     on     (a11.Calendar_Week_Id = a12.Calendar_Week_YTD_Id)
    join    ZZMQ00    pa13
     on     (a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
    join    PRSemCMNVOUT.SCAN_CODE_D    a14
     on     (a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D    a15
     on     (a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
    join    PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D    a16
     on     (a14.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D    a17
     on     (a11.Contact_Account_Seq_Num = a17.Contact_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D    a18
     on     (a17.Member_Account_Seq_Num = a18.Member_Account_Seq_Num)
    join    PRSemCMNVOUT.LOYALTY_PROGRAM_D    a19
     on     (a18.Loyalty_Program_Seq_Num = a19.Loyalty_Program_Seq_Num)
    join    PRSemCMNVOUT.STORE_D    a110
     on     (a11.Store_Seq_Num = a110.Store_Seq_Num)
    join    PRSemCMNVOUT.CONCEPT_D    a111
     on     (a110.Concept_Cd = a111.Concept_Cd)
where    (a110.Concept_Cd in ('HEA', 'HEM', 'PRX')
and a16.Art_Hier_Lvl_2_Id in ('-1', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '25', '26', '27', '31', '32', '33', '90', '91', '92', '93', '94', '95', '96', '97', '98')
and (a111.Concept_Top_Cd in ('HE')
or a19.Loyalty_Program_Id in ('1-MQ7V')
or a111.Concept_Top_Cd in ('PX')))
group by    a11.Store_Seq_Num

select    a17.Store_Type_Cd Store_Type_Cd,
    a19.Store_Type_Desc Store_Type_Desc,
    a17.Retail_Region_Cd Retail_Region_Cd,
    a18.Retail_Region_Name Retail_Region_Name,
    coalesce(pa11.Store_Seq_Num, pa12.Store_Seq_Num, pa14.Store_Seq_Num, pa16.Store_Seq_Num) Store_Seq_Num,
    a17.Store_Id Store_Id,
    a17.Store_Name Store_Name,
    pa11.ForsBelEx ForsBelEx,
    pa12.ForsBelExFg ForsBelExFg,
    pa12.WJXBFS1 WJXBFS1,
    pa14.ForsBelExAckFg ForsBelExAckFg,
    pa11.ForsBelExBVBer ForsBelExBVBer,
    pa11.InkopsBelEx InkopsBelEx,
    pa11.KampInkopsBelEx KampInkopsBelEx,
    pa12.WJXBFS2 WJXBFS2,
    pa11.KampForsBelExBVBer KampForsBelExBVBer,
    pa12.InkopsBelExFg InkopsBelExFg,
    pa12.ForsBelExBVBerFg ForsBelExBVBerFg,
    pa16.ForsBelExAck ForsBelExAck
from    ZZMD01    pa11
    full outer join    ZZMD02    pa12
     on     (pa11.Store_Seq_Num = pa12.Store_Seq_Num)
    full outer join    ZZMD03    pa14
     on     (coalesce(pa11.Store_Seq_Num, pa12.Store_Seq_Num) = pa14.Store_Seq_Num)
    full outer join    ZZMD04    pa16
     on     (coalesce(pa11.Store_Seq_Num, pa12.Store_Seq_Num, pa14.Store_Seq_Num) = pa16.Store_Seq_Num)
    join    PRSemCMNVOUT.STORE_D    a17
     on     (coalesce(pa11.Store_Seq_Num, pa12.Store_Seq_Num, pa14.Store_Seq_Num, pa16.Store_Seq_Num) = a17.Store_Seq_Num)
    join    PRSemCMNVOUT.RETAIL_REGION_D    a18
     on     (a17.Retail_Region_Cd = a18.Retail_Region_Cd)
    join    PRSemCMNVOUT.STORE_TYPE_D    a19
     on     (a17.Store_Type_Cd = a19.Store_Type_Cd)



SET QUERY_BAND = NONE For Session;

