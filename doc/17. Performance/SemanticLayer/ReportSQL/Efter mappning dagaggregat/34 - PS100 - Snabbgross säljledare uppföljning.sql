SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=PS100 - Snabbgross säljledare uppföljning; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-14'
group by	a11.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	CustCol_3	VARCHAR(50), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	ForsBelEx	FLOAT, 
	AntalSalda	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Art_Hier_Lvl_4_Id, CustCol_3, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD01 
select	a19.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	case 
when (a19.Art_Hier_Lvl_2_Id in ('05','06','07') or a19.Art_Hier_Lvl_4_Id in ('120107','120108','121025','121026')) then 'Peter R' 
when (a19.Art_Hier_Lvl_2_Id in ('10','15','16','17','18','19','20','22','24','25','26','28','31') 
  and a19.Art_Hier_Lvl_4_Id not in ('101120')) then 'Oskar' 
when a19.Art_Hier_Lvl_2_Id in ('11','13','14','21','23','27') then 'Stefan J' 
when ((a19.Art_Hier_Lvl_2_Id in ('01','02','04','08','12') and a19.Art_Hier_Lvl_4_Id not in ('120107','120108','121025','121026'))
  or a19.Art_Hier_Lvl_4_Id in ('101120')) then 'My H' 
when (a19.Art_Hier_Lvl_2_Id in ('03','09')) then 'Anna-Karin A' 
when a19.Art_Hier_Lvl_2_Id in ('-1') then 'Odefinierat' 
else 'Övrigt' end  CustCol_3,
	a19.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a19
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num and 
	a16.Art_Hier_Lvl_4_Seq_Num = a19.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a18.Concept_Cd in ('SNG')
 and case 
when (a19.Art_Hier_Lvl_2_Id in ('05','06','07') or a19.Art_Hier_Lvl_4_Id in ('120107','120108','121025','121026')) then 'Peter R' 
when (a19.Art_Hier_Lvl_2_Id in ('10','15','16','17','18','19','20','22','24','25','26','28','31') 
  and a19.Art_Hier_Lvl_4_Id not in ('101120')) then 'Oskar' 
when a19.Art_Hier_Lvl_2_Id in ('11','13','14','21','23','27') then 'Stefan J' 
when ((a19.Art_Hier_Lvl_2_Id in ('01','02','04','08','12') and a19.Art_Hier_Lvl_4_Id not in ('120107','120108','121025','121026'))
  or a19.Art_Hier_Lvl_4_Id in ('101120')) then 'My H' 
when (a19.Art_Hier_Lvl_2_Id in ('03','09')) then 'Anna-Karin A' 
when a19.Art_Hier_Lvl_2_Id in ('-1') then 'Odefinierat' 
else 'Övrigt' end in ('Peter R'))
group by	a19.Art_Hier_Lvl_4_Id,
	case 
when (a19.Art_Hier_Lvl_2_Id in ('05','06','07') or a19.Art_Hier_Lvl_4_Id in ('120107','120108','121025','121026')) then 'Peter R' 
when (a19.Art_Hier_Lvl_2_Id in ('10','15','16','17','18','19','20','22','24','25','26','28','31') 
  and a19.Art_Hier_Lvl_4_Id not in ('101120')) then 'Oskar' 
when a19.Art_Hier_Lvl_2_Id in ('11','13','14','21','23','27') then 'Stefan J' 
when ((a19.Art_Hier_Lvl_2_Id in ('01','02','04','08','12') and a19.Art_Hier_Lvl_4_Id not in ('120107','120108','121025','121026'))
  or a19.Art_Hier_Lvl_4_Id in ('101120')) then 'My H' 
when (a19.Art_Hier_Lvl_2_Id in ('03','09')) then 'Anna-Karin A' 
when a19.Art_Hier_Lvl_2_Id in ('-1') then 'Odefinierat' 
else 'Övrigt' end,
	a19.Art_Hier_Lvl_2_Id

create volatile table ZZMD02, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	CustCol_3	VARCHAR(50), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	ForsBelExFg	FLOAT, 
	AntalSaldaFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Art_Hier_Lvl_4_Id, CustCol_3, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD02 
select	a110.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	case 
when (a110.Art_Hier_Lvl_2_Id in ('05','06','07') or a110.Art_Hier_Lvl_4_Id in ('120107','120108','121025','121026')) then 'Peter R' 
when (a110.Art_Hier_Lvl_2_Id in ('10','15','16','17','18','19','20','22','24','25','26','28','31') 
  and a110.Art_Hier_Lvl_4_Id not in ('101120')) then 'Oskar' 
when a110.Art_Hier_Lvl_2_Id in ('11','13','14','21','23','27') then 'Stefan J' 
when ((a110.Art_Hier_Lvl_2_Id in ('01','02','04','08','12') and a110.Art_Hier_Lvl_4_Id not in ('120107','120108','121025','121026'))
  or a110.Art_Hier_Lvl_4_Id in ('101120')) then 'My H' 
when (a110.Art_Hier_Lvl_2_Id in ('03','09')) then 'Anna-Karin A' 
when a110.Art_Hier_Lvl_2_Id in ('-1') then 'Odefinierat' 
else 'Övrigt' end  CustCol_3,
	a110.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  WJXBFS2
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a110
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a110.Art_Hier_Lvl_2_Seq_Num and 
	a17.Art_Hier_Lvl_4_Seq_Num = a110.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a19
	  on 	(a11.Store_Seq_Num = a19.Store_Seq_Num)
where	(a19.Concept_Cd in ('SNG')
 and case 
when (a110.Art_Hier_Lvl_2_Id in ('05','06','07') or a110.Art_Hier_Lvl_4_Id in ('120107','120108','121025','121026')) then 'Peter R' 
when (a110.Art_Hier_Lvl_2_Id in ('10','15','16','17','18','19','20','22','24','25','26','28','31') 
  and a110.Art_Hier_Lvl_4_Id not in ('101120')) then 'Oskar' 
when a110.Art_Hier_Lvl_2_Id in ('11','13','14','21','23','27') then 'Stefan J' 
when ((a110.Art_Hier_Lvl_2_Id in ('01','02','04','08','12') and a110.Art_Hier_Lvl_4_Id not in ('120107','120108','121025','121026'))
  or a110.Art_Hier_Lvl_4_Id in ('101120')) then 'My H' 
when (a110.Art_Hier_Lvl_2_Id in ('03','09')) then 'Anna-Karin A' 
when a110.Art_Hier_Lvl_2_Id in ('-1') then 'Odefinierat' 
else 'Övrigt' end in ('Peter R'))
group by	a110.Art_Hier_Lvl_4_Id,
	case 
when (a110.Art_Hier_Lvl_2_Id in ('05','06','07') or a110.Art_Hier_Lvl_4_Id in ('120107','120108','121025','121026')) then 'Peter R' 
when (a110.Art_Hier_Lvl_2_Id in ('10','15','16','17','18','19','20','22','24','25','26','28','31') 
  and a110.Art_Hier_Lvl_4_Id not in ('101120')) then 'Oskar' 
when a110.Art_Hier_Lvl_2_Id in ('11','13','14','21','23','27') then 'Stefan J' 
when ((a110.Art_Hier_Lvl_2_Id in ('01','02','04','08','12') and a110.Art_Hier_Lvl_4_Id not in ('120107','120108','121025','121026'))
  or a110.Art_Hier_Lvl_4_Id in ('101120')) then 'My H' 
when (a110.Art_Hier_Lvl_2_Id in ('03','09')) then 'Anna-Karin A' 
when a110.Art_Hier_Lvl_2_Id in ('-1') then 'Odefinierat' 
else 'Övrigt' end,
	a110.Art_Hier_Lvl_2_Id

select	coalesce(pa11.CustCol_3, pa12.CustCol_3)  CustCol_3,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a15.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id)  Art_Hier_Lvl_4_Id,
	max(a16.Art_Hier_Lvl_4_Desc)  Art_Hier_Lvl_4_Desc,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa12.ForsBelExFg)  ForsBelExFg,
	max(pa11.AntalSalda)  AntalSalda,
	max(pa12.WJXBFS1)  WJXBFS1,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa11.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa12.WJXBFS2)  WJXBFS2,
	max(pa12.AntalSaldaFg)  AntalSaldaFg,
	max(pa11.KampForsBelExBVBer)  KampForsBelExBVBer,
	max(pa12.InkopsBelExFg)  InkopsBelExFg,
	max(pa12.ForsBelExBVBerFg)  ForsBelExBVBerFg
from	ZZMD01	pa11
	full outer join	ZZMD02	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Art_Hier_Lvl_4_Id = pa12.Art_Hier_Lvl_4_Id and 
	pa11.CustCol_3 = pa12.CustCol_3)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a15
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = a15.Art_Hier_Lvl_2_Id)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num and 
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id) = a16.Art_Hier_Lvl_4_Id and 
	coalesce(pa11.CustCol_3, pa12.CustCol_3) = case 
when (a16.Art_Hier_Lvl_2_Id in ('05','06','07') or a16.Art_Hier_Lvl_4_Id in ('120107','120108','121025','121026')) then 'Peter R' 
when (a16.Art_Hier_Lvl_2_Id in ('10','15','16','17','18','19','20','22','24','25','26','28','31') 
  and a16.Art_Hier_Lvl_4_Id not in ('101120')) then 'Oskar' 
when a16.Art_Hier_Lvl_2_Id in ('11','13','14','21','23','27') then 'Stefan J' 
when ((a16.Art_Hier_Lvl_2_Id in ('01','02','04','08','12') and a16.Art_Hier_Lvl_4_Id not in ('120107','120108','121025','121026'))
  or a16.Art_Hier_Lvl_4_Id in ('101120')) then 'My H' 
when (a16.Art_Hier_Lvl_2_Id in ('03','09')) then 'Anna-Karin A' 
when a16.Art_Hier_Lvl_2_Id in ('-1') then 'Odefinierat' 
else 'Övrigt' end)
group by	coalesce(pa11.CustCol_3, pa12.CustCol_3),
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id),
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id)


SET QUERY_BAND = NONE For Session;
