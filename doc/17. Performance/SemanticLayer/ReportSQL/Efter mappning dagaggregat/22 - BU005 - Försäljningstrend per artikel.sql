SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU005 - Försäljningstrend per artikel; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt between DATE '2012-06-27' and DATE '2013-08-14'
group by	a11.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelInkl	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	AntalBtkMedFsg	INTEGER, 
	KampAntalSalda	FLOAT, 
	KampForsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Store_Type_Cd, Calendar_Week_Id, Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD01 
select	a13.Store_Type_Cd  Store_Type_Cd,
	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((a11.Unit_Selling_Price_Amt + a11.Tax_Amt))  ForsBelInkl,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Item_Qty else NULL end))  KampAntalSalda,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Selling_Price_Amt else NULL end))  KampForsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
where	a16.Article_Id = '100309024'
group by	a13.Store_Type_Cd,
	a11.Calendar_Week_Id,
	a13.Concept_Cd,
	a11.Scan_Code_Seq_Num

select	pa13.Calendar_Week_Id  Calendar_Week_Id,
	a15.Article_Seq_Num  Article_Seq_Num,
	max(a18.Article_Id)  Article_Id,
	max(a18.Article_Desc)  Article_Desc,
	a14.Measuring_Unit_Seq_Num  Measuring_Unit_Seq_Num,
	max(a15.MU_Id)  MU_Id,
	max(a15.MU_Desc)  MU_Desc,
	pa13.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	max(a14.Scan_Cd)  Scan_Cd,
	pa13.Concept_Cd  Concept_Cd,
	max(a16.Concept_Name)  Concept_Name,
	pa13.Store_Type_Cd  Store_Type_Cd,
	max(a17.Store_Type_Desc)  Store_Type_Desc,
	max(pa13.AntalSalda)  AntalSalda,
	max(pa13.ForsBelEx)  ForsBelEx,
	max(pa13.KampAntalSalda)  KampAntalSalda,
	max(pa13.KampForsBelEx)  KampForsBelEx,
	max(pa13.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa13.ForsBelInkl)  ForsBelInkl,
	max(pa13.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa13.InkopsBelEx)  InkopsBelEx,
	max(pa13.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa13.KampForsBelExBVBer)  KampForsBelExBVBer
from	ZZMD01	pa13
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(pa13.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.CONCEPT_D	a16
	  on 	(pa13.Concept_Cd = a16.Concept_Cd)
	join	UV2SemCMNVOUT.STORE_TYPE_D	a17
	  on 	(pa13.Store_Type_Cd = a17.Store_Type_Cd)
	join	UV2SemCMNVOUT.ARTICLE_D	a18
	  on 	(a15.Article_Seq_Num = a18.Article_Seq_Num)
group by	pa13.Calendar_Week_Id,
	a15.Article_Seq_Num,
	a14.Measuring_Unit_Seq_Num,
	pa13.Scan_Code_Seq_Num,
	pa13.Concept_Cd,
	pa13.Store_Type_Cd


SET QUERY_BAND = NONE For Session;

