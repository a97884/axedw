SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU014 - Brödrapport; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	GODWFLAG1_1	INTEGER, 
	TotForsExMomsAllProd	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP00 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum((Case when a17.Art_Hier_Lvl_2_Id in ('01') then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelEx,
	sum((Case when a17.Art_Hier_Lvl_2_Id in ('01') then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBer,
	sum((Case when a17.Art_Hier_Lvl_2_Id in ('01') then a11.Unit_Cost_Amt else NULL end))  InkopsBelEx,
	max((Case when a17.Art_Hier_Lvl_2_Id in ('01') then 1 else 0 end))  GODWFLAG1_1,
	sum((Case when a17.Art_Hier_Lvl_2_Id not in ('24', '28') then a11.Unit_Selling_Price_Amt else NULL end))  TotForsExMomsAllProd
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a12.Concept_Cd in ('WIL')
 and a11.Calendar_Week_Id in (201334)
 and (a17.Art_Hier_Lvl_2_Id in ('01')
 or a17.Art_Hier_Lvl_2_Id not in ('24', '28')))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZOP01, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Concept_Cd) on commit preserve rows

;insert into ZZOP01 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Concept_Cd  Concept_Cd,
	pa01.ForsBelEx  WJXBFS1,
	pa01.ForsBelExBVBer  WJXBFS2,
	pa01.InkopsBelEx  WJXBFS3
from	ZZSP00	pa01
where	pa01.GODWFLAG1_1 = 1

create volatile table ZZSP02, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	SvinnKr	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP02 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Total_Line_Value_Amt)  SvinnKr
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a17.Art_Hier_Lvl_2_Id in ('01')
 and a12.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201334)
 and a11.Known_Loss_Adj_Reason_Cd in ('SV'))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD03, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	SvinnKr	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.WJXBFS1  ForsBelEx,
	pa12.SvinnKr  SvinnKr,
	pa11.WJXBFS2  ForsBelExBVBer,
	pa11.WJXBFS3  InkopsBelEx
from	ZZOP01	pa11
	full outer join	ZZSP02	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZSP04, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelExBakeoff	FLOAT, 
	InkopsBelExBakeoff	FLOAT, 
	ForsBelExBVBerBakeoff	FLOAT, 
	GODWFLAG4_1	INTEGER, 
	ForsBelExLosbrod	FLOAT, 
	InkopsBelExLosbrod	FLOAT, 
	ForsBelExBVBerLosbrod	FLOAT, 
	GODWFLAG7_1	INTEGER)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP04 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010516', '010417') then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExBakeoff,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010516', '010417') then a11.Unit_Cost_Amt else NULL end))  InkopsBelExBakeoff,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010516', '010417') then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBerBakeoff,
	max((Case when a17.Art_Hier_Lvl_4_Id in ('010516', '010417') then 1 else 0 end))  GODWFLAG4_1,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010418', '010519') then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExLosbrod,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010418', '010519') then a11.Unit_Cost_Amt else NULL end))  InkopsBelExLosbrod,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010418', '010519') then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBerLosbrod,
	max((Case when a17.Art_Hier_Lvl_4_Id in ('010418', '010519') then 1 else 0 end))  GODWFLAG7_1
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a17
	  on 	(a16.Art_Hier_Lvl_4_Seq_Num = a17.Art_Hier_Lvl_4_Seq_Num)
where	(a12.Concept_Cd in ('WIL')
 and a11.Calendar_Week_Id in (201334)
 and (a17.Art_Hier_Lvl_4_Id in ('010516', '010417')
 or a17.Art_Hier_Lvl_4_Id in ('010418', '010519')))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZOP05, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Concept_Cd) on commit preserve rows

;insert into ZZOP05 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Concept_Cd  Concept_Cd,
	pa01.ForsBelExBakeoff  WJXBFS1,
	pa01.InkopsBelExBakeoff  WJXBFS2,
	pa01.ForsBelExBVBerBakeoff  WJXBFS3
from	ZZSP04	pa01
where	pa01.GODWFLAG4_1 = 1

create volatile table ZZSP06, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	SvinnKrBakeoff	FLOAT, 
	GODWFLAG5_1	INTEGER, 
	SvinnKrLosbrod	FLOAT, 
	GODWFLAG8_1	INTEGER)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP06 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010516', '010417') then a11.Total_Line_Value_Amt else NULL end))  SvinnKrBakeoff,
	max((Case when a17.Art_Hier_Lvl_4_Id in ('010516', '010417') then 1 else 0 end))  GODWFLAG5_1,
	sum((Case when a17.Art_Hier_Lvl_4_Id in ('010418', '010519') then a11.Total_Line_Value_Amt else NULL end))  SvinnKrLosbrod,
	max((Case when a17.Art_Hier_Lvl_4_Id in ('010418', '010519') then 1 else 0 end))  GODWFLAG8_1
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a17
	  on 	(a16.Art_Hier_Lvl_4_Seq_Num = a17.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a12.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201334)
 and a11.Known_Loss_Adj_Reason_Cd in ('SV')
 and (a17.Art_Hier_Lvl_4_Id in ('010516', '010417')
 or a17.Art_Hier_Lvl_4_Id in ('010418', '010519')))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZOP07, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Concept_Cd) on commit preserve rows

;insert into ZZOP07 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Concept_Cd  Concept_Cd,
	pa01.SvinnKrBakeoff  WJXBFS1
from	ZZSP06	pa01
where	pa01.GODWFLAG5_1 = 1

create volatile table ZZMD08, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelExBakeoff	FLOAT, 
	SvinnKrBakeoff	FLOAT, 
	InkopsBelExBakeoff	FLOAT, 
	ForsBelExBVBerBakeoff	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD08 
select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.WJXBFS1  ForsBelExBakeoff,
	pa12.WJXBFS1  SvinnKrBakeoff,
	pa11.WJXBFS2  InkopsBelExBakeoff,
	pa11.WJXBFS3  ForsBelExBVBerBakeoff
from	ZZOP05	pa11
	full outer join	ZZOP07	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZOP09, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Concept_Cd) on commit preserve rows

;insert into ZZOP09 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Concept_Cd  Concept_Cd,
	pa01.ForsBelExLosbrod  WJXBFS1,
	pa01.InkopsBelExLosbrod  WJXBFS2,
	pa01.ForsBelExBVBerLosbrod  WJXBFS3
from	ZZSP04	pa01
where	pa01.GODWFLAG7_1 = 1

create volatile table ZZOP0A, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Concept_Cd) on commit preserve rows

;insert into ZZOP0A 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Concept_Cd  Concept_Cd,
	pa01.SvinnKrLosbrod  WJXBFS1
from	ZZSP06	pa01
where	pa01.GODWFLAG8_1 = 1

create volatile table ZZMD0B, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelExLosbrod	FLOAT, 
	SvinnKrLosbrod	FLOAT, 
	InkopsBelExLosbrod	FLOAT, 
	ForsBelExBVBerLosbrod	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD0B 
select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.WJXBFS1  ForsBelExLosbrod,
	pa12.WJXBFS1  SvinnKrLosbrod,
	pa11.WJXBFS2  InkopsBelExLosbrod,
	pa11.WJXBFS3  ForsBelExBVBerLosbrod
from	ZZOP09	pa11
	full outer join	ZZOP0A	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

select	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd)  Retail_Region_Cd,
	max(a18.Retail_Region_Name)  Retail_Region_Name,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd)  Concept_Cd,
	max(a19.Concept_Name)  Concept_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id)  Store_Id,
	max(a17.Store_Name)  Store_Name,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa11.SvinnKr)  SvinnKr,
	max(pa12.ForsBelExBakeoff)  ForsBelExBakeoff,
	max(pa12.SvinnKrBakeoff)  SvinnKrBakeoff,
	max(pa13.ForsBelExLosbrod)  ForsBelExLosbrod,
	max(pa13.SvinnKrLosbrod)  SvinnKrLosbrod,
	(ZEROIFNULL(max(pa12.ForsBelExBakeoff)) + ZEROIFNULL(max(pa13.ForsBelExLosbrod)))  WJXBFS1,
	max(pa14.TotForsExMomsAllProd)  TotForsExMomsAllProd,
	max(pa14.ForsBelEx)  ForsBelExBrod,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa13.InkopsBelExLosbrod)  InkopsBelExLosbrod,
	max(pa12.InkopsBelExBakeoff)  InkopsBelExBakeoff,
	max(pa12.ForsBelExBVBerBakeoff)  ForsBelExBVBerBakeoff,
	max(pa13.ForsBelExBVBerLosbrod)  ForsBelExBVBerLosbrod
from	ZZMD03	pa11
	full outer join	ZZMD08	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZMD0B	pa13
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa13.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = pa13.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	full outer join	ZZSP00	pa14
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd) = pa14.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd) = pa14.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd) = a17.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd) = a17.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id) = a17.Store_Id)
	join	UV2SemCMNVOUT.RETAIL_REGION_D	a18
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd) = a18.Retail_Region_Cd)
	join	UV2SemCMNVOUT.CONCEPT_D	a19
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd) = a19.Concept_Cd)
group by	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id)


SET QUERY_BAND = NONE For Session;


