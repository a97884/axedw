SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU013 - Huvudgrupp/Varugrupp analys; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZSP00 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201334)
 and a17.Concept_Cd in ('WIL')
 and a18.Art_Hier_Lvl_2_Id in ('17')
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a11.Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id,
	a18.Art_Hier_Lvl_2_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	SvinnKr	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZSP01 
select	a19.Calendar_Week_Id  Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Total_Line_Value_Amt)  SvinnKr
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a19
	  on 	(a11.Adjustment_Dt = a19.Calendar_Dt)
where	(a19.Calendar_Week_Id in (201334)
 and a17.Concept_Cd in ('WIL')
 and a18.Art_Hier_Lvl_2_Id in ('17')
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a19.Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id,
	a18.Art_Hier_Lvl_2_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	SvinnKr	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD02 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id)  Art_Hier_Lvl_4_Id,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	pa11.AntalSalda  AntalSalda,
	pa11.ForsBelEx  ForsBelEx,
	pa11.ForsBelExBVBer  ForsBelExBVBer,
	pa12.SvinnKr  SvinnKr,
	pa11.InkopsBelEx  InkopsBelEx
from	ZZSP00	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Art_Hier_Lvl_4_Id = pa12.Art_Hier_Lvl_4_Id and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)

create volatile table ZZSP03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	TotForsExMomsAllProd	FLOAT, 
	TotForsBelExBVBerAllaProd	FLOAT, 
	TotInkopsbelExAllaProd	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id) on commit preserve rows

;insert into ZZSP03 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	sum(a11.Unit_Selling_Price_Amt)  TotForsExMomsAllProd,
	sum(a11.GP_Unit_Selling_Price_Amt)  TotForsBelExBVBerAllaProd,
	sum(a11.Unit_Cost_Amt)  TotInkopsbelExAllaProd
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201334)
 and a17.Concept_Cd in ('WIL')
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a11.Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id

create volatile table ZZSP04, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	TotSvinnKrAllaProd	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id) on commit preserve rows

;insert into ZZSP04 
select	a19.Calendar_Week_Id  Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	sum(a11.Total_Line_Value_Amt)  TotSvinnKrAllaProd
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a19
	  on 	(a11.Adjustment_Dt = a19.Calendar_Dt)
where	(a19.Calendar_Week_Id in (201334)
 and a17.Concept_Cd in ('WIL')
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a19.Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id

create volatile table ZZMD05, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	TotForsExMomsAllProd	FLOAT, 
	TotSvinnKrAllaProd	FLOAT, 
	TotForsBelExBVBerAllaProd	FLOAT, 
	TotInkopsbelExAllaProd	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id) on commit preserve rows

;insert into ZZMD05 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id)  Art_Hier_Lvl_4_Id,
	pa11.TotForsExMomsAllProd  TotForsExMomsAllProd,
	pa12.TotSvinnKrAllaProd  TotSvinnKrAllaProd,
	pa11.TotForsBelExBVBerAllaProd  TotForsBelExBVBerAllaProd,
	pa11.TotInkopsbelExAllaProd  TotInkopsbelExAllaProd
from	ZZSP03	pa11
	full outer join	ZZSP04	pa12
	  on 	(pa11.Art_Hier_Lvl_4_Id = pa12.Art_Hier_Lvl_4_Id and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)

create volatile table ZZMD06, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	ManuellRab	FLOAT, 
	AntalSaldaManRab	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD06 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Discount_Amt)  ManuellRab,
	sum(a11.Disc_Item_Qty)  AntalSaldaManRab
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201334)
 and a17.Concept_Cd in ('WIL')
 and a18.Art_Hier_Lvl_2_Id in ('17')
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a11.Discount_Type_Cd in ('MP'))
group by	a11.Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id,
	a18.Art_Hier_Lvl_2_Id

create volatile table ZZMD07, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	TotRabExMomsAllProd	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id) on commit preserve rows

;insert into ZZMD07 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	sum(a11.Discount_Amt)  TotRabExMomsAllProd
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201334)
 and a17.Concept_Cd in ('WIL')
 and a11.Discount_Type_Cd in ('MP')
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a11.Calendar_Week_Id,
	a18.Art_Hier_Lvl_4_Id

create volatile table ZZMD08, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSaldaFg	FLOAT, 
	ForsBelExFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_4_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD08 
select	a12.Calendar_Week_Id  Calendar_Week_Id,
	a19.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a19.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a19
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num and 
	a16.Art_Hier_Lvl_4_Seq_Num = a19.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a12.Calendar_Week_Id in (201334)
 and a18.Concept_Cd in ('WIL')
 and a19.Art_Hier_Lvl_2_Id in ('17')
 and a19.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a12.Calendar_Week_Id,
	a19.Art_Hier_Lvl_4_Id,
	a19.Art_Hier_Lvl_2_Id

select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a17.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id, pa13.Art_Hier_Lvl_4_Id, pa14.Art_Hier_Lvl_4_Id)  Art_Hier_Lvl_4_Id,
	max(a18.Art_Hier_Lvl_4_Desc)  Art_Hier_Lvl_4_Desc,
	max(pa11.AntalSalda)  AntalSalda,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa15.TotForsExMomsAllProd)  TotForsExMomsAllProd,
	max(pa12.ManuellRab)  ManuellRab,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa15.TotSvinnKrAllaProd)  TotSvinnKrAllaProd,
	max(pa11.SvinnKr)  SvinnKr,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa12.AntalSaldaManRab)  AntalSaldaManRab,
	max(pa16.TotRabExMomsAllProd)  TotRabExMomsAllProd,
	max(pa15.TotForsBelExBVBerAllaProd)  TotForsBelExBVBerAllaProd,
	max(pa13.ForsBelEx)  ForsBelExSvinn,
	max(pa14.AntalSaldaFg)  AntalSaldaFg,
	max(pa14.ForsBelExFg)  ForsBelExFg,
	max(pa14.InkopsBelExFg)  InkopsBelExFg,
	max(pa14.ForsBelExBVBerFg)  ForsBelExBVBerFg,
	max(pa15.TotInkopsbelExAllaProd)  TotInkopsbelExAllaProd
from	ZZMD02	pa11
	full outer join	ZZMD06	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Art_Hier_Lvl_4_Id = pa12.Art_Hier_Lvl_4_Id and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	full outer join	ZZSP00	pa13
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa13.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id) = pa13.Art_Hier_Lvl_4_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id)
	full outer join	ZZMD08	pa14
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id) = pa14.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id, pa13.Art_Hier_Lvl_4_Id) = pa14.Art_Hier_Lvl_4_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id)
	left outer join	ZZMD05	pa15
	  on 	(coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id, pa13.Art_Hier_Lvl_4_Id, pa14.Art_Hier_Lvl_4_Id) = pa15.Art_Hier_Lvl_4_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id) = pa15.Calendar_Week_Id)
	left outer join	ZZMD07	pa16
	  on 	(coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id, pa13.Art_Hier_Lvl_4_Id, pa14.Art_Hier_Lvl_4_Id) = pa16.Art_Hier_Lvl_4_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id) = pa16.Calendar_Week_Id)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id) = a17.Art_Hier_Lvl_2_Id)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id, pa13.Art_Hier_Lvl_4_Id, pa14.Art_Hier_Lvl_4_Id) = a18.Art_Hier_Lvl_4_Id)
group by	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id),
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id),
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id, pa13.Art_Hier_Lvl_4_Id, pa14.Art_Hier_Lvl_4_Id)


SET QUERY_BAND = NONE For Session;


