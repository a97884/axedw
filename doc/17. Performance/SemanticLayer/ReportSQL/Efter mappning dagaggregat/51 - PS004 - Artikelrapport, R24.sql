SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=PS004 - Artikelrapport, R24; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Scan_Code_Seq_Num	INTEGER, 
	AntalBtkMedFsgR24	INTEGER, 
	AntalSaldaR24	FLOAT, 
	ForsBelExR24	FLOAT, 
	ForsBelInklR24	FLOAT, 
	InkopsBelExR24	FLOAT, 
	AntalVkMedFsgR24v	INTEGER, 
	WJXBFS1	FLOAT, 
	KampAntalSaldaR24	FLOAT, 
	KampForsBelExR24	FLOAT, 
	KampForsBelInklR24	FLOAT, 
	KampInkopsBelExR24	FLOAT, 
	KampForsBelExBVBerR24	FLOAT)
primary index (Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD00 
select	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsgR24,
	sum(a11.Item_Qty)  AntalSaldaR24,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExR24,
	sum((a11.Unit_Selling_Price_Amt + a11.Tax_Amt))  ForsBelInklR24,
	sum(a11.Unit_Cost_Amt)  InkopsBelExR24,
	count(distinct a11.Calendar_Week_Id)  AntalVkMedFsgR24v,
	sum(a11.GP_Unit_Selling_Price_Amt)  WJXBFS1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Item_Qty else NULL end))  KampAntalSaldaR24,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Selling_Price_Amt else NULL end))  KampForsBelExR24,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then (a11.Unit_Selling_Price_Amt + a11.Tax_Amt) else NULL end))  KampForsBelInklR24,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelExR24,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBerR24
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_R24_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_Week_R24_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a12.Calendar_Week_Id in (201334)
 and a18.Art_Hier_Lvl_2_Id in ('01')
 and a13.Concept_Cd in ('WIL'))
group by	a11.Scan_Code_Seq_Num

select	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a18.Art_Hier_Lvl_2_Desc  Art_Hier_Lvl_2_Desc,
	a111.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a111.Art_Hier_Lvl_4_Desc  Art_Hier_Lvl_4_Desc,
	a110.Prod_Hier_Lvl1_Seq_Num  Prod_Hier_Lvl1_Seq_Num,
	a114.Prod_Hier_Lvl1_Id  Prod_Hier_Lvl1_Id,
	a114.Prod_Hier_Lvl1_Desc  Prod_Hier_Lvl1_Desc,
	a110.Prod_Hier_Lvl2_Seq_Num  Prod_Hier_Lvl2_Seq_Num,
	a113.Prod_Hier_Lvl2_Id  Prod_Hier_Lvl2_Id,
	a113.Prod_Hier_Lvl2_Desc  Prod_Hier_Lvl2_Desc,
	a16.Prod_Hier_Lvl3_Seq_Num  Prod_Hier_Lvl3_Seq_Num,
	a110.Prod_Hier_Lvl3_Id  Prod_Hier_Lvl3_Id,
	a110.Prod_Hier_Lvl3_Desc  Prod_Hier_Lvl3_Desc,
	pa13.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	a14.Scan_Cd  Scan_Cd,
	a15.Article_Seq_Num  Article_Seq_Num,
	a16.Article_Id  Article_Id,
	a16.Article_Desc  Article_Desc,
	a16.Article_Available_From_Dt  Article_Available_From_Dt,
	a16.Article_Business_Expiry_Dt  Article_Business_Expiry_Dt,
	a15.MU_Conversion_Factor  MU_Conversion_Factor,
	a15.MU_Conversion_To_UOM  MU_Conversion_To_UOM,
	a19.Art_Hier_Lvl_3_Id  Art_Hier_Lvl_3_Id,
	a19.Art_Hier_Lvl_3_Desc  Art_Hier_Lvl_3_Desc,
	a16.Vendor_Seq_Num  Vendor_Seq_Num,
	a112.Vendor_Id  Vendor_Id,
	a112.Vendor_Name  Vendor_Name,
	pa13.AntalBtkMedFsgR24  AntalBtkMedFsgR24,
	pa13.AntalSaldaR24  AntalSaldaR24,
	pa13.ForsBelExR24  ForsBelExR24,
	pa13.ForsBelInklR24  ForsBelInklR24,
	pa13.InkopsBelExR24  InkopsBelExR24,
	pa13.KampAntalSaldaR24  KampAntalSaldaR24,
	pa13.KampForsBelExR24  KampForsBelExR24,
	pa13.KampForsBelInklR24  KampForsBelInklR24,
	pa13.KampInkopsBelExR24  KampInkopsBelExR24,
	pa13.AntalVkMedFsgR24v  AntalVkMedFsgR24v,
	pa13.KampForsBelExBVBerR24  KampForsBelExBVBerR24,
	pa13.WJXBFS1  WJXBFS1
from	ZZMD00	pa13
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(pa13.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_3_D	a19
	  on 	(a17.Art_Hier_Lvl_3_Seq_Num = a19.Art_Hier_Lvl_3_Seq_Num)
	join	UV2SemCMNVOUT.PROD_HIER_LVL_3_D	a110
	  on 	(a16.Prod_Hier_Lvl3_Seq_Num = a110.Prod_Hier_Lvl3_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a111
	  on 	(a17.Art_Hier_Lvl_4_Seq_Num = a111.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.VENDOR_D	a112
	  on 	(a16.Vendor_Seq_Num = a112.Vendor_Seq_Num)
	join	UV2SemCMNVOUT.PROD_HIER_LVL_2_D	a113
	  on 	(a110.Prod_Hier_Lvl2_Seq_Num = a113.Prod_Hier_Lvl2_Seq_Num)
	join	UV2SemCMNVOUT.PROD_HIER_LVL_1_D	a114
	  on 	(a110.Prod_Hier_Lvl1_Seq_Num = a114.Prod_Hier_Lvl1_Seq_Num)


SET QUERY_BAND = NONE For Session;
