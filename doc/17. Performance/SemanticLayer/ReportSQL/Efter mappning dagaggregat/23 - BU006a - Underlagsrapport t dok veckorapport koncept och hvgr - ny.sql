SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU006a - Underlagsrapport t dok veckorapport/koncept och hvgr - ny; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-14'
group by	a11.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Store_Type_Cd, Calendar_Week_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD01 
select	a13.Store_Type_Cd  Store_Type_Cd,
	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Concept_Cd  Concept_Cd,
	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a13.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28', '31', '32', '33', '90', '91', '92', '93', '94', '95', '96', '97')
 and a13.Concept_Cd in ('HEM', 'PRX', 'WIL', 'WHE', 'WH2', 'SNG')
 and a18.Art_Hier_Lvl_2_Id not in ('98'))
group by	a13.Store_Type_Cd,
	a11.Calendar_Week_Id,
	a13.Concept_Cd,
	a18.Art_Hier_Lvl_2_Id,
	a13.Store_Id

create volatile table ZZMD02, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	ForsBelExFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Store_Type_Cd, Calendar_Week_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD02 
select	a14.Store_Type_Cd  Store_Type_Cd,
	a12.Calendar_Week_Id  Calendar_Week_Id,
	a14.Concept_Cd  Concept_Cd,
	a19.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a14.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  WJXBFS2
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a15
	  on 	(a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a16
	  on 	(a15.Measuring_Unit_Seq_Num = a16.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a17
	  on 	(a16.Article_Seq_Num = a17.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28', '31', '32', '33', '90', '91', '92', '93', '94', '95', '96', '97')
 and a14.Concept_Cd in ('HEM', 'PRX', 'WIL', 'WHE', 'WH2', 'SNG')
 and a19.Art_Hier_Lvl_2_Id not in ('98'))
group by	a14.Store_Type_Cd,
	a12.Calendar_Week_Id,
	a14.Concept_Cd,
	a19.Art_Hier_Lvl_2_Id,
	a14.Store_Id

select	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a16.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	max(a15.Store_Name)  Store_Name,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	max(a17.Concept_Name)  Concept_Name,
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd)  Store_Type_Cd,
	max(a18.Store_Type_Desc)  Store_Type_Desc,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa12.ForsBelExFg)  ForsBelExFg,
	max(pa12.WJXBFS1)  WJXBFS1,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa11.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa12.WJXBFS2)  WJXBFS2,
	max(pa11.KampForsBelExBVBer)  KampForsBelExBVBer,
	max(pa12.InkopsBelExFg)  InkopsBelExFg,
	max(pa12.ForsBelExBVBerFg)  ForsBelExBVBerFg
from	ZZMD01	pa11
	full outer join	ZZMD02	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Store_Id = pa12.Store_Id and 
	pa11.Store_Type_Cd = pa12.Store_Type_Cd)
	join	UV2SemCMNVOUT.STORE_D	a15
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id) = a15.Store_Id)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = a16.Art_Hier_Lvl_2_Id)
	join	UV2SemCMNVOUT.CONCEPT_D	a17
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = a17.Concept_Cd)
	join	UV2SemCMNVOUT.STORE_TYPE_D	a18
	  on 	(coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd) = a18.Store_Type_Cd)
group by	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id),
	coalesce(pa11.Store_Id, pa12.Store_Id),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd),
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id),
	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd)


SET QUERY_BAND = NONE For Session;
