SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU007 - Driftrapport med försäljnings-KPIer; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	GODWFLAG1_1	INTEGER, 
	ForsBelExFarskvaror	FLOAT, 
	ForsbelExSCO	FLOAT, 
	ForsbelExManuell	FLOAT, 
	ForsBelExHvg2428	FLOAT, 
	ForsBelExBVBerButKampanj	FLOAT, 
	InkopsBelExButKampanj	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP00 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum((Case when a17.Art_Hier_Lvl_2_Id not in ('24', '28') then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelEx,
	sum((Case when a17.Art_Hier_Lvl_2_Id not in ('24', '28') then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBer,
	sum((Case when a17.Art_Hier_Lvl_2_Id not in ('24', '28') then a11.Unit_Cost_Amt else NULL end))  InkopsBelEx,
	max((Case when a17.Art_Hier_Lvl_2_Id not in ('24', '28') then 1 else 0 end))  GODWFLAG1_1,
	sum((Case when a17.Art_Hier_Lvl_2_Id in ('01', '02', '03', '04', '05', '06', '07', '08', '09') then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExFarskvaror,
	sum((Case when (a17.Art_Hier_Lvl_2_Id not in ('24', '28') and a11.Receipt_Type_Cd in ('2')) then a11.Unit_Selling_Price_Amt else NULL end))  ForsbelExSCO,
	sum((Case when (a17.Art_Hier_Lvl_2_Id not in ('24', '28') and a11.Sales_Location_Cd in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '99')) then a11.Unit_Selling_Price_Amt else NULL end))  ForsbelExManuell,
	sum((Case when a17.Art_Hier_Lvl_2_Id in ('24', '28') then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExHvg2428,
	sum((Case when (a17.Art_Hier_Lvl_2_Id not in ('24', '28') and a11.Campaign_Sales_Type_Cd in ('L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBerButKampanj,
	sum((Case when (a17.Art_Hier_Lvl_2_Id not in ('24', '28') and a11.Campaign_Sales_Type_Cd in ('L  ')) then a11.Unit_Cost_Amt else NULL end))  InkopsBelExButKampanj,
	sum((Case when (a17.Art_Hier_Lvl_2_Id not in ('24', '28') and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when (a17.Art_Hier_Lvl_2_Id not in ('24', '28') and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a12.Concept_Cd in ('WIL')
 and a11.Calendar_Week_Id in (201334)
 and (a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 or a17.Art_Hier_Lvl_2_Id in ('01', '02', '03', '04', '05', '06', '07', '08', '09')
 or a17.Art_Hier_Lvl_2_Id in ('24', '28')))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZOP01, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Concept_Cd	CHAR(3), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Concept_Cd) on commit preserve rows

;insert into ZZOP01 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Concept_Cd  Concept_Cd,
	pa01.ForsBelEx  WJXBFS1,
	pa01.ForsBelExBVBer  WJXBFS2,
	pa01.InkopsBelEx  WJXBFS3
from	ZZSP00	pa01
where	pa01.GODWFLAG1_1 = 1

create volatile table ZZSP02, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	SvinnKr	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP02 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Total_Line_Value_Amt)  SvinnKr
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a11.Known_Loss_Adj_Reason_Cd in ('SV')
 and a12.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201334))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD03, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	SvinnKr	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.WJXBFS1  ForsBelEx,
	pa11.WJXBFS2  ForsBelExBVBer,
	pa12.SvinnKr  SvinnKr,
	pa11.WJXBFS3  InkopsBelEx
from	ZZOP01	pa11
	full outer join	ZZSP02	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD04, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsbelExPrestore	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD04 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsbelExPrestore
from	UV2SemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.STORE_DEPARTMENT_D	a13
	  on 	(a11.Store_Department_Seq_Num = a13.Store_Department_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a19
	  on 	(a11.Tran_Dt = a19.Calendar_Dt)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a19.Calendar_Week_Id in (201334)
 and a13.Store_Department_Id in ('__Prestore__'))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD05, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ManuellRab	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD05 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Discount_Amt)  ManuellRab
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a11.Calendar_Week_Id in (201334)
 and a11.Discount_Type_Cd in ('MP'))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD06, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelExEMV	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD06 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExEMV
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.PROD_HIER_LVL_3_D	a18
	  on 	(a15.Prod_Hier_Lvl3_Seq_Num = a18.Prod_Hier_Lvl3_Seq_Num)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a11.Calendar_Week_Id in (201334)
 and a18.Prod_Hier_Lvl2_Seq_Num in (5, 8, 19))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD07, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	TotInkopsbelExAllaProd	FLOAT, 
	TotForsBelExBVBerAllaProd	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD07 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Unit_Cost_Amt)  TotInkopsbelExAllaProd,
	sum(a11.GP_Unit_Selling_Price_Amt)  TotForsBelExBVBerAllaProd
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_1_D	a18
	  on 	(a16.Art_Hier_Lvl_1_Seq_Num = a18.Art_Hier_Lvl_1_Seq_Num)
where	(a12.Concept_Cd in ('WIL')
 and a11.Calendar_Week_Id in (201334)
 and a18.Art_Hier_Lvl_1_Id in ('GG')
 and a17.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

select	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa15.Retail_Region_Cd, pa17.Retail_Region_Cd, pa18.Retail_Region_Cd, pa111.Retail_Region_Cd)  Retail_Region_Cd,
	max(a114.Retail_Region_Name)  Retail_Region_Name,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa15.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd, pa111.Concept_Cd)  Concept_Cd,
	max(a115.Concept_Name)  Concept_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa15.Store_Id, pa17.Store_Id, pa18.Store_Id, pa111.Store_Id)  Store_Id,
	max(a113.Store_Name)  Store_Name,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa12.ForsBelExFarskvaror)  ForsBelExFarskvaror,
	max(pa12.ForsbelExSCO)  ForsbelExSCO,
	max(pa12.ForsbelExManuell)  ForsbelExManuell,
	max(pa15.ForsbelExPrestore)  ForsbelExPrestore,
	max(pa12.ForsBelExHvg2428)  ForsBelExHvg2428,
	max(pa17.ManuellRab)  ManuellRab,
	max(pa18.ForsBelExEMV)  ForsBelExEMV,
	max(pa11.SvinnKr)  SvinnKr,
	max(pa12.ForsBelExBVBer)  KampForsBelExBVBer,
	max(pa12.ForsBelExBVBerButKampanj)  ForsBelExBVBerButKampanj,
	max(pa111.TotInkopsbelExAllaProd)  TotInkopsbelExAllaProd,
	max(pa12.InkopsBelExButKampanj)  InkopsBelExButKampanj,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa12.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa12.InkopsBelEx)  KampInkopsBelEx1,
	max(pa12.KampForsBelExBVBer)  KampForsBelExBVBer1,
	max(pa111.TotForsBelExBVBerAllaProd)  TotForsBelExBVBerAllaProd
from	ZZMD03	pa11
	full outer join	ZZSP00	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZMD04	pa15
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa15.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = pa15.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa15.Store_Id)
	full outer join	ZZMD05	pa17
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa15.Concept_Cd) = pa17.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa15.Retail_Region_Cd) = pa17.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa15.Store_Id) = pa17.Store_Id)
	full outer join	ZZMD06	pa18
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa15.Concept_Cd, pa17.Concept_Cd) = pa18.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa15.Retail_Region_Cd, pa17.Retail_Region_Cd) = pa18.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa15.Store_Id, pa17.Store_Id) = pa18.Store_Id)
	full outer join	ZZMD07	pa111
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa15.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd) = pa111.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa15.Retail_Region_Cd, pa17.Retail_Region_Cd, pa18.Retail_Region_Cd) = pa111.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa15.Store_Id, pa17.Store_Id, pa18.Store_Id) = pa111.Store_Id)
	join	UV2SemCMNVOUT.STORE_D	a113
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa15.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd, pa111.Concept_Cd) = a113.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa15.Retail_Region_Cd, pa17.Retail_Region_Cd, pa18.Retail_Region_Cd, pa111.Retail_Region_Cd) = a113.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa15.Store_Id, pa17.Store_Id, pa18.Store_Id, pa111.Store_Id) = a113.Store_Id)
	join	UV2SemCMNVOUT.RETAIL_REGION_D	a114
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa15.Retail_Region_Cd, pa17.Retail_Region_Cd, pa18.Retail_Region_Cd, pa111.Retail_Region_Cd) = a114.Retail_Region_Cd)
	join	UV2SemCMNVOUT.CONCEPT_D	a115
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa15.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd, pa111.Concept_Cd) = a115.Concept_Cd)
group by	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa15.Retail_Region_Cd, pa17.Retail_Region_Cd, pa18.Retail_Region_Cd, pa111.Retail_Region_Cd),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa15.Concept_Cd, pa17.Concept_Cd, pa18.Concept_Cd, pa111.Concept_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa15.Store_Id, pa17.Store_Id, pa18.Store_Id, pa111.Store_Id)


SET QUERY_BAND = NONE For Session;

