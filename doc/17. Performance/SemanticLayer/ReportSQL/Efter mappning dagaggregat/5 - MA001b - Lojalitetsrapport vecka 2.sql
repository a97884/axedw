SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=MA001b - Lojalitetsrapport vecka 2; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntRekrKontaktAck	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD00 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontaktAck
from	UV2SemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	UV2SemCMNVOUT.CALENDAR_WEEK_YTD_D	a13
	  on 	(a12.Calendar_Week_Id = a13.Calendar_Week_YTD_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a13.Calendar_Week_Id in (201334)
 and a14.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Week_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntKvMedl	FLOAT, 
	GODWFLAG2_1	INTEGER, 
	AntKv	FLOAT, 
	AntKvEjMedl	FLOAT, 
	GODWFLAGb_1	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP01 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Receipt_Cnt else NULL end))  AntKvMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG2_1,
	sum(a11.Receipt_Cnt)  AntKv,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Receipt_Cnt else NULL end))  AntKvEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAGb_1
from	UV2SemCMNVOUT.SALES_TRANSACTION_WEEK_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a11.Calendar_Week_Id

create volatile table ZZOP02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP02 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntKvMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAG2_1 = 1

create volatile table ZZSP03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	ForsBelExMedl	FLOAT, 
	GODWFLAG3_1	INTEGER, 
	WJXBFS3	FLOAT, 
	WJXBFS4	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVEjMedl	FLOAT, 
	ForsBelExEjMedl	FLOAT, 
	InkBelExEjMedl	FLOAT, 
	GODWFLAGc_1	INTEGER, 
	KampInkBelExEjMedl	FLOAT, 
	KampForsExBVEjMedl	FLOAT, 
	GODWFLAG1a_1	INTEGER, 
	KampForsExBVMedl	FLOAT, 
	KampInkBelExMedl	FLOAT, 
	GODWFLAG1d_1	INTEGER, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT, 
	GODWFLAG24_1	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP03 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS1,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Cost_Amt else NULL end))  WJXBFS2,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG3_1,
	sum(a11.GP_Unit_Selling_Price_Amt)  WJXBFS3,
	sum(a11.Unit_Cost_Amt)  WJXBFS4,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAGc_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExEjMedl,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG1a_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG1d_1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer,
	max((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then 1 else 0 end))  GODWFLAG24_1
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a11.Calendar_Week_Id

create volatile table ZZOP04, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP04 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.WJXBFS1  WJXBFS1,
	pa01.WJXBFS2  WJXBFS2,
	pa01.ForsBelExMedl  WJXBFS3
from	ZZSP03	pa01
where	pa01.GODWFLAG3_1 = 1

create volatile table ZZSP05, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	TotMedlRbt	FLOAT, 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP05 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Tot_Loy_Discount_Amt)  TotMedlRbt,
	sum(a11.Tot_Discount_Amt)  WJXBFS1
from	UV2SemCMNVOUT.SALES_TRANSACTION_TOTALS_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Contact_Account_Seq_Num <> -1)
group by	a13.Calendar_Week_Id

create volatile table ZZMD06, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntKvMedl	FLOAT, 
	WJXBFS1	FLOAT, 
	TotMedlRbt	FLOAT, 
	TotRbtExMedlRbt	FLOAT, 
	ForsBelExMedl	FLOAT, 
	InkBelExMedl	FLOAT, 
	ForsBelExBVMedlem	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD06 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id)  Calendar_Week_Id,
	pa11.WJXBFS1  AntKvMedl,
	(ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.WJXBFS2))  WJXBFS1,
	pa13.TotMedlRbt  TotMedlRbt,
	(ZEROIFNULL(pa13.WJXBFS1) - ZEROIFNULL(pa13.TotMedlRbt))  TotRbtExMedlRbt,
	pa12.WJXBFS3  ForsBelExMedl,
	pa12.WJXBFS2  InkBelExMedl,
	pa12.WJXBFS1  ForsBelExBVMedlem
from	ZZOP02	pa11
	full outer join	ZZOP04	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	full outer join	ZZSP05	pa13
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id)

create volatile table ZZSP07, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntRekrKontakt	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP07 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontakt
from	UV2SemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Week_Id

create volatile table ZZSP08, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP08 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Accrual_Amt)  AccrualAmt,
	sum(a11.Purchase_Amt)  PurchaseAmt
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Processing_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Week_Id

create volatile table ZZMD09, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntRekrKontakt	FLOAT, 
	AntKv	FLOAT, 
	WJXBFS1	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD09 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id)  Calendar_Week_Id,
	pa11.AntRekrKontakt  AntRekrKontakt,
	pa12.AntKv  AntKv,
	(ZEROIFNULL(pa13.WJXBFS3) - ZEROIFNULL(pa13.WJXBFS4))  WJXBFS1,
	pa13.ForsBelEx  ForsBelEx,
	pa13.WJXBFS3  ForsBelExBVBer,
	pa13.WJXBFS4  InkopsBelEx,
	pa14.AccrualAmt  AccrualAmt,
	pa14.PurchaseAmt  PurchaseAmt
from	ZZSP07	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	full outer join	ZZSP03	pa13
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id)
	full outer join	ZZSP08	pa14
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id)

create volatile table ZZOP0A, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0A 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntKvEjMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAGb_1 = 1

create volatile table ZZOP0B, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0B 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.ForsBelExBVEjMedl  WJXBFS1,
	pa01.ForsBelExEjMedl  WJXBFS2,
	pa01.InkBelExEjMedl  WJXBFS3
from	ZZSP03	pa01
where	pa01.GODWFLAGc_1 = 1

create volatile table ZZMD0C, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntKvEjMedl	FLOAT, 
	ForsBelExBVEjMedl	FLOAT, 
	ForsBelExEjMedl	FLOAT, 
	InkBelExEjMedl	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0C 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	pa11.WJXBFS1  AntKvEjMedl,
	pa12.WJXBFS1  ForsBelExBVEjMedl,
	pa12.WJXBFS2  ForsBelExEjMedl,
	pa12.WJXBFS3  InkBelExEjMedl
from	ZZOP0A	pa11
	full outer join	ZZOP0B	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)

create volatile table ZZMD0D, no fallback, no log(
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Membership_Num) on commit preserve rows

;insert into ZZMD0D 
select	a13.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num <> -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Processing_Dt = a14.Calendar_Dt)
where	(a14.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Membership_Num

create volatile table ZZMD0E, no fallback, no log(
	AccrualAmtButik	FLOAT) on commit preserve rows

;insert into ZZMD0E 
select	sum(pa11.WJXBFS1)  AccrualAmtButik
from	ZZMD0D	pa11

create volatile table ZZSP0F, no fallback, no log(
	Forfallen_bonus	FLOAT) on commit preserve rows

;insert into ZZSP0F 
select	sum(a11.Expired_Amt)  Forfallen_bonus
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Expiration_Dt = a12.Calendar_Dt)
where	a12.Calendar_Week_Id in (201334)

create volatile table ZZSP0G, no fallback, no log(
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZSP0G 
select	sum(a11.Downgraded_Amt)  DowngradedAmt
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_WEEK_F	a11
where	a11.Calendar_Week_Id in (201334)

create volatile table ZZMD0H, no fallback, no log(
	Forfallen_bonus	FLOAT, 
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZMD0H 
select	pa11.Forfallen_bonus  Forfallen_bonus,
	pa12.DowngradedAmt  DowngradedAmt
from	ZZSP0F	pa11
	cross join	ZZSP0G	pa12

create volatile table ZZMD0I, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	ForsBelExBVMedlKvitto	FLOAT, 
	InkBelExMedlKvitto	FLOAT, 
	GODWFLAG13_1	INTEGER, 
	ForsBelExBVKvitto	FLOAT, 
	InkBelExKvitto	FLOAT, 
	ForsBelExBVEjMedlKvitto	FLOAT, 
	InkBelExEjMedlKvitto	FLOAT, 
	GODWFLAG27_1	INTEGER)
primary index (Calendar_Week_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0I 
select	a12.Calendar_Week_Id  Calendar_Week_Id,
	a11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	a11.Store_Seq_Num  Store_Seq_Num,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVMedlKvitto,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExMedlKvitto,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG13_1,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVKvitto,
	sum(a11.Unit_Cost_Amt)  InkBelExKvitto,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVEjMedlKvitto,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExEjMedlKvitto,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG27_1
from	UV2SemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
where	(a12.Calendar_Week_Id in (201334)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a12.Calendar_Week_Id,
	a11.Sales_Tran_Seq_Num,
	a11.Store_Seq_Num

create volatile table ZZSP0J, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrMedlKvitto	FLOAT, 
	GODWFLAG14_1	INTEGER, 
	BVKrEjMedlKvitto	FLOAT, 
	GODWFLAG28_1	INTEGER)
primary index (Calendar_Week_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZSP0J 
select	pa11.Calendar_Week_Id  Calendar_Week_Id,
	pa11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num  Store_Seq_Num,
	sum((Case when pa11.GODWFLAG13_1 = 1 then (ZEROIFNULL(pa11.ForsBelExBVMedlKvitto) - ZEROIFNULL(pa11.InkBelExMedlKvitto)) else NULL end))  BVKrMedlKvitto,
	max((Case when pa11.GODWFLAG13_1 = 1 then 1 else 0 end))  GODWFLAG14_1,
	sum((Case when pa11.GODWFLAG27_1 = 1 then (ZEROIFNULL(pa11.ForsBelExBVEjMedlKvitto) - ZEROIFNULL(pa11.InkBelExEjMedlKvitto)) else NULL end))  BVKrEjMedlKvitto,
	max((Case when pa11.GODWFLAG27_1 = 1 then 1 else 0 end))  GODWFLAG28_1
from	ZZMD0I	pa11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(pa11.Store_Seq_Num = a12.Store_Seq_Num)
where	(pa11.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (pa11.GODWFLAG13_1 = 1
 or pa11.GODWFLAG27_1 = 1))
group by	pa11.Calendar_Week_Id,
	pa11.Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num

create volatile table ZZOP0K, no fallback, no log(
	Store_Seq_Num	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Seq_Num, Sales_Tran_Seq_Num, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0K 
select	pa01.Store_Seq_Num  Store_Seq_Num,
	pa01.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.BVKrMedlKvitto  WJXBFS1
from	ZZSP0J	pa01
where	pa01.GODWFLAG14_1 = 1

create volatile table ZZMD0L, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrMedlKvitto	FLOAT, 
	ForsBelExBVKvitto	FLOAT, 
	InkBelExKvitto	FLOAT)
primary index (Calendar_Week_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0L 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Sales_Tran_Seq_Num, pa12.Sales_Tran_Seq_Num)  Sales_Tran_Seq_Num,
	coalesce(pa11.Store_Seq_Num, pa12.Store_Seq_Num)  Store_Seq_Num,
	pa11.WJXBFS1  BVKrMedlKvitto,
	pa12.ForsBelExBVKvitto  ForsBelExBVKvitto,
	pa12.InkBelExKvitto  InkBelExKvitto
from	ZZOP0K	pa11
	full outer join	ZZMD0I	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)

create volatile table ZZMD0M, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	BVProcMedlKvitto	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0M 
select	pa11.Calendar_Week_Id  Calendar_Week_Id,
	sum(ZEROIFNULL((pa11.BVKrMedlKvitto / NULLIFZERO(pa12.ForsBelExBVMedlKvitto))))  BVProcMedlKvitto
from	ZZMD0L	pa11
	join	ZZMD0I	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Week_Id in (201334)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and pa12.GODWFLAG13_1 = 1)
group by	pa11.Calendar_Week_Id

create volatile table ZZMD0N, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrKvitto	FLOAT)
primary index (Calendar_Week_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0N 
select	pa11.Calendar_Week_Id  Calendar_Week_Id,
	pa11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num  Store_Seq_Num,
	sum((ZEROIFNULL(pa11.ForsBelExBVKvitto) - ZEROIFNULL(pa11.InkBelExKvitto)))  BVKrKvitto
from	ZZMD0L	pa11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(pa11.Store_Seq_Num = a12.Store_Seq_Num)
where	(pa11.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	pa11.Calendar_Week_Id,
	pa11.Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num

create volatile table ZZMD0O, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	BVProcKvitto	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0O 
select	pa11.Calendar_Week_Id  Calendar_Week_Id,
	sum(ZEROIFNULL((pa11.BVKrKvitto / NULLIFZERO(pa12.ForsBelExBVKvitto))))  BVProcKvitto
from	ZZMD0N	pa11
	join	ZZMD0L	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Week_Id in (201334)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	pa11.Calendar_Week_Id

create volatile table ZZOP0P, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0P 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.KampInkBelExEjMedl  WJXBFS1,
	pa01.KampForsExBVEjMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG1a_1 = 1

create volatile table ZZMD0Q, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntalSaldaStEjMedl	FLOAT, 
	AntSaldaViktartEjMedl	FLOAT, 
	AntalSaldaStMedl	FLOAT, 
	AntSaldaViktartMedl	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0Q 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum((Case when (a11.UOM_Category_Cd in ('UNT') and a11.Contact_Account_Seq_Num = -1) then a11.Item_Qty else NULL end))  AntalSaldaStEjMedl,
	sum((Case when (a11.UOM_Category_Cd in ('WGH') and a11.Contact_Account_Seq_Num = -1) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartEjMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartMedl
from	UV2SemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and ((a11.UOM_Category_Cd in ('UNT')
 and a11.Contact_Account_Seq_Num = -1)
 or (a11.UOM_Category_Cd in ('WGH')
 and a11.Contact_Account_Seq_Num = -1)
 or (a11.Contact_Account_Seq_Num <> -1
 and a11.UOM_Category_Cd in ('UNT'))
 or (a11.Contact_Account_Seq_Num <> -1
 and a11.UOM_Category_Cd in ('WGH'))))
group by	a13.Calendar_Week_Id

create volatile table ZZOP0R, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0R 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.KampForsExBVMedl  WJXBFS1,
	pa01.KampInkBelExMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG1d_1 = 1

create volatile table ZZMD0S, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id, Membership_Num) on commit preserve rows

;insert into ZZMD0S 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a12.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num = -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_WEEK_F	a11
	join	UV2SemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
where	a11.Calendar_Week_Id in (201334)
group by	a11.Calendar_Week_Id,
	a12.Membership_Num

create volatile table ZZMD0T, no fallback, no log(
	AccrualAmtEjButik	FLOAT) on commit preserve rows

;insert into ZZMD0T 
select	sum(pa11.WJXBFS1)  AccrualAmtEjButik
from	ZZMD0S	pa11
where	pa11.Calendar_Week_Id in (201334)

create volatile table ZZMD0U, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	WJXBFS1	INTEGER, 
	GODWFLAG22_1	INTEGER, 
	WJXBFS2	INTEGER, 
	GODWFLAG25_1	INTEGER)
primary index (Calendar_Week_Id, Sales_Tran_Seq_Num) on commit preserve rows

;insert into ZZMD0U 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	count(distinct (Case when a11.Contact_Account_Seq_Num = -1 then a11.Scan_Code_Seq_Num else NULL end))  WJXBFS1,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG22_1,
	count(distinct (Case when a11.Contact_Account_Seq_Num <> -1 then a11.Scan_Code_Seq_Num else NULL end))  WJXBFS2,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG25_1
from	UV2SemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201334)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (a11.Contact_Account_Seq_Num = -1
 or a11.Contact_Account_Seq_Num <> -1))
group by	a13.Calendar_Week_Id,
	a11.Sales_Tran_Seq_Num

create volatile table ZZMD0V, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntVarorEjMedl	FLOAT, 
	AntVarorMedl	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0V 
select	pa11.Calendar_Week_Id  Calendar_Week_Id,
	sum((Case when pa11.GODWFLAG22_1 = 1 then pa11.WJXBFS1 else NULL end))  AntVarorEjMedl,
	sum((Case when pa11.GODWFLAG25_1 = 1 then pa11.WJXBFS2 else NULL end))  AntVarorMedl
from	ZZMD0U	pa11
where	(pa11.Calendar_Week_Id in (201334)
 and (pa11.GODWFLAG22_1 = 1
 or pa11.GODWFLAG25_1 = 1))
group by	pa11.Calendar_Week_Id

create volatile table ZZOP0W, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0W 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.KampInkopsBelEx  WJXBFS1,
	pa01.KampForsBelExBVBer  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG24_1 = 1

create volatile table ZZMD0X, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	BVProcEjMedlKvitto	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0X 
select	pa11.Calendar_Week_Id  Calendar_Week_Id,
	sum(ZEROIFNULL((pa11.BVKrEjMedlKvitto / NULLIFZERO(pa12.ForsBelExBVEjMedlKvitto))))  BVProcEjMedlKvitto
from	ZZSP0J	pa11
	join	ZZMD0I	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Week_Id in (201334)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and pa12.GODWFLAG27_1 = 1
 and pa11.GODWFLAG28_1 = 1)
group by	pa11.Calendar_Week_Id

select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id, pa113.Calendar_Week_Id, pa114.Calendar_Week_Id, pa116.Calendar_Week_Id)  Calendar_Week_Id,
	pa11.AntRekrKontaktAck  AntRekrKontaktAck,
	pa12.AntKvMedl  AntKvMedl,
	ZEROIFNULL((pa13.AntRekrKontakt / NULLIFZERO((ZEROIFNULL(pa13.AntRekrKontakt) + (ZEROIFNULL(pa13.AntKv) - ZEROIFNULL(pa12.AntKvMedl))))))  RekrKvotMed,
	ZEROIFNULL((pa12.WJXBFS1 / NULLIFZERO(pa13.WJXBFS1)))  AndelBVMedl,
	pa12.TotMedlRbt  TotMedlRbt,
	pa12.TotRbtExMedlRbt  TotRbtExMedlRbt,
	pa13.ForsBelEx  ForsBelEx,
	pa12.ForsBelExMedl  ForsBelExMedl,
	pa14.AntKvEjMedl  AntKvEjMedl,
	pa117.AccrualAmtButik  AccrualAmtButik,
	pa118.Forfallen_bonus  Forfallen_bonus,
	pa15.BVProcMedlKvitto  BVProcMedlKvitto,
	pa16.BVProcKvitto  BVProcKvitto,
	pa17.WJXBFS1  KampInkBelExEjMedl,
	pa13.ForsBelExBVBer  ForsBelExBVBer,
	pa12.InkBelExMedl  InkBelExMedl,
	(ZEROIFNULL(pa18.AntalSaldaStEjMedl) + ZEROIFNULL(pa18.AntSaldaViktartEjMedl))  AntStreckkodEjMedl,
	pa110.WJXBFS1  KampForsExBVMedl,
	(ZEROIFNULL(pa18.AntalSaldaStMedl) + ZEROIFNULL(pa18.AntSaldaViktartMedl))  AntStreckkodMedl,
	pa13.AntKv  AntKv,
	pa119.AccrualAmtEjButik  AccrualAmtEjButik,
	pa17.WJXBFS2  KampForsExBVEjMedl,
	pa13.InkopsBelEx  InkopsBelEx,
	pa110.WJXBFS2  KampInkBelExMedl,
	pa113.AntVarorEjMedl  AntVarorEjMedl,
	pa114.WJXBFS1  KampInkopsBelEx,
	ZEROIFNULL(((pa13.AccrualAmt / NULLIFZERO(pa117.AccrualAmtButik)) * pa118.DowngradedAmt))  NedgraderingBonusButik,
	pa13.PurchaseAmt  PurchaseAmt,
	pa13.AccrualAmt  AccrualAmt,
	pa113.AntVarorMedl  AntVarorMedl,
	pa12.ForsBelExBVMedlem  ForsBelExBVMedlem,
	pa14.ForsBelExBVEjMedl  ForsBelExBVEjMedl,
	pa14.ForsBelExEjMedl  ForsBelExEjMedl,
	pa114.WJXBFS2  KampForsBelExBVBer,
	pa14.InkBelExEjMedl  InkBelExEjMedl,
	pa116.BVProcEjMedlKvitto  BVProcEjMedlKvitto
from	ZZMD00	pa11
	full outer join	ZZMD06	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	full outer join	ZZMD09	pa13
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id)
	full outer join	ZZMD0C	pa14
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id)
	full outer join	ZZMD0M	pa15
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id) = pa15.Calendar_Week_Id)
	full outer join	ZZMD0O	pa16
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id) = pa16.Calendar_Week_Id)
	full outer join	ZZOP0P	pa17
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id) = pa17.Calendar_Week_Id)
	full outer join	ZZMD0Q	pa18
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id) = pa18.Calendar_Week_Id)
	full outer join	ZZOP0R	pa110
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id) = pa110.Calendar_Week_Id)
	full outer join	ZZMD0V	pa113
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id) = pa113.Calendar_Week_Id)
	full outer join	ZZOP0W	pa114
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id, pa113.Calendar_Week_Id) = pa114.Calendar_Week_Id)
	full outer join	ZZMD0X	pa116
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id, pa113.Calendar_Week_Id, pa114.Calendar_Week_Id) = pa116.Calendar_Week_Id)
	cross join	ZZMD0E	pa117
	cross join	ZZMD0H	pa118
	cross join	ZZMD0T	pa119


SET QUERY_BAND = NONE For Session;

