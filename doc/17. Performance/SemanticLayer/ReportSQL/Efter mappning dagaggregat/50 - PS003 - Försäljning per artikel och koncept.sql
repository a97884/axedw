SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=PS003 - Försäljning per artikel och koncept; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-15'
group by	a11.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	ForsBelEx	FLOAT, 
	AntalSalda	FLOAT, 
	WJXBFS1	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD01 
select	a13.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.Item_Qty)  AntalSalda,
	sum((a11.Unit_Selling_Price_Amt + a11.Tax_Amt))  WJXBFS1,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a19
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num and 
	a17.Art_Hier_Lvl_4_Seq_Num = a19.Art_Hier_Lvl_4_Seq_Num)
where	(a13.Concept_Cd in ('HEM', 'WIL')
 and (a19.Art_Hier_Lvl_2_Id in ('12')
 or a19.Art_Hier_Lvl_4_Id in ('010320'))
 and a16.Vendor_Seq_Num in (6570))
group by	a13.Concept_Cd,
	a11.Scan_Code_Seq_Num

create volatile table ZZMD02, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	ForsBelExFg	FLOAT, 
	AntalSaldaFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT)
primary index (Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD02 
select	a14.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a15
	  on 	(a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a16
	  on 	(a15.Measuring_Unit_Seq_Num = a16.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a17
	  on 	(a16.Article_Seq_Num = a17.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a110
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a110.Art_Hier_Lvl_2_Seq_Num and 
	a18.Art_Hier_Lvl_4_Seq_Num = a110.Art_Hier_Lvl_4_Seq_Num)
where	(a14.Concept_Cd in ('HEM', 'WIL')
 and (a110.Art_Hier_Lvl_2_Id in ('12')
 or a110.Art_Hier_Lvl_4_Id in ('010320'))
 and a17.Vendor_Seq_Num in (6570))
group by	a14.Concept_Cd,
	a11.Scan_Code_Seq_Num

create volatile table ZZMD03, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	AntalBtkMedFsg	INTEGER)
primary index (Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD03 
select	a18.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a19
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num and 
	a16.Art_Hier_Lvl_4_Seq_Num = a19.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a18.Concept_Cd in ('HEM', 'WIL')
 and (a19.Art_Hier_Lvl_2_Id in ('12')
 or a19.Art_Hier_Lvl_4_Id in ('010320'))
 and a15.Vendor_Seq_Num in (6570))
group by	a18.Concept_Cd,
	a11.Scan_Code_Seq_Num

select	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	max(a18.Art_Hier_Lvl_4_Desc)  Art_Hier_Lvl_4_Desc,
	coalesce(pa11.Scan_Code_Seq_Num, pa12.Scan_Code_Seq_Num, pa13.Scan_Code_Seq_Num)  Scan_Code_Seq_Num,
	max(a14.Scan_Cd)  Scan_Cd,
	a15.Article_Seq_Num  Article_Seq_Num,
	max(a16.Article_Id)  Article_Id,
	max(a16.Article_Desc)  Article_Desc,
	a16.Vendor_Seq_Num  Vendor_Seq_Num,
	max(a110.Vendor_Id)  Vendor_Id,
	max(a110.Vendor_Name)  Vendor_Name,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd)  Concept_Cd,
	max(a19.Concept_Name)  Concept_Name,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa12.ForsBelExFg)  ForsBelExFg,
	max(pa11.AntalSalda)  AntalSalda,
	max(pa12.AntalSaldaFg)  AntalSaldaFg,
	max(pa13.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa11.WJXBFS1)  WJXBFS1,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa12.InkopsBelExFg)  InkopsBelExFg,
	max(pa12.ForsBelExBVBerFg)  ForsBelExBVBerFg
from	ZZMD01	pa11
	full outer join	ZZMD02	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Scan_Code_Seq_Num = pa12.Scan_Code_Seq_Num)
	full outer join	ZZMD03	pa13
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa13.Concept_Cd and 
	coalesce(pa11.Scan_Code_Seq_Num, pa12.Scan_Code_Seq_Num) = pa13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(coalesce(pa11.Scan_Code_Seq_Num, pa12.Scan_Code_Seq_Num, pa13.Scan_Code_Seq_Num) = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a17.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.CONCEPT_D	a19
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd) = a19.Concept_Cd)
	join	UV2SemCMNVOUT.VENDOR_D	a110
	  on 	(a16.Vendor_Seq_Num = a110.Vendor_Seq_Num)
group by	a18.Art_Hier_Lvl_4_Id,
	coalesce(pa11.Scan_Code_Seq_Num, pa12.Scan_Code_Seq_Num, pa13.Scan_Code_Seq_Num),
	a15.Article_Seq_Num,
	a16.Vendor_Seq_Num,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd)


SET QUERY_BAND = NONE For Session;

