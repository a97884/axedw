SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU203a - AO Månadsrapport - alla mått; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Month_Id	INTEGER)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Month_Id  Calendar_Month_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-07-01'
group by	a11.Calendar_Month_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	AntalOR	FLOAT, 
	AntalMinskadeOR	FLOAT, 
	AntalOkadeOR	FLOAT, 
	AntalOkadeORUtanAtgard	FLOAT, 
	AntalMinskadeORUtanAtgard	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD01 
select	pa13.Calendar_Month_Id  Calendar_Month_Id,
	a14.Concept_Cd  Concept_Cd,
	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a14.Store_Id  Store_Id,
	sum(a11.Inv_Request_Cnt)  AntalOR,
	sum(a11.Decreased_Cnt)  AntalMinskadeOR,
	sum(a11.Increased_Cnt)  AntalOkadeOR,
	sum((Case when a11.Fixed_Order_Ind = 0 then a11.Increased_Cnt else NULL end))  AntalOkadeORUtanAtgard,
	sum((Case when a11.Fixed_Order_Ind = 0 then a11.Decreased_Cnt else NULL end))  AntalMinskadeORUtanAtgard
from	UV2SemCMNVOUT.ORDER_CONTROL_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Order_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Month_Id = pa13.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a11.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
group by	pa13.Calendar_Month_Id,
	a14.Concept_Cd,
	a17.Art_Hier_Lvl_2_Id,
	a14.Store_Id

create volatile table ZZSP02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	RiktadSK	FLOAT, 
	GenomfSK	FLOAT, 
	SpontanSK	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZSP02 
select	pa13.Calendar_Month_Id  Calendar_Month_Id,
	a14.Concept_Cd  Concept_Cd,
	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a14.Store_Id  Store_Id,
	sum(a11.Requested_Cntrl_Cnt)  RiktadSK,
	sum(a11.Performed_Cntrl_Cnt)  GenomfSK,
	sum(a11.Spontaneous_Cntrl_Cnt)  SpontanSK
from	UV2SemCMNVOUT.STOCK_BALANCE_REQUEST_CNTRL_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Stock_Balance_Request_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Month_Id = pa13.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a11.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	a11.AO_Ind in (1)
group by	pa13.Calendar_Month_Id,
	a14.Concept_Cd,
	a17.Art_Hier_Lvl_2_Id,
	a14.Store_Id

create volatile table ZZSP03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	AOSvinnKr	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZSP03 
select	pa13.Calendar_Month_Id  Calendar_Month_Id,
	a14.Concept_Cd  Concept_Cd,
	a19.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a14.Store_Id  Store_Id,
	sum(a11.Total_Line_Value_Amt)  AOSvinnKr
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Adjustment_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Month_Id = pa13.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a15
	  on 	(a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a16
	  on 	(a15.Measuring_Unit_Seq_Num = a16.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a17
	  on 	(a16.Article_Seq_Num = a17.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	a11.AO_Ind in (1)
group by	pa13.Calendar_Month_Id,
	a14.Concept_Cd,
	a19.Art_Hier_Lvl_2_Id,
	a14.Store_Id

create volatile table ZZSP04, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	AOForsBelExBVBer	FLOAT, 
	AOInkopsBelEx	FLOAT, 
	AOForsBelExMoms	FLOAT, 
	AOForsBvL09	FLOAT, 
	AOInkBelL09	FLOAT, 
	GODWFLAGa_1	INTEGER)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZSP04 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a13.Concept_Cd  Concept_Cd,
	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a13.Store_Id  Store_Id,
	sum(a11.GP_Unit_Selling_Price_Amt)  AOForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  AOInkopsBelEx,
	sum(a11.Unit_Selling_Price_Amt)  AOForsBelExMoms,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  AOForsBvL09,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end))  AOInkBelL09,
	max((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then 1 else 0 end))  GODWFLAGa_1
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	a11.AO_Ind in (1)
group by	a11.Calendar_Month_Id,
	a13.Concept_Cd,
	a18.Art_Hier_Lvl_2_Id,
	a13.Store_Id

create volatile table ZZMD05, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	RiktadSK	FLOAT, 
	AOSvinnKr	FLOAT, 
	GenomfSK	FLOAT, 
	AOForsBelExBVBer	FLOAT, 
	SpontanSK	FLOAT, 
	AOInkopsBelEx	FLOAT, 
	AOForsBelExMoms	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD05 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd)  Concept_Cd,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id)  Store_Id,
	pa11.RiktadSK  RiktadSK,
	pa12.AOSvinnKr  AOSvinnKr,
	pa11.GenomfSK  GenomfSK,
	pa13.AOForsBelExBVBer  AOForsBelExBVBer,
	pa11.SpontanSK  SpontanSK,
	pa13.AOInkopsBelEx  AOInkopsBelEx,
	pa13.AOForsBelExMoms  AOForsBelExMoms
from	ZZSP02	pa11
	full outer join	ZZSP03	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZSP04	pa13
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa13.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa13.Concept_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)

create volatile table ZZMD06, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	AntArtIAoSort	INTEGER, 
	AntSaldo	INTEGER, 
	WJXBFS1	INTEGER)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD06 
select	pa13.Calendar_Month_Id  Calendar_Month_Id,
	a14.Concept_Cd  Concept_Cd,
	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a14.Store_Id  Store_Id,
	count(distinct a11.Article_Seq_Num)  AntArtIAoSort,
	count(a11.Article_Seq_Num)  AntSaldo,
	(Case when max((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then 1 else 0 end)) = 1 then count((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then a11.Article_Seq_Num else NULL end)) else NULL end)  WJXBFS1
from	UV2SemCMNVOUT.PERPETUAL_INVENTORY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Perpetual_Inv_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Month_Id = pa13.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a11.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	a11.AO_Ind in (1)
group by	pa13.Calendar_Month_Id,
	a14.Concept_Cd,
	a17.Art_Hier_Lvl_2_Id,
	a14.Store_Id

create volatile table ZZMD07, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	AntalAndrMPL	FLOAT, 
	AntalAndrTD	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD07 
select	pa13.Calendar_Month_Id  Calendar_Month_Id,
	a14.Concept_Cd  Concept_Cd,
	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a14.Store_Id  Store_Id,
	sum((Case when a11.Auto_Repl_Control_Type_Cd in ('MPL') then a11.Auto_Repl_Control_Cnt else NULL end))  AntalAndrMPL,
	sum((Case when a11.Auto_Repl_Control_Type_Cd in ('TD ') then a11.Auto_Repl_Control_Cnt else NULL end))  AntalAndrTD
from	UV2SemCMNVOUT.AUTO_REPL_CNTRL_PARAM_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Change_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Month_Id = pa13.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a11.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a11.AO_Ind in (1)
 and (a11.Auto_Repl_Control_Type_Cd in ('MPL')
 or a11.Auto_Repl_Control_Type_Cd in ('TD ')))
group by	pa13.Calendar_Month_Id,
	a14.Concept_Cd,
	a17.Art_Hier_Lvl_2_Id,
	a14.Store_Id

create volatile table ZZOP08, no fallback, no log(
	Store_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Concept_Cd	CHAR(3), 
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Store_Id, Art_Hier_Lvl_2_Id, Concept_Cd, Calendar_Month_Id) on commit preserve rows

;insert into ZZOP08 
select	pa01.Store_Id  Store_Id,
	pa01.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	pa01.Concept_Cd  Concept_Cd,
	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AOForsBvL09  WJXBFS1,
	pa01.AOInkBelL09  WJXBFS2
from	ZZSP04	pa01
where	pa01.GODWFLAGa_1 = 1

create volatile table ZZMD09, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD09 
select	pa12.Calendar_Month_Id  Calendar_Month_Id,
	a11.Month_Nbr_Of_Days  WJXBFS1
from	UV2SemCMNVOUT.CALENDAR_MONTH_D	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Month_Id = pa12.Calendar_Month_Id)

create volatile table ZZMD0A, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	AOManRbt	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD0A 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a13.Concept_Cd  Concept_Cd,
	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a13.Store_Id  Store_Id,
	sum(a11.Discount_Amt)  AOManRbt
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a11.AO_Ind in (1)
 and a11.Discount_Type_Cd in ('MP'))
group by	a11.Calendar_Month_Id,
	a13.Concept_Cd,
	a18.Art_Hier_Lvl_2_Id,
	a13.Store_Id

create volatile table ZZMD0B, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Calendar_Dt	DATE, 
	Store_Id	INTEGER, 
	AntalAndrParamDag	INTEGER)
primary index (Concept_Cd, Art_Hier_Lvl_2_Id, Calendar_Dt, Store_Id) on commit preserve rows

;insert into ZZMD0B 
select	a14.Concept_Cd  Concept_Cd,
	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a11.Change_Dt  Calendar_Dt,
	a14.Store_Id  Store_Id,
	count(distinct a11.Article_Seq_Num)  AntalAndrParamDag
from	UV2SemCMNVOUT.AUTO_REPL_CNTRL_PARAM_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Change_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Month_Id = pa13.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a11.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
where	(a11.Auto_Repl_Control_Type_Cd in ('MPL', 'TD ')
 and a11.AO_Ind in (1))
group by	a14.Concept_Cd,
	a17.Art_Hier_Lvl_2_Id,
	a11.Change_Dt,
	a14.Store_Id

create volatile table ZZMD0C, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Store_Id	INTEGER, 
	AntalAndrParam	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Store_Id) on commit preserve rows

;insert into ZZMD0C 
select	pa13.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Concept_Cd  Concept_Cd,
	pa11.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	pa11.Store_Id  Store_Id,
	sum(pa11.AntalAndrParamDag)  AntalAndrParam
from	ZZMD0B	pa11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(pa11.Calendar_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Month_Id = pa13.Calendar_Month_Id)
group by	pa13.Calendar_Month_Id,
	pa11.Concept_Cd,
	pa11.Art_Hier_Lvl_2_Id,
	pa11.Store_Id

select	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa110.Store_Id)  Store_Id,
	max(a112.Store_Name)  Store_Name,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id, pa18.Art_Hier_Lvl_2_Id, pa110.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a113.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa18.Concept_Cd, pa110.Concept_Cd)  Concept_Cd,
	max(a114.Concept_Name)  Concept_Name,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa110.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa11.AntalOR)  AntalOR,
	max(pa12.RiktadSK)  RiktadSK,
	max(pa13.AntArtIAoSort)  AntArtIAoSort,
	max(pa14.AntalAndrMPL)  AntalAndrMPL,
	max(pa12.AOSvinnKr)  AOSvinnKr,
	max(pa15.WJXBFS1)  AOForsBvL09,
	max(pa13.WJXBFS1)  HIHExklRest,
	max(pa12.GenomfSK)  GenomfSK,
	max(pa111.WJXBFS1)  WJXBFS1,
	max(pa11.AntalOkadeORUtanAtgard)  AntalOkadeORUtanAtgard,
	max(pa13.AntSaldo)  AntSaldo,
	max(pa11.AntalMinskadeOR)  AntalMinskadeOR,
	max(pa18.AOManRbt)  AOManRbt,
	max(pa14.AntalAndrTD)  AntalAndrTD,
	max(pa12.AOForsBelExBVBer)  AOForsBelExBVBer,
	max(pa12.SpontanSK)  SpontanSK,
	max(pa15.WJXBFS2)  AOInkBelL09,
	max(pa12.AOInkopsBelEx)  AOInkopsBelEx,
	max(pa110.AntalAndrParam)  AntalAndrParam,
	max(pa12.AOForsBelExMoms)  AOForsBelExMoms,
	max(pa11.AntalMinskadeORUtanAtgard)  AntalMinskadeORUtanAtgard,
	max(pa11.AntalOkadeOR)  AntalOkadeOR
from	ZZMD01	pa11
	full outer join	ZZMD05	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZMD06	pa13
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa13.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa13.Concept_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	full outer join	ZZMD07	pa14
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id) = pa14.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id) = pa14.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd) = pa14.Concept_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
	full outer join	ZZOP08	pa15
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id) = pa15.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa15.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd) = pa15.Concept_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id) = pa15.Store_Id)
	full outer join	ZZMD0A	pa18
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id) = pa18.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id) = pa18.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd) = pa18.Concept_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id) = pa18.Store_Id)
	full outer join	ZZMD0C	pa110
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id, pa18.Art_Hier_Lvl_2_Id) = pa110.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id) = pa110.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa18.Concept_Cd) = pa110.Concept_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id) = pa110.Store_Id)
	left outer join	ZZMD09	pa111
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa110.Calendar_Month_Id) = pa111.Calendar_Month_Id)
	join	UV2SemCMNVOUT.STORE_D	a112
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa110.Store_Id) = a112.Store_Id)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a113
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id, pa18.Art_Hier_Lvl_2_Id, pa110.Art_Hier_Lvl_2_Id) = a113.Art_Hier_Lvl_2_Id)
	join	UV2SemCMNVOUT.CONCEPT_D	a114
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa18.Concept_Cd, pa110.Concept_Cd) = a114.Concept_Cd)
group by	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa18.Store_Id, pa110.Store_Id),
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id, pa18.Art_Hier_Lvl_2_Id, pa110.Art_Hier_Lvl_2_Id),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd, pa15.Concept_Cd, pa18.Concept_Cd, pa110.Concept_Cd),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa18.Calendar_Month_Id, pa110.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;

