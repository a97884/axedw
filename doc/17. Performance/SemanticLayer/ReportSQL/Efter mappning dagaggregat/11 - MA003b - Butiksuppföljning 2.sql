SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=MA003b - Butiksuppföljning 2; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-14'
group by	a11.Calendar_Week_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntRekrKontakt	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP01 
select	pa13.Calendar_Week_Id  Calendar_Week_Id,
	a14.Retail_Region_Cd  Retail_Region_Cd,
	a14.Store_Id  Store_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontakt
from	UV2SemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	a14.Concept_Cd in ('WIL', 'WHE', 'WH2')
group by	pa13.Calendar_Week_Id,
	a14.Retail_Region_Cd,
	a14.Store_Id

create volatile table ZZSP02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExMedl	FLOAT, 
	ForsBelInklMedl	FLOAT, 
	GODWFLAG7_1	INTEGER, 
	ForsBelInklEjMedl	FLOAT, 
	GODWFLAGb_1	INTEGER)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP02 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Retail_Region_Cd  Retail_Region_Cd,
	a13.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExMedl,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then (a11.Unit_Selling_Price_Amt + a11.Tax_Amt) else NULL end))  ForsBelInklMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG7_1,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then (a11.Unit_Selling_Price_Amt + a11.Tax_Amt) else NULL end))  ForsBelInklEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAGb_1
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
where	a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
group by	a11.Calendar_Week_Id,
	a13.Retail_Region_Cd,
	a13.Store_Id

create volatile table ZZMD03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntRekrKontakt	FLOAT, 
	ForsBelEx	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.AntRekrKontakt  AntRekrKontakt,
	pa12.ForsBelEx  ForsBelEx
from	ZZSP01	pa11
	full outer join	ZZSP02	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD04, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntRekrKontaktAck	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD04 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a15.Retail_Region_Cd  Retail_Region_Cd,
	a15.Store_Id  Store_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontaktAck
from	UV2SemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	UV2SemCMNVOUT.CALENDAR_WEEK_YTD_D	a13
	  on 	(a12.Calendar_Week_Id = a13.Calendar_Week_YTD_Id)
	join	ZZMQ00	pa14
	  on 	(a13.Calendar_Week_Id = pa14.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a15
	  on 	(a11.Store_Seq_Num = a15.Store_Seq_Num)
where	a15.Concept_Cd in ('WIL', 'WHE', 'WH2')
group by	a13.Calendar_Week_Id,
	a15.Retail_Region_Cd,
	a15.Store_Id

create volatile table ZZOP05, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP05 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.ForsBelExMedl  WJXBFS1,
	pa01.ForsBelInklMedl  WJXBFS2
from	ZZSP02	pa01
where	pa01.GODWFLAG7_1 = 1

create volatile table ZZSP06, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntKvMedl	FLOAT, 
	GODWFLAG8_1	INTEGER, 
	AntKvEjMedl	FLOAT, 
	GODWFLAGa_1	INTEGER)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP06 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Retail_Region_Cd  Retail_Region_Cd,
	a13.Store_Id  Store_Id,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Receipt_Cnt else NULL end))  AntKvMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG8_1,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Receipt_Cnt else NULL end))  AntKvEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAGa_1
from	UV2SemCMNVOUT.SALES_TRANSACTION_WEEK_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
where	(a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (a11.Contact_Account_Seq_Num <> -1
 or a11.Contact_Account_Seq_Num = -1))
group by	a11.Calendar_Week_Id,
	a13.Retail_Region_Cd,
	a13.Store_Id

create volatile table ZZOP07, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP07 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntKvMedl  WJXBFS1
from	ZZSP06	pa01
where	pa01.GODWFLAG8_1 = 1

create volatile table ZZMD08, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelExMedl	FLOAT, 
	AntKvMedl	FLOAT, 
	ForsBelInklMedl	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD08 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.WJXBFS1  ForsBelExMedl,
	pa12.WJXBFS1  AntKvMedl,
	pa11.WJXBFS2  ForsBelInklMedl
from	ZZOP05	pa11
	full outer join	ZZOP07	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZOP09, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP09 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntKvEjMedl  WJXBFS1
from	ZZSP06	pa01
where	pa01.GODWFLAGa_1 = 1

create volatile table ZZOP0A, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0A 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.ForsBelInklEjMedl  WJXBFS1
from	ZZSP02	pa01
where	pa01.GODWFLAGb_1 = 1

create volatile table ZZMD0B, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntKvEjMedl	FLOAT, 
	ForsBelInklEjMedl	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD0B 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.WJXBFS1  AntKvEjMedl,
	pa12.WJXBFS1  ForsBelInklEjMedl
from	ZZOP09	pa11
	full outer join	ZZOP0A	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD0C, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntalSaldaStEjMedl	FLOAT, 
	AntSaldaViktartEjMedl	FLOAT, 
	AntalSaldaStMedl	FLOAT, 
	AntSaldaViktartMedl	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD0C 
select	pa13.Calendar_Week_Id  Calendar_Week_Id,
	a14.Retail_Region_Cd  Retail_Region_Cd,
	a14.Store_Id  Store_Id,
	sum((Case when (a11.UOM_Category_Cd in ('UNT') and a11.Contact_Account_Seq_Num = -1) then a11.Item_Qty else NULL end))  AntalSaldaStEjMedl,
	sum((Case when (a11.UOM_Category_Cd in ('WGH') and a11.Contact_Account_Seq_Num = -1) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartEjMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartMedl
from	UV2SemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a14.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and ((a11.UOM_Category_Cd in ('UNT')
 and a11.Contact_Account_Seq_Num = -1)
 or (a11.UOM_Category_Cd in ('WGH')
 and a11.Contact_Account_Seq_Num = -1)
 or (a11.Contact_Account_Seq_Num <> -1
 and a11.UOM_Category_Cd in ('UNT'))
 or (a11.Contact_Account_Seq_Num <> -1
 and a11.UOM_Category_Cd in ('WGH'))))
group by	pa13.Calendar_Week_Id,
	a14.Retail_Region_Cd,
	a14.Store_Id

select	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd)  Retail_Region_Cd,
	max(a110.Retail_Region_Name)  Retail_Region_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id)  Store_Id,
	max(a19.Store_Name)  Store_Name,
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id)  Calendar_Week_Id,
	max(pa11.AntRekrKontakt)  AntRekrKontakt,
	max(pa12.AntRekrKontaktAck)  AntRekrKontaktAck,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa13.ForsBelExMedl)  ForsBelExMedl,
	max(pa14.AntKvEjMedl)  AntKvEjMedl,
	max(pa14.ForsBelInklEjMedl)  ForsBelInklEjMedl,
	(ZEROIFNULL(max(pa15.AntalSaldaStEjMedl)) + ZEROIFNULL(max(pa15.AntSaldaViktartEjMedl)))  AntStreckkodEjMedl,
	max(pa13.AntKvMedl)  AntKvMedl,
	(ZEROIFNULL(max(pa15.AntalSaldaStMedl)) + ZEROIFNULL(max(pa15.AntSaldaViktartMedl)))  AntStreckkodMedl,
	max(pa13.ForsBelInklMedl)  ForsBelInklMedl
from	ZZMD03	pa11
	full outer join	ZZMD04	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZMD08	pa13
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = pa13.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	full outer join	ZZMD0B	pa14
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd) = pa14.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
	full outer join	ZZMD0C	pa15
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id) = pa15.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd) = pa15.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id) = pa15.Store_Id)
	join	UV2SemCMNVOUT.STORE_D	a19
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id) = a19.Store_Id)
	join	UV2SemCMNVOUT.RETAIL_REGION_D	a110
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd) = a110.Retail_Region_Cd)
group by	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id),
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id)


SET QUERY_BAND = NONE For Session;

