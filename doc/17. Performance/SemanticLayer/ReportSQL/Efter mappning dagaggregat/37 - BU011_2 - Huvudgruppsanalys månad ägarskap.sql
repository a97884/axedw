SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU011 - Huvudgruppsanalys; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalBtkMedFsg	INTEGER, 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Calendar_Month_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD00 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a17.Concept_Cd in ('HEM')
 and a17.Store_Type_Cd in ('CORP')
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a11.Calendar_Month_Id in (201308))
group by	a11.Calendar_Month_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	SvinnKr	FLOAT)
primary index (Calendar_Month_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZSP01 
select	a18.Calendar_Month_Id  Calendar_Month_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Total_Line_Value_Amt)  SvinnKr
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a17.Concept_Cd in ('HEM')
 and a17.Store_Type_Cd in ('CORP')
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a18.Calendar_Month_Id in (201308))
group by	a18.Calendar_Month_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	SvinnKr	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Month_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD02 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	pa11.AntalSalda  AntalSalda,
	pa11.ForsBelEx  ForsBelEx,
	pa11.ForsBelExBVBer  ForsBelExBVBer,
	pa12.SvinnKr  SvinnKr,
	pa11.InkopsBelEx  InkopsBelEx
from	ZZMD00	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSaldaFg	FLOAT, 
	ForsBelExFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD03 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  WJXBFS2
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_PYS_Month_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a18.Concept_Cd in ('HEM')
 and a18.Store_Type_Cd in ('CORP')
 and a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Calendar_Month_Id in (201308))
group by	a12.Calendar_Month_Id,
	a17.Art_Hier_Lvl_2_Id

create volatile table ZZSP04, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	TotForsExMomsAllProd	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP04 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Unit_Selling_Price_Amt)  TotForsExMomsAllProd
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a17.Concept_Cd in ('HEM')
 and a17.Store_Type_Cd in ('CORP')
 and a11.Calendar_Month_Id in (201308)
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a11.Calendar_Month_Id

create volatile table ZZSP05, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	TotSvinnKrAllaProd	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP05 
select	a18.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Total_Line_Value_Amt)  TotSvinnKrAllaProd
from	UV2SemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a17.Concept_Cd in ('HEM')
 and a17.Store_Type_Cd in ('CORP')
 and a18.Calendar_Month_Id in (201308)
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a18.Calendar_Month_Id

create volatile table ZZMD06, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	TotForsExMomsAllProd	FLOAT, 
	TotSvinnKrAllaProd	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD06 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	pa11.TotForsExMomsAllProd  TotForsExMomsAllProd,
	pa12.TotSvinnKrAllaProd  TotSvinnKrAllaProd
from	ZZSP04	pa11
	full outer join	ZZSP05	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)

create volatile table ZZMD07, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	TotForsExMomsAllProdFg	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD07 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Unit_Selling_Price_Amt)  TotForsExMomsAllProdFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_PYS_Month_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a18.Concept_Cd in ('HEM')
 and a18.Store_Type_Cd in ('CORP')
 and a12.Calendar_Month_Id in (201308)
 and a17.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a12.Calendar_Month_Id

create volatile table ZZMD08, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	ManuellRab	FLOAT, 
	AntalSaldaManRab	FLOAT)
primary index (Calendar_Month_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD08 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Discount_Amt)  ManuellRab,
	sum(a11.Disc_Item_Qty)  AntalSaldaManRab
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a16.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a17.Concept_Cd in ('HEM')
 and a17.Store_Type_Cd in ('CORP')
 and a11.Calendar_Month_Id in (201308)
 and a11.Discount_Type_Cd in ('MP'))
group by	a11.Calendar_Month_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZMD09, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	TotRabExMomsAllProd	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD09 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Discount_Amt)  TotRabExMomsAllProd
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a17.Concept_Cd in ('HEM')
 and a17.Store_Type_Cd in ('CORP')
 and a11.Calendar_Month_Id in (201308)
 and a11.Discount_Type_Cd in ('MP')
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a11.Calendar_Month_Id

select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a111.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	max(pa11.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa12.AntalSalda)  AntalSalda,
	max(pa13.AntalSaldaFg)  AntalSaldaFg,
	max(pa12.ForsBelEx)  ForsBelEx,
	max(pa13.ForsBelExFg)  ForsBelExFg,
	max(pa18.TotForsExMomsAllProd)  TotForsExMomsAllProd,
	max(pa19.TotForsExMomsAllProdFg)  TotForsExMomsAllProdFg,
	max(pa14.ManuellRab)  ManuellRab,
	max(pa13.WJXBFS1)  WJXBFS1,
	max(pa12.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa18.TotSvinnKrAllaProd)  TotSvinnKrAllaProd,
	max(pa12.SvinnKr)  SvinnKr,
	max(pa12.InkopsBelEx)  InkopsBelEx,
	max(pa14.AntalSaldaManRab)  AntalSaldaManRab,
	max(pa110.TotRabExMomsAllProd)  TotRabExMomsAllProd,
	max(pa11.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa13.WJXBFS2)  WJXBFS2,
	max(pa11.ForsBelEx)  ForsBelExSvinn,
	max(pa11.KampForsBelExBVBer)  KampForsBelExBVBer,
	max(pa13.InkopsBelExFg)  InkopsBelExFg,
	max(pa13.ForsBelExBVBerFg)  ForsBelExBVBerFg
from	ZZMD00	pa11
	full outer join	ZZMD02	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	full outer join	ZZMD03	pa13
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa13.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id)
	full outer join	ZZMD08	pa14
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id) = pa14.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id) = pa14.Calendar_Month_Id)
	left outer join	ZZMD06	pa18
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa18.Calendar_Month_Id)
	left outer join	ZZMD07	pa19
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa19.Calendar_Month_Id)
	left outer join	ZZMD09	pa110
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa110.Calendar_Month_Id)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a111
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id) = a111.Art_Hier_Lvl_2_Id)
group by	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id),
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id)


SET QUERY_BAND = NONE For Session;

