SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=PS001 - Försäljning och index per hvg/vg per månad; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	ForsBelEx	FLOAT, 
	AntalSalda	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Art_Hier_Lvl_4_Id, Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD00 
select	a19.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a11.Calendar_Month_Id  Calendar_Month_Id,
	a17.Concept_Cd  Concept_Cd,
	a19.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a19
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a19.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	UV2SemCMNVOUT.PROD_HIER_LVL_3_D	a18
	  on 	(a14.Prod_Hier_Lvl3_Seq_Num = a18.Prod_Hier_Lvl3_Seq_Num)
where	(a17.Concept_Cd in ('WIL')
 and a14.Vendor_Seq_Num in (11181)
 and a18.Prod_Hier_Lvl1_Seq_Num in (4)
 and a11.Calendar_Month_Id in (201308))
group by	a19.Art_Hier_Lvl_4_Id,
	a11.Calendar_Month_Id,
	a17.Concept_Cd,
	a19.Art_Hier_Lvl_2_Id

create volatile table ZZMD01, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	ForsBelExFg	FLOAT, 
	AntalSaldaFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Art_Hier_Lvl_4_Id, Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD01 
select	a110.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a12.Calendar_Month_Id  Calendar_Month_Id,
	a18.Concept_Cd  Concept_Cd,
	a110.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  WJXBFS2
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_PYS_Month_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a110
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a110.Art_Hier_Lvl_2_Seq_Num and 
	a16.Art_Hier_Lvl_4_Seq_Num = a110.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
	join	UV2SemCMNVOUT.PROD_HIER_LVL_3_D	a19
	  on 	(a15.Prod_Hier_Lvl3_Seq_Num = a19.Prod_Hier_Lvl3_Seq_Num)
where	(a18.Concept_Cd in ('WIL')
 and a15.Vendor_Seq_Num in (11181)
 and a19.Prod_Hier_Lvl1_Seq_Num in (4)
 and a12.Calendar_Month_Id in (201308))
group by	a110.Art_Hier_Lvl_4_Id,
	a12.Calendar_Month_Id,
	a18.Concept_Cd,
	a110.Art_Hier_Lvl_2_Id

create volatile table ZZMD02, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	TotForsExMomsAllProd	FLOAT, 
	TotForsBelExBVBerAllaProd	FLOAT, 
	TotInkopsbelExAllaProd	FLOAT)
primary index (Art_Hier_Lvl_4_Id, Calendar_Month_Id, Concept_Cd) on commit preserve rows

;insert into ZZMD02 
select	a18.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a11.Calendar_Month_Id  Calendar_Month_Id,
	a17.Concept_Cd  Concept_Cd,
	sum(a11.Unit_Selling_Price_Amt)  TotForsExMomsAllProd,
	sum(a11.GP_Unit_Selling_Price_Amt)  TotForsBelExBVBerAllaProd,
	sum(a11.Unit_Cost_Amt)  TotInkopsbelExAllaProd
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a18
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num and 
	a15.Art_Hier_Lvl_4_Seq_Num = a18.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a17.Concept_Cd in ('WIL')
 and a11.Calendar_Month_Id in (201308)
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a18.Art_Hier_Lvl_4_Id,
	a11.Calendar_Month_Id,
	a17.Concept_Cd

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT, 
	AntalSaldaHVGFg	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD03 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a18.Concept_Cd  Concept_Cd,
	a17.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Unit_Selling_Price_Amt)  WJXBFS1,
	sum(a11.Unit_Cost_Amt)  WJXBFS2,
	sum(a11.GP_Unit_Selling_Price_Amt)  WJXBFS3,
	sum(a11.Item_Qty)  AntalSaldaHVGFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_PYS_Month_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a18.Concept_Cd in ('WIL')
 and a12.Calendar_Month_Id in (201308))
group by	a12.Calendar_Month_Id,
	a18.Concept_Cd,
	a17.Art_Hier_Lvl_2_Id

create volatile table ZZMD04, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	TotForsExMomsAllProdFg	FLOAT, 
	TotInkopsbelExAllaProdFg	FLOAT, 
	TotForsBelExBVBerAllaProdFg	FLOAT)
primary index (Art_Hier_Lvl_4_Id, Calendar_Month_Id, Concept_Cd) on commit preserve rows

;insert into ZZMD04 
select	a19.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a12.Calendar_Month_Id  Calendar_Month_Id,
	a18.Concept_Cd  Concept_Cd,
	sum(a11.Unit_Selling_Price_Amt)  TotForsExMomsAllProdFg,
	sum(a11.Unit_Cost_Amt)  TotInkopsbelExAllaProdFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  TotForsBelExBVBerAllaProdFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_PYS_Month_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a19
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num and 
	a16.Art_Hier_Lvl_4_Seq_Num = a19.Art_Hier_Lvl_4_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a18.Concept_Cd in ('WIL')
 and a12.Calendar_Month_Id in (201308)
 and a19.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a19.Art_Hier_Lvl_4_Id,
	a12.Calendar_Month_Id,
	a18.Concept_Cd

create volatile table ZZMD05, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSaldaHVG	FLOAT, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Calendar_Month_Id, Concept_Cd, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD05 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a17.Concept_Cd  Concept_Cd,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Item_Qty)  AntalSaldaHVG,
	sum(a11.GP_Unit_Selling_Price_Amt)  WJXBFS1,
	sum(a11.Unit_Cost_Amt)  WJXBFS2,
	sum(a11.Unit_Selling_Price_Amt)  WJXBFS3
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
where	(a17.Concept_Cd in ('WIL')
 and a11.Calendar_Month_Id in (201308))
group by	a11.Calendar_Month_Id,
	a17.Concept_Cd,
	a16.Art_Hier_Lvl_2_Id

select	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id)  Art_Hier_Lvl_4_Id,
	max(a111.Art_Hier_Lvl_4_Desc)  Art_Hier_Lvl_4_Desc,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	max(a110.Concept_Name)  Concept_Name,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a19.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa12.ForsBelExFg)  ForsBelExFg,
	max(pa11.AntalSalda)  AntalSalda,
	max(pa12.AntalSaldaFg)  AntalSaldaFg,
	max(pa15.TotForsExMomsAllProd)  TotForsExMomsAllProd,
	max(pa16.WJXBFS1)  WJXBFS1,
	max(pa17.TotForsExMomsAllProdFg)  TotForsExMomsAllProdFg,
	max(pa16.WJXBFS2)  WJXBFS2,
	max(pa17.TotInkopsbelExAllaProdFg)  TotInkopsbelExAllaProdFg,
	max(pa12.WJXBFS1)  WJXBFS3,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa18.AntalSaldaHVG)  AntalSaldaHVG,
	max(pa16.WJXBFS3)  WJXBFS4,
	max(pa17.TotForsBelExBVBerAllaProdFg)  TotForsBelExBVBerAllaProdFg,
	max(pa18.WJXBFS1)  WJXBFS5,
	max(pa16.AntalSaldaHVGFg)  AntalSaldaHVGFg,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa11.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa15.TotForsBelExBVBerAllaProd)  TotForsBelExBVBerAllaProd,
	max(pa12.WJXBFS2)  WJXBFS6,
	max(pa18.WJXBFS2)  WJXBFS7,
	max(pa11.KampForsBelExBVBer)  KampForsBelExBVBer,
	max(pa18.WJXBFS3)  WJXBFS8,
	max(pa12.InkopsBelExFg)  InkopsBelExFg,
	max(pa12.ForsBelExBVBerFg)  ForsBelExBVBerFg,
	max(pa15.TotInkopsbelExAllaProd)  TotInkopsbelExAllaProd
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Art_Hier_Lvl_4_Id = pa12.Art_Hier_Lvl_4_Id and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd)
	left outer join	ZZMD02	pa15
	  on 	(coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id) = pa15.Art_Hier_Lvl_4_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa15.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa15.Concept_Cd)
	left outer join	ZZMD03	pa16
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa16.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa16.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa16.Concept_Cd)
	left outer join	ZZMD04	pa17
	  on 	(coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id) = pa17.Art_Hier_Lvl_4_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa17.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa17.Concept_Cd)
	left outer join	ZZMD05	pa18
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa18.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa18.Calendar_Month_Id and 
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa18.Concept_Cd)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = a19.Art_Hier_Lvl_2_Id)
	join	UV2SemCMNVOUT.CONCEPT_D	a110
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = a110.Concept_Cd)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a111
	  on 	(coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id) = a111.Art_Hier_Lvl_4_Id)
group by	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd),
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)


SET QUERY_BAND = NONE For Session;

