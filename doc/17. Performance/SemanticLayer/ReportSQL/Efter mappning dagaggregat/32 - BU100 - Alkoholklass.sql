SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU100 - Alkoholklass; ClientUser=Administrator;' For Session;


select	a14.Alcohol_Group_Cd  Alcohol_Group_Cd,
	max(a17.Alcohol_Group_Desc)  Alcohol_Group_Desc,
	a14.Alcohol_Net_Qty  Alcohol_Net_Qty,
	a14.Alcohol_Volume_Pct  Alcohol_Volume_Pct,
	a13.MU_Conversion_Factor  MU_Conversion_Factor,
	max(a13.MU_Conversion_To_UOM)  MU_Conversion_To_UOM,
	a14.Alcohol_Tax_Amt  Alcohol_Tax_Amt,
	a16.Customer_Id  Customer_Id,
	max(a16.Customer_Name)  Customer_Name,
	a15.Store_Id  Store_Id,
	max(a15.Store_Name)  Store_Name,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.Additional_Tax_Amt)  Alkoholskattekr,
	sum(a11.Item_Qty)  AntalSalda
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_ALCOHOL_DETAILS_B	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a15
	  on 	(a11.Store_Seq_Num = a15.Store_Seq_Num)
	join	UV2SemCMNVOUT.CUSTOMER_D	a16
	  on 	(a11.Customer_Seq_Num = a16.Customer_Seq_Num)
	join	UV2SemCMNVOUT.ALCOHOL_GROUP_D	a17
	  on 	(a14.Alcohol_Group_Cd = a17.Alcohol_Group_Cd)
where	(a15.Concept_Cd in ('SNG')
 and a11.Sales_Tran_Dt in (DATE '2013-08-20'))
group by	a14.Alcohol_Group_Cd,
	a14.Alcohol_Net_Qty,
	a14.Alcohol_Volume_Pct,
	a13.MU_Conversion_Factor,
	a14.Alcohol_Tax_Amt,
	a16.Customer_Id,
	a15.Store_Id


SET QUERY_BAND = NONE For Session;
