SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=BU011 - Huvudgruppsanalys; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalBtkMedFsg	INTEGER)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD00 
select	a17.Calendar_Week_Id  Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a17
	  on 	(a11.Tran_Dt = a17.Calendar_Dt)
	join	ITSemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a16.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a18.Concept_Cd in ('WIL')
 and a17.Calendar_Week_Id in (201318)
 and a16.Art_Hier_Lvl_2_Id in ('07'))
group by	a17.Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZSP01 
select	a18.Calendar_Week_Id  Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
where	(a16.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a17.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201318)
 and a16.Art_Hier_Lvl_2_Id in ('07'))
group by	a18.Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZSP02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	SvinnKr	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZSP02 
select	a18.Calendar_Week_Id  Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Total_Line_Value_Amt)  SvinnKr
from	ITSemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a16.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a17.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201318)
 and a16.Art_Hier_Lvl_2_Id in ('07'))
group by	a18.Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZMD03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	SvinnKr	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD03 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	pa11.AntalSalda  AntalSalda,
	pa11.ForsBelEx  ForsBelEx,
	pa11.ForsBelExBVBer  ForsBelExBVBer,
	pa12.SvinnKr  SvinnKr,
	pa11.InkopsBelEx  InkopsBelEx
from	ZZSP01	pa11
	full outer join	ZZSP02	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)

create volatile table ZZMD04, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	AntalSaldaFg	FLOAT, 
	ForsBelExFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD04 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a18.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  WJXBFS2
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.CALENDAR_WEEK_D	a13
	  on 	(a12.Calendar_Week_Id = a13.Calendar_PYS_Week_Id)
	join	ITSemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a19
	  on 	(a11.Store_Seq_Num = a19.Store_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a19.Concept_Cd in ('WIL')
 and a13.Calendar_Week_Id in (201318)
 and a18.Art_Hier_Lvl_2_Id in ('07'))
group by	a13.Calendar_Week_Id,
	a18.Art_Hier_Lvl_2_Id

create volatile table ZZSP05, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	TotForsExMomsAllProd	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP05 
select	a18.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Unit_Selling_Price_Amt)  TotForsExMomsAllProd
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
where	(a17.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201318)
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a18.Calendar_Week_Id

create volatile table ZZSP06, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	TotSvinnKrAllaProd	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP06 
select	a18.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Total_Line_Value_Amt)  TotSvinnKrAllaProd
from	ITSemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a17.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201318)
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a18.Calendar_Week_Id

create volatile table ZZMD07, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	TotForsExMomsAllProd	FLOAT, 
	TotSvinnKrAllaProd	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD07 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	pa11.TotForsExMomsAllProd  TotForsExMomsAllProd,
	pa12.TotSvinnKrAllaProd  TotSvinnKrAllaProd
from	ZZSP05	pa11
	full outer join	ZZSP06	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)

create volatile table ZZMD08, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	TotForsExMomsAllProdFg	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD08 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Unit_Selling_Price_Amt)  TotForsExMomsAllProdFg
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.CALENDAR_WEEK_D	a13
	  on 	(a12.Calendar_Week_Id = a13.Calendar_PYS_Week_Id)
	join	ITSemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a19
	  on 	(a11.Store_Seq_Num = a19.Store_Seq_Num)
where	(a19.Concept_Cd in ('WIL')
 and a13.Calendar_Week_Id in (201318)
 and a18.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a13.Calendar_Week_Id

create volatile table ZZMD09, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	ManuellRab	FLOAT, 
	AntalSaldaManRab	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD09 
select	a18.Calendar_Week_Id  Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Discount_Amt)  ManuellRab,
	sum(a11.Disc_Item_Qty)  AntalSaldaManRab
from	ITSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
where	(a16.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a17.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201318)
 and a16.Art_Hier_Lvl_2_Id in ('07')
 and a11.Discount_Type_Cd in ('MP'))
group by	a18.Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZMD0A, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	TotRabExMomsAllProd	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0A 
select	a18.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Discount_Amt)  TotRabExMomsAllProd
from	ITSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
where	(a17.Concept_Cd in ('WIL')
 and a18.Calendar_Week_Id in (201318)
 and a11.Discount_Type_Cd in ('MP')
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a18.Calendar_Week_Id

select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa16.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa16.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a111.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	max(pa11.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa12.AntalSalda)  AntalSalda,
	max(pa13.AntalSaldaFg)  AntalSaldaFg,
	max(pa12.ForsBelEx)  ForsBelEx,
	max(pa13.ForsBelExFg)  ForsBelExFg,
	max(pa18.TotForsExMomsAllProd)  TotForsExMomsAllProd,
	max(pa19.TotForsExMomsAllProdFg)  TotForsExMomsAllProdFg,
	max(pa14.ManuellRab)  ManuellRab,
	max(pa13.WJXBFS1)  WJXBFS1,
	max(pa12.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa18.TotSvinnKrAllaProd)  TotSvinnKrAllaProd,
	max(pa12.SvinnKr)  SvinnKr,
	max(pa12.InkopsBelEx)  InkopsBelEx,
	max(pa14.AntalSaldaManRab)  AntalSaldaManRab,
	max(pa110.TotRabExMomsAllProd)  TotRabExMomsAllProd,
	max(pa16.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa13.WJXBFS2)  WJXBFS2,
	max(pa16.ForsBelEx)  ForsBelExSvinn,
	max(pa16.KampForsBelExBVBer)  KampForsBelExBVBer,
	max(pa13.InkopsBelExFg)  InkopsBelExFg,
	max(pa13.ForsBelExBVBerFg)  ForsBelExBVBerFg
from	ZZMD00	pa11
	full outer join	ZZMD03	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	full outer join	ZZMD04	pa13
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa13.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id)
	full outer join	ZZMD09	pa14
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id) = pa14.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id)
	full outer join	ZZSP01	pa16
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id) = pa16.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id) = pa16.Calendar_Week_Id)
	left outer join	ZZMD07	pa18
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa16.Calendar_Week_Id) = pa18.Calendar_Week_Id)
	left outer join	ZZMD08	pa19
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa16.Calendar_Week_Id) = pa19.Calendar_Week_Id)
	left outer join	ZZMD0A	pa110
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa16.Calendar_Week_Id) = pa110.Calendar_Week_Id)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a111
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa16.Art_Hier_Lvl_2_Id) = a111.Art_Hier_Lvl_2_Id)
group by	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa16.Calendar_Week_Id),
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa14.Art_Hier_Lvl_2_Id, pa16.Art_Hier_Lvl_2_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZSP01

drop table ZZSP02

drop table ZZMD03

drop table ZZMD04

drop table ZZSP05

drop table ZZSP06

drop table ZZMD07

drop table ZZMD08

drop table ZZMD09

drop table ZZMD0A
