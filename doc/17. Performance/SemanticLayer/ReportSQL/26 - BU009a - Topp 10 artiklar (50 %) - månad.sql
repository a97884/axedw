Report: BU009a - Topp 10 artiklar (50 %) - m�nad
Job: 124581
Report Cache Used: No

Number of Columns Returned:		11
Number of Temp Tables:		4

Total Number of Passes:		15
Number of SQL Passes:		15
Number of Analytical Passes:		0

Tables Accessed:
ARTICLE_D
ARTICLE_HIERARCHY_LVL_2_D
ARTICLE_HIERARCHY_LVL_8_D
CALENDAR_DAY_D
CONCEPT_D
MEASURING_UNIT_D
SALES_TRANSACTION_LINE_F
SALES_TRAN_DISC_LINE_ITEM_F
SCAN_CODE_D
STORE_D


SQL Statements:

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=BU009a - Topp 10 artiklar (50 %) - m�nad; ClientUser=Administrator;' For Session;


create volatile table ZZAM00, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZAM00 
select	a12.Concept_Cd  Concept_Cd,
	a14.Article_Seq_Num  Article_Seq_Num,
	a15.Calendar_Month_Id  Calendar_Month_Id,
	rank () over(partition by a12.Concept_Cd order by sum(a11.Discount_Amt) desc)  WJXBFS1
from	PRSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	PRSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
	join	PRSemCMNVOUT.ARTICLE_D	a16
	  on 	(a14.Article_Seq_Num = a16.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Month_Id in (201304)
 and a11.Discount_Type_Cd in ('MP'))
group by	a12.Concept_Cd,
	a14.Article_Seq_Num,
	a15.Calendar_Month_Id

create volatile table ZZMQ01, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Month_Id	INTEGER)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZMQ01 
select	pa11.Concept_Cd  Concept_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	pa11.Calendar_Month_Id  Calendar_Month_Id
from	ZZAM00	pa11
where	(pa11.WJXBFS1 <=  10.0)

create volatile table ZZMD02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	ManuellRab	FLOAT, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD02 
select	a15.Calendar_Month_Id  Calendar_Month_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	sum(a11.Discount_Amt)  ManuellRab,
	rank () over(partition by a12.Concept_Cd order by sum(a11.Discount_Amt) desc)  WJXBFS1
from	PRSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	PRSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
	join	ZZMQ01	pa16
	  on 	(a12.Concept_Cd = pa16.Concept_Cd and 
	a14.Article_Seq_Num = pa16.Article_Seq_Num and 
	a15.Calendar_Month_Id = pa16.Calendar_Month_Id)
	join	PRSemCMNVOUT.ARTICLE_D	a17
	  on 	(a14.Article_Seq_Num = a17.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Month_Id in (201304)
 and a11.Discount_Type_Cd in ('MP'))
group by	a15.Calendar_Month_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	AntalBtkMedFsg	INTEGER, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Month_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD03 
select	a15.Calendar_Month_Id  Calendar_Month_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx
from	PRSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	PRSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
	join	ZZMQ01	pa16
	  on 	(a12.Concept_Cd = pa16.Concept_Cd and 
	a14.Article_Seq_Num = pa16.Article_Seq_Num and 
	a15.Calendar_Month_Id = pa16.Calendar_Month_Id)
	join	PRSemCMNVOUT.ARTICLE_D	a17
	  on 	(a14.Article_Seq_Num = a17.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a15.Calendar_Month_Id in (201304))
group by	a15.Calendar_Month_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	max(a14.Concept_Name)  Concept_Name,
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num)  Article_Seq_Num,
	max(a15.Article_Id)  Article_Id,
	max(a15.Article_Desc)  Article_Desc,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa11.ManuellRab)  ManuellRab,
	max(pa12.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa12.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.WJXBFS1)  WJXBFS1,
	max(pa12.InkopsBelEx)  InkopsBelEx
from	ZZMD02	pa11
	full outer join	ZZMD03	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd)
	join	PRSemCMNVOUT.CONCEPT_D	a14
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = a14.Concept_Cd)
	join	PRSemCMNVOUT.ARTICLE_D	a15
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = a15.Article_Seq_Num)
group by	coalesce(pa11.Concept_Cd, pa12.Concept_Cd),
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZAM00

drop table ZZMQ01

drop table ZZMD02

drop table ZZMD03


[Analytical engine calculation steps:
	1.  Calculate metric: <Bruttovinst %> in the dataset
	2.  Calculate metric limit(s)
	3.  Perform dynamic aggregation over <M�nad>
	4.  Calculate metric: <Bruttovinst %> at original data level in the view
	5.  Calculate subtotal: <Totalt #1> 
	6.  Calculate subtotal: <Totalt> 
	7.  Calculate metric: <Bruttovinst %> at subtotal levels in the view
	8.  Perform cross-tabbing
]
