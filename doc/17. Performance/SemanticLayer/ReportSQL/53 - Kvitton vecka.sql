
SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=53 - Kvitton vecka; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Store_Department_Id	VARCHAR(50), 
	Store_Id	INTEGER, 
	AntKv	FLOAT, 
	AntKvEjMedl	FLOAT, 
	AntKvMedl	FLOAT)
primary index (Calendar_Week_Id, Store_Department_Id, Store_Id) on commit preserve rows

;insert into ZZMD00 
select	a14.Calendar_Week_Id  Calendar_Week_Id,
	a13.Store_Department_Id  Store_Department_Id,
	a12.Store_Id  Store_Id,
	sum(a11.Receipt_Cnt)  AntKv,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Receipt_Cnt else NULL end))  AntKvEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Receipt_Cnt else NULL end))  AntKvMedl
from	ITSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_DEPARTMENT_D	a13
	  on 	(a11.Store_Department_Seq_Num = a13.Store_Department_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Tran_Dt = a14.Calendar_Dt)
where	(a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a12.Store_Type_Cd in ('CORP')
 and a14.Calendar_Week_Id in (201318)
 and a13.Store_Department_Id not in ('__Prestore__'))
group by	a14.Calendar_Week_Id,
	a13.Store_Department_Id,
	a12.Store_Id

select	pa13.Store_Id  Store_Id,
	max(a14.Store_Name)  Store_Name,
	pa13.Store_Department_Id  Store_Department_Id,
	max(a15.Store_Department_Name)  Store_Department_Name,
	pa13.Calendar_Week_Id  Calendar_Week_Id,
	max(pa13.AntKv)  AntKv,
	max(pa13.AntKvEjMedl)  AntKvEjMedl,
	max(pa13.AntKvMedl)  AntKvMedl
from	ZZMD00	pa13
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(pa13.Store_Id = a14.Store_Id)
	join	ITSemCMNVOUT.STORE_DEPARTMENT_D	a15
	  on 	(pa13.Store_Department_Id = a15.Store_Department_Id)
group by	pa13.Store_Id,
	pa13.Store_Department_Id,
	pa13.Calendar_Week_Id


SET QUERY_BAND = NONE For Session;


drop table ZZMD00