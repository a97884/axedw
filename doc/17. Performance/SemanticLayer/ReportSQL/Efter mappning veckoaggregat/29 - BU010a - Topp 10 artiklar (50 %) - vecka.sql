SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU010a - Topp 10 artiklar (50 %) - vecka; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-15'
group by	a11.Calendar_Week_Id

create volatile table ZZAM01, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Week_Id) on commit preserve rows

;insert into ZZAM01 
select	a13.Concept_Cd  Concept_Cd,
	a15.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Week_Id  Calendar_Week_Id,
	rank () over(partition by a13.Concept_Cd order by sum(a11.Discount_Amt) desc)  WJXBFS1
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_WEEK_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a13.Concept_Cd in ('WIL')
 and a11.Discount_Type_Cd in ('MP'))
group by	a13.Concept_Cd,
	a15.Article_Seq_Num,
	a11.Calendar_Week_Id

create volatile table ZZMQ02, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Article_Seq_Num	INTEGER, 
	Calendar_Week_Id	INTEGER)
primary index (Concept_Cd, Article_Seq_Num, Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ02 
select	pa11.Concept_Cd  Concept_Cd,
	pa11.Article_Seq_Num  Article_Seq_Num,
	pa11.Calendar_Week_Id  Calendar_Week_Id
from	ZZAM01	pa11
where	(pa11.WJXBFS1 <=  10.0)

create volatile table ZZMD03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	ManuellRab	FLOAT, 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD03 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	sum(a11.Discount_Amt)  ManuellRab,
	rank () over(partition by a12.Concept_Cd order by sum(a11.Discount_Amt) desc)  WJXBFS1
from	UV2SemCMNVOUT.SALES_TRAN_D_L_ITEM_WEEK_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ZZMQ02	pa15
	  on 	(a11.Calendar_Week_Id = pa15.Calendar_Week_Id and 
	a12.Concept_Cd = pa15.Concept_Cd and 
	a14.Article_Seq_Num = pa15.Article_Seq_Num)
	join	ZZMQ00	pa16
	  on 	(a11.Calendar_Week_Id = pa16.Calendar_Week_Id)
	join	UV2SemCMNVOUT.ARTICLE_D	a17
	  on 	(a14.Article_Seq_Num = a17.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL')
 and a11.Discount_Type_Cd in ('MP'))
group by	a11.Calendar_Week_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

create volatile table ZZMD04, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Article_Seq_Num	INTEGER, 
	Concept_Cd	CHAR(3), 
	AntalBtkMedFsg	INTEGER, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Week_Id, Article_Seq_Num, Concept_Cd) on commit preserve rows

;insert into ZZMD04 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	a12.Concept_Cd  Concept_Cd,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx
from	UV2SemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ZZMQ02	pa15
	  on 	(a11.Calendar_Week_Id = pa15.Calendar_Week_Id and 
	a12.Concept_Cd = pa15.Concept_Cd and 
	a14.Article_Seq_Num = pa15.Article_Seq_Num)
	join	ZZMQ00	pa16
	  on 	(a11.Calendar_Week_Id = pa16.Calendar_Week_Id)
	join	UV2SemCMNVOUT.ARTICLE_D	a17
	  on 	(a14.Article_Seq_Num = a17.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a19
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num)
where	(a19.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Concept_Cd in ('WIL'))
group by	a11.Calendar_Week_Id,
	a14.Article_Seq_Num,
	a12.Concept_Cd

select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	max(a14.Concept_Name)  Concept_Name,
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num)  Article_Seq_Num,
	max(a15.Article_Id)  Article_Id,
	max(a15.Article_Desc)  Article_Desc,
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	max(pa11.ManuellRab)  ManuellRab,
	max(pa12.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa12.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa12.InkopsBelEx)  InkopsBelEx,
	max(pa11.WJXBFS1)  WJXBFS1
from	ZZMD03	pa11
	full outer join	ZZMD04	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd)
	join	UV2SemCMNVOUT.CONCEPT_D	a14
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = a14.Concept_Cd)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = a15.Article_Seq_Num)
group by	coalesce(pa11.Concept_Cd, pa12.Concept_Cd),
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num),
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMQ00

drop table ZZAM01

drop table ZZMQ02

drop table ZZMD03

drop table ZZMD04

