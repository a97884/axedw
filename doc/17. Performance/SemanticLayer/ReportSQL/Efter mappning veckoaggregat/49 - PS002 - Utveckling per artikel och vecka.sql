SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=PS002 - Utveckling per artikel och vecka; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-15'
group by	a11.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	WJXBFS1	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	AntalBtkMedFsg	INTEGER)
primary index (Calendar_Week_Id, Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD01 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((a11.Unit_Selling_Price_Amt + a11.Tax_Amt))  WJXBFS1,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
where	(a13.Concept_Cd in ('WIL')
 and a15.Article_Seq_Num in (588))
group by	a11.Calendar_Week_Id,
	a13.Concept_Cd,
	a11.Scan_Code_Seq_Num

create volatile table ZZMD02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	AntalSaldaFgVka	FLOAT)
primary index (Calendar_Week_Id, Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD02 
select	a12.Calendar_Week_Id  Calendar_Week_Id,
	a14.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	sum(a11.Item_Qty)  AntalSaldaFgVka
from	UV2SemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_Previous_Week_Id)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a15
	  on 	(a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a16
	  on 	(a15.Measuring_Unit_Seq_Num = a16.Measuring_Unit_Seq_Num)
where	(a14.Concept_Cd in ('WIL')
 and a16.Article_Seq_Num in (588))
group by	a12.Calendar_Week_Id,
	a14.Concept_Cd,
	a11.Scan_Code_Seq_Num

create volatile table ZZMD03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	AntalSaldaFg	FLOAT, 
	ForsBelExFg	FLOAT)
primary index (Calendar_Week_Id, Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD03 
select	a12.Calendar_Week_Id  Calendar_Week_Id,
	a14.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a15
	  on 	(a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a16
	  on 	(a15.Measuring_Unit_Seq_Num = a16.Measuring_Unit_Seq_Num)
where	(a14.Concept_Cd in ('WIL')
 and a16.Article_Seq_Num in (588))
group by	a12.Calendar_Week_Id,
	a14.Concept_Cd,
	a11.Scan_Code_Seq_Num

select	coalesce(pa12.Scan_Code_Seq_Num, pa13.Scan_Code_Seq_Num, pa14.Scan_Code_Seq_Num)  Scan_Code_Seq_Num,
	max(a15.Scan_Cd)  Scan_Cd,
	a16.Article_Seq_Num  Article_Seq_Num,
	max(a18.Article_Id)  Article_Id,
	max(a18.Article_Desc)  Article_Desc,
	coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd)  Concept_Cd,
	max(a17.Concept_Name)  Concept_Name,
	coalesce(pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id)  Calendar_Week_Id,
	max(pa12.AntalSalda)  AntalSalda,
	max(pa12.ForsBelEx)  ForsBelEx,
	max(pa12.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa13.AntalSaldaFgVka)  AntalSaldaFgVka,
	max(pa12.WJXBFS1)  WJXBFS1,
	max(pa12.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa12.InkopsBelEx)  InkopsBelEx,
	max(pa14.AntalSaldaFg)  AntalSaldaFg,
	max(pa14.ForsBelExFg)  ForsBelExFg
from	ZZMD01	pa12
	full outer join	ZZMD02	pa13
	  on 	(pa12.Calendar_Week_Id = pa13.Calendar_Week_Id and 
	pa12.Concept_Cd = pa13.Concept_Cd and 
	pa12.Scan_Code_Seq_Num = pa13.Scan_Code_Seq_Num)
	full outer join	ZZMD03	pa14
	  on 	(coalesce(pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id and 
	coalesce(pa12.Concept_Cd, pa13.Concept_Cd) = pa14.Concept_Cd and 
	coalesce(pa12.Scan_Code_Seq_Num, pa13.Scan_Code_Seq_Num) = pa14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a15
	  on 	(coalesce(pa12.Scan_Code_Seq_Num, pa13.Scan_Code_Seq_Num, pa14.Scan_Code_Seq_Num) = a15.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a16
	  on 	(a15.Measuring_Unit_Seq_Num = a16.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.CONCEPT_D	a17
	  on 	(coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd) = a17.Concept_Cd)
	join	UV2SemCMNVOUT.ARTICLE_D	a18
	  on 	(a16.Article_Seq_Num = a18.Article_Seq_Num)
group by	coalesce(pa12.Scan_Code_Seq_Num, pa13.Scan_Code_Seq_Num, pa14.Scan_Code_Seq_Num),
	a16.Article_Seq_Num,
	coalesce(pa12.Concept_Cd, pa13.Concept_Cd, pa14.Concept_Cd),
	coalesce(pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMQ00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03

