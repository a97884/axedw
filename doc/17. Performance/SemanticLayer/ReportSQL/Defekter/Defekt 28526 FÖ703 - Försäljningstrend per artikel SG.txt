Rapport: F�703 - F�rs�ljningstrend per artikel
Jobb: 125034
Anv�nt rapportcache: Nej

Antal returnerade kolumner:		23
Antal tempor�ra tabeller:		2

Totalt antal passager:		11
Antal SQL-passager:		11
Antal analyspassager:		0

�ppnade tabeller:
ARTICLE_D
CALENDAR_DAY_D
CONCEPT_D
MEASURING_UNIT_D
SALES_TRAN_LINE_WEEK_F
SCAN_CODE_D
STORE_D
STORE_TYPE_D


Styrning:

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=F�703 - F�rs�ljningstrend per artikel; ClientUser=Administrator;' For Session;


Set Session Account='$M_APPMSTRA&S&D&H' for session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	PRSemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt between DATE '2012-11-28' and DATE '2014-01-15'
group by	a11.Calendar_Week_Id

COLLECT STATISTICS ON ZZMQ00 INDEX (Calendar_Week_Id)

create volatile table ZZMD01, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	Scan_Code_Seq_Num	INTEGER, 
	AntalSalda	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	Moms	FLOAT, 
	AntalBtkMedFsg	INTEGER, 
	KampAntalSalda	FLOAT, 
	KampForsBelEx	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Store_Type_Cd, Calendar_Week_Id, Concept_Cd, Scan_Code_Seq_Num) on commit preserve rows

;insert into ZZMD01 
select	a13.Store_Type_Cd  Store_Type_Cd,
	a11.Calendar_Week_Id  Calendar_Week_Id,
	a13.Concept_Cd  Concept_Cd,
	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum(a11.Tax_Amt)  Moms,
	count(distinct a11.Store_Seq_Num)  AntalBtkMedFsg,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Item_Qty else NULL end))  KampAntalSalda,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Selling_Price_Amt else NULL end))  KampForsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	PRSemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	PRSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	PRSemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
where	(a13.Concept_Cd in ('SNG')
 and a15.Article_Id = '100996317')
group by	a13.Store_Type_Cd,
	a11.Calendar_Week_Id,
	a13.Concept_Cd,
	a11.Scan_Code_Seq_Num

COLLECT STATISTICS ON ZZMD01 INDEX (Store_Type_Cd, Calendar_Week_Id, Concept_Cd, Scan_Code_Seq_Num);

select	pa13.Calendar_Week_Id  Calendar_Week_Id,
	a14.Article_Seq_Num  Article_Seq_Num,
	max(a18.Article_Id)  Article_Id,
	max(a18.Article_Desc)  Article_Desc,
	a14.Measuring_Unit_Seq_Num  Measuring_Unit_Seq_Num,
	max(a17.MU_Id)  MU_Id,
	max(a17.MU_Desc)  MU_Desc,
	pa13.Scan_Code_Seq_Num  Scan_Code_Seq_Num,
	max(a14.Scan_Cd)  Scan_Cd,
	pa13.Concept_Cd  Concept_Cd,
	max(a15.Concept_Name)  Concept_Name,
	pa13.Store_Type_Cd  Store_Type_Cd,
	max(a16.Store_Type_Desc)  Store_Type_Desc,
	max(pa13.AntalSalda)  AntalSalda,
	max(pa13.ForsBelEx)  ForsBelEx,
	max(pa13.KampAntalSalda)  KampAntalSalda,
	max(pa13.KampForsBelEx)  KampForsBelEx,
	max(pa13.AntalBtkMedFsg)  AntalBtkMedFsg,
	max(pa13.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa13.InkopsBelEx)  InkopsBelEx,
	max(pa13.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa13.KampForsBelExBVBer)  KampForsBelExBVBer,
	max(pa13.Moms)  Moms
from	ZZMD01	pa13
	join	PRSemCMNVOUT.SCAN_CODE_D	a14
	  on 	(pa13.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	PRSemCMNVOUT.CONCEPT_D	a15
	  on 	(pa13.Concept_Cd = a15.Concept_Cd)
	join	PRSemCMNVOUT.STORE_TYPE_D	a16
	  on 	(pa13.Store_Type_Cd = a16.Store_Type_Cd)
	join	PRSemCMNVOUT.MEASURING_UNIT_D	a17
	  on 	(a14.Measuring_Unit_Seq_Num = a17.Measuring_Unit_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_D	a18
	  on 	(a14.Article_Seq_Num = a18.Article_Seq_Num)
group by	pa13.Calendar_Week_Id,
	a14.Article_Seq_Num,
	a14.Measuring_Unit_Seq_Num,
	pa13.Scan_Code_Seq_Num,
	pa13.Concept_Cd,
	pa13.Store_Type_Cd


drop table ZZMQ00

drop table ZZMD01


[Ber�kningssteg i analysmotorn:
	1.  Ber�kna m�ttenhet: <Kamp - Bruttovinst kr> i dataupps�ttningen
	2.  Ber�kna m�ttenhet: <F�rs belopp inkl moms> i dataupps�ttningen
	3.  Ber�kna m�ttenhet: <Bruttovinst %> i dataupps�ttningen
	4.  Ber�kna m�ttenhet: <Snittpris exkl moms> i dataupps�ttningen
	5.  Ber�kna m�ttenhet: <Bruttovinst kr> i dataupps�ttningen
	6.  Ber�kna m�ttenhet: <Kamp - Bruttovinst %> i dataupps�ttningen
	7.  Ber�kna m�ttenhet: <Kamp - Bruttovinst kr> p� den ursprungliga dataniv�n i vyn
	8.  Ber�kna m�ttenhet: <Bruttovinst kr> p� den ursprungliga dataniv�n i vyn
	9.  Ber�kna m�ttenhet: <Snittpris exkl moms> p� den ursprungliga dataniv�n i vyn
	10.  Ber�kna m�ttenhet: <Kamp - Bruttovinst %> p� den ursprungliga dataniv�n i vyn
	11.  Ber�kna m�ttenhet: <Bruttovinst %> p� den ursprungliga dataniv�n i vyn
	12.  Ber�kna m�ttenhet: <Snitt inpris exkl moms> p� den ursprungliga dataniv�n i vyn
	13.  Ber�kna m�ttenhet: <Snitt konsumentpris inkl moms> p� den ursprungliga dataniv�n i vyn
	14.  Ber�kna m�ttenhet: <Snitt kampanjpris exkl moms> p� den ursprungliga dataniv�n i vyn
	15.  Ber�kna delsumma: <Totalt> 
	16.  Ber�kna m�ttenhet: <Kamp - Bruttovinst kr> p� delsummeniv� i vyn
	17.  Ber�kna m�ttenhet: <Bruttovinst kr> p� delsummeniv� i vyn
	18.  Ber�kna m�ttenhet: <Snittpris exkl moms> p� delsummeniv� i vyn
	19.  Ber�kna m�ttenhet: <Kamp - Bruttovinst %> p� delsummeniv� i vyn
	20.  Ber�kna m�ttenhet: <Bruttovinst %> p� delsummeniv� i vyn
	21.  Ber�kna m�ttenhet: <Snitt inpris exkl moms> p� delsummeniv� i vyn
	22.  Ber�kna m�ttenhet: <Snitt konsumentpris inkl moms> p� delsummeniv� i vyn
	23.  Ber�kna m�ttenhet: <Snitt kampanjpris exkl moms> p� delsummeniv� i vyn
	24.  Utf�r korstabulering
]
