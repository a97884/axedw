Report: BU205 - AO Veckorapport - saldokontroller, svinn och minskade orderrader
Job: 182672
Report Cache Used: No

Number of Columns Returned:		13
Number of Temp Tables:		6

Total Number of Passes:		21
Number of SQL Passes:		21
Number of Analytical Passes:		0

Tables Accessed:
ARTICLE_D
ARTICLE_HIERARCHY_LVL_2_D
ARTICLE_HIERARCHY_LVL_8_D
ARTICLE_KNOWN_LOSS_F
BALANCE_CONTROL_TRANSACTION_F
CALENDAR_DAY_D
ORDER_CONTROL_F
PERPETUAL_INVENTORY_F
RETAIL_REGION_D
STOCK_BALANCE_REQUEST_CNTRL_F
STORE_D


SQL Statements:

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=BU205 - AO Veckorapport - saldokontroller, svinn och minskade orderrader; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntDagSK	INTEGER)
primary index (Calendar_Week_Id, Store_Id) on commit preserve rows

;insert into ZZSP00 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a12.Store_Id  Store_Id,
	count(distinct a11.Inv_Tran_Event_Dt)  AntDagSK
from	PRSemCMNVOUT.BALANCE_CONTROL_TRANSACTION_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Inv_Tran_Event_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201324)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2', 'SNG', 'HEM', 'HEA'))
group by	a13.Calendar_Week_Id,
	a12.Store_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntDagSVKtrl	INTEGER)
primary index (Calendar_Week_Id, Store_Id) on commit preserve rows

;insert into ZZSP01 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a12.Store_Id  Store_Id,
	count(distinct a11.Adjustment_Dt)  AntDagSVKtrl
from	PRSemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Adjustment_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201324)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2', 'SNG', 'HEM', 'HEA'))
group by	a13.Calendar_Week_Id,
	a12.Store_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntDagSK	INTEGER, 
	AntDagSVKtrl	INTEGER)
primary index (Calendar_Week_Id, Store_Id) on commit preserve rows

;insert into ZZMD02 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.AntDagSK  AntDagSK,
	pa12.AntDagSVKtrl  AntDagSVKtrl
from	ZZSP00	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	a16.Calendar_Week_Id  Calendar_Week_Id,
	a15.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	(sum(a11.Performed_Cntrl_Cnt) + sum(a11.Spontaneous_Cntrl_Cnt))  WJXBFS1
from	PRSemCMNVOUT.STOCK_BALANCE_REQUEST_CNTRL_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_D	a13
	  on 	(a11.Article_Seq_Num = a13.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a14
	  on 	(a13.Art_Hier_Lvl_8_Seq_Num = a14.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a15
	  on 	(a14.Art_Hier_Lvl_2_Seq_Num = a15.Art_Hier_Lvl_2_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a16
	  on 	(a11.Stock_Balance_Request_Dt = a16.Calendar_Dt)
where	(a16.Calendar_Week_Id in (201324)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2', 'SNG', 'HEM', 'HEA')
 and a11.AO_Ind in (1))
group by	a16.Calendar_Week_Id,
	a15.Art_Hier_Lvl_2_Id,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD04, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntArtIAoSort	INTEGER)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD04 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	count(distinct a11.Article_Seq_Num)  AntArtIAoSort
from	PRSemCMNVOUT.PERPETUAL_INVENTORY_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Perpetual_Inv_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.ARTICLE_D	a14
	  on 	(a11.Article_Seq_Num = a14.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
where	(a13.Calendar_Week_Id in (201324)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2', 'SNG', 'HEM', 'HEA')
 and a11.AO_Ind in (1))
group by	a13.Calendar_Week_Id,
	a16.Art_Hier_Lvl_2_Id,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD05, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntalMinskadeOR	FLOAT, 
	AntalOR	FLOAT)
primary index (Calendar_Week_Id, Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD05 
select	a16.Calendar_Week_Id  Calendar_Week_Id,
	a15.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Decreased_Cnt)  AntalMinskadeOR,
	sum(a11.Inv_Request_Cnt)  AntalOR
from	PRSemCMNVOUT.ORDER_CONTROL_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_D	a13
	  on 	(a11.Article_Seq_Num = a13.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a14
	  on 	(a13.Art_Hier_Lvl_8_Seq_Num = a14.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a15
	  on 	(a14.Art_Hier_Lvl_2_Seq_Num = a15.Art_Hier_Lvl_2_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a16
	  on 	(a11.Order_Dt = a16.Calendar_Dt)
where	(a16.Calendar_Week_Id in (201324)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2', 'SNG', 'HEM', 'HEA'))
group by	a16.Calendar_Week_Id,
	a15.Art_Hier_Lvl_2_Id,
	a12.Retail_Region_Cd,
	a12.Store_Id

select	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd)  Retail_Region_Cd,
	max(a16.Retail_Region_Name)  Retail_Region_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id)  Store_Id,
	max(a15.Store_Name)  Store_Name,
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a17.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id)  Calendar_Week_Id,
	max(pa14.AntDagSK)  AntDagSK,
	max(pa14.AntDagSVKtrl)  AntDagSVKtrl,
	max(pa11.WJXBFS1)  WJXBFS1,
	max(pa12.AntArtIAoSort)  AntArtIAoSort,
	max(pa13.AntalMinskadeOR)  AntalMinskadeOR,
	max(pa13.AntalOR)  AntalOR
from	ZZMD03	pa11
	full outer join	ZZMD04	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZMD05	pa13
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa13.Art_Hier_Lvl_2_Id and 
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = pa13.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	left outer join	ZZMD02	pa14
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
	join	PRSemCMNVOUT.STORE_D	a15
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = a15.Store_Id)
	join	PRSemCMNVOUT.RETAIL_REGION_D	a16
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd) = a16.Retail_Region_Cd)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id) = a17.Art_Hier_Lvl_2_Id)
group by	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id),
	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id),
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZSP00

drop table ZZSP01

drop table ZZMD02

drop table ZZMD03

drop table ZZMD04

drop table ZZMD05

