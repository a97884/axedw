SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=54 - BU300 Kundf�rs�ljning; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Customer_Id	VARCHAR(50), 
	Store_Id	INTEGER, 
	Industry_Level_2_Cd	VARCHAR(10), 
	Industry_Level_1_Cd	VARCHAR(10), 
	AntKv	FLOAT)
primary index (Customer_Id, Store_Id, Industry_Level_2_Cd, Industry_Level_1_Cd) on commit preserve rows

;insert into ZZMD00 
select	a12.Customer_Id  Customer_Id,
	a13.Store_Id  Store_Id,
	a12.Industry_Level_2_Cd  Industry_Level_2_Cd,
	a12.Industry_Level_1_Cd  Industry_Level_1_Cd,
	sum(a11.Receipt_Cnt)  AntKv
from	ITSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	ITSemCMNVOUT.CUSTOMER_D	a12
	  on 	(a11.Customer_Seq_Num = a12.Customer_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_DEPARTMENT_D	a14
	  on 	(a11.Store_Department_Seq_Num = a14.Store_Department_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Tran_Dt = a15.Calendar_Dt)
where	(a12.Customer_Id in ('205513', '217185', '220036', '227530', '167095')
 and a13.Concept_Cd in ('SNG')
 and a15.Calendar_Month_Id in (201304)
 and a14.Store_Department_Id not in ('__Prestore__'))
group by	a12.Customer_Id,
	a13.Store_Id,
	a12.Industry_Level_2_Cd,
	a12.Industry_Level_1_Cd

create volatile table ZZMD01, no fallback, no log(
	Customer_Id	VARCHAR(50), 
	Store_Id	INTEGER, 
	Industry_Level_2_Cd	VARCHAR(10), 
	Industry_Level_1_Cd	VARCHAR(10), 
	ForsBelEx	FLOAT)
primary index (Customer_Id, Store_Id, Industry_Level_2_Cd, Industry_Level_1_Cd) on commit preserve rows

;insert into ZZMD01 
select	a12.Customer_Id  Customer_Id,
	a13.Store_Id  Store_Id,
	a12.Industry_Level_2_Cd  Industry_Level_2_Cd,
	a12.Industry_Level_1_Cd  Industry_Level_1_Cd,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.CUSTOMER_D	a12
	  on 	(a11.Customer_Seq_Num = a12.Customer_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Tran_Dt = a14.Calendar_Dt)
where	(a12.Customer_Id in ('205513', '217185', '220036', '227530', '167095')
 and a13.Concept_Cd in ('SNG')
 and a14.Calendar_Month_Id in (201304))
group by	a12.Customer_Id,
	a13.Store_Id,
	a12.Industry_Level_2_Cd,
	a12.Industry_Level_1_Cd

create volatile table ZZMD02, no fallback, no log(
	Customer_Id	VARCHAR(50), 
	Store_Id	INTEGER, 
	Industry_Level_2_Cd	VARCHAR(10), 
	Industry_Level_1_Cd	VARCHAR(10), 
	KundRab	FLOAT)
primary index (Customer_Id, Store_Id, Industry_Level_2_Cd, Industry_Level_1_Cd) on commit preserve rows

;insert into ZZMD02 
select	a12.Customer_Id  Customer_Id,
	a13.Store_Id  Store_Id,
	a12.Industry_Level_2_Cd  Industry_Level_2_Cd,
	a12.Industry_Level_1_Cd  Industry_Level_1_Cd,
	sum(a11.Discount_Amt)  KundRab
from	ITSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F	a11
	join	ITSemCMNVOUT.CUSTOMER_D	a12
	  on 	(a11.Customer_Seq_Num = a12.Customer_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Tran_Dt = a14.Calendar_Dt)
where	(a12.Customer_Id in ('205513', '217185', '220036', '227530', '167095')
 and a13.Concept_Cd in ('SNG')
 and a14.Calendar_Month_Id in (201304)
 and a11.Discount_Type_Cd in ('CA'))
group by	a12.Customer_Id,
	a13.Store_Id,
	a12.Industry_Level_2_Cd,
	a12.Industry_Level_1_Cd

select	coalesce(pa11.Industry_Level_1_Cd, pa12.Industry_Level_1_Cd, pa13.Industry_Level_1_Cd)  Industry_Level_1_Cd,
	max(a14.Industry_Desc)  Industry_Desc,
	coalesce(pa11.Industry_Level_2_Cd, pa12.Industry_Level_2_Cd, pa13.Industry_Level_2_Cd)  Industry_Level_2_Cd,
	max(a15.Industry_Desc)  Industry_Desc0,
	coalesce(pa11.Customer_Id, pa12.Customer_Id, pa13.Customer_Id)  Customer_Id,
	max(a17.Customer_Name)  Customer_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id)  Store_Id,
	max(a16.Store_Name)  Store_Name,
	max(pa11.AntKv)  AntKv,
	max(pa12.ForsBelEx)  ForsBelEx,
	max(pa13.KundRab)  KundRab
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Customer_Id = pa12.Customer_Id and 
	pa11.Industry_Level_1_Cd = pa12.Industry_Level_1_Cd and 
	pa11.Industry_Level_2_Cd = pa12.Industry_Level_2_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZMD02	pa13
	  on 	(coalesce(pa11.Customer_Id, pa12.Customer_Id) = pa13.Customer_Id and 
	coalesce(pa11.Industry_Level_1_Cd, pa12.Industry_Level_1_Cd) = pa13.Industry_Level_1_Cd and 
	coalesce(pa11.Industry_Level_2_Cd, pa12.Industry_Level_2_Cd) = pa13.Industry_Level_2_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	join	ITSemCMNVOUT.INDUSTRY_D	a14
	  on 	(coalesce(pa11.Industry_Level_1_Cd, pa12.Industry_Level_1_Cd, pa13.Industry_Level_1_Cd) = a14.Industry_Cd)
	join	ITSemCMNVOUT.INDUSTRY_D	a15
	  on 	(coalesce(pa11.Industry_Level_2_Cd, pa12.Industry_Level_2_Cd, pa13.Industry_Level_2_Cd) = a15.Industry_Cd)
	join	ITSemCMNVOUT.STORE_D	a16
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = a16.Store_Id)
	join	ITSemCMNVOUT.CUSTOMER_D	a17
	  on 	(coalesce(pa11.Customer_Id, pa12.Customer_Id, pa13.Customer_Id) = a17.Customer_Id)
group by	coalesce(pa11.Industry_Level_1_Cd, pa12.Industry_Level_1_Cd, pa13.Industry_Level_1_Cd),
	coalesce(pa11.Industry_Level_2_Cd, pa12.Industry_Level_2_Cd, pa13.Industry_Level_2_Cd),
	coalesce(pa11.Customer_Id, pa12.Customer_Id, pa13.Customer_Id),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02
