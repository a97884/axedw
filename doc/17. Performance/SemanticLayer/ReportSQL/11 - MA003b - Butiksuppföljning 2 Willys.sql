SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=MA003b - Butiksuppföljning 2; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntRekrKontakt	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP00 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontakt
from	ITSemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a13.Calendar_Week_Id in (201318))
group by	a13.Calendar_Week_Id,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExMedl	FLOAT, 
	ForsBelInklMedl	FLOAT, 
	GODWFLAG5_1	INTEGER, 
	ForsBelInklEjMedl	FLOAT, 
	GODWFLAG7_1	INTEGER, 
	AntalSaldaStEjMedl	FLOAT, 
	GODWFLAG8_1	INTEGER, 
	AntSaldaViktartEjMedl	FLOAT, 
	GODWFLAG9_1	INTEGER, 
	AntalSaldaStMedl	FLOAT, 
	GODWFLAGb_1	INTEGER, 
	AntSaldaViktartMedl	FLOAT, 
	GODWFLAGc_1	INTEGER)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP01 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExMedl,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then (a11.Unit_Selling_Price_Amt + a11.Tax_Amt) else NULL end))  ForsBelInklMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG5_1,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then (a11.Unit_Selling_Price_Amt + a11.Tax_Amt) else NULL end))  ForsBelInklEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG7_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('UNT')) then 1 else 0 end))  GODWFLAG8_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('WGH')) then 1 else 0 end))  GODWFLAG9_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then 1 else 0 end))  GODWFLAGb_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then 1 else 0 end))  GODWFLAGc_1
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a13.Calendar_Week_Id in (201318))
group by	a13.Calendar_Week_Id,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntRekrKontakt	FLOAT, 
	ForsBelEx	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD02 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.AntRekrKontakt  AntRekrKontakt,
	pa12.ForsBelEx  ForsBelEx
from	ZZSP00	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntRekrKontaktAck	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a14.Retail_Region_Cd  Retail_Region_Cd,
	a14.Store_Id  Store_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontaktAck
from	ITSemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.CALENDAR_WEEK_YTD_D	a13
	  on 	(a12.Calendar_Week_Id = a13.Calendar_Week_YTD_Id)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a14.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a13.Calendar_Week_Id in (201318))
group by	a13.Calendar_Week_Id,
	a14.Retail_Region_Cd,
	a14.Store_Id

create volatile table ZZOP04, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP04 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.ForsBelExMedl  WJXBFS1,
	pa01.ForsBelInklMedl  WJXBFS2
from	ZZSP01	pa01
where	pa01.GODWFLAG5_1 = 1

create volatile table ZZMD05, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	AntKvEjMedl	FLOAT, 
	AntKvMedl	FLOAT)
primary index (Calendar_Week_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD05 
select	a14.Calendar_Week_Id  Calendar_Week_Id,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Receipt_Cnt else NULL end))  AntKvEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Receipt_Cnt else NULL end))  AntKvMedl
from	ITSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_DEPARTMENT_D	a13
	  on 	(a11.Store_Department_Seq_Num = a13.Store_Department_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Tran_Dt = a14.Calendar_Dt)
where	(a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a14.Calendar_Week_Id in (201318)
 and a13.Store_Department_Id not in ('__Prestore__')
 and (a11.Contact_Account_Seq_Num = -1
 or a11.Contact_Account_Seq_Num <> -1))
group by	a14.Calendar_Week_Id,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZOP06, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP06 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.ForsBelInklEjMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAG7_1 = 1

create volatile table ZZOP07, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP07 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntalSaldaStEjMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAG8_1 = 1

create volatile table ZZOP08, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP08 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntSaldaViktartEjMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAG9_1 = 1

create volatile table ZZOP09, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP09 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntalSaldaStMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAGb_1 = 1

create volatile table ZZOP0A, no fallback, no log(
	Store_Id	INTEGER, 
	Retail_Region_Cd	CHAR(6), 
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Id, Retail_Region_Cd, Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0A 
select	pa01.Store_Id  Store_Id,
	pa01.Retail_Region_Cd  Retail_Region_Cd,
	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.AntSaldaViktartMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAGc_1 = 1

select	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd, pa16.Retail_Region_Cd, pa17.Retail_Region_Cd, pa19.Retail_Region_Cd, pa110.Retail_Region_Cd)  Retail_Region_Cd,
	max(a112.Retail_Region_Name)  Retail_Region_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id, pa17.Store_Id, pa19.Store_Id, pa110.Store_Id)  Store_Id,
	max(a111.Store_Name)  Store_Name,
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa19.Calendar_Week_Id, pa110.Calendar_Week_Id)  Calendar_Week_Id,
	max(pa11.AntRekrKontakt)  AntRekrKontakt,
	max(pa12.AntRekrKontaktAck)  AntRekrKontaktAck,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa13.WJXBFS1)  ForsBelExMedl,
	max(pa14.AntKvEjMedl)  AntKvEjMedl,
	max(pa15.WJXBFS1)  ForsBelInklEjMedl,
	(ZEROIFNULL(max(pa16.WJXBFS1)) + ZEROIFNULL(max(pa17.WJXBFS1)))  AntStreckkodEjMedl,
	max(pa14.AntKvMedl)  AntKvMedl,
	(ZEROIFNULL(max(pa19.WJXBFS1)) + ZEROIFNULL(max(pa110.WJXBFS1)))  AntStreckkodMedl,
	max(pa13.WJXBFS2)  ForsBelInklMedl
from	ZZMD02	pa11
	full outer join	ZZMD03	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZOP04	pa13
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = pa13.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	full outer join	ZZMD05	pa14
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa14.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd) = pa14.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
	full outer join	ZZOP06	pa15
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id) = pa15.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd) = pa15.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id) = pa15.Store_Id)
	full outer join	ZZOP07	pa16
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id) = pa16.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd) = pa16.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id) = pa16.Store_Id)
	full outer join	ZZOP08	pa17
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id) = pa17.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd, pa16.Retail_Region_Cd) = pa17.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id) = pa17.Store_Id)
	full outer join	ZZOP09	pa19
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id) = pa19.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd, pa16.Retail_Region_Cd, pa17.Retail_Region_Cd) = pa19.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id, pa17.Store_Id) = pa19.Store_Id)
	full outer join	ZZOP0A	pa110
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa19.Calendar_Week_Id) = pa110.Calendar_Week_Id and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd, pa16.Retail_Region_Cd, pa17.Retail_Region_Cd, pa19.Retail_Region_Cd) = pa110.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id, pa17.Store_Id, pa19.Store_Id) = pa110.Store_Id)
	join	ITSemCMNVOUT.STORE_D	a111
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id, pa17.Store_Id, pa19.Store_Id, pa110.Store_Id) = a111.Store_Id)
	join	ITSemCMNVOUT.RETAIL_REGION_D	a112
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd, pa16.Retail_Region_Cd, pa17.Retail_Region_Cd, pa19.Retail_Region_Cd, pa110.Retail_Region_Cd) = a112.Retail_Region_Cd)
group by	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa14.Retail_Region_Cd, pa15.Retail_Region_Cd, pa16.Retail_Region_Cd, pa17.Retail_Region_Cd, pa19.Retail_Region_Cd, pa110.Retail_Region_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id, pa17.Store_Id, pa19.Store_Id, pa110.Store_Id),
	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa14.Calendar_Week_Id, pa15.Calendar_Week_Id, pa16.Calendar_Week_Id, pa17.Calendar_Week_Id, pa19.Calendar_Week_Id, pa110.Calendar_Week_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZSP00

drop table ZZSP01

drop table ZZMD02

drop table ZZMD03

drop table ZZOP04

drop table ZZMD05

drop table ZZOP06

drop table ZZOP07

drop table ZZOP08

drop table ZZOP09

drop table ZZOP0A

