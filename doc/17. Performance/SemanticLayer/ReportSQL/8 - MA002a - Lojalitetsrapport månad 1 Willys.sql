/* Ny version av Lojalitetsrapport 1 månad, 201304 alla Willys koncept */

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=3 - MA002a - Lojalitetsrapport månad 1; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntMedl	FLOAT, 
	AntNyaMedl	FLOAT, 
	AntAktivMedl	FLOAT, 
	AntPassivMedl	FLOAT, 
	AntForlMedl	FLOAT, 
	AntAteraktMedl	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD00 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a14.Loyalty_Program_Id  Loyalty_Program_Id,
	sum((Case when a11.Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntMedl,
	sum((Case when a11.New_Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntNyaMedl,
	sum((Case when a11.Active_Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntAktivMedl,
	sum((Case when (a11.Active_Member_Ind = 0 and a11.Member_Ind > 0) then a11.Member_Sum_Cnt else NULL end))  AntPassivMedl,
	sum((Case when a11.Lost_Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntForlMedl,
	sum((Case when a11.ReActived_Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntAteraktMedl
from	ITSemCMNVOUT.LOYALTY_MEMBER_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a12.Home_Store_Seq_Num = a13.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a12.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a11.Calendar_Month_Id in (201304)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (a11.Member_Ind > 0
 or a11.New_Member_Ind > 0
 or a11.Active_Member_Ind > 0
 or a11.Lost_Member_Ind > 0
 or a11.ReActived_Member_Ind > 0))
group by	a11.Calendar_Month_Id,
	a14.Loyalty_Program_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntKont	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD01 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a14.Loyalty_Program_Id  Loyalty_Program_Id,
	sum(a11.Contact_Sum_Cnt)  AntKont
from	ITSemCMNVOUT.LOYALTY_CONTACT_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a12.Home_Store_Seq_Num = a13.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a14
	  on 	(a12.Loyalty_Program_Seq_Num = a14.Loyalty_Program_Seq_Num)
where	(a11.Calendar_Month_Id in (201304)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a11.Calendar_Month_Id,
	a14.Loyalty_Program_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntAktiveradMedl	INTEGER)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD02 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a15.Loyalty_Program_Id  Loyalty_Program_Id,
	count(distinct a11.Member_Account_Seq_Num)  AntAktiveradMedl
from	ITSemCMNVOUT.LOY_MEMBER_ACTIVED_STATUS_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Member_Enrollment_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a13.Home_Store_Seq_Num = a14.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a15
	  on 	(a13.Loyalty_Program_Seq_Num = a15.Loyalty_Program_Seq_Num)
where	(a12.Calendar_Month_Id in (201304)
 and a14.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Activated_Member_Ind > 0)
group by	a12.Calendar_Month_Id,
	a15.Loyalty_Program_Id

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntMedlMedKop	INTEGER)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD03 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a16.Loyalty_Program_Id  Loyalty_Program_Id,
	count(distinct a12.Member_Account_Seq_Num)  AntMedlMedKop
from	ITSemCMNVOUT.SALES_LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a12
	  on 	(a11.Contact_Account_Seq_Num = a12.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a12.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a14.Home_Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a16
	  on 	(a14.Loyalty_Program_Seq_Num = a16.Loyalty_Program_Seq_Num)
where	(a13.Calendar_Month_Id in (201304)
 and a15.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Training_Mode_Status in ('N'))
group by	a13.Calendar_Month_Id,
	a16.Loyalty_Program_Id

create volatile table ZZMD04, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Program_Id	VARCHAR(20), 
	AntKvMedl	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Program_Id) on commit preserve rows

;insert into ZZMD04 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a17.Loyalty_Program_Id  Loyalty_Program_Id,
	sum(a11.Receipt_Cnt)  AntKvMedl
from	ITSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_DEPARTMENT_D	a12
	  on 	(a11.Store_Department_Seq_Num = a12.Store_Department_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a14
	  on 	(a11.Contact_Account_Seq_Num = a14.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a15
	  on 	(a14.Member_Account_Seq_Num = a15.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a16
	  on 	(a15.Home_Store_Seq_Num = a16.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a17
	  on 	(a15.Loyalty_Program_Seq_Num = a17.Loyalty_Program_Seq_Num)
where	(a13.Calendar_Month_Id in (201304)
 and a16.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Contact_Account_Seq_Num <> -1
 and a12.Store_Department_Id not in ('__Prestore__'))
group by	a13.Calendar_Month_Id,
	a17.Loyalty_Program_Id

select	coalesce(pa11.Loyalty_Program_Id, pa12.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id, pa110.Loyalty_Program_Id)  Loyalty_Program_Id,
	max(a111.Loyalty_Program_Name)  Loyalty_Program_Name,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa11.AntMedl)  AntMedl,
	max(pa12.AntKont)  AntKont,
	max(pa11.AntNyaMedl)  AntNyaMedl,
	max(pa11.AntAktivMedl)  AntAktivMedl,
	max(pa11.AntPassivMedl)  AntPassivMedl,
	max(pa11.AntForlMedl)  AntForlMedl,
	max(pa11.AntAteraktMedl)  AntAteraktMedl,
	max(pa18.AntAktiveradMedl)  AntAktiveradMedl,
	max(pa19.AntMedlMedKop)  AntMedlMedKop,
	max(pa110.AntKvMedl)  AntKvMedl
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Loyalty_Program_Id = pa12.Loyalty_Program_Id)
	full outer join	ZZMD02	pa18
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa18.Calendar_Month_Id and 
	coalesce(pa11.Loyalty_Program_Id, pa12.Loyalty_Program_Id) = pa18.Loyalty_Program_Id)
	full outer join	ZZMD03	pa19
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa18.Calendar_Month_Id) = pa19.Calendar_Month_Id and 
	coalesce(pa11.Loyalty_Program_Id, pa12.Loyalty_Program_Id, pa18.Loyalty_Program_Id) = pa19.Loyalty_Program_Id)
	full outer join	ZZMD04	pa110
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id) = pa110.Calendar_Month_Id and 
	coalesce(pa11.Loyalty_Program_Id, pa12.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id) = pa110.Loyalty_Program_Id)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a111
	  on 	(coalesce(pa11.Loyalty_Program_Id, pa12.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id, pa110.Loyalty_Program_Id) = a111.Loyalty_Program_Id)
group by	coalesce(pa11.Loyalty_Program_Id, pa12.Loyalty_Program_Id, pa18.Loyalty_Program_Id, pa19.Loyalty_Program_Id, pa110.Loyalty_Program_Id),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03

drop table ZZMD04
