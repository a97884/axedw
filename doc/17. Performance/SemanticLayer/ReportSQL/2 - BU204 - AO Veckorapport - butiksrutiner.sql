create volatile table T12T819UHMD000, no fallback, no log(
Art_Hier_Lvl_2_Id CHAR(2), 
Retail_Region_Cd CHAR(6), 
Store_Id INTEGER, 
AntArtIAoSort INTEGER, 
AntSaldo INTEGER, 
WJXBFS1 INTEGER)
primary index (Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;

insert into T12T819UHMD000 
select a16.Art_Hier_Lvl_2_Id Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd Retail_Region_Cd,
a12.Store_Id Store_Id,
count(distinct a11.Article_Seq_Num) AntArtIAoSort,
count(a11.Article_Seq_Num) AntSaldo,
(Case when max((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then 1 else 0 end)) = 1 then count((Case when (a11.Empty_Shelf_Ind in (1) and a11.Supplier_Out_Of_Stock_Ind in (0) and a11.Incomplete_Delivery_Ind in (0)) then a11.Article_Seq_Num else NULL end)) else NULL end) WJXBFS1
from ITSemCMNVOUT.PERPETUAL_INVENTORY_F a11
join ITSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join ITSemCMNVOUT.CALENDAR_DAY_D a13
on (a11.Perpetual_Inv_Dt = a13.Calendar_Dt)
join ITSemCMNVOUT.ARTICLE_D a14
on (a11.Article_Seq_Num = a14.Article_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D a15
on (a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D a16
on (a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
where (a13.Calendar_Week_Id in (201230)
and a12.Concept_Cd in ('HEM', 'PRX', 'SNG', 'TEM', 'WIL', 'WHE', 'WH2')
and a12.Store_Type_Cd in ('-1 ', 'CORP', 'FRAN')
and a11.AO_Ind in (1))
group by a16.Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd,
a12.Store_Id

create volatile table TA3QFAH0PMD001, no fallback, no log(
Art_Hier_Lvl_2_Id CHAR(2), 
Retail_Region_Cd CHAR(6), 
Store_Id INTEGER, 
RiktadSK FLOAT, 
SpontanSK FLOAT, 
GenomfSK FLOAT)
primary index (Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;

insert into TA3QFAH0PMD001 
select a15.Art_Hier_Lvl_2_Id Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd Retail_Region_Cd,
a12.Store_Id Store_Id,
sum(a11.Requested_Cntrl_Cnt) RiktadSK,
sum(a11.Spontaneous_Cntrl_Cnt) SpontanSK,
sum(a11.Performed_Cntrl_Cnt) GenomfSK
from ITSemCMNVOUT.STOCK_BALANCE_REQUEST_CNTRL_F a11
join ITSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join ITSemCMNVOUT.ARTICLE_D a13
on (a11.Article_Seq_Num = a13.Article_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D a14
on (a13.Art_Hier_Lvl_8_Seq_Num = a14.Art_Hier_Lvl_8_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D a15
on (a14.Art_Hier_Lvl_2_Seq_Num = a15.Art_Hier_Lvl_2_Seq_Num)
join ITSemCMNVOUT.CALENDAR_DAY_D a16
on (a11.Stock_Balance_Request_Dt = a16.Calendar_Dt)
where (a16.Calendar_Week_Id in (201230)
and a12.Concept_Cd in ('HEM', 'PRX', 'SNG', 'TEM', 'WIL', 'WHE', 'WH2')
and a12.Store_Type_Cd in ('-1 ', 'CORP', 'FRAN')
and a11.AO_Ind in (1))
group by a15.Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd,
a12.Store_Id

create volatile table ZZMD02, no fallback, no log(
Art_Hier_Lvl_2_Id CHAR(2), 
Retail_Region_Cd CHAR(6), 
Store_Id INTEGER, 
AntalAndrMPL FLOAT, 
AntalAndrTD FLOAT)
primary index (Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;

insert into ZZMD02 
select a15.Art_Hier_Lvl_2_Id Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd Retail_Region_Cd,
a12.Store_Id Store_Id,
sum((Case when a11.Auto_Repl_Control_Type_Cd in ('MPL') then a11.Auto_Repl_Control_Cnt else NULL end)) AntalAndrMPL,
sum((Case when a11.Auto_Repl_Control_Type_Cd in ('TD ') then a11.Auto_Repl_Control_Cnt else NULL end)) AntalAndrTD
from ITSemCMNVOUT.AUTO_REPL_CNTRL_PARAM_F a11
join ITSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join ITSemCMNVOUT.ARTICLE_D a13
on (a11.Article_Seq_Num = a13.Article_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D a14
on (a13.Art_Hier_Lvl_8_Seq_Num = a14.Art_Hier_Lvl_8_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D a15
on (a14.Art_Hier_Lvl_2_Seq_Num = a15.Art_Hier_Lvl_2_Seq_Num)
join ITSemCMNVOUT.CALENDAR_DAY_D a16
on (a11.Change_Dt = a16.Calendar_Dt)
where (a16.Calendar_Week_Id in (201230)
and a12.Concept_Cd in ('HEM', 'PRX', 'SNG', 'TEM', 'WIL', 'WHE', 'WH2')
and a12.Store_Type_Cd in ('-1 ', 'CORP', 'FRAN')
and a11.AO_Ind in (1)
and (a11.Auto_Repl_Control_Type_Cd in ('MPL')
or a11.Auto_Repl_Control_Type_Cd in ('TD ')))
group by a15.Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd,
a12.Store_Id

create volatile table ZZMD03, no fallback, no log(
Art_Hier_Lvl_2_Id CHAR(2), 
Retail_Region_Cd CHAR(6), 
Store_Id INTEGER, 
AntalMinskadeOR FLOAT, 
AntalOR FLOAT)
primary index (Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;

insert into ZZMD03 
select a15.Art_Hier_Lvl_2_Id Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd Retail_Region_Cd,
a12.Store_Id Store_Id,
sum(a11.Decreased_Cnt) AntalMinskadeOR,
sum(a11.Inv_Request_Cnt) AntalOR
from ITSemCMNVOUT.ORDER_CONTROL_F a11
join ITSemCMNVOUT.STORE_D a12
on (a11.Store_Seq_Num = a12.Store_Seq_Num)
join ITSemCMNVOUT.ARTICLE_D a13
on (a11.Article_Seq_Num = a13.Article_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D a14
on (a13.Art_Hier_Lvl_8_Seq_Num = a14.Art_Hier_Lvl_8_Seq_Num)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D a15
on (a14.Art_Hier_Lvl_2_Seq_Num = a15.Art_Hier_Lvl_2_Seq_Num)
join ITSemCMNVOUT.CALENDAR_DAY_D a16
on (a11.Order_Dt = a16.Calendar_Dt)
where (a16.Calendar_Week_Id in (201230)
and a12.Concept_Cd in ('HEM', 'PRX', 'SNG', 'TEM', 'WIL', 'WHE', 'WH2')
and a12.Store_Type_Cd in ('-1 ', 'CORP', 'FRAN'))
group by a15.Art_Hier_Lvl_2_Id,
a12.Retail_Region_Cd,
a12.Store_Id

select coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa15.Retail_Region_Cd) Retail_Region_Cd,
max(a18.Retail_Region_Name) Retail_Region_Name,
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa15.Store_Id) Store_Id,
max(a17.Store_Name) Store_Name,
coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id) Art_Hier_Lvl_2_Id,
max(a19.Art_Hier_Lvl_2_Desc) Art_Hier_Lvl_2_Desc,
max(pa11.AntArtIAoSort) AntArtIAoSort,
max(pa12.RiktadSK) RiktadSK,
max(pa12.SpontanSK) SpontanSK,
max(pa13.AntalAndrMPL) AntalAndrMPL,
max(pa11.WJXBFS1) HIHExklRest,
max(pa12.GenomfSK) GenomfSK,
max(pa11.AntSaldo) AntSaldo,
max(pa15.AntalMinskadeOR) AntalMinskadeOR,
max(pa13.AntalAndrTD) AntalAndrTD,
max(pa15.AntalOR) AntalOR
from T12T819UHMD000 pa11
full outer join TA3QFAH0PMD001 pa12
on (pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
pa11.Store_Id = pa12.Store_Id)
full outer join ZZMD02 pa13
on (coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = pa13.Art_Hier_Lvl_2_Id and 
coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = pa13.Retail_Region_Cd and 
coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
full outer join ZZMD03 pa15
on (coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id) = pa15.Art_Hier_Lvl_2_Id and 
coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd) = pa15.Retail_Region_Cd and 
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa15.Store_Id)
join ITSemCMNVOUT.STORE_D a17
on (coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa15.Store_Id) = a17.Store_Id)
join ITSemCMNVOUT.RETAIL_REGION_D a18
on (coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa15.Retail_Region_Cd) = a18.Retail_Region_Cd)
join ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D a19
on (coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id) = a19.Art_Hier_Lvl_2_Id)
group by coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa15.Retail_Region_Cd),
coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa15.Store_Id),
coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id, pa13.Art_Hier_Lvl_2_Id, pa15.Art_Hier_Lvl_2_Id)

