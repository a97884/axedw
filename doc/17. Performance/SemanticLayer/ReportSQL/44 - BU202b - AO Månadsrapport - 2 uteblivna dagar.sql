Report: BU202b - AO Månadsrapport - 2 uteblivna dagar
Job: 182621
Report Cache Used: No

Number of Columns Returned:		6
Number of Temp Tables:		4

Total Number of Passes:		15
Number of SQL Passes:		15
Number of Analytical Passes:		0

Tables Accessed:
ARTICLE_KNOWN_LOSS_F
BALANCE_CONTROL_TRANSACTION_F
CALENDAR_DAY_D
CALENDAR_MONTH_D
STORE_D


SQL Statements:

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=BU202b - AO Månadsrapport - 2 uteblivna dagar; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntDagSK	INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZSP00 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	count(distinct a11.Inv_Tran_Event_Dt)  AntDagSK
from	PRSemCMNVOUT.BALANCE_CONTROL_TRANSACTION_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Inv_Tran_Event_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_D	a14
	  on 	(a13.Calendar_Month_Id = a14.Calendar_Month_Id)
where	a14.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01'
group by	a13.Calendar_Month_Id,
	a12.Store_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntDagSVKtrl	INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZSP01 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	count(distinct a11.Adjustment_Dt)  AntDagSVKtrl
from	PRSemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Adjustment_Dt = a13.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_D	a14
	  on 	(a13.Calendar_Month_Id = a14.Calendar_Month_Id)
where	a14.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01'
group by	a13.Calendar_Month_Id,
	a12.Store_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	AntDagSK	INTEGER, 
	AntDagSVKtrl	INTEGER)
primary index (Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD02 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.AntDagSK  AntDagSK,
	pa12.AntDagSVKtrl  AntDagSVKtrl
from	ZZSP00	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD03 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a11.Month_Nbr_Of_Days  WJXBFS1
from	PRSemCMNVOUT.CALENDAR_MONTH_D	a11
where	a11.Calendar_Month_Start_Dt between DATE '2013-02-01' and DATE '2013-05-01'

select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Store_Id  Store_Id,
	max(a13.Store_Name)  Store_Name,
	max(pa11.AntDagSK)  AntDagSK,
	max(pa12.WJXBFS1)  WJXBFS1,
	max(pa11.AntDagSVKtrl)  AntDagSVKtrl
from	ZZMD02	pa11
	left outer join	ZZMD03	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	join	PRSemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Id = a13.Store_Id)
group by	pa11.Calendar_Month_Id,
	pa11.Store_Id


SET QUERY_BAND = NONE For Session;


drop table ZZSP00

drop table ZZSP01

drop table ZZMD02

drop table ZZMD03

