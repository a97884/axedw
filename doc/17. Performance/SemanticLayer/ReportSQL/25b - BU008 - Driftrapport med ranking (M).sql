SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=25 - BU008 - Driftrapport med ranking (V); ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	ForsBelExBVBerButKampanj	FLOAT, 
	InkopsBelExButKampanj	FLOAT, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP00 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVBerButKampanj,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('L  ') then a11.Unit_Cost_Amt else NULL end))  InkopsBelExButKampanj,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Store_Name not like '%ANUL%'
 and a12.Concept_Cd in ('WIL')
 and a18.Calendar_Month_Id in (201304))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZSP01, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	SvinnKr	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZSP01 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Total_Line_Value_Amt)  SvinnKr
from	ITSemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Adjustment_Dt = a18.Calendar_Dt)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Store_Name not like '%ANUL%'
 and a11.Known_Loss_Adj_Reason_Cd in ('SV')
 and a12.Concept_Cd in ('WIL')
 and a18.Calendar_Month_Id in (201304))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD02, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	SvinnKr	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD02 
select	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	pa11.ForsBelEx  ForsBelEx,
	pa12.SvinnKr  SvinnKr,
	pa11.ForsBelExBVBer  ForsBelExBVBer,
	pa11.InkopsBelEx  InkopsBelEx
from	ZZSP00	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)

create volatile table ZZMD03, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ManuellRab	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD03 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Discount_Amt)  ManuellRab
from	ITSemCMNVOUT.SALES_TRAN_DISC_LINE_ITEM_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Store_Name not like '%ANUL%'
 and a12.Concept_Cd in ('WIL')
 and a18.Calendar_Month_Id in (201304)
 and a11.Discount_Type_Cd in ('MP'))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

create volatile table ZZMD04, no fallback, no log(
	Concept_Cd	CHAR(3), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelExEMV	FLOAT)
primary index (Concept_Cd, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD04 
select	a12.Concept_Cd  Concept_Cd,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	a12.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExEMV
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
	join	ITSemCMNVOUT.PROD_HIER_LVL_3_D	a19
	  on 	(a15.Prod_Hier_Lvl3_Seq_Num = a19.Prod_Hier_Lvl3_Seq_Num)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a12.Store_Name not like '%ANUL%'
 and a12.Concept_Cd in ('WIL')
 and a18.Calendar_Month_Id in (201304)
 and a19.Prod_Hier_Lvl2_Seq_Num in (5, 8, 19))
group by	a12.Concept_Cd,
	a12.Retail_Region_Cd,
	a12.Store_Id

select	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa16.Retail_Region_Cd)  Retail_Region_Cd,
	max(a18.Retail_Region_Name)  Retail_Region_Name,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa16.Concept_Cd)  Concept_Cd,
	max(a19.Concept_Name)  Concept_Name,
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa16.Store_Id)  Store_Id,
	max(a17.Store_Name)  Store_Name,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa11.SvinnKr)  SvinnKr,
	max(pa12.ManuellRab)  ManuellRab,
	max(pa13.ForsBelExBVBerButKampanj)  ForsBelExBVBerButKampanj,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa13.InkopsBelExButKampanj)  InkopsBelExButKampanj,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa13.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa13.ForsBelEx)  ForsBelExSvinn,
	max(pa16.ForsBelExEMV)  ForsBelExEMV,
	max(pa13.KampForsBelExBVBer)  KampForsBelExBVBer
from	ZZMD02	pa11
	full outer join	ZZMD03	pa12
	  on 	(pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	full outer join	ZZSP00	pa13
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = pa13.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = pa13.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id) = pa13.Store_Id)
	full outer join	ZZMD04	pa16
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd) = pa16.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd) = pa16.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id) = pa16.Store_Id)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa16.Concept_Cd) = a17.Concept_Cd and 
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa16.Retail_Region_Cd) = a17.Retail_Region_Cd and 
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa16.Store_Id) = a17.Store_Id)
	join	ITSemCMNVOUT.RETAIL_REGION_D	a18
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa16.Retail_Region_Cd) = a18.Retail_Region_Cd)
	join	ITSemCMNVOUT.CONCEPT_D	a19
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa16.Concept_Cd) = a19.Concept_Cd)
group by	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd, pa13.Retail_Region_Cd, pa16.Retail_Region_Cd),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd, pa13.Concept_Cd, pa16.Concept_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id, pa13.Store_Id, pa16.Store_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZSP00

drop table ZZSP01

drop table ZZMD02

drop table ZZMD03

drop table ZZMD04