SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=Antal minskade kollin per HG; ClientUser=Administrator;' For Session;

select	a12.Concept_Cd  Concept_Cd,
	max(a17.Concept_Name)  Concept_Name,
	a11.Order_Dt  Calendar_Dt,
	a15.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	max(a15.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	a12.Store_Id  Store_Id,
	max(a12.Store_Name)  Store_Name,
	a12.Retail_Region_Cd  Retail_Region_Cd,
	max(a16.Retail_Region_Name)  Retail_Region_Name,
	sum(a11.Order_Proposed_Item_Qty)  AntalKolliGen,
	sum(a11.Order_Item_Qty)  AntalKolliAtt,
	sum(a11.Inv_Request_Cnt)  AntalOR,
	sum(a11.Decreased_Cnt)  AntalMinskadeOR
from	PRSemCMNVOUT.ORDER_CONTROL_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_D	a13
	  on 	(a11.Article_Seq_Num = a13.Article_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a14
	  on 	(a13.Art_Hier_Lvl_8_Seq_Num = a14.Art_Hier_Lvl_8_Seq_Num)
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a15
	  on 	(a14.Art_Hier_Lvl_2_Seq_Num = a15.Art_Hier_Lvl_2_Seq_Num)
	join	PRSemCMNVOUT.RETAIL_REGION_D	a16
	  on 	(a12.Retail_Region_Cd = a16.Retail_Region_Cd)
	join	PRSemCMNVOUT.CONCEPT_D	a17
	  on 	(a12.Concept_Cd = a17.Concept_Cd)
where	(a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Order_Dt in (DATE '2013-04-14', DATE '2013-04-13', DATE '2013-04-12', DATE '2013-04-11', DATE '2013-04-10', DATE '2013-04-09', DATE '2013-04-08')
 and a15.Art_Hier_Lvl_2_Id in ('12'))
group by	a12.Concept_Cd,
	a11.Order_Dt,
	a15.Art_Hier_Lvl_2_Id,
	a12.Store_Id,
	a12.Retail_Region_Cd