/* check test case 1 */
.run file=/centralhome/k9105194/.logondb_edwt
.set session charset 'UTF8'
.import vartext file=input.dat
.os mv -f output.dat output.dat.`date '+%y%m%d%M%H%S'`
.export report file=output.dat
USING str (VARCHAR(100))
Select clobreplerrc(
Cast(
	Translate(Cast(:str as CLOB(100)) USING UNICODE_TO_LATIN WITH ERROR)
	as CLOB(100))
)
;
.export reset
.os grep 'XQ S' output.dat && echo SUCCESS || echo FAILURE
