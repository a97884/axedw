/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: udfclobreplerrc.c 7413 2012-11-12 15:43:46Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-11-12 16:43:46 +0100 (mån, 12 nov 2012) $
# Last Revision    : $Revision: 7413 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/site/clobreplerrc/udfclobreplerrc.c $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

#include "udfstrstr.h"

#define ERROR_CHARACTER '\x1A'	/* Error character for untranslatable UNICODE to Latin characters */
#define REPL_CHARACTER ' '	/* Replacement character for untranslatable UNICODE to Latin characters */

boolean verifyArgsclobreplerrc(StrStrInfo *strinfo);
void beginclobreplerrc(StrStrInfo *strinfo);
void replerrcclobrepl(StrStrInfo *strinfo);

/**
 * Replace error character (Latin) in the LOB source and return the new string in a LOB
 *
 * If the string length of LOB source or LOB set is zero, or if the LOB set is larger than the 
 * LOB source, then this UDF will not perform any replace and not initialize a return value.
 *
 * The following conditions will cause the UDF to fail:
 * the LOB set is larger than the maximum available memory
 * the required memory for storing the LOB source or LOB set was not allocated
 * the LOB set was not completely read into memory
 * the return string got truncated
 *
 * Here's an SQL statement that creates the function:<pre>
 * CREATE FUNCTION clobreplerrc
 *        (A CLOB AS LOCATOR)
 *    RETURNS CLOB AS LOCATOR
 *    LANGUAGE C
 *    NO SQL
 *    SPECIFIC udfclobreplerrc
 *    EXTERNAL NAME 'CI!udfstrstr!udfstrstr.h!CS!udfclobreplerrc!udfclobreplerrc.c!F!udfclobreplerrc'
 *   PARAMETER STYLE TD_GENERAL;
 *
 * Here's an example of its use and output, assuming the following table:
 * CREATE TABLE tableLobErrc(
 *     rownum      INTEGER, 
 *     myClob1     CLOB FORMAT CHARACTER SET UNICODE
 * );
 * INSERT INTO tableLobErrc values (2, TRANSLATE('XQ�S' UNICODE_TO_LATIN with ERROR));
 * SELECT rownum, clobreplerrc(myClob1) FROM tableLobErrc ORDER BY rownum;
 *      rownum clobreplerrc(myClob1)
 * ----------- -----------------------
 *           2 XQ S
 *
 * @param src       - LOB string to be replaced
 * @param sqlstate  - error condition encountered during the search
 */
void udfclobreplerrc(LOB_LOCATOR *src,
                     LOB_RESULT_LOCATOR *dest,
                     char sqlstate[6])
{
    LobInfo lobsrc;
    StrStrInfo strinfo;

    lobsrc.loc = src;

    strinfo.lobsrc = &lobsrc;
    strinfo.lobdest = dest;
    strinfo.sqlstate = sqlstate;

    FNC_LobOpen(*lobsrc.loc, &lobsrc.id, 0, 0); // open LOBs

    lobsrc.len = FNC_GetLobLength(*lobsrc.loc); // get the data length of LOB

    if (verifyArgsclobreplerrc(&strinfo) == TRUE) // if false, then arguments have a problem -- abort!
    {
        beginclobreplerrc(&strinfo);
    }

    FNC_LobClose(lobsrc.id); // close LOB
}

/**
 * Verifies the needed parameters contained in the given structure.
 * @param strinfo   - the structure containing all parameters required to perform the operation
 * @return true if arguments check out fine, else false
 */
boolean verifyArgsclobreplerrc(StrStrInfo *strinfo)
{
    if (strinfo->lobsrc->len == 0) // no data to process
    {                                                               // string size is zero
        return FALSE;
    }
    return TRUE;
}

/**
 * Allocates the required memory to perform the operation.
 * @param strinfo   - the structure containing all parameters required to perform the operation
 */
void beginclobreplerrc(StrStrInfo *strinfo)
{
    // check so we have enough memory to hold the lob to process
    if (strinfo->lobsrc->len > (MAX_CUMULATIVE_MALLOC_SIZE - 2))
    {
	strcpy(strinfo->sqlstate, UDF_STRSTR_NO_SIZE);
	return;
    }
    else
    {
        strinfo->srclen = strinfo->lobsrc->len;
    }

    strinfo->srcbuf = malloc(strinfo->srclen + 1); // allocate memory for source data buffer
    strinfo->setbuf = malloc(strinfo->srclen + 1); // allocate memory for processed data buffer
    strinfo->setlen = strinfo->srclen;

    if ((strinfo->srcbuf == NULL) || (strinfo->setbuf == NULL)) // if true, then a buffer was not allocated the
    {                                                           // memory it needs to perform a string search
        strcpy(strinfo->sqlstate, UDF_STRSTR_BUF_NULL);
    }
    else
    {
        replerrcclobrepl(strinfo);
    }

    free(strinfo->srcbuf); // free memory for data buffer
    free(strinfo->setbuf); // free memory for data buffer
}

/**
 * Performs the operation of the LOB and store back to LOB
 * @param strinfo   - the structure containing all parameters required to perform the operation
 */
void replerrcclobrepl(StrStrInfo *strinfo)
{
	FNC_LobLength_t actlen = 0; // used to hold the actual length read from or written to a LOB
	FNC_LobLength_t offset = 0; // used to handle char set positioned at data buffer boundaries of char src
	int i = 0;
	
	// read into a data buffer the entire lob
	while (FNC_LobRead(strinfo->lobsrc->id, strinfo->srcbuf, (FNC_LobLength_t)strinfo->srclen, &actlen) == 0)
	{
		strinfo->srcbuf[actlen] = 0; // ensure NULL termination of data buffer
	}
	if (strinfo->srclen != strlen((const char*)strinfo->srcbuf)) // if true, then not all of the character set was read -- abort!
	{                                               // (it has to be completely in memory in order to process!)
		strcpy(strinfo->sqlstate, UDF_STRSTR_LOBSET_NOT_COMPLETE);
		return;
	}

	for ( i = 0; i < strinfo->srclen; i++ )
	{
		if ( strinfo->srcbuf[i] == ERROR_CHARACTER )
			strinfo->setbuf[i] = REPL_CHARACTER;
		else
			strinfo->setbuf[i] = strinfo->srcbuf[i];
	}
	strinfo->setlen = i;
	if ( strinfo->setlen == strinfo->srclen )
	{
		actlen = strinfo->setlen;
        // append replaced string to the destination
        if (FNC_LobAppend(*strinfo->lobdest, strinfo->setbuf, actlen, &actlen) != 0)
        {
        	strcpy(strinfo->sqlstate, UDF_STRSTR_TRUNCATION_ERR);
        	return;
        }
	}
	else
	{
        	strcpy(strinfo->sqlstate, UDF_STRSTR_REPLACE_ERR);
        	return;
	}
}
