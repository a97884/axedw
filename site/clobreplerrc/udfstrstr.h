/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: udfstrstr.h 7413 2012-11-12 15:43:46Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-11-12 16:43:46 +0100 (mån, 12 nov 2012) $
# Last Revision    : $Revision: 7413 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/site/clobreplerrc/udfstrstr.h $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

/* 
 * Copyright (c) 2003 NCR Corporation. All Rights Reserved.
 * NCR Confidential and Trade Secret.
 *
 * This document, and the information contained herein, are the exclusive
 * property of NCR. In no case shall this document or its contents be
 * reproduced or copied by any means, in whole or in part, or disseminated
 * outside of the company, without prior written permission of a company
 * officer.
 *
 * Author: Andreas Marek
 */
/*
    History:
        DRxxxxx-am120250-01 2003Jul18 New file
        Changed the functionality from string compare to compress 2012-03-27, Anders Bagge
*/

#define SQL_TEXT Latin_Text

#include <sqltypes_td.h>
#include <string.h>

//#define MAX_CUMULATIVE_MALLOC_SIZE 14 // this is for test purposes, to check out boundaries on memory management!
#define MAX_CUMULATIVE_MALLOC_SIZE ((2*1024*1024)-1) // this is the real limit on memory management!

#define FALSE 0
#define TRUE 1

#define UDF_STRSTR_TRUNCATION_ERR       "22001" // a truncation error occured while writing a LOB
#define UDF_STRSTR_UNCOMPRESS_ERR       "22002" // uncompress error
#define UDF_STRSTR_REPLACE_ERR  		"22003" // the LOB set was not set completely
#define UDF_STRSTR_NO_SIZE              "U0001" // a string size of zero bytes was detected
#define UDF_STRSTR_SET_GT_SRC           "U0002" // the character set is greater than the character source
#define UDF_STRSTR_LOBSET_TOO_BIG       "U0003" // the LOB set is too big
#define UDF_STRSTR_BUF_NULL             "U0004" // a buffer was not allocated any memory
#define UDF_STRSTR_LOBSET_NOT_COMPLETE  "U0005" // the LOB set was not read completely

/**
 * Defines a data type where 0=FALSE and 1=TRUE.
 */
typedef BYTE boolean;

/**
 * Stores key attributes of a LOB.
 */
typedef struct {
    /**
     * The locator attribute of a LOB.
     */
    LOB_LOCATOR *loc;

    /**
     * The identifier attribute of a LOB.
     */
    LOB_CONTEXT_ID id;

    /**
     * The size of the LOB data.
     */
    FNC_LobLength_t len;
} LobInfo;

/**
 * Stores the attributes needed to perform a string search.
 *
 * Members are used on an "as-needed" basis.
 */
typedef struct {
    /**
     * The key attributes of the LOB to be searched in.
     */
    LobInfo *lobsrc;

    /**
     * The source buffer used to contain the string to be searched in.
     */
    BYTE *srcbuf;

    /**
     * The size of the "source" buffer.
     */
    size_t srclen;

    /**
     * The key attributes of the LOB to be searched for.
     */
    LobInfo *lobset;

    /**
     * The set buffer used to contain the string to be searched for.
     */
    BYTE *setbuf;

    /**
     * The size of the "set" buffer.
     */
    size_t setlen;

    /**
     * The LOB destination that will contain the search result.
     */
    LOB_RESULT_LOCATOR *lobdest;

    /**
     * The character destination that will contain the search result.
     */
    CHARACTER *chardest;

    /**
     * The index into the LOB or character source that contains the first instance of "set".
     */
    INTEGER *index;

    /**
     * The result of any error condition encountered.
     */
    char *sqlstate;
} StrStrInfo;
