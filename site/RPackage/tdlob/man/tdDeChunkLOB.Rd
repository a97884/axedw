\name{tdDeChunkLOB}
\alias{tdDeChunkLOB}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
tdDeChunkLOB
}
\description{
Read parts from a LOB and put them together into one R object.
Can be used from ExecR table operator only.
}
\usage{
tdDeChunkLOB(inHandle, colIndex, increment = 3e+07)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{inHandle}{
R handle to read the LOB from
}
  \item{colIndex}{
the index of the LOB column to read from
}
  \item{increment}{
the size of each chunk in the LOB
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
An unserialized R object read from a Large Object in the Teradata database
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Anders Bagge, anders.bagge@teradata.com
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
## The function is currently defined as
function (inHandle, colIndex, increment = 3e+07) 
{
    ret_model <- list()
    ret_model <- unlist(ret_model)
    rawModel <- list()
    rawModel <- unlist(rawModel)
    loc_model <- list()
    if ((out <- tdr.Read(inHandle) == 0)) {
        locator <- tdr.GetAttributeByNdx(inHandle, colIndex, 
            NULL)
        i <- 0
        start <- 0
        while (i < locator$length) {
            inlob <- tdr.LobOpen_CL(locator, start, increment)
            if (inlob$LOBlen == 0) {
                break
            }
            loc_model <- tdr.LobRead(inlob$contextID, inlob$LOBlen)
            rawModel <- c(rawModel, loc_model$buffer)
            tdr.LobClose(inlob$contextID)
            i <- i + 1
            start <- start + increment
        }
        ret_model <- unserialize(unlist(rawModel), NULL)
    }
    return(ret_model)
}

## Example to execute from SQL
DROP  TABLE score_historical;
CREATE SET TABLE score_historical
(
      CustID INTEGER NOT NULL,
      predBuyProduct FLOAT NOT NULL,
      score_timestamp TIMESTAMP(6) NOT NULL
)
PRIMARY INDEX ( CustID )
;

INSERT INTO score_historical (CustID,predBuyProduct,score_timestamp)
SELECT CustID,predBuyProduct,CURRENT_TIMESTAMP as score_timestamp
FROM TD_SYSGPL.ExecR (
	ON (SELECT a.* FROM tlobTestData as a ) HASH BY CustID
	ON (SELECT * FROM tlobModels where ID = (select max(ID) from tlobModels) ) DIMENSION
USING
	Contract('library(tdr)
		Streamno <- 0
		slno <- list(datatype="INTEGER_DT", bytesize="SIZEOF_INTEGER")
		real <- list(datatype="REAL_DT", bytesize="SIZEOF_REAL")
		coldef <- list(CustID=slno, predBuyProduct=real)
		tdr.SetOutputColDef(Streamno, coldef)
	')
	Operator('
		library(tdr)
		library(tdlob)

		# Read the trained model
		mdstream <- 1
		mdHandle <- tdr.Open("R", mdstream, 0)
		lobColIndex <- 1
		fit_model <- tdDeChunkLOB( mdHandle, lobColIndex )
		tdr.Close(mdHandle)

		# Read the test data table
		dtstream <- 0
		opstream <- 0
		dtHandle <- tdr.Open("R", dtstream, 0)
		opHandle <- tdr.Open("W", opstream, 0)

		test_data <- data.frame()
		buff_size <- as.integer(10000000)
		somerows <- tdr.TblRead(dtHandle, buff_size)
		while( nrow(somerows) > 0 ) {
			test_data <- rbind(test_data, somerows)
			somerows <- tdr.TblRead(dtHandle, buff_size)
		}
		tdr.Close(dtHandle)

		ids <- test_data["CustID"]
		test_data["CustID"] <- NULL
		print( dim(test_data) )

		# predict and write results
		prediction <- predict(fit_model, test_data, type="response")
		names(prediction) <- "predBuyProduct"
		results <- cbind(ids, prediction)
		tdr.TblWrite(opHandle, results)

		# close streams
		tdr.Close(opHandle)
	')
	keeplog(1)
) AS D;

drop table target_n_predBuyProduct;
create table target_n_predBuyProduct as (
select 
	a.*
	,b.BuyProduct 
from score_historical a
left join tlobTestData b
on a.CustID = b.CustID
) with data
;

select 
	a.*
	,b.BuyProduct 
from score_historical a
left join tlobTestData b
on a.CustID = b.CustID
and a.predBuyProduct <> b.BuyProduct
;

select BuyProduct,average(predBuyProduct) as score_avg from target_n_predBuyProduct
group by BuyProduct
;
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
