/*
 * Copyright (C) 2008 by Teradata Corporation.
 * All Rights Reserved.
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
/*
 * replaces - REPLACE UDF
 *
 * This UDF searches input_string for all occurrences of search_string,
 * and for each occurrence found, substitutes replace_string. There
 * may be zero, one, or many substitutions.
 *
 * If input_string is NULL, a NULL will be returned.
 *
 * If search_string is NULL or empty, then input_string will be returned
 * unaltered.
 *
 * If replace_string is NULL or empty, all occurrences of search_string
 * are removed from input_string.
 *
 * SELECT replaces('JACK and JUE', 'J', 'BL') (TITLE 'Changes');
 *
 * Returns the following result:
 *
 * Changes
 * ---------------
 * BLACK and BLUE
 *
 * A User Defined Exception will be returned if the return result
 * exceeds the maximum defined (see define MAXIMUM_LENGTH).
 *
 * NOTE: The define MAXIMUM_LENGTH must match the VARCHAR length for
 * the return parameter in the CREATE/REPLACE FUNCTION SQL.
 *

REPLACE FUNCTION REPLACES (
  input_string VARCHAR(60000) CHARACTER SET LATIN,
  search_string VARCHAR(512) CHARACTER SET LATIN,
  replace_string VARCHAR(512) CHARACTER SET LATIN)
RETURNS VARCHAR(60000) CHARACTER SET LATIN
SPECIFIC replaces
LANGUAGE C
NO SQL
PARAMETER STYLE SQL
DETERMINISTIC
CALLED ON NULL INPUT
EXTERNAL NAME 'CS!replaces!$PGMPATH$/replaces.c'
*/


#define SQL_TEXT Latin_Text

#include "sqltypes_td.h"
#include <string.h>
#include <stdio.h>


#define IS_NULL -1
#define IS_NOT_NULL 0
#define MAXIMUM_LENGTH 60000
#define EOS '\0'
#define UDF_OK "00000"
#define UDF_ERR_RESULTLENGTH1 "U0001"
#define UDF_MSG_RESULTLENGTH1 "Result exceeded maximum length"
#define UDF_ERR_RESULTLENGTH2 "U0002"
#define UDF_MSG_RESULTLENGTH2 UDF_MSG_RESULTLENGTH1
#define UDF_ERR_RESULTLENGTH3 "U0003"
#define UDF_MSG_RESULTLENGTH3 UDF_MSG_RESULTLENGTH1
#define UDF_ERR_RESULTLENGTH4 "U0004"
#define UDF_MSG_RESULTLENGTH4 UDF_MSG_RESULTLENGTH1


void REPLACES(VARCHAR_LATIN *input_string,
               VARCHAR_LATIN *search_string,
               VARCHAR_LATIN *replace_string,
               VARCHAR_LATIN *result_string,
               int *input_string_indicator,
               int *search_string_indicator,
               int *replace_string_indicator,
               int *result_string_indicator,
               char sqlstate[6],
               SQL_TEXT extname[129],
               SQL_TEXT specific_name[129],
               SQL_TEXT error_message[257])
{
    int source_length;
    int search_length;
    int replace_length;
    unsigned char *source_ptr;
    unsigned char *source_limit;
    unsigned char *match_limit;
    unsigned char *save_source_ptr;
    unsigned char *target_ptr;
    unsigned char *target_limit;
    unsigned char *search_ptr;
    unsigned char *search_limit;


    if (*input_string_indicator == IS_NULL)
        {
        *result_string_indicator = IS_NULL;

        (void) strcpy(sqlstate, UDF_OK);
        error_message[0] = EOS;

        return;
        }

    source_length = strlen((char *) input_string);

    if (*replace_string_indicator == IS_NOT_NULL)
        replace_length = strlen((char *) replace_string);
    else
        replace_length = 0;

    /* Don't search if it is impossible to get a match. */

    if ((source_length == 0) ||
        (*search_string_indicator == IS_NULL) ||
        ((search_length = strlen((char *) search_string)) == 0) ||
        (search_length > source_length))
        {
        if (source_length > MAXIMUM_LENGTH)
            {
            *result_string_indicator = IS_NULL;

            (void) sprintf(sqlstate, UDF_ERR_RESULTLENGTH1);
            (void) strcpy((char *) error_message, UDF_MSG_RESULTLENGTH1);

            return;
            }

        (void) strcpy((char *) result_string, (char *) input_string);

        *result_string_indicator = IS_NOT_NULL;

        (void) strcpy(sqlstate, UDF_OK);
        error_message[0] = EOS;

        return;
        }

    source_ptr = input_string;
    source_limit = input_string + source_length;
    match_limit = (source_limit - search_length) + 1;
    target_ptr = result_string;
    target_limit = result_string + MAXIMUM_LENGTH;
    search_ptr = search_string;
    search_limit = search_string + search_length;

    while (source_ptr < source_limit)
        {
        if ((*search_ptr == *source_ptr) && (source_ptr < match_limit))
            {
            /* Possible match. Check remainder of pattern */

            save_source_ptr = source_ptr;

            while ((++search_ptr < search_limit) &&
                   (*search_ptr == *(++source_ptr)));

            if (search_ptr == search_limit)
                {
                /* Pattern match. */

                ++source_ptr;

                if (replace_length > 0)
                    {
                    /* Substitute new value. */

                    *target_ptr = EOS;

                    if (((target_ptr + replace_length) - 1) >= target_limit)
                        {
                        *result_string_indicator = IS_NULL;

                        (void) sprintf(sqlstate, UDF_ERR_RESULTLENGTH2);
                        (void) strcpy((char *) error_message,
                            UDF_MSG_RESULTLENGTH2);

                        return;
                        }

                    strcat((char *) target_ptr, (char *) replace_string);

                    target_ptr += replace_length;
                    }
                }
            else
                {
                source_ptr = save_source_ptr;

                if (target_ptr == target_limit)
                    {
                    *result_string_indicator = IS_NULL;

                    (void) sprintf(sqlstate, UDF_ERR_RESULTLENGTH3);
                    (void) strcpy((char *) error_message,
                        UDF_MSG_RESULTLENGTH3);

                    return;
                    }

                    *(target_ptr++) = *(source_ptr++);
                    }

            search_ptr = search_string;
            }
        else
            {
            if (target_ptr == target_limit)
                {
                *result_string_indicator = IS_NULL;

                (void) sprintf(sqlstate, UDF_ERR_RESULTLENGTH4);
                (void) strcpy((char *) error_message, UDF_MSG_RESULTLENGTH4);

                return;
                }

            *(target_ptr++) = *(source_ptr++);
            }
        }

    *target_ptr = EOS;

    *result_string_indicator = IS_NOT_NULL;

    (void) strcpy(sqlstate, UDF_OK);
    error_message[0] = EOS;
}

