/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: udfclobcmpr.c 5436 2012-09-05 11:53:29Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-09-05 13:53:29 +0200 (ons, 05 sep 2012) $
# Last Revision    : $Revision: 5436 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/site/clobcmpr/udfclobcmpr.c $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

/* 
 * Copyright (c) 2003 NCR Corporation. All Rights Reserved.
 * NCR Confidential and Trade Secret.
 *
 * This document, and the information contained herein, are the exclusive
 * property of NCR. In no case shall this document or its contents be
 * reproduced or copied by any means, in whole or in part, or disseminated
 * outside of the company, without prior written permission of a company
 * officer.
 *
 * Author: Andreas Marek
 */
/*
    History:
        DRxxxxx-am120250-01 2003Jul18 New file
	Changed the functionality from string compare to compress 2012-03-27, Anders Bagge
*/
#include <zlib.h>
#include "udfstrstr.h"

/**@#-*/ // this comment turns off ccdoc parsing on the following forward declarations of functions
boolean verifyArgsclobcmpr(StrStrInfo *strinfo);
void beginclobcmpr(StrStrInfo *strinfo);
void compressclobcmpr(StrStrInfo *strinfo);
/**@#+*/ // this comment turns on ccdoc parsing

/**
 * Compress the LOB source and return a compressed LOB
 *
 * If the string length of LOB source or LOB set is zero, or if the LOB set is larger than the 
 * LOB source, then this UDF will not perform any compress and not initialize a return value.
 *
 * The following conditions will cause the UDF to fail:<ul>
 * <li>the LOB set is larger than the maximum available memory
 * <li>the required memory for storing the LOB source or LOB set was not allocated
 * <li>the LOB set was not completely read into memory
 * <li>the return string got truncated</ul>
 *
 * Here's an SQL statement that creates the function:<pre>
 * CREATE FUNCTION clobcmpr
 *        (A CLOB AS LOCATOR)
 *    RETURNS BLOB AS LOCATOR
 *    LANGUAGE C
 *    NO SQL
 *    SPECIFIC udfclobcmpr
 *    EXTERNAL NAME 'CI!udfstrstr!udfstrstr.h!CS!udfclobcmpr!udfclobcmpr.c!F!udfclobcmpr'
 *   PARAMETER STYLE TD_GENERAL;
 * </pre>
 * Here's an example of its use and output, assuming the following table:<pre>
 * CREATE TABLE tableStrStrLobLob(
 *     rownum      INTEGER, 
 *     myClob1     CLOB FORMAT 'X(40)'
 * );
 * INSERT INTO tableStrStrLobLob values (2, 'test123test');
 * SELECT rownum, clobcmpr(myClob1) FROM tableStrStrLobLob ORDER BY rownum;
 *      rownum clobcmpr(myClob1,myClob2)
 * ----------- -----------------------
 *           2 x
 * </pre>
 * @param src       - LOB string to be compressed
 * @param dest      - compressed result in the form of a LOB string
 * @param sqlstate  - error condition encountered during the search
 */
void udfclobcmpr(LOB_LOCATOR *src,
                     LOB_RESULT_LOCATOR *dest,
                     char sqlstate[6])
{
    LobInfo lobsrc;
    StrStrInfo strinfo;

    lobsrc.loc = src;

    strinfo.lobsrc = &lobsrc;
    strinfo.lobdest = dest;
    strinfo.sqlstate = sqlstate;

    FNC_LobOpen(*lobsrc.loc, &lobsrc.id, 0, 0); // open LOBs

    lobsrc.len = FNC_GetLobLength(*lobsrc.loc); // get the data length of LOB

    if (verifyArgsclobcmpr(&strinfo) == TRUE) // if false, then arguments have a problem -- abort!
    {
        beginclobcmpr(&strinfo);
    }

    FNC_LobClose(lobsrc.id); // close LOB
}

/**
 * Verifies the needed parameters contained in the given structure.
 * @param strinfo   - the structure containing all parameters required to perform the clobcmpr operation
 * @return true if arguments check out fine, else false
 */
boolean verifyArgsclobcmpr(StrStrInfo *strinfo)
{
    if (strinfo->lobsrc->len == 0) // no data to compress
    {                                                               // string size is zero
        return FALSE;
    }
    return TRUE;
}

/**
 * Allocates the required memory to perform the strstr operation.
 * @param strinfo   - the structure containing all parameters required to perform the strstr operation
 */
void beginclobcmpr(StrStrInfo *strinfo)
{
    // check so we have enough memory to hold the lob to compress
    if (strinfo->lobsrc->len > (MAX_CUMULATIVE_MALLOC_SIZE - 2))
    {
	strcpy(strinfo->sqlstate, UDF_STRSTR_NO_SIZE);
	return;
    }
    else
    {
        strinfo->srclen = strinfo->lobsrc->len;
    }

    strinfo->srcbuf = malloc(strinfo->srclen + 1); // allocate memory for source data buffer
    strinfo->setbuf = malloc(strinfo->srclen + 1); // allocate memory for compressed data buffer
    strinfo->setlen = strinfo->srclen;

    if ((strinfo->srcbuf == NULL) || (strinfo->setbuf == NULL)) // if true, then a buffer was not allocated the
    {                                                           // memory it needs to perform a string search
        strcpy(strinfo->sqlstate, UDF_STRSTR_BUF_NULL);
    }
    else
    {
        compressclobcmpr(strinfo);
    }

    free(strinfo->srcbuf); // free memory for data buffer
    free(strinfo->setbuf); // free memory for data buffer
}

/**
 * Performs the compress of the LOB and store back to LOB
 * @param strinfo   - the structure containing all parameters required to perform the compress operation
 */
void compressclobcmpr(StrStrInfo *strinfo)
{
	FNC_LobLength_t actlen = 0; // used to hold the actual length read from or written to a LOB
	FNC_LobLength_t offset = 0; // used to handle char set positioned at data buffer boundaries of char src
	BYTE *buf; // used as the data buffer for search results

	// read into a data buffer the entire lob for compression
	while (FNC_LobRead(strinfo->lobsrc->id, strinfo->srcbuf, (FNC_LobLength_t)strinfo->srclen, &actlen) == 0)
	{
		strinfo->srcbuf[actlen] = 0; // ensure NULL termination of data buffer
	}
	if (strinfo->srclen != strlen((const char*)strinfo->srcbuf)) // if true, then not all of the character set was read -- abort!
	{                                               // (it has to be completely in memory in order to compress!)
		strcpy(strinfo->sqlstate, UDF_STRSTR_LOBSET_NOT_COMPLETE);
		return;
	}

	// do compress
	// ZEXTERN int ZEXPORT compress2 OF((Bytef *dest,   uLongf *destLen,
        //                          const Bytef *source, uLong sourceLen,
        //                          int level));

	if ( compress( (Bytef *)strinfo->setbuf, (uLongf *)&(strinfo->setlen), (Bytef *)strinfo->srcbuf, (uLongf)strinfo->srclen ) == Z_OK )
	{
		actlen = strinfo->setlen;
        	// append compressed characters to the destination
        	if (FNC_LobAppend(*strinfo->lobdest, strinfo->setbuf, actlen, &actlen) != 0)
        	{
        		strcpy(strinfo->sqlstate, UDF_STRSTR_TRUNCATION_ERR);
        		return;
        	}
	}
	else
	{
        	strcpy(strinfo->sqlstate, UDF_STRSTR_TRUNCATION_ERR);
        	return;
	}
}
