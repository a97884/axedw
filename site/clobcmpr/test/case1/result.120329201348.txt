BTEQ 13.10.00.02 Thu Mar 29 13:20:48 2012
 
+---------+---------+---------+---------+---------+---------+---------+----
/* check test case 1 */
.run file=/centralhome/k9105194/.logondb_edwp
+---------+---------+---------+---------+---------+---------+---------+----
.logon 194.14.143.128/prdbadmin,

 *** Logon successfully completed.
 *** Teradata Database Release is 13.10.01.01                   
 *** Teradata Database Version is 13.10.01.02                     
 *** Transaction Semantics are BTET.
 *** Session Character Set Name is 'ASCII'.
 
 *** Total elapsed time was 1 second.
 
+---------+---------+---------+---------+---------+---------+---------+----
+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select max(characters(s.XmlFile))
From Archv_POS.From_Sonic2 as s
Where s.BusinessDayID = '2010-03-31'
;

 *** Query completed. One row found. One column returned. 
 *** Total elapsed time was 3 seconds.

Maximum(Characters(Xmlfile))
----------------------------
                      245747

+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 1 for 30000) as VARCHAR(30000)) <> Cast(
substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 1 for 30000) as VARCHAR(300
00))
and characters(s.Xmlfile) <= 30000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was one minute and 53 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 30001 for 30000) as VARCHAR(30000)) <> C
ast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 30001 for 30000) as VAR
CHAR(30000))
and characters(s.Xmlfile) between 30001 and 60000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 28 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 60001 for 30000) as VARCHAR(30000)) <> C
ast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 60001 for 30000) as VAR
CHAR(30000))
and characters(s.Xmlfile) between 60001 and 90000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 15 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 90001 for 30000) as VARCHAR(30000)) <> C
ast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 90001 for 30000) as VAR
CHAR(30000))
and characters(s.Xmlfile) between 90001 and 120000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 6 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename,
 Cast(substring(s.Xmlfile from 1 for 120001) as VARCHAR(30000)) as orig,
 Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 120001 for 30000) as 
VARCHAR(30000)) as compr
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 120001 for 30000) as VARCHAR(30000)) <> 
Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 120001 for 30000) as V
ARCHAR(30000))
and characters(s.Xmlfile) between 120001 and 150000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 5 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 150001 for 30000) as VARCHAR(30000)) <> 
Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 150001 for 30000) as V
ARCHAR(30000))
and characters(s.Xmlfile) between 150001 and 180000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 5 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 180001 for 30000) as VARCHAR(30000)) <> 
Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 180001 for 30000) as V
ARCHAR(30000))
and characters(s.Xmlfile) between 180001 and 210000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 4 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 210001 for 30000) as VARCHAR(30000)) <> 
Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 210001 for 30000) as V
ARCHAR(30000))
and characters(s.Xmlfile) between 210001 and 240000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 3 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select
 s.RetailstoreID,
 s.WorkstationID,
 s.SequenceNr,
 s.ReceiptSequenceNr,
 s.TS,
 s.TransactionTypecode,
 s.BusinessDayID,
 s.OperatorID,
 s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
Where s.BusinessDayID = '2010-03-31'
and Cast(substring(s.Xmlfile from 240001 for 30000) as VARCHAR(30000)) <> 
Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 240001 for 30000) as V
ARCHAR(30000))
and characters(s.Xmlfile) between 240001 and 270000
;

 *** Query completed. No rows found. 
 *** Total elapsed time was 4 seconds.


+---------+---------+---------+---------+---------+---------+---------+----

Lock row for access
Select count(*)
From Archv_POS.From_Sonic2 as s
Where s.BusinessDayID = '2010-03-31'
and characters(s.Xmlfile) > 270000
;

 *** Query completed. One row found. One column returned. 
 *** Total elapsed time was 3 seconds.

   Count(*)
-----------
          0

+---------+---------+---------+---------+---------+---------+---------+----

 *** BTEQ exiting due to EOF on stdin.

 *** Exiting BTEQ...
 *** RC (return code) = 0 
