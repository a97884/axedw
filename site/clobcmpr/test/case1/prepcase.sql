/* prep test case 1 */
.run file=/centralhome/k9105194/.logondb_edwp

insert into PRStgAxboArchT.Stg_ArchPOS
(
RetailstoreID
,WorkstationID
,SequenceNr
,ReceiptSequenceNr
,TS
,TransactionTypecode
,BusinessDayID
,OperatorID
,Filename
,ArchiveKey
,Xmlfile
)
Select
RetailstoreID, WorkstationID, SequenceNr, ReceiptSequenceNr, TS, TransactionTypecode, BusinessDayID, OperatorID, Filename,
	(	TRIM(SequenceNr)
		|| '_' || TRIM(RetailstoreID)
		|| '_' || SUBSTRING(CAST(TS AS VARCHAR(26)) FROM 1 FOR 10)
		|| '_' || SUBSTRING(CAST(TS AS VARCHAR(26)) FROM 12 FOR 15)
		),
 Xmlfile
From Archv_POS.From_Sonic2_compr
where BusinessDayID = '2010-03-31'
;
