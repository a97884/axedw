/* check test case 2 */
.run file=/centralhome/k9105194/.logondb_edwp

Lock row for access
Select max(characters(s.XmlFile))
From Archv_POS.From_Sonic2 as s
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
;
/* 511495 */

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 1 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 1 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) <= 30000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 30001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 30001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 30001 and 60000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 60001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 60001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 60001 and 90000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 90001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 90001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 90001 and 120000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename,
	Cast(substring(s.Xmlfile from 1 for 120001) as VARCHAR(30000)) as orig,
	Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 120001 for 30000) as VARCHAR(30000)) as compr
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 120001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 120001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 120001 and 150000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 150001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 150001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 150001 and 180000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 180001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 180001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 180001 and 210000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 210001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 210001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 210001 and 240000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 240001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 240001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 240001 and 270000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 270001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 270001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 270001 and 300000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 300001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 300001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 300001 and 330000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 330001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 330001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 330001 and 360000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 360001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 360001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 360001 and 390000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 410001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 410001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 410001 and 440000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 440001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 440001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 440001 and 470000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 470001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 470001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 470001 and 500000
;

Lock row for access
Select
	s.RetailstoreID,
	s.WorkstationID,
	s.SequenceNr,
	s.ReceiptSequenceNr,
	s.TS,
	s.TransactionTypecode,
	s.BusinessDayID,
	s.OperatorID,
	s.Filename
From Archv_POS.From_Sonic2 as s
Inner Join PRStgAxBOArchT.Stg_ArchPOS as t
On s.RetailstoreID = t.RetailstoreID
and s.WorkstationID = t.WorkstationID
and s.SequenceNr = t.SequenceNr
and s.BusinessDayID = t.BusinessDayID
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and Cast(substring(s.Xmlfile from 500001 for 30000) as VARCHAR(30000)) <> Cast(substring(ARCHV_POS.clobuncmpr(t.Xmlfile) from 500001 for 30000) as VARCHAR(30000))
and characters(s.Xmlfile) between 500001 and 530000
;

Lock row for access
Select count(*)
From Archv_POS.From_Sonic2 as s
Where  s.BusinessDayID between date '2010-03-01' and '2010-03-31'
and characters(s.Xmlfile) > 530000
;

