#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: startAgent.sh 29387 2019-10-30 16:41:04Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-10-30 17:41:04 +0100 (ons, 30 okt 2019) $
# Last Revision    : $Revision: 29387 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/site/jenkins/startAgent.sh $
# --------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
#
# startAgent.sh - start jenkins agent for remote builds
#

#
# include .profile from the home folder
#
cd $HOME
. ./.profile;

#
# change working folder to jenkins subfolder and include .profile
#
cd ./jenkins;
. ./.profile;

#
# start jenins agent for remote builds
#
# -Djava.io.tmpdir - where temporary files are created, like the shell script created for the builds
# -Dhudson.util.ProcessTree.disable - disable process tree to avoid bug on AIX
# -jar - the jar file to execute
# -workdir - where all the working files are created, like the remoting subfolder where all the remote logging is written
#
/usr/java8_64/bin/java -Djava.io.tmpdir=$HOME/jenkins/tmp -Dhudson.util.ProcessTree.disable=true -jar agent.jar -workDir $HOME/jenkins
stat=$?

exit $stat
