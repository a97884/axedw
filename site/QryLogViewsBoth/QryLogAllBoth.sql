REPLACE  VIEW SYS_MGMT.QryLogAllBoth
AS
SELECT 
     QueryID,
'NTP' as SystemGeneration,
cast(starttime as date) as logdate,
     ProcID,
     CollectTimeStamp,
     UserID,
     UserName,
     DefaultDatabase,
     AcctString,
     ExpandAcctString,
     SessionID,
     LogicalHostID,
     RequestNum,
     InternalRequestNum,
     LogonDateTime,
     AcctStringTime,
     AcctStringHour,
     AcctStringDate,
     LogonSource,
     AppID,
     ClientID,
     ClientAddr,
     QueryBand,
     ProfileID,
     StartTime,
     FirstStepTime,
     FirstRespTime,
     ElapsedTime,
  cast(
 extract(hour from ElapsedTime) * 60 * 60 +
 extract(minute from ElapsedTime) * 60 +
 extract(second from ElapsedTime) 
 as decimal(9,0))
  as ElapsedSeconds,
     NumSteps,
     NumStepswPar,
     MaxStepsInPar,
     NumResultRows,
     TotalIOCount,
     AMPCPUTime,
     ParserCPUTime,
     UtilityByteCount,
     UtilityRowCount,
     ErrorCode,
     ErrorText,
     WarningOnly,
     AbortFlag,
     CacheFlag,
     StatementType,
     StatementGroup,
     QueryText,
     NumOfActiveAMPs,
     MaxAmpCPUTime,
     MaxCPUAmpNumber,
     MinAmpCPUTime,
     MaxAmpIO,
     MaxIOAmpNumber,
     MinAmpIO,
     SpoolUsage,
     LSN,
     EstResultRows,
     EstProcTime,
     EstMaxRowCount,
     TDWMEstMemUsage,
     AMPCPUTimeNorm,
     ParserCPUTimeNorm,
     MaxAmpCPUTimeNorm,
     MaxCPUAmpNumberNorm,
     MinAmpCPUTimeNorm,
--     ParExpreqTime,
     ParserExpReq,
     ProxyUser,
     ProxyRole,
     SessionTemporalQualifier,
     CalendarName,
     CPUDecayLevel, 
     IODecayLevel,
     TacticalCPUException, 
     TacticalIOException,
     SeqRespTime,
     ReqIOKB,
     ReqPhysIO,
     ReqPhysIOKB,
     DataCollectAlg,
     CallNestingLevel,
     NumRequestCtx,
     KeepFlag,
     QueryReDriven,
     ReDriveKind,
     LastRespTime,
     DisCPUTime,
     Statements,
     DisCPUTimeNorm,
     TxnMode,
     RequestMode,
     DBQLStatus,
     NumFragments,
     VHLogicalIO,
     VHPhysIO,
     VHLogicalIOKB,
     VHPhysIOKB,
     LockDelay,
     PersistentSpool
      FROM DBC.QryLog
UNION ALL
SELECT 
     QueryID,
'NTP' as SystemGeneration,
  Logdate,
     ProcID,
     CollectTimeStamp,
     UserID,
     UserName,
     DefaultDatabase,
     AcctString,
     ExpandAcctString,
     SessionID,
     LogicalHostID,
     RequestNum,
     InternalRequestNum,
     LogonDateTime,
     AcctStringTime,
     AcctStringHour,
     AcctStringDate,
     LogonSource,
     AppID,
     ClientID,
     ClientAddr,
     QueryBand,
     ProfileID,
     StartTime,
     FirstStepTime,
     FirstRespTime,
     ElapsedTime,
  cast(
 extract(hour from ElapsedTime) * 60 * 60 +
 extract(minute from ElapsedTime) * 60 +
 extract(second from ElapsedTime) 
 as decimal(9,0))
  as ElapsedSeconds,
     NumSteps,
     NumStepswPar,
     MaxStepsInPar,
     NumResultRows,
     TotalIOCount,
     AMPCPUTime,
     ParserCPUTime,
     UtilityByteCount,
     UtilityRowCount,
     ErrorCode,
     ErrorText,
     WarningOnly,
     AbortFlag,
     CacheFlag,
     StatementType,
     StatementGroup,
     QueryText,
     NumOfActiveAMPs,
     MaxAmpCPUTime,
     MaxCPUAmpNumber,
     MinAmpCPUTime,
     MaxAmpIO,
     MaxIOAmpNumber,
     MinAmpIO,
     SpoolUsage,
     LSN,
     EstResultRows,
     EstProcTime,
     EstMaxRowCount,
     TDWMEstMemUsage,
     AMPCPUTimeNorm,
     ParserCPUTimeNorm,
     MaxAmpCPUTimeNorm,
     MaxCPUAmpNumberNorm,
     MinAmpCPUTimeNorm,
--     ParExpreqTime,
     ParserExpReq,
     ProxyUser,
     ProxyRole,
     SessionTemporalQualifier,
     CalendarName,
     CPUDecayLevel, 
     IODecayLevel,
     TacticalCPUException, 
     TacticalIOException,
     SeqRespTime,
     ReqIOKB,
     ReqPhysIO,
     ReqPhysIOKB,
     DataCollectAlg,
     CallNestingLevel,
     NumRequestCtx,
     KeepFlag,
     QueryReDriven,
     ReDriveKind,
     LastRespTime,
     DisCPUTime,
     Statements,
     DisCPUTimeNorm,
     TxnMode,
     RequestMode,
     DBQLStatus,
     NumFragments,
     VHLogicalIO,
     VHPhysIO,
     VHLogicalIOKB,
     VHPhysIOKB,
     LockDelay,
     PersistentSpool
      FROM SYS_MGMT.QryLog
UNION ALL
SELECT 
     QueryID,
'OTP' as SystemGeneration,
  Logdate,
     ProcID,
     CollectTimeStamp,
     UserID,
     UserName,
     DefaultDatabase,
     AcctString,
     ExpandAcctString,
     SessionID,
     LogicalHostID,
     RequestNum,
     InternalRequestNum,
     LogonDateTime,
     AcctStringTime,
     AcctStringHour,
     AcctStringDate,
     LogonSource,
     AppID,
     ClientID,
     ClientAddr,
     QueryBand,
     ProfileID,
     StartTime,
      FirstStepTime,
     FirstRespTime,
     ElapsedTime,
  cast(
 extract(hour from ElapsedTime) * 60 * 60 +
 extract(minute from ElapsedTime) * 60 +
 extract(second from ElapsedTime) 
 as decimal(9,0))
  as ElapsedSeconds,
     NumSteps,
     NumStepswPar,
     MaxStepsInPar,
     NumResultRows,
     TotalIOCount,
     AMPCPUTime,
     ParserCPUTime,
     0 as UtilityByteCount,
     UtilityRowCount,
     ErrorCode,
     ErrorText,
     WarningOnly,
     AbortFlag,
     CacheFlag,
     StatementType,
     StatementGroup,
     QueryText,
     NumOfActiveAMPs,
     MaxAmpCPUTime,
     MaxCPUAmpNumber,
     MinAmpCPUTime,
     MaxAmpIO,
     MaxIOAmpNumber,
     MinAmpIO,
     SpoolUsage,
     LSN,
     EstResultRows,
     EstProcTime,
     EstMaxRowCount,
     0 as TDWMEstMemUsage,
     AMPCPUTimeNorm,
     ParserCPUTimeNorm,
     MaxAmpCPUTimeNorm,
     MaxCPUAmpNumberNorm,
     MinAmpCPUTimeNorm,
--     ParExpreqTime,
     0 as ParserExpReq,
     '' as ProxyUser,
     '' as ProxyRole,
     '' as SessionTemporalQualifier,
     '' as CalendarName,
     0 as CPUDecayLevel, 
     0 as IODecayLevel,
     0 as TacticalCPUException, 
     0 as TacticalIOException,
     0 as SeqRespTime,
     0 as ReqIOKB,
     0 as ReqPhysIO,
     0 as ReqPhysIOKB,
     0 as DataCollectAlg,
     0 as CallNestingLevel,
     0 as NumRequestCtx,
     '' as KeepFlag,
     '' as QueryReDriven,
     '' as ReDriveKind,
     cast(FirstRespTime + ElapsedTime as  timestamp(2)) as LastRespTime,
     0 as DisCPUTime,
     0 as Statements,
     0 as DisCPUTimeNorm,
     '' as TxnMode,
     '' as RequestMode,
     0 as DBQLStatus,
     0 as NumFragments,
     0 as VHLogicalIO,
     0 as VHPhysIO,
     0 as VHLogicalIOKB,
     0 as VHPhysIOKB,
     0 as LockDelay,
     0 as PersistentSpool
FROM
  SYS_MGMT_OTP.QryLog
;