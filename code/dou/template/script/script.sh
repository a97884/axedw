#!/bin/bash 
if [ "$DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: script.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/template/script/script.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Description
#   <description>
#
# Parameters:
#	Usage <script>.sh PMRootDir
#
# --------------------------------------------------------------------------
#
pgm="$0"
PMRootDir="$1"

############################################################################
# TO GET LOGON INFORMATION FOR BTEQ                                        #
############################################################################

PWD_FILE="$PMRootDir/PWD_BteqLogon"
echo "PWD_FILE = $PWD_FILE"

LOGON="`grep '_LOGON=' ${PWD_FILE}`"
LOGON="`echo "$LOGON" | cut -d'=' -f2`"

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	exit -1 
}
CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS=$CURRENT_TS"

LOG_FILE="$PMRootDir/log/${pgm}${CURRENT_TS}.log"
exec >"$LOG_FILE" 2>&1

echo "Logging into file: ${LOG_FILE}"
echo "PMRootDir = $PMRootDir"

if [ $# -ne 1 ] ; then
    echo "Invalid number of parameters."
	echo "Usage: ${pgm} PMRootDir"
	exit 1
fi

bteq <<-end
$LOGON
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=UPDATE PARTITIONS;' FOR SESSION;

<database statement1>
;
.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE

<database statement2>
;
.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE

.logoff
.quit 0
end
stat=$?

exit $stat
