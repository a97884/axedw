--
-- template for runorder file
--
BTQRUN	template/database/database.btq
BTQRUN	template/table/table.btq
BTQRUN	template/init/init.btq
BTQRUN	template/statistic/statistic.btq
BTQRUN	template/index/index.btq
BTQRUN	template/trigger/trigger.btq
BTQRUN	template/view/view.btq
BTQRUN	template/macro/macro.btq
BTQCOMPILE	template/procedure/procedure.btq
BTQRUN	template/view/accessview/accessview.btq
BTQLOADLOB	template/rbinary/binary.btq template/rbinary/model.bin
BTQLOADLOB2	template/execr/execr.btq template/execr/contract.execr  template/execr/operator.execr 
