#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: set_wallet_password.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/set_wallet_password.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
#   Set the wallet password for tdwallet
#
# Parameters:
#	1 = password
#	2 = old password (optional)
#	eg. secret
#	eg. secret old_secret
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Generate file for input to tdwallet
# 2.) Use expect command to send the content to tdwallet
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
if [ $# -lt 1 -o $# -gt 2 ]
then
	echo "$0: Usage: `basename $0` password [old_password]" >&2
	exit 1
fi

pwd="$1"
old_pwd="$2"

expect -i <<-end
set o "$old_pwd"
spawn tdwallet
expect "tdwallet> "
send "chgpwd\n"
expect {
	"Enter existing wallet password:"	{
							puts "Old password expected\n";
							if { \$o != "" } {
								send "${old_pwd}\r";
							} \
							else {
								puts "none supplied! Unable to continue.";
								exit 1
							};
							exp_continue
						}
	"Enter desired wallet password:"	{send "${pwd}\r"; exp_continue}
	"Reenter desired wallet password:"	{send "${pwd}\r"}
}

expect {
	"Wallet password changed.  Remember your wallet password."	{puts "Wallet password changed."; exp_continue}
	"ERROR:" 	{puts "Failed to change Wallet password."; exit 1}
	"tdwallet>"	{send "exit 0\n"}
}

end
status=$?

echo ""

exit $status
