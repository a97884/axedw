#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
export BUILD_TRACE="${BUILD_TRACE:-0}"
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: findDep.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/findDep.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2019-04-10	Teradata	Initial version
#
# --------------------------------------------------------------------------
# Description
#   Find dependencies between objects
#
# Parameters:
#	1 = space separated list of folders to analyze
#
export DEBUG="${BUILD_DEBUG}"
dupPrint=""
if [ "$1" = "-p" ]
then
        shift
        dupPrint="yes"
fi
folders=$@

deComment()
{
        sed -e 's/\(.*\)---*.*/\1/g' | awk '/\/\*/,/\*\// { printf( "\n" ); next; } { print $0 }'
}

export dir=$folders
for d in $dir
do
        if [ ! -d "${d}" ]
        then
                echo "${d} is not a directory, unable to process"
                exit 1
        fi
done
find ${dir} -name '*.btq' -print | \
        xargs egrep . /dev/null | \
        deComment | \
        egrep -i 'REPLACE|CREATE' | \
        awk -v DEBUG="${DEBUG}" -F: '
        {
                if ( DEBUG > 0 ) {
                        printf( "debug:File to process: %s\n", $1 );
                        printf( "debug:\tObject to process: %s %s %s\n", $2, $3, $4 );
                }
                split($2,words,"[ \t]+");
                oind=2;
                if ( toupper(words[2]) == "SET" || toupper(words[2]) == "MULTISET" ) {
                        oind++;
                }
                if ( toupper(words[oind]) == "TABLE" ) {
                        printf( "define:table:%s\n", words[oind+1] );
                        Tword=words[oind+1];
                        gsub( "T[.]", "VOUT.", Tword );
                        printf( "define:view:%s\n", Tword );
                }
                if ( toupper(words[oind]) == "VIEW" ) {
                        printf( "define:view:%s\n", words[oind+1] );
                }
                oind=2;
                if ( toupper(words[oind]) == "PROCEDURE" ) {
                        split(words[oind+1], pwords, "(");
                        printf( "define:procedure:%s\n", pwords[1] );
                }

        }' | \
perl -E '
my %refBy = ( "", "" );
my %uses = ( "", "" );
my $dupPrint = "no";
my $a = shift;

if ( $ENV{DEBUG} ne "0" ) {
        print "a=$a\n";
}
print "dir=$ENV{dir}\n";
if ( $a eq "yes" ) {
        $dupPrint = "yes";
        if ( $ENV{DEBUG} ne "0" ) {
                print "dupPrint changed to=$dupPrint\n";
        }
}
foreach my $line ( <STDIN> ) {
        chomp( $line );
        my @spl = split(":", $line);

        if ( $ENV{DEBUG} != "0" ) {
                print "GOT: $line\n";
                $ss = substr( $line, 0, 5 );
                if ( $ss eq "debug" )   {
                        next;
                }
        }

        my $refCmd = $spl[0];
        my $refType = $spl[1];
        my $refObject = $spl[2];

        if ( $ENV{DEBUG} != "0" ) {
                print "\trefCmd: $refCmd\n";
                print "\tRefType: $refType\n";
                print "\trefObject: $refObject\n";
        }

        $refBy{$refType . " " . $refObject}{0} = $refObject;
                $cmd = sprintf("find %s -name *.btq -print | xargs fgrep %c%s%c /dev/null | cut -d: -f1 | sort | uniq |", $ENV{dir}, 39, $refObject, 39 );
                if ( $ENV{DEBUG} != "0" ) {
                        print "cmd=$cmd\n";
                }
                open(FIND_F, $cmd);

                while (<FIND_F>) {
                        ($w1, $w2, $w3, $w4, $w5, $rest) = split;
                        if ( $ENV{DEBUG} != "0" ) {
                                print "\tGOT FROM FIND $refObject: $w1 $w2 $w3 $w4 $w5 $rest\n";
                        }
                        $refBy{$refType . " " . $refObject}{$w1} = $w1;
                        $uses{$w1}{$refType . " " . $refObject} = $refType . " " . $refObject;
                }

                close(FIND_F);

}
if ( $ENV{DEBUG} != "0" ) {
        print "dupPrint=$dupPrint\n";
        for $refObject ( keys %refBy ) {
                print "$refObject: ";
                for $usesObject ( keys % { $refBy{ $refObject } } ) {
                        print "$refBy{$refObject}{$usesObject} ";
                }
                print "\n";
        }

}
print "Objects used by:\n";
for $refObject ( keys %refBy ) {
        my @spl = split(" ", $refObject);
        if ( $refObject eq "" ) {
                next;
        }
        $nrKeys = keys % { $refBy{ $refObject } };
        $nrKeys--;
        print "\t$refObject is used by( $nrKeys ):\n";
        for $usesObject ( keys % { $refBy{ $refObject } } ) {
                $refObjectText = $refBy{$refObject}{$usesObject};
                if ( $spl[1] eq $refObjectText ) {
                        next;
                }
                print "\t";
                if ( $dupPrint eq "yes" ) {
                        print "$refObject";
                }
                print "\t$refObjectText\n";
        }
}
print "Objects uses:\n";
for $refObject ( keys %uses ) {
        if ( $refObject eq "" ) {
                next;
        }
        $nrKeys = keys % { $uses{ $refObject } };
        print "\t$refObject uses( $nrKeys ):\n";
        for $usesObject ( keys % { $uses{ $refObject } } ) {
                $refObjectText = $uses{$refObject}{$usesObject};
                print "\t";
                if ( $dupPrint eq "yes" ) {
                        print "$refObject";
                }
                print "\t\t$refObjectText\n";
        }
}
' "${dupPrint}"
