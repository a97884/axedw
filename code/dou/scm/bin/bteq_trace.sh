#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: bteq_trace.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/bteq_trace.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
#
# --------------------------------------------------------------------------
# Description
#   Run bteq with stdout and stderr output teed to temporary trace file in /tmp
#
# Parameters: same as for bteq
#
# Assumes the following environment variables are set:
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Create temp file
# 2.) Run bteq with stdout and stderr to temp file
# 3.) Exit with return code from bteq
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
tmp_dir="/tmp"
tmp_file="`mktemp ${tmp_dir}/bteq_trace.XXXXX`"
bteq $@ 2>&1 | tee $tmp_file 2>&1
status=$?
exit $status
