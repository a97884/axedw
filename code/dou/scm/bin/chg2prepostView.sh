#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: chg2prepostView.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/chg2prepostView.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# convert a TD2 view sql script to a dbadmin btq view script
#
# chg2prepostView [-o] workspace file [file]...
#
# -o = only create a _runorder.txt file
#
# Example in applDB folder: chg2prepostView.sh $HOME/git/applDB/ *.sql
#
onlyRunorder="no"
if [ "$1" = "-o" ]
then
	shift
	onlyRunorder="yes"
fi
if [ $# -lt 2 ]
then
	echo "Usage: `basename $0` workspace file [file]..." >&2
	echo "-o = only create a _runorder.txt file" >&2
	exit 1
fi
workspace="$1"
shift

#
# init pattern variables
#
sqlProlog=".SET MAXERROR 0;

DATABASE %s\$#TD2_ENVNAME#;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMIT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

";

scmHead="/*
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : \$Id:  \$
# Last Changed By  : \$Author:  \$
# Last Change Date : \$Date:  \$
# Last Revision    : \$Revision:  \$
# SCM URL          : \$HeadURL:  \$
# --------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
*/
";

sqlPostlog=".IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
COMMIT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
";

#
# process input files
# expect a .sql file and output to a .btq file
#
runorderFile="_runorder.txt"
runorderNewFile="_runorder.txt.new"
if [ -r "${runorderFile}" -a "${onlyRunorder}" = "no" ]
then
	mv -f --backup=numbered "${runorderFile}" "${runorderFile}.old" || exit 1
fi
if [ -r "${runorderNewFile}" ]
then
	mv -f --backup=numbered "${runorderNewFile}" "${runorderNewFile}.old" || exit 1
fi

baseDir="`pwd | sed -e "s,$workspace,,"`"
dbSuffix='$#TD2_ENVNAME#'

if [ "$onlyRunorder" = "no" ]
then
	for inputFile in $@
	do
		echo "Processing file $inputFile"
		if [ ! -r "${inputFile}" ]
		then
			echo "`basename $0`: Unable to read ${inputFile}" >&2
			exit 1
		elif [ "`basename "${inputFile}" .sql`" = "${inputFile}" ]
		then
			echo "`basename $0`: Expecting the input file ($inputFile) with .sql as suffix" >&2
			exit 1
		fi

		outputFile="`basename "${inputFile}" .sql`.btq"
		if [ -r "${outputFile}" ]
		then
			mv -f --backup=numbered "${outputFile}" "${outputFile}.old" || exit 1
		fi

		grep Infostamp $inputFile >/dev/null
		if [ $? -eq 0 ]
		then
			#
			# already in SCM format, nothing to do except change suffix
			#
			cat "${inputFile}" >"${outputFile}"
		else
			#
			# make .sql file into .btq SCM format
			#
			awk '
			BEGIN	{
				IGNORECASE = 1;
			}
			/replace.*view/,/;/ {
				if ( $1 == "replace" ) {
					nrFields = split( $0, words );
					for ( f in words ) {
						if ( words[f] == "view" ) {
							n = split(  words[ f + 1 ], db_tab, "." );
							db = db_tab[1];
							ci = index(db, "$#TD2_ENVNAME#");
							if ( ci > 0 ) {
								db = substr( db, 1, ci - 1 );
							}
							tab = db_tab[2];
							words[ f + 1 ] = sprintf( "%s$#TD2_ENVNAME#.%s", db, tab );
						}
					}
					printf( "%s", h );
					printf( p, db, db, tab, db, tab );
					i = 0;
					while ( i <= nrFields ) {
						printf( "%s ", words[ i ] );
						i++;
					}
				}
				else {
					print $0;
				}
				next;
			}
			/commit/	{
				printf( o );
				next;
			}
			{
				print $0;
			}
			END	{
				printf( "BTQRUN_ANSI\t%s/%s\n", b, FILENAME ) >>r;
			}
			' \
				p="$sqlProlog" \
				h="$scmHead" \
				o="$sqlPostlog" \
				r="$runorderFile" \
				b="${baseDir}" \
				dbSuffix="${dbSuffix}" \
				"${inputFile}" >"${outputFile}"
		fi

	done
fi

#
# generate list of files (TODO: sort in dependency order)
#
for file in `egrep -l -i 'replace.*view' *.btq`
do
	echo -e "BQTRUN_ANSI\t$file"
done >"${runorderNewFile}"

sed -i -e "s,\t,\t${baseDir}/," "${runorderNewFile}"

mv -f --backup=numbered "${runorderFile}" "${runorderFile}.old" || exit 1
mv -f --backup=numbered "${runorderNewFile}" "${runorderFile}" || exit 1

exit 0
