#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: run_ddl.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/run_ddl.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2011-01-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
#             Connect to database and run specified SQL file, most often a DDL
#             Also check for compilation errors when compiling a procedure
#             Do not exit on compilation warnings
#   
#
# Parameters:
# 1 - name of file to process
# 2 - RUN_FOLDER folder where all execution takes place
# 3 - Temporary folder where all temporary files are created
# 4 - session mode {BTET,ANSI,null} (optional, default = system default)
#
# Assumes the following environment variables are set:
#	Teradata system to logon: TD_SYSTEM
#	Teradata logon mechanism: TD_LOGMECH
#	Userid: TD_USER
#	Password: TD_PASSWORD
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Initialize all variables from ini files and define base functions
# 2.) Initialization
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
. buildFunctions.sh
SESSION_MODE=""
BTEQ=bteq
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
DDL_INPUT_COMPLETE="$1"
RUN_FOLDER="$2"
TMP_FOLDER="$3"
test $# = 4 && SESSION_MODE="$4"
cd ${RUN_FOLDER} || exit 1

DDL_INPUT_FILE=`basename $DDL_INPUT_COMPLETE`
DDL_TMP_PATH=$TMP_FOLDER
DDL_LOG_PATH=$TMP_FOLDER
test ! -d $TMP_FOLDER && mkdir $TMP_FOLDER

DDL_TMP_FILE=${DDL_TMP_PATH}/${DDL_INPUT_FILE}.${SCRIPT_START_DATE}.tmp
DDL_LOG_FILE=${DDL_LOG_PATH}/${DDL_INPUT_FILE}.${SCRIPT_START_DATE}.log

cat /dev/null >${DDL_TMP_FILE}
test "$SESSION_MODE" != "" && echo ".SET SESSION TRANSACTION $SESSION_MODE" >>${DDL_TMP_FILE}
echo ".logmech ${TD_LOGMECH}"        >> ${DDL_TMP_FILE}
echo ".logon ${TD_SYSTEM}/${TD_USER},${TD_PASSWORD}"        >> ${DDL_TMP_FILE}
echo ".set width 300"                                       >> ${DDL_TMP_FILE}
cat ${DDL_INPUT_COMPLETE} | sed -e "s/\$#DB_ENV#/$TD_ENVNAME/g" >> ${DDL_TMP_FILE}
test "$SESSION_MODE" = "ANSI" && echo "COMMIT WORK;" >>${DDL_TMP_FILE}

echo "Output from processing the file: $DDL_TMP_FILE"
$BTEQ -c UTF8 < ${DDL_TMP_FILE} 2>&1 | tee ${DDL_LOG_FILE}
stat=$?

ERROR_PATTERN='^SPL[0-9][0-9][0-9][0-9]:E'
WARNING_PATTERN='^SPL[0-9][0-9][0-9][0-9]:W'
if grep -i '^.COMPILE' ${DDL_TMP_FILE} >/dev/null
then
        loc_stat=0
        if grep "$WARNING_PATTERN" ${DDL_LOG_FILE} >/dev/null
        then
                echo "-------------- WARNINGS DURING COMPILE OF PROCEDURE --------------" >&2
                loc_stat=2
        fi
        if grep "$ERROR_PATTERN" ${DDL_LOG_FILE} >/dev/null
        then
                echo "************** ERRORS DURING COMPILE OF PROCEDURE **************" >&2
                loc_stat=1
        fi
        if [ $loc_stat -gt 0 ]
        then
                egrep -i "^.COMPILE|$WARNING_PATTERN|$ERROR_PATTERN" ${DDL_LOG_FILE} >&2
        fi
        if [ $loc_stat -eq 1 ]
        then
                stat=5526
        fi
fi

case $stat in
0)
		echo "BTEQ returned ok status" >&2
		;;
4)
        echo "Warnings during BTEQ execution, please check logfile: ${DDL_LOG_FILE}" >&2
        ;;
*)
        echo "Errors during BTEQ execution (RC=$stat), please check logfile: ${DDL_LOG_FILE}" >&2
        ;;
esac

exit $stat
