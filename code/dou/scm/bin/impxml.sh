#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: impxml.sh 32632 2020-06-17 14:37:23Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-17 16:37:23 +0200 (ons, 17 jun 2020) $
# Last Revision    : $Revision: 32632 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/impxml.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2011-01-10	Teradata	Initial version
# 2013-06-10	Teradata	Adjusted for RTC requirements
# 2017-06-10	Teradata	Adjusted for GIT requirements
# --------------------------------------------------------------------------
# Description
#   Import XML file to Informatica repository
#	The following commands are recognized in the list of files to import
#	IMPORT - import object from xml file
#	INFAREP - send filepath to Informatica pmrep command
#	INFACMD - send filepath to Informatica pmcmd command
#	.OS	- send arguments to Linux ksh(1)
#
# Parameters:
#	1 = list of files to import (format of the file is lines of: command filepath)
#	2 = name of folder to start processing
#	3 = temporary folder where all temporary files will be generated
#	4 = version
#	eg. import.lst /myruns /tmp
#
# Assumes the following environment variables are set:
#	INFA_REPOSITORY - Informatica repository to logon to
#	INFA_DOMAIN - Informatica domain to logon to
#	INFA_SECDOMAIN - Informatica security domain to logon to
#	INFA_SHARED - Where Infa_shared is
#	INFA_IMPORT_CONFIG - configuration file for import of objects
#	INFA_SHAREDFOLDER_PREFIX - prefix to add to target shared folders
#	Userids: INFA_USER TD2_USER ORA_USER
#	Passwords: INFA_PASSWORD TD2_PASSWORD ORA_PASSWORD
#	Environment name: TD_ENVNAME TD2_ENVNAME ORA_SCHEMA
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Initialize all variables from parameters and environment
# 2.) Initialize and connect to repository
# 3.) Read change-list
# 4.) Generate control file
# 5.) Import file
# 6.) Check result
# 7.) Logoff from repository
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
. buildFunctions.sh
file_list="$1"
run_folder="$2"
tmp_path="$3"
version="$4"
status=0
cnnstatus=0
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
IMP_LOG_FILE="${tmp_path}/`basename ${file_list}`.${SCRIPT_START_DATE}.log"

cd "${run_folder}" || exit 1

echo "Importing files from ${file_list} running in ${run_folder} using temporary folder ${tmp_path}"
echo "Running in `pwd`"
echo "INFA_DOMAIN: ${INFA_DOMAIN}"
echo "INFA_IS: ${INFA_IS}"
echo "INFA_REPOSITORY: ${INFA_REPOSITORY}"
echo "INFA_FOLDER: ${INFA_FOLDER}"
echo "INFA_SHAREDFOLDER_PREFIX: ${INFA_SHAREDFOLDER_PREFIX}"

echo "INFA_SECDOMAIN: ${INFA_SECDOMAIN}"
echo "INFA_USER: ${INFA_USER}"
echo "INFA_PASSWORD: "
echo "INFA_PASSWORD_VARIABLE: $INFA_PASSWORD_VARIABLE"
echo "INFA_SHARED: ${INFA_SHARED}"
echo "INFA_IMPORT_CONFIG: ${INFA_IMPORT_CONFIG}"
echo "TD_ENVNAME: ${TD_ENVNAME}"
echo "IMP_LOG_FILE: ${IMP_LOG_FILE}"
cat /dev/null >"${IMP_LOG_FILE}"

infaimport_errorcode="${tmp_path}/infaimport_errorcode.$version"
infacnn_errorcode="${tmp_path}/infacnn_errorcode.$version"
rm -f "${infaimport_errorcode}" "${infacnn_errorcode}"
tmp_dir="${WORKSPACE}/tmp"

(
	export INFA_REPCNX_INFO="$(mktemp ${tmp_dir}/pmrepXXXXX.cnx)"

	wfctrlfile_template="${INFA_IMPORT_CONFIG}"
	wfctrlfile="${tmp_path}/import_controlfile.xml"

	printTrace 0 "pmrep connect -r \"${INFA_REPOSITORY}\" -n \"${INFA_USER}\" -X \"${INFA_PASSWORD_VARIABLE}\" -d \"${INFA_DOMAIN}\" -s \"$INFA_SECDOMAIN\""
	pmrep connect -r "${INFA_REPOSITORY}" -n "${INFA_USER}" -X "${INFA_PASSWORD_VARIABLE}" -d "${INFA_DOMAIN}" -s "$INFA_SECDOMAIN"
		
	if [ $? -eq 0 ] ; 
	then     
		echo "Successfully connected..."
	else
		echo "PowerCenter Connect command failed..."
		exit 1
	fi

	#
	# create Informatica connections (all files ending with .cnn)
	#
	echo ""
	echo "*************** Processing connection files ***************"

	grep -i '.cnn$' "${file_list}" | while read op file
	do
		grep -v '^#' "${file}" \
		| while IFS=";" read objecttype connsystem connnode connname conntype connsubtype connusrname connusrpass connconnectstring conndbname CODE_PAGE sessionmode Wlpriority rest

		do
			rm -f "${infacnn_errorcode}"
			cnnstatus=0
			
			#
			# process connection file
			#
			echo "$objecttype;$connsystem;$connnode;$connname;$conntype;$connusrname;XXX;$connconnectstring;$conndbname;$CODE_PAGE;$sessionmode;$WLpriority;$connsubtype;$sysoperator"
			case "$objecttype" in
			"connection")
				echo ""
				echo "Creating connection $connname"
				echo ""

				#
				# set command parameters depending on the system to connect to
				#
				case "$connsystem" in
				"IDW")
					CONN_SYSTEM_FLAG="-a"
					CONN_SYSTEM="$TD_SYSTEM"
					CONN_ENVIRONMENT="$TD_ENVNAME"
					;;
				"TD2")
					CONN_SYSTEM_FLAG="-a"
					CONN_SYSTEM="$TD2_SYSTEM"
					CONN_ENVIRONMENT="$TD2_ENVNAME"
					;;
				"PR")
					CONN_SYSTEM_FLAG=""
					CONN_SYSTEM=""
					CONN_ENVIRONMENT="$ORA_ENVNAME"
					;;
				*)
					echo "$0: Unknown system type: $connsystem"
					exit 1
					;;
				esac
				if [ "$sessionmode" = "ANSI" ]
				then
					CONN_SYSTEM="${CONN_SYSTEM}_ANSI"
				fi

				#
				# set command parameters depending on connection subtype
				# connection subtype can be one of:
				#	Teradata
				#	Oracle
				#	Teradata PT Connection
				#	ODBC
				#
				typeset -l connsubtype
				case "$connsubtype" in
				"teradata")
					CONN_DBNAME_FLAG="-b"
					CONN_DBNAME="${conndbname}"
					CONN_CONNECTSTRING=""
					CONN_ENVIRONMENT_SQL_FLAG="-e "
					CONN_ENVIRONMENT_SQL="\"SET QUERY_BAND='PRIO=${WLpriority};"
					CONN_ENVIRONMENT_SQL="$CONN_ENVIRONMENT_SQL"'JOBID=\$\$Jobname;WFID=\$PMWorkflowName;SID=\$PMSessionName;MID=\$PMMappingName;'"'"
					CONN_ENVIRONMENT_SQL="$CONN_ENVIRONMENT_SQL"' UPDATE FOR SESSION;"'
					;;
				"oracle")
					CONN_DBNAME_FLAG=""
					CONN_DBNAME=""
					CONN_CONNECTSTRING="-c $connconnectstring"
					CONN_ENVIRONMENT_SQL_FLAG="-e"
					CONN_ENVIRONMENT_SQL="'alter session set current_schema = \$\$SCHEMA_NAME'"
					;;
				"teradata pt connection")
					CONN_ENVIRONMENT_SQL_FLAG=""
					CONN_ENVIRONMENT_SQL=""
					CONN_DBNAME_FLAG=""
					CONN_DBNAME=""
					;;
				"odbc")
					;;
				*)
					echo "$0: Unknown connection type: $connsubtype"
					exit 1
					;;
				esac

				#
				# set connection parameters depending on connection type
				# connection type can be one of:
				#	Relational
				#	Queue
				#	FTP
				#	Application
				#	Loader
				#
				typeset -l conntype
				case "$conntype" in
				"relational")
					;;
				"fpt")
					;;
				"application")
					;;
				"loader")
					;;
				*)
					echo "$0: Unknown connection sub type: $conntype"
					exit 1
					;;
				esac
				#
				# save connection information if attributes are specified later
				#
				ATTR_CONNNAME="${connname}"
				ATTR_CONNTYPE="${conntype}"
				ATTR_CONNSUBTYPE="${connsubtype}"
				
				#
				# check if the connection already exist
				#
				pmrep listconnections | grep -i "^${connname},${conntype}" >/dev/null
				list_status=$?
				pmrep_stat=0
				if [ $list_status -eq 0 ]
				then
					#
					# ok, connection already exists so update what we can
					#
					echo "Connection $connname already exists, will update it"
					echo pmrep updateconnection -d "${connname}" -t "${connsubtype}" -u "${connusrname}" -p "${connusrpass}" ${CONN_CONNECTSTRING} -s "${conntype}" -l "${CODE_PAGE}"
					pmrep updateconnection -d "${connname}" -t "${connsubtype}" -u "${connusrname}" -p "${connusrpass}" ${CONN_CONNECTSTRING} -s "${conntype}" -l "${CODE_PAGE}"
					pmrep_stat=$?
				else
					#
					# connection does not exist so create it
					#
					echo "Connection $connname does not exist so create it"
					echo "pmrep createconnection -s "$connsubtype" -n $connname -u $connusrname -p $connusrpass ${CONN_ENVIRONMENT_SQL_FLAG} ${CONN_ENVIRONMENT_SQL} ${CONN_DBNAME_FLAG} ${CONN_DBNAME} ${CONN_SYSTEM_FLAG} $CONN_SYSTEM  -l $CODE_PAGE ${CONN_CONNECTSTRING}"
					eval pmrep createconnection -s "\"${connsubtype}\"" -n "${connname}" -u "${connusrname}" -p "${connusrpass}" ${CONN_ENVIRONMENT_SQL_FLAG} ${CONN_ENVIRONMENT_SQL} ${CONN_DBNAME_FLAG} ${CONN_DBNAME} ${CONN_SYSTEM_FLAG} ${CONN_SYSTEM} -l "${CODE_PAGE}" ${CONN_CONNECTSTRING} 
					pmrep_stat=$?
				fi
				if [ $pmrep_stat -ne 0 ]
				then
					echo "update/createconnection ${connname} failed with code $pmrep_stat (stop processing)"
					echo "${pmrep_stat}" >"${infacnn_errorcode}"
					break
				fi
				
				echo pmrep assignpermission -o connection -t Relational -n "${connname}" -g Everyone -p rxw
				pmrep assignpermission -o connection -t Relational -n "${connname}" -g Everyone -p rxw
				pmrep_stat=$?
				if [ $pmrep_stat -ne 0 ]
				then
					echo "assignpermission for ${connname} failed with code $pmrep_stat (stop processing)"
					echo "${pmrep_stat}" >"${infacnn_errorcode}"
					break
				fi
				;;
			"attribute")
				#
				# process attributes for a connection
				#
				#
				# if the connection exists or was created update the attributes
				# else ignore the error
				#
				name="${connsystem}"
				value="${connnode}"
				echo -e "\t# Update attribute name=$name for connection $ATTR_CONNNAME with value=$value"
				echo -e "\t" pmrep updateconnection -t "${ATTR_CONNSUBTYPE}" -s "${ATTR_CONNTYPE}" -d "${ATTR_CONNNAME}" -a "${name}" -v "${value}"
				pmrep updateconnection -t "${ATTR_CONNSUBTYPE}" -s "${ATTR_CONNTYPE}" -d "${ATTR_CONNNAME}" -a "${name}" -v "${value}"
				pmrep_stat=$?
				if [ $pmrep_stat -ne 0 ]
				then
					echo "updateconnection for ${connname} failed with code $pmrep_stat (stop processing)"
					echo "${pmrep_stat}" >"${infacnn_errorcode}"
					break
				fi
				;;
			esac
		done 
		test -f "${infacnn_errorcode}" && cnnstatus="`cat "${infacnn_errorcode}"`"
		test ${cnnstatus} != 0 && break
	done
	echo "*************** End of Processing connection files ***************"
	echo ""

	echo "*************** Processing xml files to import ***************"
	#
	# Import xml files into Informatica and execute all cmd files with pm commands (all files not ending with .cmd)
	#
	egrep -v '.cnn$' "${file_list}" | while read op file rest
	do
		echo -e "\tProcessing file: $file $rest"
		rm -f "${infaimport_errorcode}"
		case "$op" in
		"IMPORT")
			printTrace 0 "IMPORT $file $rest"
			
			source_repository=`grep "<REPOSITORY NAME=\"" "${file}" | sed -e 's/.*<REPOSITORY NAME=\"//; s/\" VERSION=\(.*\)//'`
			source_notshared_folder=`grep "<FOLDER NAME=\".*SHARED=\"NOTSHARED" "${file}" | sed -e 's/.*<FOLDER NAME=\"//; s/\" GROUP=\(.*\)//'`
			source_shared_folder=`grep "<FOLDER NAME=\".*SHARED=\"SHARED" "${file}" | sed -e 's/.*<FOLDER NAME=\"//; s/\" GROUP=\(.*\)//'`
			target_repository="${INFA_REPOSITORY}"
			target_notshared_folder="`basename "${file}" | cut -d. -f3`${source_notshared_folder}" # prefix with the third part of file name (dot separated) is the Informatica shared folder name prefix
			target_shared_folder_prefix="`basename "${file}" | cut -d. -f3`" # prefix with the third part of file name
			target_shared_folder="${target_shared_folder_prefix}${source_shared_folder}"
			source_shortcut_folder=`grep "<SHORTCUT .* FOLDERNAME =\"" "${file}" | sed -e 's/.*<SHORTCUT .* FOLDERNAME =\"//; s/\" NAME =\(.*\)//'`
			target_shortcut_folder="${target_shared_folder_prefix}${source_shortcut_folder}"

			echo "source_repository=${source_repository}"
			echo "source_notshared_folder=${source_notshared_folder}"
			echo "source_shared_folder=${source_shared_folder}"
			echo "target_repository=${target_repository}"
			echo "target_notshared_folder=${target_notshared_folder}"
			echo "target_shared_folder_prefix=${target_shared_folder_prefix}"
			echo "target_shared_folder=${target_shared_folder}"
			echo "source_shortcut_folder=${source_shortcut_folder}"
			echo "target_shortcut_folder=${target_shortcut_folder}"

			foldermap=""
			if [ "${source_shared_folder}" != "" ]
			then
				#
				# source contains a shared folder, map the shared folder source and target names
				#
				foldermap="${foldermap} <FOLDERMAP SOURCEFOLDERNAME=\"${source_shared_folder}\" SOURCEREPOSITORYNAME=\"${source_repository}\" TARGETFOLDERNAME=\"${target_shared_folder}\" TARGETREPOSITORYNAME=\"${target_repository}\"/>"
			fi
			if [ "${source_notshared_folder}" != "" ]
			then
				#
				# source contains a nonshared folder, map the nonshared folder source and target names
				#
				foldermap="${foldermap} <FOLDERMAP SOURCEFOLDERNAME=\"${source_notshared_folder}\" SOURCEREPOSITORYNAME=\"${source_repository}\" TARGETFOLDERNAME=\"${target_notshared_folder}\" TARGETREPOSITORYNAME=\"${target_repository}\"/>"
			fi
			if [ "${source_shortcut_folder}" != "" ]
			then
				#
				# source contains a nonshared folder, map the nonshared folder source and target names
				#
				foldermap="${foldermap} <FOLDERMAP SOURCEFOLDERNAME=\"${source_shortcut_folder}\" SOURCEREPOSITORYNAME=\"${source_repository}\" TARGETFOLDERNAME=\"${target_shortcut_folder}\" TARGETREPOSITORYNAME=\"${target_repository}\"/>"
			fi

			cat  "${wfctrlfile_template}" | sed \
					-e "s/Updated by build process/Imported by build ${BUILD_TAG}/" \
					-e "s,<FOLDERMAP\(.*\),${foldermap}," \
					> "${wfctrlfile}"

			echo pmrep objectimport -p -i "${file}" -c "${wfctrlfile}"

			status=0
			tmp_log_file="`mktemp ${tmp_dir}/infaimportXXXXXX.log`"
			echo "pmrep objectimport -p -i ${file} -c ${wfctrlfile}"
			pmrep objectimport -p -i "${file}" -c "${wfctrlfile}" 2>&1 | tee "${tmp_log_file}"
			pmrep_status=$?
			egrep -i "Processed, [1-9][0-9]* error" "${tmp_log_file}"
			egrep_status=$?
			if [ ${egrep_status} -eq 0 ]
			then
				printTrace 0 "Error text found in output, set status=1"
				status=1
			else
				printTrace 0 "pmrep command returned code ${PIPESTATUS[0]}"
				status=${pmrep_status}
			fi
			test $status -ne 0 && echo "${status}" >"${infaimport_errorcode}"
			
			printTrace 0 "DONE(${status}): IMPORT $file $rest"
			if [ ${status} -ne 0 ]
			then
				echo "IMPORT: pmrep returned bad status: $status" >&2
				break
			fi
			;;
		"INFAREP")
			printTrace 0 "INFAREP $file $rest"
			echo pmrep run -f "${file}" $rest

			status=0
			tmp_log_file="`mktemp ${tmp_dir}/infarepXXXXXX.log`"
			pmrep run -f "${file}" $rest 2>&1 | tee "${tmp_log_file}"
			pmrep_status=$?
			egrep -i "Processed, [1-9][0-9]* error" "${tmp_log_file}"
			egrep_status=$?
			if [ ${egrep_status} -eq 0 ]
			then
				printTrace 0 "Error text found in output, set status=1"
				status=1
			else
				printTrace 0 "pmrep command returned code ${PIPESTATUS[0]}"
				status=${pmrep_status}
			fi
			test $status -ne 0 && echo "${status}" >"${infaimport_errorcode}"

			printTrace 0 "DONE(${status}): INFAREP $file $rest"
			if [ ${status} -ne 0 ]
			then
				echo "INFAREP: pmrep returned bad status: $status" >&2
				break
			fi
			;;
		"INFACMD")
			printTrace 0 "INFACMD $file $rest"
			echo pmcmd run -f "${file}" $rest

			status=0
			tmp_log_file="`mktemp ${tmp_dir}/infacmdXXXXXX.log`"
			pmcmd run -f "${file}" $rest 2>&1 | tee "${tmp_log_file}"
			pmrep_status=$?
			egrep -i "Processed, [1-9][0-9]* error" "${tmp_log_file}"
			egrep_status=$?
			if [ ${egrep_status} -eq 0 ]
			then
				printTrace 0 "Error text found in output, set status=1"
				status=1
			else
				printTrace 0 "pmrep command returned code ${PIPESTATUS[0]}"
				status=${pmrep_status}
			fi
			test $status -ne 0 && echo "${status}" >"${infaimport_errorcode}"

			printTrace 0 "DONE(${status}): INFACMD $file $rest"
			if [ ${status} -ne 0 ]
			then
				echo "INFACMD: pmcmd returned bad status: $status" >&2
				break
			fi
			;;
		".OS")
			printTrace 0 ".OS $file $rest"

			echo ksh -c "$file $rest"
			$SHELL -c "$file $rest"
			status=$?
			test $status -ne 0 && echo "${status}" >"${infaimport_errorcode}"
			printTrace 0 "DONE(${status}): .OS $file $rest"
			if [ ${status} -ne 0 ]
			then
				echo "${SHELL} returned bad status: $status" >&2
				break
			fi
			;;
		*)
			printTrace 0 "$op $file $rest"

			echo "$0: I am sorry, I do not know how to do $op" >&2
			status=1
			echo "${status}" >"${infaimport_errorcode}"
			printTrace 0 "DONE(${status}): $op $file $rest"
			
			break
			;;
		esac
	done

	echo "*************** End of Processing xml files to import ***************"
	echo ""
) 2>&1 | tee -a "${IMP_LOG_FILE}"
printTrace 0 "End of input on file ${file_list}"

printTrace 0 "infaimport_errorcode: ${infaimport_errorcode}"
printTrace 0 "infacnn_errorcode: ${infacnn_errorcode}"

status=0
cnnstatus=0
test -f "${infaimport_errorcode}" && status="`cat "${infaimport_errorcode}"`"
test -f "${infacnn_errorcode}" && cnnstatus="`cat "${infacnn_errorcode}"`"
if [ ${status} -eq 0 ]
then
	egrep -i "Processed, [1-9][0-9]* error" "${IMP_LOG_FILE}" >/dev/null
	if [ $? = 0 ]
	then
		printTrace 0 "Set status to 1 based on error output in file: ${IMP_LOG_FILE}"
		status=1
	fi

fi
printTrace 0 "exit impxml.sh with status=$status and cnnstatus=$cnnstatus"

test ${status} -ne 0 && exit ${status}
test ${cnnstatus} -ne 0 && exit ${cnnstatus}

exit 0
