#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: add2keystore.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/add2keystore.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# --------------------------------------------------------------------------
# 2017-01-10	Teradata	Initial version
#
# Description
#   Add entry to keystore in home folder
#
# Parameters:
#	1 = system name for testing the password
#	2 = name of entry
#	3 = password of entry
#	eg. DEV1_Common_trf_00001 DEV1_Common_trf_00001
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Generate file for input to tdwallet
# 2.) Use expect command to send the content to tdwallet
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
if [ $# != 3 ]
then
	echo "$0: Usage: `basename $0` system name password" >&2
	exit 1
fi

export system="$1"
export name="$2"
export password="$3"
export progpath="`dirname $0`"
export libpath="${progpath}/../lib"
export keystore="$HOME/keystore"
export encryptProgramPath="${progpath}/../ExternalSrc"
export encryptProgramClass="TJEncryptPassword.class"
export encryptProgramJava="TJEncryptPassword.java"
export CLASSPATH="${progpath}:${CLASSPATH}"
#
# if the keystore does not exist then create it with restricted permissions
#
echo "Checking for keystore: ${keystore}"
if [ ! -d "${keystore}" ]
then
	echo "Keystore ${keystore} not found, creating it..."
	mkdir -p -m 700 "${keystore}" || exit $?
fi

#
# if the password encryption program is not there or older than source then try to compile it
#
compile=no
echo "Checking if the encryption program ${progpath}/${encryptProgramClass} is available"
if [ ! -f "${progpath}/${encryptProgramClass}" ]
then
	echo "No it is not, checking if the source to the encryption program ${encryptProgramPath}/${encryptProgramJava} is available"
	if [ -f "${encryptProgramPath}/${encryptProgramJava}" ]
	then
		echo "Yes, try to compile it"
		compile=yes
	else
		echo "${encryptProgramPath}/${encryptProgramJava} not found" >&2
		echo "No it is not, cannot do anything else but exit with failure"
		exit 1
	fi
else
	if [ "${encryptProgramPath}/${encryptProgramJava}" -nt "${progpath}/${encryptProgramClass}" ]
	then
		compile=yes
	else
		echo "Yes, go ahead and encrypt"
	fi
fi

if [ "${compile}" = "yes" ]
then
	echo "Compiling and installing"
	javac "${encryptProgramPath}/${encryptProgramJava}" || exit $?
	echo "Program compiled ok, install it in ${progpath}"
	mv "${encryptProgramPath}/${encryptProgramClass}" "${progpath}" || exit $?
fi

cd "${keystore}" || exit $?

echo "Encrypting password for ${name}"
java TJEncryptPassword AES/CBC/NoPadding -default HmacSHA1 "PassKey.${name}.properties" "EncPass.${name}.properties" "${system}" "${name}" "${password}" || exit $?
echo "Encrypted password for ${name} ok"

exit 0
