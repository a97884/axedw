#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: chk_runorder.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/chk_runorder.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# compare runorder entries with files in file system
#
# The output is lines as:
# R:Only in runorder"
# F:Only in file system"
# D:Not equal"
#
# Example: chk_runorder.sh $HOME/git/applDB `pwd`/_runorder.txt
#

if [ $# -ne 2 ]
then
	echo "Usage: `basename $0` workspace runorder" >&2
	exit 1
fi

workspace="$1"
runorder="$2"
excludeRunorderPattern='^--|^CD|^[ \t]*$' 
excludeFindPattern='/\.git/|_runorder.txt' 
tmp_dir="${WORKSPACE}/tmp"

#
# check for files that exists in file system but not registered in _runorder.txt 
#
runorderFiles="`mktemp ${tmp_dir}/runorderFilesXXXXX`"
filesystemFiles="`mktemp ${tmp_dir}/filesystemFilesXXXXX`"

egrep -v "${excludeRunorderPattern}" "${runorder}" | tr ' \t' ' ' | sed -e 's/  */ /g' | tr ' ' '@' | cut -d '@' -f2 | sort >"$runorderFiles"
location="`dirname $runorder`"
find "$location" -type f -print | egrep -v "${excludeFindPattern}" | sed -e "s,${workspace}/,," | sort >"$filesystemFiles"

resultFile="`mktemp ${tmp_dir}/ResultXXXXX`"

diff -W 200 -y --suppress-common-lines "$runorderFiles" "$filesystemFiles" >"${resultFile}"

grep '<' "${resultFile}" | sed -e 's/[ \t]*<[ \t]*//' -e 's/^/R:/'
grep '>' "${resultFile}" | sed -e 's/[ \t]*>[ \t]*//' -e 's/^/F:/'
grep '|' "${resultFile}" | sed -e 's/^/D:/'

exit 0
