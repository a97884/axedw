#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: add2wallet.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/add2wallet.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-01-10	Teradata	Initial version
#
# --------------------------------------------------------------------------
# Description
#   Add entry to tdwallet
#
# Parameters:
#	1 = name of entry
#	2 = value of entry
#	eg. DEV1_Common_trf_00001 DEV1_Common_trf_00001
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Generate file for input to tdwallet
# 2.) Use expect command to send the content to tdwallet
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
if [ $# != 2 ]
then
	echo "$0: Usage: `basename $0` name value" >&2
	exit 1
fi

name="$1"
value="$2"

#name_exists="`tdwallet list | grep -i "^${name}$"`"
# commented list command since there is currently a limit of number of entires possible to list
# ERROR: Could not get list of item names because:  End of file encountered during read.
#
name_exists="$name"
expect -i <<-end
spawn tdwallet
expect "tdwallet> "
send "chgsavkey\n"
expect "Enter new passphrase:  "
send "$(date)\n"
expect "Saved-key changed.\r\ntdwallet> "
`if [ "${name_exists}" != "" ]; then echo send \\"del ${name}\\\n\\"; else echo send \\"version\\\n\\"; fi`
expect "tdwallet> "
send "addsk ${name}\n"
expect "Enter desired value for the item named \"${name}\":  "
send "${value}\r"
expect "Item named \"${name}\" added.\r\n"
expect "tdwallet> "
send "exit\n"
end

echo ""
echo "$0: ${name} `if [ "$name_exists" != "" ]; then echo "replaced"; else echo created; fi`"
exit 0
