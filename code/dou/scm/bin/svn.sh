#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: svn.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/svn.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2011-01-10	Teradata	Initial version
# 2017-10-10	Teradata	Merge several scripts into one and changed from svn diff to svn log
# --------------------------------------------------------------------------
# Description
# Get list of changed files from SVN

if [ $# != 6 ]
then
	echo "Usage: `basename $0` component snapshot_file change_list version build_all checkpoint" >&2
	exit 1
fi
. buildFunctions.sh

component="$1"
snapshot_file="$2"
change_list="$3"
version="$4"
build_all="$5"
checkpoint="$6"

snapshot_exist="true"

#
# if the component is suffixed with ampersand & (for background build) remove it
#
bck="$(echo "$component" | sed 's/.*\(.\)$/\1/')"
if [ "$bck" = '&' ]
then
	component="$(echo "$component" | sed 's/\(.*\).$/\1/')"
fi

#
# component specified, must use component specific snapshot file
# if component specific snapshot does not exists try use the generic one
# replace any slashes with a dot in component before using it as a suffix
#
file_suffix="$(echo $component | sed -e 's,/,.,g')"
component_snapshot_file="${snapshot_file}.${file_suffix}"	
component_change_list="${change_list}.${file_suffix}"

echo "Using snapshot file: $component_snapshot_file"
echo "Using changelist file: $component_change_list"

tmp_dir="${WORKSPACE}/tmp"
cd ${ROOT}/${component}	
	
if [ ! -e "${component_snapshot_file}" ]
then
	#
	# no snapshot file found, must be first time running so create it
	#		
	echo "$component has no snapshot file so we need to create"
	${SVN:-svn} info | grep '^Last Changed Rev:' | while read tag1 tag2 tag3 value; do echo $value; done > "${component_snapshot_file}" 
	if [ $? -ne 0 ]
	then
		echo "svn info to create snapshot failed" >&2
		exit 1
	fi
	snapshot_exist="false"
fi
prevRevision="$(cat "${component_snapshot_file}")"
currRevision="$(${SVN:-svn} info | grep '^Last Changed Rev:' | while read tag1 tag2 tag3 value; do echo $value; done)"
if [ $? -ne 0 ]
then
	echo "svn info to get current revision failed" >&2
	exit 1
fi

#
# compare previous snapshot with current and generate change list file
# first backup possible existing change list file
#
echo "Searching for files to process..."
ignoreProperties="$(svn help diff | grep -i "ignore properties during the operation")"
if [ "${ignoreProperties}" != "" ]
then
	ignoreProperties="--ignore-properties"
fi

test -f "${component_change_list}" && cp --backup=numbered "${component_change_list}" "${component_change_list}.${version}"						
if [ ${build_all:0:1} = "N" ] 
then
	if [ "${checkpoint}" != "" ]
	then
		prevRevision="${checkpoint}"
	fi
	if [ "${prevRevision}" != "${currRevision}" ]
	then
		echo "================================= Building ${component} changes from revision $prevRevision to $currRevision ================================="
		echo "${SVN:-svn} diff --summarize ${ignoreProperties} -r "${prevRevision}":"${currRevision}""
		nr_changes="`${SVN:-svn} diff --summarize -r "${prevRevision}":"${currRevision}" | wc -l`"
		if [ "${nr_changes}" = "0" ]
		then
			echo "No changes detected since last build in component: ${component}"
			cat /dev/null > "${component_change_list}"
			exit 0
		fi
		${SVN:-svn} diff --summarize ${ignoreProperties} -r "${prevRevision}":"${currRevision}" | \
			awk -v c="${component}" '{
				if ( $2 == "." || $2 == "" ) {
					;
				}
				else
				{
					printf( "%s\t%s/%s\n", $1, c, $2 );
				}
			}' > "${component_change_list}"
		if [ $? -ne 0 ]
		then
			echo "svn diff to create component change list failed for: ${component}" >&2
			exit 1
		fi
	else
		cat /dev/null > "${component_change_list}"
		echo "Nothing to build since previous revision $prevRevision is same as current revision $currRevision"
	fi

else		
	snv_log_status_file="$(mktemp ${tmp_dir}/svn.statusXXXXX)"
	svn_status=""
	(
		echo "${ROOT}/${component}"
		find "${ROOT}/${component}" ! -path "${ROOT}/${component}" -prune -type d -print | egrep -v "^${ROOT}$|/${component}/.svn.*"
	) | \
	while read entry
	do
	(
		cd "$entry" || continue

		#
		# find all _runorder.txt files up and down from current folder
		# and check if the files mentioned in the runorder files have changed using the SCM log
		#
		(find . -name '_runorder.txt' -print; upfind -name '_runorder.txt' -print) | xargs egrep -v '^--|^CD|^$' | \
		while IFS='' read -r op file 
		do						
			case "$op" in
				"EXPAND")							
					(cd $file && find . -type f -print | sed -e 's,^\./,,' | while read file; do echo "INSTALL $entry/$file"; done)
					;;
				*)							
					echo "$op $file" 
					;;
			esac
		done | egrep "[[:space:]]${component}/" | awk '{printf("%s/%s\n", r, $2)}' r="${ROOT}" | \
			while read file
			do
				if [ -f "${file}" ]
				then
					echo "M $file" | awk -v c="${component}" '{
						if ( $2 == "." || $2 == "" ) {
							;
						}
						else
						{
							printf( "%s\t%s/%s\n", $1, c, $2 );
						}
					}'
				else
					printTrace 0 "Warning: ${file} is mentioned in _runorder.txt but the file does not exist in $entry"
					echo "${file}" >>"${snv_log_status_file}"
				fi
			done
	)
	done > ${component_change_list}
	
	svn_status="$(cat ${snv_log_status_file} | wc -l)"
	if [ "$svn_status" = "1" -a "$(cat ${snv_log_status_file})" = "-1" ]
	then
		echo "svn diff failed" >&2
		exit 1
	fi
	if [ "$svn_status" != "0" ]
	then
		echo "Warning: ${svn_status} files found in _runorder.txt files that were not in the filesystem"
	fi
fi	

if [ "${snapshot_exist}" = "true" -o "${build_all:0:1}" != "N" ]
then
	echo "Component change list generated: ${component_change_list}"
	exit 0
fi

exit 1
