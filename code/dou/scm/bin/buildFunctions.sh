#!/bin/bash 
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp				(DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID			   : $Id: buildFunctions.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision	   : $Revision: 32502 $
# SCM URL		   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/buildFunctions.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date			Author			Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
#	Create utility functions, initialize and initialize all properties as environment variables
#
# Parameters:
#	Usage . buildFunctions.sh 
#
# --------------------------------------------------------------------------
#
set -o pipefail # set return code of a failed pipe to the failed command return code, default is the last command in pipe

export script_start_time="`date +%Y%m%d%H%M%S`"

#
# setProperties - set properties into environment from properties file by using eval command
#
setProperties()
{
	for file in $(echo $1 | tr ':' ' ')
	do
		if [ -r "$file" ]
		then
			setProperties_propertiesFile="$file"
			break
		fi
	done
	if [ ! -r "$setProperties_propertiesFile" ]
	then
		echo "Unable to continue, cannot read any properties from file list: $1"
		exit 1
	fi
			
	setProperties_properties="$(egrep -v '^[[:space:]]*#|^[[:space:]]*$' "${setProperties_propertiesFile}" | tr '=' '@' | while IFS='@' read -r setProperties_name setProperties_value
	do
		echo "export ${setProperties_name}=${setProperties_value};"
	done)"

	eval $setProperties_properties
}

#
# locateProgram - find program located in a folder that is included in the variable PATH
#
locateProgram()
{
	locateProgram_prog="$1"
	echo "$PATH" | tr ':' '\n' | while read -r locateProgram_location
	do
		locateProgram_path="${locateProgram_location}/${locateProgram_prog}"
		if [ -r "${locateProgram_path}" -a -x "${locateProgram_path}" ]
		then
			echo "${locateProgram_path}"
			break;
		fi
	done
}

#
# printTrace - print trace string on standard error
#
printTrace()
{
	if [ "${BUILD_TRACE}" = "" ]
	then
		BUILD_TRACE=0
	fi
	printTrace_level=$1
	printTrace_msg="$2"
	
	test ${BUILD_TRACE} -gt ${printTrace_level} && echo -e "Trace ${BUILD_TRACE}: ${printTrace_msg}" >&2
}

#
# getRunorder - search all _runorder.txt files upwards in folder hierarchy and get runorder entries for given pattern 
#
getRunorderEntries()
{
	startList="$1"
	startDir="$2"
	module="$3"

	#
	# give all entries in startList, normally the _runorder.txt file in current folder if it exists
	# prefix all lines with the filename separated by a :
	#
	test -r "$startList" && awk '{ printf( "%s:%s\n", l, $0 ); }' l="${startList}" "${startList}" 

	#
	# look in all _runorder.txt files up in folder hierarchy and give all entries for the module
	# prefix all lines with the filename separated by a :
	#
	dir="$(dirname $startDir)" # start with parent folder
	while true
	do
		runorderFile="${dir}/_runorder.txt"
		test -r "${runorderFile}" && awk '{ printf( "%s:%s\n", l, $0 ); }' l="${runorderFile}" "${runorderFile}"
		dir="$(dirname ${dir})"
		test "${dir}" == "." -o "${dir}" == "/" -o "${dir}" == "$(dirname $module)" && break
	done
}

#
# upfind - find a file up in folder hierarchy
#
upfind()
{
	(
		#
		# Usage: upfind -name _runorder.txt -print
		#
		while [[ $PWD != / ]] ; do
			find "$PWD"/. ! -name . -prune "$@"
			cd ..
		done
	)
}

#
# getCheckpoint - get all checkpoint information from the database for given component and export into given file
# optionally get specified checkpoint IDs
#
getCheckpoint()
{
	component="$1"
	tmp_export_file="$2"
	cpID="$3"
	
	node="${TD_SYSTEM}"
	uid="${TD_USER}"
	passwd="${TD_PASSWORD}"

	bteq <<-end
	LOGON ${node}/${uid},${passwd};
	.set width 20000
	.SET ECHOREQ ERRORONLY
	.SET QUIET ON ALL
	.set sidetitle on all
	.set underline off
	.set heading off
	.export report file=${tmp_export_file}
	SELECT
		'echo "'||
		CheckpointRow||
		CASE rank() over (PARTITION BY CheckpointPath, CheckpointID ORDER BY CheckpointPath, CheckpointID, CheckpointRowNo)
		WHEN 1 THEN '" > '
		ELSE '" >> '
		END||
		CheckpointPath||'/'||CheckpointID (TITLE '')
	FROM $#DB_ENV#$#DBADMIN_DB#.SCMCheckpoint
	WHERE CheckpointComponent = '$component'
	AND CheckpointID = CASE WHEN '$cpID' = '' THEN CheckpointID ELSE '$cpID' END
	ORDER BY
		 CheckpointPath
		,CheckpointID
		,CheckpointRowNo
		,InsertDttm
		ASC
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
	.export reset
	LOGOFF;
	EXIT;
end
}

#
# getSnapshot - get all snapshot information from the database for given component and export into given file
# optionally get specified snapshot IDs
#
getSnapshot()
{
	component="$1"
	tmp_export_file="$2"
	ssID="$3"
	
	node="${TD_SYSTEM}"
	uid="${TD_USER}"
	passwd="${TD_PASSWORD}"

	bteq <<-end
	LOGON ${node}/${uid},${passwd};
	.set width 20000
	.SET ECHOREQ ERRORONLY
	.SET QUIET ON ALL
	.set sidetitle on all
	.set underline off
	.set heading off
	.export report file=${tmp_export_file}
	SELECT
		'echo "'||
		SnapshotRow||
		'" > '||
		SnapshotPath||'/'||SnapshotID (TITLE '')
	FROM $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot
	WHERE SnapshotComponent = '$component'
	AND SnapshotID = CASE WHEN '$ssID' = '' THEN SnapshotID ELSE '$ssID' END
	ORDER BY
		 SnapshotPath
		,SnapshotID
		,InsertDttm
		ASC
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
	.export reset
	LOGOFF;
	EXIT;
end
}

#
# putCheckpoint - put all checkpoint information from the file system from given file into the database
#
putCheckpoint()
{
	component="$1"
	import_file="$2"
	cpID="$3"

	node="${TD_SYSTEM}"
	uid="${TD_USER}"
	passwd="${TD_PASSWORD}"

	#
	# create temporary file and add rownumber in front of each line
	#
	tmp_import_file="$(mktemp ${ROOT}/tmp/${cpID}.imp)"
	nl -ba -s'|' "${import_file}" >"${tmp_import_file}"

	#
	# get timestamp from basename import_file: checkpoint.YYYYMMDDHHMMSS.module*
	#
	cpTS="$(echo "$(basename ${import_file})" | while read d rest; do echo ${d:11:14}; done)"

	bteq <<-end
	LOGON ${node}/${uid},${passwd};
	.set width 20000
	.SET ECHOREQ ERRORONLY
	.SET QUIET ON ALL

	--
	-- alter tabel to current and ignore if online loggig is on or reconciliation issues occurs
	--
	ALTER TABLE $#DB_ENV#$#DBADMIN_DB#.SCMCheckpoint TO CURRENT WITH DELETE;
	.IF ERRORCODE = 7595 THEN .GOTO CONTINUEWITHCHECKPOINT
	.IF ERRORCODE = 9247 THEN .GOTO CONTINUEWITHCHECKPOINT
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

	.LABEL CONTINUEWITHCHECKPOINT
	CREATE VOLATILE TABLE #CheckpointRow#V1
	,NO LOG
	AS (
		SELECT
			CheckpointID
			,CheckpointDttm
			,CheckpointPath
			,CheckpointComponent
			,CheckpointRowNo
			,CheckpointRow
		FROM  $#DB_ENV#$#DBADMIN_DB#.SCMCheckpoint
		)
	WITH NO DATA
	ON COMMIT PRESERVE ROWS
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

	.import vartext file=${tmp_import_file}
	.REPEAT *
	USING (CheckpointRowNo VARCHAR(100), CheckpointRow VARCHAR(10000))
	INSERT INTO #CheckpointRow#V1
	(
		CheckpointID
		,CheckpointDttm
		,CheckpointPath
		,CheckpointComponent
		,CheckpointRowNo
		,CheckpointRow
	)
	VALUES
	(
		'$cpID'
		,TO_TIMESTAMP('$cpTS','YYYYMMDDHH24MISS')
		,'${import_file}'
		,'${component}'
		,:CheckpointRowNo
		,:CheckpointRow
	)
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
	.import reset

	MERGE INTO $#DB_ENV#$#DBADMIN_DB#.SCMCheckpoint
	AS t1
	USING (
		SELECT
			s0.CheckpointID
			,s0.CheckpointDttm
			,s0.CheckpointPath
			,s0.CheckpointComponent
			,s0.CheckpointRowNo
			,s0.CheckpointRow
		FROM #CheckpointRow#V1 AS s0
		LEFT OUTER JOIN $#DB_ENV#$#DBADMIN_DB#.SCMCheckpoint AS t0
		ON t0.CheckpointId = s0.CheckpointId
		AND t0.CheckpointRowNo = s0.CheckpointRowNo
		AND t0.CheckpointDttm = s0.CheckpointDttm
		WHERE	t0.CheckpointId IS NULL
		OR	COALESCE(TRIM(t0.CheckpointPath),'') <> s0.CheckpointPath
		OR	COALESCE(TRIM(t0.CheckpointComponent),'') <> s0.CheckpointComponent
		OR	COALESCE(TRIM(t0.CheckpointRow),'') <> s0.CheckpointRow
	) AS s1
	ON t1.CheckpointId = s1.CheckpointId
	AND t1.CheckpointRowNo = s1.CheckpointRowNo
	AND t1.CheckpointDttm = s1.CheckpointDttm
	WHEN MATCHED THEN UPDATE
		SET	CheckpointPath = s1.CheckpointPath
			,CheckpointComponent = s1.CheckpointComponent
			,CheckpointRow = s1.CheckpointRow
	WHEN NOT MATCHED THEN INSERT
	(
		CheckpointID
		,CheckpointDttm
		,CheckpointPath
		,CheckpointComponent
		,CheckpointRowNo
		,CheckpointRow
	)
	VALUES
	(
		s1.CheckpointID
		,s1.CheckpointDttm
		,s1.CheckpointPath
		,s1.CheckpointComponent
		,s1.CheckpointRowNo
		,s1.CheckpointRow
	)
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
	LOGOFF;
	EXIT 0;
end
		rm -f "${tmp_import_file}"
}

#
# putSnapshot - put all snapshot information from the file system from given file into the database
#
putSnapshot()
{
	component="$1"
	import_file="$2"
	ssID="$3"

	node="${TD_SYSTEM}"
	uid="${TD_USER}"
	passwd="${TD_PASSWORD}"

	#
	# get timestamp from basename import_file: latest_snapshot.module.YYYYMMDDHHMMSS
	#
	cpTS="$(echo "$(basename ${import_file})" | sed -e 's/.*\.\(.*\)/\1/')"

	bteq <<-end
	LOGON ${node}/${uid},${passwd};
	.SET width 20000
	.SET ECHOREQ ERRORONLY
	.SET QUIET ON ALL

	--
	-- alter tabel to current and ignore if online loggig is on or reconciliation issues occurs
	--
	ALTER TABLE $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot TO CURRENT WITH DELETE;
	.IF ERRORCODE = 7595 THEN .GOTO CONTINUEWITHSNAPSHOT
	.IF ERRORCODE = 9247 THEN .GOTO CONTINUEWITHSNAPSHOT
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

	.LABEL CONTINUEWITHSNAPSHOT
	CREATE VOLATILE TABLE #SnapshotRow#V1
	,NO LOG
	AS (
		SELECT
			SnapshotID
			,SnapshotDttm
			,SnapshotPath
			,SnapshotComponent
			,SnapshotRow
		FROM  $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot
		)
	WITH NO DATA
	ON COMMIT PRESERVE ROWS
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

	.import vartext file=${import_file}
	.REPEAT *
	USING (SnapshotRow VARCHAR(10000))
	INSERT INTO #SnapshotRow#V1
	(
		SnapshotID
		,SnapshotDttm
		,SnapshotPath
		,SnapshotComponent
		,SnapshotRow
	)
	VALUES
	(
		'$ssID'
		,TO_TIMESTAMP('$cpTS','YYYYMMDDHH24MISS')
		,'${import_file}'
		,'${component}'
		,:SnapshotRow
	)
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
	.import reset

	MERGE INTO $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot
	AS t1
	USING (
		SELECT
			s0.SnapshotID
			,s0.SnapshotDttm
			,s0.SnapshotPath
			,s0.SnapshotComponent
			,s0.SnapshotRow
		FROM #SnapshotRow#V1 AS s0
		LEFT OUTER JOIN $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot AS t0
		ON t0.SnapshotId = s0.SnapshotId
		AND t0.SnapshotDttm = s0.SnapshotDttm
		WHERE	t0.SnapshotId IS NULL
		OR	COALESCE(TRIM(t0.SnapshotPath),'') <> s0.SnapshotPath
		OR	COALESCE(TRIM(t0.SnapshotComponent),'') <> s0.SnapshotComponent
		OR	COALESCE(TRIM(t0.SnapshotRow),'') <> s0.SnapshotRow
	) AS s1
	ON t1.SnapshotId = s1.SnapshotId
	AND t1.SnapshotDttm = s1.SnapshotDttm
	WHEN MATCHED THEN UPDATE
		SET	SnapshotPath = s1.SnapshotPath
			,SnapshotComponent = s1.SnapshotComponent
			,SnapshotRow = s1.SnapshotRow
	WHEN NOT MATCHED THEN INSERT
	(
		SnapshotID
		,SnapshotDttm
		,SnapshotPath
		,SnapshotComponent
		,SnapshotRow
	)
	VALUES
	(
		s1.SnapshotID
		,s1.SnapshotDttm
		,s1.SnapshotPath
		,s1.SnapshotComponent
		,s1.SnapshotRow
	)
	;
	.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
	LOGOFF;
	EXIT 0;
end
}
