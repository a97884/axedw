#!/usr/bin/perl
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: getsvninfo.pl 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/getsvninfo.pl $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# Find changed files and output information on stdout

use strict;
use warnings;

my $input_file=$ARGV[0];
my $component=$ARGV[1];

local $/; # enable slurp

my @changelist = split /\./, $input_file;

open (my $difflist, "<", $input_file) or die "Can't open $input_file: $!";

my @changes = split /\n/, <$difflist>;
my @commit;
$commit[0] = " ";
$commit[1] = "";
$commit[2] = "";
$commit[3] = "";

for my $change (@changes) {
	if (length($change) > 0) {
		if (substr($change, 0, 1) eq ">") {
			@commit = split /;/, $change;		
		} else {
			my @file = split /\t/, $change;
			if ($file[0] ne "D") {
				print "M;" . $component . "/" . $file[1] . ";" . substr($commit[0],1) . ";" . $commit[1] . ";" . $commit[2] . ";" . $commit[3] . ";" . $component . "\n"
			}
		}
	}
}
close $difflist or die "$difflist: $!";

