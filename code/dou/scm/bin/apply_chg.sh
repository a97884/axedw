#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
export BUILD_TRACE="${BUILD_TRACE:-0}"
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: apply_chg.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/apply_chg.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2011-01-10	Teradata	Initial version
# 2013-06-10	Teradata	Adjusted for RTC requirements
# 2017-06-10	Teradata	Adjusted for GIT requirements
# 2017-10-10	Teradata	Merged module specific scripts into one
#
# --------------------------------------------------------------------------
# Description
#   Apply changes
#
# Parameters:
#	1 = change log from SCM
#	2 = name of folder to start processing
#	3 = version
#	4 = database environment
#	5 = temporary folder where all temporary files will be generated
#	6 = run folder where all bteq runs will take place
#	7 = level of folders nested during recursion
#	8 = name of checkpoint file to use
#	9 = name of module to build
#	eg. changes.txt dbadmin 2 DBENV /tmp /myruns "" /tmp/checkpoint dbadmin
#
# Assumes the following environment variables are set:
#	TD_SYSTEM - Teradata system to logon to
#	TD_LOGMECH - Teradata logon mechanism
#	TD_USER - Userid for Teradata logon
#	TD_PASSWORD - Password for Teradata logon
#   This will be the default system and credentials, can be changed in the file _runorder.txt 
#
#	BUILD_TARGET_SYSTEM - The target system to build (TD_SYSTEM or TD2_SYSTEM)
#	BUILD_RESTART_FROM_POINT_FAILURE - if Y a restart will be done by excluding all entries already in the checkpoint file
#	INFA_FOLDER - if DEFAULT use folder name from object to import. This can be changed using INFAFOLDER in _runorder.txt
#	INFA_SHAREDFOLDER_PREFIX - use as prefix on shared folder name when importing Informatica objects, also follows INFAFOLDER from _runorder.txt if DEFAULT
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Initialize all variables from ini files and define base functions
# 2.) Initialization
# 3.) Read change-list
# 4.) Recursively traverse folder structure
# 5.) do variable substitutions
# 6.) Generate code to execute
# 7.) execute code
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
. buildFunctions.sh
if [ $# != 9 ]
then
	echo "$0: Usage: `basename $0` change_list entry version ENVNAME tmp_path run_folder level checkpointfile module" >&2
	exit 1
fi
umask 022 # to make files and folders readable by everyone
PrivateKeyFile="$HOME/.ssh/$(uname -n)" # for remote installation

change_list="$1"
entry="$2"
version="$3"
ENVNAME="$4"
tmp_path="$5"
run_folder="$6"
level="$7"
checkpointfile="$8"
module="$9"
export restart_from_checkpoint=${BUILD_RESTART_FROM_POINT_FAILURE:-N}

trap "if [ \"\${generatedModules}\" != \"\" ]
then
        rm -f \"\${run_list}\"
fi" 0

cd "${run_folder}" || exit 1

#
# make sure environment name is uppercase for database scripts
#
export ENVNAME="`echo "${ENVNAME}" | tr '[a-z]' '[A-Z]'`"

#
# if _runorder.txt file exists check if it in Unix format or exit with failure
#
export run_list="$entry/_runorder.txt"
test ! -d $tmp_path && mkdir $tmp_path
if [ -r "${run_list}" ]
then
	cat -v "${run_list}" | grep '\^M' >/dev/null
	if [ $? -eq 0 ]
	then
		echo "ERROR: the file ${run_list} contains DOS EOL, please add eol-style=Native as Subversion property"
		exit 1
	fi
fi

#
# support for three methods for the builds (controlled using directives in _runorder.txt files):
#   btq - Teradata database objects installation, file installation and file execution
#   infa - Informatica repository objects import using xml files and connection files
#   ora - oracle database objects installation
#
#
# replace any slashes with a dot in component before using it as a suffix
#
file_suffix="$(echo $module | sed -e 's,/,.,g')"

export run_file_btq="$tmp_path/_runfile.${file_suffix}.$version.btq"
export run_file_infa="$tmp_path/_runfile.${file_suffix}.$version.infa"
export run_file_ora="$tmp_path/_runfile.${file_suffix}.$version.ora"

export procedure_errorcode="${tmp_path}/procedure_errorcode.$version"
export execution_errorcode="${tmp_path}/execution_errorcode.$version"
destination_folder="" # can be set using INSTDIR <folder> but only local for one level _runorder.txt, default is $INFA_SHARED/$module
replace_variables="Y" # can be set using INSTCONF NOREPLVARS,REPLVARS but only local for one level _runorder.txt, default is Y
build_remote="N" # can be set using INSTCONF BUILDREMOTE,BUILDLOCAL but only local for one level _runorder.txt, default is N

echo "change_list=${change_list}"
echo "entry=${entry}"
echo "version=${version}"
echo "ENVNAME=${ENVNAME}"
echo "tmp_path=${tmp_path}"
echo "run_folder=${run_folder}"
echo "level=${level}"
echo "checkpointfile=${checkpointfile}"
echo "restart_from_checkpoint=${restart_from_checkpoint}"

tmp_dir="${WORKSPACE}/tmp"

if [ "$level" = "" ]
then
	echo "Generate commands to build Teradata objects into: $run_file_btq"
	cat /dev/null >"${run_file_btq}"
	echo "Generate commands to build Informatica objects into: $run_file_infa"
	cat /dev/null >"${run_file_infa}"
	echo "Generate commands to build Oracle objects into: $run_file_ora"
	cat /dev/null >"${run_file_ora}"

	#
	# set default session mode, this can be changed by appending _ANSI after the _runorder directive in column 1 in the _runorder file
	#
	export SM_FILE=${tmp_dir}/$$.sessmode
	echo "BTET" >$SM_FILE
	#
	# set default target system, this can be changed by adding TARGET_SYSTEM in the _runorder in column 1 in the _runorder file
	# after the TARGET_SYSTEM there must be specified TARGET_USER and TARGET_PASSWORD
	# e.g.
	#	TARGET_SYSTEM TD2_SYSTEM TD2_USER TD2_PASSWORD [TD2_LOGMECH]
	#	or
	#	TARGET_SYSTEM TD_SYSTEM TD_USER TD_PASSWORD [TD_LOGMECH]
	#
	# the variables TD2_SYSTEM, TD2_LOGMECH, TD2_USER, TD2_PASSWORD, TD_SYSTEM, TD_LOGMECH, TD_USER and TD_PASSWORD must be defined as build properties with proper values
	#
	# default is to connect to TD_SYSTEM with TD_USER and TD_PASSWORD
	#
	export TARGET_FILE=${tmp_dir}/$$.target_system
	echo "TD_SYSTEM TD_USER TD_PASSWORD TD_LOGMECH" >$TARGET_FILE
	#
	# set default installation system for files
	# this can be changed by adding INSTALL_SYSTEM in the _runorder.txt in column 1
	# e.g.
	#    INSTALL_SYSTEM S3LOADER_SYSTEM S3LOADER_USER S3LOADER_PASSWORD
	# and back to default: INFA_SHARED_NODE (current local server)
	#    INSTALL_SYSTEM INFA_SHARED_NODE
	#
	export INSTALL_FILE=${tmp_dir}/$$.install_system
	echo "INSTALL_SYSTEM DEFAULT" >$INSTALL_FILE
	#
	# AIX does not support indirect references using typeset so we have to evaluate the content of the variables using eval
	#
	if [ "${BUILD_OS}" = "AIX" ]
	then
		TARGET_SYSTEM=TD_SYSTEM
		TARGET_LOGMECH=TD_LOGMECH
		TARGET_USER=TD_USER
		TARGET_PASSWORD=TD_PASSWORD
		export TARGET_SYSTEM TARGET_LOGMECH TARGET_USER TARGET_PASSWORD
		eval "T_S=\"\${$TARGET_SYSTEM}\""
		eval "T_L=\"\${$TARGET_LOGMECH}\""
		eval "T_U=\"\${$TARGET_USER}\""
		eval "T_P=\"\${$TARGET_PASSWORD}\""
		echo ".logoff" >$run_file_btq
		echo ".logmech ${T_L}" >>$run_file_btq
		echo ".logon ${T_S}/${T_U},${T_P}" >>$run_file_btq
		
		# using PS1 as placeholder, user and password not used as default
		INSTALL_SYSTEM="INFA_SHARED_NODE"
		INSTALL_USER="PS1"
		INSTALL_PASSWORD="PS1"
		export INSTALL_SYSTEM INSTALL_USER INSTALL_PASSWORD
	else
		typeset -n TARGET_SYSTEM=TD_SYSTEM
		typeset -n TARGET_LOGMECH=TD_LOGMECH
		typeset -n TARGET_USER=TD_USER
		typeset -n TARGET_PASSWORD=TD_PASSWORD
		export TARGET_SYSTEM TARGET_USER TARGET_PASSWORD
		echo ".logoff" >$run_file_btq
		echo ".logmech ${TARGET_LOGMECH}" >>$run_file_btq
		echo ".logon ${TARGET_SYSTEM}/${TARGET_USER},${TARGET_PASSWORD}" >>$run_file_btq

		# using PS1 as placeholder, user and password not used as default
		typeset -n INSTALL_SYSTEM=INFA_SHARED_NODE
		typeset -n INSTALL_USER="PS1"
		typeset -n INSTALL_PASSWORD="PS1"
		export INSTALL_SYSTEM INSTALL_USER INSTALL_PASSWORD
	fi

	level=0

	#
	# if we are doing a restart then reset checkpoint file else create a new empty file
	#
	if [ "$restart_from_checkpoint" = "Y" ]
	then
		#
		# we are restarting from the checkpoint
		# reset checkpointfile to the latest version
		#
		checkpoint_folder="`dirname ${checkpointfile}`"
		old_checkpointfile="`ls -tr ${checkpoint_folder}/checkpoint.*.${file_suffix} | tail -1`"
		if [ -r "$old_checkpointfile" ]
		then
			#
			# preserve old checkpoint file
			#
			cp -p ${old_checkpointfile} ${old_checkpointfile}.restarted_by.${version}
			#
			# restart by using the old checkpoint file
			#
			cp ${old_checkpointfile} ${checkpointfile}
			echo "`basename $0`: Restart requested and checkpoint file found: ${old_checkpointfile}"
		else
			echo "`basename $0`: Restart requested but no checkpoint file found in ${checkpoint_folder}"
			exit 1
		fi
	else
		cat /dev/null >"${checkpointfile}"
	fi
fi

echo "Processing ${run_list} and all parent _runorder.txt files"
if [ ! -r "${run_list}" -a "${BUILD_GENERATE_MISSING_RUNORDER}" = "Y" ]
then
	echo "The runlist ${run_list} not found, generating a temporary runlist for all subfolders:"
	generatedModules="`(find "${entry}" ! -path "${entry}" -prune -type d ! -name '.*' -a ! -name tmp -print | while read dir; do basename $dir; done)`"
	if [ "${generatedModules}" != "" ]
	then
		for moduleToGenerate in ${generatedModules}
		do
			modulePath="`echo "${moduleToGenerate}" | perl -p -E "s,${WORKSPACE}/*,,"`"
			if [ "${BUILD_OS}" = "AIX" ]
			then
				perl -e "printf( \"CD\t${modulePath}\n\" )"
			else
				echo -e "CD\t${modulePath}"
			fi
		done | sort >"${run_list}"
		cat "${run_list}"
	else
		echo "No modules to build was found"
	fi
elif [ ! -r "${run_list}" ]
then
	echo "Warning: missing runlist file: $run_list" # can continue and look up in folder hierarchy by using getRunorderEntries
fi

getRunorderEntries "${run_list}" "${PWD}" "${module}" | grep -n . | tr : ' ' | while read lineNo runlistfile f1 f2 f3 f4 f5 rest
do

	printTrace 0 "got: runlistfile:$runlistfile, lineNo:$lineNo, f1:$f1, f2:$f2, f3:$f3 f4:$f4 f5:$f5 rest:$rest"

	if [ "`echo "$f1" | cut -c1-2`" = "--" ]
	then
		# comment row found, skip this line and continue to next
		continue
	fi

	if [ "$f2" = "" ]
	then
		printTrace 0 "command $f1 without any parameter, ignored"
		continue
	fi

	# check if the $f2 "path" exist in the change-list or we have a TARGET_SYSTEM directive, else no need to process
	file_to_check="$f2"
	if [ "$f1" = "EXECUTE" ]
	then
		#
		# for EXECUTE we check if the first parameter points to a files that has been changed
		#
		file_to_check="$f3"
	fi
	#
	# remove the module name from file paths
	#
	f2="`echo "$f2" | sed -e "s,^${module}/,,"`"
	f3="`echo "$f3" | sed -e "s,^${module}/,,"`"
	f4="`echo "$f4" | sed -e "s,^${module}/,,"`"

	if [ "$f1" != "TARGET_SYSTEM" -a "$f1" != "INSTALL_SYSTEM" -a "$f1" != "INSTDIR" -a "$f1" != "INSTCONF" -a "$f1" != "INFAFOLDER" ]
	then
		#
		# check if the input is part of the change list, the input ($f2) can be a folder so no match on semicolon delimiter
		#
		printTrace 0 "First: Checking if file file_to_check ${file_to_check} is in changelist ${change_list}"
		printTrace 0 'grep ^[MmAa]'".*${file_to_check}.* $change_list"

		grep "^[MmAa].*${file_to_check}.*" $change_list >/dev/null
		if [ $? -ne 0 ]
		then
			printTrace 0 "file ${file_to_check} is not in the changelist ${change_list}"
			continue
		else
			printTrace 0 "file ${file_to_check} is  in the changelist ${change_list} and will be processed"
		fi
	fi

	# process object
	if [ -d $f2 -a "$f1" != "TARGET_SYSTEM" -a "$f1" != "INSTALL_SYSTEM" -a "$f1" != "INSTDIR" -a "$f1" != "INSTCONF" -a "$f1" != "INFAFOLDER" ]
	then
		echo "Recursing to $f2"
	
		TARGET_SYSTEM="${TARGET_SYSTEM}" TARGET_LOGMECH="${TARGET_LOGMECH}" TARGET_USER="${TARGET_USER}" TARGET_PASSWORD="${TARGET_PASSWORD}" \
		INSTALL_SYSTEM="${INSTALL_SYSTEM}" INSTALL_USER="${INSTALL_USER}" INSTALL_PASSWORD="${TARGET_PASSWORD}" \
			$0 "${change_list}" "${f2}" "${version}" "${ENVNAME}" "${tmp_path}" "${run_folder}" "`expr $level + 1`" "${checkpointfile}" "${module}"
	else 
		if [ -f $f2 -a "$f1" != "TARGET_SYSTEM" -a "$f1" != "INSTALL_SYSTEM" -a "$f1" != "INSTDIR" -a "$f1" != "INSTCONF" -a "$f1" != "INFAFOLDER" ]
		then
			#
			# first check if the BUILD_TARGET_SYSTEM is set and is not the target for this file then continue with the next entry
			#
			if [ "${BUILD_OS}" = "AIX" ]
			then
				eval "T_S=\"\${$TARGET_SYSTEM}\""
			else
				T_S="${TARGET_SYSTEM}"
			fi

			if [ "${T_S}" != "${BUILD_TARGET_SYSTEM:-${T_S}}" ]
			then
				printTrace 0 "TARGET_SYSTEM ${T_S} is not the same as BUILD_TARGET_SYSTEM ${BUILD_TARGET_SYSTEM}, skipping this file"
				continue
			fi

			#
			# this entry is supposed to get built
			#
			echo | awk "{while (i--) { printf(\" \"); } }" i="$(($level + 1))"
			echo "Building: $f2"
			object_file=${tmp_dir}/$$.lst
			rm -f $object_file

			#
			# check if the $f2 "path" exist in the change-list, else no need to recurse to $f2
			# full path match with semicolon delimiter
			#
			printTrace 0 "Second: Checking if file file_to_check ${file_to_check} is in changelist ${change_list} adding result into $object_file"
			printTrace 0 'grep ^[MmAa]'".*${file_to_check}.* $change_list >$object_file"
 			grep "^[MmAa].*${file_to_check};.*" $change_list >$object_file
			if [ $? -ne 0 ]
			then
				printTrace 0 "file ${file_to_check} not found in changelist ${change_list}"
				continue
			fi
			test "$f2" = "" && continue

			printTrace 0 "file ${file_to_check} found in changelist ${change_list}"

			#
			# if we restart from checkpoint check if the entry is in the checkpoint file and skip it if so
			#
			if [ "$restart_from_checkpoint" = "Y" ]
			then
				grep "${runlistfile} ${lineNo} $f1 $f2$" "$checkpointfile" >/dev/null
				if [ $? -eq 0 ]
				then
					echo "Restarting from checkpoint, skipping $f2"
					continue
				fi
			fi
			
			# set SCM variables for this object
			ifs_sav="$IFS"
			IFS=';'
			object_variables="`while read op file_path SCM_HEADURL SCM_DATE SCM_USERID SCM_AUTHOR SCM_COMPONENT rest
			do
					echo export op="'$op'"
					echo export file_path="'$file_path'"
					echo export SCM_HEADURL="'$SCM_HEADURL'"
					echo export SCM_DATE="'$SCM_DATE'"
					echo export SCM_USERID="'$SCM_USERID'"
					echo export SCM_AUTHOR="'$SCM_AUTHOR'"
					echo export SCM_COMPONENT="'$SCM_COMPONENT'"
			done <$object_file`"
			IFS="$ifs_sav"
			eval $object_variables
			SCM_AUTHOR="${SCM_USERID} ${SCM_AUTHOR}"
			SCM_HEADURL="${SCM_HEADURL}:${file_path}"
			SCM_REVISION="${version}"
			SCM_ID="`echo "${version}" | cut -d- -f2-3`"
			SCM_REV="${SCM_ID}"

			#
			# format SCM_DT to numbers only from SCM_DATE
			#
			if [ "${BUILD_OS}" = "AIX" ]
			then
            			SCM_DT="$(echo "${SCM_DATE}" | while read d t rest; do echo ${d:0:4}${d:5:2}${d:8:2}${t:0:2}${t:3:2}${t:6:2}; done)"
			else
				SCM_DT="`date '+%Y%m%d%H%M%S' --date "${SCM_DATE}"`"
			fi

			#
			# Create temporary file for the target system
			#
			if [ "${BUILD_OS}" = "AIX" ]
			then
				eval "T_S=\"\${$TARGET_SYSTEM}\""
			else
				T_S="${TARGET_SYSTEM}"
			fi
			base_filename="`basename $f2`"
			tmp_file="`mktemp -p $tmp_path $base_filename.${version}.${T_S}.XXXXX`"

			#
			# remove temporary file created by mktemp, need original file to retain file permissions using cp command
			#
			rm -f "$tmp_file"
			cp "$f2" "$tmp_file"
						
			compile_file=$tmp_file.compile.btq

			#
			# check if we need to change the session mode
			#
			curr_sessmode="`cat $SM_FILE`"
			if echo "$f1" | grep '_ANSI' >/dev/null
			then
					new_sessmode="ANSI"
			else
					new_sessmode="BTET"
			fi
			if [ "$curr_sessmode" != "$new_sessmode" ]
			then
				#
				# yes, we need to logoff, set session mode, logon again and save the new mode
				#
				echo ".logoff" >>$run_file_btq
				echo "SET SESSION TRANSACTION $new_sessmode"
				echo ".SET SESSION TRANSACTION $new_sessmode" >>$run_file_btq
				#
				# generate logon command
				#
				if [ "${BUILD_OS}" = "AIX" ]
				then
					eval "T_S=\"\${$TARGET_SYSTEM}\""
					eval "T_L=\"\${$TARGET_LOGMECH}\""
					eval "T_U=\"\${$TARGET_USER}\""
					eval "T_P=\"\${$TARGET_PASSWORD}\""
				else
					T_S="${TARGET_SYSTEM}"
					T_L="${TARGET_LOGMECH}"
					T_U="${TARGET_USER}"
					T_P="${TARGET_PASSWORD}"
				fi
				echo ".logmech ${T_L}" >>$run_file_btq
				echo ".logon ${T_S}/${T_U},${T_P}" >>$run_file_btq

				echo "$new_sessmode" >$SM_FILE
				curr_sessmode="$new_sessmode"
			fi
	


			if [ "${replace_variables}" = "Y" ]
			then
				#
				# do variable substitution and create temporary file to execute
				#
				# get list of all environment variable names and generate sed substitute commands to
				# replace $#envname# to the value of $envname, e.g. ADS_BP is defined as ADSBP the $#ADS_BP# -> ADSBP
				# also replace all remaining references to undefined variables with the first part as database name: e.g. $#ADS_DB# -> ADS
				#
				# remove any leading BOM characters used for UTF-8 files (377,376) and UTF-8 with BOM (357,273,277)
				#
				# add specific substitution for variable mappings for the rest not using the names from the environment
				#	
				sedScript="$(mktemp ${tmp_dir}/XXXXX.sed)"
				if [ "${BUILD_OS}" = "AIX" ]
				then
					envNames="`perl -e 'use Env; Env::import(); foreach $key (keys(%ENV)) { printf("%s\n", $key); }'`"
				else
					envNames="`env -0 | sed -z -e 's/\([^=][^=]*\)=.*/\1/' | tr '\0' '\n'`"
				fi

				for name in $envNames
				do
					test "${name}" = "_" && continue
					nameValue="`eval echo \\$$name`"
					if [ "${BUILD_OS}" = "AIX" ]
					then
						perl -e "printf( \"s\x0B\\$\#${name}#\x0B${nameValue}\x0Bg\n\" )"
					else
						echo -e 's\v$#'"${name}#\v${nameValue}\vg"
					fi
				done | tr -d '\0' >>"${sedScript}"
				cat "$f2" | \
				sed \
					-f "${sedScript}" \
					-e "s/^$(echo |awk '{printf("\357\273\277")}')//" \
					-e "s/^$(echo |awk '{printf("\377\376")}')//" \
					-e "s,\$#TMP#,${tmp_path},g" \
					-e "s,\$#DBADM_ENV#,${TARGET_USER},g" \
					-e "s,\$#DB_ENV#,${ENVNAME},g" \
					-e "s,\${DB_ENV},${ENVNAME},g" \
					-e "s,\$Id:[ ][ ]*\\$,\$Id:\ ${SCM_ID}\$,g" \
					-e "s,\$Author:[ ][ ]*\\$,\$Author:\ ${SCM_AUTHOR}\$,g" \
					-e "s,\$Date:[ ][ ]*\\$,\$Date:\ ${SCM_DATE}\$,g" \
					-e "s,\$Dt:[ ][ ]*\\$,\$Dt:\ ${SCM_DT}\$,g" \
					-e "s,\$Revision:[ ][ ]*\\$,\$Revision:\ ${SCM_REVISION}\$,g" \
					-e "s,\$Rev:[ ][ ]*\\$,\$Rev:\ ${SCM_REV}\$,g" \
					-e "s,\$HeadURL:[ ][ ]*\\$,\$HeadURL:\ ${SCM_HEADURL}\$,g" | \
					sed -e 's/\$#\([^#][^#]*\)_DB#/\1/g' \
					> $tmp_file || exit 1
			else
				cat "$f2" > $tmp_file || exit 1
			fi
			
			printTrace 0 "Processing command: '$f1'"
			case "$f1" in
			"INSTALL")
				if [ "$destination_folder" = "" ]
				then
					#
					# first INSTALL in this _runorder.txt file, need to set default destination_folder
					#
					destination_folder="${INFA_SHARED}/${module}/`dirname $f2`"
					printTrace 0 "Setting destination_folder to: $destination_folder"
				fi
				
				#
				# get tmp_file and destination name and build file from original with env-variable substituted, add statement to runfile
				#
				destination="${destination_folder}/`basename $f2`"

				#
				# do variable substitution in the file name and assign new destination name
				# Note that instead of a $# we use !# to identify the variable since $# is expanded to number by shell
				#
				new_destination="`echo "${destination}" | sed -e "s/!#FILEENV#/$FILEENV/g"`"

				printTrace 0 "Destination file before expanding: '${destination}'"
				printTrace 0 "Destination file after expanding: '${new_destination}'"

				destination="${new_destination}"
				
				dest_dir="`dirname $destination`"
				dir_chmod_params="$f3"
				if [ "$dir_chmod_params" = "" ]
				then
					dir_chmod_params="755"
				fi
			
				file_chmod_params="$f4"
				if [ "$file_chmod_params" = "" ]
				then
					file_chmod_params="644"
				fi

				if [ "${build_remote}" = 'N' ]
				then
					echo "Copy $tmp_file to $destination"
					echo ".OS test ! -d \"$dest_dir\" && { mkdir -p \"$dest_dir\"; test -n \"$dir_chmod_params\" && chmod \"$dir_chmod_params\" \"$dest_dir\"; } || :" >> $run_file_btq
					echo ".OS test -n \"$file_chmod_params\" && chmod \"$file_chmod_params\" \"$tmp_file\" || :" >> $run_file_btq
					echo ".OS cp --backup=numbered -p \"$tmp_file\" \"$destination\" || echo 1 >${execution_errorcode}" >> $run_file_btq
				else
					eval "I_S=\"\${$INSTALL_SYSTEM}\""
					eval "I_U=\"\${$INSTALL_USER}\""
					eval "I_P=\"\${$INSTALL_PASSWORD}\""
					echo "Copy $tmp_file to ${I_S}:$destination"
					echo ".OS ssh -n -i \"${PrivateKeyFile}\" -l \"${I_U}\" $I_S \"test ! -d $dest_dir && { mkdir -p $dest_dir; test -n $dir_chmod_params && chmod $dir_chmod_params $dest_dir; }\" || :" >> $run_file_btq
					echo ".OS test -n \"$file_chmod_params\" && chmod \"$file_chmod_params\" \"$tmp_file\" || :" >> $run_file_btq
					echo ".OS scp -p -i \"${PrivateKeyFile}\" \"$tmp_file\" \"${I_U}\"@\"${I_S}\":\"$destination\" || echo 1 >${execution_errorcode}" >> $run_file_btq
				fi
				echo ".OS echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest} >>${checkpointfile}" >> $run_file_btq
				;;
			"EXECUTE")
				file_to_execute="${destination}"
				echo "Executing $file_to_execute ${f3} ${f4} ${f5} $rest"
				echo ".OS $file_to_execute ${f3} ${f4} ${f5} ${rest} || echo 1 >${execution_errorcode}" >>$run_file_btq
				echo ".OS echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest} >>${checkpointfile}" >> $run_file_btq
				;;
			"BTQRUN" | "BTQRUN_ANSI")
				echo ".RUN FILE $tmp_file" >> $run_file_btq
				test "$curr_sessmode" = "ANSI" && echo "COMMIT WORK;" >> $run_file_btq
				echo ".OS echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest} >>${checkpointfile}" >> $run_file_btq
				;;
			"BTQCOMPILE" | "BTQCOMPILE_ANSI")
				echo ".COMPILE FILE $tmp_file" > $compile_file

				#
				# generate call to run_ddl.sh
				#
				if [ "${BUILD_OS}" = "AIX" ]
				then
					eval "T_S=\"\${$TARGET_SYSTEM}\""
					eval "T_L=\"\${$TARGET_LOGMECH}\""
					eval "T_U=\"\${$TARGET_USER}\""
					eval "T_P=\"\${$TARGET_PASSWORD}\""
				else
					T_S="${TARGET_SYSTEM}"
					T_L="${TARGET_LOGMECH}"
					T_U="${TARGET_USER}"
					T_P="${TARGET_PASSWORD}"
				fi
				echo ".OS TD_SYSTEM='${T_S}' TD_LOGMECH='${T_L}' TD_USER='${T_U}' TD_PASSWORD='${T_P}' run_ddl.sh "${compile_file}" "${run_folder}" "${tmp_path}" "${curr_sessmode}" || echo 1 >${procedure_errorcode}" >> $run_file_btq
				#
				# generate command to insert into checkpoint file
				#
				echo ".OS echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest} >>${checkpointfile}" >> $run_file_btq
				;;
			"BTQLOAD"*)
				#
				# Create temporary file for the datafile to load for the target system
				#
				file_to_load="$f3"
				if [ "${f1}" = "BTQLOADLOB2" ]
				then
					file_to_load2="$f4"
				fi
				
				if [ ! -e "${file_to_load}" -o '(' "${f1}" = "BTQLOADLOB2" -a ! -e "${file_to_load2}" ')' ]
				then
					echo "BTQLOAD requires a data file as parameter following the btq script:"
					echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest}
				elif [ "$f1" = "BTQLOAD" -o "$f1" = "BTQLOADLOB" -o "$f1" = "BTQLOADLOB2" ]
				then
					base_filename="`basename "${file_to_load}"`"
					if [ "${BUILD_OS}" = "AIX" ]
					then
						eval "T_S=\"\${$TARGET_SYSTEM}\""
					else
						T_S="${TARGET_SYSTEM}"
					fi
					if [ "${f1}" = "BTQLOADLOB2" ]
					then
						base_filename2="`basename "${file_to_load2}"`"
						tmp_data_file2="`mktemp -p $tmp_path $base_filename.${version}.${T_S}.XXXXX`"
						rm -f "$tmp_data_file2"
						cp "${file_to_load2}" "${tmp_data_file2}"
					fi
					
					#
					# remove temporary file created by mktemp, need original file to retain file permissions using cp command
					#
					tmp_data_file="`mktemp -p $tmp_path $base_filename.${version}.${T_S}.XXXXX`"
					rm -f "$tmp_data_file"
					
					#
					# put data to load in file file_to_load and the name of the file to load in tmp_data_file_deferred
					# use deferred import to import the LOB
					# Note, the LOB column must be first in the USING clause in the SQL script
					#
					cp "${file_to_load}" "${tmp_data_file}"
					tmp_data_file_deferred="`mktemp -p $tmp_path $base_filename.deferred.${version}.${T_S}.XXXXX`"

					if [ "${f1}" = "BTQLOADLOB" ]
					then
						echo "${tmp_data_file}" >"${tmp_data_file_deferred}"
						#
						# need to switch to ASCII charset during deferred import and then back to UTF8 as default
						#
						echo ".SET SESSION CHARSET 'ASCII'" >> $run_file_btq
						echo ".IMPORT VARTEXT DEFERCOLS=1 FILE=$tmp_data_file_deferred" >> $run_file_btq
						echo ".SET SESSION CHARSET 'UTF8'" >> $run_file_btq
					elif [ "${f1}" = "BTQLOADLOB2" ]
					then
						echo "${tmp_data_file}|${tmp_data_file2}" >"${tmp_data_file_deferred}"
						#
						# need to switch to ASCII charset during deferred import and then back to UTF8 as default
						#
						echo ".SET SESSION CHARSET 'ASCII'" >> $run_file_btq
						echo ".IMPORT VARTEXT DEFERCOLS=2 FILE=$tmp_data_file_deferred" >> $run_file_btq
						echo ".SET SESSION CHARSET 'UTF8'" >> $run_file_btq
					else
						echo ".IMPORT VARTEXT FILE=$tmp_data_file" >> $run_file_btq
					fi
					echo ".RUN FILE $tmp_file" >> $run_file_btq
					test "$curr_sessmode" = "ANSI" && echo "COMMIT WORK;" >> $run_file_btq
					echo ".OS echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest} >>${checkpointfile}" >> $run_file_btq
				else
					echo "WARNING: Unknown BTQLOAD command $f1 - ignored" >&2
				fi
				;;
#			"IMPORT")
			"IMPORT" | "INFAREP" | "INFACMD" | ".OS")
				if [ "${f1}" = "IMPORT" ]
				then
					#
					# do variable substitution and create temporary file to import
					#
					old_IS="`grep '<WORKFLOW DESCRIPTION =\"' "${tmp_file}" \
						| grep 'SERVERNAME =\"' \
						| sed -e 's/.*SERVERNAME =\"//; s/\" SERVER_DOMAINNAME.*//'`"
					old_DOMAIN="`grep '<WORKFLOW DESCRIPTION =\"' "${tmp_file}" \
						| grep 'SERVER_DOMAINNAME =\"' \
						| sed -e 's/.*SERVER_DOMAINNAME =\"//; s/\" SUSPEND_ON_ERROR.*//'`"
					if [ "$old_IS" = "" -a "`grep '<WORKFLOW DESCRIPTION =\"' "${tmp_file}"`" != "" ]
					then
						echo "WARNING: ************ No integration service specified in file ${tmp_file}" >&2
					fi
					#
					# also continue into next case part using ;&
					# - due to ;& is nonexisting in old bash version we have to use an if statement instead
					#
				fi
#				;&
#			"INFAREP" | "INFACMD" | ".OS")
				#
				# generate command to run Informatica script using pmrep or pmcmd
				#
				if [ "${BUILD_OS}" = "AIX" ]
				then
					eval "T_S=\"\${$TARGET_SYSTEM}\""
				else
					T_S="${TARGET_SYSTEM}"
				fi
				#
				# generate filename for object to import, the second dot separated part of basename is the informatica folder
				#   the third part of basename is the Informatica shared foldername prefix unless INFA_FOLDER = DEFAULT
				# INFA_FOLDER = DEFAULT then use the objects folder name else use the content of INFA_FOLDER which can be set in _runorder.txt using INFAFOLDER directive
				#
				if [ "${INFA_FOLDER}" = "DEFAULT" ]
				then
					folder="$(basename $(dirname $f2))"
					shared_folder_prefix=""
				else
					folder="${INFA_FOLDER}"
					shared_folder_prefix="${INFA_SHAREDFOLDER_PREFIX}"
				fi
				tmp_file_infa="`mktemp -p $tmp_path ${version}.${folder}.${shared_folder_prefix}.$base_filename.${T_S}.infa.XXXXX`"
				cat "${tmp_file}" | \
				sed \
					-e "/<WORKFLOW DESCRIPTION =\"/s/${old_IS}/${INFA_IS}/g" \
					-e "/<WORKFLOW DESCRIPTION =\"/s/${old_DOMAIN}/${INFA_DOMAIN}/g" \
					> "${tmp_file_infa}"

				printTrace 0 "File ${tmp_file} after expanding:";
				if [ ${BUILD_TRACE} -gt 0 ]
				then
					ls -l "${tmp_file_infa}";
				fi
				printTrace 0 "Add temp file ${tmp_file_infa} into run_file ${run_file_infa}"

				cmd="${f1}"
				echo "${cmd} $tmp_file_infa" >> $run_file_infa
				echo ".OS echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest} >>${checkpointfile}" >> $run_file_infa
				;;
			"ORARUN")
				echo "@${tmp_file}" >> $run_file_ora
				echo "HOST echo ${runlistfile} ${lineNo} ${f1} ${f2} ${f3} ${f4} ${f5} ${rest} >>${checkpointfile}" >> $run_file_ora
				echo "COMMIT;" >> $run_file_ora
				;;
			*)
				echo "WARNING: Unknown command: $f1 - ignored" >&2
				;;
			esac
		else
			#
			# check if we need to change the target system
			#
			curr_system="`cat $TARGET_FILE`"
			curr_install_system="`cat $INSTALL_FILE`"
			if [ "$f1" = "TARGET_SYSTEM" ]
			then
					if [ "$f2 $f3 $f4 $f5" != "$curr_system" ]
					then
						new_system="$f2 $f3 $f4 $f5"
						echo "$new_system" >$TARGET_FILE
						echo "Change target system from $curr_system system to $new_system"
						curr_system="$new_system"
						echo ".logoff" >>$run_file_btq
						curr_sessmode="`cat $SM_FILE`"
						echo "SET SESSION TRANSACTION $curr_sessmode"
						echo ".SET SESSION TRANSACTION $curr_sessmode" >>$run_file_btq

						if [ "${BUILD_OS}" = "AIX" ]
						then
							TARGET_SYSTEM=$f2
							TARGET_USER=$f3
							TARGET_PASSWORD=$f4
							TARGET_LOGMECH=$f5
							export TARGET_SYSTEM TARGET_USER TARGET_PASSWORD TARGET_LOGMECH
							eval "T_S=\"\${$TARGET_SYSTEM}\""
							eval "T_L=\"\${$TARGET_LOGMECH}\""
							eval "T_U=\"\${$TARGET_USER}\""
							eval "T_P=\"\${$TARGET_PASSWORD}\""
							echo ".logmech ${T_L}" >>$run_file_btq
							echo ".logon ${T_S}/${T_U},${T_P}" >>$run_file_btq
						else
							typeset -n TARGET_SYSTEM=$f2
							typeset -n TARGET_USER=$f3
							typeset -n TARGET_PASSWORD=$f4
							typeset -n TARGET_LOGMECH=$f5
							export TARGET_SYSTEM TARGET_USER TARGET_PASSWORD TARGET_LOGMECH
							echo ".logmech ${TARGET_LOGMECH}" >>$run_file_btq
							echo ".logon ${TARGET_SYSTEM}/${TARGET_USER},${TARGET_PASSWORD}" >>$run_file_btq
						fi
					#else already connected to correct system so we do not have to do anything
					fi
			elif [ "$f1" = "INSTALL_SYSTEM" ]
			then
					if [ "$f2 $f3 $f4 $f5" != "$curr_install_system" ]
					then
						new_instal_system="$f2 $f3 $f4 $f5"
						echo "$new_install_system" >$INSTALL_FILE
						echo "Change install system from $curr_install_system system to $new_install_system"
						curr_install_system="$new_install_system"

						INSTALL_SYSTEM=$f2
						INSTALL_USER=$f3
						INSTALL_PASSWORD=$f4
						export INSTALL_SYSTEM INSTALL_USER INSTALL_PASSWORD
					#else already connected to correct system so we do not have to do anything
					fi
			elif [ "$f1" = "INSTDIR" ]
			then
				if [ "$f2" = "DEFAULT" ]
				then
					destination_folder="${INFA_SHARED}/${module}/`dirname $f2`"
				else
					if [ "${f2:0:1}" = "/" ]
					then
						#
						# absolute path specified, use it as destination
						#
						destination_folder="$f2"
					else
						#
						# absolute path not specified, use INFA_SHARED as parent
						#
						destination_folder="${INFA_SHARED}/$f2"
					fi
				fi
				printTrace 0 "Setting destination_folder to: $destination_folder"
			elif [ "$f1" = "INSTCONF" ]
			then
				if [ "$f2" = "NOREPLVARS" ]
				then
					replace_variables=N
				elif [ "$f2" = "REPLVARS" ]
				then
					replace_variables=Y
				elif [ "$f2" = "BUILDREMOTE" ]
				then
					build_remote=Y
				elif [ "$f2" = "BUILDLOCAL" ]
				then
					build_remote=N
				else
					echo "Unknown parameter to command $f1, ignored" >&2
				fi
				printTrace 0 "Setting replace_variables to: $replace_variables"
			elif [ "$f1" = "INFAFOLDER" ]
			then
				export INFA_FOLDER="${f2}"
				printTrace 0 "Setting INFA_FOLDER to: $f2"
				if [ "${INFA_SHAREDFOLDER_PREFIX}" = "DEFAULT" ]
				then
					export INFA_SHAREDFOLDER_PREFIX="${INFA_FOLDER}"
					printTrace 0 "Setting INFA_SHAREDFOLDER_PREFIX to: ${INFA_SHAREDFOLDER_PREFIX}"
				fi
			else
				echo "WARNING: Unknown filetype for file $f2 - ignored" >&2
			fi
		fi
	fi
done

run_ddl_status=0
impxml_status=0
sqlplus_status=0

#
# Process the Teradata database and file entries
#
if [ "$level" = "0" -a "$BUILD_DEBUG" = "0" -a -s "${run_file_btq}" ]
then
	echo "-- End Of $module" >>$run_file_btq
	echo "Execute generated Teradata sql code: ${run_file_btq} in folder: ${run_folder} logging in ${tmp_path}"
	echo run_ddl.sh "${run_file_btq}" "${run_folder}" "${tmp_path}"
	rm -f "${procedure_errorcode}" 
	run_ddl.sh "${run_file_btq}" "${run_folder}" "${tmp_path}" ""
	run_ddl_status=$?
	echo "run_ddl.sh returned $run_ddl_status as status"
	rm -f $SM_FILE
	test -f "${procedure_errorcode}" && exit `cat "${procedure_errorcode}"`
	test -f "${execution_errorcode}" && exit `cat "${execution_errorcode}"`
fi

#
# Process the Informatica repository entries
#
if [ "$level" = "0" -a "$BUILD_DEBUG" = "0" -a -s "${run_file_infa}" ]
then
	echo "Import generated Informatica XML code: ${run_file_infa} in folder: ${run_folder} logging in ${tmp_path}"
	echo impxml.sh "${run_file_infa}" "${run_folder}" "${tmp_path}" "${version}"
	impxml.sh "${run_file_infa}" "${run_folder}" "${tmp_path}" "${version}"
	impxml_status=$?
	echo "impxml returned $impxml_status as status"
fi

#
# Process the Oracle database entries
#
if [ "$level" = "0" -a "$BUILD_DEBUG" = "0" -a -s "${run_file_ora}" ]
then
	echo "Execute generated Oracle sql code: ${run_file_ora} in folder: ${run_folder} logging in ${tmp_path}"
	echo sqlplus "${run_file_ora}" "${run_folder}" "${tmp_path}" "${version}"
	SCRIPT_START_DATE="`date +%Y%m%d%H%M%S`"
	sqlplus_log="${tmp_path}/`basename ${run_file_ora}`.${SCRIPT_START_DATE}.log"

	(
	cd "${run_folder}" || exit 1
	sqlplus -s "${ORA_USER}/${ORA_PASSWORD}@${ORA_TNSNAME}" <<-EOF >"${sqlplus_log}" 2>&1
		set head off;
		@$run_file_ora
		EXIT;
	EOF
	)
	sqlplus_status=$?
	echo "Output from processing the file: ${run_file_ora}"
	cat "${sqlplus_log}"
	echo "sqlplus returned $sqlplus_status as status"
fi

if [ "${generatedModules}" != "" ]
then
	rm -f "${run_list}"
fi

exit $((${run_ddl_status} + ${impxml_status} + ${sqlplus_status}))
