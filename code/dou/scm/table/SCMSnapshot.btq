/*
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SCMSnapshot.btq 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/table/SCMSnapshot.btq $
# --------------------------------------------------------------------------
# SCM Info END
*/
.SET MAXERROR 0;

DATABASE $#DB_ENV#$#DBADMIN_DB#;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL $#DB_ENV#$#DBADMIN_DB#.DBA_NewTabDef('$#DB_ENV#$#DBADMIN_DB#','SCMSnapshot','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot
(
	SnapshotID VARCHAR(255) NOT NULL,
	SnapshotDttm TIMESTAMP(6) NOT NULL,
	SnapshotPath VARCHAR(4096) NOT NULL,
	SnapshotComponent VARCHAR(1000) NOT NULL,
	SnapshotRow VARCHAR(10000),
	InsertDttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	CONSTRAINT PKSCMSnapshot PRIMARY KEY (SnapshotID, SnapshotPath, SnapshotComponent)
)
PRIMARY INDEX PISCMSnapshot(
	SnapshotID
)
PARTITION BY RANGE_N(SnapshotDttm BETWEEN CURRENT_TIMESTAMP(6) - INTERVAL '1' YEAR AND CURRENT_TIMESTAMP(6) + INTERVAL '30' DAY EACH INTERVAL '1' DAY)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL $#DB_ENV#$#DBADMIN_DB#.DBA_NewTabDef('$#DB_ENV#$#DBADMIN_DB#','SCMSnapshot','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON TABLE $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot IS '$Rev: 32502 $$Dt: $Snapshot for build from SCM. One line per snapshot built so far'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON COLUMN $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot.SnapshotID IS 'Identity for the Snapshot, usually the filename in the build system'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON COLUMN $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot.SnapshotDttm IS 'Timestamp for the snapshot, usually the time when the file was created'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON COLUMN $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot.SnapshotPath IS 'The path for the Snapshot, usually the folder path for the filename in the build system'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON COLUMN $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot.SnapshotComponent IS 'The name of the component for the Snapshot row'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON COLUMN $#DB_ENV#$#DBADMIN_DB#.SCMSnapshot.SnapshotRow IS 'The Snapshot row information. Usually commit information from the SCM system'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
