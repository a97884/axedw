A start configuration for a jenkins job can be found in the file util/config/jenkins/SampleProject.xml

To run commands using jenkins cli download the jar file from the local jenkins server e.g.: wget http://user:password@localgit/jnlpJars/jenkins-cli.jar

To list all job definitions: java -jar jenkins-cli.jar -s http://user:password@localgit:8080/ list-jobs
To copy a job definition: java -jar jenkins-cli.jar -s http://user:password@localgit:8080/ copy-job SampleProject CopyOfSampleProject
To export a job definition: java -jar jenkins-cli.jar -s http://user:password@localgit:8080/ get-job SampleProject >SampleProject.xml
To import a job definition: java -jar jenkins-cli.jar -s http://user:password@localgit:8080/ create-job NewSampleProject <SampleProject.xml
To delete a job definition: java -jar jenkins-cli.jar -s http://user:password@localgit:8080/ delete-job NewSampleProject
Similar for node and view.

To run a job definition: java -jar jenkins-cli.jar -s http://user:password@localgit:8080/ build NewSampleProject -s

