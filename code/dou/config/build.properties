#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: build.properties 32572 2020-06-17 08:57:49Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-17 10:57:49 +0200 (ons, 17 jun 2020) $
# Last Revision    : $Revision: 32572 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/config/build.properties $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
#
# build.properties - properties for the build process. dot executed by the build script buildFrom.sh which is started by the build engine
#
# properties set by the build engine (like jenkins)
# Change the properties to set default values
# The value assigned, if the property has not been set, is between the dash - and the ending bracket
#
env="${env:-dev1}"
	# the environment prefix to use
	# default: ""BUILD_ALL_OBJECTS="${BUILD_ALL_OBJECTS:-N}"
	# Y - build all objects that are listed in _runorder.txt files
	# N - build only changed objects
	# default: N
	# the optional system name can be supplied after the Y/N flag to specify which system to build (Y local1510 - build all objects on system local1510)
WORKSPACE="${WORKSPACE:-$HOME/BuildWorkspace}"
	# where the build workspace is located
	# default: ""
ROOT="${ROOT:-${WORKSPACE}}"
	# where the root of the build is
	# default: "${WORKSPACE}"
BUILD_BLACKLIST="${BUILD_BLACKLIST:-}"
	# a semicolon separated list of paths to be excluded from the build. Note! do NOT end the list with a semicolon or all paths will be excluded
	# default: ""
BUILD_CHECKPOINT="${BUILD_CHECKPOINT:-}"
	# build using the checkpoint specified, the checkpoint is the revision from the SCM.
	# default: the last checkpoint saved in build workspace
	# If one or more space separated values with a slash character / is entered then the values are interpreted as file paths to specific objects to build.
	# The format for the path entered is the same as in _runorder.txt file with addition of a semicolon at end followed by the name of the component.
	# E.g.: code/infa_xml/global_shared/source/TDC_TgtT.ARTICLE_RETAIL_PRICE.xml;code/infa_xml
BUILD_COMPONENTS="${BUILD_COMPONENTS:-}"
	# a list of space separated components to build (aka modules or submodules or subfolders)
	# default: ""
BUILD_CREATE_TAG="${BUILD_CREATE_TAG:-N}"
	# should we build a tag after a successful build, Y or N
	# default: N
BUILD_DEBUG="${BUILD_DEBUG:-0}"
	# set debug level
	# 0 - no debug
	# 1 - print information and do not execute database sessions
	# 2 - also enable shell trace
	# default: 0
BUILD_GENERATE_MISSING_RUNORDER="${BUILD_GENERATE_MISSING_RUNORDER:-N}"
	# should we generate missing _runorder.txt files, Y or N
	# default: N
BUILD_OBJECT_LIST="${BUILD_OBJECT_LIST:-}"
	# path to a file containing list of paths to build
	# default: "SCM" - get the list of changed files from the SCM system
BUILD_OS="${BUILD_OS:-AIX}"
	# the name of the type of OS the build process is running on (Linux or AIX)
	# default: "AIX"
BUILD_PROGRAM="${BUILD_PROGRAM:-apply_chg.sh}"
	# name of the program to use for the build
	# default: apply_<module>_chg.sh - where <module> is the module, submodule, folder to build
	# for a generic database and file build program use apply_chg.sh
BUILD_PROPERTIES="${BUILD_PROPERTIES:-${WORKSPACE}/util/config/build.properties}"
	# name of the properties file to use for the build
	# default: ${WORKSPACE}/util/config/build.properties}
BUILD_READ_DATABASE="${BUILD_READ_DATABASE:-N}"
	# should we read the database to get build info such as snapshot and checkpoint, Y or N
	# default: N
BUILD_RESTART_FROM_POINT_FAILURE="${BUILD_RESTART_FROM_POINT_FAILURE:-}"
	# restart the build from the point of failure in the last build
	# Y - restart
	# N - do build from start
	# default: N
BUILD_SCM="${BUILD_SCM:-SVN}"
	# the Source Code Management system to use
	# GIT - Git
	# RTC - RTC
	# SVN - Subversion
	# NIL - Plain file system
	# default: SVN
BUILD_TAG="${BUILD_TAG:-}"
	# the tag to build
	# default: default - currently not used
BUILD_TARGET_SYSTEM="${BUILD_TARGET_SYSTEM:-}"
	# the name of the target system to build
	# default: set from _runorder.txt file using the TARGET_SYSTEM directive
BUILD_TRACE="${BUILD_TRACE:-0}"
	# set trace level
	# 0 - no trace
	# 1 - print additional information during build
	# default: 0
BUILD_UPDATE_DATABASE="${BUILD_UPDATE_DATABASE:-N}"
	# should we update the database with build info such as snapshot and checkpoint, Y or N
	# default: N
BUILD_USER="${BUILD_USER:-}"
	# the name of the user to use when running the build, also the owner of all installed files
	# default: no default, can sudo to this user in jenkins before calling the build script buildFrom.sh

#
# properties of this configuration
#
# change as needed, override by setting the property in the calling process
# The value assigned, if the property has not been set, is between the dash - and the ending bracket }
#

#
# Default to default language
#
LC_ALL="C"

#
# Teradata configuration, 2 configurations enables build of two systems in the same build
# The system to build is specified in the _runorder.txt by specifying one of the following:
# TARGET_SYSTEM TD_SYSTEM TD_USER TD_PASSWORD
# TARGET_SYSTEM TD2_SYSTEM TD2_USER TD2_PASSWORD
#
#
DBADMIN_DB="${DBADMIN_DB:-dbadmin}"
TD_ENVNAME="${TD_ENVNAME:-${env}}"
TD_SYSTEM="${TD_SYSTEM:-dwtscop1.axfood.se}"
TD_LOGMECH="${TD_LOGMECH:-}"
TD_USER="${TD_USER:-${TD_ENVNAME}${DBADMIN_DB}}"
TD_PASSWORD="${TD_PASSWORD:-\$tdwallet(${TD_USER})}"

TD2_ENVNAME="${TD2_ENVNAME:-${env}}"
TD2_SYSTEM="${TD2_SYSTEM:-dwtscop1.axfood.se}"
TD2_LOGMECH="${TD2_LOGMECH:-}"
TD2_USER="${TD2_USER:-${TD2_ENVNAME}${DBADMIN_DB}}"
TD2_PASSWORD="${TD2_PASSWORD:-\$tdwallet(${TD2_USER})}"

#
# Informatica configuration
#
INFA_DOMAIN="${INFA_DOMAIN:-${env}domain}"
INFA_IS="${INFA_IS:-${env}IS}"
INFA_USER="${INFA_USER:-${env}admin}"
INFA_PASSWORD="${INFA_PASSWORD:-${env}password}"
INFA_PASSWORD_VARIABLE="${INFA_PASSWORD_VARIABLE:-INFA_PASSWORD}"
INFA_REPOSITORY="${INFA_REPOSITORY:-${env}repository}"
INFA_SECDOMAIN="${INFA_SECDOMAIN:-${env}secdomain}"
INFA_SHARED="${INFA_SHARED:-/${env}/files}"
INFA_SHARED_NODE="${INFA_SHARED_NODE:-dwutet04.axfood.se}"
PMREP_REMOTE_NODE="${PMREP_REMOTE_NODE:-${INFA_SHARED_NODE}}"
PMREP_REMOTE_USER="${PMREP_REMOTE_USER:-${env}}"
INFA_TRUSTSTORE="${INFA_TRUSTSTORE:-${HOME}/jenkins}"
INFA_HOME="${INFA_HOME:-/opt/etl/uv/informatica/10.1.1}"
DOU_ROOT="${DOU_ROOT:-${INFA_SHARED}/dou}"
INFA_IMPORT_CONFIG="${INFA_IMPORT_CONFIG:-${DOU_ROOT}/config/import_controlfile_template.xml}"
INFA_FOLDER="${INFA_FOLDER:-DEFAULT}"
INFA_SHAREDFOLDER_PREFIX="${INFA_SHAREDFOLDER_PREFIX:-${INFA_FOLDER}}"

#
# Oracle configuration
#
ORA_ENVNAME="${ORA_ENVNAME:-${env}}"
ORA_SCHEMA="${ORA_SCHEMA:-${env}schema}"
ORA_USER="${ORA_USER:-${env}dbadmin}"
ORA_PASSWORD="${ORA_PASSWORD:-${env}password}"

#
# File configuration
# use this to specify parameter in file name
# the !#FILEENV# is used in file names of files to INSTALL
#
FILEENV="${FILEENV:-${env}}"

#
# Database configuration, all databases defaults to the TD_USER
# Not necessary to define all database to use, default is to expand variable Database_DB to Database
#
ADS_DB="${ADS_DB:-${TD_USER}}"
CTRL_DB="${CTRL_DB:-${TD_USER}}"
METADATA_DB="${METADATA_DB:-${TD_USER}}"
SEMCMN_DB="${SEMCMN_DB:-${TD_USER}}"
SEMEXP_DB="${SEMEXP_DB:-${TD_USER}}"
STAGE_DB="${STAGE_DB:-${TD_USER}}"
TARGET_DB="${TARGET_DB:-${TD_USER}}"
UTIL_DB="${UTIL_DB:-${TD_USER}}"
STGAXBO_DB="${STGAXBO_DB:-${TD_USER}}"

#
# PATH where to look for programs
#
PATH="${PATH:-/opt/devutil:${DOU_ROOT}/scm/bin:$WORKSPACE/util/scm/bin:$PATH}"

#
# svn command to use
#
SVN="${SVN:-svn --config-dir $HOME/jenkins/.subversion}"
