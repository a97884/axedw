<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type='text/xsl' href='d:/axbobackup/bin/TransactionArchiveFile.xslt'?><BOArchiveMsg xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="BoArchiveData.xsd">
 <Transaction CancelFlag="false" TrainingModeFlag="false">
  <TransactionID>8a81368228fbdffd01299caeac71688f</TransactionID>
  <RetailStoreID>587832</RetailStoreID>
  <DepartmentID>__System__</DepartmentID>
  <WorkstationID>2</WorkstationID>
  <OperatorID>16</OperatorID>
  <SequenceNumber>129059</SequenceNumber>
  <BeginDateTime>2010-07-04T10:58:36+02:00</BeginDateTime>
  <EndDateTime>2010-07-04T10:59:30+02:00</EndDateTime>
  <ReceiptSequenceNumber>6</ReceiptSequenceNumber>
  <CreateDateTimeStamp>2010-07-04T10:59:39+02:00</CreateDateTimeStamp>
  <ExportStatus>EC</ExportStatus>
  <Retail SuspendedFlag="false" SuspendedStatusCode="NO" KeyedOfflineFlag="false" VoidFlag="false" ReceiptType="0">
   <TenderElapsedTime>0</TenderElapsedTime>
   <RingElapsedTime>54</RingElapsedTime>
   <LineItem EntryMethod="TA" VoidFlag="false" VoidTypeCode="TA">
    <SequenceNumber>18</SequenceNumber>
    <ReceiptLineItemSequenceNumber>0</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:13+02:00</EndDateTimestamp>
    <Sale ItemType="TA" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>98</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>98</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>Justerings artikel</Description>
     <ActualUnitPrice>0.01</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>0.00</CostUnitPrice>
     <Quantity>0.00</Quantity>
     <Units>0.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>900098</ItemID>
     <POSIdentityID>900098</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>0.01</TaxableAmount>
      <TaxAmount>-0.01</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="KE" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>17</SequenceNumber>
    <ReceiptLineItemSequenceNumber>9</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:36+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>730</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>20</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>3</TaxGroupID>
     <Description>BÄRKASSE PLAST</Description>
     <ActualUnitPrice>1.20</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>0.40</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>5999</ItemID>
     <POSIdentityID>5999</POSIdentityID>
     <Tax>
      <TaxGroupID>3</TaxGroupID>
      <TaxableAmount>1.20</TaxableAmount>
      <TaxAmount>0.30</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="NO" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>16</SequenceNumber>
    <ReceiptLineItemSequenceNumber>10</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:36+02:00</EndDateTimestamp>
    <Tax>
     <TaxGroupID>3</TaxGroupID>
     <TaxableAmount>9.16</TaxableAmount>
     <TaxAmount>2.29</TaxAmount>
    </Tax>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>5</SequenceNumber>
    <ReceiptLineItemSequenceNumber>11</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:37+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>446</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>10</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>STRÖSOCKER 2KG</Description>
     <ActualUnitPrice>15.09</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>14.18</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7311041040973</ItemID>
     <POSIdentityID>7311041040973</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>15.09</TaxableAmount>
      <TaxAmount>1.81</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="NO" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>6</SequenceNumber>
    <ReceiptLineItemSequenceNumber>12</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:37+02:00</EndDateTimestamp>
    <Tax>
     <TaxGroupID>2</TaxGroupID>
     <TaxableAmount>162.20</TaxableAmount>
     <TaxAmount>19.46</TaxAmount>
    </Tax>
   </LineItem>
   <LineItem EntryMethod="KE" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>13</SequenceNumber>
    <ReceiptLineItemSequenceNumber>13</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:41+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>445</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>10</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>VETEMJÖL 5KG</Description>
     <ActualUnitPrice>17.63</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>16.86</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7311041009093</ItemID>
     <POSIdentityID>5109</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>17.63</TaxableAmount>
      <TaxAmount>2.12</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>1</SequenceNumber>
    <ReceiptLineItemSequenceNumber>14</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:43+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>712</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>19</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>3</TaxGroupID>
     <Description>BAKPLÅTSPAPPER</Description>
     <ActualUnitPrice>7.96</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>5.92</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7311041057995</ItemID>
     <POSIdentityID>7311041057995</POSIdentityID>
     <Tax>
      <TaxGroupID>3</TaxGroupID>
      <TaxableAmount>7.96</TaxableAmount>
      <TaxAmount>1.99</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>3</SequenceNumber>
    <ReceiptLineItemSequenceNumber>15</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:46+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>180</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>4</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>MJÖLK 3%</Description>
     <ActualUnitPrice>6.34</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>5.68</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7310865000194</ItemID>
     <POSIdentityID>7310865000194</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>6.34</TaxableAmount>
      <TaxAmount>0.76</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="KE" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>8</SequenceNumber>
    <ReceiptLineItemSequenceNumber>16</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:58:56+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>187</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>4</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>ÄGG 15P M INBUR</Description>
     <ActualUnitPrice>22.23</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>17.03</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7391730008934</ItemID>
     <POSIdentityID>7391730008934</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>22.23</TaxableAmount>
      <TaxAmount>2.67</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>7</SequenceNumber>
    <ReceiptLineItemSequenceNumber>17</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:00+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>510</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>12</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>HALLON/BLÅBÄR 300G</Description>
     <ActualUnitPrice>19.46</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>11.98</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7311041050323</ItemID>
     <POSIdentityID>7311041050323</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>19.46</TaxableAmount>
      <TaxAmount>2.34</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>10</SequenceNumber>
    <ReceiptLineItemSequenceNumber>18</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:01+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>186</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>4</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>KRONJÄST 50G</Description>
     <ActualUnitPrice>6.61</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>5.60</CostUnitPrice>
     <Quantity>4.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>73500346</ItemID>
     <POSIdentityID>73500346</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>6.61</TaxableAmount>
      <TaxAmount>0.79</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>2</SequenceNumber>
    <ReceiptLineItemSequenceNumber>20</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:05+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>109</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>1</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>PISTAGE&amp;VANILJLÄNGD</Description>
     <ActualUnitPrice>19.60</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>14.20</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7314878359003</ItemID>
     <POSIdentityID>7314878359003</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>19.60</TaxableAmount>
      <TaxAmount>2.35</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="KE" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>4</SequenceNumber>
    <ReceiptLineItemSequenceNumber>21</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:09+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="true" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>161</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>3</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>NEKTARINER</Description>
     <ActualUnitPrice>9.37</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>6.32</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>0.458</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>4379</ItemID>
     <POSIdentityID>4379</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>9.37</TaxableAmount>
      <TaxAmount>1.12</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>9</SequenceNumber>
    <ReceiptLineItemSequenceNumber>22</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:11+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>488</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>11</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>GUANABANA/PINE 60CL</Description>
     <ActualUnitPrice>8.84</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>6.28</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>73100768</ItemID>
     <POSIdentityID>73100768</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>8.84</TaxableAmount>
      <TaxAmount>1.06</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>11</SequenceNumber>
    <ReceiptLineItemSequenceNumber>23</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:11+02:00</EndDateTimestamp>
    <Sale ItemType="DE" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>981</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>96</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>PANT</Description>
     <ActualUnitPrice>0.89</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>0.89</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>76</ItemID>
     <POSIdentityID>76</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>0.89</TaxableAmount>
      <TaxAmount>0.11</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="KE" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>14</SequenceNumber>
    <ReceiptLineItemSequenceNumber>24</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:13+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="true" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>160</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>3</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>BANANER KG</Description>
     <ActualUnitPrice>13.90</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>10.63</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>0.824</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>4237</ItemID>
     <POSIdentityID>4237</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>13.90</TaxableAmount>
      <TaxAmount>1.67</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="SC" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>12</SequenceNumber>
    <ReceiptLineItemSequenceNumber>25</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:14+02:00</EndDateTimestamp>
    <Sale ItemType="ST" BulkItemFlag="false" ReturnFlag="false" ReasonCode="NO">
     <MerchandiseStructure>
      <MerchandiseStructureID>165</MerchandiseStructureID>
      <SummaryMerchandiseHierarchyGroupID>3</SummaryMerchandiseHierarchyGroupID>
     </MerchandiseStructure>
     <TaxGroupID>2</TaxGroupID>
     <Description>AVOCADO GIRSACK 700</Description>
     <ActualUnitPrice>22.23</ActualUnitPrice>
     <AdditionalTaxAmount>0.00</AdditionalTaxAmount>
     <CostUnitPrice>16.50</CostUnitPrice>
     <Quantity>1.00</Quantity>
     <Units>1.000</Units>
     <POSIdentityItemCount>1.0</POSIdentityItemCount>
     <ItemID>7311041052112</ItemID>
     <POSIdentityID>7311041052112</POSIdentityID>
     <Tax>
      <TaxGroupID>2</TaxGroupID>
      <TaxableAmount>22.23</TaxableAmount>
      <TaxAmount>2.67</TaxAmount>
     </Tax>
    </Sale>
   </LineItem>
   <LineItem EntryMethod="KE" VoidFlag="false" VoidTypeCode="NO">
    <SequenceNumber>15</SequenceNumber>
    <ReceiptLineItemSequenceNumber>27</ReceiptLineItemSequenceNumber>
    <EndDateTimestamp>2010-07-04T10:59:27+02:00</EndDateTimestamp>
    <Tender ReturnFlag="false">
     <TenderID>3</TenderID>
     <TenderTypeCode>CR</TenderTypeCode>
     <Amount>193.11</Amount>
     <Data>
      <AccountNumber>4581********3553</AccountNumber>
     </Data>
    </Tender>
   </LineItem>
   <Total Type="SC">
    <Amount>11.00</Amount>
    <Rounding>
     <Amount>0.00</Amount>
    </Rounding>
   </Total>
   <Total Type="IC">
    <Amount>16.00</Amount>
    <Rounding>
     <Amount>0.00</Amount>
    </Rounding>
   </Total>
   <Total Type="SA">
    <Amount>171.36</Amount>
    <Rounding>
     <Amount>0.00</Amount>
    </Rounding>
   </Total>
   <Total Type="GA">
    <Amount>193.11</Amount>
    <Rounding>
     <Amount>0.00</Amount>
    </Rounding>
   </Total>
   <Total Type="TT">
    <Amount>171.36</Amount>
    <Rounding>
     <Amount>0.00</Amount>
    </Rounding>
   </Total>
   <Total Type="TA">
    <Amount>21.75</Amount>
    <Rounding>
     <Amount>0.00</Amount>
    </Rounding>
   </Total>
  </Retail>
  <TransactionLayout>
   <TypeCode>CU</TypeCode>
   <ReceiptLayout>                                                  
     WILLYS LYCKEBY      
                 TEL. 0455-57570                  
               ORG.NR: 556163-2232                
                    Öppettider                    
                Måndag-Fredag 7-21                
           Lördag  8-21   Söndag  9-21            
--------------------------------------------------
BÄRKASSE PLAST                                1,50
STRÖSOCKER 2KG                               16,90
VETEMJÖL 5KG                                 19,75
BAKPLÅTSPAPPER                                9,95
MJÖLK 3&#37;                                      7,10
ÄGG 15P M INBUR                              24,90
HALLON/BLÅBÄR 300G                           21,80
KRONJÄST 50G                 4st*1,85         7,40
PISTAGE&amp;VANILJLÄNGD                          21,95
NEKTARINER                                        
                   0,458kg*22,90kr/kg        10,49
GUANABANA/PINE 60CL                           9,90
  +PANT                                       1,00
BANANER KG                                        
                   0,824kg*18,90kr/kg        15,57
AVOCADO GIRSACK 700                          24,90
--------------------------------------------------
  Totalt 16 varor                                 
 Totalt        193,00 SEK
                                                  
Mottaget    Kontokort                       193,11
4581********3553                              1111
VISA                                           KÖP
Belastat bankkonto                                
                                                  
Personlig kod                                     
                                                  
Ca1 5 000 SWE 520529                              
Butik: 941096                                     
Termid: 2 / 00001758                              
Ref.nr: 007043166756                              
Datum: 2010-07-04 10:58:40                        
AID : A0000000031010                              
TVR : 0080001000                                  
TSI : F800                                        
                                                  
                                                  
 Moms&#37;          Moms            Netto       Brutto
 12,00         19,46           162,20       181,66
 25,00          2,29             9,16        11,45
                                                  
--------------------------------------------------
      SPARA KVITTOT      
                                                  
    Tack för besöket     
                  Välkommen åter                  
             Detta kvitto gäller som              
            köpbevis ska uppvisas vid             
              eventuell reklamation.              
                                                  
                Du betjänades av                  
                      Sofie                       
                                                  
Kassa: 2/6                      2010-07-04   10:59
</ReceiptLayout>
  </TransactionLayout>
 </Transaction>
</BOArchiveMsg>
