#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: archive_sap.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/archive_sap.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------


export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

DB_ENVAchv="$DB_ENV"Achv
DB_ENVMetaDataVIN="$DB_ENV"MetaDataVIN
DB_ENVStgSAPT="$DB_ENV"StgSAPT

JobRunID=$2
echo "JobRunID=$JobRunID"

CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS=$CURRENT_TS"

exec >$PMRootDir/log/`basename $0`_"Arch_"$CURRENT_TS"_script".log 2>&1 

echo "writing to log"
echo "PMRootDir = $PMRootDir"
echo "DB_ENV = $DB_ENV"
echo "CURRENT_TS = $CURRENT_TS"
echo "JobRunID = $JobRunID"

echo "DB_ENVAchv = ${DB_ENV}Achv"
echo "DB_ENVMetaDataVIN = ${DB_ENV}MetaDataVIN"
echo "DB_ENVStgSAPT = ${DB_ENV}StgSAPT"


###################################################################################################
#  Print script syntax and help
###################################################################################################

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <JobRunID>"
    echo ""
    echo "   Base directory is   : PMRootDir"
    echo "   <JobRunID>         : JobRunID to archive"
    echo ""
    }


############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


############################################################################
# Script and log file directories                                          #
############################################################################

# Log Directory
LOG_DIR=$1/log
echo "LOG_DIR = " $LOG_DIR


# Bteq Log File
LOG_FILE=$LOG_DIR/`basename $0 .sh`_$JobRunID"_"$CURRENT_TS"_bteq".log
echo "LOG_FILE = " $LOG_FILE
#chmod 777 $LOG_FILE


# Bteq Directory
SCRIPT_DIR=$1/bin
echo "SCRIPT_DIR = " $SCRIPT_DIR


# Bteq File
#BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/bin/`basename $0 .sh`_$CURRENT_TS.bteq
BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/tmp/`basename $0 .sh`_$CURRENT_TS.bteq
echo "BTEQ_ARCHIVE_SOURCE_SCRIPT = " $BTEQ_ARCHIVE_SOURCE_SCRIPT 
>$BTEQ_ARCHIVE_SOURCE_SCRIPT
#chmod 777 $BTEQ_ARCHIVE_SOURCE_SCRIPT


############################################################################
# TO GET LOGON INFORMATION FOR BTEQ                                        #
############################################################################

PWD_FILE=$SCRIPT_DIR/PWD_BteqLogon
echo "PWD_FILE = " $PWD_FILE

LOGON=`grep $DB_ENV"_"LOGON $PWD_FILE `

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	return -1 
}

#LOGON_FILE=$SCRIPT_DIR/$DB_ENV"_"$JobRunID"_LOGON"
LOGON_FILE=$1/tmp/$DB_ENV"_"$JobRunID"_LOGON"
echo "LOGON_FILE = " $LOGON_FILE
#chmod 777 $LOGON_FILE

LOGON=`echo $LOGON | cut -d "=" -f2`

#Test condition to check if the file already exists

if [ -f $LOGON_FILE ]		
then
	#echo "Logon File Exists"
	EXISTING_LOGON=`head -1 $LOGON_FILE`
	
	if [[ $LOGON = $EXISTING_LOGON ]]
	then
	echo "Same credentials reusing existing file"
	else
	echo $LOGON > $LOGON_FILE
	fi
else 
	#file does not exist, use latest credentials
	echo $LOGON > $LOGON_FILE
fi
chmod 777 $LOGON_FILE

echo "
.SET SESSION TRANSACTION ANSI;

.RUN FILE = $LOGON_FILE" > $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=CreateArchiveTables;ClientUser=$LOGNAME;' FOR SESSION;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "commit;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

############################################################################
# BTEQ                                                                                     
############################################################################

echo "
-- ---------------------------------------------------------------------------------------------------------------------
-- The output may exceed the standard 80 characters, it is expanded to 5000
-- ---------------------------------------------------------------------------------------------------------------------
.width 5000
.set format on
.foldline off" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

start_time=`date`
echo "start_time = " $start_time > $LOG_FILE


echo "
------------------------------------------------------------------------------------------
-- 1) SAP_Article Delta Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_SAP_Article
(
         SequenceId
        ,ContentType
        ,SubContentType
        ,TransactionTS
        ,ExtractionTS
        ,IEReceivedTS
        ,IEDeliveryTS
        ,SourceVersion
        ,RecordCount
        ,LoadIndicator
        ,TS
        ,XmlData
        ,ArchiveKey
        ,JobRunID
)
SELECT

         Art.SequenceId
        ,Art.ContentType
        ,Art.SubContentType
        ,Art.TransactionTS
        ,Art.ExtractionTS
        ,Art.IEReceivedTS
        ,Art.IEDeliveryTS
        ,Art.SourceVersion
        ,Art.RecordCount
        ,Art.LoadIndicator
        ,Art.TS
        ,Art.XmlData
        ,Art.ArchiveKey
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Article Art

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ArticleQ ArtQ
        ON
        Art.SequenceId      = ArtQ.SequenceId
    AND Art.TransactionTS   = ArtQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Article_Load_Cache ArtCache
        ON
        Art.SequenceId      = ArtCache.SequenceId
    AND Art.TransactionTS   = ArtCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Article_ArticleMaster ArtMas
        ON
        Art.ArchiveKey = ArtMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Art.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Article'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Art.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Article'
        AND XPV.ResolveStatus = 'E'

WHERE
        Art.LoadIndicator = 'D'         AND
        ArtQ.SequenceId    IS NULL      AND
        ArtCache.SequenceId   IS NULL   AND
		ArtMas.ArchiveKey     IS NULL   AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL

;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Article WHERE ArchiveKey IN
(
SELECT
        Art.ArchiveKey
FROM
        ${DB_ENV}StgSAPT.Stg_Article Art

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ArticleQ ArtQ
        ON
        Art.SequenceId      = ArtQ.SequenceId
    AND Art.TransactionTS   = ArtQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Article_Load_Cache ArtCache
        ON
        Art.SequenceId      = ArtCache.SequenceId
    AND Art.TransactionTS   = ArtCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Article_ArticleMaster ArtMas
        ON
        Art.ArchiveKey = ArtMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Art.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Article'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Art.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Article'
        AND XPV.ResolveStatus = 'E'

WHERE
        Art.LoadIndicator = 'D'         AND
        ArtQ.SequenceId    IS NULL      AND
        ArtCache.SequenceId   IS NULL   AND
		ArtMas.ArchiveKey     IS NULL   AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)AND LoadIndicator = 'D'
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 2) SAP_Article Full Load Archiving
------------------------------------------------------------------------------------------


INSERT INTO ${DB_ENV}Achv.Arc_SAP_Article
(
         SequenceId
        ,ContentType
        ,SubContentType
        ,TransactionTS
        ,ExtractionTS
        ,IEReceivedTS
        ,IEDeliveryTS
        ,SourceVersion
        ,RecordCount
        ,LoadIndicator
        ,TS
        ,XmlData
        ,ArchiveKey
        ,JobRunID
)
SELECT
         Art.SequenceId
        ,Art.ContentType
        ,Art.SubContentType
        ,Art.TransactionTS
        ,Art.ExtractionTS
        ,Art.IEReceivedTS
        ,Art.IEDeliveryTS
        ,Art.SourceVersion
        ,Art.RecordCount
        ,Art.LoadIndicator
        ,Art.TS
        ,Art.XmlData
        ,Art.ArchiveKey
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Article Art

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_Article_Full_Load ArtQ
        ON
        Art.SequenceId      = ArtQ.SequenceId
    AND Art.TransactionTS   = ArtQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Article_Load_Cache ArtCache
        ON
        Art.SequenceId      = ArtCache.SequenceId
    AND Art.TransactionTS   = ArtCache.ReferenceTS	
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Article_ArticleMaster ArtMas
        ON
        Art.ArchiveKey = ArtMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Art.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Article'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Art.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Article'
        AND XPV.ResolveStatus = 'E'

WHERE
        Art.LoadIndicator <> 'D'         AND
        ArtQ.SequenceId    IS NULL       AND
        ArtCache.SequenceId   IS NULL    AND
		ArtMas.ArchiveKey     IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL 

;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Article WHERE ArchiveKey IN
(
SELECT
        Art.ArchiveKey
FROM
        ${DB_ENV}StgSAPT.Stg_Article Art

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_Article_Full_Load ArtQ
        ON
        Art.SequenceId      = ArtQ.SequenceId
    AND Art.TransactionTS   = ArtQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Article_Load_Cache ArtCache
        ON
        Art.SequenceId      = ArtCache.SequenceId
    AND Art.TransactionTS   = ArtCache.ReferenceTS	
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Article_ArticleMaster ArtMas
        ON
        Art.ArchiveKey = ArtMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Art.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Article'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Art.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Article'
        AND XPV.ResolveStatus = 'E'

WHERE
        Art.LoadIndicator <> 'D'         AND
        ArtQ.SequenceId    IS NULL       AND
        ArtCache.SequenceId   IS NULL    AND
		ArtMas.ArchiveKey     IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL 
GROUP BY 1
) AND LoadIndicator <> 'D'
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 3) SAP_ArticleHierarchy Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_SAP_ArticleHierarchy 
(
         SequenceId    
        ,ContentType   
        ,SubContentType
        ,TransactionTS 
        ,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,RecordCount   
        ,TS            
        ,ArchiveKey    
        ,XmlData       
        ,JobRunId      
)
SELECT
         Art.SequenceId    
        ,Art.ContentType   
        ,Art.SubContentType
        ,Art.TransactionTS 
        ,Art.ExtractionTS  
        ,Art.IEReceivedTS  
        ,Art.IEDeliveryTS  
        ,Art.SourceVersion 
        ,Art.RecordCount   
        ,Art.TS            
        ,Art.ArchiveKey    
        ,Art.XmlData       
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_ArticleHierarchy Art

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ArticleHierarchyQ ArtQ
        ON
        Art.SequenceId      = ArtQ.SequenceId
    AND Art.ExtractionTS    = ArtQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_ArticleHie_Load_Cache ArtCache
        ON
        Art.SequenceId      = ArtCache.SequenceId
    AND Art.ExtractionTS   = ArtCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_ArticleHie_E1WAH02 ArtHie
        ON
        Art.ArchiveKey = ArtHie.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Art.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_ArticleHierarchy'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Art.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_ArticleHierarchy'
        AND XPV.ResolveStatus = 'E'

WHERE
        ArtQ.SequenceId    IS NULL      AND
        ArtCache.SequenceId   IS NULL	AND
		ArtHie.ArchiveKey IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL 

;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_ArticleHierarchy WHERE ArchiveKey IN
(
SELECT
         Art.ArchiveKey    
FROM
        ${DB_ENV}StgSAPT.Stg_ArticleHierarchy Art

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ArticleHierarchyQ ArtQ
        ON
        Art.SequenceId      = ArtQ.SequenceId
    AND Art.ExtractionTS    = ArtQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_ArticleHie_Load_Cache ArtCache
        ON
        Art.SequenceId      = ArtCache.SequenceId
    AND Art.ExtractionTS   = ArtCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_ArticleHie_E1WAH02 ArtHie
        ON
        Art.ArchiveKey = ArtHie.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Art.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_ArticleHierarchy'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Art.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_ArticleHierarchy'
        AND XPV.ResolveStatus = 'E'

WHERE
        ArtQ.SequenceId    IS NULL      AND
        ArtCache.SequenceId   IS NULL	AND
		ArtHie.ArchiveKey IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL 
GROUP BY 1
);

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 4) SAP_Customer Delta And Full Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_SAP_Customer
(
         SequenceId
        ,ContentType
        ,SubContentType
        ,TransactionTS
        ,ExtractionTS
        ,IEReceivedTS
        ,IEDeliveryTS
        ,SourceVersion
        ,RecordCount
        ,TS
        ,MessageFunction
        ,ArchiveKey
        ,XmlData
        ,JobRunID
)
SELECT

         Cust.SequenceId      
        ,Cust.ContentType     
        ,Cust.SubContentType  
        ,Cust.TransactionTS   
        ,Cust.ExtractionTS    
        ,Cust.IEReceivedTS    
        ,Cust.IEDeliveryTS    
        ,Cust.SourceVersion   
        ,Cust.RecordCount     
        ,Cust.TS              
        ,Cust.MessageFunction 
        ,Cust.ArchiveKey      
        ,Cust.XmlData         
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Customer Cust

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_CustomerQ CustQ
        ON
        Cust.SequenceId      = CustQ.SequenceId
    AND Cust.ExtractionTS    = CustQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_Customer_Full_Load CustFullLoad
        ON
        Cust.SequenceId      = CustFullLoad.SequenceId
    AND Cust.ExtractionTS    = CustFullLoad.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Customer_Load_Cache CustCache
        ON
        Cust.SequenceId      = CustCache.SequenceId
    AND Cust.ExtractionTS    = CustCache.ReferenceTS	
		
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Customer_DEBMAS07 DebMas
        ON
        Cust.ArchiveKey = DebMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Cust.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Customer'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Cust.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Customer'
        AND XPV.ResolveStatus = 'E'

WHERE
        CustQ.SequenceId    IS NULL      AND
		CustFullLoad.SequenceId    IS NULL      AND
        CustCache.SequenceId   IS NULL      AND
        DebMas.ArchiveKey   IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL

;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Customer WHERE ArchiveKey IN
(

SELECT
         Cust.ArchiveKey
FROM
        ${DB_ENV}StgSAPT.Stg_Customer Cust

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_CustomerQ CustQ
        ON
        Cust.SequenceId      = CustQ.SequenceId
    AND Cust.ExtractionTS    = CustQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_Customer_Full_Load CustFullLoad
        ON
        Cust.SequenceId      = CustFullLoad.SequenceId
    AND Cust.ExtractionTS    = CustFullLoad.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Customer_Load_Cache CustCache
        ON
        Cust.SequenceId      = CustCache.SequenceId
    AND Cust.ExtractionTS    = CustCache.ReferenceTS	
		
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Customer_DEBMAS07 DebMas
        ON
        Cust.ArchiveKey = DebMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Cust.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Customer'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Cust.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Customer'
        AND XPV.ResolveStatus = 'E'

WHERE
        CustQ.SequenceId    IS NULL      AND
		CustFullLoad.SequenceId    IS NULL      AND
        CustCache.SequenceId   IS NULL      AND
        DebMas.ArchiveKey   IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 5) SAP_Site Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_SAP_Site
(
         SequenceId      
        ,ContentType     
        ,SubContentType  
        ,TransactionTS   
        ,ExtractionTS    
        ,IEReceivedTS    
        ,IEDeliveryTS    
        ,SourceVersion   
        ,RecordCount     
        ,LoadIndicator   
        ,TS              
        ,MessageFunction 
        ,ArchiveKey      
        ,XmlData         
        ,JobRunId      
)
SELECT

         Site.SequenceId     
        ,Site.ContentType    
        ,Site.SubContentType 
        ,Site.TransactionTS  
        ,Site.ExtractionTS   
        ,Site.IEReceivedTS   
        ,Site.IEDeliveryTS   
        ,Site.SourceVersion  
        ,Site.RecordCount    
        ,Site.LoadIndicator  
        ,Site.TS             
        ,Site.MessageFunction
        ,Site.ArchiveKey     
        ,Site.XmlData        
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Site Site

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_SiteQ SiteQ
        ON
        Site.SequenceId      = SiteQ.SequenceId
    AND Site.ExtractionTS    = SiteQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Site_Load_Cache SiteCache
        ON
        Site.SequenceId      = SiteCache.SequenceId
    AND Site.ExtractionTS    = SiteCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Site_BETMAS01 BetMas
        ON
        Site.ArchiveKey = BetMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Site.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Site'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Site.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Site'
        AND XPV.ResolveStatus = 'E'

WHERE
        SiteQ.SequenceId    IS NULL      AND
        SiteCache.SequenceId   IS NULL      AND
        BetMas.ArchiveKey   IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL

;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Site WHERE ArchiveKey IN
(
SELECT
         Site.ArchiveKey     

FROM
        ${DB_ENV}StgSAPT.Stg_Site Site

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_SiteQ SiteQ
        ON
        Site.SequenceId      = SiteQ.SequenceId
    AND Site.ExtractionTS    = SiteQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Site_Load_Cache SiteCache
        ON
        Site.SequenceId      = SiteCache.SequenceId
    AND Site.ExtractionTS    = SiteCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Site_BETMAS01 BetMas
        ON
        Site.ArchiveKey = BetMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Site.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Site'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Site.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Site'
        AND XPV.ResolveStatus = 'E'

WHERE
        SiteQ.SequenceId    IS NULL      AND
        SiteCache.SequenceId   IS NULL      AND
        BetMas.ArchiveKey   IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
);

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR



------------------------------------------------------------------------------------------
-- 6) SAP_Assortment Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_SAP_Assortment
(
		SequenceId
		,ContentType
		,SubContentType
		,TransactionTS
		,ExtractionTS
		,IEReceivedTS
		,IEDeliveryTS
		,SourceVersion
		,RecordCount
		,LoadIndicator
		,TS
		,XmlData
		,ArchiveKey
		,JobRunId  
)
SELECT

         Assort.SequenceId
        ,Assort.ContentType
        ,Assort.SubContentType
        ,Assort.TransactionTS
        ,Assort.ExtractionTS
        ,Assort.IEReceivedTS
        ,Assort.IEDeliveryTS
        ,Assort.SourceVersion
        ,Assort.RecordCount
        ,Assort.LoadIndicator
        ,Assort.TS
		,Assort.XmlData
		,Assort.ArchiveKey
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Assortment Assort

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_AssortmentQ AssortQ
        ON
        Assort.SequenceId      = AssortQ.SequenceId
    AND Assort.TransactionTS   = AssortQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Assortment_Load_Cache AssortCache
        ON
        Assort.SequenceId      = AssortCache.SequenceId
    AND Assort.TransactionTS   = AssortCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_AssortmentMaster AssortMas
        ON
        Assort.ArchiveKey = AssortMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Assort.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Assortment'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Assort.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Assortment'
        AND XPV.ResolveStatus = 'E'

WHERE
        AssortQ.SequenceId    IS NULL      AND
        AssortCache.SequenceId   IS NULL   AND
		AssortMas.ArchiveKey     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Assortment WHERE ArchiveKey IN
(
SELECT
        Assort.ArchiveKey
FROM
        ${DB_ENV}StgSAPT.Stg_Assortment Assort

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_AssortmentQ AssortQ
        ON
        Assort.SequenceId      = AssortQ.SequenceId
    AND Assort.TransactionTS   = AssortQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Assortment_Load_Cache AssortCache
        ON
        Assort.SequenceId      = AssortCache.SequenceId
    AND Assort.TransactionTS   = AssortCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_AssortmentMaster AssortMas
        ON
        Assort.ArchiveKey = AssortMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Assort.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Assortment'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Assort.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Assortment'
        AND XPV.ResolveStatus = 'E'

WHERE
        AssortQ.SequenceId    IS NULL      AND
        AssortCache.SequenceId   IS NULL   AND
		AssortMas.ArchiveKey     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 8) VENDOR Load Archiving
------------------------------------------------------------------------------------------


INSERT INTO ${DB_ENV}Achv.Arc_SAP_Vendor
(
		SequenceId
		,ContentType
		,SubContentType
		,TransactionTS
		,ExtractionTS
		,IEReceivedTS
		,IEDeliveryTS
		,SourceVersion
		,RecordCount
		,LoadIndicator
		,TS
		,ArchiveKey
		,XmlData  
        ,JobRunId      
)
SELECT
		 Vendor.SequenceId
		,Vendor.ContentType
		,Vendor.SubContentType
		,Vendor.TransactionTS
		,Vendor.ExtractionTS
		,Vendor.IEReceivedTS
		,Vendor.IEDeliveryTS
		,Vendor.SourceVersion
		,Vendor.RecordCount
		,Vendor.LoadIndicator
		,Vendor.TS
		,Vendor.ArchiveKey
		,Vendor.XmlData
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Vendor Vendor

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_VendorQ VendorQ
        ON
        Vendor.SequenceID   = VendorQ.SequenceID
    AND Vendor.TransactionTS    = VendorQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Vendor_Load_Cache VendorCache
        ON
        Vendor.SequenceID   = VendorCache.SequenceID
    AND Vendor.TransactionTS    = VendorCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Vendor_VendorMaster VendorMaster1
        ON
        Vendor.ArchiveKey = VendorMaster1.ArchiveKey

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S2_SAP_Vendor_VendorMaster VendorMaster2
        ON
        VendorMaster1.ArchiveKey = VendorMaster2.ArchiveKey
       
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Vendor.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Vendor'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Vendor.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Vendor'
        AND XPV.ResolveStatus = 'E'

WHERE
        VendorQ.SequenceID       IS NULL      AND
        VendorCache.SequenceID   IS NULL      AND
        VendorMaster1.ArchiveKey  IS NULL      AND
        VendorMaster2.ArchiveKey  IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Vendor WHERE ArchiveKey IN
(
SELECT
         Vendor.ArchiveKey

FROM
        ${DB_ENV}StgSAPT.Stg_Vendor Vendor

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_VendorQ VendorQ
        ON
        Vendor.SequenceID   = VendorQ.SequenceID
    AND Vendor.TransactionTS    = VendorQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Vendor_Load_Cache VendorCache
        ON
        Vendor.SequenceID   = VendorCache.SequenceID
    AND Vendor.TransactionTS    = VendorCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Vendor_VendorMaster VendorMaster1
        ON
        Vendor.ArchiveKey = VendorMaster1.ArchiveKey

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S2_SAP_Vendor_VendorMaster VendorMaster2
        ON
        VendorMaster1.ArchiveKey = VendorMaster2.ArchiveKey
       
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Vendor.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Vendor'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Vendor.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Vendor'
        AND XPV.ResolveStatus = 'E'

WHERE
        VendorQ.SequenceID       IS NULL      AND
        VendorCache.SequenceID   IS NULL      AND
        VendorMaster1.ArchiveKey  IS NULL      AND
        VendorMaster2.ArchiveKey  IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
        GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 9) SAP_Pricing Delta Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_SAP_Pricing
(
         SequenceId
        ,ContentType
        ,SubContentType
        ,TransactionTS
        ,ExtractionTS
        ,IEReceivedTS
        ,IEDeliveryTS
        ,SourceVersion
        ,RecordCount
        ,LoadIndicator
        ,TS
        ,XmlData
        ,ArchiveKey
        ,JobRunID
)
SELECT

         Price.SequenceId
        ,Price.ContentType
        ,Price.SubContentType
        ,Price.TransactionTS
        ,Price.ExtractionTS
        ,Price.IEReceivedTS
        ,Price.IEDeliveryTS
        ,Price.SourceVersion
        ,Price.RecordCount
        ,Price.LoadIndicator
        ,Price.TS
        ,Price.XmlData
        ,Price.ArchiveKey
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Pricing Price

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_PricingQ PriceQ
        ON
        Price.SequenceId      = PriceQ.SequenceId
    AND Price.TransactionTS   = PriceQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Pricing_Load_Cache PriceCache
        ON
        Price.SequenceId      = PriceCache.SequenceId
    AND Price.TransactionTS   = PriceCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Pricing_Item PriceMas
        ON
        Price.ArchiveKey = PriceMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Price.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Pricing'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Price.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Pricing'
        AND XPV.ResolveStatus = 'E'

WHERE
        Price.LoadIndicator = 'D'         AND
        PriceQ.SequenceId    IS NULL      AND
        PriceCache.SequenceId   IS NULL   AND
		PriceMas.ArchiveKey     IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Pricing WHERE ArchiveKey IN
(
SELECT
        Price.ArchiveKey
FROM
        ${DB_ENV}StgSAPT.Stg_Pricing Price

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_PricingQ PriceQ
        ON
        Price.SequenceId      = PriceQ.SequenceId
    AND Price.TransactionTS   = PriceQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Pricing_Load_Cache PriceCache
        ON
        Price.SequenceId      = PriceCache.SequenceId
    AND Price.TransactionTS   = PriceCache.ReferenceTS	

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Pricing_Item PriceMas
        ON
        Price.ArchiveKey = PriceMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Price.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Pricing'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Price.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Pricing'
        AND XPV.ResolveStatus = 'E'

WHERE
        Price.LoadIndicator = 'D'         AND
        PriceQ.SequenceId    IS NULL      AND
        PriceCache.SequenceId   IS NULL   AND
		PriceMas.ArchiveKey     IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)AND LoadIndicator = 'D'
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 10) SAP_Pricing Full Load Archiving
------------------------------------------------------------------------------------------


INSERT INTO ${DB_ENV}Achv.Arc_SAP_Pricing
(
         SequenceId
        ,ContentType
        ,SubContentType
        ,TransactionTS
        ,ExtractionTS
        ,IEReceivedTS
        ,IEDeliveryTS
        ,SourceVersion
        ,RecordCount
        ,LoadIndicator
        ,TS
        ,XmlData
        ,ArchiveKey
        ,JobRunID
)
SELECT
         Price.SequenceId
        ,Price.ContentType
        ,Price.SubContentType
        ,Price.TransactionTS
        ,Price.ExtractionTS
        ,Price.IEReceivedTS
        ,Price.IEDeliveryTS
        ,Price.SourceVersion
        ,Price.RecordCount
        ,Price.LoadIndicator
        ,Price.TS
        ,Price.XmlData
        ,Price.ArchiveKey
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_Pricing Price

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_Pricing_Full_Load PriceQ
        ON
        Price.SequenceId      = PriceQ.SequenceId
    AND Price.TransactionTS   = PriceQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Pricing_Load_Cache PriceCache
        ON
        Price.SequenceId      = PriceCache.SequenceId
    AND Price.TransactionTS   = PriceCache.ReferenceTS	
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Pricing_Item PriceMas
        ON
        Price.ArchiveKey = PriceMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Price.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Pricing'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Price.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Pricing'
        AND XPV.ResolveStatus = 'E'

WHERE
        Price.LoadIndicator <> 'D'         AND
        PriceQ.SequenceId    IS NULL       AND
        PriceCache.SequenceId   IS NULL    AND
		PriceMas.ArchiveKey     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL

;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_Pricing WHERE ArchiveKey IN
(
SELECT
        Price.ArchiveKey
FROM
        ${DB_ENV}StgSAPT.Stg_Pricing Price

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_Pricing_Full_Load PriceQ
        ON
        Price.SequenceId      = PriceQ.SequenceId
    AND Price.TransactionTS   = PriceQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Pricing_Load_Cache PriceCache
        ON
        Price.SequenceId      = PriceCache.SequenceId
    AND Price.TransactionTS   = PriceCache.ReferenceTS	
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_Pricing_Item PriceMas
        ON
        Price.ArchiveKey = PriceMas.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Price.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_Pricing'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Price.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_Pricing'
        AND XPV.ResolveStatus = 'E'

WHERE
        Price.LoadIndicator <> 'D'         AND
        PriceQ.SequenceId    IS NULL       AND
        PriceCache.SequenceId   IS NULL    AND
		PriceMas.ArchiveKey     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
) AND LoadIndicator <> 'D'
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 11) SAP ComponentList Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_SAP_ComponentList
(
         SequenceId       
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData      
        ,JobRunId      
)
SELECT
         SapCmpList.SequenceId        
        ,SapCmpList.ContentType         
        ,SapCmpList.SubContentType    
		,SapCmpList.TransactionTS	
        ,SapCmpList.ExtractionTS   
        ,SapCmpList.IEReceivedTS   
        ,SapCmpList.IEDeliveryTS   
        ,SapCmpList.SourceVersion  
        ,SapCmpList.TS             
        ,SapCmpList.ArchiveKey   
		,SapCmpList.XmlData      
		,$JobRunID AS JobRunID
FROM
        ${DB_ENV}StgSAPT.Stg_ComponentList SapCmpList

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ComponentListQ SapCmpListQ
        ON
        SapCmpList.SequenceId   = SapCmpListQ.SequenceId
    AND SapCmpList.TransactionTS    = SapCmpListQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_ComponentList_Load_Cache SapCmpListCache
        ON
        SapCmpList.SequenceId   = SapCmpListCache.SequenceId
    AND SapCmpList.TransactionTS    = SapCmpListCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_ComponentList_E1MASTM S1CmpList
        ON
        SapCmpList.ArchiveKey = S1CmpList.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        SapCmpList.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_ArticleComponentList'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        SapCmpList.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_ArticleComponentList'
        AND XPV.ResolveStatus = 'E'

WHERE
        SapCmpListQ.SequenceId       IS NULL      AND
        SapCmpListCache.SequenceId   IS NULL      AND
        S1CmpList.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL

;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_ComponentList WHERE ArchiveKey IN
(
SELECT
         SapCmpList.ArchiveKey
FROM
        ${DB_ENV}StgSAPT.Stg_ComponentList SapCmpList

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ComponentListQ SapCmpListQ
        ON
        SapCmpList.SequenceId   = SapCmpListQ.SequenceId
    AND SapCmpList.TransactionTS    = SapCmpListQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_ComponentList_Load_Cache SapCmpListCache
        ON
        SapCmpList.SequenceId   = SapCmpListCache.SequenceId
    AND SapCmpList.TransactionTS    = SapCmpListCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_SAP_ComponentList_E1MASTM S1CmpList
        ON
        SapCmpList.ArchiveKey = S1CmpList.ArchiveKey
	        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        SapCmpList.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_ArticleComponentList'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        SapCmpList.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_ArticleComponentList'
        AND XPV.ResolveStatus = 'E'

WHERE
        SapCmpListQ.SequenceId       IS NULL      AND
        SapCmpListCache.SequenceId   IS NULL      AND
        S1CmpList.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 12) CAMPAIGN ECC Load Archiving
------------------------------------------------------------------------------------------


INSERT INTO ${DB_ENV}Achv.Arc_ECC_PROMOTION
(
		SequenceId
		,ContentType
		,SubContentType
		,TransactionTS
		,ExtractionTS
		,IEReceivedTS
		,IEDeliveryTS
		,SourceVersion
		,LoadIndicator
		,TS
		,ArchiveKey
		,XmlData  
        ,JobRunId      
)
SELECT
		 ECC_PROMOTION.SequenceId
		,ECC_PROMOTION.ContentType
		,ECC_PROMOTION.SubContentType
		,ECC_PROMOTION.TransactionTS
		,ECC_PROMOTION.ExtractionTS
		,ECC_PROMOTION.IEReceivedTS
		,ECC_PROMOTION.IEDeliveryTS
		,ECC_PROMOTION.SourceVersion
		,ECC_PROMOTION.LoadIndicator
		,ECC_PROMOTION.TS
		,ECC_PROMOTION.ArchiveKey
		,ECC_PROMOTION.XmlData
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_ECC_PROMOTION ECC_PROMOTION

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ECC_PROMOTIONQ ECC_PROMOTIONQ
        ON
        ECC_PROMOTION.SequenceID   = ECC_PROMOTIONQ.SequenceID
    AND ECC_PROMOTION.TransactionTS    = ECC_PROMOTIONQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_ECC_PROMOTION_Load_Cache ECC_PROMOTIONCache
        ON
        ECC_PROMOTION.SequenceID   = ECC_PROMOTIONCache.SequenceID
    AND ECC_PROMOTION.TransactionTS    = ECC_PROMOTIONCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_ECC_Promotion_Header ECC_Promotion_Header
        ON
        ECC_PROMOTION.ArchiveKey = ECC_Promotion_Header.ArchiveKey
       
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        ECC_PROMOTION.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_ECC_PROMOTION'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        ECC_PROMOTION.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_ECC_PROMOTION'
        AND XPV.ResolveStatus = 'E'

WHERE
        ECC_PROMOTIONQ.SequenceID       IS NULL      AND
        ECC_PROMOTIONCache.SequenceID   IS NULL      AND
        ECC_Promotion_Header.ArchiveKey  IS NULL      AND       
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_ECC_PROMOTION WHERE ArchiveKey IN
(
SELECT
         ECC_PROMOTION.ArchiveKey

FROM
        ${DB_ENV}StgSAPT.Stg_ECC_PROMOTION ECC_PROMOTION

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_ECC_PROMOTIONQ ECC_PROMOTIONQ
        ON
        ECC_PROMOTION.SequenceID   = ECC_PROMOTIONQ.SequenceID
    AND ECC_PROMOTION.TransactionTS    = ECC_PROMOTIONQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_Vendor_Load_Cache ECC_PROMOTIONCache
        ON
        ECC_PROMOTION.SequenceID   = ECC_PROMOTIONCache.SequenceID
    AND ECC_PROMOTION.TransactionTS    = ECC_PROMOTIONCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_ECC_Promotion_Header Promotion_Header
        ON
        ECC_PROMOTION.ArchiveKey = Promotion_Header.ArchiveKey
       
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        ECC_PROMOTION.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_ECC_PROMOTION'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        ECC_PROMOTION.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_ECC_PROMOTION'
        AND XPV.ResolveStatus = 'E'

WHERE
        ECC_PROMOTIONQ.SequenceID       IS NULL      AND
        ECC_PROMOTIONCache.SequenceID   IS NULL      AND
        Promotion_Header.ArchiveKey  IS NULL      AND
 		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
        GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 13) CAMPAIGN PMR Load Archiving
------------------------------------------------------------------------------------------


INSERT INTO ${DB_ENV}Achv.Arc_PMR_PROMOTION
(
		SequenceId
		,ContentType
		,SubContentType
		,TransactionTS
		,ExtractionTS
		,IEReceivedTS
		,IEDeliveryTS
		,SourceVersion
		,LoadIndicator
		,TS
		,ArchiveKey
		,XmlData  
        ,JobRunId      
)
SELECT
		 PMR_PROMOTION.SequenceId
		,PMR_PROMOTION.ContentType
		,PMR_PROMOTION.SubContentType
		,PMR_PROMOTION.TransactionTS
		,PMR_PROMOTION.ExtractionTS
		,PMR_PROMOTION.IEReceivedTS
		,PMR_PROMOTION.IEDeliveryTS
		,PMR_PROMOTION.SourceVersion
		,PMR_PROMOTION.LoadIndicator
		,PMR_PROMOTION.TS
		,PMR_PROMOTION.ArchiveKey
		,PMR_PROMOTION.XmlData
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgSAPT.Stg_PMR_PROMOTION PMR_PROMOTION

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_PMR_PROMOTIONQ PMR_PROMOTIONQ
        ON
        PMR_PROMOTION.SequenceID   = PMR_PROMOTIONQ.SequenceID
    AND PMR_PROMOTION.TransactionTS    = PMR_PROMOTIONQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_PMR_PROMOTION_Load_Cache PMR_PROMOTIONCache
        ON
        PMR_PROMOTION.SequenceID   = PMR_PROMOTIONCache.SequenceID
    AND PMR_PROMOTION.TransactionTS    = PMR_PROMOTIONCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_PMR_Offer_Header PMR_Offer_Header
        ON
        PMR_PROMOTION.ArchiveKey = PMR_Offer_Header.ArchiveKey
       
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        PMR_PROMOTION.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_PMR_PROMOTION'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        PMR_PROMOTION.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_PMR_PROMOTION'
        AND XPV.ResolveStatus = 'E'

WHERE
        PMR_PROMOTIONQ.SequenceID       IS NULL      AND
        PMR_PROMOTIONCache.SequenceID   IS NULL      AND
        PMR_Offer_Header.ArchiveKey  IS NULL      AND        
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgSAPT.Stg_PMR_PROMOTION WHERE ArchiveKey IN
(
SELECT
         PMR_PROMOTION.ArchiveKey

FROM
        ${DB_ENV}StgSAPT.Stg_PMR_PROMOTION PMR_PROMOTION

LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.Stg_PMR_PROMOTIONQ PMR_PROMOTIONQ
        ON
        PMR_PROMOTION.SequenceID   = PMR_PROMOTIONQ.SequenceID
    AND PMR_PROMOTION.TransactionTS    = PMR_PROMOTIONQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.SAP_PMR_PROMOTION_Load_Cache PMR_PROMOTIONCache
        ON
        PMR_PROMOTION.SequenceID   = PMR_PROMOTIONCache.SequenceID
    AND PMR_PROMOTION.TransactionTS    = PMR_PROMOTIONCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgSAPT.S1_PMR_Offer_Header Offer_Header
        ON
        PMR_PROMOTION.ArchiveKey = Offer_Header.ArchiveKey
       
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        PMR_PROMOTION.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'SAP_PMR_PROMOTION'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        PMR_PROMOTION.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'SAP_PMR_PROMOTION'
        AND XPV.ResolveStatus = 'E'

WHERE
        PMR_PROMOTIONQ.SequenceID       IS NULL      AND
        PMR_PROMOTIONCache.SequenceID   IS NULL      AND
        Offer_Header.ArchiveKey  IS NULL      AND
 		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
        GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 14) JobRun Finish
------------------------------------------------------------------------------------------

UPDATE ${DB_ENV}MetaDataVIN.JobRun
SET JobEnd = CURRENT_TIMESTAMP
WHERE
JobRunID = $JobRunID
;


.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE
COMMIT WORK;
.IF ERRORCODE <> 0 THEN  .QUIT ERRORCODE

.QUIT 0


------------------------------------------------------------------------------------------
-- 15) Error Handling
------------------------------------------------------------------------------------------

.LABEL RETURNERROR

.QUIT ERRORCODE" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT START ----------------" >> $LOG_FILE
echo >> $LOG_FILE

cat $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT END ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS START AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

bteq < $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE 2>&1

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS END AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "start time $start_time" >> $LOG_FILE
echo "end time `date`" >> $LOG_FILE


BTEQ_RETURN_CODE=""
BTEQ_RETURN_CODE=`grep 'RC (return code)' $LOG_FILE `

test "$BTEQ_RETURN_CODE" = "" && {
        echo "Error: Unable to execute $BTEQ_ARCHIVE_SOURCE_SCRIPT"
        echo "Error: Could not find BTEQ_RETURN_CODE from $LOG_FILE"
        echo "Info: Try executing the command task again, check logon user and password"
        return -1
}

BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`
echo "BTEQ_RETURN_CODE ="$BTEQ_RETURN_CODE

if [ "$BTEQ_RETURN_CODE" = "0" ]; then
        rm  -f $BTEQ_ARCHIVE_SOURCE_SCRIPT
        rm  -f $LOG_FILE
        rm -f $LOGON_FILE
        #rm -f $PMRootDir/log/`basename $0`_$JobRunID"_"$CURRENT_TS"_script".log
        return $BTEQ_RETURN_CODE
        

else
        return -1
fi