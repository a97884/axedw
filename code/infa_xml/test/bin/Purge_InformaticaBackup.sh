#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Purge_InformaticaBackup.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/Purge_InformaticaBackup.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

#!/usr/bin/ksh
#This script will remove backups of the infa_shared repository that are older than 7 days
#set -x
edw_env=''

        if [[ $(hostname -s) = dwpret02 ]]
        then
                        edw_env=pr

        elif [[ $(hostname -s) = dwitet02 ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

LOGFILE=/opt/axedw/${edw_env}/is_axedw1/infa_shared/log/$(hostname -s)_cleanup_infabackup.log


cd /opt/axedw/${edw_env}/is_axedw1/infa_shared/Backup

find . -name "*.rep" -mtime +7 -exec ls -l {} \; >> ${LOGFILE}
find . -name "*.rep" -mtime +7 -exec rm {} \;
