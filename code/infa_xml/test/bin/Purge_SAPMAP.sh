#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Purge_SAPMAP.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/Purge_SAPMAP.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#!/usr/bin/ksh

edw_env=''

        if [[ $(hostname -s) = dwpret02 ]]
        then
                        edw_env=pr

        elif [[ $(hostname -s) = dwitet02 ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

export DATE=`date +"%Y%m%d%H%M%S"`

mv /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Export_SAPMAP/EDW_SAPMAP.txt /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Export_SAPMAP/EDW_SAPMAP_${DATE}

find /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Export_SAPMAP/EDW_SAPMAP_2* -mtime +14 -exec rm {} \;
