#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CollectStatistics_5.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/CollectStatistics_5.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=COLLECT_STAT;' FOR SESSION;

--********************************************************************************
-- E tabeller (N�gra av dessa kommer att f� uppdaterad statistik i ETL-mappningar,
-- p� de �vriga kan statistiken efter analys tas bort (droppas))
--********************************************************************************

COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_DL_SALES_STORE_DATE;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_DL_SALES_TRANSACTION;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_DL_SALES_TRANSACTION_DISC;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_DL_SALES_TRANS_DISC_RANK;
--COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_DM_SALES_TRANSACTION;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_OAS_HIST_POS_REQ_MAP;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_OAS_SALES_STORE_DATE;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_OAS_SALES_TRANSACTION;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E2_OAS_HIST_SALES_REQ;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E2_OAS_HIST_SALES_REQ_WEEKLY;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_SAPDM_SALES_TRANSACTION;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.E1_SAPMAP_SALES_TRANSACTION;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_DIREKTMEDIA_SALES;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_DL_HFSG;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_DL_HFSG_SG;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_DL_PERIOD_HEADER;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_DL_PERIOD_HEADER_SG;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_DL_POS_CHECKSUM;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_DL_POS_CHECKSUM_SG;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_AXCRM_Q;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_OAS;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_OAS_HIST_DAILY;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_OAS_HIST_WEEKLY;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_SAPDM;
COLLECT STATISTICS ON ${DB_ENV}SemEXPT.ER_SAPMAP;



SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
