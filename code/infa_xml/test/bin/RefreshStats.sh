#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: RefreshStats.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/RefreshStats.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
	current_day_num="`date +%d`"
	oddeven_current_day_num="`expr $current_day_num % 2`"
	for FILE in $PMRootDir/bin/CollectStatistics_[0-9]*.sh
	do
			filenum="`echo $FILE | sed -e 's,.*_,,' -e 's,\..*,,'`" # extract filenumber from filename
			oddeven_filenum="`expr $filenum % 2`" # 1 = filenum is odd, 0 = filenum is even
			if [ "$oddeven_filenum" = "$oddeven_current_day_num" -a "$filenum" -gt "5" ]
			then
					echo "$FILE: filenum:$filenum oddeven_filenum:$oddeven_filenum oddeven_current_day_num:$oddeven_current_day_num"
					$FILE $PMRootDir &
			elif [ "$filenum" -le "5" ]
			then
					$FILE $PMRootDir &
			fi
	done

wait

)  2>&1 | tee $logfile



