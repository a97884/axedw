#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Backup.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/Backup.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
############################################################################
# -----------
# Description:
# -----------
# This program has been modified to release HUT locks if a Teradata
# BAR databas backup exit with a none 0 SEVERITY code. The release
# of HUT locks are done by execute an arc script that contains all
# the required information that are needed to execute the release of
# HUT locks for the specified database.
#
# An argumnet for the database name are required to execute this program
# together with the BAR archive script.
# The release HUT lock script must be pre-configured and tested with the
# following naming convention "release_HUT_lock_${td_db}.arc". The variabel
# <${td_db}> value contain the databse name that are entered when this program
# are running.
# The are no naming convention for the BAR archive script that are required
# by this program.
#
# Modified by:          Jesper Gullberg, Middlecon
# Modified date:        2013-07-12
#
# Modified by:          Jesper Gullberg, Middlecon
# Modified date:        2013-09-30
# Description:          Run the release lock function without testing
#                       the backup RC code. This is a fix becuase when
#                       Teradata backup are initiated by CA Unicenter
#                       the error codes do not inherits back to the
#                       origin startbar script.
#
############################################################################

. $HOME/.infascm_profile

tarauser=Administrator
tarapwd=Teradata1
taraserver=dwprba02.axfood.se
tara_run=/opt/teradata/tara/gui/bin/taralaunch

prg_arg_chk()
{

                if [[ -z ${bar_script} ]] || [[ -z ${td_db} ]]
                then
                                # Initiate function <prg_usage>.

                                display_help
                fi

}

run_backup()
{

        ssh ${taraserver} ${tara_run} -j "${bar_script}" -u "${tarauser}" -p "${tarapwd}" -s DWPRBA02 -w

        bar_status=$?

        ntd_db=$(print ${td_db} | tr  [:upper:] [:lower:])

        if [[ "${ntd_db}" != "dbc" ]]
        then
                # Execute the function <release_hut_lock>.

                release_hut_lock

        fi
}

release_hut_lock()
{

        ssh ${taraserver} ${tara_run} -j "release_HUT_lock_${td_db}.arc" -u "${tarauser}" -p "${tarapwd}" -s DWPRBA02 -w

        release_status=${?}

        print "\n\tThe db ${td_db} Relase HUT lock exit code are ${release_status}"
        # print "\n\tThe BAR backup of db ${td_db} FAILED!!!!\n\tRelase HUT lock exit code are ${release_status}"
}

display_help()
{

set -x

        print "\t\nUSAGE:\n\t\t${0##*/} -s <bar script> -d <TD db name>\n\n"

        exit 300
}

################################################################
############################# MAIN #############################
################################################################


                while getopts s:d: args
                do
                case ${args} in
                               s) bar_script=${OPTARG};;
                               d) td_db=${OPTARG};;
                            ?) display_help;;

        esac
                done

#
# Function <prg_arg_chk>
#

        prg_arg_chk

#
# Function <run_backup>
#
        run_backup


exit ${bar_status}