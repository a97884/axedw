#!/bin/ksh

export PMRootDir=$1
export FILE_DIR="$PMRootDir/SrcFiles/Axbo/InvAdjust"
export SCRIPT_DIR="$PMRootDir/bin"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"
export FILE_LIST_NAME="xmllist"
export I_StoreId=$2
export I_SequenceId=$3
export I_ReceiptSequenceNr=$4
export I_ReferenceTS=$5

exec >$PMRootDir/log/`basename $0`.log 2>&1 

echo "PMRootDir=$1"
echo "I_StoreId=$2"
echo "I_SequenceId=$3"
echo "I_ReceiptSequenceNr=$4"
echo "I_ReferenceTS=$5"
if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

#  Print script syntax and help

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <StoreId> <SequenceId> <ReceiptSequenceNr> <ReferenceTS>"
    echo ""
    echo "   Base directory is   : $FILE_DIR"
    echo ""
    }

############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 5 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
# Check directory for XML-files                                            #
############################################################################

echo ""
echo "###############################################################################"
echo "# Checking directory for XML-files ...                                        #"
echo "###############################################################################"
echo ""

if [ -e "$FILE_DIR" ] ; then
    echo "   Directory $FILE_DIR exists."
else
	echo "  Directory $FILE_DIR does not exist.		"
	echo "  Exiting..  "
	echo ""

    	exit 1
fi

############################################################################
#  Run TPT to export XML-files                                             #
############################################################################
echo ""
echo "###############################################################################"
echo "# Starting TPT job to export XML-files ...                                    #"
echo "###############################################################################"
echo "" 
tbuild -f $SCRIPT_DIR/InvAdjust_exp.tpt -v $SCRIPT_DIR/tptrun.var -u "DB_ENV='$DB_ENV', TargetFileName = '$FILE_LIST_NAME', LobDirPath = '$FILE_DIR', StoreId = '$I_StoreId', SequenceId = '$I_SequenceId', ReceiptSequenceNr = '$I_ReceiptSequenceNr', ReferenceTS = '$I_ReferenceTS'"
