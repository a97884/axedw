#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: archive_CUB.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/archive_CUB.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

DB_ENVAchv="$DB_ENV"Achv
DB_ENVStgIntactixT="$DB_ENV"StgCUBT
DB_ENVMetaDataVIN="$DB_ENV"MetaDataVIN

JobRunID=$2
echo "JobRunID=$JobRunID"

CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS=$CURRENT_TS"

exec >$PMRootDir/log/`basename $0`_"Arch_"$CURRENT_TS"_script".log 2>&1 

echo "writing to log"
echo "PMRootDir = $PMRootDir"
echo "DB_ENV = $DB_ENV"
echo "CURRENT_TS = $CURRENT_TS"
echo "JobRunID = $JobRunID"

echo "DB_ENVAchv = ${DB_ENV}Achv"
echo "DB_ENVStgIntactixT = ${DB_ENV}StgCUBT"
echo "DB_ENVMetaDataVIN = ${DB_ENV}MetaDataVIN"


###################################################################################################
#  Print script syntax and help
###################################################################################################

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <JobRunID>"
    echo ""
    echo "   Base directory is   : PMRootDir"
    echo "   <JobRunID>         : JobRunID to archive"
    echo ""
    }


############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


############################################################################
# Script and log file directories                                          #
############################################################################

# Log Directory
LOG_DIR=$1/log
echo "LOG_DIR = " $LOG_DIR


# Bteq Log File
LOG_FILE=$LOG_DIR/`basename $0 .sh`_$JobRunID"_"$CURRENT_TS"_bteq".log
echo "LOG_FILE = " $LOG_FILE
#chmod 777 $LOG_FILE


# Bteq Directory
SCRIPT_DIR=$1/bin
echo "SCRIPT_DIR = " $SCRIPT_DIR


# Bteq File
#BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/bin/`basename $0 .sh`_$CURRENT_TS.bteq
BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/tmp/`basename $0 .sh`_$CURRENT_TS.bteq
echo "BTEQ_ARCHIVE_SOURCE_SCRIPT = " $BTEQ_ARCHIVE_SOURCE_SCRIPT 
>$BTEQ_ARCHIVE_SOURCE_SCRIPT
#chmod 777 $BTEQ_ARCHIVE_SOURCE_SCRIPT


############################################################################
# TO GET LOGON INFORMATION FOR BTEQ                                        #
############################################################################

PWD_FILE=$SCRIPT_DIR/PWD_BteqLogon
echo "PWD_FILE = " $PWD_FILE

LOGON=`grep $DB_ENV"_"LOGON $PWD_FILE `

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	return -1 
}

#LOGON_FILE=$SCRIPT_DIR/$DB_ENV"_"$JobRunID"_LOGON"
LOGON_FILE=$1/tmp/$DB_ENV"_"$JobRunID"_LOGON"
echo "LOGON_FILE = " $LOGON_FILE
#chmod 777 $LOGON_FILE

LOGON=`echo $LOGON | cut -d "=" -f2`

#Test condition to check if the file already exists

if [ -f $LOGON_FILE ]		
then
	#echo "Logon File Exists"
	EXISTING_LOGON=`head -1 $LOGON_FILE`
	
	if [[ $LOGON = $EXISTING_LOGON ]]
	then
	echo "Same credentials reusing existing file"
	else
	echo $LOGON > $LOGON_FILE
	fi
else 
	#file does not exist, use latest credentials
	echo $LOGON > $LOGON_FILE
fi
chmod 777 $LOGON_FILE

echo "
.SET SESSION TRANSACTION ANSI;

.RUN FILE = $LOGON_FILE" > $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=CreateArchiveTables;ClientUser=$LOGNAME;' FOR SESSION;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "commit;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

############################################################################
# BTEQ                                                                                     
############################################################################

echo "
-- ---------------------------------------------------------------------------------------------------------------------
-- The output may exceed the standard 80 characters, it is expanded to 5000
-- ---------------------------------------------------------------------------------------------------------------------
.width 5000
.set format on
.foldline off" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

start_time=`date`
echo "start_time = " $start_time > $LOG_FILE


echo "
------------------------------------------------------------------------------------------
-- 1) CUB  Load Archiving
------------------------------------------------------------------------------------------


INSERT INTO ${DB_ENV}Achv.Arc_CUB_FREIGHT_CARRIER 
(
PickingSlipId,
    DeliveryPieceId,
    DeliveryPieceName,
    Amount,
    TransactionTS,
    TS,
    ArchiveKey,
    JobRunId
)
SELECT
STGFC.PickingSlipId,
    STGFC.DeliveryPieceId,
    STGFC.DeliveryPieceName,
    STGFC.Amount,
    STGFC.TransactionTS,
    STGFC.TS,
    STGFC.ArchiveKey,
    $JobRunID AS JobRunID
FROM
        ${DB_ENV}StgCUBT.STG_CUB_FREIGHT_CARRIER STGFC

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_FREIGHT_CARRIER S2FC
        ON
        STGFC.PickingSlipId   = S2FC.PickingSlipId
		AND
		STGFC.DeliveryPieceId = S2FC.DeliveryPieceId
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgCUBT.STG_CUB_FREIGHT_CARRIER  WHERE ArchiveKey IN
(
SELECT
         STGFC.ArchiveKey
FROM
        ${DB_ENV}StgCUBT.STG_CUB_FREIGHT_CARRIER STGFC

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_FREIGHT_CARRIER S2FC
        ON
        STGFC.PickingSlipId   = S2FC.PickingSlipId
		AND
		STGFC.DeliveryPieceId = S2FC.DeliveryPieceId
  
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

--
--

INSERT INTO ${DB_ENV}Achv.Arc_CUB_PICKLIST 
(
PickingSlipId,
    SalesTranId,
    StoreId,
    ContactAccountId,
    PickingSlipStatusCd,
    PickingSlipUpdatedDt,
    OrderDt,
    EstDttmReadyForRouting,
    DateRouted,
    SelectedDate,
    DeliveryAddress,
    TransactionTS,
    TS,
    ArchiveKey,
    JobRunId
)
SELECT
STG.PickingSlipId,
    STG.SalesTranId,
    STG.StoreId,
    STG.ContactAccountId,
    STG.PickingSlipStatusCd,
    STG.PickingSlipUpdatedDt,
    STG.OrderDt,
    STG.EstDttmReadyForRouting,
    STG.DateRouted,
    STG.SelectedDate,
    STG.DeliveryAddress,
    STG.TransactionTS,
    STG.TS,
    STG.ArchiveKey,
    $JobRunID AS JobRunID
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST  S2
        ON
        STG.PickingSlipId   = S2.PickingSlipId
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgCUBT.STG_CUB_PICKLIST  WHERE ArchiveKey IN
(
SELECT
         STG.ArchiveKey
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST S2
        ON
        STG.PickingSlipId   = S2.PickingSlipId
  
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

--
--

INSERT INTO ${DB_ENV}Achv.Arc_CUB_PICKLIST_ITEM 
(
 PickingSlipId,
    PickingSlipLineNo,
    ScanCd,
    ScanDesc,
    OrderedQty,
    PickedQty,
    PickedDttm,
    UserId,
    ReplacementLineNo,
    id,
    ReplacementAllowedInd,
    OrderedOriginCountryId,
    PickingSlipStatusCd,
    SingleBasePrice,
    SinglePrice,
    NetPrice,
    GrossPrice,
    Tax,
    UOM,
    TransactionTS,
    TS,
    ArchiveKey,
    JobRunId
)
SELECT
 STG.PickingSlipId,
    STG.PickingSlipLineNo,
    STG.ScanCd,
    STG.ScanDesc,
    STG.OrderedQty,
    STG.PickedQty,
    STG.PickedDttm,
    STG.UserId,
    STG.ReplacementLineNo,
    STG.id,
    STG.ReplacementAllowedInd,
    STG.OrderedOriginCountryId,
    STG.PickingSlipStatusCd,
    STG.SingleBasePrice,
    STG.SinglePrice,
    STG.NetPrice,
    STG.GrossPrice,
    STG.Tax,
    STG.UOM,
    STG.TransactionTS,
    STG.TS,
    STG.ArchiveKey,
    $JobRunID AS JobRunID
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ITEM STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST_ITEM S2
        ON
        STG.PickingSlipId   = S2.PickingSlipId
		AND
		STG.PickingSlipLineNo = S2.PickingSlipLineNo
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ITEM  WHERE ArchiveKey IN
(
SELECT
         STG.ArchiveKey
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ITEM STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST_ITEM S2
        ON
        STG.PickingSlipId   = S2.PickingSlipId
 		AND
		STG.PickingSlipLineNo = S2.PickingSlipLineNo
 
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

--
--

INSERT INTO ${DB_ENV}Achv.Arc_CUB_PICKLIST_ROUTE 
(
 RouteId,
    StoreId,
    UserId,
    PickingRouteStatusCd,
    PickingRouteSectionCd,
    RouteCreatedDttm,
    RouteStartedDttm,
    RouteFinishedDttm,
    RouteLastUpdatedDttm,
    TransactionTS,
    TS,
    ArchiveKey,
    JobRunId
)
SELECT
    STG.RouteId,
    STG.StoreId,
    STG.UserId,
    STG.PickingRouteStatusCd,
    STG.PickingRouteSectionCd,
    STG.RouteCreatedDttm,
    STG.RouteStartedDttm,
    STG.RouteFinishedDttm,
    STG.RouteLastUpdatedDttm,
    STG.TransactionTS,
    STG.TS,
    STG.ArchiveKey,
    $JobRunID AS JobRunID
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ROUTE STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST_ROUTE S2
        ON
        STG.RouteId   = S2.RouteId
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ROUTE  WHERE ArchiveKey IN
(
SELECT
         STG.ArchiveKey
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ROUTE STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST_ROUTE S2
        ON
        STG.RouteId   = S2.RouteId
 
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

--
--

INSERT INTO ${DB_ENV}Achv.Arc_CUB_PICKLIST_ROUTE_LINE 
(
 RouteId,
    RouteLineNo,
    PickingSlipId,
    PickingSlipLineNo,
    ScanCd,
    ScanDesc,
    PickedDttm,
    OrderedQty,
    PickedQty,
    PickedWeight,
    TransactionTS,
    TS,
    ArchiveKey,
    JobRunId
)
SELECT
 STG.RouteId,
    STG.RouteLineNo,
    STG.PickingSlipId,
    STG.PickingSlipLineNo,
    STG.ScanCd,
    STG.ScanDesc,
    STG.PickedDttm,
    STG.OrderedQty,
    STG.PickedQty,
    STG.PickedWeight,
    STG.TransactionTS,
    STG.TS,
    STG.ArchiveKey,
    $JobRunID AS JobRunID
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ROUTE_LINE STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST_ROUTE_LINE S2
        ON
        STG.RouteId   = S2.RouteId
		AND
		STG.RouteLineNo = S2.RouteLineNo
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ROUTE_LINE  WHERE ArchiveKey IN
(
SELECT
         STG.ArchiveKey
FROM
        ${DB_ENV}StgCUBT.STG_CUB_PICKLIST_ROUTE_LINE STG

INNER JOIN
        ${DB_ENV}StgCUBT.S2_CUB_PICKLIST_ROUTE_LINE S2
        ON
        STG.RouteId   = S2.RouteId
 		AND
		STG.RouteLineNo = S2.RouteLineNo

GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 2) JobRun Finish
------------------------------------------------------------------------------------------

UPDATE ${DB_ENV}MetaDataVIN.JobRun
SET JobEnd = CURRENT_TIMESTAMP
WHERE
JobRunID = $JobRunID
;


.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE
COMMIT WORK;
.IF ERRORCODE <> 0 THEN  .QUIT ERRORCODE

.QUIT 0


------------------------------------------------------------------------------------------
-- 3) Error Handling
------------------------------------------------------------------------------------------

.LABEL RETURNERROR

.QUIT ERRORCODE" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT START ----------------" >> $LOG_FILE
echo >> $LOG_FILE

cat $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT END ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS START AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

bteq < $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE 2>&1

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS END AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "start time $start_time" >> $LOG_FILE
echo "end time `date`" >> $LOG_FILE


BTEQ_RETURN_CODE=""
BTEQ_RETURN_CODE=`grep 'RC (return code)' $LOG_FILE `

test "$BTEQ_RETURN_CODE" = "" && {
        echo "Error: Unable to execute $BTEQ_ARCHIVE_SOURCE_SCRIPT"
        echo "Error: Could not find BTEQ_RETURN_CODE from $LOG_FILE"
        echo "Info: Try executing the command task again, check logon user and password"
        return -1
}

BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`
echo "BTEQ_RETURN_CODE ="$BTEQ_RETURN_CODE

if [ "$BTEQ_RETURN_CODE" = "0" ]; then
        rm  -f $BTEQ_ARCHIVE_SOURCE_SCRIPT
        rm  -f $LOG_FILE
        rm -f $LOGON_FILE
        #rm -f $PMRootDir/log/`basename $0`_$JobRunID"_"$CURRENT_TS"_script".log
        return $BTEQ_RETURN_CODE
        

else
        return -1
fi