#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: pos_exp.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/pos_exp.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------


export PMRootDir=$1
export FILE_DIR="$PMRootDir/SrcFiles/Axbo/POS"
export SCRIPT_DIR="$PMRootDir/bin"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"
export FILE_LIST_NAME="xmllist"
export I_MINTS=$2
export I_MAXTS=$3

exec >$PMRootDir/log/`basename $0`.log 2>&1 

echo "PMRootDir=$PMRootDir"
echo "I_MINTS=$I_MINTS"
echo "I_MAXTS=$I_MAXTS"
if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"
echo "DB_ENV=$DB_ENV"


#  Print script syntax and help

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <MinTS> <MaxTS>"
    echo ""
    echo "   Base directory is   : $POS_DIR"
    echo "   <MinTS>          : Timestamp of first POS to include in load"
    echo "   <MaxTS>          : Timestamp of last POS to include in load"
    echo ""
    }

############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 3 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
# Check directory for XML-files                                            #
############################################################################

echo ""
echo "###############################################################################"
echo "# Checking directory for XML-files ...                                        #"
echo "###############################################################################"
echo ""

if [ -e "$FILE_DIR" ] ; then
    echo "   Directory $FILE_DIR exists."
else
	echo "  Directory $FILE_DIR does not exist.		"
	echo "  Exiting..  "
	echo ""

    	exit 1
fi

############################################################################
#  Run TPT to export XML-files                                             #
############################################################################
echo ""
echo "###############################################################################"
echo "# Starting TPT job to export XML-files ...                                    #"
echo "###############################################################################"
echo "" 
tbuild -f $SCRIPT_DIR/pos_exp.tpt -v $SCRIPT_DIR/tptrun.var -u "DB_ENV='$DB_ENV', TargetFileName = '$FILE_LIST_NAME', LobDirPath = '$FILE_DIR', MinTS = '$I_MINTS', MaxTS = '$I_MAXTS'"
