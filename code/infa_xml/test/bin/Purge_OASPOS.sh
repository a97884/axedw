#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Purge_OASPOS.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/Purge_OASPOS.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#!/bin/ksh
#This script will remove backups of the infa_shared OAS-POS files that are older than 20 days
#set -x

edw_env=''

        if [[ $(hostname -s) = dwpret02 ]]
        then
                        edw_env=pr

        elif [[ $(hostname -s) = dwitet02 ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi



LOGFILE=/opt/axedw/${edw_env}/is_axedw1/infa_shared/log/$(hostname -s)_cleanup_oaspos.log

touch ${LOGFILE}

DIRDAILY=/opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/OAS/POSDaily/Archive
DIRHIST=/opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/OAS/POSHistoric/Archive

        if [ -d $DIRDAILY ]
                then
                        cd $DIRDAILY
                        find . -mtime +20 -exec rm {} \;
                        pwd

                else
                        echo "Warning, ${DIRDAILY} is missing!"
        fi

        if [ -d $DIRHIST ]
                then
                        cd $DIRHIST
                        find . -mtime +20 -exec rm {} \;
                        pwd
                else
                        echo "Warning, ${DIRHIST} is missing!"
        fi


