#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CollectStatistics_2.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/CollectStatistics_2.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=COLLECT_STAT;' FOR SESSION;

--*********************************************
--TgtT tabeller
--*********************************************

.os date 
collect statistics on ${DB_ENV}TgtT.SALES_TRAN_LOYALTY_ACCOUNT;
.os date 
collect statistics on ${DB_ENV}TgtT.SALES_TRANSACTION;
.os date 
collect statistics on ${DB_ENV}TgtT.SALES_TRANSACTION_AGREEMENT;
.os date 
collect statistics on ${DB_ENV}TgtT.SALES_TRANSACTION_CHARGE_LINE;
.os date 
collect statistics on ${DB_ENV}TgtT.SALES_TRANSACTION_LINE_ACTUAL;
.os date 
collect statistics on ${DB_ENV}TgtT.SALES_TRANSACTION_PARTY;
.os date 


SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
