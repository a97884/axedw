#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CollectStatistics_1.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/CollectStatistics_1.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=COLLECT_STAT;' FOR SESSION;

--*************************************************
--METADATA
--*************************************************
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.JobRun;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.SourceLoad;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.StreamLoadRef;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.StreamRun;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.StreamStepRun;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.SourceDeliveryStatistics;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.Target;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.TargetLoad;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.TargetExtractPeriod;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_ACCT;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_ADDRESS;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_ARTICLE;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_ARTICLE_GROUP;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_ARTICLE_HIER_NODE;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_ASSORTMENT;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_CAMPAIGN;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_COST_PRICE_LIST;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_KPI;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LM_VERSION;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LOC;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LOCATOR;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LOYALTY_ATTRIBUTE;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LOYALTY_IDENTIFIER;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LOYALTY_PROGRAM;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LOYALTY_PROMOTION;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_LOYALTY_TIER_LEVEL;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_MU;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_MU_SCAN_CODE;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_PARTY;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_UOM;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_STORE_DEPT;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_POS_REGISTER;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_PARTY;
.os date 
collect statistics on ${DB_ENV}MetaDataT.MAP_SALES_TRANS;
.os date 
collect statistics on ${DB_ENV}MetaDataT.WT_SourceLoadAssortment;
.os date 
collect statistics on ${DB_ENV}MetaDataT.WT_SourceLoadCustomer;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.MAP_PROMOTION_OFFER;
.os date 
COLLECT STATISTICS ON ${DB_ENV}MetadataT.MAP_VENDOR_FUND;
.os date 

SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
