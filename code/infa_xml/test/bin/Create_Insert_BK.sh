#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Create_Insert_BK.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/Create_Insert_BK.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

###################################################################################################
# Copied from Andres script
###################################################################################################

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"


exec >$PMRootDir/log/`basename $0`"_"$2"_script".log 2>&1 

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

TABLE_NAME=$2
echo "TABLE_NAME = " $TABLE_NAME

Len=`expr length $TABLE_NAME`
echo "Table_Name Length = " $Len

Sub_TableName=`expr substr $TABLE_NAME 4 $Len`
echo "Sub_TableName = " $Sub_TableName

BK_TABLE_NAME="BK_"$Sub_TableName
echo "Backup Table Name = " $BK_TABLE_NAME

CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS = " $CURRENT_TS

#exec >$PMRootDir/log/`basename $0`_$Sub_TableName"_"$CURRENT_TS"_script".log 2>&1 

###################################################################################################
#
###################################################################################################

#  Print script syntax and help

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <TableName>"
    echo ""
    echo "   Base directory is   : $1"
    echo "   <TableName>         : Table to create and rename"
    echo ""
    }

############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
# Script and log file directories                                          #
############################################################################

# Log Directory
LOG_DIR=$1/log
echo "LOG_DIR = " $LOG_DIR


# Bteq Log File
LOG_FILE=$LOG_DIR/`basename $0 .sh`_$Sub_TableName"_"$CURRENT_TS"_bteq".log
echo "LOG_FILE = " $LOG_FILE
#chmod 777 $LOG_FILE


# Script Directory
SCRIPT_DIR=$1/bin
echo "SCRIPT_DIR = " $SCRIPT_DIR

# TMP Directory
TMP_DIR=$1/tmp
echo "TMP_DIR = " $TMP_DIR


BTEQ_PRE_APP_SCRIPT=$TMP_DIR/`basename $0 .sh`$CURRENT_TS.bteq
echo "BTEQ_PRE_APP_SCRIPT= " $BTEQ_PRE_APP_SCRIPT
>$BTEQ_PRE_APP_SCRIPT
#chmod 777 $BTEQ_PRE_APP_SCRIPT


############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

PWD_FILE=$SCRIPT_DIR/PWD_BteqLogon
echo "PWD_FILE = " $PWD_FILE

LOGON=`grep $DB_ENV"_"LOGON $PWD_FILE `

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	return -1 
}

LOGON_FILE=$TMP_DIR/$DB_ENV"_"$TABLE_NAME"_LOGON"
echo "LOGON_FILE = " $LOGON_FILE
#chmod 777 $LOGON_FILE

LOGON=`echo $LOGON | cut -d "=" -f2`

#Test condition to check if the file already exists

if [ -f $LOGON_FILE ]		
then
	#echo "Logon File Exists"
	EXISTING_LOGON=`head -1 $LOGON_FILE`
	
	if [[ $LOGON = $EXISTING_LOGON ]]
	then
	echo "Same credentials reusing existing file"
	else
	echo $LOGON > $LOGON_FILE
	fi
else 
	#file does not exist, use latest credentials
	echo $LOGON > $LOGON_FILE
fi
#chmod 777 $LOGON_FILE

############################################################################
# BTEQ			Start     						   #
############################################################################


echo ".SET SESSION TRANSACTION ANSI;" > $BTEQ_PRE_APP_SCRIPT

echo ".RUN FILE = $LOGON_FILE" >> $BTEQ_PRE_APP_SCRIPT

echo "SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=CreateBKtables;ClientUser=$LOGNAME;' FOR SESSION;" >> $BTEQ_PRE_APP_SCRIPT
      
echo "commit;" >> $BTEQ_PRE_APP_SCRIPT

echo "
.width 5000
.set format on
.foldline off" >> $BTEQ_PRE_APP_SCRIPT

start_time=`date`
echo "start_time = " $start_time > $LOG_FILE

echo "
.SET ERRORLEVEL (3807) SEVERITY 0" >> $BTEQ_PRE_APP_SCRIPT

echo "
DROP TABLE "$DB_ENV"CntlLoadReadyT."$BK_TABLE_NAME" ;" >> $BTEQ_PRE_APP_SCRIPT

echo "
.SET ERRORLEVEL (3807) SEVERITY 8" >> $BTEQ_PRE_APP_SCRIPT

echo "
COMMIT WORK;" >> $BTEQ_PRE_APP_SCRIPT

echo "
CREATE TABLE "$DB_ENV"CntlLoadReadyT."$BK_TABLE_NAME" AS "$DB_ENV"CntlLoadReadyT."$TABLE_NAME" WITH NO DATA;" >> $BTEQ_PRE_APP_SCRIPT

echo "
.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE" >> $BTEQ_PRE_APP_SCRIPT

echo "
COMMIT WORK;" >> $BTEQ_PRE_APP_SCRIPT

echo "
.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE" >> $BTEQ_PRE_APP_SCRIPT

echo "
INSERT INTO "$DB_ENV"CntlLoadReadyT."$BK_TABLE_NAME" SELECT * FROM "$DB_ENV"CntlLoadReadyT."$TABLE_NAME";" >> $BTEQ_PRE_APP_SCRIPT

echo "
.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE" >> $BTEQ_PRE_APP_SCRIPT

echo "
COMMIT WORK;" >> $BTEQ_PRE_APP_SCRIPT

echo "
.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE" >> $BTEQ_PRE_APP_SCRIPT

echo "
	.LOGOFF;
	.QUIT; " >> $BTEQ_PRE_APP_SCRIPT


############################################################################
# BTEQ			End        						     #
############################################################################



echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT START ----------------" >> $LOG_FILE
echo >> $LOG_FILE

cat $BTEQ_PRE_APP_SCRIPT>> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT END ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS START AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

bteq < $BTEQ_PRE_APP_SCRIPT>> $LOG_FILE 2>&1

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS END AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "start time $start_time" >> $LOG_FILE
echo "end time `date`" >> $LOG_FILE


BTEQ_RETURN_CODE=""
BTEQ_RETURN_CODE=`grep 'RC (return code)' $LOG_FILE `

test "$BTEQ_RETURN_CODE" = "" && {
	echo "Error: Unable to execute $BTEQ_PRE_APP_SCRIPT"
	echo "Error: Could not find BTEQ_RETURN_CODE from $LOG_FILE"
	echo "Info: Try executing the command task again, check logon user and password"
	return -1
}

BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`
echo "BTEQ_RETURN_CODE ="$BTEQ_RETURN_CODE

if [ "$BTEQ_RETURN_CODE" = "0" ]; then
	rm -f $BTEQ_PRE_APP_SCRIPT
	rm -f $LOG_FILE
	rm -f $LOGON_FILE
	#rm -f $PMRootDir/log/`basename $0`_$Sub_TableName"_"$CURRENT_TS"_script".log
	rm -f $PMRootDir/log/`basename $0`"_"$2"_script".log
	return $BTEQ_RETURN_CODE
	

else
	return -1
fi