#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Backup_start.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/Backup_start.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
#
############################################################################
# -----------
# Description:
# -----------
# Usage: Backup_start is launched to start a backup
#        It calls a second script (called Backup.sh)
#        Otherwise the release_lock-backup-script
#        wouldn't be launched when backup was failing
#
############################################################################

. $HOME/.infascm_profile

edw_env=''

        if [[ $(hostname -s) = dwpret02 ]]
        then
                        edw_env=pr

        elif [[ $(hostname -s) = dwitet02 ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

bar_bup="/opt/axedw/${edw_env}/is_axedw1/infa_shared/bin"

display_help()
{
        print "\n\tUSAGE:\n\t\t$(basename ${0}) -s <bar script> -d <database name>\n\n"
        exit 355
}

chk_args()
{

set -x

        if [[ -z ${bar_script} ]]
        then
                # Run function <display_help>

                display_help
        fi

        if [[ -z ${td_db} ]]
        then
                # Run function <display_help>

                display_help
        fi
}

	while getopts :s:d: args
	do
		case ${args} in
			s) bar_script=${OPTARG};;
			d) td_db=${OPTARG};;
            ?) display_help;;
		esac
	done

	${bar_bup}/Backup.sh -s "${bar_script}" -d ${td_db}

