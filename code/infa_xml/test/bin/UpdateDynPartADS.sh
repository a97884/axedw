#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: UpdateDynPartADS.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/UpdateDynPartADS.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

PMRootDir=$1
export PMRootDir
PARAM_DIR="$PMRootDir/par"
export PARAM_DIR
PARAM_FILE="axedwparameters.prm"
export PARAM_FILE

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=UPDATE PARTITIONS;' FOR SESSION;

--**********************************************************************
--Update tables with dynamic partitioning (create next months partitions
--**********************************************************************
.os date 
CALL ${DB_ENV}dbadmin.DBA_ReDeIndex ('${DB_ENV}ADST','CL_BASKET_TOTAL_A','DE','SH',1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}ADST','CL_BASKET_TOTAL_A',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_ReDeIndex ('${DB_ENV}ADST','CL_BASKET_TOTAL_A','RE','SH',1);
.os date 

SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile

BTEQ_RETURN_CODE=`grep 'RC (return code)' $logfile`
BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`

if [ "$BTEQ_RETURN_CODE" = "0" ]
then
        exit $BTEQ_RETURN_CODE
else
        exit 1
fi