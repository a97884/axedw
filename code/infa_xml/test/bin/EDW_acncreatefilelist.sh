#!/bin/sh
##############################################################################
#                                                                            #
# acncreatefilelist - Creates file with list of files in Nielsen ftp         #
#                                                                            #
# 2016-04-12 Erik Kaar                                                       #
##############################################################################
HOST='ftp.acnielsen.se'
USER='AX'
PASSWD='4Xsten'
PMRootDir=$1/SrcFiles/Nielsen

ftp -n $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
prompt off
passive
nlist FFS $PMRootDir/ACNFileList.txt
quit
END_SCRIPT
exit 0

