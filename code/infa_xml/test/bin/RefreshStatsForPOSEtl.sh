#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: RefreshStatsForPOSEtl.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/RefreshStatsForPOSEtl.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=RefreshStatsForPOSEtl.sh;Action=COLLECT_STAT;' FOR SESSION;


--*************************************************
-- POS METADATA TABLES
--*************************************************

COLLECT STATISTICS INDEX(TransactionId, RetailStoreID) ON ${DB_ENV}MetadataT.MAP_SALES_TRANS;
COLLECT STATISTICS INDEX(WorkstationID, SAPCustomerID) ON ${DB_ENV}MetadataT.MAP_POS_REGISTER;
COLLECT STATISTICS INDEX(N_Store_Dept_Id) ON ${DB_ENV}MetadataT.MAP_STORE_DEPT;
COLLECT STATISTICS INDEX(N_Tender_Id) ON ${DB_ENV}MetadataT.MAP_TENDER_METHOD;
COLLECT STATISTICS INDEX(N_Node_Id) ON ${DB_ENV}MetadataT.MAP_ARTICLE_HIER_NODE;
COLLECT STATISTICS INDEX(N_Loc_Id) ON ${DB_ENV}MetadataT.MAP_LOC;
COLLECT STATISTICS INDEX(N_Loyalty_Idenfier_Id) ON ${DB_ENV}MetadataT.MAP_LOYALTY_IDENTIFIER;
COLLECT STATISTICS INDEX(N_Scan_Cd) ON ${DB_ENV}MetadataT.MAP_MU_SCAN_CODE;
COLLECT STATISTICS COLUMN(N_Party_Id, MasterSourceId) ON ${DB_ENV}MetadataT.MAP_PARTY;
COLLECT STATISTICS COLUMN(N_Node_Legacy_Id) ON ${DB_ENV}MetadataT.MAP_ARTICLE_HIER_NODE;

--*************************************************
-- C1 TABLES
--*************************************************

COLLECT STATISTICS COLUMN(TransactionId, RetailStoreID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(Control_VoidedTransactionID, RetailStoreID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(SuspendedTransactionId, RetailStoreID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(Control_ReferringTransactionID, RetailStoreID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(WorkstationID, SAPCustomerID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(DepartmentID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(SAPCustomerID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(Control_ItemID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_Receipt;
COLLECT STATISTICS COLUMN(TenderID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem;
COLLECT STATISTICS COLUMN(SumMerchandiseHierarchyGroupID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem;
COLLECT STATISTICS COLUMN(Identifier) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem;
COLLECT STATISTICS COLUMN(ItemID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem;
COLLECT STATISTICS COLUMN(CustomerID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem;
COLLECT STATISTICS COLUMN(POSIdentityID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem;
COLLECT STATISTICS COLUMN(MerchandiseStructureID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem;
COLLECT STATISTICS COLUMN(TenderLineItem_TenderID) ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_TenderLineItem;

SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
