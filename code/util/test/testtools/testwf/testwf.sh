#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: testwf.sh 13015 2014-05-19 16:28:44Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2014-05-19 18:28:44 +0200 (mån, 19 maj 2014) $
# Last Revision    : $Revision: 13015 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/test/testtools/testwf/testwf.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# testwf.sh - test tool for Informatica workflows
#
# read list of folder and workflow from config file and execute workflows in sequence or in parallel (if third column is &
#
# list of workflows to execute should be stored in a config file with the following format:
#
# folder workflow [&]
#
# where folder = name of forlder in Informatica where the workflow is.
# The optional third field can be specified as an ampersand (&) and will put the execution of the workflow in background
#
# The logfile is created in the current directory and is called testwf.sh.YYYYMMDD_HHMMSS.log
#
wf_file="$1"
if [ ! -r "$wf_file" ]
then
        echo "`basename $0`: Unable to open file '$wf_file'" 2>&1
        echo "Usage: `basename $0` wf_file" 2>&1
        echo "wf_file should contain one line for each folder and workflow pair for workflows to execute" 2>&1
        exit 1
fi
bindir=/opt/axedw/uv/is_axedw2/infa_shared/bin
startinfa=execwf.sh
LOG_FILE=`basename $0`.`date +%Y%m%d_%H%M%S`.log
PID_FILE=`basename $0`.`date +%Y%m%d_%H%M%S`.pid

tee "$LOG_FILE" >/dev/tty |&
exec 1>&p
exec 2>&1

#
# read the workflow file and start the execution
#
while read folder wf bkgr
do
        echo "`date`:Starting:$bindir/$startinfa $folder $wf"
        if [ "$bkgr" = "&" ]
        then
                ($bindir/$startinfa "$folder" "$wf" | while read line; do echo "$$: $line"; done) &
                pid=$!
                echo $pid >>$PID_FILE
        else
                $bindir/$startinfa $folder $wf
        fi
        stat=$?
        if [ $stat != 0 ]
        then
                echo "$wf failed with status $stat" >&2
                exit $stat
        fi
        if [ "$bkgr" = "&" ]
        then
                echo "`date`:Scheduled,pid=$pid:$bindir/$startinfa $folder $wf"
        else
                echo "`date`:Ending:$bindir/$startinfa $folder $wf"
        fi
done <"$wf_file"

echo "`date`:Waiting for subprocesses to finish...:"
while read pid
do
        wait $pid
        echo "`date`:done:$pid"

done <$PID_FILE
echo "`date`:done:all"

exit 0
