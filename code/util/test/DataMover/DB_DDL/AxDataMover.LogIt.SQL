REPLACE PROCEDURE AxDataMover.LOGIT  (IN p_Txt VARCHAR(64000))
/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: AxDataMover.LogIt.SQL 9026 2013-04-25 09:41:53Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-04-25 11:41:53 +0200 (tor, 25 apr 2013) $
# Last Revision    : $Revision: 9026 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/test/DataMover/DB_DDL/AxDataMover.LogIt.SQL $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
BEGIN
	INSERT INTO AxDataMover.DM_tmp(LogText)VALUES(:p_Txt);
END;