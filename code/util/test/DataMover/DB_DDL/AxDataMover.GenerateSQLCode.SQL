
REPLACE MACRO AxDataMover.GenerateSQLCode(p_SourceDatabaseName VARCHAR(30),p_TargetDatabaseName VARCHAR(30),p_DataMoveCd VARCHAR(4))
AS 
/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: AxDataMover.GenerateSQLCode.SQL 9056 2013-05-03 14:50:16Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-05-03 16:50:16 +0200 (fre, 03 maj 2013) $
# Last Revision    : $Revision: 9056 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/test/DataMover/DB_DDL/AxDataMover.GenerateSQLCode.SQL $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
( 
WITH S (TargetDatabaseName,SourceDatabaseName,TableName,ColumnName,ColumnId,MaxColumnId,MinColumnId) AS
(
	SELECT DISTINCT TargetDB.DatabaseName AS TargetDatabaseName,
		SourceDB.DatabaseName AS SourceDatabaseName,
		TargetDB.TableName,
		TargetDB.ColumnName,
		TargetDB.ColumnId,
		MAX(TargetDB.ColumnID) OVER (PARTITION BY TargetDB.TableName) AS MaxColumnId,
		MIN(TargetDB.ColumnID) OVER (PARTITION BY TargetDB.TableName) AS MinColumnId
	FROM
	(
		SELECT TRIM(DatabaseName) AS DatabaseName,
			TRIM(TableName) AS TableName,
			TRIM(ColumnName) AS ColumnName,
			ColumnId
		FROM DBC.COLUMNS AS C 
		WHERE C.DatabaseName=:p_SourceDatabaseName
	) AS SourceDB
	JOIN
	(
		SELECT TRIM(DatabaseName) AS DatabaseName,
			TRIM(TableName) AS TableName,
			TRIM(ColumnName) AS ColumnName,ColumnId
		FROM DBC.COLUMNS AS C 
		WHERE C.DatabaseName=:p_TargetDatabaseName
	) AS TargetDB
	ON TargetDB.TableName=SourceDB.TableName AND TargetDB.ColumnName=SourceDB.ColumnName
)
SELECT SQLStmt
FROM 
(
	SELECT S.TableName,1 AS OrderNum,ColumnId,
		CASE 
			WHEN ColumnId=MinColumnId AND ColumnId<>MaxColumnId
				THEN 'DELETE FROM ' || TargetDatabaseName || '.' || TableName || '; INSERT INTO ' || TargetDatabaseName || '.' || TableName || ' (' || ColumnName
			WHEN ColumnId=MaxColumnId
				THEN ',' || ColumnName|| ')'
			WHEN ColumnId<>MinColumnId AND ColumnId<>MaxColumnId
				THEN ',' || ColumnName
		END (VARCHAR(32000)) AS SqlStmt
	FROM S
	
	UNION ALL
	
	SELECT TableName,2 AS OrderNum,ColumnId,
		CASE 
			WHEN ColumnId=MinColumnId AND ColumnId<>MaxColumnId
				THEN 'SELECT ' || ColumnName
			WHEN ColumnId=MaxColumnId
				THEN ',' || ColumnName|| ' FROM ' || S.SourceDatabaseName || '.' || S.TableName ||
						Case when D.FilterTableWhereText is not null
							Then	' Where ' || Cast(SYSLIB.REPLACES(D.FilterTableWhereText,'''','''''') as VARCHAR(3000))
							Else ''
						End || ';'
			WHEN ColumnId<>MinColumnId AND ColumnId<>MaxColumnId
				THEN ',' || ColumnName
		END
		 (VARCHAR(32000)) AS SqlStmt
	FROM S
	LEFT OUTER JOIN AxDataMover.DM_CONFIG D
	On	
			S.SourceDatabaseName = D.TargetDatabaseName
	and S.TableName = D.FilterTableName
	and	D.DataMoveCd = :p_DataMoveCd
) X
ORDER BY TableName, OrderNum,ColumnId
;
 ) ;                        

COMMENT ON AxDataMover.GenerateSQLCode AS 'Macro generates SQL DELETE, INSERT SELECT statements to load data from the same server source db to target';
COMMENT ON AxDataMover.GenerateSQLCode.p_SourceDatabaseName AS 'Source database name';
COMMENT ON AxDataMover.GenerateSQLCode.p_TargetDatabaseName AS 'Target database name';
--EXEC AxDataMover.GenerateSQLCode('DMTgtT','K9108439')

