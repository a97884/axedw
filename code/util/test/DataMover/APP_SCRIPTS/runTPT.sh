#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: runTPT.sh 9068 2013-05-08 14:10:04Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-05-08 16:10:04 +0200 (ons, 08 maj 2013) $
# Last Revision    : $Revision: 9068 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/test/DataMover/APP_SCRIPTS/runTPT.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
tbuild -h 128M -f ./DMM6.LOAD.TPT -u "TargetUserName='dm_admin_user',TargetUserPassword='dm_admin_user', SourceUserName='dm_admin_user',SourceUserPassword='admin_dm_user'"
