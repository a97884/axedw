CREATE DATABASE mmMeta FROM "dbc"
AS PERM=100000000
   ACCOUNT='DBC'
NO FALLBACK
NO BEFORE JOURNAL
NO AFTER JOURNAL;

/* GRANT ALL ON mmMeta TO {user}; */

database mmMeta;

/* Authentication tables ******************************/

create set table mm_users, no fallback,
	no before journal,
	no after journal
(
	userid varchar(25) CHARACTER SET UNICODE not null,
	passwd varchar(100) CHARACTER SET UNICODE not null,
	email varchar(100) CHARACTER SET UNICODE
)
unique primary index pkp_mm_users (userid);

create set table mm_roles, no fallback,
	no before journal,
	no after journal
(
	userid varchar(25) CHARACTER SET UNICODE not null,
	roleid varchar(25) CHARACTER SET UNICODE not null
)
primary index pkp_mm_roles (userid);

insert into mm_users (userid, passwd) values ('mmadmin', 'e122a6a881038d42413cde67d2d2e9');
insert into mm_roles (userid, roleid) values ('mmadmin', 'mm_admin');
insert into mm_users (userid, passwd) values ('mmuser', 'a149e5404bc18e9297a77ef8d5a08d53');
insert into mm_roles (userid, roleid) values ('mmuser', 'mm_user');

/* Quartz scheduler tables ******************************/

create set table qrtz_job_details ,no fallback,
	no before journal,
	no after journal
(
  job_name varchar(80) CHARACTER SET UNICODE not null,
  job_group varchar(80) CHARACTER SET UNICODE not null,
  description varchar(120) CHARACTER SET UNICODE null,
  job_class_name varchar(128) CHARACTER SET UNICODE not null,
  is_durable byteint not null,
  is_volatile byteint not null,
  is_stateful byteint not null,
  requests_recovery byteint not null,
  job_data blob(2000) not null
)
unique primary index pkp_qrtz_job_details (job_name,job_group);

create set table qrtz_job_listeners,no fallback,
	no before journal,
	no after journal
(
  job_name varchar(80) CHARACTER SET UNICODE not null,
  job_group varchar(80) CHARACTER SET UNICODE not null,
  job_listener varchar(80) CHARACTER SET UNICODE not null
)
unique primary index pkp_qrtz_job_listeners (job_name,job_group,job_listener);

create set table qrtz_triggers,no fallback,
	no before journal,
	no after journal
(
  trigger_name varchar(80) CHARACTER SET UNICODE not null,
  trigger_group varchar(80) CHARACTER SET UNICODE not null,
  job_name varchar(80) CHARACTER SET UNICODE not null,
  job_group varchar(80) CHARACTER SET UNICODE not null,
  is_volatile byteint not null,
  description varchar(120) CHARACTER SET UNICODE null,
  next_fire_time decimal(13,0),
  prev_fire_time decimal(13,0),
  priority integer,
  trigger_state varchar(16) CHARACTER SET UNICODE not null,
  trigger_type varchar(8) CHARACTER SET UNICODE not null,
  start_time decimal(13,0) not null,
  end_time decimal(13,0),
  calendar_name varchar(80) CHARACTER SET UNICODE,
  misfire_instr smallint,
  job_data blob(2000) not null
)
unique primary index pkp_qrtz_triggers (trigger_name,trigger_group);

create set table qrtz_simple_triggers,no fallback,
	no before journal,
	no after journal
(
  trigger_name varchar(80) CHARACTER SET UNICODE not null,
  trigger_group varchar(80) CHARACTER SET UNICODE not null,
  repeat_count decimal(13,0) not null,
  repeat_interval decimal(13,0) not null,
  times_triggered decimal(13,0) not null
)
unique primary index pkp_qrtz_simple_triggers (trigger_name,trigger_group);

create set table qrtz_cron_triggers,no fallback,
	no before journal,
	no after journal
(
  trigger_name varchar(80) CHARACTER SET UNICODE not null,
  trigger_group varchar(80) CHARACTER SET UNICODE not null,
  cron_expression varchar(80) CHARACTER SET UNICODE not null,
  time_zone_id varchar(80) CHARACTER SET UNICODE
)
unique primary index pkp_qrtz_cron_triggers (trigger_name,trigger_group);

create set table qrtz_blob_triggers,no fallback,
	no before journal,
	no after journal
(
  trigger_name varchar(80) CHARACTER SET UNICODE not null,
  trigger_group varchar(80) CHARACTER SET UNICODE not null,
  blob_data blob(2000) not null
)
unique primary index pkp_qrtz_blob_triggers (trigger_name,trigger_group);

create set table qrtz_trigger_listeners,no fallback,
	no before journal,
	no after journal
(
  trigger_name varchar(80) CHARACTER SET UNICODE not null,
  trigger_group varchar(80) CHARACTER SET UNICODE not null,
  trigger_listener varchar(80) CHARACTER SET UNICODE not null
)
unique primary index pkp_qrtz_trigger_listeners (trigger_name,trigger_group,trigger_listener);

create set table qrtz_calendars,no fallback,
	no before journal,
	no after journal
(
  calendar_name varchar(80) CHARACTER SET UNICODE not null,
  calendar blob(2000) not null
)
unique primary index pkp_qrtz_calendars (calendar_name);

create set table qrtz_fired_triggers,no fallback,
	no before journal,
	no after journal
(
  entry_id varchar(95) CHARACTER SET UNICODE not null,
  trigger_name varchar(80) CHARACTER SET UNICODE not null,
  trigger_group varchar(80) CHARACTER SET UNICODE not null,
  is_volatile byteint not null,
  instance_name varchar(80) CHARACTER SET UNICODE not null,
  fired_time decimal(13,0) not null,
  priority integer not null,
  state varchar(16) CHARACTER SET UNICODE not null,
  job_name varchar(80) CHARACTER SET UNICODE null,
  job_group varchar(80) CHARACTER SET UNICODE null,
  is_stateful byteint null,
  requests_recovery byteint null
)
unique primary index pkp_qrtz_fired_triggers (entry_id);


create set table qrtz_paused_trigger_grps,no fallback,
	no before journal,
	no after journal
(
  trigger_group  varchar(80) CHARACTER SET UNICODE not null
)
unique primary index pkp_qrtz_paused_trigger_grps (trigger_group);

create set table qrtz_scheduler_state ,no fallback,
	no before journal,
	no after journal
(
  instance_name varchar(80) CHARACTER SET UNICODE not null,
  last_checkin_time decimal(13,0) not null,
  checkin_interval decimal(13,0) not null
)
unique primary index pkp_qrtz_scheduler_state (instance_name);

create set table qrtz_locks ,no fallback,
	no before journal,
	no after journal
(
    lock_name  varchar(40) CHARACTER SET UNICODE not null
)
unique primary index pkp_qrtz_locks  (lock_name);

insert into qrtz_locks values('TRIGGER_ACCESS');
insert into qrtz_locks values('JOB_ACCESS');
insert into qrtz_locks values('CALENDAR_ACCESS');
insert into qrtz_locks values('STATE_ACCESS');
insert into qrtz_locks values('MISFIRE_ACCESS');

CREATE SET TABLE mmMeta.mm_app_status ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT
     (
      App_ID VARCHAR(25) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Event_Datetime TIMESTAMP,
      Job_Status integer
)
Unique PRIMARY INDEX ( App_ID, Event_Datetime );

CREATE SET TABLE mmMeta.mm_model_status ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT
     (
      Model_ID VARCHAR(25) CHARACTER SET UNICODE NOT CASESPECIFIC,
      App_ID VARCHAR(25) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Event_Datetime TIMESTAMP,
      Job_Status integer
)
Unique PRIMARY INDEX ( Model_ID, Event_Datetime );

CREATE SET TABLE mmMeta.mm_dependency ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT
     (
      Model_ID VARCHAR(25),
      App_Id VARCHAR(25) CHARACTER SET UNICODE NOT CASESPECIFIC,
	  App_Type VARCHAR(1) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Retry_Delay integer,
      Retry_Num integer
)
Unique PRIMARY INDEX ( Model_ID, App_Id );

/* GRANT ALL ON {TWM Publish DB} TO {user}; */
