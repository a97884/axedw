// DEPENDENCY: functions in mmCommon.js

function mmValidateScheduler(formObj, msgInvalidDate, msgInvalidTime, msgInvalidCount, msgInvalidEndDate)
{
	var focusObj = null;
	var msg = '';
	
	if (trim(formObj.mmSchedDate.value) != '' && !mmValidateDate(formObj.mmSchedDate.value)) {
		msg += document.getElementById(msgInvalidDate).innerHTML + '\n';
		if (focusObj == null) focusObj = formObj.mmSchedDate;
	}
	
	if (trim(formObj.mmSchedTime.value) != '' && !mmValidateTime(formObj.mmSchedTime.value)) {
		msg += document.getElementById(msgInvalidTime).innerHTML + '\n';
		if (focusObj == null) focusObj = formObj.mmSchedTime;
	}
	
	if (formObj.mmSchedRepeat.checked) {
		// validate these only if repeat option is enabled
		if (formObj.mmSchedDuration[2] != null &&
		    formObj.mmSchedDuration[2].checked &&
		    trim(formObj.mmSchedEndDate.value) != '' &&
			!mmValidateDate(formObj.mmSchedEndDate.value)) {
			msg += document.getElementById(msgInvalidEndDate).innerHTML + '\n';
			if (focusObj == null) focusObj = formObj.mmSchedEndDate;
		}
		
		if (formObj.mmSchedDuration[1] != null &&
		    formObj.mmSchedDuration[1].checked &&
			!mmValidateSchedNumber(formObj.mmSchedCount.value)) {
			msg += document.getElementById(msgInvalidCount).innerHTML + '\n';
			if (focusObj == null) focusObj = formObj.mmSchedCount;
		}
	}
	
	if (msg != '') {
		alert(msg);
		focusObj.focus();
		focusObj.select();
		return false;
	} else {
		return true;
	}
} // mmValidateScheduler

function mmValidateSchedNumber(value)
{
	var trimVal = trim(value);
	var numRegExpr = /^\d+$/
	return (trimVal.match(numRegExpr) != null);
} // mmValidateSchedNumber

function mmShowSchedulerByList(tableID, selForm, msgSelOne)
{
	mmHideDiv('dependencyExists');
	initializeDependencyForm('MODEL');
	document.mmSchedForm.mmSchedDepdOnModelID.options.length = 0;
	
	var selObj = selForm.elements['m'];
	var selID = null;
	var modelDependencyExists = false;
	var retrySelected = false;
	
	if (mmValidateSelectOne(selObj, msgSelOne)) {
		if (selObj.length) {
			for (var i = 0; i < selObj.length; i++) {
				if (selObj[i].checked) {
					
					selID = selObj[i].value;
					document.mmSchedForm.mmModelID.value = selID;
					
					mmShowSpan("spanSelADS");
					mmShowSpan("spanSelScore");
					mmShowSpan("spanSelBoth");
					
					if (selForm.mmHasADS[i].value.length > 0) {
						if (selForm.mmHasScore[i].value.length > 0) {
							// all job type options are already available
							document.mmSchedForm.elements['mmJobType'][2].checked = true;
						} else {
							mmHideSpan("spanSelScore");
							mmHideSpan("spanSelBoth");
							document.mmSchedForm.elements['mmJobType'][0].checked = true;
						}
					} else if (selForm.mmHasScore[i].value.length > 0) {
						mmHideSpan("spanSelADS");
						mmHideSpan("spanSelBoth");
						document.mmSchedForm.elements['mmJobType'][1].checked = true;
					}
	
					var dependencyTable = document.getElementById('dependencylist'); 
					var dependencies = null;
					
					if(null != dependencyTable) {
						dependencies = dependencyTable.getElementsByTagName('tr');
					}
					
					var dependsOnType = null;
					var dependsOnApp = null;
					var existingRetryDelay = null;
					var existingRetryNum = null;
					
					
					for(var k = 1; null != dependencies && k < dependencies.length; k++) {
						var dRow = dependencies[k];
						var dCell = dRow.getElementsByTagName('td');
				
						if(selID == dCell[0].innerHTML) {
							modelDependencyExists = true;
							mmShowDiv('dependencyExists');
							dependsOnApp = dCell[1].innerHTML;
							dependsOnType = dCell[2].innerHTML;
							existingRetryDelay = dCell[3].innerHTML;
							existingRetryNum = dCell[4].innerHTML;
							
							document.mmSchedForm.mmSchedDepdOn.checked = true;
							document.mmSchedForm.mmSchedDepdOn.disabled = true;
							
							if('MODEL' == dependsOnType) {
								document.mmSchedForm.mmSchedDepdOnModel.checked = true;
								document.mmSchedForm.mmSchedDepdOnApp.checked = false;
				
								var optn = document.createElement("OPTION");
								optn.text = dependsOnApp;
						    	optn.value = dependsOnApp;
								document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
							}else {
								document.mmSchedForm.mmSchedDepdOnModel.checked = false;
								document.mmSchedForm.mmSchedDepdOnApp.checked = true;
								document.mmSchedForm.mmSchedDepdOnAppID.value = dependsOnApp;
								
								var optn = document.createElement("OPTION");
								optn.text = '';
							    optn.value = '';
								document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
							}
							
							document.mmSchedForm.mmSchedDepdOnModel.disabled = true;
							document.mmSchedForm.mmSchedDepdOnExApp.disabled = true;
							document.mmSchedForm.mmSchedDepdOnModelID.disabled = true;
							document.mmSchedForm.mmSchedDepdOnAppID.disabled = true;
							
							//By default, we store (0,0) to the database when the retry checkbox is not checked at scheduling
							if( !('0' == existingRetryDelay && '0' == existingRetryNum) ) {
								retrySelected = true;
								document.mmSchedForm.mmSchedRetry.checked = true;
								document.mmSchedForm.mmSchedRetry.disabled = true;
								document.mmSchedForm.mmSchedDelay.value = existingRetryDelay;
								document.mmSchedForm.mmSchedDelay.disabled = true;
								document.mmSchedForm.mmSchedRetryNum.value = existingRetryNum;
								document.mmSchedForm.mmSchedRetryNum.disabled = true;
							}else {
								document.mmSchedForm.mmSchedRetry.checked = false;
								document.mmSchedForm.mmSchedRetry.disabled = true;
							}
							break;
						}
					}
					
					break;
				}
			}
		} else if (selObj.checked) {
			
			selID = selObj.value;
			document.mmSchedForm.mmModelID.value = selID;
			
			mmShowSpan("spanSelADS");
			mmShowSpan("spanSelScore");
			mmShowSpan("spanSelBoth");
			
			if (selForm.mmHasADS.value.length > 0) {
				if (selForm.mmHasScore.value.length > 0) {
					// all job type options are already available
					document.mmSchedForm.elements['mmJobType'][2].checked = true;
				} else {
					mmHideSpan("spanSelScore");
					mmHideSpan("spanSelBoth");
					document.mmSchedForm.elements['mmJobType'][0].checked = true;
				}
			} else if (selForm.mmHasScore.value.length > 0) {
				mmHideSpan("spanSelADS");
				mmHideSpan("spanSelBoth");
				document.mmSchedForm.elements['mmJobType'][1].checked = true;
			}
		}
		
		// prepopulate the model list that this model can be dependent on 
		var jobListTable = document.getElementById('hiddenjoblist');
		var moList = null;

		if (null != jobListTable) {
			moList = jobListTable.getElementsByTagName('tr');
		}
		
		var dependencyTable = document.getElementById('dependencylist'); 
		var dependencies = null;
		
		if (null != dependencyTable) {
			dependencies = dependencyTable.getElementsByTagName('tr');
		}
		
		if (document.mmSchedForm.mmSchedDepdOnModelID.options.length < 1
			&& !modelDependencyExists) {
			var optn = document.createElement("OPTION");
			optn.text = '';
			optn.value = '';
			document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
		}
			
		for ( var k = 1; null != moList && k < moList.length; k++) {

			var jobModel = moList[k];
			var jobModelCell = jobModel.getElementsByTagName('td');
			var jobModelID = jobModelCell[3].innerHTML;

			if (!isNaN(jobModelID) && selID != jobModelID) {

				var options = document.mmSchedForm.mmSchedDepdOnModelID.options;
				var modelExistsInOptions = false;
				for ( var j = 0; j < options.length; j++) {
					if (jobModelID == options[j].text) {
						modelExistsInOptions = true;
					}
				}

				if (!modelExistsInOptions) {
					var modelExistsInDependencyModel = false;
					var modelExistsInDependencyApp = false;
					var tempDependencies = dependencies;
					var isDependencyLocked = false;
					
					for(var m = 1; null != dependencies && m < dependencies.length; m++) {
					 	var dRow = dependencies[m];
						var dCell = dRow.getElementsByTagName('td');
									
						if( jobModelID == dCell[0].innerHTML ) {
							
							// dependency for this model exists
							modelExistsInDependencyModel = true;
										
							var checkDModel = dCell[1].innerHTML;
							
							for( var p = 1; p < dependencies.length; p++ ) {
										
								
								for( var q = 1; q < tempDependencies.length; q++ ) {
									var dTempRow = tempDependencies[q];
									var dTempCell = dTempRow.getElementsByTagName('td');
												
									if ( checkDModel == dTempCell[0].innerHTML ) {
										checkDModel = dTempCell[1].innerHTML;
										break;
									}
												
									if( q == (tempDependencies.length-1) ) {
										isDependencyLocked = false;
									}
								}
											
								if( selID == checkDModel ) {
									isDependencyLocked = true;
									break;
								}
							}
										
						}
						
						if( jobModelID == dCell[1].innerHTML ) {
							// dependency on this model exists
							modelExistsInDependencyApp = true;
						}
					}
					
					if( null == dependencies || !modelExistsInDependencyApp && !isDependencyLocked ) {
						var optn = document.createElement("OPTION");
						optn.text = jobModelID;
					    optn.value = jobModelID;
						document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
					}
				}
			}
		}

		var today = new Date();
		var month = parseInt(today.getMonth()) + 1;
		var date = today.getDate();
		var year =  today.getFullYear();
		var dateString = '' + month + '/' + date + '/' + year;
		
		document.mmSchedForm.mmSchedDate.value = dateString;
		
		mmShowScheduleOptions();
		mmShowDiv('divOuterSchedPane');
		
		if(modelDependencyExists) {
			mmShowDiv('divDependent');
		}else{
			mmHideDiv('divDependent');
		}
		
		if(retrySelected){
			mmShowDiv('divRetryDelay');
		}else{
			mmHideDiv('divRetryDelay');
		}
		
		var shield = document.getElementById('divOuterSchedPane').lastChild;
		var scrollHeight = document.documentElement.scrollHeight;
		var docHeight = document.documentElement.clientHeight;
		while (shield != null && shield.id != 'mmShield') {
			shield = shield.previousSibling;
		}
		mmSetElementStyle(shield, 'height:' + (scrollHeight > docHeight ? scrollHeight : docHeight) + 'px');
		
		var innerPane = document.getElementById('divInnerSchedPane');
		var scrollOffset = document.documentElement.scrollTop;
		var viewPort = document.documentElement.clientHeight;
		var divHeight = innerPane.offsetHeight;
		mmSetElementStyle(innerPane, 'top:' + (scrollOffset + (viewPort / 2) - (divHeight / 2)) + 'px');
		
		document.onkeypress = mmHideSchedulerOnEsc;
	}
} // mmShowScheduler

function mmShowSchedulerById(jobList, modelID)
{
	mmHideDiv('dependencyExists');
	initializeDependencyForm('MODEL');
	document.mmSchedForm.mmSchedDepdOnModelID.options.length = 0;
	document.mmSchedForm.mmModelID.value = modelID;
	
	mmShowSpan("spanSelADS");
	mmShowSpan("spanSelScore");
	mmShowSpan("spanSelBoth");
	
	var propForm = document.propForm;
	if (propForm.mmHasADS.value.length > 0) {
		if (propForm.mmHasScore.value.length > 0) {
			// all job type options are already available
			document.mmSchedForm.elements['mmJobType'][2].checked = true;
		} else {
			mmHideSpan("spanSelScore");
			mmHideSpan("spanSelBoth");
			document.mmSchedForm.elements['mmJobType'][0].checked = true;
		}
	} else if (propForm.mmHasScore.value.length > 0) {
		mmHideSpan("spanSelADS");
		mmHideSpan("spanSelBoth");
		document.mmSchedForm.elements['mmJobType'][1].checked = true;
	}
	
	var jobListTable = document.getElementById(jobList);
	var moList = null;
					
	if( null != jobListTable ) {
		moList = jobListTable.getElementsByTagName('tr');
	}
	
	var dependencyTable = document.getElementById('dependencylist'); 
	var dependencies = null;
	
	if(null != dependencyTable) {
		dependencies = dependencyTable.getElementsByTagName('tr');
	}
	
	var dependsOnType = null;
	var dependsOnApp = null;
	var existingRetryDelay = null;
	var existingRetryNum = null;
	var modelDependencyExists = false;
	var retrySelected = false;
	
	for(var k = 1; null != dependencies && k < dependencies.length; k++) {
		var dRow = dependencies[k];
		var dCell = dRow.getElementsByTagName('td');

		if(modelID == dCell[0].innerHTML) {
			modelDependencyExists = true;
			mmShowDiv('dependencyExists');
			dependsOnApp = dCell[1].innerHTML;
			dependsOnType = dCell[2].innerHTML;
			existingRetryDelay = dCell[3].innerHTML;
			existingRetryNum = dCell[4].innerHTML;
			
			document.mmSchedForm.mmSchedDepdOn.checked = true;
			document.mmSchedForm.mmSchedDepdOn.disabled = true;
			
			if('MODEL' == dependsOnType) {
				document.mmSchedForm.mmSchedDepdOnModel.checked = true;
				document.mmSchedForm.mmSchedDepdOnApp.checked = false;

				var optn = document.createElement("OPTION");
				optn.text = dependsOnApp;
		    	optn.value = dependsOnApp;
				document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
			}else {
				document.mmSchedForm.mmSchedDepdOnModel.checked = false;
				document.mmSchedForm.mmSchedDepdOnApp.checked = true;
				document.mmSchedForm.mmSchedDepdOnAppID.value = dependsOnApp;
				
				var optn = document.createElement("OPTION");
				optn.text = '';
			    optn.value = '';
				document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
			}
			
			document.mmSchedForm.mmSchedDepdOnModel.disabled = true;
			document.mmSchedForm.mmSchedDepdOnExApp.disabled = true;
			document.mmSchedForm.mmSchedDepdOnModelID.disabled = true;
			document.mmSchedForm.mmSchedDepdOnAppID.disabled = true;
			
			//By default, we store (0,0) to the database when the retry checkbox is not checked at scheduling
			if( !('0' == existingRetryDelay && '0' == existingRetryNum) ) {
				retrySelected = true;
				document.mmSchedForm.mmSchedRetry.checked = true;
				document.mmSchedForm.mmSchedRetry.disabled = true;
				document.mmSchedForm.mmSchedDelay.value = existingRetryDelay;
				document.mmSchedForm.mmSchedDelay.disabled = true;
				document.mmSchedForm.mmSchedRetryNum.value = existingRetryNum;
				document.mmSchedForm.mmSchedRetryNum.disabled = true;
			}else {
				document.mmSchedForm.mmSchedRetry.checked = false;
				document.mmSchedForm.mmSchedRetry.disabled = true;
			}
			break;
		}
	}

	if(document.mmSchedForm.mmSchedDepdOnModelID.options.length < 1 && !modelDependencyExists){
		var optn = document.createElement("OPTION");
		optn.text = '';
	    optn.value = '';
		document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
	}
	
	
	for(var k = 1; null != moList && k < moList.length; k++){
		
		var jobModel = moList[k];
		var jobModelCell = jobModel.getElementsByTagName('td');
		var jobModelID = jobModelCell[3].innerHTML;
			
		if(!isNaN(jobModelID) && modelID != jobModelID){
			
			var options = document.mmSchedForm.mmSchedDepdOnModelID.options;
			var modelExistsInOptions = false;
			for(var j = 0; j < options.length; j++){
				if(jobModelID == options[j].text){
					modelExistsInOptions = true;
				}
			}
			
			if(!modelExistsInOptions){
				var modelExistsInDependencyModel = false;
				var modelExistsInDependencyApp = false;
				var tempDependencies = dependencies;
				var isDependencyLocked = false;
								
				for(var m = 1; null != dependencies && m < dependencies.length; m++) {
					var dRow = dependencies[m];
					var dCell = dRow.getElementsByTagName('td');
									
					if( jobModelID == dCell[0].innerHTML ) {
						// dependency for this model exists
						modelExistsInDependencyModel = true;
										
						var checkDModel = dCell[1].innerHTML;
										
						for( var p = 1; p < dependencies.length; p++ ) {
											
							for( var q = 1; q < tempDependencies.length; q++ ) {
								var dTempRow = tempDependencies[q];
								var dTempCell = dTempRow.getElementsByTagName('td');
												
								if ( checkDModel == dTempCell[0].innerHTML ) {
									checkDModel = dTempCell[1].innerHTML;
									break;
								}
												
								if( q == (tempDependencies.length-1) ) {
									isDependencyLocked = false;
								}
							}
										
							if( modelID == checkDModel ) {
								isDependencyLocked = true;
								break;
							}
						}
										
					}
									
					if( jobModelID == dCell[1].innerHTML ) {
						// dependency on this model exists
						modelExistsInDependencyApp = true;
					}
				}
				
				if( null == dependencies || !modelExistsInDependencyApp && !isDependencyLocked ) {
						var optn = document.createElement("OPTION");
						optn.text = jobModelID;
						optn.value = jobModelID;
						document.mmSchedForm.mmSchedDepdOnModelID.options.add(optn);
				}
				
			}
		}
	}
	
	var today = new Date();
	var month = parseInt(today.getMonth()) + 1;
	var date = today.getDate();
	var year =  today.getFullYear();
	var dateString = '' + month + '/' + date + '/' + year;
	
	document.mmSchedForm.mmSchedDate.value = dateString;
	
	mmShowScheduleOptions();
	mmShowDiv('divOuterSchedPane');
	
	if(modelDependencyExists) {
		mmShowDiv('divDependent');
	}
	if(retrySelected){
		mmShowDiv('divRetryDelay');
	}
	
	var shield = document.getElementById('divOuterSchedPane').lastChild;
	var scrollHeight = document.documentElement.scrollHeight;
	var docHeight = document.documentElement.clientHeight;
	while (shield != null && shield.id != 'mmShield') {
		shield = shield.previousSibling;
	}
	mmSetElementStyle(shield, 'height:' + (scrollHeight > docHeight ? scrollHeight : docHeight) + 'px');
	
	var innerPane = document.getElementById('divInnerSchedPane');
	var scrollOffset = document.documentElement.scrollTop;
	var viewPort = document.documentElement.clientHeight;
	var divHeight = innerPane.offsetHeight;
	mmSetElementStyle(innerPane, 'top:' + (scrollOffset + (viewPort / 2) - (divHeight / 2)) + 'px');
	
	document.onkeypress = mmHideSchedulerOnEsc;
} // mmShowScheduler


function mmShowEditSchedulerById(tableID, selForm, msgSelOne, msgSelOnlyOne)
{
	initializeDependencyForm('EDIT');
	document.mmSchedFormEdit.mmSchedDepdOnModelIDEdit.options.length = 0;
	var selObj = selForm.elements['mmJob'];
	// selected job id
	var selID = null;
	
	if (mmValidateSelectOnlyOne(selObj, msgSelOne, msgSelOnlyOne)) {
	
		if (selObj.length) {
			for (var i = 0; i < selObj.length; i++) {
				if (selObj[i].checked) {
					selID = selObj[i].value;
					var jobs = document.getElementById(tableID).getElementsByTagName('tr');
					var modelID = null;
					
					for(var k = 1; null != jobs && k < jobs.length; k++){
						var jobRow = jobs[k];
						var jobCells = jobRow.getElementsByTagName('td');
						
						if(selID == jobCells[1].innerHTML){
							modelID = jobCells[3].innerHTML;
							document.mmSchedFormEdit.mmModelIDEdit.value = modelID;
							break;
						}
					}

					var dependencyTable = document.getElementById('dependencylist'); 
					
					var dependencies = null;
					if(null != dependencyTable) {
						dependencies = dependencyTable.getElementsByTagName('tr');
					}
									
					var modelDependencyExists = false;
					var appType = null;
					var dependsOnApp = null;
					
					for(var k = 1; null != dependencies && k < dependencies.length; k++) {
						var dRow = dependencies[k];
						var dCell = dRow.getElementsByTagName('td');
						
						if(modelID == dCell[0].innerHTML) {
							
							modelDependencyExists = true;
							document.mmSchedFormEdit.mmSchedDepdOnEdit.checked = true;
							mmShowRecurrenceSection(document.mmSchedFormEdit.mmSchedDepdOnEdit, 'divDependentEdit');
							
							mmShowRecurrenceSection(document.mmSchedFormEdit.mmSchedRetryEdit, 'divRetryDelayEdit');
							
							if('MODEL' == dCell[2].innerHTML) {
								appType = 'MODEL';
								document.mmSchedFormEdit.mmSchedDepdOnModelEdit.checked = true;
								document.mmSchedFormEdit.mmSchedDepdOnApplicationEdit.checked = false;
								var optn = document.createElement("OPTION");
								optn.text = dCell[1].innerHTML;
			    				optn.value = dCell[1].innerHTML;
			    				dependsOnApp = optn.value;
								document.mmSchedFormEdit.mmSchedDepdOnModelIDEdit.options.add(optn);
							}else if('APP' == dCell[2].innerHTML){
								appType = 'APP';
								document.mmSchedFormEdit.mmSchedDepdOnModelEdit.checked = false;
								document.mmSchedFormEdit.mmSchedDepdOnApplicationEdit.checked = true;
								document.mmSchedFormEdit.mmSchedDepdOnAppIDEdit.value = dCell[1].innerHTML;
								dependsOnApp = document.mmSchedFormEdit.mmSchedDepdOnAppIDEdit.value;
							}else {
								alert('invalid dependency data : appType');
							}
							
							if('0' == dCell[3].innerHTML && '0' == dCell[4].innerHTML) {
								document.mmSchedFormEdit.mmSchedRetryEdit.checked = false;
							}else {
								document.mmSchedFormEdit.mmSchedRetryEdit.checked = true;
								mmShowRecurrenceSection(document.mmSchedFormEdit.mmSchedRetryEdit, 'divRetryDelayEdit');
								document.mmSchedFormEdit.mmSchedDelayEdit.value = dCell[3].innerHTML;
								document.mmSchedFormEdit.mmSchedRetryNumEdit.value = dCell[4].innerHTML;
							}
							
							break;
						}
						
					}
					
					if(!modelDependencyExists || null == dependencies) {
						document.mmSchedFormEdit.mmSchedDepdOnEdit.checked = true;
						mmShowRecurrenceSection(document.mmSchedFormEdit.mmSchedDepdOnEdit, 'divDependentEdit');
						document.mmSchedFormEdit.mmSchedRetryEdit.checked = false;
						mmShowRecurrenceSection(document.mmSchedFormEdit.mmSchedRetryEdit, 'divRetryDelayEdit');
						document.mmSchedFormEdit.mmSchedDepdOnModelEdit.checked = false;
						document.mmSchedFormEdit.mmSchedDepdOnAppEdit.checked = true;
						document.mmSchedFormEdit.mmSchedDepdOnAppIDEdit.value = '';
						document.mmSchedFormEdit.mmSchedDelayEdit.value = '';
						document.mmSchedFormEdit.mmSchedRetryNumEdit.value = '';
					}
					
					var jobListTable = document.getElementById(tableID);
					var moList = null;
					
					if( null != jobListTable ) {
						moList = jobListTable.getElementsByTagName('tr');
					}
					
					if(document.mmSchedFormEdit.mmSchedDepdOnModelIDEdit.options.length < 1){
						var optn = document.createElement("OPTION");
							optn.text = '';
						    optn.value = '';
						document.mmSchedFormEdit.mmSchedDepdOnModelIDEdit.options.add(optn);
					}

					for(var k = 1; null != moList && k < moList.length; k++){
						
						var jobModel = moList[k];
						var jobModelCell = jobModel.getElementsByTagName('td');
						var jobModelID = jobModelCell[3].innerHTML;
						
						if(!isNaN(jobModelID) && modelID != jobModelID){

							var options = document.mmSchedFormEdit.mmSchedDepdOnModelIDEdit.options;
							var modelExistsInOptions = false;
							for(var j = 0; j < options.length; j++){
								if(jobModelID == options[j].text){
									modelExistsInOptions = true;
								}
							}

							if(!modelExistsInOptions){
								var modelExistsInDependencyModel = false;
								var modelExistsInDependencyApp = false;
								var tempDependencies = dependencies;
								var isDependencyLocked = false;
								
								for(var m = 1; null != dependencies && m < dependencies.length; m++) {
									var dRow = dependencies[m];
									var dCell = dRow.getElementsByTagName('td');
									//alert('On Job Model : ' + jobModelID);
									
									if( jobModelID == dCell[0].innerHTML ) {
										// dependency for this model exists
										modelExistsInDependencyModel = true;
										var checkDModel = dCell[1].innerHTML;
										
										if( modelID == checkDModel ) {
											modelExistsInDependencyApp = true;
											break;
										}
										
										//traverse to the right in the loop
										for( var p = 1; p < dependencies.length; p++ ) {
											for( var q = 1; q < tempDependencies.length; q++ ) {
												var dTempRow = tempDependencies[q];
												var dTempCell = dTempRow.getElementsByTagName('td');
												
												if ( checkDModel == dTempCell[0].innerHTML ) {
													checkDModel = dTempCell[1].innerHTML;
													break;
												}
												
												if( q == (tempDependencies.length-1) ) {
													isDependencyLocked = false;
												}
											}
											
											if( modelID == checkDModel ) {
												isDependencyLocked = true;
												break;
											}
										}
										
										if( modelID == checkDModel ) {
											isDependencyLocked = true;
											break;
										}
										
										//traverse to the left in the loop
										checkDModel = dCell[1].innerHTML;
										for( var p = 1; p < dependencies.length; p++ ) {
											for( var q = 1; q < tempDependencies.length; q++ ) {
												var dTempRow = tempDependencies[q];
												var dTempCell = dTempRow.getElementsByTagName('td');
												
												if ( checkDModel == dTempCell[1].innerHTML ) {
													checkDModel = dTempCell[0].innerHTML;
													break;
												}
												
												if( q == (tempDependencies.length-1) ) {
													isDependencyLocked = false;
												}
											}
											
											if( modelID == checkDModel ) {
												isDependencyLocked = true;
												break;
											}
										}
										
										if( jobModelID == dCell[1].innerHTML ) {
											// dependency on this model exists
											modelExistsInDependencyApp = true;
										}
										
//										break;
									}
									
									if( jobModelID == dCell[1].innerHTML ) {
										// dependency on this model exists
										modelExistsInDependencyApp = true;
									}
								}
								
								if( null == dependencies || (!modelExistsInDependencyApp && !isDependencyLocked) ) {
										var optn = document.createElement("OPTION");
										optn.text = jobModelID;
									    optn.value = jobModelID;
										document.mmSchedFormEdit.mmSchedDepdOnModelIDEdit.options.add(optn);
								}
								
							}
						}
					}
					
					break;
				}
				
				}
			} 
		
		mmShowDiv('divOuterSchedPaneEdit');
	
		var shield = document.getElementById('divOuterSchedPaneEdit').lastChild;
	
		var scrollHeight = document.documentElement.scrollHeight;
		var docHeight = document.documentElement.clientHeight;
	
		while (shield != null && shield.id != 'mmShieldEdit') {
			shield = shield.previousSibling;
		}
	
		mmSetElementStyle(shield, 'height:' + (scrollHeight > docHeight ? scrollHeight : docHeight) + 'px');
	
		var innerPane = document.getElementById('divInnerSchedPaneEdit');
		var scrollOffset = document.documentElement.scrollTop;
		var viewPort = document.documentElement.clientHeight;
		var divHeight = innerPane.offsetHeight;
		mmSetElementStyle(innerPane, 'top:' + (scrollOffset + (viewPort / 2) - (divHeight / 2)) + 'px');
	
		document.onkeypress = mmHideSchedulerEditOnEsc;
	}

}  // mmShowEditScheduler


function mmShowRunNowById(jobList, modelID)
{
	mmShowSchedulerById(jobList, modelID);
	mmHideScheduleOptions();
} // mmShowRunNowById

function mmHideScheduler()
{
	
	mmHideDiv('divOuterSchedPane');
} // mmHideScheduler

function mmHideSchedulerEdit()
{
	
	mmHideDiv('divOuterSchedPaneEdit');
} // mmHideScheduler

function mmShowRecurrenceSection(checkObj, divObj)
{
	if (checkObj.checked) {
		document.getElementById(divObj).style.display = "block";
	} else {
		document.getElementById(divObj).style.display = "none";
	}
} // mmShowRecurrenceSection

function mmShowScheduleOptions()
{
	mmHideDiv('divSelOption');
	mmHideDiv('divSubmitRun');
	mmShowDiv('fieldSchedOption');
	mmShowDiv('divSubmitSched');
} // mmShowScheduleOptions

function mmHideScheduleOptions()
{
	mmShowDiv('divSelOption');
	mmShowDiv('divSubmitRun');
	mmHideDiv('fieldSchedOption');
	mmHideDiv('divSubmitSched');
} // mmShowScheduleOptions

function mmHideSchedulerOnEsc(e)
{
	// alert('which: ' + e.which + ' charCode: ' + e.charCode + ' keyCode: ' + e.keyCode);
	var code = (window.event ? window.event.keyCode : e.keyCode);
	if (code == 27) {
		mmHideDiv('divOuterSchedPane');
		document.onkeypress = '';
	}
} // mmHideSchedulerOnEsc

function mmHideSchedulerEditOnEsc(e)
{
	// alert('which: ' + e.which + ' charCode: ' + e.charCode + ' keyCode: ' + e.keyCode);
	var code = (window.event ? window.event.keyCode : e.keyCode);
	if (code == 27) {
		mmHideDiv('divOuterSchedPaneEdit');
		document.onkeypress = '';
	}
} // mmHideSchedulerEditOnEsc

function mmValidateAddDependency(selForm, msgDepend, msgRetry, msgExAppID, msgModelID, msgDelay, msgDelayNum, msgDelayErr, msgDelayNumErr, msgSchedApp, msgSchedMM, maxDelay, maxDelayNum) 
{
	var dependsOn = selForm.mmSchedDepdOn.checked;
	var appSelection = selForm.mmSchedDepdOnApp;
	
	var selectedButton = null;
	for(var i=0; i<appSelection.length; i++) {
		if(appSelection[i].checked == true) {
			selectedButton = appSelection[i].value
		}
	}
	
	var externalAppID = selForm.mmSchedDepdOnAppID.value;
	var modelID = selForm.mmSchedDepdOnModelID.value;
	var retry = selForm.mmSchedRetry.checked;
	var delay = selForm.mmSchedDelay.value;
	var delayNum = selForm.mmSchedRetryNum.value;
	var msg = '';
	
	if(dependsOn) {
		if( null == selectedButton ){
			return false;
		}else if( selectedButton == document.getElementById(msgSchedApp).innerHTML ){
			if( trim(externalAppID) == '' ) {
				msg = document.getElementById(msgExAppID).innerHTML;
			}
		} else if( selectedButton == document.getElementById(msgSchedMM).innerHTML ) {
			if( modelID == '' ) {
				msg = document.getElementById(msgModelID).innerHTML;
			}
		}
		if(retry == true) {
			if( trim(delay) == '' ) {
				msg += '\n' + document.getElementById(msgDelay).innerHTML; 
			}else {
				if( isNaN(delay) || parseInt(delay) <= 0 ) {
					msg += '\n ' + document.getElementById(msgDelayErr).innerHTML;
				}
			}
			
			if( trim(delayNum) == '' ) {
				msg += '\n' + document.getElementById(msgDelayNum).innerHTML;
			}else {
				if( isNaN(delayNum) || parseInt(delayNum) <= 0 ) {
					msg += '\n ' + document.getElementById(msgDelayNumErr).innerHTML;
				}
			}
		}
	}
	
	if( msg != '' ) {
		alert(msg);
		return false;
	}else {
		return true;
	}
}

function mmValidateUpdateDependency(selForm, msgDepend, msgRetry, msgExAppID, msgModelID, msgDelay, msgDelayNum, msgDelayErr, msgDelayNumErr, msgSchedApp, msgSchedMM, maxDelay, maxDelayNum) 
{
	var dependsOn = selForm.mmSchedDepdOnEdit.checked;
	var appSelection = selForm.mmSchedDepdOnAppEdit;
	
	var selectedButton = null;
	for(var i=0; i<appSelection.length; i++) {
		if(appSelection[i].checked == true) {
			selectedButton = appSelection[i].value
		}
	}
	
	var externalAppID = selForm.mmSchedDepdOnAppIDEdit.value;
	var model = selForm.mmSchedDepdOnModelEdit.value;
	var modelID = selForm.mmSchedDepdOnModelIDEdit.value;
	var retry = selForm.mmSchedRetryEdit.checked;
	var delay = selForm.mmSchedDelayEdit.value;
	var delayNum = selForm.mmSchedRetryNumEdit.value;
	var msg = '';
	
	if(dependsOn) {
		if( null == selectedButton ){
			return false;
		}else if( selectedButton == document.getElementById(msgSchedApp).innerHTML ){
			if( trim(externalAppID) == '' ) {
				msg = document.getElementById(msgExAppID).innerHTML;
			}
		} else if( selectedButton == document.getElementById(msgSchedMM).innerHTML ) {
			if( modelID == '' ) {
				msg = document.getElementById(msgModelID).innerHTML;
			}
		}
		
		if(retry == true) {
			if( trim(delay) == '' ) {
				msg += '\n' + document.getElementById(msgDelay).innerHTML; 
			}else {
				if( isNaN(delay) || parseInt(delay) <= 0 ) {
					msg += '\n ' + document.getElementById(msgDelayErr).innerHTML;
				}
			}
			
			if( trim(delayNum) == '' ) {
				msg += '\n' + document.getElementById(msgDelayNum).innerHTML;
			}else {
				if( isNaN(delayNum) || parseInt(delayNum) <= 0 ) {
					msg += '\n ' + document.getElementById(msgDelayNumErr).innerHTML;
				}
			}
		}
	}
	
	if( msg != '' ) {
		alert(msg);
		return false;
	}else {
		return true;
	}
}

function initializeDependencyForm(form) 
{	
	if('EDIT' != form) {
		document.mmSchedForm.mmSchedDepdOn.checked = false;
		mmShowRecurrenceSection(document.mmSchedForm.mmSchedDepdOn, 'divDependent');
		document.mmSchedForm.mmSchedRetry.checked = false;
		mmShowRecurrenceSection(document.mmSchedForm.mmSchedRetry, 'divRetryDelay');
		document.mmSchedForm.mmSchedDepdOnModel.checked = false;
		document.mmSchedForm.mmSchedDepdOnExApp.checked = true;
		document.mmSchedForm.mmSchedDepdOnAppID.value = '';
		document.mmSchedForm.mmSchedDelay.value = '';
		document.mmSchedForm.mmSchedRetryNum.value = '';
		
		document.mmSchedForm.mmSchedDepdOn.disabled = false;
		
		document.mmSchedForm.mmSchedDepdOnModel.disabled = false;
		document.mmSchedForm.mmSchedDepdOnExApp.disabled = false;
		document.mmSchedForm.mmSchedDepdOnModelID.disabled = false;
		document.mmSchedForm.mmSchedDepdOnAppID.disabled = false;
		document.mmSchedForm.mmSchedRetry.disabled = false;
		document.mmSchedForm.mmSchedDelay.disabled = false;
		document.mmSchedForm.mmSchedRetryNum.disabled = false;
		
	} else {
		document.mmSchedFormEdit.mmSchedDepdOnEdit.checked = false;
		mmShowRecurrenceSection(document.mmSchedFormEdit.mmSchedDepdOnEdit, 'divDependentEdit');
		document.mmSchedFormEdit.mmSchedRetryEdit.checked = false;
		mmShowRecurrenceSection(document.mmSchedFormEdit.mmSchedRetryEdit, 'divRetryDelayEdit');
		document.mmSchedFormEdit.mmSchedDepdOnModelEdit.checked = false;
		document.mmSchedFormEdit.mmSchedDepdOnAppEdit.checked = true;
		document.mmSchedFormEdit.mmSchedDepdOnAppIDEdit.value = '';
		document.mmSchedFormEdit.mmSchedDelayEdit.value = '';
		document.mmSchedFormEdit.mmSchedRetryNumEdit.value = '';
		
		document.mmSchedFormEdit.mmSchedDepdOnEdit.disabled = false;
		
		document.mmSchedFormEdit.mmSchedDepdOnModelEdit.disabled = false;
		document.mmSchedFormEdit.mmSchedDepdOnAppEdit.disabled = false;
		document.mmSchedFormEdit.mmSchedDepdOnModelIDEdit.disabled = false;
		document.mmSchedFormEdit.mmSchedDepdOnAppIDEdit.disabled = false;
		
		document.mmSchedFormEdit.mmSchedRetryEdit.disabled = false;
		document.mmSchedFormEdit.mmSchedDelayEdit.disabled = false;
		document.mmSchedFormEdit.mmSchedRetryNumEdit.disabled = false;
	}
}
