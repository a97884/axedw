
function mmSubmitModelList(formObj, ctx, msgSelOne, msgDelConfirm)
{
	if (formObj.elements['a'].value == 'Refresh') {
		formObj.action = ctx + '/modellist.do';		
		return true;
	}	
	if (formObj.elements['a'].value != 'Delete') {
		formObj.action = ctx + '/modelproperties.do';
		return mmValidateSelectOne(formObj.elements['m'], msgSelOne);
	} else {
		return mmConfirmDelete(formObj.elements['m'], msgSelOne, msgDelConfirm);
	}
} // mmSubmitModelList
