// NOTE to use this JavaScript, the caller JSP/HTML must
//      define the following regular expressions:
//
//      dtRegExpr - date format regular expression (locale specific)
//      tmRegExpr - time format regular expression (locale specific)
//      tsRegExpr - timestamp format regular expression (locale specific)
//      numRegExpr - number format regular expression (locale specific)
//
//      e.g.
//
//      var dtRegExpr = /^\d\d\d\d-[\d]?\d-[\d]?\d$/

function mmValidateModelProperties(formObj, msgPrefix)
{
	if (formObj.a.value != 'Save') {
		return true;
	}
	
	var focusObj = null;
	var msg = '';
	
	if (!mmValidateDate(formObj.mmModelExpire.value)) {
		msg += document.getElementById(msgPrefix + 'mmModelExpire').innerHTML;
		msg += '\n';
		mmSetElementClass(formObj.mmModelExpire, 'mmInputSErr');
		if (focusObj == null) focusObj = formObj.mmModelExpire;
	} else {
		mmSetElementClass(formObj.mmModelExpire, 'mmInputS');
	}
	
	if (formObj.mmTargetDate != null && !mmValidateDate(formObj.mmTargetDate.value)) {
		msg += document.getElementById(msgPrefix + 'mmTargetDate').innerHTML;
		msg += '\n';
		mmSetElementClass(formObj.mmTargetDate, 'mmInputSErr');
		if (focusObj == null) focusObj = formObj.mmTargetDate;
	} else {
		mmSetElementClass(formObj.mmTargetDate, 'mmInputS');
	}
	
	var paramType = formObj.mmParamType;
	var typeDate = document.getElementById('t_date').innerHTML;
	var typeTime = document.getElementById('t_time').innerHTML;
	var typeTS = document.getElementById('t_timestamp').innerHTML;
	var typeNum = document.getElementById('t_number').innerHTML;
	if (paramType != null) {
		if (paramType[0] != null) { // multiple parameters
			for (var i = 0; i < paramType.length; i++) {
				var seq = formObj.mmParamSeq[i].value;
				var wellFormed;
				switch (paramType[i].value) {
				case typeDate:
					wellFormed = mmValidateDate(formObj.elements['mmParam' + seq].value);
					break;
				case typeTime:
					wellFormed = mmValidateTime(formObj.elements['mmParam' + seq].value);
					break;
				case typeTS:
					wellFormed = mmValidateTimestamp(formObj.elements['mmParam' + seq].value);
					break;
				case typeNum:
					wellFormed = mmValidateNumber(formObj.elements['mmParam' + seq].value);
					break;
				default:
					wellFormed = (trim(formObj.elements['mmParam' + seq].value) != '');
				}
				if (!wellFormed) {
					msg += document.getElementById(msgPrefix + 'mmParam' + seq).innerHTML;
					msg += '\n';
					mmSetElementClass(formObj.elements['mmParam' + seq], 'mmInputSErr');
					if (focusObj == null) focusObj = formObj.elements['mmParam' + seq];
				} else {
					mmSetElementClass(formObj.elements['mmParam' + seq], 'mmInputS');
				}
			}
		} else { // single parameter
			var seq = formObj.mmParamSeq.value;
			var wellFormed;
			switch (paramType[i].value) {
			case typeDate:
				wellFormed = mmValidateDate(formObj.elements['mmParam' + seq].value);
				break;
			case typeTime:
				wellFormed = mmValidateTime(formObj.elements['mmParam' + seq].value);
				break;
			case typeTS:
				wellFormed = mmValidateTimestamp(formObj.elements['mmParam' + seq].value);
				break;
			case typeNum:
				wellFormed = mmValidateNumber(formObj.elements['mmParam' + seq].value);
				break;
			default:
				wellFormed = (trim(formObj.elements['mmParam' + seq].value) != '');
			}
			if (!wellFormed) {
				msg += document.getElementById(msgPrefix + 'mmParam' + seq).innerHTML;
				msg += '\n';
				mmSetElementClass(formObj.elements['mmParam' + seq], 'mmInputSErr');
				if (focusObj == null) focusObj = formObj.elements['mmParam' + seq];
			} else {
				mmSetElementClass(formObj.elements['mmParam' + seq], 'mmInputS');
			}
		}
	}
	
	if (msg != '') {
		alert(msg);
		focusObj.focus();
		focusObj.select();
		return false;
	} else {
		return true;
	}
} // mmValidateModelProperties

function mmEditModelProperties()
{
	document.propForm.a.value='Edit';
	document.propForm.submit();
} // mmEditModelProperties

// for popup help dialog *******************************************************
var currPopup = null;
var currPopupId = null;
var currPopupTimer = 0;

function showPopup(idx)
{
	hidePopup(true);
	currPopupId = idx;
	currPopup = document.getElementById(idx);
	mmSetElementStyle(currPopup, 'display:inline;filter:alpha(opacity=100);opacity:1');
	currPopupTimer = 10; // 10 seconds before popup automatically disappears
} // showPopup

function hidePopup(noFade)
{
	if (currPopup != null) {
		if (noFade) {
			currPopup.style.display = 'none';
		} else {
			mmFade(currPopupId);
		}
		currPopup = null;
	}
} // hidePopup

function hidePopupTimer()
{
	if (currPopup != null) {
		if (currPopupTimer > 0) {
			currPopupTimer--;
		} else {
			hidePopup();
		}
	}
} // hidePopupTimer

setInterval('hidePopupTimer()', 1000);

// for selecting anchor table **************************************************
function mmShowAnchorSelection()
{
	//alert('page y offset: ' + window.pageYOffset);
	//alert('scroll top: ' + document.documentElement.scrollTop);
	//alert('viewable height: ' + document.documentElement.clientHeight);
	//alert('scroll height: ' + document.documentElement.scrollHeight);
	//alert('document client height: ' + document.body.clientHeight);
	
	mmShowDiv('divOuterPane');
	
	// bootstrap to populate anchor DB list initially
	if (document.anchorForm.mmAnchorDB.options.length == 0) {
		mmRequestAnchorList(null);
	}
	
	// adjust dimension and position
	var shield = document.getElementById('divOuterPane').lastChild;
	var scrollHeight = document.documentElement.scrollHeight;
	var docHeight = document.documentElement.clientHeight;
	while (shield != null && shield.id != 'mmShield') {
		shield = shield.previousSibling;
	}
	mmSetElementStyle(shield, 'height:' + (scrollHeight > docHeight ? scrollHeight : docHeight) + 'px');
	
	mmSetAnchorBoxPosition();
	
	document.onkeypress = mmHideAnchorSelectionOnEsc;
} // mmShowAnchorSelection

function mmHideAnchorSelection()
{
	mmHideDiv('divOuterPane');
} // mmHideAnchorSelection

function mmHideAnchorSelectionOnEsc(e)
{
	// alert('which: ' + e.which + ' charCode: ' + e.charCode + ' keyCode: ' + e.keyCode);
	var code = (window.event ? window.event.keyCode : e.keyCode);
	if (code == 27) {
		mmHideDiv('divOuterPane');
		document.onkeypress = '';
	}
} // mmHideAnchorSelectionOnEsc

function mmSelectAnchor()
{
	var srcFormObj = document.anchorForm;
	var dstFormObj = document.propForm;
	
	dstFormObj.mmAnchorDB.value = srcFormObj.mmAnchorDB.options[srcFormObj.mmAnchorDB.selectedIndex].value;
	dstFormObj.mmAnchorTable.value = srcFormObj.mmAnchorTable.options[srcFormObj.mmAnchorTable.selectedIndex].value;
	
	mmHideAnchorSelection();
} // mmSelectAnchor

var request;

function mmRequestAnchorList(db)
{
	if (db == null) {
		document.anchorForm.mmAnchorDB.options.length = 0; // clear out existing list
		mmShowLoading('tdAnchorDB');
	} else {
		document.anchorForm.mmAnchorTable.options.length = 0; // clear out existing list
		mmShowLoading('tdAnchorTable');
	}
	request = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	request.open('POST', 'selectAnchor.do', true);
	request.onreadystatechange = mmHandlePublishDB;
	request.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
	request.send(db);
} // mmRequestAnchorList

function mmHandlePublishDB()
{
	var formObj = document.anchorForm;
	var selectObj = null;
	var currValue = null;
	var generateTable = false;
	
	if (request.readyState == 4 && request.status == 200) {
		items = request.responseText.split('\n');
		for (var i = 0; i < items.length; i++) {
			items[i] = trim(items[i]);
			if (selectObj == null) {
				if (items[i] == 'database') {
					selectObj = formObj.mmAnchorDB;
					selectObj.options.length = 0;
					currValue = trim(document.propForm.mmAnchorDB.value);
					generateTable = true;
					mmHideLoading('tdAnchorDB');
				} else if (items[i] == 'table') {
					selectObj = formObj.mmAnchorTable;
					selectObj.options.length = 0;
					currValue = trim(document.propForm.mmAnchorTable.value);
					mmHideLoading('tdAnchorTable');
				}
			} else if (items[i] != '') {
				selectObj.options[selectObj.options.length] = new Option(items[i], items[i], false, (currValue.toLowerCase() == items[i].toLowerCase()));
			}
		}
		
		// generate anchor table list if anchor db list just got populated
		if (generateTable && currValue != null) {
			mmRequestAnchorList(currValue);
		}
		
		mmSetAnchorBoxPosition();
		//alert('received response from server' + items.length);
	}
} // mmHandlePublishDB

function mmShowLoading(objId)
{
	var loadingObj = document.getElementById(objId).lastChild;
	while (loadingObj != null && loadingObj.id != 'mmLoading') {
		loadingObj = loadingObj.previousSibling;
	}
	mmSetElementStyle(loadingObj, 'display:inline');
} // mmShowLoading

function mmHideLoading(objId)
{
	var loadingObj = document.getElementById(objId).lastChild;
	while (loadingObj != null && loadingObj.id != 'mmLoading') {
		loadingObj = loadingObj.previousSibling;
	}
	mmSetElementStyle(loadingObj, 'display:none');
} // mmHideLoading

function mmSetAnchorBoxPosition()
{
	// this function is needed for Firefox to properly resize the inner pane DIV
	var innerPane = document.getElementById('divInnerPane');
	var tableObj = document.anchorForm.firstChild;
	while (tableObj != null && tableObj.id != 'mmFormTable') {
		tableObj = tableObj.nextSibling;
	}
	//alert('form width: ' + document.anchorForm.offsetWidth + '\ntable width: ' + tableObj.offsetWidth);
	
	var scrollOffset = document.documentElement.scrollTop;
	var viewPort = document.documentElement.clientHeight;
	var divHeight = innerPane.offsetHeight;
	mmSetElementStyle(innerPane, 'top:' + (scrollOffset + (viewPort / 2) - (divHeight / 2)) + 'px'
							   + (tableObj != null ? ';width:' + tableObj.offsetWidth + 'px' : ''));
} // mmSetAnchorBoxPosition
// *****************************************************************************
