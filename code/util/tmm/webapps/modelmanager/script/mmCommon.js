// NOTE to use the validate functions in this JavaScript, the caller JSP/HTML must
//      define the following regular expressions:
//
//      dtRegExpr - date format regular expression (locale specific)
//      tmRegExpr - time format regular expression (locale specific)
//      tsRegExpr - timestamp format regular expression (locale specific)
//      numRegExpr - number format regular expression (locale specific)
//
//      e.g.
//
//      var dtRegExpr = /^\d\d\d\d-[\d]?\d-[\d]?\d$/

function mmValidateSelectOne(listObj, msgSelOne)
{
	var selectOne = false;
	
	if (listObj[0]) {
		for (i = 0; i < listObj.length; i++) {
			if (listObj[i].checked) {
				selectOne = true;
				break;
			}
		}
	} else if (listObj) {
		selectOne = listObj.checked;
	} else {
		selectOne = true;
	}
	
	if (!selectOne) {
		var msg = document.getElementById(msgSelOne).innerHTML;
		alert(msg);
	}
	
	return selectOne;
} // mmValidateSelectOne

function mmValidateSelectOnlyOne(listObj, msgSelOne, msgSelOnlyOne)
{
	var selectOne = false;
	var selectOnlyOne = false;
	var selectedList = new Array();
	var selectionCount = 0;
	
	if (listObj[0]) {
		for (i = 0; i < listObj.length; i++) {
			if (listObj[i].checked) {
				selectOne = true;
				selectionCount = selectionCount + 1;
			}
		}
	} else if (listObj) {
		selectOne = listObj.checked;
	} else {
		selectOne = true;
	}
	
	if (!selectOne) {
		var msg = document.getElementById(msgSelOne).innerHTML;
		alert(msg);
		return selectOne;
	}
	
	if (selectionCount != 1) {
		var msg = document.getElementById(msgSelOnlyOne).innerHTML;
		alert(msg);
		return false;
	}else {
		return true;
	}
	
} // mmValidateSelectOne

function mmConfirmDelete(listObj, msgSelOne, msgDelConfirm)
{
	if (mmValidateSelectOne(listObj, msgSelOne)) {
		var msg = document.getElementById(msgDelConfirm).innerHTML;
		return confirm(msg);
	} else {
		return false;
	}
} // mmConfirmDelete

function mmToggleCheckbox(masterCheckbox, checkboxList)
{
	var state = masterCheckbox.checked;
	
	if (checkboxList[0] != null) {
		for (var i = 0; i < checkboxList.length; i++) {
			checkboxList[i].checked = state;
		}
	} else {
		checkboxList.checked = state;
	}
} // mmToggleCheckbox

function mmShowDiv(name)
{
	document.getElementById(name).style.display = "block";
} // mmShowDiv

function mmHideDiv(name)
{
	document.getElementById(name).style.display = "none";
} // mmHideDiv

function mmShowSpan(name)
{
	document.getElementById(name).style.display = "inline";
} // mmShowDiv

function mmHideSpan(name)
{
	document.getElementById(name).style.display = "none";
} // mmHideDiv

// timer is the number of seconds before hiding the element with specified ID
function mmHideMe(id, timer)
{
    if (timer > 0) {
	    setTimeout('mmHideMe("' + id + '", 0)', (timer * 1000));
	} else {
	    mmFade(id);
	}
} // mmHideMe

var mmFadeItems = new Array();

function mmFade(id, opacity)
{
	if (opacity == null) {
		if (mmFadeItems[id] != null) {
			return; // this item is in the process of fading
		} else {
			mmFadeItems[id] = document.getElementById(id);
		}
		opacity = 100;
	}
	var obj = mmFadeItems[id];
	if (opacity > 0) {
		var displayStr = obj.style.display;
		mmSetElementStyle(obj, 'filter:alpha(opacity=' + opacity + ');opacity:' + (opacity / 100.0));
		if (displayStr != null) {
			obj.style.display = displayStr;
		}
		setTimeout('mmFade("' + id + '", ' + (opacity - 10) + ')', 50);
	} else {
		obj.style.display = 'none';
		mmFadeItems[id] = null;
	}
} // mmFade

// NOTE this implementation is taken from
//      http://www.elated.com/articles/javascript-and-cookies/
function mmGetCookie(name)
{
	var results = document.cookie.match('(^|;)?' + name + '=([^;]*)(;|$)');
	
	if (results)
		return unescape(results[2]);
	else
		return null;
} // mmGetCookie

// NOTE this implementation is taken from
//      http://www.elated.com/articles/javascript-and-cookies/
function mmSetCookie(name, value, daysToExpire, path, domain, secure)
{
	var cookie_string = name + "=" + escape(value);
	
	if (daysToExpire) {
		var expires = new Date();
		expires.setDate(expires.getDate() + daysToExpire);
		cookie_string += "; expires=" + expires.toGMTString();
	}
	if (path) cookie_string += "; path=" + escape(path);
	if (domain) cookie_string += "; domain=" + escape(domain);
	if (secure) cookie_string += "; secure";
	document.cookie = cookie_string;
} // mmSetCookie

// NOTE this implementation is taken from
//      http://www.elated.com/articles/javascript-and-cookies/
function deleteCookie(name)
{
	var cookie_date = new Date();  // current date & time
	cookie_date.setTime(cookie_date.getTime() - 1);
	document.cookie = name += "=; expires=" + cookie_date.toGMTString();
} // deleteCookie

function trim(str)
{
    return str.replace(/^\s*(\S.*\S|\S|)\s*$/g, "$1");
} // trim

function mmSetElementClass(obj, classname)
{
	if (obj != null) {
		obj.setAttribute('class', classname);
		obj.setAttribute('className', classname);
	}
} // mmSetElementClass

function mmSetElementStyle(obj, styleStr)
{
	if (obj != null) {
		if (typeof(obj.style.cssText) == 'string') {
			obj.style.cssText = styleStr;
		}
		obj.setAttribute('style', styleStr);
	}
} // mmSetElementStyle

function mmSetTHAnchorStyle(obj)
{
	if (obj.nodeName != null &&
		obj.nodeName.toLowerCase() == 'a') {
		mmSetElementClass(obj, 'mmTH');
	} else {
		var childObj = obj.firstChild;
		while (childObj != null) {
			mmSetTHAnchorStyle(childObj); // depth first search
			childObj = childObj.nextSibling;
		}
	}
} // mmSetTHAnchorStyle

function mmValidateDate(value)
{
	return (trim(value).match(dtRegExpr) != null);
} // mmValidateDate

function mmValidateTime(value)
{
	return (trim(value).match(tmRegExpr) != null);
} // mmValidateTime

function mmValidateTimestamp(value)
{
	return (trim(value).match(tsRegExpr) != null);
} // mmValidateTimestamp

function mmValidateNumber(value)
{
	return (trim(value).match(numRegExpr) != null);
} // mmValidateNumber

function mmValidateEmail(value)
{
	var emailRegExpr = /^("[^"@]+")?\s*[\w!#$%*/?|\^{}`~&'+\-=_]([.]?([\w!#$%*/?|\^{}`~&'+\-=_][.]?)*[\w!#$%*/?|\^{}`~&'+\-=_])?@[\w-]+[.][\w]+([.][\w]+)*$/
	return (trim(value).match(emailRegExpr) != null);
} // mmValidateEmail
