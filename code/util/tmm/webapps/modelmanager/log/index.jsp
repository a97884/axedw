<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="java.util.logging.Level" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMRole" %>
<%@ page import="com.teradata.modelmanager.util.DisplayTagUtil" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="disp" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final String LIST_TABLE_ID = "mmLogTable";
%>
<%
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = Messages.LOG_TITLE;
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [][] {
	        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
	};
	int linkIdx;
	
	DisplayTagUtil tagUtil = new DisplayTagUtil(request, LIST_TABLE_ID);
	
	Level currLevel = Logger.getLogger("com.teradata.modelmanager").getLevel();
	
	if (!request.isUserInRole(MMRole.ADMIN)) {
	    request.getRequestDispatcher("/").forward(request, response);
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		  "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		select { vertical-align:middle; }
	</style>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="../jsp/common/titleBar.snippet" %>
	
	<p/>
	
	<div style="text-align:center">
	
	<form method="POST"
		  action="<%= mmCtx %>/logs.do">
		<%= Messages.LOG_CURRENT_LEVEL %>
		<select name="mmLogLevel" onchange="this.form.submit()">
		<option value="<%= Level.OFF.getName() %>"<%= currLevel.equals(Level.OFF) ? " selected" : "" %>><%= Level.OFF.getName() %></option>
		<option value="<%= Level.SEVERE.getName() %>"<%= currLevel.equals(Level.SEVERE) ? " selected" : "" %>><%= Level.SEVERE.getName() %></option>
		<option value="<%= Level.WARNING.getName() %>"<%= currLevel.equals(Level.WARNING) ? " selected" : "" %>><%= Level.WARNING.getName() %></option>
		<option value="<%= Level.INFO.getName() %>"<%= currLevel.equals(Level.INFO) ? " selected" : "" %>><%= Level.INFO.getName() %></option>
		<option value="<%= Level.CONFIG.getName() %>"<%= currLevel.equals(Level.CONFIG) ? " selected" : "" %>><%= Level.CONFIG.getName() %></option>
		<option value="<%= Level.FINE.getName() %>"<%= currLevel.equals(Level.FINE) ? " selected" : "" %>><%= Level.FINE.getName() %></option>
		<option value="<%= Level.FINER.getName() %>"<%= currLevel.equals(Level.FINER) ? " selected" : "" %>><%= Level.FINER.getName() %></option>
		<option value="<%= Level.FINEST.getName() %>"<%= currLevel.equals(Level.FINEST) ? " selected" : "" %>><%= Level.FINEST.getName() %></option>
		<option value="<%= Level.ALL.getName() %>"<%= currLevel.equals(Level.ALL) ? " selected" : "" %>><%= Level.ALL.getName() %></option>
		</select>
		<input type="hidden" name="a" value="SetLevel"/>
	</form>
	
	<p/>
		  
	<disp:table name="mylist" id="<%= LIST_TABLE_ID %>" class="listrender" pagesize="15" sort="list" defaultsort="2" defaultorder="descending" requestURI="/logs.do">
		<disp:column property="name" sortable="true" title="<%= tagUtil.getFirstColumnTitle(Messages.LOG_NAME) %>" style="width:200px;text-align:center" decorator="com.teradata.modelmanager.display.LogDecorator"></disp:column>
		<disp:column property="lastmodified" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.LOG_LAST_MODIFIED) %>" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
		<disp:column property="length" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.LOG_SIZE) %>" style="width:100px;text-align:center"></disp:column>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	if (document.getElementById('<%= LIST_TABLE_ID %>')) {
		mmSetTHAnchorStyle(document.getElementById('<%= LIST_TABLE_ID %>').getElementsByTagName('thead')[0]);
	}
	</script>
	
	</div>
	
</body>
</html>