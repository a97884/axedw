﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = Messages.LOGIN_TITLE;
	boolean failed = (request.getParameter("fail") != null);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		body { text-align:center;font-size:8pt; }
		div#mmLoginPrompt { width:250px; }
		div.headh { color:#FFF; padding-bottom:2px; }
		input#mmLoginName,
		input#mmLoginPass { width:150px; }
		input#mmSubmit { font-size:8pt; }
		table#mmLoginTable { font-size:8pt; margin:auto;}
		td.mmLoginLabel { text-align:right; color:#888; }
		div#mmTitle { font-size:14pt; }
		table.mmImgBorder { border-style:solid;border-width:1px;border-color:#AAA; background-color:white; }
	</style>
	<title><%= pageTitle %></title>
</head>
<body onload="document.mmLogin.j_username.focus()">
	<br/>
	<br/>
	<br/>
	<div id="mmTitle"><%= appTitle %></div>
	<br/>
	<br/>
	<%= Messages.APP_VERSION %>.<%= Messages.APP_BUILD %>
	<br/>
	<br/>
	<br/>
<%
	if (failed) {
%>
	<div style="color:red;font-weight:bold"><%= Messages.LOGIN_MSG_UNSUCCESSFUL %></div>
	<br/>
<%
	} else {
%>
	<br/>
	<br/>
<%	} %>
	<form name="mmLogin"
		  method="POST"
		  action="j_security_check">
	<div id="mmLoginPrompt">
	<!-- rounded corner wrapper ============================================ -->
	<b class="b1h"></b><b class="b2h"></b><b class="b3h"></b><b class="b4h"></b>
	<div class="headh"><%= pageTitle %></div>
	
	<div class="contenth">
		<table id="mmLoginTable" border=0 cellspacing=5 cellpadding=0>
		<tr>
			<td class="mmLoginLabel"><%= Messages.LOGIN_NAME %></td>
			<td><input id="mmLoginName" type="text" name="j_username" onfocus="this.select()"></td>
		</tr>
		<tr>
			<td class="mmLoginLabel"><%= Messages.LOGIN_PASSWD %></td>
			<td><input id="mmLoginPass" type="password" name="j_password" onfocus="this.select()"></td>
		</tr>
		<tr>
			<td colspan="2"><input id="mmSubmit" type="submit" value="<%= Messages.LOGIN_LOG_IN %>"/></td>
		</tr>
		</table>
	</div>
	<b class="b4bh"></b><b class="b3bh"></b><b class="b2bh"></b><b class="b1h"></b>
	<!-- end of rounded corner wrapper ===================================== -->
	</div>
	</form>
	
	<div style="position:absolute;bottom:25px;left:0px;width:100%;text-align:center">
		<table border=0 cellspacing=5 cellpadding=0 class="mmImgBorder">
		<tr><td>
			<img src="<%= mmCtx %>/image/Teradata.gif"/>
		</td></tr>
		</table>
		<br/>
		Copyright &copy;2006-2011 Teradata Corporation
	</div>
</body>
</html>