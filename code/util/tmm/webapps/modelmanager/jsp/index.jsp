<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="com.teradata.modelmanager.common.AppUtil" %>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMRole" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = Messages.MAIN_TITLE;
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [0][0];
	int linkIdx;
	
	String publishDB = (String)session.getAttribute(MMAttr.PUBLISH_DB);
	if (publishDB == null && request.getAttribute(MMAttr.MSG) == null) request.setAttribute(MMAttr.MSG, Messages.SELECTDB_MSG_MISSING);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
            "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		div#mmMainMenu { text-align:center; position:relative; margin-top:50px; width:275px; }
		div.headh { color:#FFF; padding-bottom:2px; }
		ul { text-align:left; color:#AAA; margin-top:1em; margin-bottom:0; }
		li { list-style:square; padding-top:0.5em; padding-bottom:0.5em; font-size:12pt; }
		div#divOuterPane { position:absolute;top:0;left:0;width:100%;height:768px;z-index:2; }
		div#divInnerPane { width:310px;position:relative;top:200px;left:0;z-index:2;border-color:#AAA;border-width:2px;border-style:solid;font-size:8pt;text-align:left;padding:5px;background-color:white; }
		table#mmSelDB { font-size:8pt; }
	</style>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<script type="text/javascript" language="JavaScript">
		function getPublishDB()
		{
			if (document.dbForm == null) {
				return;
			}
			
			var publishDB = document.dbForm.mmPublishDB;
			var currDB = "<%= HTMLUtil.escapeHTML(publishDB) %>";
			
			if (currDB) {
				publishDB.value = currDB;
			} else {
				currDB = mmGetCookie("mmPublishDB");
				if (currDB != null) {
					publishDB.value = currDB;
				}
			}
			//publishDB.focus();
			//publishDB.select();
		} // getPublishDB
		
		function setPublishDB()
		{
			var publishDB = document.dbForm.mmPublishDB;
			var currDB = publishDB.value;
			mmSetCookie('mmPublishDB', currDB, 7, '/');
			
			document.getElementById('mmBtnPublishDB').src = '<%= mmCtx %>/image/button_submit_1.gif';
		} // setPublishDB
	</script>
	<title><%= appTitle %> - <%= pageTitle %></title>
</head>
<body onload="getPublishDB();">
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<div id="mmMainMenu">
	
	<!-- rounded corner wrapper ============================================ -->
	<b class="b1h"></b><b class="b2h"></b><b class="b3h"></b><b class="b4h"></b>
	<div class="headh"><%= Messages.MAIN_TITLE %></div>
	
	<div class="contenth">
	<br/>
	<!-- published database -->
	<table id="mmSelDB" border=0 cellspacing=0 cellpadding=0>
	<tr><td style="text-align:left">
		<%= Messages.MAIN_SEL_PUB_DB %>:
		<a href="<%= mmCtx %>/main.do?mmPublishDB="><%= publishDB %></a>
	</td></tr>
	</table>
	
	<!-- menu list -->
	<table border=0 cellspacing=0 cellpadding=0>
	<tr><td>
	
	<ul>
	<li><a href="<%= mmCtx %>/modellist.do"><%= Messages.MAIN_MODEL_LIST %></a></li>
	<li><a href="<%= mmCtx %>/joblist.do"><%= Messages.MAIN_SCHEDULED_JOBS %></a></li>
<%	if (AppUtil.useAppLoginModule()) { %>
	<li><a href="javascript:mmShowUserSettings()"><%= Messages.MAIN_USER_MANAGEMENT %></a></li>
<%	} %>
<%	if (request.isUserInRole(MMRole.ADMIN)) { %>
	<li><a href="<%= mmCtx %>/logs.do"><%= Messages.MAIN_APP_LOGS %></a></li>
<%	} %>
	</ul>
	
	</td></tr>
	</table>
	<br/>
	</div>
	<b class="b4bh"></b><b class="b3bh"></b><b class="b2bh"></b><b class="b1h"></b>
	<!-- end of rounded corner wrapper ===================================== -->
	
	</div>

<%	if (publishDB == null || publishDB.trim().length() == 0 ||
        request.getAttribute(MMAttr.PUBLISH_DB) != null) { %>
	<%@include file="popup/selectPublishDB.snippet" %>
<%	} %>
<%	if (AppUtil.useAppLoginModule()) { %>
	<%@include file="popup/userSettings.snippet" %>
<%	} %>
</body>
</html>