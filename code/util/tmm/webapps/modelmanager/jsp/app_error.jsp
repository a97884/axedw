<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.logging.Level" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="org.apache.struts.Globals" %>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final Logger logger = Logger.getLogger("com.teradata.modelmanager.jsp.app_error");
%>
<%
	Throwable e = (Throwable)request.getAttribute(MMAttr.EXCEPTION); // application exception
	if (e == null) {
	    e = (Throwable)request.getAttribute(Globals.EXCEPTION_KEY); // STRUTS exception forwarding
		if (e == null) {
			e = exception; // JSP exception
		}
	}
	if (e != null) {
	    logger.log(Level.SEVERE, "application error", e);
	}
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = Messages.ERROR_TITLE;
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [][] {
	        {Messages.MAIN_TITLE, request.getContextPath() + "/main.do"},
	};
	int linkIdx;
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		body { text-align:center; font-size:10pt }
		pre { text-align:left; }
		h1#mmErrLabel { color:red; }
		div#errMsg { color:red; width:600px; text-align:left; border-width:2px; border-color:red; border-style:solid; padding:5px; }
		div#mmException { background-color:#EEE; width:600px; overflow:auto; border-width:2px; border-color:#AAA; border-style:solid; padding:5px; }
		ul { margin-bottom:0; }
		li { list-style:square }
		input { width:100px; }
	</style>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<p/>
	<div id="errMsg">
	<%= HTMLUtil.escapeHTML(Messages.ERROR_MSG_ENCOUNTER) %>
	<ul>
<%
	Throwable t = e;
	while (t != null) {
	    String errMsg = HTMLUtil.escapeHTML(t.getLocalizedMessage());
	    if (errMsg == null || errMsg.trim().length() == 0) {
	        errMsg = t.getClass().getName();
	    }
%>
	<li><%= errMsg %></li>
<%
	    t = t.getCause();
	}
%>
	</ul>
	</div>
	<p/>
	
	<div id="hideDetail" style="display:block">
	<a href="javascript:mmHideDiv('hideDetail');mmShowDiv('errDetail')"><%= Messages.ERROR_SHOW_DETAILS %></a>
	</div>
	<div id="errDetail" style="display:none">
	<a href="javascript:mmShowDiv('hideDetail');mmHideDiv('errDetail')"><%= Messages.ERROR_HIDE_DETAILS %></a>
	
	<div id="mmException">
	<tt>
	<pre>
<%
	if (e != null) {
		PrintWriter pw = new PrintWriter(out);
		e.printStackTrace(pw);
	}
%>
	</pre>
	</tt>
	</div>
	
	</div>
</body>
</html>