<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.teradata.modelmanager.common.AppUtil" %>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%@ page import="com.teradata.modelmanager.vo.ModelProperties" %>
<%@ page import="com.teradata.modelmanager.vo.ADSProperties" %>
<%@ page import="com.teradata.modelmanager.vo.ADSParam" %>
<%@ page import="com.teradata.modelmanager.vo.ScoreProperties" %>
<%@ page import="com.teradata.modelmanager.util.DisplayTagUtil" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="disp" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final String LIST_TABLE_ID = "mmListTable";
%>
<%
	ModelProperties modelProp;
	ADSProperties adsProp = null;
	ScoreProperties scoreProp = null;
	ADSParam adsParam = null;
	HTMLUtil htmlUtil = new HTMLUtil();
	String msgKey, paramType;
	
	modelProp = (ModelProperties)session.getAttribute(MMAttr.MODEL_PROP);
	if (modelProp != null) {
		adsProp = modelProp.getAds();
		scoreProp = modelProp.getScore();
		if (adsProp != null) {
			adsParam = adsProp.getParams();
		}
	}
	
	String listQueryStr = (String)session.getAttribute(MMAttr.MODEL_LIST_QUERY);
	listQueryStr = (listQueryStr == null ? "" : listQueryStr);
	
	String action = (String)request.getAttribute(MMAttr.ACTION);
	boolean editMode = (action != null && action.equals("Edit"));
	
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = Messages.MODELPROP_TITLE;
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [][] {
	        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
	        {Messages.MODELLIST_TITLE, mmCtx + "/modellist.do" + listQueryStr},
	        {Messages.JOBLIST_TITLE, mmCtx + "/joblist.do"},
	};
	int linkIdx;
	
	DisplayTagUtil tagUtil = new DisplayTagUtil(request, LIST_TABLE_ID);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		td.section { background-color:#888; color:white; border-style:solid; border-width:1px; border-color:#888; }
		td.subsection { text-align:right; }
		td.propLabel { width: 100px; text-align:right; font-weight:normal; color:#888; font-size:8pt; }
		td.propValue { width: 500px; text-align:left; background-color:#EEE; padding-left:5px; padding-right:5px; }
		td#actionPane { width: 200px; vertical-align:top; text-align:left; padding-right:5px; }
		li { list-style:square; padding-top:0.25em; padding-bottom:0.25em; color:#AAA; }
		input.mmInputS { width: 300px; }
		input.mmInputL { width: 500px; }
		input.mmInputSErr { width:300px; background-color:#FCC; border-style:solid; border-width:1px; border-color:#555; }
		span.popup { display:none; position:absolute; margin-left:10px; z-index:1; border-style:solid; border-width:1px; border-color:#AAA; padding:5px; background-color:#FFC; color:#333; font-size:8pt; cursor:pointer; }
		span.popupAnchor { font-size:8pt; color:#00F; cursor:pointer; }
		div#divOuterPane { position:absolute;top:0;left:0;width:100%;height:768px;z-index:2; }
		div#divInnerPane { width:300px;position:relative;top:200px;left:0;z-index:2;border-color:#AAA;border-width:2px;border-style:solid;font-size:8pt;text-align:left;padding:5px;background-color:white; }
	</style>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmModelProp.js"></script>
	<script type="text/javascript" language="JavaScript">
		var dtRegExpr = /^<%= AppUtil.dateExpr %>$/
		var tmRegExpr = /^<%= AppUtil.timeExpr %>$/
		var tsRegExpr = /^<%= AppUtil.timestampExpr %>$/
		var numRegExpr = /^<%= AppUtil.numExpr %>$/
	</script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<div style="text-align:center">
	
	<form name="propForm"
		  method="post"
		  action="<%= mmCtx %>/modelproperties.do"
		  onSubmit="return mmValidateModelProperties(this, 'msg_')">
	
	<table border=0 cellspacing=0 cellpadding=0>
	<tr>
	<%-- properties grid -----------------------------------------------------%>
	<td id="propPane">
<%	if (!editMode) { // read only mode ************************************** %>
	<table border=0 cellspacing=2 cellpadding=0 class="listrender">
	<tr>
	<td class="section" colspan="2"><%= Messages.MODELPROP_MODEL %> <%= modelProp.getId() %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_NAME %></td><td class="propValue"><%= HTMLUtil.escapeHTML(modelProp.getName()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_DESC %></td><td class="propValue"><%= HTMLUtil.escapeHTML(modelProp.getDescription()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_VERSION %></td><td class="propValue"><%= HTMLUtil.escapeHTML(modelProp.getVersion()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_EXPIRE %></td><td class="propValue"><%= htmlUtil.formatDate(modelProp.getExpiration()) %>&nbsp;</td>
	</tr>
<%		if (adsProp != null) { %>
	<tr>
	<td class="section" colspan="2"><%= Messages.MODELPROP_ADS %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_REFRESH_TIME %></td><td class="propValue"><%= htmlUtil.formatNumber(adsProp.getRefreshTime()) %> <%= Messages.MODELPROP_SECONDS %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_LAST_REFRESH %></td><td class="propValue"><%= htmlUtil.formatTimestamp(adsProp.getLastRuntime()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ANCHOR_DB %></td><td class="propValue"><%= HTMLUtil.escapeHTML(adsProp.getAnchorDB()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ANCHOR_TABLE %></td><td class="propValue"><%= HTMLUtil.escapeHTML(adsProp.getAnchorTable()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ADS_DB %></td><td class="propValue"><%= HTMLUtil.escapeHTML(adsProp.getAdsDB()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ADS_TABLE %></td><td class="propValue"><%= HTMLUtil.escapeHTML(adsProp.getAdsTable()) %>&nbsp;</td>
	</tr><tr>
	<td class="subsection"><%= Messages.MODELPROP_PARAMETERS %></td><td>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_TARGET_DATE %></td><td class="propValue"><%= htmlUtil.formatDate(adsProp.getTargetDate()) %>&nbsp;</td>
<%
			if (adsParam != null) {
			    for (Iterator itr = adsParam.iterator(); itr.hasNext();) {
			        adsParam = (ADSParam)itr.next();
%>
	</tr><tr>
	<td class="propLabel"><%= HTMLUtil.escapeHTML(adsParam.getName()) %></td><td class="propValue"><%= adsParam.format(htmlUtil) %>&nbsp;
<%					if (adsParam.getDescription() != null && adsParam.getDescription().length() > 0) { %>
	<span id="mmPop<%= adsParam.getSequence() %>" class="popup" onclick="hidePopup()"><%= HTMLUtil.escapeHTML(adsParam.getDescription()) %></span>
	<span class="popupAnchor" onmouseover="showPopup('mmPop<%= adsParam.getSequence() %>')">?</span>
<%					} %>
	</td>
<%
			    }
			}
%>
	</tr>
<%		} %>
<%		if (scoreProp != null) { %>
	<tr>
	<td class="section" colspan="2"><%= Messages.MODELPROP_SCORE %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_SCORE_TIME %></td><td class="propValue"><%= htmlUtil.formatNumber(scoreProp.getScoreTime()) %> <%= Messages.MODELPROP_SECONDS %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_LAST_RUN %></td><td class="propValue"><%= htmlUtil.formatTimestamp(scoreProp.getLastRunTime()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_IN_DB %></td><td class="propValue"><%= HTMLUtil.escapeHTML(scoreProp.getInputDB()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_IN_TABLE %></td><td class="propValue"><%= HTMLUtil.escapeHTML(scoreProp.getInputTable()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_OUT_DB %></td><td class="propValue"><%= HTMLUtil.escapeHTML(scoreProp.getOutputDB()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_OUT_TABLE %></td><td class="propValue"><%= HTMLUtil.escapeHTML(scoreProp.getOutputTable()) %>&nbsp;</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_UDF_DB %></td><td class="propValue"><%= HTMLUtil.escapeHTML(scoreProp.getMetadataDB()) %>&nbsp;</td>
	</tr>
<%		} %>
	</table>
<%	} else { // edit mode *************************************************** %>
	<table border=0 cellspacing=2 cellpadding=0 class="listrender">
	<tr>
	<td class="section" colspan="2"><%= Messages.MODELPROP_MODEL %> <%= modelProp.getId() %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_NAME %></td><td class="propValue"><input class="mmInputS" type="text" name="mmModelName" value="<%= HTMLUtil.escapeHTML(modelProp.getName()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_DESC %></td><td class="propValue"><input class="mmInputL" type="text" name="mmModelDesc" value="<%= HTMLUtil.escapeHTML(modelProp.getDescription()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_VERSION %></td><td class="propValue"><input class="mmInputS" type="text" name="mmModelVersion" value="<%= HTMLUtil.escapeHTML(modelProp.getVersion()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_EXPIRE %></td><td class="propValue"><input class="mmInputS" type="text" name="mmModelExpire" value="<%= htmlUtil.formatDate(modelProp.getExpiration()) %>"/></td>
	</tr>
<%		if (adsProp != null) { %>
	<tr>
	<td class="section" colspan="2"><%= Messages.MODELPROP_ADS %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_REFRESH_TIME %></td><td class="propValue"><%= htmlUtil.formatNumber(adsProp.getRefreshTime()) %> <%= Messages.MODELPROP_SECONDS %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_LAST_REFRESH %></td><td class="propValue"><%= htmlUtil.formatTimestamp(adsProp.getLastRuntime()) %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ANCHOR_DB %></td><td class="propValue">
		<input class="mmInputS" type="text" name="mmAnchorDB" value="<%= HTMLUtil.escapeHTML(adsProp.getAnchorDB()) %>"/>
		<a href="javascript:mmShowAnchorSelection()"><%= Messages.MODELPROP_SEARCH %></a>
	</td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ANCHOR_TABLE %></td><td class="propValue"><input class="mmInputS" type="text" name="mmAnchorTable" value="<%= HTMLUtil.escapeHTML(adsProp.getAnchorTable()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ADS_DB %></td><td class="propValue"><input class="mmInputS" type="text" name="mmADSDB" value="<%= HTMLUtil.escapeHTML(adsProp.getAdsDB()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_ADS_TABLE %></td><td class="propValue"><input class="mmInputS" type="text" name="mmADSTable" value="<%= HTMLUtil.escapeHTML(adsProp.getAdsTable()) %>"/></td>
	</tr><tr>
	<td class="subsection"><%= Messages.MODELPROP_PARAMETERS %></td><td>&nbsp;</td>
	</tr><tr>
<%			if (adsProp.getTargetDate() != null) { %>
	<td class="propLabel"><%= Messages.MODELPROP_TARGET_DATE %></td><td class="propValue"><input class="mmInputS" type="text" name="mmTargetDate" value="<%= htmlUtil.formatDate(adsProp.getTargetDate()) %>"/></td>
<%			} else { %>
	<td class="propLabel"><%= Messages.MODELPROP_TARGET_DATE %></td><td class="propValue">&nbsp;</td>
<%			} %>
<%
			if (adsParam != null) {
			    for (Iterator itr = adsParam.iterator(); itr.hasNext(); ) {
			        adsParam = (ADSParam)itr.next();
			        paramType = adsParam.getTypeLabel();
			        
			        if (adsParam.isReadOnly()) {
%>
  	</tr><tr>
  	<td class="propLabel"><%= HTMLUtil.escapeHTML(adsParam.getName()) %></td><td class="propValue"><%= adsParam.format(htmlUtil) %>&nbsp;
  	<span id="mmPop<%= adsParam.getSequence() %>" class="popup" onclick="hidePopup()"><%= HTMLUtil.escapeHTML(adsParam.getDescription()) %></span>
  	<span class="popupAnchor" onmouseover="showPopup('mmPop<%= adsParam.getSequence() %>')">?</span>
  	</td>
<%
			        } else {
%>
	</tr><tr>
	<td class="propLabel"><%= HTMLUtil.escapeHTML(adsParam.getName()) %></td><td class="propValue"><input class="mmInputS" type=text" name="mmParam<%= adsParam.getSequence() %>" value="<%= adsParam.format(htmlUtil) %>"/>
	<span id="mmPop<%= adsParam.getSequence() %>" class="popup" onclick="hidePopup()"><%= "[" + paramType + "]<br/>" + HTMLUtil.escapeHTML(adsParam.getDescription()) %></span>
	<span class="popupAnchor" onmouseover="showPopup('mmPop<%= adsParam.getSequence() %>')">?</span>
	<input type="hidden" name="mmParamSeq" value="<%= adsParam.getSequence() %>"/>
	<input type="hidden" name="mmParamType" value="<%= adsParam.getType() %>"/>
	</td>
<%
			        }
			    }
			}
%>
	</tr>
<%		} %>
<%		if (scoreProp != null) { %>
	<tr>
	<td class="section" colspan="2"><%= Messages.MODELPROP_SCORE %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_SCORE_TIME %></td><td class="propValue"><%= htmlUtil.formatNumber(scoreProp.getScoreTime()) %> <%= Messages.MODELPROP_SECONDS %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_LAST_RUN %></td><td class="propValue"><%= htmlUtil.formatTimestamp(scoreProp.getLastRunTime()) %></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_IN_DB %></td><td class="propValue"><input class="mmInputS" type="text" name="mmInDB" value="<%= HTMLUtil.escapeHTML(scoreProp.getInputDB()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_IN_TABLE %></td><td class="propValue"><input class="mmInputS" type="text" name="mmInTable" value="<%= HTMLUtil.escapeHTML(scoreProp.getInputTable()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_OUT_DB %></td><td class="propValue"><input class="mmInputS" type="text" name="mmOutDB" value="<%= HTMLUtil.escapeHTML(scoreProp.getOutputDB()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_OUT_TABLE %></td><td class="propValue"><input class="mmInputS" type="text" name="mmOutTable" value="<%= HTMLUtil.escapeHTML(scoreProp.getOutputTable()) %>"/></td>
	</tr><tr>
	<td class="propLabel"><%= Messages.MODELPROP_UDF_DB %></td><td class="propValue"><input class="mmInputS" type="text" name="mmUDFDB" value="<%= HTMLUtil.escapeHTML(scoreProp.getMetadataDB()) %>"/></td>
	</tr>
<%		} %>
	</table>
<%	} %>
	<p/>
<%	if (editMode) { %>
	<table border=0 cellspacing=0 cellpadding=0>
	<tr><td>
		<div id="action-shadow"><div id="action-content">
		<input class="mmAction" type="submit" value="<%= Messages.MODELPROP_SAVE %>" onClick="this.form.a.value='Save'"/>
		<input class="mmAction" type="submit" value="<%= Messages.MODELPROP_CANCEL %>" onClick="this.form.a.value='Cancel'"/>
		</div></div>
	</td></tr>
	</table>
<%	} %>
	
	</td>
	<%-- end of properties grid ----------------------------------------------%>
	
	<%-- action links --------------------------------------------------------%>
	<td id="actionPane">
	<ul>
<%	if (!editMode) { %>
	<li><a href="javascript:mmEditModelProperties()"><%= Messages.MODELPROP_EDIT %></a></li>
<%	} %>
	<li><a href="javascript:mmShowRunNowById('hiddenjoblist', <%= modelProp.getId() %>)"><%= Messages.MODELPROP_RUN_NOW %></a></li>
	<li><a href="javascript:mmShowSchedulerById('hiddenjoblist', <%= modelProp.getId() %>)"><%= Messages.MODELPROP_SCHED_JOB %></a></li>
	<li><a href="<%= mmCtx %>/modelhistory.do"><%= Messages.MODELPROP_VIEW_HIST %></a></li>
	<li><a href="<%= mmCtx %>/cmdlist.do"><%= Messages.MODELPROP_VIEW_SQL_COMMANDS %></a></li>
	<li><a href="<%= mmCtx %>/modelvariables.do"><%= Messages.MODELPROP_VIEW_VARIABLES %></a></li>
<%	if (scoreProp != null) { %>
	<li><a href="<%= mmCtx %>/scorecolumns.do"><%= Messages.MODELPROP_VIEW_SCORE %></a></li>
	<li><a href="<%= mmCtx %>/scoredata.do"><%= Messages.MODELPROP_VIEW_DATA %></a></li>
<%	} %>
	</ul>
	</td>
	<%-- end of action links ---------------------------------------------- --%>
	</tr>
	</table>
	
	<disp:table name="joblist" id="hiddenjoblist" style="display:none" pagesize="500" sort="list" requestURI="/modelproperties.do">
		<disp:column property="select" title="<%= tagUtil.getFirstColumnTitle(\"<input type='checkbox' onclick='mmToggleCheckbox(this, jobForm.mmJob)'/>\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="name" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_JOB) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="publishDB" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PUBLISH_DB) %>" sortable="true" style="width:100px;text-align:center"></disp:column>
		<disp:column property="modelID" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_MODEL) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="refreshADS" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_ADS) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="scoreSQL" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_SCORE) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="description" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_DESCRIPTION) %>" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="dependency" title="Depends On" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="previousFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PREVIOUS) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
		<disp:column property="nextFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_NEXT) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
	</disp:table>
	
	<disp:table name="dependencylist" id="dependencylist" style="display:none" pagesize="500" sort="list" requestURI="joblist.do">
		<disp:column property="modelID" title="<%= tagUtil.getFirstColumnTitle(\"<input type='checkbox' onclick='mmToggleCheckbox(this, jobForm.mmJob)'/>\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="appID" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_JOB) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="appType" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PUBLISH_DB) %>" sortable="true" style="width:100px;text-align:center"></disp:column>
		<disp:column property="retryDelay" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_MODEL) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="retryNum" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_ADS) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
	</disp:table>
	
	<input type="hidden" name="a" value=""/>
	<input type="hidden" name="m" value="<%= modelProp.getId() %>"/>
	<input type="hidden" name="mmHasADS" value="<%= adsProp != null ? "X" : "" %>"/>
	<input type="hidden" name="mmHasScore" value="<%= scoreProp != null ? "X" : "" %>"/>
	</form>
	
	</div>
	
<%	if (editMode) { %>
	<%-- various validation messages -----------------------------------------%>
	<div class="mmDialog" id="t_date"><%= ADSParam.TYPE_DATE %></div>
	<div class="mmDialog" id="t_time"><%= ADSParam.TYPE_TIME %></div>
	<div class="mmDialog" id="t_timestamp"><%= ADSParam.TYPE_TIMESTAMP %></div>
	<div class="mmDialog" id="t_number"><%= ADSParam.TYPE_NUMBER %></div>
	<div class="mmDialog" id="msg_mmModelExpire"><%= HTMLUtil.escapeHTML(Messages.getString(Messages.MODELPROP_KEY_INVALID_DATE, new Object [] { Messages.MODELPROP_EXPIRE })) %></div>
	<div class="mmDialog" id="msg_mmTargetDate"><%= HTMLUtil.escapeHTML(Messages.getString(Messages.MODELPROP_KEY_INVALID_DATE, new Object [] { Messages.MODELPROP_TARGET_DATE })) %></div>
<%
		if (adsParam != null) {
		    for (Iterator itr = adsParam.iterator(); itr.hasNext(); ) {
		        adsParam = (ADSParam)itr.next();
		        if (!adsParam.isReadOnly()) {
		            msgKey = adsParam.getErrorMsgKey();
%>
	<div class="mmDialog" id="msg_mmParam<%= adsParam.getSequence() %>"><%= HTMLUtil.escapeHTML(Messages.getString(msgKey, new Object [] { adsParam.getName() })) %></div>
<%
		        } // read only
		    } // for-do
		} // adsParam exists
%>
	<%-- end of validation messages ------------------------------------------%>
	<%-- search anchor table dialog box --------------------------------------%>
	<%@include file="popup/selectAnchorTable.snippet" %>
	<%-- end of search anchor table dialog box -------------------------------%>
<%	} // editMode %>
	
	<%@include file="popup/scheduler.snippet" %>
	
</body>
</html>