<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="java.util.List" %>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.util.DisplayTagUtil" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%@ page import="com.teradata.modelmanager.vo.ModelProperties" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="disp" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final String JOB_TABLE_ID = "jobTable";
%>
<%
	ModelProperties model = (ModelProperties)session.getAttribute(MMAttr.MODEL_PROP);
	
	String listQueryStr = (String)session.getAttribute(MMAttr.MODEL_LIST_QUERY);
	listQueryStr = (listQueryStr == null ? "" : listQueryStr);
	
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = Messages.JOBLIST_TITLE;
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks;
	if (model != null) {
		menuLinks = new String [][] {
		        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
		        {Messages.MODELLIST_TITLE, mmCtx + "/modellist.do" + listQueryStr},
		        {Messages.MODELPROP_TITLE, mmCtx + "/modelproperties.do?m=" + model.getId()},
		};
	} else {
	    menuLinks = new String [][] {
		        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
		        {Messages.MODELLIST_TITLE, mmCtx + "/modellist.do" + listQueryStr},
		};
	}
	int linkIdx;
	
	DisplayTagUtil tagUtil = new DisplayTagUtil(request, JOB_TABLE_ID);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		  "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		table.listrender { margin-left:0; }
		td.section { font-size:12pt; font-weight:bold; text-align:left; }
		td.content { text-align:center; }
	</style>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<div style="text-align:center">
	
<%	if (((List)request.getAttribute("joblist")).size() > 0) { %>
	<table border=0 cellspacing=5 cellpadding=0>
	<tr><td class="section"><%= Messages.JOBLIST_ACTIVE_JOBS %>
	</td></tr>
	<tr><td class="content">
	
	<form name="jobForm"
		  method="post"
		  action="joblist.do"
		  onSubmit="return mmConfirmDelete(this.elements['mmJob'], 'msgSelectOne', 'msgDelConfirm')">
		  
	<disp:table name="joblist" id="<%= JOB_TABLE_ID %>" class="listrender" pagesize="15" sort="list" requestURI="joblist.do">
		<disp:column property="select" title="<%= tagUtil.getFirstColumnTitle(\"<input type='checkbox' onclick='mmToggleCheckbox(this, jobForm.mmJob)'/>\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="publishDB" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PUBLISH_DB) %>" sortable="true" style="width:100px;text-align:center"></disp:column>
		<disp:column property="modelID" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_MODEL) %>" sortable="true" href="modelproperties.do" paramId="m" style="width:50px;text-align:center"></disp:column>
		<disp:column property="refreshADS" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_ADS) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="scoreSQL" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_SCORE) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="description" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_DESCRIPTION) %>" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="dependency" title="Depends On" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="previousFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PREVIOUS) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
		<disp:column property="nextFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_NEXT) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
	</disp:table>
	
	<disp:table name="joblist" id="hiddenjoblist" style="display:none" pagesize="500" sort="list" requestURI="joblist.do">
		<disp:column property="select" title="<%= tagUtil.getFirstColumnTitle(\"<input type='checkbox' onclick='mmToggleCheckbox(this, jobForm.mmJob)'/>\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="name" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_JOB) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="publishDB" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PUBLISH_DB) %>" sortable="true" style="width:100px;text-align:center"></disp:column>
		<disp:column property="modelID" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_MODEL) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="refreshADS" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_ADS) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="scoreSQL" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_SCORE) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="description" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_DESCRIPTION) %>" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="dependency" title="Depends On" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="previousFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PREVIOUS) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
		<disp:column property="nextFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_NEXT) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
	</disp:table>
	
	<disp:table name="dependencylist" id="dependencylist" style="display:none" pagesize="500" sort="list" requestURI="joblist.do">
		<disp:column property="modelID" title="<%= tagUtil.getFirstColumnTitle(\"<input type='checkbox' onclick='mmToggleCheckbox(this, jobForm.mmJob)'/>\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="appID" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_JOB) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="appType" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PUBLISH_DB) %>" sortable="true" style="width:100px;text-align:center"></disp:column>
		<disp:column property="retryDelay" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_MODEL) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="retryNum" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_ADS) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	if (document.getElementById('<%= JOB_TABLE_ID %>')) {
		mmSetTHAnchorStyle(document.getElementById('<%= JOB_TABLE_ID %>').getElementsByTagName('thead')[0]);
	}
	</script>
	
	<br>
	<br>
	<table border=0 cellspacing=5 cellpadding=0
		   style="border-width:1px;border-color:#CCC;border-style:solid">
	<tr><td>
		<!--  <input class="mmAction" type="submit" value="<%= Messages.JOBLIST_EDIT %>" onClick="this.form.a.value='Edit'"/>   -->
		<input class="mmAction" type="button" value="<%= Messages.JOBLIST_EDIT %>" onClick="mmShowEditSchedulerById('hiddenjoblist', this.form, 'msgSelectOne', 'msgSelectOnlyOne')"/> 
		<input class="mmAction" type="submit" value="<%= Messages.JOBLIST_DELETE %>" onClick="this.form.a.value='Delete'"/>
	</td></tr>
	</table>
	
	<input type="hidden" name="a" value=""/>
	</form>
	
	</td></tr>
	</table>
	
	<div class="mmDialog" id="msgSelectOne"><%= Messages.JOBLIST_MSG_SELECT_ONE %></div>
	<div class="mmDialog" id="msgSelectOnlyOne"><%= Messages.JOBLIST_MSG_SELECT_ONLY_ONE %></div>
	<div class="mmDialog" id="msgDelConfirm"><%= Messages.JOBLIST_MSG_DEL_CONFIRM %></div>
<%	} else { %>
	<table border=0 cellspacing=5 cellpadding=0>
	<tr><td class="content"><%= Messages.JOBLIST_MSG_NO_ACTIVE_JOBS %>
	</td></tr>
	</table>
<%	} %>
	
	</div>
	<%@include file="popup/scheduler.snippet" %>
</body>
</html>