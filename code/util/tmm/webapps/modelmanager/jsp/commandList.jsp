<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.util.DisplayTagUtil" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%@ page import="com.teradata.modelmanager.vo.ModelProperties" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="disp" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final String ADS_TABLE_ID = "mmADSCmd";
	private static final String SCORE_TABLE_ID = "mmScoreCmd";
%>
<%
	ModelProperties model = (ModelProperties)session.getAttribute(MMAttr.MODEL_PROP);
	String listQueryStr = (String)session.getAttribute(MMAttr.MODEL_LIST_QUERY);
	listQueryStr = (listQueryStr == null ? "" : listQueryStr);
	
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = HTMLUtil.escapeHTML(Messages.CMDLIST_TITLE + " - "
										   + model.getName() + " ("
									       + model.getId() + ")"
										   );
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [][] {
	        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
	        {Messages.MODELLIST_TITLE, mmCtx + "/modellist.do" + listQueryStr},
	        {Messages.MODELPROP_TITLE, mmCtx + "/modelproperties.do?m=" + model.getId()},
	};
    int linkIdx;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		  "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		table.listrender { margin-left:0; }
		td.section { font-size:12pt; font-weight:bold; text-align:left; }
		td.command { text-align:center; }
		td.cmddisp { font-family:monospace; font-size:8pt; word-wrap:normal; }
	</style>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<div style="text-align:center">
	
	<table border=0 cellspacing=5 cellpadding=0>
<%
	if (model.getAds() != null) {
	    DisplayTagUtil tagUtil = new DisplayTagUtil(request, ADS_TABLE_ID);
%>
	<tr><td class="section"><a name="ads"/><%= Messages.CMDLIST_ADS %>
	</td></tr>
	<tr><td class="command">
	
	<disp:table name="adsCmds" id="<%= ADS_TABLE_ID %>" class="listrender" pagesize="10" sort="list" requestURI="/cmdlist.do#ads">
		<disp:column property="sequence" sortable="true" title="<%= tagUtil.getFirstColumnTitle(Messages.CMDLIST_SEQUENCE) %>" style="width:50px;text-align:center"></disp:column>
		<disp:column property="sqlCommand" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.CMDLIST_COMMAND) %>" class="cmddisp" style="width:650px;text-align:left"></disp:column>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	if (document.getElementById('<%= ADS_TABLE_ID %>')) {
		mmSetTHAnchorStyle(document.getElementById('<%= ADS_TABLE_ID %>').getElementsByTagName('thead')[0]);
	}
	</script>
	
	</td></tr>
<%	} %>
	<tr><td>&nbsp;</td></tr>
<%
	if (model.getScore() != null) {
	    DisplayTagUtil tagUtil = new DisplayTagUtil(request, SCORE_TABLE_ID);
%>
	<tr><td class="section"><a name="score"/><%= Messages.CMDLIST_SCORE %>
	</td></tr>
	<tr><td class="command">
	
	<disp:table name="scoreCmds" id="<%= SCORE_TABLE_ID %>" class="listrender" pagesize="10" sort="list" requestURI="/cmdlist.do#score">
		<disp:column property="sequence" sortable="true" title="<%= tagUtil.getFirstColumnTitle(Messages.CMDLIST_SEQUENCE) %>" style="width:50px;text-align:center"></disp:column>
		<disp:column property="sqlCommand" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.CMDLIST_COMMAND) %>" class="cmddisp" style="width:650px;text-align:left"></disp:column>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	mmSetTHAnchorStyle(document.getElementById('<%= SCORE_TABLE_ID %>').getElementsByTagName('thead')[0]);
	</script>
	
	</td></tr>
<%	} %>
	</table>
	
	</div>
	
</body>
</html>