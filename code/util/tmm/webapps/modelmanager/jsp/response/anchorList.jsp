<%@ page language="java" contentType="text/plain; charset=UTF-8" pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%
	List list;
	String item;

	list = (List)request.getAttribute(MMAttr.ANCHOR_LIST);
	for (Iterator itr = list.iterator(); itr.hasNext(); ) {
	    item = (String)itr.next();
%>
<%= item %>
<%	} %>