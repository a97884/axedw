<%@ page language="java" contentType="text/plain; charset=UTF-8" pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.user.MMUser" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%
	MMUser user;
	List list;
	String [] roles;
	
	user = (MMUser)request.getAttribute(MMAttr.USER_MAP);
	list = (List)request.getAttribute(MMAttr.USER_LIST);
	
	if (user != null) {
	    roles = user.getRoles();
%>
id=<%= user.getUserID() %>
email=<%= (user.getEmail() != null ? user.getEmail() : "") %>
<%		for (int i = 0; i < roles.length; i++) { %>
role=<%= roles[i] %>
<%
		}
	} else if (list != null) {
	    for (Iterator itr = list.iterator(); itr.hasNext(); ) {
%>
<%= (String)itr.next() %>
<%
	    }
	}
%>