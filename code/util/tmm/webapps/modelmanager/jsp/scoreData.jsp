<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.util.DisplayTagUtil" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%@ page import="com.teradata.modelmanager.vo.ModelProperties" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="disp" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final String DATA_TABLE_ID = "mmScoreData";
%>
<%
	String [] scoreCols;
	String scoreCol;
	ModelProperties model = (ModelProperties)session.getAttribute(MMAttr.MODEL_PROP);
	String listQueryStr = (String)session.getAttribute(MMAttr.MODEL_LIST_QUERY);
	listQueryStr = (listQueryStr == null ? "" : listQueryStr);
	
	DisplayTagUtil tagUtil = new DisplayTagUtil(request, DATA_TABLE_ID);
	
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = HTMLUtil.escapeHTML(Messages.SCOREDATA_TITLE + " - "
									       + model.getName() + " ("
								           + model.getId() + ")"
									       );
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [][] {
	        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
	        {Messages.MODELLIST_TITLE, mmCtx + "/modellist.do" + listQueryStr},
	        {Messages.MODELPROP_TITLE, mmCtx + "/modelproperties.do?m=" + model.getId()},
	};
	int linkIdx;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		  "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<div style="text-align:center">
	
	<disp:table name="scoreData" id="<%= DATA_TABLE_ID %>" class="listrender" pagesize="20" sort="list" requestURI="/scoredata.do">
<%
	scoreCols = model.getScore().getScoreCols();
	for (int i = 0; i < scoreCols.length; i++) {
        scoreCol = scoreCols[i];
%>
		<disp:column property="<%= scoreCol %>" sortable="true" title="<%= tagUtil.getColumnTitle(i, scoreCols[i].trim()) %>" style="text-align:left" decorator="com.teradata.modelmanager.display.GenericDecorator"></disp:column>
<%
	}
%>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	if (document.getElementById('<%= DATA_TABLE_ID %>')) {
		mmSetTHAnchorStyle(document.getElementById('<%= DATA_TABLE_ID %>').getElementsByTagName('thead')[0]);
	}
	</script>
	
	</div>
	
</body>
</html>