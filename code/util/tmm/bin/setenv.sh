#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: setenv.sh 7076 2012-10-30 16:48:06Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-10-30 17:48:06 +0100 (tis, 30 okt 2012) $
# Last Revision    : $Revision: 7076 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/tmm/bin/setenv.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# set CATALINA_PID so that shutdown.sh waits for the process to stop
#
export CATALINA_PID=$CATALINA_HOME/run.pid
