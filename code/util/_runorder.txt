--
-- install deployment scripts into bin folder
--
-- To diable expansion of variables in the code use the following:
--INSTCONF NOREPLVARS

--
-- install config scripts into config folder
--
INSTDIR	devutil/scm/config
INSTALL code/util/scm/config/.scm_profile 775 664
INSTALL code/util/scm/config/.scm_profile.uv2 775 664
INSTALL code/util/scm/config/infa_import_consolidated_object_order_config 775 664
INSTALL code/util/scm/config/.infascm_profile.template 775 664
INSTALL code/util/scm/config/.infascm_profile.uv2 775 664
INSTALL code/util/scm/config/update_windowsclient_svnconfig.cmd 775 664
INSTALL code/util/scm/config/.infa_profile 775 664
INSTALL code/util/scm/config/infa_import_folder_list.txt 775 664
INSTALL code/util/scm/config/svn_config 775 664
INSTALL code/util/scm/config/infa_import_object_order_config 775 664
--
-- install deployment scripts into bin folder
--
INSTDIR	devutil/scm/bin
INSTALL code/util/scm/bin/02_mrg2sys.sh 775 775
INSTALL code/util/scm/bin/td_updwa.sh 775 775
INSTALL code/util/scm/bin/10_create_hfx.sh 775 775
INSTALL code/util/scm/bin/td_merge2.sh 775 775
INSTALL code/util/scm/bin/07_tag_int_build.sh 775 775
INSTALL code/util/scm/bin/vi_get_svn_properties.sh 775 775
INSTALL code/util/scm/bin/11_tag_hfx.sh 775 775
INSTALL code/util/scm/bin/03_create_sys_build.sh 775 775
INSTALL code/util/scm/bin/td_package.sh 775 775
INSTALL code/util/scm/bin/get_infa_shortcut_duplicates.sh 775 775
INSTALL code/util/scm/bin/td_build_result.sh 775 775
INSTALL code/util/scm/bin/04_mrg2int.sh 775 775
INSTALL code/util/scm/bin/09_tag_rel.sh 775 775
INSTALL code/util/scm/bin/prep_wa_usr.sh 775 775
INSTALL code/util/scm/bin/run_ddl.sh 775 775
INSTALL code/util/scm/bin/01_expinfadev.sh 775 775
INSTALL code/util/scm/bin/infafunc.sh 775 775
INSTALL code/util/scm/bin/creupdenv.sh 775 775
INSTALL code/util/scm/bin/prep_wa_dev.sh 775 775
INSTALL code/util/scm/bin/get_files_per_svnprop.sh 775 775
INSTALL code/util/scm/bin/set_usr_unix_env.sh 775 775
INSTALL code/util/scm/bin/check_props.sh 775 775
INSTALL code/util/scm/bin/05_create_int_build.sh 775 775
INSTALL code/util/scm/bin/creparalenv.sh 775 775
INSTALL code/util/scm/bin/get_distinct_props.sh 775 775
INSTALL code/util/scm/bin/merge2trunk.sh 775 775
INSTALL code/util/scm/bin/impxml.sh 775 775
INSTALL code/util/scm/bin/apply_db_chg.sh 775 775
INSTALL code/util/scm/bin/get_files_without_svnprop.sh 775 775
INSTALL code/util/scm/bin/08_create_rel.sh 775 775
INSTALL code/util/scm/bin/modules.txt 775 665
INSTALL code/util/scm/bin/basefunc.sh 775 775
INSTALL code/util/scm/bin/td_merge.sh 775 775
INSTALL code/util/scm/bin/prep_wa_sys.sh 775 775
INSTALL code/util/scm/bin/getinfatags.sh 775 775
INSTALL code/util/scm/bin/prep_wa.sh 775 775
INSTALL code/util/scm/bin/expwrk2dev.sh 775 775
INSTALL code/util/scm/bin/set_tec_unix_env.sh 775 775
INSTALL code/util/scm/bin/packages.cfg 775 664
INSTALL code/util/scm/bin/set_dev_unix_env.sh 775 775

--
-- install templates into template folder
--
INSTDIR	devutil/scm/template
INSTALL code/util/scm/template/build/deploy_target_restore_script_header.txt 755 664
INSTALL code/util/scm/template/build/deploy_target_backup_script_header.txt 755 664
INSTALL code/util/scm/template/build/deploy_target_restore_script_footer.txt 755 664
INSTALL code/util/scm/template/build/deploy_target_backup_script_footer.txt 755 664

--
-- install infa files into infa folders
--
INSTDIR	devutil/infa/operate
INSTALL code/util/infa/operate/backupDomain.sh 775 775
INSTALL code/util/infa/operate/backupRepos.sh 775 775

INSTDIR	devutil/infa/install
INSTALL code/util/infa/install/add_os_profile.sh 775 775
INSTALL code/util/infa/install/environment.txt 755 664
INSTALL code/util/infa/install/install_stage.sh 775 775
INSTALL code/util/infa/install/.install.st1.profile.template 755 664
INSTALL code/util/infa/install/add_db_conn.sh 775 775
INSTALL code/util/infa/install/db2_connections.txt 755 664
INSTALL code/util/infa/install/tpt_connections.txt 755 664
INSTALL code/util/infa/install/.install.uv2.profile.template 755 664
INSTALL code/util/infa/install/folder.txt 755 664
INSTALL code/util/infa/install/install_func.sh 775 775
INSTALL code/util/infa/install/tdc_connections.txt 755 664
INSTALL code/util/infa/install/devuser.txt 755 664

--INSTALL code/util/infa/install/log

--
-- install wrapper commands into util folder
--
INSTDIR	devutil
INSTALL code/util/svn 775 775
INSTALL code/util/bteq 775 775
