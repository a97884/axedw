<%@ page import='java.util.List'%>
<%@ page import='java.util.Iterator'%>
<%@ page import='com.teradata.tjmsAdapter.Service'%>
<%@ page import='com.teradata.tjmsAdapter.Response'%>
<%@ page import='com.teradata.tjmsAdapter.Status'%>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants'%>
<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>

<%
	String sAppType = (String) request.getAttribute(ServiceConstants.APP_TYPE);
	String sAppName = "";
	String [] sArrTargetProps = null;
	if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
	{
		sAppName = ResourceBundleFactory.getString("UI_D_LOADER",request);
		sArrTargetProps = ServiceConstants.SERVICE_PROPS_TARGET_LOADER;
	}
	else
	{
		sAppName = ResourceBundleFactory.getString("UI_D_ROUTER",request);
		sArrTargetProps = ServiceConstants.SERVICE_PROPS_TARGET_EXTRACTOR;
	}
	Response responseSrv = (Response) request.getAttribute(ServiceConstants.SERVICE_RESPONSE);
	String sStatusMess = responseSrv.getStatusMessage();
	String sErrorMess = responseSrv.getErrorMessage();
	Service serviceDetails = responseSrv.getServiceDetails();
	List listServices = responseSrv.getServicesList();
	List listErrors = responseSrv.getErrorList();
	String sBaseURL = "Dispatcher?apptype=" + sAppType + "&request=performsrvact&" + ServiceConstants.SERVICE_ID + "=";
	String sRefreshURL = "Dispatcher?apptype=" + sAppType + "&request=performsrvact&" + ServiceConstants.SERVICE_ID + "=*&" + ServiceConstants.ACTION + "=" + ServiceConstants.DISPLAY_ALL;
%>

<script language="JavaScript" TYPE="text/javascript">

function warnOnRemove (serviceId)
{
	var warnVar = "UI_D_DELETE_SERVICE_WARN";
	var alertMessage = "<%= ResourceBundleFactory.getString("UI_D_DELETE_SERVICE_WARN",request) %>: " + serviceId;
	var bAnswer = confirm(alertMessage);
	if (bAnswer)
	{
		return true;
	}
	else
	{
		return false;
	}

}

function onStopService (serviceId)
{
	var imgID = 'stopped_img_' + serviceId;
	var removeID = 'stopped_remove_' + serviceId;
	var actionID = 'stopped_action_' + serviceId;

	document.getElementById(imgID).innerHTML = '<img src="images/shuttingDown.bmp">';
	document.getElementById(removeID).innerHTML = '';
	document.getElementById(actionID).innerHTML = '';

	return true;
}

</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

	<jsp:include page="topbar.jsp"/>
	<tr>						
		<td class="titleBox">&nbsp;<%= ResourceBundleFactory.getString("UI_D_DISPLAY_SERV",request) %> - <%= sAppName %></td>
	</tr>
	<jsp:include page="bottombar.jsp"/>

		<%
			if (listServices.size() > 0)
			{
		%>
				<tr><td class="whiteSpace"></td></tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td><input type="button" value=" <%= ResourceBundleFactory.getString("UI_B_REFRESH",request) %> " onClick="window.location='<%= sRefreshURL %>'"></td>
								<td align="right">
									<img src="images/started.bmp">=<font class="small"><%= ResourceBundleFactory.getString("UI_D_STARTED",request) %></font>
									<img src="images/stopped.bmp">=<font class="small"><%= ResourceBundleFactory.getString("UI_D_STOPPED",request) %></font>
									<img src="images/shuttingDown.bmp">=<font class="small"><%= ResourceBundleFactory.getString("UI_D_SHUT_DOWN",request) %></font>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td class="whiteSpace"></td></tr>

				<tr>
					<td>
					<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center">

						<%				
							for (int i = 0; i < listServices.size(); i++)
							{
								Service service = (Service) listServices.get(i);
								String sServiceID = service.getServiceID();
								String sServiceIDKey = service.getServiceIDKey();
								String sStatus = service.getStatus();
								String sServiceType = service.getName(ServiceConstants.SERVICE_TYPE);
								if (sServiceType == null)
									sServiceType = ServiceConstants.SINGLE;
								String sTdClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";
								String sTdClassImage = (i % 2 == 0) ? "evenRow" : "oddRow";
								String sImageSrc = (i % 2 == 0) ? "images/remove_whiteBG.gif" : "images/remove.gif";

								if (i == 0)
								{
						%>
									<tr>
										<td class="tableTopL" width="5%"><b><%= ResourceBundleFactory.getString("UI_D_STATUS",request) %></b></td>
										<td class="tableTopL" width="5%"></td>
										<td class="tableTopL" width="7%"><b><%= ResourceBundleFactory.getString("UI_D_ACTION",request) %></b></td>
										<td class="tableTopL" width="8%"><b>Error Log</b></td>
										<%
										if (sAppType.equals(ServiceConstants.APP_TYPE_LOADER)) {
										%>
											<td class="tableTopL" width="22%"><b><%= ResourceBundleFactory.getString("UI_D_SERVICE_ID",request) %></b></td>
											<td class="tableTopL" width="53%"><b><%= ResourceBundleFactory.getString("UI_D_SERVICE_TYPE",request) %></b></td>
										<%} else {%>
											<td class="tableTopL" width="75%"><b><%= ResourceBundleFactory.getString("UI_D_SERVICE_ID",request) %></b></td>
										<%} %>
									</tr>
						<%
								}
						%>
								<tr>
						<%
								if (sStatus.equals(ServiceConstants.STOPPED))
								{
						%>
									<td class="<%= sTdClassImage %>"><img src="images/stopped.bmp"></td>
									<td class="<%= sTdClassImage %>"><a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.REMOVE %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><img src="<%= sImageSrc %>" border="0" alt="<%= ResourceBundleFactory.getString("UI_D_DELETE",request) %>" onclick="return warnOnRemove('<%= sServiceID %>')"></a></td>
									<td class="<%= sTdClass %>"><a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.START %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><%= ResourceBundleFactory.getString("UI_D_START",request) %></a></td>
									<td class="<%= sTdClass %>"><a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.ERROR_LOGS %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>">View</a></td>
									<td class="<%= sTdClass %>">
										<a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.SHOW_DETAILS %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><%= sServiceID %></a>
									</td>
									<%
									if (sAppType.equals(ServiceConstants.APP_TYPE_LOADER)) {
									%>
										<td class="<%= sTdClass %>"><%= sServiceType %></td>
									<%} %>
						<%
								}
								else if (sStatus.equals(ServiceConstants.STARTED))
								{
						%>	
									<td class="<%= sTdClassImage %>" id="stopped_img_<%= sServiceID %>"><img src="images/started.bmp"></td>
									<td class="<%= sTdClassImage %>" id="stopped_remove_<%= sServiceID %>"><a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.REMOVE %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><img src="<%= sImageSrc %>" border="0" alt="<%= ResourceBundleFactory.getString("UI_D_DELETE",request) %>" onclick="return warnOnRemove('<%= sServiceID %>')"></a></td>
									<td class="<%= sTdClass %>" id="stopped_action_<%= sServiceID %>"><a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.STOP %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>" onclick="return onStopService('<%= sServiceID %>')"><%= ResourceBundleFactory.getString("UI_D_STOP",request) %></a></td>
									<td class="<%= sTdClass %>"><a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.ERROR_LOGS %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>">View</a></td>
									<td class="<%= sTdClass %>">
										<a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.SHOW_DETAILS %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><%= sServiceID %></a>
									</td>
									<%
									if (sAppType.equals(ServiceConstants.APP_TYPE_LOADER)) {
									%>
										<td class="<%= sTdClass %>"><%= sServiceType %></td>
									<%} %>
						<%
								}
								else
								{
						%>
									<td class="<%= sTdClassImage %>"><img src="images/shuttingDown.bmp"></td>
									<td class="<%= sTdClassImage %>"></td>
									<td class="<%= sTdClass %>"></td>
									<td class="<%= sTdClass %>"></td>
									<td class="<%= sTdClass %>">
										<a href="<%= sBaseURL %><%= sServiceID %>&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.SHOW_DETAILS %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><%= sServiceID %></a>
									</td>
									<%
									if (sAppType.equals(ServiceConstants.APP_TYPE_LOADER)) {
									%>
										<td class="<%= sTdClass %>"><%= sServiceType %></td>
									<%} %>
						<%
								}
						%>
								</tr>
						<%
							}
						%>

					</table>
					</td>
				</tr>
		<%
			}
		%>


		<%
			if (sErrorMess != null && sErrorMess.length() > 0)
			{
		%>
				<tr><td class="whiteSpace"></td></tr>
				<tr><td><%= sErrorMess %></td></tr>
		<%
			}
			else if (sStatusMess != null && sStatusMess.length() > 0)
			{
		%>
				<tr><td class="whiteSpace"></td></tr>
				<tr><td><%= sStatusMess %></td></tr>
		<%
			}
		%>

		<%
			if (serviceDetails != null)
			{
		%>
				<tr><td class="whiteSpace"></td></tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

						<%
							for(int i = 0; i < ServiceConstants.SERVICE_PROPS_SOURCE.length; i++)
							{
								String sName = ServiceConstants.SERVICE_PROPS_SOURCE[i];
								String sValue = serviceDetails.getName(sName);
								String sTdClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";

								if (i == 0)
								{
						%>
									<tr>
										<td class="oddRowL"><b><%= ResourceBundleFactory.getString("UI_D_KEY",request) %></b></td>
										<td class="oddRowL"><b><%= ResourceBundleFactory.getString("UI_D_VALUE",request) %></b></td>
									</tr>

						<%
								}
						%>
								<tr>
									<td class="<%= sTdClass %>"><%= sName %></td>
									<td class="<%= sTdClass %>"><%= sValue %></td>
								</tr>
						<%
							}
						%>
								<tr>
									<td class="whiteSpace"></td>
									<td class="whiteSpace"></td>
								</tr>
						<%
							for(int i = 0; i < sArrTargetProps.length; i++)
							{
								String sName = sArrTargetProps[i];
								String sValue = serviceDetails.getName(sName);
								String sTdClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";
								//For target operation type
								if (sName.equalsIgnoreCase(ServiceConstants.SERVICE_TYPE)) {
									if (sValue == null)
										sValue = ServiceConstants.SINGLE;
									sTdClass = "evenRowL";
									%>
									<tr>
										<td class="whiteSpace"></td>
										<td class="whiteSpace"></td>
									</tr>
									<%
								}

								if (i == 0 || sName.equalsIgnoreCase(ServiceConstants.SERVICE_TYPE))
								{
						%>
									<tr>
										<td class="oddRowL"><b><%= ResourceBundleFactory.getString("UI_D_KEY",request) %></b></td>
										<td class="oddRowL"><b><%= ResourceBundleFactory.getString("UI_D_VALUE",request) %></b></td>
									</tr>

						<%
								}
						%>
								<tr>
									<td class="<%= sTdClass %>"><%= sName %></td>
									<td class="<%= sTdClass %>"><%= sValue %></td>
								</tr>
						<%
							}
						%>
					</table>
					</td>
				</tr>
		<%
			}
		%>

		<%
			if (listErrors == null)
			{
		%>
				<tr><td class="whiteSpace"></td></tr>
				<tr><td><%= ResourceBundleFactory.getString("UI_D_ERROR_EMPTY",request) %></td></tr>
		<%
			}
			else
			{
		%>
				<tr><td class="whiteSpace"></td></tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center">

						<%
							for(int i = 0; i < listErrors.size(); i++)
							{
								Status status = (Status) listErrors.get(i);
								String sTdClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";

								if (i == 0)
								{
						%>
									<tr>
										<td class="tableTopL" width="120px"><b>Time</b></td>
										<td class="tableTopL" width="120px"><b>ErrorFile</b></td>
										<td class="tableTopL" width="120px"><b>Error Code</b></td>
										<td class="tableTopL"><b>Error Message</b></td>
									</tr>

						<%
								}
						%>
								<tr>
									<td class="<%= sTdClass %>"><%= status.getReportTime() %></td>
									<td class="<%= sTdClass %>"><%= status.getErrorFile() %></td>
									<td class="<%= sTdClass %>"><%= status.getCode() %></td>
									<td class="<%= sTdClass %>"><%= status.getMessage() %></td>
								</tr>
						<%
							}
						%>
								<tr>
									<td class="whiteSpace"></td>
									<td class="whiteSpace"></td>
									<td class="whiteSpace"></td>
									<td class="whiteSpace"></td>
								</tr>
					</table>
					</td>
				</tr>
		<%
			}
		%>

		<tr><td class="whiteSpace"></td></tr>				

</table>