<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<% String sMessage = (String) request.getAttribute("Message"); %>
<% String sInfoMessage = (String) request.getAttribute("InfoMessage"); %>
<% String sBackButton = (String) request.getAttribute("BackButton"); %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td><%= sMessage %></td>
	</tr>
	<tr>
		<td><font color="red"><%= sInfoMessage %></font></td>
	</tr>
	<%if (sBackButton != null && sBackButton.length() > 0) {%>
	
	<tr><td class="whiteSpace"></td></tr>

	<tr>
		<jsp:include page="topbar.jsp"/>
		<jsp:include page="bottombar.jsp"/>
	</tr>
	
	<tr><td class="whiteSpace"></td></tr>

	<tr>
		<td>
			<input type="button" name="back" value="< <%= ResourceBundleFactory.getString("UI_B_BACK",request) %> " onClick="history.back()"></input>
		</td>
	</tr>
	<%} %>
</table>
