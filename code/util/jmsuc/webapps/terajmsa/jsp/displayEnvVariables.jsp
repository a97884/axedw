<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory' %>
<%@ page import='com.teradata.tjmsl.util.Constants' %>
<%@ page import="com.teradata.shared.environment.EnvironmentVariables" %>
<%@ page import="com.teradata.shared.environment.Variable" %>
<%@ page import="com.teradata.shared.environment.Category" %>
<%@ page import="java.util.List" %>

<html>
<body>

<%	
	EnvironmentVariables env = (EnvironmentVariables) request.getAttribute(Constants.ENV_VARS);
	List listCategories = env.getCategories();
	String baseHref = "Dispatcher?request=GetEnvVar&fn=";
	String viewHref = baseHref + "View" + "&varName=";
	String editHref = baseHref + "Edit" + "&varName=";
%>

<form name="myForm" method="post">

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<jsp:include page="topbar.jsp"/>
		<tr>
			<td class="titleBox">&nbsp;<%= ResourceBundleFactory.getString("UI_D_ENV_VARS",request) %></td>
		</tr>
		<jsp:include page="bottombar.jsp"/>

	</table>

	<table WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="0" ALIGN="CENTER">

		<% for(int i=0; i < listCategories.size(); i++) {
			Category cat = (Category) listCategories.get(i);
			String sCategoryName = cat.getName();
			List listVariable = cat.getVariableList();
		%>

			<% if (!sCategoryName.equals("Email Notification")){ %>
				<tr><td class="whiteSpace"></td></tr>
				<tr>
					<!-- For Intel POC-->
						<td style="width:15%; padding-left:3px" ALIGN="LEFT" class="titleBox"><b><%= sCategoryName %></b></td>
						<td style="width:20%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"><%= ResourceBundleFactory.getString("ALL_D_NAME", request) %></font>
						</td>
						<td style="width:50%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"><%= ResourceBundleFactory.getString("ALL_D_VALUE",request) %></font>
						</td>
				</tr>
			<%} %>

			<% for(int j=0; j < listVariable.size(); j++) {
				Variable var = (Variable) listVariable.get(j);
				String varName = var.getName();
				String row = "oddRowL";
				if( j%2 == 0)
					row = "evenRowL";
			%>
				<tr>
					<!-- For Intel POC-->
					<% if (!varName.equals(Constants.MAX_BATCH_SIZE) && 
							!varName.equals(Constants.EMAIL_LEVEL) && !varName.equals(Constants.SMTP_SERVER) &&
							!varName.equals(Constants.PORT_NUMBER) && !varName.equals(Constants.EMAIL_RECEIVER) &&
							!varName.equals(Constants.USE_PROPERTIES))
					{ %>

					<td style="width:15%; padding-left:3px" class="<%= row %>">
						<a class="metadata" href="<%= viewHref + varName %>"><%= ResourceBundleFactory.getString("UI_B_VIEW",request) %></a>
						<% if (!varName.equals(Constants.VERSION)) { %>
							<a class="metadata" href="<%= editHref + varName %>"><%= ResourceBundleFactory.getString("UI_B_EDIT",request) %></a>
						<%} %>
					</td>
					<td style="width:20%; padding-left:3px" class="<%= row %>"><%= varName %></td>
						<% if (!varName.equals(Constants.EMAIL_LEVEL)) { %>
							<td style="width:50%; padding-left:3px" class="<%= row %>"><%= var.getStringValueToFit() %></td>
						<%} else {
							String emailLevelValue = "";
							java.lang.Integer intObj = (java.lang.Integer) var.getValue();
							switch (intObj.intValue()) {
								case 0: emailLevelValue = ""; break;
								case 1: emailLevelValue = "START"; break;
								case 10: emailLevelValue = "STOP"; break;
								case 11: emailLevelValue = "STOP,START"; break;
								case 100: emailLevelValue = "ERROR"; break;
								case 101: emailLevelValue = "ERROR,START"; break;
								case 110: emailLevelValue = "ERROR,STOP"; break;
								case 111: emailLevelValue = "ERROR,STOP,START"; break;
								default: emailLevelValue = ""; break;
							}
						%>
							<td style="width:50%; padding-left:3px" class="<%= row %>"><%= emailLevelValue %></td>
						<%} %>
					<%} %>
				</tr>
			<%} %>
		<%} %>
	</table>
</form>
</body>
</html>