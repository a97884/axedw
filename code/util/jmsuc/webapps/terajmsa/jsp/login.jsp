<%@ page contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title><%= ResourceBundleFactory.getString("UI_D_MAIN_APP_TITLE_WITH_PRODUCT_VERSION",request) %></title>
<link rel="stylesheet" type="text/css" href="css/tjmsl.css" />
<link rel="icon" href="images/favicon.ico" /> 
<link rel="shortcut icon" href="images/favicon.ico" /> 

<script type="text/javascript">

function badLogon(error) {alert(error);}

function onField(){ document.myForm.userName.focus(); }

function check()
{
 inputs = document.myForm;
 useN = inputs.userName.value;
 pass = inputs.password.value;

 submitOK="True";

 if ( (useN < 1) || (pass < 1) )
 {
   if (useN < 1) 
   {
     alert("<%= ResourceBundleFactory.getString("UI_E_USER_NAME_EMPTY",request) %>");
     document.myForm.userName.focus(); 
   }
   else 
   { 
     alert("<%= ResourceBundleFactory.getString("UI_E_PASSWORD_EMPTY",request) %>");

     document.myForm.password.focus(); 
   }

   submitOK="False";
 }
 
 if (submitOK=="False")
 {
   return false;
 }

 else
 {
   return true;
 }
}

</script>
</head>
<body onload="onField()">
<form name="myForm" action="Authenticate" method="post" onsubmit="return check()">

	<% 
		String errMsg = (String) request.getAttribute("UIMessage");

		if( ( errMsg != null) && (errMsg.length() > 0) )
		{%>
			<script type="text/javascript">
				badLogon("<%= errMsg %>");
			</script>
		<%}
	%>

	<TABLE WIDTH="30%" HEIGHT="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<tr>
			<td ALIGN="MIDDLE">
				<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">
					<tr><td align="center"><img src="images/TeradataLogo.bmp"></td></tr>

					<jsp:include page="topbar.jsp"/>
					
					<tr>
						<td class="titleBox" align="center"><%= ResourceBundleFactory.getString("UI_D_MAIN_APP_TITLE",request) %></td>			
					</tr>

					<jsp:include page="bottombar.jsp"/>
					
					<tr>
						<td>
							<table BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">
								<tr>
									<td align="right"><%= ResourceBundleFactory.getString("UI_D_DATABASE_USER",request) %>:

									</td>

									<td align="left">
										<input type="text" name="userName" size="17" maxlength="50">
									</td>
								</tr>

								<tr>
									<td align="right"><%= ResourceBundleFactory.getString("UI_D_DATABASE_PASSWORD",request) %>:

									</td>

									<td align="left">
										<input type="password" name="password" size="17" maxlength="50">
									</td>
								</tr>
							</table>
						</td>
					</tr>
							
					<tr>
						<td align="center">
							<input type="submit" name="Button" value="<%= ResourceBundleFactory.getString("UI_B_LOGIN",request) %>">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>
