<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants'%>
<%@ page import='com.teradata.tjmsl.util.Constants' %>

<% 
	String sAppType = (String) request.getAttribute(ServiceConstants.APP_TYPE);
	String sAppName = "";
	if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
	{
		sAppName = ResourceBundleFactory.getString("UI_D_LOADER",request);
	}
	else
	{
		sAppName = ResourceBundleFactory.getString("UI_D_ROUTER",request);
	}
%>

<html>
<head>

<script type="text/javascript">

function onField()
{
	document.myForm.serviceid.focus(); 
}

function validate() 
{	
	inputs = document.myForm;
	vServiceID = inputs.<%= ServiceConstants.SERVICE_ID %>.value; 
	vServiceIDTrimmed = vServiceID.replace(/^\s+|\s+$/g, '');
	inputs.<%= ServiceConstants.SERVICE_ID %>.value = vServiceIDTrimmed;
	vServiceID = vServiceIDTrimmed;

	vSourceName = inputs.<%= ServiceConstants.SOURCE_NAME %>.value;
	vSourceConnectionName  = inputs.<%= ServiceConstants.SOURCE_CONNECTION_NAME %>.value;
	vTargetName = inputs.<%= ServiceConstants.TARGET_NAME %>.value;
	vTargetConnectionName  = inputs.<%= ServiceConstants.TARGET_CONNECTION_NAME %>.value;

	if(vServiceID.length == 0)
	{
		alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " is required");
		return false;
	}
	
	for(var i = 0; i < vServiceID.length; i++)
	{
		var vASCIIValue = vServiceID.charCodeAt(i);
		
		//Numbers 0x30 ~ 0x39
		//Upper Letters 0x41 ~ 0x5a
		//_ 0x5f
		//Lower Letters 0x61 ~ 0x7a

		if(vASCIIValue < 0x30)
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x39) && (vASCIIValue < 0x41))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x5a) && (vASCIIValue < 0x5f))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x5f) && (vASCIIValue < 0x61))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if(vASCIIValue > 0x7a)
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}
	}

	if(vSourceName.length == 0)
	{
		alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SOURCE_NAME", request) %>" + " is required");
		return false;
	}

	if(vSourceConnectionName.length == 0)
	{
		alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SOURCE_CONNECTION_NAME", request) %>" + " is required" );
		return false;
	}

	if(vTargetName.length == 0)
	{
		alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_TARGET_NAME", request) %>" + " is required");
		return false;
	}

	if(vTargetConnectionName.length == 0)
	{
		alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_TARGET_CONNECTION_NAME", request) %>" + " is required" );
		return false;
	}

	vDelimiter  = document.getElementById('delimiter_text').value;
	vFormat  = document.getElementById('format_select').value;
	if((vFormat == 'VARIABLE') && vDelimiter.length == 0)
	{
		alert("The input parameter Delimiter is required when Format is VARIABLE" );
		return false;
	}
	
	return true;
}

function delimiterChange(delimiterid)
{
	document.getElementById('<%= ServiceConstants.MSG_DELIMITER %>').value = document.getElementById(delimiterid).value;
}


function formatChange(selectedFormat)
{
	var formatName = document.getElementById(selectedFormat).value;
	
	if (formatName != 'VARIABLE')
	{
		document.getElementById('delimTitle').style.display = 'none';
		document.getElementById('delimValue').style.display = 'none';
	}
	else
	{
		document.getElementById('delimTitle').style.display = 'block';
		document.getElementById('delimValue').style.display = 'block';
	}
	document.getElementById('<%= ServiceConstants.MSG_FORMAT %>').value = formatName;
}


function changeDisplay(selectedOp) {

	var opeationType = document.getElementById(selectedOp).value;
	
	if (opeationType == '<%= ServiceConstants.SINGLE %>' ) {
		document.getElementById('format_select').disabled = !document.getElementById(selectedOp).checked;
	} else {
		document.getElementById('format_select').value = 'XML';
		formatChange('format_select');
		document.getElementById('format_select').disabled = document.getElementById(selectedOp).checked;
	}
}

</script>
</head>

<body onload="onField()">

	<form name="myForm" action="Dispatcher?apptype=<%= sAppType %>&request=performsrvact&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.CREATE %>" method="post" onSubmit="return validate();">

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<tr>
			<jsp:include page="topbar.jsp"/>
			<td class="titleBox">&nbsp;<%= ResourceBundleFactory.getString("UI_D_CREATE_SERV",request) %> - <%= sAppName %></td>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr><td class="whiteSpace"></td></tr>

		<tr>
			<td>
				<table WIDTH="60%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="LEFT">
					
					<tr><td class="whiteSpace"></td></tr>

					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>:&nbsp;</td>
						<td align="left"><input type="text" name="<%= ServiceConstants.SERVICE_ID %>" size="60" maxlength="64" value=""></td>
					</tr>

					<tr><td class="whiteSpace"></td></tr>

					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_SOURCE_NAME", request) %>:&nbsp;</td>
						<td align="left"><input type="text" name="<%= ServiceConstants.SOURCE_NAME %>" size="60" maxlength="100" value=""></td>
					</tr>
					
					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_SOURCE_CONNECTION_NAME", request) %>:&nbsp;</td>
						<td align="left"><input type="text" name="<%= ServiceConstants.SOURCE_CONNECTION_NAME %>" size="60" maxlength="100" value=""></td>
					</tr>

					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_SOURCE_TYPE", request) %>:&nbsp;</td>
						<td align="left">
							<table>
							<tr>
								<%
								if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
								{
								%>
									<td>
										<input type="radio" checked name="<%= ServiceConstants.SOURCE_TYPE %>" value="<%= ServiceConstants.QUEUE %>"><%= ResourceBundleFactory.getString("UI_D_QUEUE", request) %>
									</td>

									<td>
										<input type="radio"  name="<%= ServiceConstants.SOURCE_TYPE %>" value="<%= ServiceConstants.DURABLE_TOPIC %>"><%= ResourceBundleFactory.getString("UI_D_TOPIC_DURABLE", request) %>
									</td>

									<td>
										<input type="radio"  name="<%= ServiceConstants.SOURCE_TYPE %>" value="<%= ServiceConstants.NON_DURABLE_TOPIC %>"><%= ResourceBundleFactory.getString("UI_D_TOPIC_NONDURABLE", request) %>
									</td>
								<%
								}
								else
								{
								%>
									<td>
										<input type="radio" checked name="<%= ServiceConstants.SOURCE_TYPE %>" value="<%= ServiceConstants.QUEUE_TABLE %>"><%= ResourceBundleFactory.getString("UI_D_QUEUE_TABLE", request) %>
									</td>
								<%
								}
								%>
							</tr>
							</table>
						</td>
					</tr>

					<tr><td class="whiteSpace"></td></tr>

					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_TARGET_NAME", request) %>:&nbsp;</td>
						<td align="left"><input type="text" name="<%= ServiceConstants.TARGET_NAME %>" size="60" maxlength="100" value=""></td>
					</tr>
					
					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_TARGET_CONNECTION_NAME", request) %>:&nbsp;</td>
						<td align="left"><input type="text" name="<%= ServiceConstants.TARGET_CONNECTION_NAME %>" size="60" maxlength="100" value=""></td>
					</tr>

					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_TARGET_TYPE", request) %>:&nbsp;</td>
						<td align="left">
							<table>
							<tr>
								<%
								if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
								{
								%>
									<td>
										<input type="radio" checked name="<%= ServiceConstants.TARGET_TYPE %>" value="<%= ServiceConstants.TABLE %>"><%= ResourceBundleFactory.getString("UI_D_TABLE", request) %>
									</td>
								<%
								}
								else
								{
								%>
									<td>
										<input type="radio" checked name="<%= ServiceConstants.TARGET_TYPE %>" value="<%= ServiceConstants.QUEUE %>"><%= ResourceBundleFactory.getString("UI_D_QUEUE", request) %>
									</td>

									<td>
										<input type="radio" name="<%= ServiceConstants.TARGET_TYPE %>" value="<%= ServiceConstants.TOPIC %>"><%= ResourceBundleFactory.getString("UI_D_TOPIC", request) %>
									</td>
								<%
								}
								%>
							</tr>
							</table>
						</td>
					</tr>

					<tr><td class="whiteSpace"></td></tr>
					
					<%
					if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
					{
					%>
					<tr>			
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_SERVICE_TYPE", request) %>:&nbsp;</td>
						<td align="left">
							<table>
								<tr>
								    <td>
										<input type="radio" checked name="<%= ServiceConstants.SERVICE_TYPE %>" id="singleop_radio" value="<%= ServiceConstants.SINGLE %>" onclick="changeDisplay(this.id)"/><%= ResourceBundleFactory.getString("UI_D_SINGLE", request) %>
									</td>
	
									<td>
										<input type="radio" name="<%= ServiceConstants.SERVICE_TYPE %>" id="multipleop_radio" value="<%= ServiceConstants.MULTIPLE %>" onclick="changeDisplay(this.id)"/><%= ResourceBundleFactory.getString("UI_D_MULTIPLE", request) %>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td class="whiteSpace"></td></tr>
					<tr>
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_ROLLBACK_INVALIDF_MSG",request) %>:&nbsp</td>
						<td align="left"><select name="<%= ServiceConstants.IS_ROLLBACK_INVF_MSG %>" style="width:175">
											 <option value="true">true</option>
											 <option value="false" selected>false</option>
										 </select>
						</td>
					</tr>
					<tr><td class="whiteSpace"></td></tr>
					<tr>
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_FORMAT",request) %>:&nbsp</td>
						<td align="left"><select name="format_select" id="format_select" style="width:175" onChange="formatChange(this.id)">
											 <option value="VARIABLE" selected>VARIABLE</option>
											 <option value="FIXED">FIXED</option>
											 <option value="XML">XML</option>
										 </select>
						</td>
					</tr>
					<tr><td class="whiteSpace"></td></tr>
					<tr>		
						<td align="right" nowrap="nowrap"><div id="delimTitle"><%= ResourceBundleFactory.getString("UI_D_DELIMITER",request) %>:&nbsp;</div></td>
						<td align="left">
							<div id="delimValue">
								<input type="text" name="delimiter_text" id="delimiter_text" size="30" maxlength="30" value="" onChange="delimiterChange(this.id)"></input>
							</div>
						</td>
					</tr>
					<%
					}
					%>

					<tr><td class="whiteSpace"></td></tr>

					<tr>
						<td class="whiteSpace"></td>
						<td align="left">
							<input type="Submit" name="Button" value="<%= ResourceBundleFactory.getString("UI_B_CREATE", request) %>">
						</td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
	<input type="hidden" name="<%= ServiceConstants.MSG_FORMAT %>" id="<%= ServiceConstants.MSG_FORMAT %>" value="VARIABLE"></input>
	<input type="hidden" name="<%= ServiceConstants.MSG_DELIMITER %>" id="<%= ServiceConstants.MSG_DELIMITER %>" value=""></input>
</form>
</body>
</html>