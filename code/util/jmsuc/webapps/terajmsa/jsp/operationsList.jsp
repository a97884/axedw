<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<%@ page import='com.teradata.tjmsAdapter.util.Constants'%>
<%@ page import='com.teradata.tjmsAdapter.Service'%>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants'%>
<%@ page import='java.util.Set'%>
<%@ page import='java.util.Map'%>
<%@ page import='java.util.Iterator'%>

<% 
	Map mapOperations = null;
	Service service = null;
	String sAppType = (String) request.getAttribute(ServiceConstants.APP_TYPE);
	String sAppName = "";
	String sServletName = "";
	if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
	{
		sAppName = ResourceBundleFactory.getString("UI_D_LOADER",request);
		sServletName = Constants.L_SVL_CR_P_JMS_TO_TABLE;
	}
	else
	{
		sAppName = ResourceBundleFactory.getString("UI_D_ROUTER",request);
		sServletName = Constants.R_SVL_CR_P_QTABLE_TO_JMS;
	}
	service = (Service) request.getAttribute(Constants.UI_SERVICE);
	mapOperations = (Map) request.getAttribute(Constants.UI_MAP_OPERATIONS); 
	
	String sServiceType = service.getName(ServiceConstants.SERVICE_TYPE);
	if (sServiceType == null)
		sServiceType = ServiceConstants.SINGLE;	
	
	String sRollbackInvalidFormatMsg_true = "";
	String sRollbackInvalidFormatMsg_false = "";
	if(service.getName(ServiceConstants.IS_ROLLBACK_INVF_MSG).equalsIgnoreCase("") || 
			service.getName(ServiceConstants.IS_ROLLBACK_INVF_MSG).equalsIgnoreCase("false")) {
		sRollbackInvalidFormatMsg_false = "selected";
	} else {
		sRollbackInvalidFormatMsg_true = "selected";	
	}	

%>

<html>
<head>

<script type="text/javascript">
function validate() 
{	
	inputs = document.myForm;
	vOperationName = inputs.<%= ServiceConstants.OPERATION_NAME %>.value; 
	vOperationNameTrimmed = vOperationName.replace(/^\s+|\s+$/g, '');
	inputs.<%= ServiceConstants.OPERATION_NAME %>.value = vOperationNameTrimmed;
	vOperationName = vOperationNameTrimmed;

	if(vOperationName.length == 0)
	{
		alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME", request) %>" + " is required");
		return false;
	}
	
	for(var i = 0; i < vOperationName.length; i++)
	{
		var vASCIIValue = vOperationName.charCodeAt(i);
		
		//Numbers 0x30 ~ 0x39
		//Upper Letters 0x41 ~ 0x5a
		//_ 0x5f
		//Lower Letters 0x61 ~ 0x7a

		if(vASCIIValue < 0x30)
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x39) && (vASCIIValue < 0x41))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x5a) && (vASCIIValue < 0x5f))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x5f) && (vASCIIValue < 0x61))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if(vASCIIValue > 0x7a)
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}
	}
	return true;
}



function doAction(buttonControl)
{
	var btnName = buttonControl.name;
	if(btnName == "create")
	{
		if (!validate()) {
			return false;
		}
		var operation_name = document.getElementById('<%= ServiceConstants.OPERATION_NAME %>').value;
		document.myForm.action = "Dispatcher?apptype=loader&request=VerifyOperation&serviceid=<%= service.getServiceID() %>&serviceidkey=<%= service.getServiceIDKey() %>&operationname=" + operation_name;
		document.myForm.btnaction.value = "create"; 
		return true;
	}
	else if(btnName == "modify")
	{
		var chk_value = get_check_value();
		if (chk_value == "") {
			alert("You must select one operation name to modify the properties.");
			return false;
		}
		document.myForm.action = "Dispatcher?apptype=loader&request=CreatePropertiesJMSToTable&serviceid=<%= service.getServiceID() %>&serviceidkey=<%= service.getServiceIDKey() %>&operationname=" + chk_value;
		document.myForm.btnaction.value = "modify"; 
		return true;
	}
	else if(btnName == "remove")
	{
		if(warnOnRemove())
		{
			var chk_remove_value = get_check_value();
			if (chk_remove_value == "") {
				alert("You must select one operation name to remove the properties.");
				return false;
			}
			document.myForm.action = "Dispatcher?apptype=loader&request=RemoveOperation&serviceid=<%= service.getServiceID() %>&serviceidkey=<%= service.getServiceIDKey() %>&operationname=" + chk_remove_value;
			document.myForm.btnaction.value = "remove"; 
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		var rollbackinvfmsg = document.myForm.<%= Constants.L_UI_ROLLBACK_INVALIDF_MSG %>.value;
		document.myForm.action = "Dispatcher?apptype=loader&request=ModifyServiceDefinition&serviceid=<%= service.getServiceID() %>&serviceidkey=<%= service.getServiceIDKey() %>&rollbackinvfmsg=" + rollbackinvfmsg;
		document.myForm.btnaction.value = "modify_servicedef"; 
		return true;
	}
}

function warnOnRemove()
{
	var alertMessage = "Do you want to remove a selected operation from the service? This action will stop the service if it is running.";
	var bAnswer = confirm(alertMessage);
	if (bAnswer)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_check_value()
{
	var c_value = "";

	for (var i=0; i < document.myForm.ch_operationname.length; i++)
   	{
   		if (document.myForm.ch_operationname[i].checked)
      	{
      		c_value = document.myForm.ch_operationname[i].value;
      		break;
      	}
   	}

	if (c_value == "") {
		if (document.myForm.ch_operationname.checked) {
			c_value = document.getElementById("ch_operationname").value;
		}
	}
   	
   	return c_value;
}

function checkOnlyOne(input)
{
	var field = document.myForm.ch_operationname;
	for (i = 0; i< field.length; i++) {
		if (input.value != field[i].value) {
			field[i].checked = false;
		}
	}
}
</script>

</head>

<body>

<form name="myForm" action="" method="post">

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<tr>
			<jsp:include page="topbar.jsp"/>
			<td class="titleBox">&nbsp;<%= service.getServiceID() %> <%= ResourceBundleFactory.getString("UI_D_OPERATIONS",request) %> - <%= sAppName %></td>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<%if (sServiceType.equalsIgnoreCase(ServiceConstants.MULTIPLE)) {%>
			
			<tr><td class="whiteSpace"></td></tr>
			
			<tr>
				<td><b><%= ResourceBundleFactory.getString("UI_D_SERVICE_ATTRIBUTES",request) %>:&nbsp;</b></td>
			</tr>
			
			<tr>
				<td>
					<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="LEFT">
						<tr>
							<td class="whiteSpace" width=3></td>
							<td class="whitespace"></td>
							<td class="whitespace"></td>
							<td class="whitespace" width="70%"></td>
						</tr>
						
						<tr><td class="whiteSpace"></td></tr>
					
						<tr>
							<td class="whiteSpace"></td>	
							<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_ROLLBACK_INVALIDF_MSG",request) %>:&nbsp</td>
							<td align="left">
								<select name="<%= Constants.L_UI_ROLLBACK_INVALIDF_MSG %>" id="<%= Constants.L_UI_ROLLBACK_INVALIDF_MSG %>" style="width:175">
									 <option value="true" <%= sRollbackInvalidFormatMsg_true %>>true</option>
									 <option value="false" <%= sRollbackInvalidFormatMsg_false %>>false</option>
								</select>
							</td>
							<td class="whitespace"></td>
						</tr>
						
						<tr><td class="whiteSpace"></td></tr>
						
						<tr>
							<td class="whiteSpace"></td>	
							<td align="right"  align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_FORMAT",request) %>:&nbsp</td>
							<td align="left">
								<input readonly type="text" name="<%= Constants.L_UI_FORMAT %>" id="<%= Constants.L_UI_FORMAT %>" size="30" maxlength="30" value="<%= service.getName(ServiceConstants.MSG_FORMAT) %>"></input>
							</td>
							<td class="whiteSpace"></td>
						</tr>

						<tr><td class="whiteSpace"></td></tr>
					</table>
				</td>		
			</tr>
			
			<tr><td class="whiteSpace"></td></tr>

			<tr>
				<td>
					<input type="submit" name="modify_servicedef" value="<%= ResourceBundleFactory.getString("UI_B_MODIFY",request) %>" onclick="return doAction(this);">
				</td>
			</tr>		

			<tr><td class="whiteSpace"></td></tr>
			
			<tr>
				<jsp:include page="topbar.jsp"/>
				<jsp:include page="bottombar.jsp"/>
			</tr>
		<%} %>


		<tr><td class="whiteSpace"></td></tr>

		<tr>
			<td><b><%= ResourceBundleFactory.getString("UI_D_CREATE_OPS_NAME",request) %></b></td>
		</tr>

		<tr>
			<td>
				<input type="text" name="<%= ServiceConstants.OPERATION_NAME %>" id="<%= ServiceConstants.OPERATION_NAME %>" size="50" maxlength="64">
			</td>
		</tr>

		<tr><td class="whiteSpace"></td></tr>

		<tr>
			<td>
				<input type="submit" name="create" value="<%= ResourceBundleFactory.getString("UI_B_CREATE",request) %>" onclick="return doAction(this);">
			</td>
		</tr>		

		<tr><td class="whiteSpace"></td></tr>
		
		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>
		</tr>
		
		<tr><td class="whiteSpace"></td></tr>

	<%
		if (mapOperations.size() == 0)
		{
	%>
		<tr>
			<td>&nbsp;<%= ResourceBundleFactory.getString("UI_D_OPERATION_EMPTY",request) %></td>
		</tr>
		
	<%
		}
		else
		{
	%>

		<tr>
			<td>
				<table WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="0" ALIGN="LEFT">
					<tr>
						<td style="width:10%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"> </font>
						</td>
						<td style="width:20%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"><%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME",request) %></font>
						</td>
						<td style="width:70%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"><%= ResourceBundleFactory.getString("UI_D_ACTION_TYPE",request) %></font>
						</td>
					</tr>
					<%
					Set setNames = mapOperations.keySet();
					int i = 0;
					for (Iterator iter = setNames.iterator(); iter.hasNext(); )
					{
						String sOperationName = (String) iter.next();
						String sActionType = (String) mapOperations.get(sOperationName);    
						String rowClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";
						String chBox = (i % 2 == 0) ? "chBxOdd" : "chBxEven";
						i++;
					%>
						<tr>
							<td style="width:10%; padding-left:3px" class="<%= rowClass %>">
								<input type="checkbox" class="<%= chBox %>" name="ch_operationname" id="ch_operationname" value="<%= sOperationName %>" onclick="checkOnlyOne(this)" />
							</td>
							<td style="width:20%; padding-left:3px" class="<%= rowClass %>"><%= sOperationName %></td>
							<td style="width:70%; padding-left:3px" class="<%= rowClass %>"><%= sActionType %></td>
						</tr>
					<%
					}
					%>

				</table>
			</td>
		</tr>
	<%
		}
	%>

		<tr><td class="whiteSpace"></td></tr>

		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr><td class="whiteSpace"></td></tr>

		<tr>
			<td>
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">
					<tr>
						<td class="whiteSpace" width="5%"></td>
						<td class="whiteSpace" width="1%"></td>
						<td class="whiteSpace" width="5%"></td>
						<td class="whiteSpace" width="1%"></td>
						<td class="whiteSpace" width="88%"></td>
					</tr>

					<tr>
						<td>
							<input type="button" name="back" value="< <%= ResourceBundleFactory.getString("UI_B_BACK",request) %> " onClick="history.back()"></input>
						</td>
						<td class="whiteSpace"></td>
						<td>
							<input type="submit" name="modify" value=" <%= ResourceBundleFactory.getString("UI_B_MODIFY",request) %> " onClick="return doAction(this);"></input>
						</td>
						<td class="whiteSpace"></td>
						<td>
							<input type="submit" name="remove" value=" <%= ResourceBundleFactory.getString("UI_B_REMOVE",request) %> " onClick="return doAction(this);"></input>
						</td>
					</tr>

				</table>
			</td>
		</tr>		

		<tr><td class="whiteSpace"></td></tr>

	</table>
	
	<input type="hidden" name="serviceid" value="" size="100">
	<input type="hidden" name="serviceidkey" value="" size="100">
	<input type="hidden" name="btnaction" value="" size="100">
</form>
</body>
</html>