<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<%@ page import='com.teradata.tjmsAdapter.util.Constants'%>
<%@ page import='com.teradata.tjmsAdapter.Service'%>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants'%>
<%@ page import='java.util.List'%>
<%@ page import='java.util.Map'%>

<% 
	List listServices = null;

	String sAppType = (String) request.getAttribute(ServiceConstants.APP_TYPE);
	String sAppName = "";
	String sServletName = "";
	if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
	{
		sAppName = ResourceBundleFactory.getString("UI_D_LOADER",request);
	}
	else
	{
		sAppName = ResourceBundleFactory.getString("UI_D_ROUTER",request);
		sServletName = Constants.R_SVL_CR_P_QTABLE_TO_JMS;
	}
	listServices = (List) request.getAttribute(Constants.UI_LIST_SERVICES); 
%>

<html>
<head>

<script type="text/javascript">
</script>

</head>

<body>

<form name="myForm" method="post">

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<tr>
			<jsp:include page="topbar.jsp"/>
			<td class="titleBox">&nbsp;<%= ResourceBundleFactory.getString("UI_D_MANAGE_PROPS",request) %> - <%= sAppName %></td>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr><td class="whiteSpace"></td></tr>

	<%
		if (listServices.size() == 0)
		{
	%>
		<tr>
			<td>&nbsp;<%= ResourceBundleFactory.getString("UI_D_SERVICE_EMPTY",request) %></td>
		</tr>
	<%
		}
		else
		{
	%>

		<tr>
			<td>
				<table WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="0" ALIGN="LEFT">
					<tr>
						<td style="width:10%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"> </font>
						</td>
						<%if (sAppType.equals(ServiceConstants.APP_TYPE_LOADER)) {%>
							<td style="width:20%; padding-left:3px" ALIGN="LEFT" class="titleBox">
								<font class="header"><%= ResourceBundleFactory.getString("UI_D_SERVICE_ID",request) %></font>
							</td>
							<td style="width:70%; padding-left:3px" ALIGN="LEFT" class="titleBox">
								<font class="header"><%= ResourceBundleFactory.getString("UI_D_SERVICE_TYPE",request) %></font>
							</td>
						<%} else { %>
							<td style="width:90%; padding-left:3px" ALIGN="LEFT" class="titleBox">
								<font class="header"><%= ResourceBundleFactory.getString("UI_D_SERVICE_ID",request) %></font>
							</td>
						<%} %>						
					</tr>
					<%
					for (int i = 0; i < listServices.size(); i++)
					{
						Service service = (Service) listServices.get(i);
						String sServiceID = service.getServiceID();
						String sServiceIDKey = service.getServiceIDKey();
						String sServiceType = service.getName(ServiceConstants.SERVICE_TYPE);
						if (sServiceType == null)
							sServiceType = ServiceConstants.SINGLE;
						String rowClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";
					%>
						<tr>
						<%
						if (sAppType.equals(ServiceConstants.APP_TYPE_LOADER)) {
							if (sServiceType.equals(ServiceConstants.MULTIPLE))
								sServletName = Constants.L_SVL_OPRATIONS_LIST;
							else
								sServletName = Constants.L_SVL_CR_P_JMS_TO_TABLE;
						%>							
							<td style="width:10%; padding-left:3px" class="<%= rowClass %>">
								<a class="metadata" href="Dispatcher?apptype=<%= sAppType %>&request=<%= sServletName %>&<%= ServiceConstants.SERVICE_ID %>=<%= sServiceID %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><%= ResourceBundleFactory.getString("UI_D_EDIT",request) %></a>
							</td>
							<td style="width:20%; padding-left:3px" class="<%= rowClass %>"><%= sServiceID %></td>
							<td style="width:70%; padding-left:3px" class="<%= rowClass %>"><%= sServiceType %></td>
						<%} else {%>
							<td style="width:10%; padding-left:3px" class="<%= rowClass %>">
								<a class="metadata" href="Dispatcher?apptype=<%= sAppType %>&request=<%= sServletName %>&<%= ServiceConstants.SERVICE_ID %>=<%= sServiceID %>&<%= ServiceConstants.SERVICE_ID_KEY %>=<%= sServiceIDKey %>"><%= ResourceBundleFactory.getString("UI_D_EDIT",request) %></a>
							</td>
							<td style="width:90%; padding-left:3px" class="<%= rowClass %>"><%= sServiceID %></td>
						<%} %>
						</tr>
					<%
					}
					%>

				</table>
			</td>
		</tr>
	<%
		}
	%>

		<tr><td class="whiteSpace"></td></tr>

	</table>
</form>
</body>
</html>