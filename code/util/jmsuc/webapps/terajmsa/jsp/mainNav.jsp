<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants'%>


<table cellspacing="0" cellpadding=0 width="100%" height="100%" border="0">
	<tr>
	<td height=1 class="background_white"></td>
	</tr>

	<tr class="dark_blue_background">
		<td height=30 class="white_bold_text">&nbsp;&nbsp;&nbsp;</td>
	</tr>

	<tr>
	<td height=1 class="left_nav_title_2"></td>
	</tr>

	<tr>
	<td height=1 class="left_nav_title_3"></td>
	</tr>

	<tr height="100%" valign="top">
	<td height="100%" nowrap="nowrap">
		<br>

		&nbsp;<font class="navLink"><%= ResourceBundleFactory.getString("UI_D_LOADER",request) %></font><br>
		<a href="Dispatcher?apptype=loader&request=performsrvact&<%= ServiceConstants.SERVICE_ID %>=*&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.DISPLAY_ALL %>">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_DISPLAY_SERV",request) %></a><br>
		<a href="Dispatcher?apptype=loader&request=createService">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_CREATE_SERV",request) %></a><br>
		<a href="Dispatcher?apptype=loader&request=copyService">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_COPY_SERV",request) %></a><br>
		<a href="Dispatcher?apptype=loader&request=PropertiesList">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_MANAGE_PROPS",request) %></a><br>

		<br>&nbsp;<font class="navLink"><%= ResourceBundleFactory.getString("UI_D_ROUTER",request) %></font><br>
		<a href="Dispatcher?apptype=extractor&request=performsrvact&<%= ServiceConstants.SERVICE_ID %>=*&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.DISPLAY_ALL %>">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_DISPLAY_SERV",request) %></a><br>
		<a href="Dispatcher?apptype=extractor&request=createService">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_CREATE_SERV",request) %></a><br>
		<a href="Dispatcher?apptype=extractor&request=copyService">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_COPY_SERV",request) %></a><br>
		<a href="Dispatcher?apptype=extractor&request=PropertiesList">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_MANAGE_PROPS",request) %></a><br>

		<br>&nbsp;<font class="navLink"><%= ResourceBundleFactory.getString("UI_D_APP_SUBTITLE_ADMIN",request) %></font><br>
		<a href="Dispatcher?request=GetEnvVars">&nbsp;&nbsp;<%= ResourceBundleFactory.getString("UI_D_ENV_VARS",request) %></a><br>

	</td>
	</tr>
	
</table>