<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory' %>
<%@ page import='com.teradata.tjmsl.util.Constants' %>
<%@ page import='com.teradata.tjmsAdapter.model.Field' %>
<%@ page import='com.teradata.tjmsAdapter.model.Properties_Router' %>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants'%>
<%@ page import='java.util.Map' %>
<%@ page import='java.util.Set'%>
<%@ page import='java.util.List'%>
<%@ page import='java.util.Iterator'%>

<% 
	String sAppType = (String) request.getAttribute(ServiceConstants.APP_TYPE);
	String sAppName = "";
	if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
	{
		sAppName = ResourceBundleFactory.getString("UI_D_LOADER",request);
	}
	else
	{
		sAppName = ResourceBundleFactory.getString("UI_D_ROUTER",request);
	}

	Properties_Router properties_router = (Properties_Router) request.getAttribute(Constants.R_UI_PROPERTIES);
	String sServiceID			= properties_router.getServiceID();
	String sServiceIDKey		= properties_router.getServiceIDKey();
	String sTableName			= properties_router.getService().getName(ServiceConstants.SOURCE_NAME);
	String sSourceType			= properties_router.getService().getName(ServiceConstants.SOURCE_TYPE);
	String sTargetType			= properties_router.getService().getName(ServiceConstants.TARGET_TYPE);
	String sDeliveryMode		= properties_router.getName(Constants.PROPERTIES_INFO, Constants.R_PKN_DELIVERYMODE);
	String sPriority			= properties_router.getName(Constants.PROPERTIES_INFO, Constants.R_PKN_PRIORITY);
	String sTimeToLive			= properties_router.getName(Constants.PROPERTIES_INFO, Constants.R_PKN_TIMETOLIVE);
	String sRowsPerMessage		= properties_router.getName(Constants.PROPERTIES_INFO, Constants.R_PKN_ROWSPERMESSAGE);
	String sXml 				= properties_router.getName(Constants.PROPERTIES_INFO, Constants.R_PKN_XML);
	String sXmlNameSpace		= properties_router.getName(Constants.PROPERTIES_INFO, Constants.R_PKN_XML_NAME_SPACE);

	if(sServiceID == null)		sServiceID = "";
	if(sTableName == null)		sTableName = "";
	if(sSourceType == null)		sSourceType = "";
	if(sTargetType == null)		sTargetType = "";
	if(sDeliveryMode == null)	sDeliveryMode = "2";
	if(sPriority == null)		sPriority = "4";
	if(sTimeToLive == null)		sTimeToLive = "0";
	if(sRowsPerMessage == null)	sRowsPerMessage = "1";
	if(sXml == null)			sXml = "false";
	if(sXmlNameSpace == null)	sXmlNameSpace = "";

	Map mapMessageProperties = properties_router.getMap(Constants.JMS_PROPERTY);
	Map mapTableInfo = properties_router.getMap(Constants.TABLE_INFO);
	Set setMessageProperties		= mapMessageProperties.keySet();
	Iterator iterMessageProperties	= setMessageProperties.iterator();
	Set setTableInfo				= mapTableInfo.keySet();
	Iterator iterSource				= setTableInfo.iterator();
	Iterator iterTarget				= setTableInfo.iterator();

	List listAllCols = (List) request.getAttribute (Constants.L_UI_LIST_FIELDS);
	String sListSize = "";
	if(listAllCols.size() > 1)
	{
		sListSize = String.valueOf(listAllCols.size());
	}
	else
	{	
		sListSize = "2";
	}

	String sXMLNotUseSelected = "";
	String sXMLUseSelected = "";
	if(sXml.equalsIgnoreCase("false"))
	{
		sXMLNotUseSelected = "selected";
	}
	else
	{
		sXMLUseSelected = "selected";
	}
	
%>

<html>
<head>

<script type="text/javascript">
function load()
{
	// Reading check for JMS Headers
	verifyJMSHeaders('<%= sDeliveryMode %>', "Delivery Mode");
	verifyJMSHeaders('<%= sPriority %>', "Priority");
	verifyJMSHeaders('<%= sTimeToLive %>', "Time To Live");
	verifyJMSHeaders('<%= sRowsPerMessage %>', "Rows per Message");
}

function validate()
{
	// Enforce at least one column selected when user clicks "Modify" button
	var listTarget = document.getElementById("listTarget");
	var listTargetAlert = "Please choose at least one column in the Selected Columns List";

	if (listTarget.options.length == 0)
	{
		alert(listTargetAlert);
		return false;
	}	

	// Saving check for JMS Properties
	var tbl = document.getElementById('JMSPropertiesTable');
	var rows = tbl.getElementsByTagName('tr');
	var maxRows = rows.length;
	for(var i = 4; i < maxRows; i++)
	{
		var Name = 'JMSPropName' + i;
		var JMSPropName = document.getElementById(Name).value;

		var JMSPropNameTrimmed = JMSPropName.replace(/^\s+|\s+$/g, '');
		document.getElementById(Name).value = JMSPropNameTrimmed;
		JMSPropName = JMSPropNameTrimmed;

		var Value = 'JMSPropValue' + i;
		var JMSPropValue = document.getElementById(Value);
		
		if(!verifyJMSProps(JMSPropName))
		{
			alert("The input parameter " + "JMS Properties Name should be a combination of upper and lower case letters, numbers and _");
			return false;
		}
	}

	// For JMS Headers
	var vDeliveryMode = document.getElementById('<%= Constants.R_UI_DELIVERYMODE %>').value;
	if(!verifyJMSHeaders(vDeliveryMode, "Delivery Mode"))
	{
		return false;
	}

	var vPriority = document.getElementById('<%= Constants.R_UI_PRIORITY %>').value;
	if(!verifyJMSHeaders(vPriority, "Priority"))
	{
		return false;
	}

	var vTimeToLive = document.getElementById('<%= Constants.R_UI_TIMETOLIVE %>').value;
	var vTimeToLiveTrimmed = vTimeToLive.replace(/^\s+|\s+$/g, '');
	document.getElementById('<%= Constants.R_UI_TIMETOLIVE %>').value = vTimeToLiveTrimmed;
	vTimeToLive = vTimeToLiveTrimmed;
	if(!verifyJMSHeaders(vTimeToLive, "Time To Live"))
	{
		return false;
	}

	var vRowsPerMessage = document.getElementById('<%= Constants.R_UI_ROWSPERMESSAGE %>').value;
	var vRowsPerMessageTrimmed = vRowsPerMessage.replace(/^\s+|\s+$/g, '');
	document.getElementById('<%= Constants.R_UI_ROWSPERMESSAGE %>').value = vRowsPerMessageTrimmed;
	vRowsPerMessage = vRowsPerMessageTrimmed;
	if(!verifyJMSHeaders(vRowsPerMessage, "Rows per Message"))
	{
		return false;
	}

}

function verifyJMSHeaders(Header, Name)
{
	var vHeader = Header;
	var vName = Name;

	if(vHeader.length == 0)
	{
		alert("The input parameter " + vName + " is required");
		return false;
	}

	if(vName == "Delivery Mode")
	{
		// 1 or 2
		var vASCIIValue = vHeader.charCodeAt(0);
		if((vASCIIValue < 0x31) || (vASCIIValue > 0x32))
		{
			alert("The input parameter " + vName + " must be 1 or 2.");
			return false;
		}
		if((vHeader < 1) || (vHeader > 2))
		{
			alert("The input parameter " + vName + " must be 1 or 2.");
			return false;
		}
	}

	if(vName == "Priority")
	{
		// 0 - 9
		var vASCIIValue = vHeader.charCodeAt(0);
		if((vASCIIValue < 0x30) || (vASCIIValue > 0x39))
		{
			alert("The input parameter " + vName + " must be 0 - 9.");
			return false;
		}
		if((vHeader < 0) || (vHeader > 9))
		{
			alert("The input parameter " + vName + " must be 0 - 9.");
			return false;
		}
	}

	if(vName == "Time To Live")
	{
		for(var i = 0; i < vHeader.length; i++)
		{
			var vASCIIValue = vHeader.charCodeAt(i);
			if((vASCIIValue < 0x30) || (vASCIIValue > 0x39))
			{
				alert("The input parameter " + vName + " must be 0 - 1440.");
				return false;
			}
		}

		// 0 - 1440
		if((vHeader < 0) || (vHeader > 1440))
		{
			alert("The input parameter " + vName + " must be 0 - 1440.");
			return false;
		}
		var vTimeToLiveValue = new Number(vHeader);
		var vTimeToLive = document.getElementById('<%= Constants.R_UI_TIMETOLIVE %>');
		vTimeToLive.value = vTimeToLiveValue;
	}

	if(vName == "Rows per Message")
	{
		for(var i = 0; i < vHeader.length; i++)
		{
			var vASCIIValue = vHeader.charCodeAt(i);
			if((vASCIIValue < 0x30) || (vASCIIValue > 0x39))
			{
				alert("The input parameter " + vName + " must be 1 - 9999.");
				return false;
			}
		}

		// 1 - 9999
		if((vHeader < 1) || (vHeader > 9999))
		{
			alert("The input parameter " + vName + " must be 1 - 9999.");
			return false;
		}
		var vRowsPerMessageValue = new Number(vHeader);
		var vRowsPerMessage = document.getElementById('<%= Constants.R_UI_ROWSPERMESSAGE %>');
		vRowsPerMessage.value = vRowsPerMessageValue;
	}
	return true;
}

function verifyJMSProps(Prop)
{
	var vProp = Prop;

	for(var i = 0; i < vProp.length; i++)
	{
		var vASCIIValue = vProp.charCodeAt(i);
		
		//Numbers 0x30 ~ 0x39
		//Upper Letters 0x41 ~ 0x5a
		//_ 0x5f
		//Lower Letters 0x61 ~ 0x7a

		if(vASCIIValue < 0x30)
		{
			return false;
		}

		if((vASCIIValue > 0x39) && (vASCIIValue < 0x41))
		{
			return false;
		}

		if((vASCIIValue > 0x5a) && (vASCIIValue < 0x5f))
		{
			return false;
		}

		if((vASCIIValue > 0x5f) && (vASCIIValue < 0x61))
		{
			return false;
		}

		if(vASCIIValue > 0x7a)
		{
			return false;
		}
	}
	return true;
}

function doSelection(Action, listSource, listTarget)
{
	var lstSource = listSource;
	var lstTarget = listTarget;
	if(lstSource.selectedIndex == -1)
	{
		switch(Action)
		{
			case 'Add':
			{
				alert('Please select at least one record to Add');
				break;
			}
			case 'Remove':
			{
				alert('Please select at least one record to Remove');
				break;
			}
			return;
		}
	}
	else
	{
		var arraySelected = new Array();
		var j = 0;
		var numberOfItems = listSource.options.length;
		for(var i = 0; i < numberOfItems; i++)
		{
			if(listSource.options[i].selected == true)
			{
				var selIndex = i;
				var optionText = lstSource.options[selIndex].text;
				var optionVal = lstSource.options[selIndex].value;
				var optionIndex = lstTarget.options.length;
				var objOption = new Option(optionText, optionVal);
				lstTarget.options[optionIndex] = objOption;
				arraySelected[j] = optionText;
				j++;
			}
		}
		
		for(var i = 0; i < arraySelected.length; i++)
		{
			for(var j = 0; j < listSource.options.length; j++)
			{
				if(arraySelected[i] == listSource.options[j].text)
				{
					listSource.options[j] = null;
				}
			}
		}		

		orderElement(listTarget);
		saveSelections();
	}
}

function orderElement(listTarget)
{
	var arrayAllCols = new Array();

	<%	
	Iterator iterOriginal = setTableInfo.iterator();
	for(int i = 0; iterOriginal.hasNext(); i++) 
	{ 
		String sColumnName = (String) iterOriginal.next();
	%>
		arrayAllCols[<%= i %>] = '<%= sColumnName %>';
	<% 
	} 
	%>
	
	var top = 0;
	for(var i = 0; i < arrayAllCols.length; i++)
	{
		for(var j = 0; j < listTarget.options.length ; j++)
		{
			if (arrayAllCols[i] == listTarget.options[j].value)
			{
				var tempVal =  listTarget.options[top].value;
				var tempText =  listTarget.options[top].text;

				listTarget.options [top].value = listTarget.options [j].value;
				listTarget.options [top].text = listTarget.options [j].text;

				listTarget.options [j].value = tempVal;
				listTarget.options [j].text = tempText;
				top = top + 1;
			}
		}
	}
}

function saveSelections()
{
	var listTarget = document.getElementById('listTarget');
	var selectedcolumnnames = document.getElementById('<%= Constants.R_UI_SELECTEDCOLNAMES %>');
	var sListTarget = '';

	for(var i = 0; i < listTarget.options.length ; i++)
	{
		sListTarget = sListTarget + listTarget.options[i].value + '&';
	}

	selectedcolumnnames.value = sListTarget;
}


function addRow(pTableName, pName, pValue)
{ 
	var tbl = document.getElementById(pTableName);//to identify the table in which the row will get insert 
	var rows = tbl.getElementsByTagName('tr');
	var curRows = rows.length;

	try 
	{
		var textName = 'JMSPropName' + curRows;
		var textValue = 'JMSPropValue' + curRows;

		var textboxName='<input type="text" name=\''+textName+'\' value=\''+pName+'\' size="30" maxlength="100">';//for text box 
		var textboxValue='<input type="text" name=\''+textValue+'\' value=\''+pValue+'\' size="30" maxlength="100">';//for text box 
		var remove= '<a href="#" onclick="removeRow(\''+pTableName+'\', this)"/><%= ResourceBundleFactory.getString("UI_B_REMOVE", request) %></a>';    
		 
		var newRow = tbl.insertRow(curRows);//creation of new row 

		var newCell = newRow.insertCell(0);
		newCell.innerHTML = '';

		var newCell = newRow.insertCell(1);
		newCell.innerHTML = textboxName;

		var newCell = newRow.insertCell(2);
		newCell.innerHTML = textboxValue;

		var newCell = newRow.insertCell(3);
		newCell.innerHTML = remove;

		var newCell = newRow.insertCell(4);
		newCell.innerHTML = '';

	} 
	catch (ex) 
	{ 
		alert(ex);
	} 
        
} 

function removeRow(tableName, row)
{
	var i = row.parentNode.parentNode.rowIndex;
	document.getElementById(tableName).deleteRow(i);
	document.getElementById(tableName).focus();
}

</script>
</head>
<body onload="load()">

<form name="propertiesForm" action="Dispatcher?apptype=<%= sAppType %>&request=ModifyProperties" method="post" onSubmit="return validate();">

	<input type="hidden" name="<%= ServiceConstants.SERVICE_ID %>" value="<%= sServiceID %>" size="100">
	<input type="hidden" name="<%= ServiceConstants.SERVICE_ID_KEY %>" value="<%= sServiceIDKey %>" size="100">
	<input type="hidden" name="<%= ServiceConstants.SOURCE_TYPE %>" value="<%= sSourceType %>" size="100">
	<input type="hidden" name="<%= ServiceConstants.TARGET_TYPE %>" value="<%= sTargetType %>" size="100">

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<tr>
			<jsp:include page="topbar.jsp"/>
			<td class="titleBox">&nbsp;<%= sServiceID %> <%= ResourceBundleFactory.getString("UI_D_PROPS",request) %> - <%= sAppName %></td>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr>
			<td class="whiteSpace"></td>
		</tr>
		
		<tr>
			<td>

				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">
					<tr>
						<td class="whiteSpace" width=1></td>

						<td valign="top" width="60%">

							<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="TOP">

								<tr align="center" valign="middle">
									<td align="left" width="45%" style="font-weight: bold" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_SOURCE_COLUMNS",request) %>:</td>
									<td class="whitespace" width="10%"></td>
									<td align="left" width="45%" style="font-weight: bold" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_SELECTED_COLUMNS",request) %>:</td>
								</tr>

								<tr>
									<td class="whiteSpace"></td>
								</tr>

								<tr>
									<td valign="top">
										<select size="<%= sListSize %>" id="listSource" name="listSource" style="Width: 100%" multiple="multiple">
											<%
											while(iterSource.hasNext())
											{
												String sColumnName = (String) iterSource.next();
												String sSelected = (String) mapTableInfo.get(sColumnName);
												if(sSelected.equals("0"))
												{
											%>
													<option value="<%= sColumnName %>">&nbsp;<%= sColumnName %> </option>
											<%
												}
											}
											%>
										</select>
									</td>

									<td align="center">
										<table>
											<tr>
												<input type="button" name="Select" value=" -> " onClick="doSelection('Add', this.form.listSource, this.form.listTarget)">
											</tr>
											<tr>
												<input type="button" name="Delete" value=" <- " onClick="doSelection('Remove', this.form.listTarget, this.form.listSource)">
											</tr>
										</table>
									</td>

									<td valign="top">
										<select size="<%= sListSize %>" id="listTarget" name="listTarget" style="Width: 100%" multiple="multiple">
											<%
											String sListTarget = "";
											while(iterTarget.hasNext())
											{
												String sColumnName = (String) iterTarget.next();
												String sSelected = (String) mapTableInfo.get(sColumnName);
												if(sSelected.equals("1"))
												{
													sListTarget = sListTarget + sColumnName + "&";
											%>
													<option value="<%= sColumnName %>">&nbsp;<%= sColumnName %> </option>
											<%
												}
											}
											%>
										</select>
										<input type="hidden" id="<%= Constants.R_UI_SELECTEDCOLNAMES %>" name="<%= Constants.R_UI_SELECTEDCOLNAMES %>" value="<%= sListTarget %>" size="1000">
									</td>
								</tr>	
								
								<tr align="center" valign="middle">
									<td class="whitespace" width="45%" nowrap="nowrap"></td>
									<td class="whitespace" width="10%"></td>
									<td class="whitespace" width="45%" nowrap="nowrap"></td>
								</tr>

							</table>

						</td>
						
						<td class="whiteSpace" width=1></td>

						<td valign="top" width="38%">	
							<table BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="LEFT">

								<tr valign="top">
									<td align="left" width="100%" style="font-weight: bold" nowrap="nowrap">
										<b>
											<%= ResourceBundleFactory.getString("UI_D_TABLE_NAME",request) %>: &nbsp;<%= sTableName %>
										</b>
									</td>
								</tr>

								<tr><td class="whiteSpace"></td></tr>

								<tr valign="top">
									<td>
										<table WIDTH="100%" BORDER="1" BORDERCOLOR="#e3e7ea" CELLSPACING="0" CELLPADDING="0" ALIGN="left">	
											<tr valign="top">
												<td>
													<table WIDTH="100%" BORDER="0" CELLSPACING="2" CELLPADDING="2" ALIGN="left">
														<%
														for(int i=0; i < listAllCols.size(); i++)
														{
															Field field = (Field) listAllCols.get(i);
															String sColumnName = field.getName();
															String sColumnType = field.getType();
															
															String rowClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";
															%>												

														<tr>
															<td class="<%= rowClass %>" width="50%" nowrap="nowrap">
															<%
															if(field.isPI())
															{
															%>												
																<font class="pi"><%= sColumnName %></font>
															<%
															}
															else
															{
															%>
																<%= sColumnName %>
															<%
															}
															%>
															</td>

															<td class="<%= rowClass %>" width="50%" nowrap="nowrap">
															<%
															if(field.isPI())
															{
															%>												
																<font class="pi"><%= sColumnType %></font>
															<%
															}
															else
															{
															%>
																<%= sColumnType %>
															<%
															}
															%>
															</td>
														</tr>
														<%
														}
														%>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr><td class="whiteSpace"></td></tr>

								<tr valign="top">
									<td align="left" width="100%" nowrap="nowrap">
										<b><%= ResourceBundleFactory.getString("UI_D_PRIMARY_IDX1",request) %><font class="pi"> <%= ResourceBundleFactory.getString("UI_D_PRIMARY_IDX2",request) %></font> <%= ResourceBundleFactory.getString("UI_D_PRIMARY_IDX3",request) %></b>
									</td>
								</tr>

								<tr><td class="whiteSpace"></td></tr>

							</table>
						</td>
					
					</tr>

					<tr><td class="whiteSpace"></td></tr>

				</table>
			</td>
		</tr>
		
		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr>
			<td>
				<table cellspacing=2 cellpadding=2 width="100%" border=0>
						
							<tr>
								<td class="whiteSpace" width=8></td>
								<td class="whitespace"></td>
								<td class="whitespace"></td>
								<td class="whitespace" width="50%"></td>
							</tr>

							<tr>
								<td class="whiteSpace"></td>
								<td align="left" width="20%" nowrap="nowrap" style="font-weight: bold"><%= ResourceBundleFactory.getString("UI_D_MESSAGE_ATTRIBUTES",request) %>:&nbsp;</td>
								<td class="whitespace"></td>
								<td class="whitespace"></td>
							</tr>

							<tr><td class="whiteSpace"></td></tr>
							
							<tr>
								<td class="whiteSpace"></td>	
								<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_COLUMN_CONTAINS_XML",request) %>:&nbsp</td>
								<td align="left"><select name="<%= Constants.R_UI_XML %>" id="usexml" style="width:100">
													 <option value="false" <%= sXMLNotUseSelected %>>false</option>
													 <option value="true" <%= sXMLUseSelected %>>true</option>
												 </select>
								</td>
								<td class="whitespace"></td>
							</tr>

							<tr>	
								<td class="whiteSpace"></td>		
								<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_XML_NAME_SPACE",request) %>:&nbsp;</td>
								<td align="left">
									<input type="text" name="<%= Constants.R_UI_XML_NAME_SPACE %>" value="<%= sXmlNameSpace %>" size="45" maxlength="512">
								</td>
								<td class="whitespace"></td>
							</tr>
							
							<tr>	
								<td class="whiteSpace"></td>		
								<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_DELIVERY_MODE",request) %>:&nbsp;</td>
								<td align="left">
									<input type="text" name="<%= Constants.R_UI_DELIVERYMODE %>" value="<%= sDeliveryMode %>" size="4" maxlength="1">
									<font class="small">(<%= ResourceBundleFactory.getString("UI_D_DELIVERY_MODE_RANGE",request) %>)</font>
								</td>
								<td class="whitespace"></td>
							</tr>

							<tr>
								<td class="whiteSpace"></td>			
								<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_PRIORITY",request) %>:&nbsp;</td>
								<td align="left">
									<input type="text" name="<%= Constants.R_UI_PRIORITY %>" value="<%= sPriority %>" size="4" maxlength="1">
									<font class="small">(<%= ResourceBundleFactory.getString("UI_D_PRIORITY_RANGE",request) %>)</font>
								</td>
								<td class="whitespace"></td>
							</tr>

							<tr>	
								<td class="whiteSpace"></td>		
								<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_TIME_TO_LIVE",request) %>:&nbsp;</td>
								<td align="left">
									<input type="text" name="<%= Constants.R_UI_TIMETOLIVE %>" value="<%= sTimeToLive %>" size="4" maxlength="4">
									<font class="small">(<%= ResourceBundleFactory.getString("UI_D_TIME_TO_LIVE_RANGE",request) %>)</font>
								</td>
								<td class="whitespace"></td>
							</tr>

							<tr>		
								<td class="whiteSpace"></td>	
								<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_ROWS_PER_MESSAGE",request) %>:&nbsp;</td>
								<td align="left">
									<input type="text" name="<%= Constants.R_UI_ROWSPERMESSAGE %>" value="<%= sRowsPerMessage %>" size="4" maxlength="4">
									<font class="small">(<%= ResourceBundleFactory.getString("UI_D_ROWS_PER_MESSAGE_RANGE",request) %>)</font>
								</td>
								<td class="whitespace"></td>
							</tr>

							<tr><td class="whiteSpace"></td></tr>
				</table>
			</td>
		</tr>

		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>

		</tr>

		<tr>
			<td>
				<table ID="JMSPropertiesTable" WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">

					<tr>
						<td class="whiteSpace" width=8></td>
						<td class="whiteSpace" width="25%"></td>
						<td class="whiteSpace" width="25%"></td>
						<td class="whiteSpace" width="10"></td>
						<td class="whiteSpace" width="20%"></td>
					</tr>

					<tr>
						<td class="whiteSpace"></td>
						<td align="left" nowrap="nowrap" style="font-weight: bold"><%= ResourceBundleFactory.getString("UI_D_MESSAGE_PROPERTIES",request) %>:&nbsp;</td>
						<td class="whiteSpace"></td>
						<td class="whiteSpace"></td>
						<td class="whiteSpace"></td>
					</tr>

					<tr>
						<td class="whiteSpace"></td>
						<td class="whiteSpace"></td>
						<td class="whiteSpace"></td>
						<td class="whiteSpace"></td>
						<td class="whiteSpace"></td>
					</tr>

					<tr>
						<td class="whiteSpace"></td>
						<td>Name</td>
						<td>Value</td>
						<td align="left">
							<input type="button" name="Button" value="Add Property" onClick="addRow('JMSPropertiesTable', '', '')">
						</td>
						<td class="whiteSpace"></td>
					</tr>

				</table>

			</td>
		</tr>
		
		<tr><td class="whiteSpace"></td></tr>

		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>

		</tr>

		<tr>
			<td>
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">
					<tr>
						<td class="whiteSpace" width=4></td>
						<td class="whiteSpace" width="5%"></td>
						<td class="whiteSpace" width=1></td>
						<td class="whiteSpace" width="5%"></td>
						<td class="whiteSpace" width="87%"></td>
					</tr>

					<tr>
						<td class="whiteSpace"></td>
						<td>
							<input type="button" name="Button" value="< <%= ResourceBundleFactory.getString("UI_B_BACK",request) %> " onClick="history.back()"></input>
						</td>
						<td class="whiteSpace"></td>
						<td>
							<input type="submit" name="Button" value=" <%= ResourceBundleFactory.getString("UI_B_MODIFY",request) %> "></input>
						</td>
						<td class="whiteSpace"></td>
					</tr>

				</table>
			</td>
		</tr>

	</table>

</form>

<%
if(iterMessageProperties != null)
{
	while(iterMessageProperties.hasNext())
	{
		String sName = (String) iterMessageProperties.next();
		String sValue = (String) mapMessageProperties.get(sName);
	%>
		<script type="text/javascript">addRow('JMSPropertiesTable', '<%= sName %>', '<%= sValue %>')</script>
	<%
	}
}
%>

</body>
</html>