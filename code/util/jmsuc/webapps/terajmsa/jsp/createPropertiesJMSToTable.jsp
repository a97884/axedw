<%@ page import='com.teradata.tjmsAdapter.model.Properties' %>
<%@ page import='com.teradata.tjmsAdapter.model.Field' %>
<%@ page import='com.teradata.tjmsAdapter.model.Properties' %>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants' %>
<%@ page import='com.teradata.tjmsAdapter.Service' %>
<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory' %>
<%@ page import='com.teradata.tjmsl.util.Constants' %>
<%@ page import='java.util.List' %>
<%@ page import='java.util.Iterator' %>

<%	
	String sAppType = (String) request.getAttribute(ServiceConstants.APP_TYPE);
	String sAppName = "";
	if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
	{
		sAppName = ResourceBundleFactory.getString("UI_D_LOADER",request);
	}
	else
	{
		sAppName = ResourceBundleFactory.getString("UI_D_ROUTER",request);
	}

	Properties props = (Properties) request.getAttribute (Constants.L_UI_MESSAGE_PROPERTIES);
	List listAllCols = (List) request.getAttribute (Constants.L_UI_LIST_FIELDS);
	String sListSize = "";
	if(listAllCols.size() > 1)
	{
		sListSize = String.valueOf(listAllCols.size());
	}
	else
	{	
		sListSize = "2";
	}

	Service service = props.getService();
	String sServiceID = props.getServiceId(); 
	String sServiceIDKey = props.getServiceIdKey();
	String sServiceType = service.getName(ServiceConstants.SERVICE_TYPE);
	if (sServiceType == null)
		sServiceType = ServiceConstants.SINGLE;
	
	String sSelectDisabled = sServiceType.equalsIgnoreCase(ServiceConstants.SINGLE) ? "" : "disabled";
	String sTextReadOnly = "readonly";
	String sOperationName = (String) request.getAttribute (ServiceConstants.OPERATION_NAME);
	
	String sDataSource = service.getName(ServiceConstants.TARGET_CONNECTION_NAME);
	String sDml = props.getProperty (Properties.sc_sDML);
	String sTableName = service.getName(ServiceConstants.TARGET_NAME);
	String sFullyQualifiedtable = props.getFullyQualifiedTableName ("", sTableName);
	String sUpdateExp = props.getProperty(Properties.sc_sUPDATE_EXP);
	String sWhereExp = props.getProperty(Properties.sc_sWHERE_EXP);
	String sInsertExp = props.getProperty(Properties.sc_sINSERT_EXP);
	String colListHeadingValue1 = ResourceBundleFactory.getString("UI_D_CHOOSE_COLS_UPDATE",request);
	String colListHeadingValue2 = ResourceBundleFactory.getString("UI_D_CHOOSE_COLS_WHERE",request);
	String colListHeadingValue3 = ResourceBundleFactory.getString("UI_D_CHOOSE_COLS_INSERT",request);
	String saColHeadings [] = {colListHeadingValue1, colListHeadingValue2, colListHeadingValue3};
	String saListBoxId [] = {"update", "where", "insert"};
	
	String sFormatVariable = "";
	String sFormatFixed = "";
	String sFormatXml = "";
	//if(props.getProperty(Properties.sc_sFORMAT).equalsIgnoreCase("VARIABLE"))
	if(service.getName(ServiceConstants.MSG_FORMAT).equalsIgnoreCase(ServiceConstants.MSG_FORMAT_VARIABLE))
	{
		sFormatVariable = "selected";
	}
	//else if(props.getProperty(Properties.sc_sFORMAT).equalsIgnoreCase("FIXED"))
	else if(service.getName(ServiceConstants.MSG_FORMAT).equalsIgnoreCase(ServiceConstants.MSG_FORMAT_FIXED))
	{
		sFormatFixed = "selected";
	}
	else
	{
		sFormatXml = "selected";
	}
	
	String sRollbackInvalidFormatMsg_true = "";
	String sRollbackInvalidFormatMsg_false = "";
	//if(props.getProperty(Properties.sc_sROLLBACK_INVF_MSG).equalsIgnoreCase("") || 
	//		props.getProperty(Properties.sc_sROLLBACK_INVF_MSG).equalsIgnoreCase("false"))
	if(service.getName(ServiceConstants.IS_ROLLBACK_INVF_MSG).equalsIgnoreCase("") || 
			service.getName(ServiceConstants.IS_ROLLBACK_INVF_MSG).equalsIgnoreCase("false"))		
	{
		sRollbackInvalidFormatMsg_false = "selected";
	}
	else
	{
		sRollbackInvalidFormatMsg_true = "selected";	
	}

	String sDMLInsert = "";
	String sDMLUpdate = "";
	String sDMLDelete = "";
	String sDMLUpsert = "";
	if(sDml.equalsIgnoreCase("INSERT"))
	{
		sDMLInsert = "selected";
	}
	else if(sDml.equalsIgnoreCase("UPDATE"))
	{
		sDMLUpdate = "selected";
	}
	else if(sDml.equalsIgnoreCase("DELETE"))
	{
		sDMLDelete = "selected";
	}
	else
	{
		sDMLUpsert = "selected";
	}
%>


<script language="JavaScript" TYPE="text/javascript">

function formatChange(selectedFormat)
{
	var selectedFormat = document.getElementById(selectedFormat).value;

	if (selectedFormat != "VARIABLE")
	{
		document.getElementById("delimTitle").style.display = "none";
		document.getElementById("delimValue").style.display = "none";
	}
	else
	{
		document.getElementById("delimTitle").style.display = "block";
		document.getElementById("delimValue").style.display = "block";
	}
}

function dmlChange(selectedDML)
{
	var sNewDML = document.getElementById("<%= Constants.L_UI_DML %>").value;
	determineDisplay(sNewDML);
}

function manageDisplay()
{
	determineDisplay("<%= sDml %>");
}

function determineDisplay(dml)
{
	var displayContents = new Array();
	var hideContents = new Array();

	if (dml == "UPDATE")
	{
		displayContents = ["0", "1"];
		hideContents = ["2"];
	}
	else if (dml == "UPSERT")
	{
		displayContents = ["0", "1", "2"];
	}
	else if (dml == "DELETE")
	{
		displayContents = ["1"];
		hideContents = ["0", "2"];
	}
	else
	{
		displayContents = ["2"];
		hideContents = ["0", "1"];
	}
	displayCols(displayContents, hideContents);
	buildSQLText(dml);
}

function displayCols(displayContents, hideContents)
{
	var dispContents = displayContents;
	var hiContents = hideContents;

	for(var i=0; i < dispContents.length; i++)
	{
		var num = dispContents[i];
		document.getElementById("colGroupId"+num).style.display ="block";
		document.getElementById("colListHeading"+num).style.display ="block";
	}
	
	for(var i=0; i < hiContents.length; i++)
	{
		var num = hiContents[i];
		document.getElementById("colGroupId"+num).style.display ="none";
		document.getElementById("colListHeading"+num).style.display ="none";
	}

}

function moveselected(fromCombo, toCombo)
{
    var to_remove_counter =0;
	var sNewDML = document.getElementById("<%= Constants.L_UI_DML %>").value;
    
    for(var i=0; i < fromCombo.options.length;i++)
    {
        if (fromCombo.options[i].selected == true)
            {
            var addvalue =   fromCombo.options[i].value;
            var addtext = fromCombo.options[i].text;
            toCombo.options[toCombo.options.length]=new Option(addtext,addvalue);
             fromCombo.options[i].selected=false;
             ++to_remove_counter;
            }
             else
            {
            fromCombo.options[i-to_remove_counter].selected=false;
            fromCombo.options[i-to_remove_counter].text=fromCombo.options[i].text;
            fromCombo.options[i-to_remove_counter].value=fromCombo.options[i].value;
            }
    }
    
    var numToLeave=fromCombo.options.length-to_remove_counter;

    for (i=fromCombo.options.length-1;i>=numToLeave;i--)
    {
        fromCombo.options[i]=null;
    }
	
	orderElement(toCombo);
	buildSQLText(sNewDML);
}

function orderElement(toCombo)
{
	var arrayAllCols = new Array();
	<%	for(int k=0; k < listAllCols.size(); k++) 
		{ 
			Field field = (Field) listAllCols.get (k);
			String sFieldName = field.getName ();
		%>
		arrayAllCols[arrayAllCols.length] = '<%= sFieldName %>';
		<% 
		 } %>

	var top = 0;

	for(var i=0; i < arrayAllCols.length; i++)
	{
		for(var j=0; j < toCombo.options.length ; j++)
		{
			if (arrayAllCols[i] == toCombo.options[j].value)
			{
				var tempVal =  toCombo.options[top].value;
				var tempText =  toCombo.options[top].text;

				toCombo.options [top].value = toCombo.options [j].value;
				toCombo.options [top].text = toCombo.options [j].text;

				toCombo.options [j].value = tempVal;
				toCombo.options [j].text = tempText;
				top = top + 1;
			}
		}
	}
}

function buildSQLText(dml)
{
	var sql ="";
	if (dml == "UPDATE")
	{
		sql = buildUpdateSQL();
	}
	else if (dml == "UPSERT")
	{
		sql = buildUpsertSQL ();
	}
	else if (dml == "DELETE")
	{
		sql = buildDeleteSQL();
	}
	else
	{
		sql = buildInsertSQL();
	}

	document.getElementById("sqlId").innerHTML  = sql;
}
    
function buildUpdateSQL ()
{
	var staticSQL = '<font class="sqlBlue">UPDATE </font>' + '<%= sFullyQualifiedtable %>' + '<font class="sqlBlue"> SET </font>';
	var updateList = document.getElementById("update");
	var whereList = document.getElementById("where");
	var setClause = buildSetPortion (updateList);
	var whereClause = buildWherePortion (whereList);
	
	var updateSQL = staticSQL + '<font class="sqlBrown">'+ setClause +'</font>' + '<font class="sqlBlue"><br> WHERE </font>' + '<font class="sqlBrown">'+whereClause+'</font>';
	return updateSQL;
}

function buildDeleteSQL ()
{
	var staticSQL = '<font class="sqlBlue">DELETE FROM </font>' + '<%= sFullyQualifiedtable %>' + '<font class="sqlBlue"><br> WHERE </font>';
	var whereList = document.getElementById("where");
	var whereClause = buildWherePortion (whereList);
	
	var updateSQL = staticSQL + '<font class="sqlBrown">'+whereClause+'</font>';
	return updateSQL;
}

function buildInsertSQL ()
{
	var staticSQL = '<font class="sqlBlue">INSERT INTO </font>' + '<%= sFullyQualifiedtable %>';
	var insertList = document.getElementById("insert");
	var insertClause = buildInsertPortion (insertList);
	var questionMarks = buildInsertQuestionMarks(insertList);

	var insertSQL = staticSQL + '(' + '<font class="sqlBrown">'+ insertClause + '</font>' + ')'+ '<font class="sqlBlue"> VALUES </font>' + '(' + '<font class="sqlBrown">'+ questionMarks + '</font>' + ')';
	return insertSQL;
}

function buildUpsertSQL ()
{
	var staticUpdateSQL = '<font class="sqlBlue">UPDATE </font>' + '<%= sFullyQualifiedtable %>' + '<font class="sqlBlue"> SET </font>';
	var elseKeyWord = '<font class="sqlBlue"> ELSE </font>';
	var staticInseretSQL = '<font class="sqlBlue"> INSERT INTO </font>' + '<%= sFullyQualifiedtable %>';

	var updateList = document.getElementById("update");
	var whereList = document.getElementById("where");
	var insertList = document.getElementById("insert");
	
	var setClause = buildSetPortion (updateList);
	var whereClause = buildWherePortion (whereList);
	var insertClause = buildInsertPortion (insertList);

	var questionMarks = buildInsertQuestionMarks(insertList);

	var upsertSQL = staticUpdateSQL + '<font class="sqlBrown">'+ setClause +'</font>' + '<font class="sqlBlue"><br> WHERE </font>' + 
					'<font class="sqlBrown">'+whereClause+'</font>' +'<br>' + elseKeyWord + '<br>'+ 
					staticInseretSQL + '(' + '<font class="sqlBrown">'+ insertClause + '</font>' + ')'+ '<font class="sqlBlue"> VALUES </font>' + '(' + '<font class="sqlBrown">'+ questionMarks + '</font>' + ')';

	                 
	return upsertSQL;
}

function buildSetPortion (listOfCols)
{
	var counter = 0;
	var setPortion = "";
	for(var i=0; i < listOfCols.options.length; i++)
	{
		var col = listOfCols.options[i].value + "=?";
		setPortion = setPortion + col;
		counter++;
		if (counter < listOfCols.options.length)  
		{
			setPortion = setPortion + ', ';
		}
		
	}
	return setPortion;
}

function buildWherePortion(listOfCols)
{
	var counter = 0;
	var wherePortion = "";
	for(var i=0; i < listOfCols.options.length; i++)
	{
		var col = listOfCols.options[i].value + "=?";
		wherePortion = wherePortion + col;
		counter++;
		if (counter < listOfCols.options.length)  
		{
			wherePortion = wherePortion + '<font class="sqlBlue"> AND </font>';
		}
	}
	return wherePortion
}

function buildInsertPortion (listOfCols)
{
	var counter = 0;
	var insertPortion = "";
	for(var i=0; i < listOfCols.options.length; i++)
	{
		var col = listOfCols.options[i].value;
		insertPortion = insertPortion + col;
		counter++;
		if (counter < listOfCols.options.length)  
		{
			insertPortion = insertPortion + ', ';
		}
	}
	return insertPortion;
}

function buildInsertQuestionMarks (listOfCols)
{
	var counter = 0;
	var querstionMarks = "";
	var numSelectedElements = listOfCols.options.length;
	for (var i = 0; i < numSelectedElements; i++)
	{
		querstionMarks = querstionMarks + '?';
		counter++
	
		if (counter < numSelectedElements)  
		{
			querstionMarks = querstionMarks + ', ';
		}
	}
	return querstionMarks
}

function validate ()
{
	var dml = document.getElementById("<%= Constants.L_UI_DML %>").value;
	
	var updateList = document.getElementById("update");
	var whereList = document.getElementById("where");
	var insertList = document.getElementById("insert");

	var updateAlert = "Please choose at least one column in the UPDATE clause";
	var whereAlert = "Please choose at least one column in the WHERE clause";
	var insertAlert = "Please choose at least one column in the INSERT clause";

	if (dml == "UPDATE" || dml == "UPSERT")
	{
		if (updateList.options.length == 0)
		{
			alert(updateAlert);
			return false;
		}
		else if (whereList.options.length == 0)
		{
			alert(whereAlert);
			return false;
		}	
	}
	
	if (dml == "UPSERT" || dml == "INSERT")
	{
		if (insertList.options.length == 0)
		{
			alert(insertAlert);
			return false;
		}	
	}
	
	if (dml == "DELETE")
	{
		if (whereList.options.length == 0)
		{
			alert(whereAlert);
			return false;
		}	
	} 
	
	var vFormat = document.getElementById("<%= Constants.L_UI_FORMAT %>").value;
	var vDelimiter = document.getElementById("<%= Constants.L_UI_DELIMITER %>").value;
	var vDelimiterTrimmed = vDelimiter.replace(/^\s+|\s+$/g, '');
	document.getElementById("<%= Constants.L_UI_DELIMITER %>").value = vDelimiterTrimmed;
	vDelimiter = vDelimiterTrimmed;

	if ((vFormat == "VARIABLE") && (vDelimiter.length == 0))
	{
		alert('Please enter a valid delimiter');
		return false;
	}
	
	serializeListBox();
	return true;
}


function verifyTextField(txtValue)
{
	var vTxtValue = txtValue;

	for(var i = 0; i < vTxtValue.length; i++)
	{
		var vASCIIValue = vTxtValue.charCodeAt(i);
		
		//Numbers 0x30 ~ 0x39
		//Upper Letters 0x41 ~ 0x5a
		//_ 0x5f
		//Lower Letters 0x61 ~ 0x7a

		if(vASCIIValue < 0x30)
		{
			return false;
		}

		if((vASCIIValue > 0x39) && (vASCIIValue < 0x41))
		{
			return false;
		}

		if((vASCIIValue > 0x5a) && (vASCIIValue < 0x5f))
		{
			return false;
		}

		if((vASCIIValue > 0x5f) && (vASCIIValue < 0x61))
		{
			return false;
		}

		if(vASCIIValue > 0x7a)
		{
			return false;
		}
	}
	return true;
}


function serializeListBox()
{
	var listBoxId = new Array(<%= saListBoxId.length %>);
	<% for(int k=0; k < saListBoxId.length; k++)
	{ %>
		listBoxId[<%= k %>] = "<%= saListBoxId[k] %>";
	<% 
	} %>
	for(var i =0; i < listBoxId.length; i++)
	{
		var counter = 0;
		var listBox = document.getElementById(listBoxId[i]);
		var hiddenFieldValue = "";

		for(var j = 0; j < listBox.options.length; j++)
		{
			hiddenFieldValue = hiddenFieldValue + listBox.options[j].value;
			counter++;
			if (counter < listBox.options.length)
			{
				hiddenFieldValue = hiddenFieldValue + ",";
			}
		}
		
		var hiddenFieldId = "listBoxVal_" + listBoxId[i];     // The hidden fields but have ids as (listBoxVal_update or listBoxVal_where or listBoxVal_insert)
		var hiddenField = document.getElementById(hiddenFieldId);
		hiddenField.value = hiddenFieldValue;
	}
}
</script>

<body onLoad="manageDisplay()">
<table>
<form name="expressionColsForm" action="Dispatcher?apptype=<%= sAppType %>&request=ModifyProperties" method="post" onSubmit="return validate()">
	
	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<tr>
			<jsp:include page="topbar.jsp"/>
			<td class="titleBox">&nbsp;<%= sServiceID %> <%= ResourceBundleFactory.getString("UI_D_PROPS",request) %> (<%= ResourceBundleFactory.getString("UI_D_SERVICE_TYPE",request) %>: <%= sServiceType %>) - <%= sAppName %></td>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr>
			<td class="whiteSpace"></td>
		</tr>
		<%if (sServiceType.equalsIgnoreCase(ServiceConstants.MULTIPLE)) {%>
		<tr>
			<td>
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">
					<tr align="left" valign="top">
						<td class="whiteSpace" width=1 nowrap="nowrap"></td>
						<td align="left" nowrap="nowrap">
							<b>
								<%= ResourceBundleFactory.getString("UI_D_OPERATION_NAME",request) %>: <%= sOperationName %>
							</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td class="whiteSpace"></td>
		</tr>
		<%} %>
		
		<tr>

			<td>
			
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

					<tr align="left" valign="top">
						<td class="whiteSpace" width=1 nowrap="nowrap"></td>
						<td width="60%" >

							<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

							<%
								for(int iNoDivs= 0;  iNoDivs < 3; iNoDivs ++)
								{
									String sSQLExp = "";
									if(iNoDivs == 0)
									{
										sSQLExp = sUpdateExp;
									}
									else if(iNoDivs == 1)
									{
										sSQLExp = sWhereExp;
									}
									else
									{
										sSQLExp = sInsertExp;
									}
							%>

								<tr align="left" valign="top">
									<td align="left" nowrap="nowrap">
										<div id="colListHeading<%= iNoDivs %>" style="display:none">
											<b>
											<%= saColHeadings[iNoDivs] %>
											</b>
										</div>
									</td>
								</tr>

								<tr align="center" valign="middle">
									<td>
										<div id = "colGroupId<%= iNoDivs %>" style="display:none">
											<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">

												<tr><td class="whiteSpace"></td></tr>

												<tr align="center" valign="middle">
													<td> 
														<select size="<%= sListSize %>" style="Width: 100%" name="fromColumnList<%= iNoDivs %>" multiple="multiple">
															<%	for (int i=0; i < listAllCols.size(); i++)
																{	
																	Field field = (Field) listAllCols.get(i);
																	String sName = field.getName ();
																	if(sSQLExp.substring(i, i+1).equals("0"))
																	{
															%>
																	<option value=<%= sName %>>&nbsp;<%= sName %> </option>	
															<%	
																	}
																}
															%>
														</select>
													</td>
														
													<td>
														<table align="center" valign="middle" CELLSPACING="0" CELLPADDING="0">
															<tr>
																<td>
																	<input type="button" name="button<%= iNoDivs %>" value=" -> " onClick="moveselected(fromColumnList<%= iNoDivs %>, toColumnList<%= iNoDivs %>)"></input>
																</td>
															</tr>
															<tr>
																<td>
																	<input type="button" name="button<%= iNoDivs %>" value=" <- " onClick="moveselected(toColumnList<%= iNoDivs %>, fromColumnList<%= iNoDivs %>)"></input>
																</td>
															</tr>
														</table>
													</td>

													<td> 
														<select size="<%= sListSize %>" id="<%= saListBoxId[iNoDivs] %>"  style="Width: 100%" name="toColumnList<%= iNoDivs %>"  multiple="multiple">
															<%	for (int i=0; i < listAllCols.size(); i++)
																{	
																	Field field = (Field) listAllCols.get(i);
																	String sName = field.getName ();
																	if(sSQLExp.substring(i, i+1).equals("1"))
																	{
															%>
																	<option value=<%= sName %>>&nbsp;<%= sName %> </option>	
															<%	
																	}
																}
															%>
														</select>
													</td>
												</tr>
												
												<tr align="center" valign="middle">
													<td class="whitespace" width="45%" nowrap="nowrap"></td>
													<td class="whitespace" width="10%"></td>
													<td class="whitespace" width="45%" nowrap="nowrap"></td>
												</tr>
											</table>

										</div>
									</td>
								</tr>
							<%
								}
							%>
							</table>
						</td>

						<td class="whiteSpace" width=1 nowrap="nowrap"></td>

						<td width="38%" valign="top">	
							<table BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="LEFT">

								<tr valign="top">
									<td align="left" width="100%" style="font-weight: bold" nowrap="nowrap">
										<b>
											<%= ResourceBundleFactory.getString("UI_D_TABLE_NAME",request) %>: &nbsp;<%= sTableName %>
										</b>
									</td>
								</tr>

								<tr><td class="whiteSpace"></td></tr>

								<tr valign="top">
									<td>
										<table WIDTH="100%" BORDER="1" BORDERCOLOR="#e3e7ea" CELLSPACING="0" CELLPADDING="0" ALIGN="right">	
											<tr valign="top">
												<td>
													<table WIDTH="100%" BORDER="0" CELLSPACING="2" CELLPADDING="2" ALIGN="right">
														<%	for (int i=0; i < listAllCols.size(); i++)
															{	
																Field field = (Field) listAllCols.get(i);
																String sName = field.getName ();
																String sType = field.getType ();
																String rowClass = (i % 2 == 0) ? "evenRowL" : "oddRowL";
														%>
								
														<tr>
															<td class="<%= rowClass %>" width="50%" nowrap="nowrap">
															<% if (field.isPI())
															{ 
															%> 
																<font class="pi"> <%= sName %> </font>
															<%
															}
															else
															{
															%>
																<%= sName %>
															<%
															}
															%>
															</td>
															
															<td class="<%= rowClass %>" width="50%" nowrap="nowrap">
															<% if (field.isPI())
															{ 
															%> 
																<font class="pi"> <%= sType %> </font>
															<%
															}
															else
															{
															%>
																<%= sType %> 
															<%
															}
															%>
												
															</td>
														</tr>
														<%
															}
														%>
													</table>

												</td>
											</tr>
										</table>

									</td>
								</tr>

								<tr><td class="whiteSpace"></td></tr>

								<tr valign="top">
									<td align="left" width="100%" nowrap="nowrap">
										<b><%= ResourceBundleFactory.getString("UI_D_PRIMARY_IDX1",request) %><font class="pi"> <%= ResourceBundleFactory.getString("UI_D_PRIMARY_IDX2",request) %></font> <%= ResourceBundleFactory.getString("UI_D_PRIMARY_IDX3",request) %></b>
									</td>
								</tr>

								<tr><td class="whiteSpace"></td></tr>
								
							</table>
						</td>
					</tr>
						
					<tr><td class="whiteSpace"></td></tr>

				</table>

			</td>
		</tr>


		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr>
			<td>
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">
						
					<tr>
						<td class="whiteSpace" width=8></td>
						<td class="whiteSpace" nowrap="nowrap"></td>
					</tr>


					<tr align="left">
						<td class="whiteSpace"></td>
						<td align="left" nowrap="nowrap" style="font-weight: bold"><%= ResourceBundleFactory.getString("UI_D_SQL_STATEMENT",request) %>:&nbsp;</td>
					</tr>
					
					<tr>
						<td class="whiteSpace"></td>
						<td class="whiteSpace"></td>
					</tr>

					<tr align="left">
						<td class="whiteSpace"></td>
						<td>
							<table WIDTH="100%" BORDER="1" BORDERCOLOR="#e3e7ea" CELLSPACING="0" CELLPADDING="0">	
								
								<tr>
									<td>
										<table WIDTH="100%" BORDER="0" CELLSPACING="2" CELLPADDING="2">	
											<tr>
												<td class="evenRowL" id="sqlId" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_VIEW_SQL",request) %></td>
											</tr>
										</table>
									</td>
								</tr>

							</table>
						</td>
					</tr>
					
					<tr><td class="whiteSpace"></td></tr>

				</table>
			</td>
		</tr>

		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>
		</tr>


		<tr>
			<td>
				<table cellspacing=2 cellpadding=2 width="100%" border=0>
						
					<tr>
						<td class="whiteSpace" width=8></td>
						<td class="whitespace"></td>
						<td class="whitespace"></td>
						<td class="whitespace" width="70%"></td>
					</tr>

					<tr>
						<td class="whiteSpace"></td>
						<td align="left" nowrap="nowrap" style="font-weight: bold"><%= ResourceBundleFactory.getString("UI_D_MESSAGE_ATTRIBUTES",request) %>:&nbsp;</td>
						<td class="whitespace"></td>
						<td class="whitespace"></td>
					</tr>

					<tr><td class="whiteSpace"></td></tr>

					<tr>		
						<td class="whiteSpace"></td>	
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_DML_TYPE",request) %>:&nbsp;</td>
						<td align="left"><select name="<%= Constants.L_UI_DML %>" id="<%= Constants.L_UI_DML %>" style="width:175" onchange="dmlChange(this.id)">
											 <option value="INSERT" <%= sDMLInsert %>>INSERT</option>
											 <option value="UPDATE" <%= sDMLUpdate %>>UPDATE</option>
											 <option value="DELETE" <%= sDMLDelete %>>DELETE</option>
											 <option value="UPSERT" <%= sDMLUpsert %>>UPSERT</option>
										 </select>
						</td>
						<td class="whitespace"></td>
					</tr>

					<tr><td class="whiteSpace"></td></tr>

					<tr>
						<td class="whiteSpace"></td>
						<td align="left" nowrap="nowrap" style="font-weight: bold"><%= ResourceBundleFactory.getString("UI_D_SERVICE_ATTRIBUTES",request) %>:&nbsp;</td>
						<td class="whitespace"></td>
						<td class="whitespace"></td>
					</tr>

					<tr><td class="whiteSpace"></td></tr>

					<tr>
						<td class="whiteSpace"></td>	
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_ROLLBACK_INVALIDF_MSG",request) %>:&nbsp</td>
						<td align="left">
							<%if (sServiceType.equalsIgnoreCase(ServiceConstants.SINGLE)) {%>
								<select name="<%= Constants.L_UI_ROLLBACK_INVALIDF_MSG %>" id="<%= Constants.L_UI_ROLLBACK_INVALIDF_MSG %>" style="width:175">
									 <option value="true" <%= sRollbackInvalidFormatMsg_true %>>true</option>
									 <option value="false" <%= sRollbackInvalidFormatMsg_false %>>false</option>
								</select>
							<%} else {%>
								<input readonly type="text" name="<%= Constants.L_UI_ROLLBACK_INVALIDF_MSG %>" id="<%= Constants.L_UI_ROLLBACK_INVALIDF_MSG %>" size="30" maxlength="30" value="<%= service.getName(ServiceConstants.IS_ROLLBACK_INVF_MSG) %>"></input>
							<%} %>
						</td>
						<td class="whitespace"></td>
					</tr>

					<tr>
						<td class="whiteSpace"></td>	
						<td align="right" nowrap="nowrap"><%= ResourceBundleFactory.getString("UI_D_FORMAT",request) %>:&nbsp</td>
						<td align="left">
							<%if (sServiceType.equalsIgnoreCase(ServiceConstants.SINGLE)) {%>
								<select name="<%= Constants.L_UI_FORMAT %>" id="<%= Constants.L_UI_FORMAT %>" style="width:175" onChange="formatChange(this.id)">
									<option value="VARIABLE" <%= sFormatVariable %>>VARIABLE</option>
									<option value="FIXED" <%= sFormatFixed %>>FIXED</option>
									<option value="XML" <%= sFormatXml %>>XML</option>
								</select>
							<%} else {%>
								<input readonly type="text" name="<%= Constants.L_UI_FORMAT %>" id="<%= Constants.L_UI_FORMAT %>" size="30" maxlength="30" value="<%= service.getName(ServiceConstants.MSG_FORMAT) %>"></input>
							<%} %>
						</td>
						<td class="whitespace"></td>
					</tr>
					<tr>		
						<td class="whiteSpace"></td>	
						<%
						if(sFormatVariable.length() > 0)
						{
						%>
						<td align="right" nowrap="nowrap"><div id="delimTitle"><%= ResourceBundleFactory.getString("UI_D_DELIMITER",request) %>:&nbsp;</div></td>
						<td align="left">
							<div id="delimValue">
								<%if (sServiceType.equalsIgnoreCase(ServiceConstants.SINGLE)) {%>
									<input type="text" name="<%= Constants.L_UI_DELIMITER %>" id="<%= Constants.L_UI_DELIMITER %>" size="30" maxlength="30" value="<%= service.getName(ServiceConstants.MSG_DELIMITER) %>"></input>
								<%} else {%>
									<input readonly="true" type="text" name="<%= Constants.L_UI_DELIMITER %>" id="<%= Constants.L_UI_DELIMITER %>" size="30" maxlength="30" value="<%= service.getName(ServiceConstants.MSG_DELIMITER) %>"></input>
								<%} %>
							</div>
						</td>
						<%
						}
						else
						{
						%>
						<td align="right" nowrap="nowrap"><div id="delimTitle" style="display:none"><%= ResourceBundleFactory.getString("UI_D_DELIMITER",request) %>:&nbsp;</div></td>
						<td align="left">
							<div id="delimValue" style="display:none">
								<%if (sServiceType.equalsIgnoreCase(ServiceConstants.SINGLE)) {%>
									<input type="text" name="<%= Constants.L_UI_DELIMITER %>" id="<%= Constants.L_UI_DELIMITER %>" size="30" maxlength="30" value="<%= service.getName(ServiceConstants.MSG_DELIMITER) %>"></input>
								<%} else {%>
									<input readonly="true" type="text" name="<%= Constants.L_UI_DELIMITER %>" id="<%= Constants.L_UI_DELIMITER %>" size="30" maxlength="30" value="<%= service.getName(ServiceConstants.MSG_DELIMITER) %>"></input>
								<%} %>
							</div>
						</td>
						<%
						}
						%>
						<td class="whitespace"></td>
					</tr>

					<tr><td class="whiteSpace"></td></tr>

				</table>
			</td>
		</tr>

		<tr>
			<jsp:include page="topbar.jsp"/>
			<jsp:include page="bottombar.jsp"/>
		</tr>

		<tr>
			<td>
				<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="left">
					<tr>
						<td class="whiteSpace" width=4></td>
						<td class="whiteSpace" width="5%"></td>
						<td class="whiteSpace" width=1></td>
						<td class="whiteSpace" width="5%"></td>
						<td class="whiteSpace" width="87%"></td>
					</tr>

					<tr>
						<td class="whiteSpace"></td>
						<td>
							<input type="button" name="back" value="< <%= ResourceBundleFactory.getString("UI_B_BACK",request) %> " onClick="history.back()"></input>
						</td>
						<td class="whiteSpace"></td>
						<td>
							<input type="submit" name="submit" value=" <%= ResourceBundleFactory.getString("UI_B_MODIFY",request) %> "></input>
						</td>
						<td class="whiteSpace"></td>
					</tr>

				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="<%= ServiceConstants.SERVICE_ID %>" value="<%= sServiceID %>"></input>
	<input type="hidden" name="<%= ServiceConstants.SERVICE_ID_KEY %>" value="<%= sServiceIDKey %>"></input>
	<input type="hidden" name="<%= ServiceConstants.OPERATION_NAME %>" value="<%= sOperationName %>"></input>
	<input type="hidden" name="<%= Constants.L_UI_UPDATE %>" id="<%= Constants.L_UI_UPDATE %>" value=""></input>
	<input type="hidden" name="<%= Constants.L_UI_WHERE %>" id="<%= Constants.L_UI_WHERE %>" value=""></input>
	<input type="hidden" name="<%= Constants.L_UI_INSERT %>" id="<%= Constants.L_UI_INSERT %>" value=""></input>	
</form>
</table>
</body>