<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<% String sUser  = (String) session.getAttribute("userName");%>

<table cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
		<td style="padding-left: 3px;"><img src="images/TeradataLogo.bmp"></td>

		<td align="right" style="padding-right: 4px;"> 
			
			<%= ResourceBundleFactory.getString("UI_D_WELCOME",request) %>,&nbsp;<font style="color:#1c3880;"><%= sUser %></font>&nbsp;&nbsp;|&nbsp;

			<a href="Dispatcher?request=home"><font class="teradata" style="color:#ff9900;"><%= ResourceBundleFactory.getString("UI_D_HOME",request) %></a>&nbsp;&nbsp;|&nbsp;

			<a href="Dispatcher?request=logout"><font class="teradata" style="color:#ff9900;"><%= ResourceBundleFactory.getString("UI_D_LOGOFF",request) %></font></a>&nbsp;&nbsp;

		</td>
	</tr>
</table>