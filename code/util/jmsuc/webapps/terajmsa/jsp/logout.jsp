<%@ page contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title><%= ResourceBundleFactory.getString("UI_D_MAIN_APP_TITLE_WITH_PRODUCT_VERSION",request) %></title>
<link rel="stylesheet" type="text/css" href="css/tjmsl.css" />
<link rel="icon" href="images/favicon.ico" /> 
<link rel="shortcut icon" href="images/favicon.ico" /> 

</head>

<body>
	<TABLE WIDTH="40%" HEIGHT="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<% String sUser = (String)request.getAttribute("userName");
		   if (sUser == null)
			   sUser = "";
		%>

		<tr>
			<td ALIGN="MIDDLE">
				<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">
					<jsp:include page="topbar.jsp"/>
					<tr>
						<td class="titleBox" align="center"><%= ResourceBundleFactory.getString("UI_D_LOG_OUT_CONFIRMATION",request) %></td>			
					</tr>	
					<jsp:include page="bottombar.jsp"/>
					
					<tr>
						<td><font class="header"><%= sUser %></font> <%= ResourceBundleFactory.getString("UI_D_SESSION_CLOSED",request) %></td>			
					</tr>
					
					<tr>
						<td>
							<a href="Admin">
								<font class="teradata" style="color:#ff9900;"><%= ResourceBundleFactory.getString("UI_D_CLCK_LOG_BACK_IN",request) %></font>
							</a>
						</td>				
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
