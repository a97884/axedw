@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

rem ******************************************************************
rem Set parameters
rem ******************************************************************
SET "v_utilpath=%~d0%~p0"

SET "v_rlistpath=%~d1%~p1"
SET "v_rlistname=%~n1"
SET "v_rlistext=%~x1"

SET "v_ilistpath=%~d2%~p2"
SET "v_ilistname=%~n2"
SET "v_ilistext=%~x2"

IF /I "!v_rlistpath!"=="" EXIT /B 999

SET "v_err=0"
SET v_quote="
SET "v_NEQ=<>"

FOR /D %%k IN ("%v_rlistpath:~0,-1%") DO SET "v_rlistparent=%%~nk"
FOR /F "tokens=1* delims=|" %%k IN ("!v_utilpath:\util\=\|!") DO SET "v_uroot=%%k"
FOR /F "tokens=1* delims=|" %%k IN ("!v_rlistpath:\code\=\|!") DO SET "v_croot=%%k"
FOR /F "tokens=1* delims=|" %%k IN ("!v_ilistpath:\code\=\|!") DO SET "v_iroot=%%k"

SET "v_ilist=!v_ilistpath!!v_ilistname!!v_ilistext!"
rem ******************************************************************

rem ******************************************************************
rem Verify run-list
rem ******************************************************************
cls
CALL :L_BUILDLIST "!v_rlistpath!!v_rlistname!!v_rlistext!"
IF !ERRORLEVEL! GEQ 1 EXIT /B !ERRORLEVEL!

rem ******************************************************************


EXIT /B


rem ******************************************************************
rem local proc L_BUILDLIST
rem ******************************************************************
:L_BUILDLIST
SET "v_rlist=%~1"
SET "v_rlist_path=%~p1"
FOR /D %%x IN ("!v_rlist_path:~0,-1!") DO SET "v_rlist_parent=%%~nx"

rem ************************
rem Display run-info
rem ECHO.   !v_rlist!
rem ************************

IF EXIST "!v_rlist!" FOR /F "usebackq tokens=1*" %%a IN ("!v_rlist!") DO (
	SET "v_runcmd="
	IF /I "%%a"=="CD" SET "v_runcmd=CD"
	IF /I "%%a"=="BTQCOMPILE" SET "v_runcmd=.COMPILE FILE"
	IF /I "%%a"=="BTQRUN" SET "v_runcmd=.RUN FILE"
	IF /I "!v_runcmd!" NEQ "" (
		SET "v_srcobj=%%b"
		SET "v_srcfile=%v_croot%!v_srcobj:/=\!"
		SET "v_srcfile_full="
		SET "v_srcfile_path="
		SET "v_srcfile_name="
		SET "v_srcfile_ext="
		SET "v_include="
		IF /I "%v_ilist%"=="" (
			SET "v_include=1"
		) ELSE IF EXIST "%v_ilist%" (
			IF /I "!v_runcmd!"=="CD" FOR /F "usebackq tokens=1*" %%x IN (`FIND /C "!v_srcobj!" "%v_ilist%"`) DO (
				SET "v_include=%%y"
				SET "v_include=!v_include:*: =!"
				IF /I "!v_include!" NEQ "" IF !v_include! GTR 0 SET "v_include=1"
			) ELSE FOR /F "usebackq tokens=1*" %%k IN ("%v_ilist%") DO IF "%%l"=="!v_srcobj!" (
				SET "v_include=%%k"
				SET "v_include=!v_include:~0,1!"
				SET "v_include=!v_include:M=1!"
				SET "v_include=!v_include:A=1!"
			)
		)
		IF /I "!v_include!"=="1" (
			IF /I "!v_runcmd!"=="CD" (
				CALL :L_BUILDLIST "!v_srcfile!\_runorder.txt"
			) ELSE (
				IF EXIST "!v_srcfile!" (
					FOR %%k IN ("!v_srcfile!") DO IF "%%~fk" NEQ "!v_srcfile!" ECHO !v_srcobj!
				) ELSE ECHO.!v_srcobj!
			)
		)
	)
) ELSE ECHO.!v_rlist!
EXIT /B
rem ******************************************************************

