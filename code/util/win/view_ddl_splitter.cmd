@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "v_utilpath=%~d0%~p0"
SET "v_infile=%1"
SET "v_listpath=%~d1%~p1"
SET "v_inname=%~n1"
SET "v_dbname=%2"

FOR /F "tokens=1* delims=|" %%a IN ("%v_utilpath:\util\=|%") DO SET "v_croot=%%a"

SET "v_headerfile=%v_croot%\util\scm\template\svn_header_sql.txt"
SET "v_outpath=%v_listpath%view\"
SET "v_ext=.btq"
SET "v_dbnameprefix=${DB_ENV}"
SET "v_dbnamepostfix=VOUT"
SET "c_SED=%v_utilpath%sed.exe"

SET "v_date=%DATE:-=%"
SET "v_time=%TIME::=%"
SET "v_time=!v_time: =0!"
SET "v_ver=%v_date:~2%%v_time:~0,6%"

SET "v_neq=<>"
SET "v_bb=("
SET "v_be=)"
SET "v_defno=0"
SET v_blankline=0
SET "v_reptxt1="
SET "v_db="
SET "v_obj="
SET "v_objpath="
SET "v_basename="
SET "v_newobj="
SET "v_line="
SET "v_line0="
SET "v_line1="
SET "v_line2="
SET "v_row="
SET "v_crerep="

IF /I "%v_infile%"=="" EXIT /B 999

FOR /D %%k IN (%v_listpath:~0,-1%) DO SET "v_basename=%%~nk"
IF /I "%v_dbname%"=="" SET "v_dbname=%v_dbnameprefix%%v_basename%%v_dbnamepostfix%"

SET "v_objpath=code/%v_outpath:*\code\=%"
SET "v_objpath=%v_objpath:\=/%"

IF NOT EXIST "%v_outpath%" MKDIR "%v_outpath%">NUL

IF EXIST %v_listpath%_runorder.%v_inname%.txt REN %v_listpath%_runorder.%v_inname%.txt _runorder.%v_inname%.txt.!v_ver!

FOR /F "tokens=* delims=" %%x IN ('find.exe /v /n "" %v_infile%') DO (
	SET "v_line=%%x"
	SET "v_line=!v_line:*]=!"
	SET "v_line0=!v_line!"
	SET "v_crerep="
	IF /I "!v_line!"=="" (
		SET /A v_blankline+=1
	) ELSE SET v_blankline=0
	IF !v_blankline! LEQ 1 (
		FOR /F "tokens=1,2,3,4*" %%a IN ("!v_line0!") DO (
			SET "v_crerep=%%a"
			IF /I "!v_crerep!"=="REPLACE" SET "v_crerep=CREATE"
			IF /I "!v_crerep!"=="CREATE" (
				SET "v_newobj="
				SET "v_line1="
				SET "v_line2="
				IF /I "%%c"=="VIEW" (
					SET "v_newobj=%%d"
					SET "v_line1=%%a %%b %%c"
					SET "v_line2=%%e"
				)
				IF /I "%%b"=="VIEW" (
					SET "v_newobj=%%c"
					SET "v_line1=%%a %%b"
					SET "v_line2=%%d %%e"
					IF /I "%%e"=="" SET "v_line2=%%d"
				)
				IF /I "!v_newobj!" NEQ "" (
					IF /I "!v_obj!" NEQ "" (
						ECHO.>>%v_outpath%\!v_obj!!v_ext!
					)
					FOR /F "tokens=1* delims=." %%k IN ("!v_newobj!") DO (
						IF /I "%%l"=="" (
							SET "v_obj=!v_newobj!"
							SET "v_db=!v_dbname!"
						) ELSE (
							SET "v_db=%%k"
							SET "v_obj=%%l"
						)
					)
					SET /A "v_defno+=1"
					ECHO.BTQRUN	%v_objpath%!v_obj!!v_ext!>>%v_listpath%_runorder.%v_inname%.txt
					SET "v_line=!v_line1! !v_db!.!v_obj! !v_line2!"
					IF /I "!v_line2!"=="" SET "v_line=!v_line1! !v_db!.!v_obj!"
					IF EXIST %v_outpath%\!v_obj!!v_ext! REN %v_outpath%\!v_obj!!v_ext! !v_obj!._!v_ver!>NUL
					COPY /Y %v_headerfile% %v_outpath%\!v_obj!!v_ext!>NUL
					ECHO..SET MAXERROR 0;>>%v_outpath%\!v_obj!!v_ext!
					ECHO.>>%v_outpath%\!v_obj!!v_ext!
					ECHO.DATABASE !v_db!;>>%v_outpath%\!v_obj!!v_ext!
					ECHO.>>%v_outpath%\!v_obj!!v_ext!
					IF /I "%%a"=="CREATE" (
						ECHO.SELECT * FROM DBC.TABLES WHERE DatabaseName='!v_db!' AND TableName='!v_obj!';>>%v_outpath%\!v_obj!!v_ext!
						ECHO..IF ACTIVITYCOUNT = 0 THEN .GOTO CreView>>%v_outpath%\!v_obj!!v_ext!
						ECHO.DROP VIEW !v_obj!;>>%v_outpath%\!v_obj!!v_ext!
						ECHO.>>%v_outpath%\!v_obj!!v_ext!
						ECHO..LABEL CreView>>%v_outpath%\!v_obj!!v_ext!
					)
				)
			)
		)
		IF /I "!v_obj!" NEQ "" ECHO.!v_line!>>%v_outpath%\!v_obj!!v_ext!
	)
)

IF /I "!v_obj!" NEQ "" (
	IF !v_blankline!==0 ECHO.>>%v_outpath%\!v_obj!!v_ext!
)
