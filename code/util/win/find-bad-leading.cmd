@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

CALL :L_ITER 1>find-bad-leading.log 2>NUL

EXIT /B

:L_ITER
SETLOCAL ENABLEDELAYEDEXPANSION
FOR /R %%i IN (*.*) DO (
	set "v_i=%%i"
	set v_i=!v_i:.svn=!
	if "!v_i!"=="%%i" FOR /F "usebackq tokens=2* delims=:" %%x IN (`FIND /C /I "��" "%%i"`) DO (
		SET "v_cnt=%%y"
		IF /I NOT "!v_cnt!"=="" IF !v_cnt! GTR 0 ECHO %%i
	)
)
EXIT /B

:ENDSCR
EXIT /B
