@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

rem ******************************************************************
rem Set parameters
rem ******************************************************************
SET "v_utilpath=%~d0%~p0"

SET "v_rlistpath=%~d1%~p1"
SET "v_rlistname=%~n1"
SET "v_rlistext=%~x1"

SET "v_ilistpath=%~d2%~p2"
SET "v_ilistname=%~n2"
SET "v_ilistext=%~x2"

IF /I "!v_rlistpath!"=="" EXIT /B 999

SET "c_UTIL=%~n0%~x0"
SET "c_SED=%v_utilpath%sed.exe"
SET "c_BTQ=BTEQ"

SET "v_modebtet=BTET"
SET "v_modeansi=ANSI"
SET "v_envind=${DB_ENV}"

SET "v_date=!DATE:-=!"
SET "v_time=!TIME::=!"
SET "v_time=!v_time: =0!"
SET "v_ver=!v_date:~2!!v_time:~0,6!"

SET "v_err=0"
SET v_quote="
SET "v_NEQ=<>"

SET "v_listrev="
SET "v_listurl="

SET "v_tmp_i0=0"
SET "v_tmp_r0=0"
FOR /F "usebackq tokens=1*" %%k IN ("!v_rlistpath!\.svn\entries") DO (
	SET /A v_tmp_r0+=1
rem 	ECHO !v_tmp_r0! : %%k
	IF /I "!v_tmp_r0!"=="2" IF /I "%%k"=="dir" SET "v_tmp_i0=1"
	IF /I "!v_tmp_r0!"=="3" IF /I "!v_tmp_i0!"=="1" SET "v_listrev=%%k"
	IF /I "!v_tmp_r0!"=="4" IF /I "!v_tmp_i0!"=="1" SET "v_listurl=%%k"
	IF /I "!v_tmp_r0!"=="5" GOTO ENDF1
)
:ENDF1
rem pause
FOR /D %%k IN ("%v_rlistpath:~0,-1%") DO SET "v_rlistparent=%%~nk"
FOR /F "tokens=1* delims=|" %%k IN ("!v_utilpath:\util\=\|!") DO SET "v_uroot=%%k"
FOR /F "tokens=1* delims=|" %%k IN ("!v_rlistpath:\code\=\|!") DO SET "v_croot=%%k"
FOR /F "tokens=1* delims=|" %%k IN ("!v_ilistpath:\code\=\|!") DO SET "v_iroot=%%k"
SET "v_ilist=!v_ilistpath!!v_ilistname!!v_ilistext!"
SET "v_rlist=!v_rlistpath!!v_rlistname!!v_rlistext!"

rem ******************************************************************
IF NOT EXIST "%v_rlist%" (
	ECHO.runorder: %v_rlistpath%%v_rlistname% does not exist!
	EXIT /B 998
)
IF NOT EXIST "%v_ilist%" (
	ECHO.includelist: %v_ilist% does not exist!
	EXIT /B 997
)


rem ******************************************************************
rem Get additional parameters
rem ******************************************************************
CLS
ECHO.==========================================================================
ECHO.Build and Execute a list of BTEQ scripts
ECHO.==========================================================================
ECHO.Parameters
ECHO.==========================================================================
ECHO.Dir (SVN-rev: !v_listrev!) SVN-url: !v_listurl!
ECHO.
ECHO.RunList: !v_rlistpath!!v_rlistname!!v_rlistext!
ECHO.
IF /I NOT "%v_ilist%"=="" (
	ECHO.IncludeList: !v_ilist!
)
ECHO. 
ECHO.==========================================================================
ECHO.Additional parameters (Please enter/modify. Default values in brackets)
ECHO.==========================================================================
SET /P v_env="ENV : "
SET /P v_PID="PID [edwt]: "
SET /P v_USR="USR [%v_env%dbadmin]: "
SET /P v_PWD="PWD [%v_env%dbadmin]: "
IF /I "%v_env%"=="" EXIT /B 999
IF /I "%v_PID%"=="" SET v_PID=edwt
IF /I "%v_USR%"=="" SET v_USR=%v_env%dbadmin
IF /I "%v_PWD%"=="" SET v_PWD=%v_env%dbadmin
SET "v_logon=.LOGON %v_PID%/%v_USR%,%v_PWD%"
ECHO.--------------------------------------------------------------------------
ECHO.Logon : %v_logon%
ECHO.--------------------------------------------------------------------------
ECHO. 
pause
rem ******************************************************************


rem ******************************************************************
rem Build run-file
rem ******************************************************************
SET "v_tmppath=%v_croot%tmp\%v_env%.%v_rlistparent%\"
SET "v_logpath=%v_croot%log\%v_env%.%v_rlistparent%\"
SET "v_tmplist=!v_quote!%v_tmppath%_runfile.%v_ver%.btq!v_quote!"
SET "v_outfile=!v_quote!%v_logpath%_runfile.out!v_quote!"
SET "v_logfile=!v_quote!%v_logpath%_runfile.%v_ver%.log!v_quote!"


rem ************************
rem Display run-information
ECHO.
ECHO.
ECHO.==========================================================================
ECHO.Building
ECHO.==========================================================================
ECHO.RunFile: %v_tmplist%
ECHO.
ECHO.RunList(s):
rem ************************

IF EXIST "%v_tmppath%" RMDIR /S/Q "%v_tmppath%">NUL
MKDIR "%v_tmppath%">NUL
IF NOT EXIST "%v_logpath%" MKDIR "%v_logpath%">NUL

ECHO.>%v_tmplist%
ECHO..SET MAXERROR 0 >>%v_tmplist%
ECHO..SET WIDTH 300 >>%v_tmplist%
ECHO..SET ERRORLEVEL UNKNOWN SEVERITY 16 >>%v_tmplist%
rem ECHO..SET ERRORLEVEL 3706 SEVERITY 8 >>%v_tmplist%
ECHO.>>%v_tmplist%

CALL :L_BUILDLIST "!v_rlistpath!!v_rlistname!!v_rlistext!" !v_modebtet!
IF !ERRORLEVEL! GEQ 1 ( SET "v_err=!ERRORLEVEL!" ) & ( ECHO.) & ( ECHO.BTEQ error: !v_err!) & ( EXIT /B !v_err! )
ECHO.>>%v_tmplist%
ECHO..LOGOFF>>%v_tmplist%
ECHO..EXIT ERRORCODE>>%v_tmplist%
rem ******************************************************************


rem ******************************************************************
rem Execute run-file
rem ******************************************************************

rem ************************
rem Display run-info
ECHO.
ECHO.
ECHO.==========================================================================
ECHO.Executing
ECHO.==========================================================================
ECHO.RunFile: %v_tmplist%
ECHO.
ECHO.OutFile: %v_outfile%
ECHO.
ECHO.LogFile: %v_logfile%
ECHO.
rem ************************

%c_BTQ% %v_logon% 0<%v_tmplist% 1>%v_outfile% 2>&1
IF !ERRORLEVEL! GEQ 1 ( SET "v_err=!ERRORLEVEL!" ) & ( ECHO.) & ( ECHO.BTEQ error: !v_err!) & (COPY /Y %v_outfile% %v_logfile%) & ( EXIT /B !v_err! )

COPY /Y %v_outfile% %v_logfile%>NUL
ECHO.
ECHO....completed
ECHO.
rem ******************************************************************


REM IF EXIST "%v_tmppath%" RMDIR /S/Q "%v_tmppath%">NUL

EXIT /B


rem ******************************************************************
rem local proc L_BUILDLIST
rem ******************************************************************
:L_BUILDLIST
SETLOCAL ENABLEDELAYEDEXPANSION
SET "v_rlist=%~1"
SET "v_rlist_path=%~p1"
FOR /D %%x IN ("!v_rlist_path:~0,-1!") DO SET "v_rlist_parent=%%~nx"
SET v_lastmode=%2
rem ************************
rem Display run-info
ECHO.   !v_rlist!
rem ************************

IF EXIST "!v_rlist!" FOR /F "usebackq tokens=1*" %%a IN ("!v_rlist!") DO (
	SET "v_runcmd=%%a"
	SET "v_btqcmd="
	SET "v_btqmode="
	IF /I "!v_runcmd:~0,10!"=="BTQCOMPILE" (
		IF /I "!v_runcmd:~10,5!"=="_ANSI" (
			SET "v_btqmode=!v_modeansi!" 
		) ELSE SET "v_btqmode=!v_modebtet!"
		SET "v_runcmd=BTQCOMPILE"
		SET "v_btqcmd=.COMPILE FILE"
	) ELSE IF /I "!v_runcmd:~0,6!"=="BTQRUN" (
		IF /I "!v_runcmd:~6,5!"=="_ANSI" (
			SET "v_btqmode=!v_modeansi!" 
		) ELSE SET "v_btqmode=!v_modebtet!"
		SET "v_runcmd=BTQRUN"
		SET "v_btqcmd=.RUN FILE"
	) ELSE IF /I NOT "!v_runcmd!"=="CD" SET "v_runcmd="
	
	IF /I NOT "!v_runcmd!"=="" (
		SET "v_srcobj=%%b"
		SET "v_srcfile=%v_croot%!v_srcobj:/=\!"
		SET "v_srcfile_path="
		SET "v_srcfile_parent="
		SET "v_srcfile_name="
		SET "v_srcfile_ext="
		SET "v_tmpfile="
		SET "v_include="
		IF /I "%v_ilist%"=="" (
			SET "v_include=1"
		) ELSE IF EXIST "%v_ilist%" (
			IF /I "!v_runcmd!"=="CD" FOR /F "usebackq tokens=1*" %%x IN (`FIND /C /I "!v_srcobj!" "%v_ilist%"`) DO (
				SET "v_include=%%y"
				SET "v_include=!v_include:*: =!"
				IF /I NOT "!v_include!"=="" IF !v_include! GTR 0 SET "v_include=1"
			) ELSE FOR /F "usebackq tokens=1*" %%k IN ("%v_ilist%") DO IF /I "%%l"=="!v_srcobj!" (
				SET "v_include=%%k"
				SET "v_include=!v_include:~0,1!"
				SET "v_include=!v_include:M=1!"
				SET "v_include=!v_include:A=1!"
			)
		)
		IF /I "!v_include!"=="1" (
			IF /I "!v_runcmd!"=="CD" (
				CALL :L_BUILDLIST "!v_srcfile!\_runorder.txt" !v_lastmode!
			) ELSE IF EXIST "!v_srcfile!" (
				FOR %%k IN ("!v_srcfile!") DO (
					SET "v_srcfile_name=%%~nk"
					SET "v_srcfile_ext=%%~xk"
					SET "v_srcfile_path=%%~pk"
					FOR /D %%l IN ("!v_srcfile_path:~0,-1!") DO SET "v_srcfile_parent=%%~nl"
				)
				SET "v_tmpfile=!v_quote!%v_tmppath%!v_rlist_parent!.!v_srcfile_parent!.!v_srcfile_name!.%v_ver%!v_srcfile_ext!!v_quote!"
				IF /I NOT "!v_btqmode!"=="!v_lastmode!" (
					SET "v_lastmode=!v_btqmode!"
					ECHO..LOGOFF>>%v_tmplist%
					ECHO..SET SESSION TRANSACTION !v_btqmode!>>%v_tmplist%
					ECHO.%v_logon%>>%v_tmplist%
				)
				ECHO.!v_btqcmd! !v_tmpfile!>>%v_tmplist%				
				%c_SED% -e "s/\!v_envind!/%v_env%/g" !v_srcfile!>!v_tmpfile!
				IF /I "!v_runcmd!"=="BTQCOMPILE" (
					ECHO..IF ACTIVITYCOUNT!v_NEQ!0 THEN .EXIT 5526>>%v_tmplist%
				)
				ECHO..IF ERRORCODE!v_NEQ!0 THEN .EXIT ERRORCODE>>%v_tmplist%
				IF /I "!v_btqmode!"=="!v_modeansi!" (
					ECHO.COMMIT;>>%v_tmplist%
					ECHO..IF ERRORCODE!v_NEQ!0 THEN .EXIT ERRORCODE>>%v_tmplist%
				)
			)
		)
	)
)
ENDLOCAL
:ENDSCR
EXIT /B
rem ******************************************************************

