#!/bin/ksh
############################################################################
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: backupRepos.sh 129 2011-09-07 08:23:59Z k9106728 $
# Last Changed By  : $Author:  
# Last Change Date : $Date: 2011-09-07 10:23:59 +0200 (ons, 07 sep 2011) $
# Last Revision    : $Revision: 129 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/infa/operate/backupRepos.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# PURPOSE: Backup Informatica security domain
# 
# REVISION HISTORY
# ==========================================================================
# INITIALS   DATE         COMMENT
# --------------------------------------------------------------------------
# SSU (TD)   2011-05-19   Initial version
#
# OBJECT REFERENCE INFORMATION
# ==========================================================================
# TYPE    ACTION    OBJECT NAME
# --------------------------------------------------------------------------
# 
############################################################################

# load all variables from profile
. $HOME/.infascm_profile

BACKUP_FILE_DIR=$HOME/infa_backup
NOW=$(date +"%Y%m%d_%H%M%S")
BACKUP_FILE_NAME=$BACKUP_FILE_DIR/${REPOSITORY_NAME}_$NOW.rep

$PMREP connect \
    -r $REPOSITORY_NAME \
    -d $DOMAIN_NAME \
    -n $REPOSITORY_USER_NAME \
    -s $SECURITY_DOMAIN_NAME \
    -X REPOSITORY_USER_PASSWORD
if [ $? -ne 0 ] ; then
    exit 1
fi

$PMREP backup \
    -o $BACKUP_FILE_NAME
if [ $? -ne 0 ] ; then
    exit 1
fi

$PMREP cleanup
if [ $? -ne 0 ] ; then
    exit 1
fi

gzip $BACKUP_FILE_NAME

exit 0


