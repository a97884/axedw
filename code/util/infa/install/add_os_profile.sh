#!/bin/ksh
############################################################################
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)                #
############################################################################
# $Id:: Install_User.sh 4409 2009-10-13 15:38:59Z U884175                  $
# $LastChangedBy:: U884175                                                 $
# $LastChangedDate:: 2009-10-13 17:38:59 +0200 (Di, 13 Okt 2009)           $
# $LastChangedRevision:: 4409                                              $
# $URL:  $
############################################################################
# SVN Info END                                                             #
############################################################################
#                                                                          #
# REVISION HISTORY                                                         #
# ======================================================================== #
# INITIALS   DATE       COMMENT                                            #
# ------------------------------------------------------------------------ #
# SSU (TD)   2009-10-12 Initial version                                    #
#                                                                          #
# OBJECT REFERENCE INFORMATION                                             #
# ======================================================================== #
# TYPE    ACTION    OBJECT NAME                                            #
# ------------------------------------------------------------------------ #
# NONE                                                                     #
############################################################################

############################################################################
# DESCRIPTION                                                              #
# ======================================================================== #
# Add Informatica database connections to one specific database for
#  - each environment of specified project
#  - each individual user
# Loader connections are only updated and must exist before script is started
############################################################################



############################################################################
# 1.) Initialization and variable declarations
############################################################################

# PWFK_INSTALL_DIR=`dirname $0`
PWFK_INSTALL_DIR="/home/uinfadom/scripts/infa_install"
. $PWFK_INSTALL_DIR/.install.profile
. $PWFK_INSTALL_DIR/install_func.sh

typeset -u I_NEW_USER_NAME
typeset -u I_ENV_NAME
typeset -u I_PROJECT_NAME
typeset -u I_DATABASE_NAME

I_PROJECT_NAME=$1
I_DATABASE_NAME=$2
PRJ_LIST=$PWFK_INSTALL_DIR/project.txt
USR_LIST=$PWFK_INSTALL_DIR/devuser.txt
ENV_LIST=$PWFK_INSTALL_DIR/environment.txt

#  Print script syntax and help
print_help ()
    {
    echo "Usage: `basename $0` <project> <dbname>"
    echo ""
    echo "   <project>      : project name the connection belongs to, e.g. EXPORT, IMPORT, SAMBA"
    echo "   <dbname>       : database name that need new connections"
    echo ""
    }

#  Standard procedure executed at every exit, with or w/o error
at_exit ()
    {
        echo ""
        echo "Log file can be found under $PWFK_INSTALL_LOG"
        echo ""
        echo "###############################################################################"
        echo "END `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-C
trap at_exit EXIT


############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# 2.) Check for correct syntax
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
# 3.) Start logging
############################################################################

NOW=$(date +"%Y%m%d_%H%M%S")
PWFK_INSTALL_LOG=$PWFK_INSTALL_DIR/log/add_db_conn_${I_PROJECT_NAME}_${I_DATABASE_NAME}_${NOW}.log
touch $PWFK_INSTALL_LOG
# re-route all output to screen and logfile simultaneously
tee $PWFK_INSTALL_LOG >/dev/tty |&
exec 1>&p
exec 2>&1


echo "###############################################################################"
echo "START `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter 1 (Project Name)                : $I_PROJECT_NAME"
echo "Parameter 2 (New Database Name)           : $I_DATABASE_NAME"
echo ""
echo "Variables used"
echo "INFACMD               : $INFACMD"
echo "PMREP                 : $PMREP"
echo "DOMAIN_NAME           : $DOMAIN_NAME"
echo "DOMAIN_USER_NAME      : $DOMAIN_USER_NAME"
echo "SECURITY_DOMAIN_NAME  : $SECURITY_DOMAIN_NAME"
echo "REPOSITORY_NAME       : $REPOSITORY_NAME"
echo "REPOSITORY_USER_NAME  : $REPOSITORY_USER_NAME"
echo "PWFK_INSTALL_DIR      : $PWFK_INSTALL_DIR"
echo "PWFK_INSTALL_LOG      : $PWFK_INSTALL_LOG"
echo ""



############################################################################
# 6.) DB connections
############################################################################

echo ""
echo "###############################################################################"
echo "# Define DB connections and permissions ..."
echo "###############################################################################"
echo ""

$PMREP connect -r $REPOSITORY_NAME -d $DOMAIN_NAME -n $REPOSITORY_USER_NAME -s $SECURITY_DOMAIN_NAME -x $REPOSITORY_USER_PASSWORD

# create connection for all environments in specified project
cat $ENV_LIST | while read I_ENV_NAME
do
    GROUP_VERSIONADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_versionadmins
    GROUP_FOLDERADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_folderadmins
    GROUP_CONNECTIONADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_connectionadmins
    GROUP_DEVELOPER=${I_ENV_NAME}_${I_PROJECT_NAME}_developers
    GROUP_OPERATOR=${I_ENV_NAME}_${I_PROJECT_NAME}_operators
    GROUP_QUERYADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_queryadmins
    GROUP_LABELADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_labeladmins
    GROUP_DEPLOYMENTGROUPADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_deploymentgroupadmins

    # Production database users don't have an environment prefix
    if [ "$I_ENV_NAME" = "PRD" ] ; then
        I_DB_USER=TRANS_${I_PROJECT_NAME}
        echo "Production environment - using database user $I_DB_USER"
    else
        I_DB_USER=TRANS_${I_ENV_NAME}_${I_PROJECT_NAME}
        echo "Non-production environment - using database user $I_DB_USER"
    fi
    
    CreateConnection $I_DATABASE_NAME

done


# create connection for all individual users
cat $USR_LIST | while read I_NEW_USER_NAME
do
    GROUP_VERSIONADMIN=DEV_${I_NEW_USER_NAME}_versionadmins
    GROUP_FOLDERADMIN=DEV_${I_NEW_USER_NAME}_folderadmins
    GROUP_CONNECTIONADMIN=DEV_${I_NEW_USER_NAME}_connectionadmins
    GROUP_DEVELOPER=DEV_${I_NEW_USER_NAME}_developers
    GROUP_OPERATOR=DEV_${I_NEW_USER_NAME}_operators
    GROUP_QUERYADMIN=DEV_${I_NEW_USER_NAME}_queryadmins
    GROUP_LABELADMIN=DEV_${I_NEW_USER_NAME}_labeladmins
    GROUP_DEPLOYMENTGROUPADMIN=DEV_${I_NEW_USER_NAME}_deploymentgroupadmins

    I_ENV_NAME=$I_NEW_USER_NAME
    I_DB_USER=$I_NEW_USER_NAME

    CreateConnection $I_DATABASE_NAME

done


############################################################################
# END OF SCRIPT                                                            #
############################################################################

$PMREP cleanup
exit 0
