

AddUserToGroup ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Adding user $I_ENV_NAME to relevant groups ..."
    echo "###############################################################################"
    echo ""
    $INFACMD isp addUserToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -ExistingUserName $I_ENV_NAME -GroupName $GROUP_OPERATOR
    $INFACMD isp addUserToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -ExistingUserName $I_ENV_NAME -GroupName $GROUP_CONNECTIONADMIN
    $INFACMD isp addUserToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -ExistingUserName $I_ENV_NAME -GroupName $GROUP_DEVELOPER
    $INFACMD isp addUserToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -ExistingUserName $I_ENV_NAME -GroupName $GROUP_DEPLOYER
    }

AssignPermissionToFolder ()
    {
    echo ""
    echo "############################################################################################"
    echo "# Assign permissions to folder $FOLDER_NAME"
    echo "############################################################################################"
    echo ""
    $PMREP AssignPermission -o folder -n $FOLDER_NAME -g $GROUP_CONNECTIONADMIN -p ""
    $PMREP AssignPermission -o folder -n $FOLDER_NAME -g $GROUP_DEVELOPER -p rwx
    $PMREP AssignPermission -o folder -n $FOLDER_NAME -g $GROUP_OPERATOR -p rx
    $PMREP AssignPermission -o folder -n $FOLDER_NAME -g $GROUP_DEPLOYER -p rw
    $PMREP AssignPermission -o folder -n $FOLDER_NAME -g $GROUP_READONLY -p r
    $PMREP AssignPermission -o folder -n $FOLDER_NAME -g Everyone -p ""
    echo "... done"
    echo ""
    }

AssignPermissionToSharedFolder ()
    {
    echo ""
    echo "############################################################################################"
    echo "# Assign permissions to shared folder $SHARED_FOLDER_NAME"
    echo "############################################################################################"
    echo ""
    $PMREP AssignPermission -o folder -n $SHARED_FOLDER_NAME -g $GROUP_CONNECTIONADMIN -p ""
    $PMREP AssignPermission -o folder -n $SHARED_FOLDER_NAME -g $GROUP_DEVELOPER -p rw
    $PMREP AssignPermission -o folder -n $SHARED_FOLDER_NAME -g $GROUP_OPERATOR -p r
    $PMREP AssignPermission -o folder -n $SHARED_FOLDER_NAME -g $GROUP_DEPLOYER -p rw
    $PMREP AssignPermission -o folder -n $SHARED_FOLDER_NAME -g $GROUP_READONLY -p r
    $PMREP AssignPermission -o folder -n $SHARED_FOLDER_NAME -g Everyone -p ""
    }

AssignPermissionToConnection ()
    {
    echo ""
    echo "############################################################################################"
    echo "# Assign groups to connection $CONNECTION_NAME - with appropriate -rwx- setting ..."
    echo "############################################################################################"
    echo ""
    $PMREP AssignPermission -o connection -n $CONNECTION_NAME -g $GROUP_CONNECTIONADMIN -p rw
    $PMREP AssignPermission -o connection -n $CONNECTION_NAME -g $GROUP_DEVELOPER -p rx
    $PMREP AssignPermission -o connection -n $CONNECTION_NAME -g $GROUP_OPERATOR -p rx
    $PMREP AssignPermission -o connection -n $CONNECTION_NAME -g $GROUP_DEPLOYER -p r
    $PMREP AssignPermission -o connection -n $CONNECTION_NAME -g $GROUP_READONLY -p r
    $PMREP AssignPermission -o connection -n $CONNECTION_NAME -g Everyone -p ""
    echo "... done"
    echo ""
    }

AssignPermissionToApplicationConnection ()
    {
    echo ""
    echo "############################################################################################"
    echo "# Assign groups to connection $CONNECTION_NAME - with appropriate -rwx- setting ..."
    echo "############################################################################################"
    echo ""
    $PMREP AssignPermission -o connection -t Application -n $CONNECTION_NAME -g $GROUP_CONNECTIONADMIN -p rw
    $PMREP AssignPermission -o connection -t Application -n $CONNECTION_NAME -g $GROUP_DEVELOPER -p rx
    $PMREP AssignPermission -o connection -t Application -n $CONNECTION_NAME -g $GROUP_OPERATOR -p rx
    $PMREP AssignPermission -o connection -t Application -n $CONNECTION_NAME -g $GROUP_DEPLOYER -p r
    $PMREP AssignPermission -o connection -t Application -n $CONNECTION_NAME -g $GROUP_READONLY -p r
    $PMREP AssignPermission -o connection -t Application -n $CONNECTION_NAME -g Everyone -p ""
    echo "... done"
    echo ""
    }

AssignPermissionToLoaderConnection ()
    {
    echo ""
    echo "############################################################################################"
    echo "# Assign groups to connection $CONNECTION_NAME - with appropriate -rwx- setting ..."
    echo "############################################################################################"
    echo ""
    $PMREP AssignPermission -o connection -t Loader -n $CONNECTION_NAME -g $GROUP_CONNECTIONADMIN -p rw
    $PMREP AssignPermission -o connection -t Loader -n $CONNECTION_NAME -g $GROUP_DEVELOPER -p rx
    $PMREP AssignPermission -o connection -t Loader -n $CONNECTION_NAME -g $GROUP_OPERATOR -p rx
    $PMREP AssignPermission -o connection -t Loader -n $CONNECTION_NAME -g $GROUP_DEPLOYER -p r
    $PMREP AssignPermission -o connection -t Loader -n $CONNECTION_NAME -g $GROUP_READONLY -p r
    $PMREP AssignPermission -o connection -t Loader -n $CONNECTION_NAME -g Everyone -p ""
    echo "... done"
    echo ""
    }

AssignRoleToGroup ()
    {
    echo ""
    echo "############################################################################################"
    echo "# Assign roles to groups ..."
    echo "############################################################################################"
    echo ""
    echo "Assign role $ROLE_DEVELOPER to group $GROUP_DEVELOPER"
    $INFACMD isp assignRoleToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_DEVELOPER -RoleName $ROLE_DEVELOPER -ServiceName $REPOSITORY_NAME
    echo "Assign role $ROLE_OPERATOR to group $GROUP_OPERATOR"
    $INFACMD isp assignRoleToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_OPERATOR -RoleName $ROLE_OPERATOR -ServiceName $REPOSITORY_NAME
    echo "Assign role $ROLE_CONNECTIONADMIN to group $GROUP_CONNECTIONADMIN"
    $INFACMD isp assignRoleToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_CONNECTIONADMIN -RoleName $ROLE_CONNECTIONADMIN -ServiceName $REPOSITORY_NAME
    echo "Assign role $ROLE_DEPLOYER to group $GROUP_DEPLOYER"
    $INFACMD isp assignRoleToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_DEPLOYER -RoleName $ROLE_DEPLOYER -ServiceName $REPOSITORY_NAME
    echo "Assign role $ROLE_READONLY to group $GROUP_READONLY"
    $INFACMD isp assignRoleToGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_READONLY -RoleName $ROLE_READONLY -ServiceName $REPOSITORY_NAME
    echo "... done"
    echo ""
    }

CreateFolder ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Create project folder $FOLDER_NAME"
    echo "###############################################################################"
    echo ""
    $PMREP createfolder -n $FOLDER_NAME -d "$FOLDER_NAME" -o $REPOSITORY_USER_NAME -a $SECURITY_DOMAIN_NAME -p 700
    echo "... done"
    echo ""
    }

CreateSharedFolder ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Create project shared folder $SHARED_FOLDER_NAME"
    echo "###############################################################################"
    echo ""
    $PMREP createfolder -n $SHARED_FOLDER_NAME -d "$SHARED_FOLDER_NAME" -o $REPOSITORY_USER_NAME -a $SECURITY_DOMAIN_NAME -p 700 -s
    echo "... done"
    echo ""
    }

CreateRole ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Define default roles ..."
    echo "###############################################################################"
    echo ""
    echo "Define role $ROLE_DEVELOPER"
    $INFACMD isp createRole -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -RoleName $ROLE_DEVELOPER -RoleDescription "Role for: Development in PowerCenter Repository"
    echo "Define role $ROLE_OPERATOR"
    $INFACMD isp createRole -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -RoleName $ROLE_OPERATOR -RoleDescription "Role for: Operations in PowerCenter Repository"
    echo "Define role $ROLE_CONNECTIONADMIN"
    $INFACMD isp createRole -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -RoleName $ROLE_CONNECTIONADMIN -RoleDescription "Role for: Connection Administration"
    echo "Define role $ROLE_DEPLOYER"
    $INFACMD isp createRole -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -RoleName $ROLE_DEPLOYER -RoleDescription "Role for: Deployment"
    echo "Define role $ROLE_READONLY"
    $INFACMD isp createRole -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -RoleName $ROLE_READONLY -RoleDescription "Role for: Read-only Access"
    echo "... done"
    echo ""
    }

CreateGroup ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Create default groups ..."
    echo "###############################################################################"
    echo ""
    echo "Create group $GROUP_CONNECTIONADMIN"
    $INFACMD isp createGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_CONNECTIONADMIN -GroupDescription "Connection Administration"
    echo "Create group $GROUP_DEVELOPER"
    $INFACMD isp createGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_DEVELOPER -GroupDescription "Developers"
    echo "Create group $GROUP_OPERATOR"
    $INFACMD isp createGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_OPERATOR -GroupDescription "Operators for Workflow-Execution"
    echo "Create group $GROUP_DEPLOYER"
    $INFACMD isp createGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_DEPLOYER -GroupDescription "Administrator for Deployment Activities"
    echo "Create group $GROUP_READONLY"
    $INFACMD isp createGroup -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -GroupName $GROUP_READONLY -GroupDescription "Read-only Activities"
    echo "... done"
    echo ""
    }

CreateUser ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Creating user $I_ENV_NAME ..."
    echo "###############################################################################"
    echo ""
    $INFACMD isp createUser -DomainName $DOMAIN_NAME -UserName $DOMAIN_USER_NAME -Password $DOMAIN_USER_PASSWORD -SecurityDomain $SECURITY_DOMAIN_NAME -NewUserName $I_ENV_NAME -NewUserPassword $I_ENV_NAME -NewUserFullName "Devlopment User" -NewUserDescription "Development User" -NewUserEMailAddress "k12345@axfood.se" -NewUserPhoneNumber "+46 8 12345"
    echo "... done"
    echo ""
    }

CreateApplicationConnection ()
    {
    DB_MAIN_NAME=$1
    # Production databases don't have a database prefix
    if [ "$I_ENV_NAME" = "PRD" ] ; then
        DATA_BASE_NAME=$DB_MAIN_NAME
    else
        DATA_BASE_NAME=${I_ENV_NAME}_${DB_MAIN_NAME}
    fi
    USER_NAME=$I_DB_USER
    USER_PASSWORD=$CONNECTION_PASSWORD
    CONNECTION_TYPE="Teradata FastExport Connection"
    PAR_DATABASE_NAME="Database Name"
    PAR_EXECUTABLE_NAME="Executable Name"
    
    CONNECTION_NAME=${I_DB_USER}@FX8_${DATA_BASE_NAME}
    echo ""
    echo "############################################################################################"
    echo "# Creating application connection $CONNECTION_NAME ..."
    echo "############################################################################################"
    echo ""
    CODE_PAGE=US-ASCII
    CC_SUCCESSFUL=0
    $PMREP createconnection -s "$CONNECTION_TYPE" -n $CONNECTION_NAME -u $USER_NAME -p $USER_PASSWORD -l $CODE_PAGE
    if [ $? -eq 0 ] ; then
        CC_SUCCESSFUL=1
    fi
    $PMREP updateconnection -t "$CONNECTION_TYPE" -s Application -d $CONNECTION_NAME -a tdpid -v $TDPID
    $PMREP updateconnection -t "$CONNECTION_TYPE" -s Application -d $CONNECTION_NAME -a "$PAR_DATABASE_NAME" -v $DATA_BASE_NAME
    $PMREP updateconnection -t "$CONNECTION_TYPE" -s Application -d $CONNECTION_NAME -a "$PAR_EXECUTABLE_NAME" -v call_fexp_utf8.sh
    echo "... done"
    echo ""
    if [ $CC_SUCCESSFUL -eq 1 ] ; then
        AssignPermissionToApplicationConnection
    fi

    }

CreateConnection ()
    {
    DB_MAIN_NAME=$1
    DATA_BASE_NAME=${I_ENV_NAME}${DB_MAIN_NAME}
    CONNECTION_TYPE="Teradata"
	if [ "$INSTALL_TYPE" = "INFAENV" ] ; then
		USER_PASSWORD=${CONNECTION_PASSWORD}
		USER_NAME=${DEFAULT_DATABASE_USER}
		CONNECTION_NAME=TDC_${DB_MAIN_NAME}
	else
		USER_PASSWORD=${I_ENV_NAME}
		USER_NAME=${I_ENV_NAME}
		CONNECTION_NAME=ZTDC_${I_ENV_NAME}${DB_MAIN_NAME}
	fi
    CODE_PAGE=UTF-8
    echo ""
    echo "############################################################################################"
    echo "# Creating connection $CONNECTION_NAME ..."
    echo "############################################################################################"
    echo ""
    CC_SUCCESSFUL=0
    $PMREP createconnection -s "$CONNECTION_TYPE" -n $CONNECTION_NAME -u $USER_NAME -p $USER_PASSWORD -b $DATA_BASE_NAME -a $DATA_SOURCE_NAME -l $CODE_PAGE -e "$CONNECTION_ENVIRONMENT_SQL" -f "$TRANSACTION_ENVIRONMENT_SQL"
    if [ $? -eq 0 ] ; then
        CC_SUCCESSFUL=1
    fi
    echo "... done"
    echo ""
    if [ $CC_SUCCESSFUL -eq 1 ] ; then
        AssignPermissionToConnection
    fi
    }

CreateTPTConnection ()
    {
    DB_MAIN_NAME=$1
    DATA_BASE_NAME=${I_ENV_NAME}${DB_MAIN_NAME}
	CONNECTION_TYPE="Teradata PT Connection"
	PAR_DATABASE_NAME="Database Name"
	if [ "$INSTALL_TYPE" = "INFAENV" ] ; then
		USER_PASSWORD=${CONNECTION_PASSWORD}
		USER_NAME=${DEFAULT_DATABASE_USER}
		CONNECTION_NAME=TPT_${DB_MAIN_NAME}
	else
		USER_PASSWORD=${I_ENV_NAME}
		USER_NAME=${I_ENV_NAME}
		CONNECTION_NAME=ZTPT_${I_ENV_NAME}${DB_MAIN_NAME}
	fi
    CODE_PAGE=UTF-8
    echo ""
    echo "############################################################################################"
    echo "# Creating connection $CONNECTION_NAME ..."
    echo "############################################################################################"
    echo ""
    CC_SUCCESSFUL=0
    $PMREP createconnection -s "$CONNECTION_TYPE" -n $CONNECTION_NAME -u $USER_NAME -p $USER_PASSWORD -l $CODE_PAGE
    if [ $? -eq 0 ] ; then
        CC_SUCCESSFUL=1
    fi
    echo "... done"
    echo ""
    if [ $CC_SUCCESSFUL -eq 1 ] ; then
		$PMREP updateconnection -t "$CONNECTION_TYPE" -s Relational -d $CONNECTION_NAME -a "TDPID" -v $TDPID
		$PMREP updateconnection -t "$CONNECTION_TYPE" -s Relational -d $CONNECTION_NAME -a "Database Name" -v $DATA_BASE_NAME
		$PMREP updateconnection -t "$CONNECTION_TYPE" -s Relational -d $CONNECTION_NAME -a "System Operator" -v "Update"
        AssignPermissionToConnection
    fi
    }

CreateDB2Connection ()
    {
    DB_MAIN_NAME=$1
    DATA_BASE_NAME=${I_ENV_NAME}${DB_MAIN_NAME}
	CONNECTION_TYPE="DB2"
	PAR_DATABASE_NAME="Database Name"
	ENV_PREFIX="`echo $I_ENV_NAME | cut -c1-2`"
	if [ "$INSTALL_TYPE" = "INFAENV" ] ; then
		USER_PASSWORD="${ENV_PREFIX}ods" # ${CONNECTION_PASSWORD}
		USER_NAME="${ENV_PREFIX}ods" # ${DEFAULT_DATABASE_USER}
		CONNECTION_NAME=DB2_${DB_MAIN_NAME}
	else
		USER_PASSWORD=${I_ENV_NAME}
		USER_NAME=${I_ENV_NAME}
		CONNECTION_NAME=ZDB2_${I_ENV_NAME}${DB_MAIN_NAME}
	fi
	
	CONNECTION_STRING="${ENV_PREFIX}ods1"
	
    CODE_PAGE=UTF-8
    echo ""
    echo "############################################################################################"
    echo "# Creating connection $CONNECTION_NAME ..."
    echo "############################################################################################"
    echo ""
    CC_SUCCESSFUL=0
    $PMREP createconnection -s "$CONNECTION_TYPE" -n $CONNECTION_NAME -u $USER_NAME -p $USER_PASSWORD -l $CODE_PAGE -c "$CONNECTION_STRING"
    if [ $? -eq 0 ] ; then
        CC_SUCCESSFUL=1
    fi
    echo "... done"
    echo ""
    if [ $CC_SUCCESSFUL -eq 1 ] ; then
		$PMREP updateconnection -t "$CONNECTION_TYPE" -s Relational -d $CONNECTION_NAME -a "Connection Environment SQL" -v "SET SCHEMA ARK_AXBO;" -c "$CONNECTION_STRING"
        AssignPermissionToConnection
    fi
    }

CreateUserConnection ()
    {
    DB_MAIN_NAME=$1
    DATA_BASE_NAME=${I_ENV_NAME}${DB_MAIN_NAME}
    CONNECTION_NAME=${I_DB_USER}@TDC_${DATA_BASE_NAME}
    USER_PASSWORD=$CONNECTION_PASSWORD
    CONNECTION_TYPE="Teradata"
    USER_NAME=$I_DB_USER
    CODE_PAGE=UTF-8
    echo ""
    echo "############################################################################################"
    echo "# Creating connection $CONNECTION_NAME ..."
    echo "############################################################################################"
    echo ""
    CC_SUCCESSFUL=0
    $PMREP createconnection -s $CONNECTION_TYPE -n $CONNECTION_NAME -u $USER_NAME -p $USER_PASSWORD -b $DATA_BASE_NAME -a $DATA_SOURCE_NAME -l $CODE_PAGE -e "$CONNECTION_ENVIRONMENT_SQL" -f "$TRANSACTION_ENVIRONMENT_SQL"
    if [ $? -eq 0 ] ; then
        CC_SUCCESSFUL=1
    fi
    echo "... done"
    echo ""
    if [ $CC_SUCCESSFUL -eq 1 ] ; then
        AssignPermissionToConnection
    fi
    }

CreateOSProfile ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Create OS profile $OS_PROFILE ..."
    echo "###############################################################################"
    echo ""
    $INFACMD isp createOSProfile -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -on $OS_PROFILE -sn $SYSTEM_USER_NAME -po \$PMRootDir=$OS_ROOT_DIR \$PMExtProcDir=$OS_ROOT_DIR/ExtProc
    echo "... done"
    echo ""
    }

UpdateFolderWithOSProfile ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Assign OS profile $OS_PROFILE to project folder $FOLDER_NAME ..."
    echo "###############################################################################"
    echo ""
    $PMREP modifyfolder -n $FOLDER_NAME -d $FOLDER_NAME -o $REPOSITORY_USER_NAME -a $SECURITY_DOMAIN_NAME -u $OS_PROFILE
    echo "... done"
    echo ""
    }

    
UpdateLoaderConnection ()
    {
    DB_MAIN_NAME=$1
    # Production databases don't have a database prefix
    if [ "$I_ENV_NAME" = "PRD" ] ; then
        DATA_BASE_NAME=$DB_MAIN_NAME
    else
        DATA_BASE_NAME=${I_ENV_NAME}_${DB_MAIN_NAME}
    fi
    USER_NAME=$I_DB_USER
    USER_PASSWORD=$CONNECTION_PASSWORD
    PAR_DATABASE_NAME="Database Name"
    PAR_EXECUTABLE_NAME="External Loader Executable"
    PAR_TRUNCATE="Truncate Target Table"

    #   Update Multiload connection with UTF8
    CODE_PAGE=UTF-8
    CONNECTION_TYPE="Teradata Mload External Loader"
    CONNECTION_NAME=${I_DB_USER}@ML8_${DATA_BASE_NAME}
    echo ""
    echo "############################################################################################"
    echo "# Setting code page for loader connection $CONNECTION_NAME ..."
    echo "############################################################################################"
    echo ""
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -u $USER_NAME -l $CODE_PAGE
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -a tdpid -v $TDPID
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -a "$PAR_EXECUTABLE_NAME" -v call_mload_utf8.sh
    CC_SUCCESSFUL=0
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -a "$PAR_DATABASE_NAME" -v $DATA_BASE_NAME
    if [ $? -eq 0 ] ; then
        CC_SUCCESSFUL=1
    fi
    echo "... done"
    echo ""
    if [ $CC_SUCCESSFUL -eq 1 ] ; then
        AssignPermissionToLoaderConnection
    fi
    
#   Update Fastload connection with UTF8
    CODE_PAGE=UTF-8
    CONNECTION_TYPE="Teradata FastLoad External Loader"
    CONNECTION_NAME=${I_DB_USER}@FL8_${DATA_BASE_NAME}
    echo ""
    echo "############################################################################################"
    echo "# Setting code page for loader connection $CONNECTION_NAME ..."
    echo "############################################################################################"
    echo ""
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -u $USER_NAME -l $CODE_PAGE
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -a tdpid -v $TDPID
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -a "$PAR_EXECUTABLE_NAME" -v call_fastload_utf8.sh
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -a "$PAR_TRUNCATE" -v "yes"
    CC_SUCCESSFUL=0
    $PMREP updateconnection -t "$CONNECTION_TYPE" -d $CONNECTION_NAME -s Loader -a "$PAR_DATABASE_NAME" -v $DATA_BASE_NAME
    if [ $? -eq 0 ] ; then
        CC_SUCCESSFUL=1
    fi
    echo "... done"
    echo ""
    if [ $CC_SUCCESSFUL -eq 1 ] ; then
        AssignPermissionToLoaderConnection
    fi
    
    }


AddRolePrivilege ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Assign privileges to default roles ..."
    echo "###############################################################################"
    echo ""
    echo  "Granting privileges to role $ROLE_DEVELOPER"
    echo ""
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Tools/Access Workflow Manager"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Tools/Access Workflow Monitor"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Tools/Access Designer"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Tools/Access Repository Manager"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Sources and Targets/Create, Edit, and Delete"
    # $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Sources and Targets/Manage Versions"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Design Objects/Create, Edit, and Delete"
    # $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Design Objects/Manage Versions"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Runtime Objects/Create, Edit, and Delete"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Runtime Objects/Monitor/Execute/Manage Execution"
	# $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Runtime Objects/Manage Versions"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st RS -pp "Global Objects/Create Queries"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st DOMAIN -pp "Tools/Access Informatica Administrator"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEVELOPER -st DOMAIN -pp "Tools/Access Monitoring"

    echo ""
    echo  "Granting privileges to role $ROLE_OPERATOR"
    echo ""
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_OPERATOR -st RS -pp "Tools/Access Workflow Manager"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_OPERATOR -st RS -pp "Tools/Access Workflow Monitor"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_OPERATOR -st RS -pp "Runtime Objects/Monitor/Execute/Manage Execution"

    echo ""
    echo  "Granting privileges to role $ROLE_CONNECTIONADMIN"
    echo ""
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_CONNECTIONADMIN -st RS -pp "Tools/Access Workflow Manager"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_CONNECTIONADMIN -st RS -pp "Global Objects/Create Connections"

    echo ""
    echo  "Granting privileges to role $ROLE_DEPLOYER"
    echo ""
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Tools/Access Workflow Manager"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Tools/Access Designer"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Tools/Access Repository Manager"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Sources and Targets/Create, Edit, and Delete"
    # $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Sources and Targets/Manage Versions"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Design Objects/Create, Edit, and Delete"
    # $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Design Objects/Manage Versions"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Runtime Objects/Create, Edit, and Delete"
	# $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Runtime Objects/Manage Versions"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Runtime Objects/Monitor"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Global Objects/Execute Deployment Groups"
    $INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Global Objects/Manage Deployment Groups"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Global Objects/Create Queries"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Global Objects/Create Labels"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st DOMAIN -pp "Tools/Access Informatica Administrator"

    echo ""
    echo  "Granting privileges to role $ROLE_READONLY"
    echo ""
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Tools/Access Workflow Manager"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Tools/Access Designer"
	$INFACMD isp addRolePrivilege -dn $DOMAIN_NAME -un $DOMAIN_USER_NAME -pd $DOMAIN_USER_PASSWORD -sdn $SECURITY_DOMAIN_NAME -rn $ROLE_DEPLOYER -st RS -pp "Tools/Access Repository Manager"

    echo ""
    echo "... done"
    echo ""

    }

CreateIntegrationService ()
    {
    echo ""
    echo "###############################################################################"
    echo "# Create Integration Service  $INTEGRATION_SERVICE_NAME ..."
    echo "###############################################################################"
    echo ""


    # Integration Service Options
    DATAMOVEMENTMODE=ASCII
    USEOPERATINGSYSTEMPROFILES=yes
    VALIDATEDATACODEPAGES=yes
    DATEDISPLAYFORMAT="YYYY-MON-DD HH24:MI:SS"
    TIMESTAMPLOG=Yes

    # Integration Service Process Options
    PMWORKLFLOWLOGDIR="$PMROOTDIR/WorkflowLogs"
    PMSTORAGEDIR="$PMROOTDIR/Storage"
    PMEXTPROCDIR="$PMROOTDIR/ExtProc"

    CODEPAGE_ID=106
    
    # Executing createIntegrationService command
    $INFACMD createIntegrationService \
            -dn "$DOMAIN_NAME" \
            -un "$DOMAIN_USER_NAME" \
            -pd "$DOMAIN_USER_PASSWORD" \
            -sn "$INTEGRATION_SERVICE_NAME" \
            -nn "$NODE_NAME" \
            -rs "$REPOSITORY_NAME" \
            -ru "$REPOSITORY_USER_NAME" \
            -so \
                    DataMovementMode="$DATAMOVEMENTMODE" \
                    UseOperatingSystemProfiles="$USEOPERATINGSYSTEMPROFILES" \
                    ValidateDataCodePages="$VALIDATEDATACODEPAGES" \
                    DateDisplayFormat="$DATEDISPLAYFORMAT" \
                    TimeStampLog="$TIMESTAMPLOG" \
            -po \
                    \$PMRootDir="$PMROOTDIR" \
                    \$PMWorkflowLogDir="$PMWORKLFLOWLOGDIR" \
                    \$PMStorageDir="$PMSTORAGEDIR" \
                    \$PMExtProcDir="$PMEXTPROCDIR" \
                    Codepage_ID="$CODEPAGE_ID" \
            -ln "$LICENSENAME" \

    echo "... done"
    echo ""
    }

    #        -rp "$REPOSIOTRY_USER_PASSWORD" \

