#!/bin/ksh
############################################################################
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)                #
############################################################################
# $Id:: Install_User.sh 4409 2009-10-13 15:38:59Z U884175                  $
# $LastChangedBy:: U884175                                                 $
# $LastChangedDate:: 2009-10-13 17:38:59 +0200 (Di, 13 Okt 2009)           $
# $LastChangedRevision:: 4409                                              $
# $URL:  $
############################################################################
# SVN Info END                                                             #
############################################################################
#                                                                          #
# REVISION HISTORY                                                         #
# ======================================================================== #
# INITIALS   DATE       COMMENT                                            #
# ------------------------------------------------------------------------ #
# SSU (TD)   2009-10-12 Initial version                                    #
#                                                                          #
# OBJECT REFERENCE INFORMATION                                             #
# ======================================================================== #
# TYPE    ACTION    OBJECT NAME                                            #
# ------------------------------------------------------------------------ #
# NONE                                                                     #
############################################################################

############################################################################
# DESCRIPTION                                                              #
# ======================================================================== #
# Create Informatica connections for one specific database and project,
# but for each development user and environment
############################################################################



############################################################################
# 1.) Initialization and variable declarations
############################################################################

# export PWFK_INSTALL_DIR=`dirname $0`
export PWFK_INSTALL_DIR="/home/uinfadom/scripts/infa_install"
. $PWFK_INSTALL_DIR/.install.profile
. $PWFK_INSTALL_DIR/install_func.sh

typeset -u I_NEW_USER_NAME
typeset -u I_ENV_NAME
typeset -u I_PROJECT_NAME
typeset -u I_DATABASE_NAME
typeset -l CONNECTION_SHORT

export I_CONN_TYPE=$1
export CONNECTION_SHORT=`echo $I_CONN_TYPE | cut -c1`
export I_PROJECT_NAME=$2
export I_DATABASE_NAME=$3
export PRJ_LIST=$PWFK_INSTALL_DIR/project.txt
export USR_LIST=$PWFK_INSTALL_DIR/devuser.txt
export ENV_LIST=$PWFK_INSTALL_DIR/environment.txt

#  Print script syntax and help
print_help ()
    {
    echo "Usage: `basename $0` <project> <dbname> <conntype>"
    echo "Create Informatica connections for one specific database and project,"
    echo "but for each development user and environment"
    echo "   <conntype>     : connection type - (d)atabase, (l)oader, (a)pplication"
    echo "   <project>      : project name the connection belongs to, e.g. EXPORT, IMPORT, SAMBA"
    echo "   <dbname>       : database name that need new connections"
    echo ""
    }

#  Standard procedure executed at every exit, with or w/o error
at_exit ()
    {
        echo ""
        echo "Log file can be found under $PWFK_INSTALL_LOG"
        echo ""
        echo "###############################################################################"
        echo "END `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-C
trap at_exit EXIT


############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# 2.) Check for correct syntax
############################################################################

ERROR_CODE=0

if [ $# -ne 3 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

case "$CONNECTION_SHORT" in 
    d|a|l)    # OK
        ;;    
    *)  echo ""
        echo "Invalid connection type: $I_CONN_TYPE - please choose one out of (d)atabase, (l)oader, (a)pplication"
        echo ""
        ERROR_CODE=2
        ;;
    esac

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
# 3.) Start logging
############################################################################

NOW=$(date +"%Y%m%d_%H%M%S")
export PWFK_INSTALL_LOG=$PWFK_INSTALL_DIR/log/add_db_conn_${I_PROJECT_NAME}_${I_DATABASE_NAME}_${NOW}.log
touch $PWFK_INSTALL_LOG
# re-route all output to screen and logfile simultaneously
tee $PWFK_INSTALL_LOG >/dev/tty |&
exec 1>&p
exec 2>&1


echo "###############################################################################"
echo "START `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter 1 (Connection Type)             : $I_CONN_TYPE"
echo "Parameter 1 (Project Name)                : $I_PROJECT_NAME"
echo "Parameter 2 (New Database Name)           : $I_DATABASE_NAME"
echo ""
echo "Variables used"
echo "INFACMD               : $INFACMD"
echo "PMREP                 : $PMREP"
echo "DOMAIN_NAME           : $DOMAIN_NAME"
echo "DOMAIN_USER_NAME      : $DOMAIN_USER_NAME"
echo "SECURITY_DOMAIN_NAME  : $SECURITY_DOMAIN_NAME"
echo "REPOSITORY_NAME       : $REPOSITORY_NAME"
echo "REPOSITORY_USER_NAME  : $REPOSITORY_USER_NAME"
echo "PWFK_INSTALL_DIR      : $PWFK_INSTALL_DIR"
echo "PWFK_INSTALL_LOG      : $PWFK_INSTALL_LOG"
echo ""
echo "CONNECTION_SHORT      : $CONNECTION_SHORT"
echo ""


############################################################################
# 4.) Create connections for all environments and individual users
############################################################################

echo ""
echo "###############################################################################"
echo "# Define connections and permissions ..."
echo "###############################################################################"
echo ""

$PMREP connect -r $REPOSITORY_NAME -d $DOMAIN_NAME -n $REPOSITORY_USER_NAME -s $SECURITY_DOMAIN_NAME -x $REPOSITORY_USER_PASSWORD

# create connection for all environment in specified project
echo ""
echo "reading ${ENV_LIST}:"
cat $ENV_LIST
echo ""
cat $ENV_LIST | while read I_ENV_NAME
do
    echo "###############################################################################"
    echo "###############################################################################"
    echo " BEGIN PROCESSING ENVIRONMENT $I_ENV_NAME"
    echo "###############################################################################"
    echo "###############################################################################"
    echo ""
    export GROUP_VERSIONADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_versionadmins
    export GROUP_FOLDERADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_folderadmins
    export GROUP_CONNECTIONADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_connectionadmins
    export GROUP_DEVELOPER=${I_ENV_NAME}_${I_PROJECT_NAME}_developers
    export GROUP_OPERATOR=${I_ENV_NAME}_${I_PROJECT_NAME}_operators
    export GROUP_QUERYADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_queryadmins
    export GROUP_LABELADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_labeladmins
    export GROUP_DEPLOYMENTGROUPADMIN=${I_ENV_NAME}_${I_PROJECT_NAME}_deploymentgroupadmins

    # Production database users don't have an environment prefix
    echo ""
    echo ""
    echo ""
    echo ""
    if [ "$I_ENV_NAME" = "PRD" ] ; then
        export I_DB_USER=TRANS_${I_PROJECT_NAME}
        echo "Production environment - using database user $I_DB_USER"
    else
        export I_DB_USER=TRANS_${I_ENV_NAME}_${I_PROJECT_NAME}
        echo "Non-production environment - using database user $I_DB_USER"
    fi
    echo ""
    echo ""
    echo ""
    echo ""
    
    case "$CONNECTION_SHORT" in 
        d)  # Database connection
            CreateConnection $I_DATABASE_NAME
            ;;    
        a)  # Application (TD FastExport) connection
            CreateApplicationConnection $I_DATABASE_NAME
            ;;    
        l)  # Loader connection (Update only - connection must exist already)
            UpdateLoaderConnection $I_DATABASE_NAME
            ;;    
        *) echo "Unknown Option: $CONNECTION_SHORT"
           exit 1;;
    esac
    echo ""
    echo "###############################################################################"
    echo "###############################################################################"
    echo " END PROCESSING ENVIRONMENT $I_ENV_NAME"
    echo "###############################################################################"
    echo "###############################################################################"
    echo ""

done


# create connection for all individual users
echo "reading ${USR_LIST}:"
cat $USR_LIST
cat $USR_LIST | while read I_NEW_USER_NAME
do
    echo ""
    echo "###############################################################################"
    echo "###############################################################################"
    echo "# BEGIN PROCESSING USER $I_NEW_USER_NAME"
    echo "###############################################################################"
    echo "###############################################################################"
    echo ""
    export GROUP_VERSIONADMIN=DEV_${I_NEW_USER_NAME}_versionadmins
    export GROUP_FOLDERADMIN=DEV_${I_NEW_USER_NAME}_folderadmins
    export GROUP_CONNECTIONADMIN=DEV_${I_NEW_USER_NAME}_connectionadmins
    export GROUP_DEVELOPER=DEV_${I_NEW_USER_NAME}_developers
    export GROUP_OPERATOR=DEV_${I_NEW_USER_NAME}_operators
    export GROUP_QUERYADMIN=DEV_${I_NEW_USER_NAME}_queryadmins
    export GROUP_LABELADMIN=DEV_${I_NEW_USER_NAME}_labeladmins
    export GROUP_DEPLOYMENTGROUPADMIN=DEV_${I_NEW_USER_NAME}_deploymentgroupadmins

    export I_ENV_NAME=$I_NEW_USER_NAME
    export I_DB_USER=$I_NEW_USER_NAME

    case "$CONNECTION_SHORT" in 
        d)  # Database connection
            CreateConnection $I_DATABASE_NAME
            ;;    
        a)  # Application (TD FastExport) connection
            CreateApplicationConnection $I_DATABASE_NAME
            ;;    
        l)  # Loader connection (Update only - connection must exist already)
            UpdateLoaderConnection $I_DATABASE_NAME
            ;;    
        *) echo "Unknown Option: $CONNECTION_SHORT"
           exit 1;;
    esac
    echo ""
    echo "###############################################################################"
    echo "###############################################################################"
    echo "END PROCESSING USER $I_NEW_USER_NAME"
    echo "###############################################################################"
    echo "###############################################################################"
    echo ""

done


############################################################################
# END OF SCRIPT                                                            #
############################################################################

$PMREP cleanup
exit 0
