#!/usr/bin/ksh
############################################################################
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)                #
############################################################################
# $Id:: install_stage.sh 9800 2013-08-20 12:18:47Z k9105194                $
# $LastChangedBy:: k9105194                                                $
# $LastChangedDate:: 2013-08-20 14:18:47 +0200 (tis, 20 aug 2013)          $
# $LastChangedRevision:: 9800                                              $
# $URL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/infa/install/install_stage.sh $
############################################################################
# SVN Info END                                                             #
############################################################################
#                                                                          #
# REVISION HISTORY                                                         #
# ======================================================================== #
# INITIALS   DATE       COMMENT                                            #
# ------------------------------------------------------------------------ #
# SSU (TD)   2009-10-12 Initial version                                    #
#                                                                          #
# OBJECT REFERENCE INFORMATION                                             #
# ======================================================================== #
# TYPE    ACTION    OBJECT NAME                                            #
# ------------------------------------------------------------------------ #
# NONE                                                                     #
############################################################################

############################################################################
# DESCRIPTION                                                              #
# ======================================================================== #
# Set up Informatica structures for a project within an environment
# Create Informatica roles, groups, folders, connections, OS profile
# including all required permissions.
############################################################################


############################################################################
# 1.) Initialization and variable declarations
############################################################################

typeset -l I_ENV_NAME
I_ENV_NAME=$1


TDINFA_INSTALL_DIR="/opt/devutil/infa/install"

case $I_ENV_NAME in
    --uv0|uv1|uv2|uv3|uv4|uv5|uv6|uv7|uv8|uv9|st0|st1|st2|st3|st4|st5|st6|st7|st8|st9|it|pr)
		INSTALL_TYPE=INFAENV
		. $TDINFA_INSTALL_DIR/.install.$I_ENV_NAME.profile
        ;;
    *)
        INSTALL_TYPE=USER
		. $TDINFA_INSTALL_DIR/.install.uv2.profile
esac

. $HOME/.infascm_profile
. $TDINFA_INSTALL_DIR/install_func.sh



#  Print script syntax and help
print_help ()
    {
    echo "Usage: `basename $0` <environment>"
    echo ""
    echo "   <environment>      : environment, e.g. uv1|uv2|st1|st2 or Informatica individual development user name"
    echo ""
    }

#  Standard procedure executed at every exit, with or w/o error
at_exit ()
    {
        echo ""
        echo "Log file can be found under $TDINFA_INSTALL_LOG"
        echo ""
        echo "###############################################################################"
        echo "END `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-C
trap at_exit EXIT


############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# 2.) Check for correct syntax
############################################################################

ERROR_CODE=0

if [ $# -ne 1 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi


if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
# 3.) Start logging
############################################################################

NOW=$(date +"%Y%m%d_%H%M%S")
TDINFA_INSTALL_LOG=$TDINFA_INSTALL_DIR/log/install_stage_${I_ENV_NAME}_${NOW}.log
touch $TDINFA_INSTALL_LOG

# re-route all output to screen and logfile simultaneously
tee $TDINFA_INSTALL_LOG >/dev/tty |&
exec 1>&p
exec 2>&1


echo "###############################################################################"
echo "START `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter 1 (Environment Name)            : $I_ENV_NAME"
echo ""
echo "Variables used"
echo "INFACMD               : $INFACMD"
echo "PMREP                 : $PMREP"
echo "DOMAIN_NAME           : $DOMAIN_NAME"
echo "DOMAIN_USER_NAME      : $DOMAIN_USER_NAME"
echo "SECURITY_DOMAIN_NAME  : $SECURITY_DOMAIN_NAME"
echo "REPOSITORY_NAME       : $REPOSITORY_NAME"
echo "REPOSITORY_USER_NAME  : $REPOSITORY_USER_NAME"
echo "TDINFA_INSTALL_DIR    : $TDINFA_INSTALL_DIR"
echo "TDINFA_INSTALL_LOG    : $TDINFA_INSTALL_LOG"
echo ""


############################################################################
# 4.) Groups, roles & privileges definition
############################################################################

echo ""
echo "###############################################################################"
echo "###############################################################################"
echo "# Create Security Domain Objects"
echo "###############################################################################"
echo "###############################################################################"
echo ""

# Role definition
ROLE_CONNECTIONADMIN=connectionadmin
ROLE_DEVELOPER=developer
ROLE_OPERATOR=operator
ROLE_DEPLOYER=deployer
ROLE_READONLY=readonly

# Uncomment only, if roles or privileges change
# CreateRole
# AddRolePrivilege

	
# Group definition
GROUP_CONNECTIONADMIN=${I_ENV_NAME}_connectionadmin
GROUP_DEVELOPER=${I_ENV_NAME}_developer
GROUP_OPERATOR=${I_ENV_NAME}_operator
GROUP_DEPLOYER=${I_ENV_NAME}_deployer
GROUP_READONLY=${I_ENV_NAME}_readonly

CreateGroup
AssignRoleToGroup

# User definition
if [ "$INSTALL_TYPE" = "USER" ] ; then

	CreateUser
	AddUserToGroup

fi


############################################################################
# 5.) Folder definition
############################################################################

echo ""
echo "###############################################################################"
echo "###############################################################################"
echo "# Create Repository Global Objects"
echo "###############################################################################"
echo "###############################################################################"
echo ""

$PMREP connect -r $REPOSITORY_NAME -d $DOMAIN_NAME -n $REPOSITORY_USER_NAME -s $SECURITY_DOMAIN_NAME -X REPOSITORY_USER_PASSWORD


if [ "$INSTALL_TYPE" = "INFAENV" ] ; then

	SHARED_FOLDER_NAME=global_shared
	CreateSharedFolder
	AssignPermissionToSharedFolder

	OLD_FOLDERS=$TDINFA_INSTALL_DIR/log/install_stage_oldfolders_${I_ENV_NAME}_${NOW}.lst
	NEW_FOLDERS=$TDINFA_INSTALL_DIR/log/install_stage_newfolders_${I_ENV_NAME}_${NOW}.lst
	sort $TDINFA_INSTALL_DIR/folder.txt >"$NEW_FOLDERS"
	ADD_FOLDERS=$TDINFA_INSTALL_DIR/log/install_stage_addfolders_${I_ENV_NAME}_${NOW}.lst
	CMD_FILE=$TDINFA_INSTALL_DIR/log/install_stage_listobjects_${I_ENV_NAME}_${NOW}.cmd
	echo "listobjects -o Folder" >$CMD_FILE
	echo "exit" >>$CMD_FILE
	$PMREP run -f "$CMD_FILE" -o "$OLD_FOLDERS"
	sort "$OLD_FOLDERS" | grep -v '^\.' >"$OLD_FOLDERS".srt
	diff "$OLD_FOLDERS".srt "$NEW_FOLDERS" | grep "^>" | cut -c 3- | sort >"$ADD_FOLDERS"

	cat "$ADD_FOLDERS" | while read FOLDER_NAME
	do
			echo CreateFolder $FOLDER_NAME
			echo AssignPermissionToFolder
	done

	cat "$ADD_FOLDERS" | while read FOLDER_NAME
	do
		CreateFolder
		AssignPermissionToFolder
	done

else

	FOLDER_NAME=z_${I_ENV_NAME}
	CreateFolder
	AssignPermissionToFolder

fi

############################################################################
# 6.) Define DB connections and permissions
############################################################################

echo ""
echo "###############################################################################"
echo "# Define DB connections and permissions ..."
echo "###############################################################################"
echo ""

# relational TD connections
cat $TDINFA_INSTALL_DIR/tdc_connections.txt | while read I_CONN_NAME
do
	CreateConnection $I_CONN_NAME
done

# TPT connections
cat $TDINFA_INSTALL_DIR/tpt_connections.txt | while read I_CONN_NAME
do
	CreateTPTConnection $I_CONN_NAME
done

# Db2 connections
cat $TDINFA_INSTALL_DIR/db2_connections.txt | while read I_CONN_NAME
do
	CreateDB2Connection $I_CONN_NAME
done


############################################################################
# 7.) Define OS profile
############################################################################

# Dev, system test, integration test & production don't need OS profiles and additional integration services
case `echo $I_ENV_NAME | cut -c1-2` in
    --uv|"st"|"it"|"pr")
        exit 0
        ;;
    *)
        ;;
esac


# otherwise continue
if [ "$INSTALL_TYPE" = "USER" ] ; then
    OS_PROFILE="$FOLDER_NAME"
    SYSTEM_USER_NAME="$I_ENV_NAME"
    OS_ROOT_DIR="/centralhome/$I_ENV_NAME/infa_shared"
	CreateOSProfile
	UpdateFolderWithOSProfile
fi



############################################################################
# 8.) Create Integration Service
############################################################################

if [ "$INSTALL_TYPE" = "USER" ] ; then

    PMROOTDIR="$OS_ROOT_DIR"
    INTEGRATION_SERVICE_NAME="is_$I_ENV_NAME"
    CreateIntegrationService

fi

############################################################################
# END OF SCRIPT                                                            #
############################################################################

$PMREP cleanup
exit 0
