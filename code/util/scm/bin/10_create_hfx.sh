#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: 10_create_hfx.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author:  
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/10_create_hfx.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : Create hotfix branch from release tag
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2008-11-21 S.Sutter       Initial version 
# 2011-07-05 S.Sutter       Adapt to Axfood specifics
# 2011-07-05 S.Sutter       getopts parameter handling, change hotfix creation logic
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# Create hotfix branch from release tag
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Determine source release tag and new hotfix number
# 5.) Check if a hotfix branch from the same release exists already. If yes then exit
# 6.) Determine file names
# 7.) Copy release tag to new hotfix branch
# 
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

function print_help
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-h]"
    echo "Create hotfix branch from release tag"
    echo "       [-h]               : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.rel-$TARGET_HOTFIX.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

#---------------------------------------------------------------------------
# 2.) Initialization
#---------------------------------------------------------------------------

# get parameters
USAGE="h"
while getopts "$USAGE" opt; do
    case $opt in
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo ""
echo "###############################################################################"
echo "START ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Variables used:"
echo "###############################################################################"
echo ""
echo "SVN repository URL    : $SVN_BASE_URL"
echo "Local temp path       : $BUILD_TMP_PATH"
echo "Local logging path    : $BUILD_LOG_PATH"
echo ""


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

# no syntax checks required
if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    check_error 1
fi


#---------------------------------------------------------------------------
# 4.) Determine source release tag and new hotfix number
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Determine source release tag and new hotfix number"
echo "###############################################################################"
echo ""

# Check if there is a release that can be used as a hotfix source
RELEASE_CNT=`$SVN list $SVN_BASE_URL/tags/rel | grep "^rel-" | sort | tail -1 | wc -l` 2>/dev/null
# Throw error, if no release exists
if  [ $RELEASE_CNT -ne 1 ] ; then
    echo "No release tag exists. Exiting"
    check_error 2
else
    SOURCE_RELEASE_TAG=`$SVN list $SVN_BASE_URL/tags/rel | grep "^rel-" | sort | tail -1 | sed -e "s/\/$//g"`
    SOURCE_RELEASE_NO=`echo $SOURCE_RELEASE_TAG | cut -c 5-23`
    echo "Found latest release tag $SOURCE_RELEASE_TAG"
fi

COPY_SOURCE_URL=$SVN_BASE_URL/tags/rel/$SOURCE_RELEASE_TAG

generate_new_release_number "hotfix" "$SOURCE_RELEASE_NO"
TARGET_HOTFIX=$NEW_RELEASE_NUMBER
TARGET_HOTFIX_BRANCH="rel-$TARGET_HOTFIX.$SCRIPT_START_DATE"
TARGET_HOTFIX_URL="$SVN_BASE_URL/branches/hfx/$TARGET_HOTFIX_BRANCH"

echo "... done"
echo ""


#---------------------------------------------------------------------------
# 5.) Check if a hotfix branch from the same release exists already. If yes then exit
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Checking if last hotfix branch was properly tagged"
echo "###############################################################################"
echo ""

echo " ..."
# check first, if release directory exists - if not then OK
UNTAGGED_CNT=`$SVN list $SVN_BASE_URL/branches/hfx | grep "$TARGET_HOTFIX" | wc -l`

# if any non-tagged branch exists, then throw error and exit
if [ $UNTAGGED_CNT -ne 0 ] ; then
    echo "Hotfix branch $TARGET_HOTFIX already exists!!!"
    echo "Branch name is "
    $SVN list $SVN_BASE_URL/branches/hfx | grep "$TARGET_HOTFIX"
    check_error $?
    echo "Either delete that branch manually or tag (and thus move) it by using 11_tag_hotfix.sh!"
    echo "... exiting."
    check_error 4
else
    echo "    OK. Continuing."
fi
echo ""
echo "... done"

echo ""
echo "New hotfix number     : $TARGET_HOTFIX"
echo "New hotfix branch     : $TARGET_HOTFIX_BRANCH"
echo ""


#---------------------------------------------------------------------------
# 6.) Determine file names
#---------------------------------------------------------------------------

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$TARGET_HOTFIX.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt

echo "Final log file        : $FINAL_LOG_FILE"
echo "Temp log message file : $LOG_MSG_FILE"
echo ""


#---------------------------------------------------------------------------
# 7.) Copy release tag to new hotfix branch
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Create new hotfix branch"
echo "###############################################################################"
echo ""

COPY_TARGET_URL=$TARGET_HOTFIX_URL

echo "Copy Source URL       : $COPY_SOURCE_URL/code"
echo "Copy Target URL       : $COPY_TARGET_URL/code"
echo ""

# set SVN log message and do some logging
echo "Copying release tag $SOURCE_RELEASE_TAG from tags/rel to branches/hfx/$TARGET_HOTFIX_BRANCH" > $LOG_MSG_FILE
cat $LOG_MSG_FILE

$SVN copy $COPY_SOURCE_URL/code $COPY_TARGET_URL/code --parents --file $LOG_MSG_FILE 2>&1
check_error $?
echo ""
echo "... done"
echo ""


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
