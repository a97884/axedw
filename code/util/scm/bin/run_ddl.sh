#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: run_ddl.sh 13900 2014-09-16 14:00:36Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2014-09-16 16:00:36 +0200 (tis, 16 sep 2014) $
# Last Revision    : $Revision: 13900 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/run_ddl.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : 
# Project     : 
# Subproject  : 
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2011-03-09  	S.Sutter        Initial version
# --------------------------------------------------------------------------
# Description
#   
#
# Dependencies
#   parent  : 
#
# Parameters:
# 1 - name of file to process
# 2 - session mode {BTET,ANSI,null} (optional, default = system default)
#
# Variables (please add any other variables that you may use):
#   XYZ     : description
#   
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Initialize all variables from ini files and define base functions
# 2.) Initialization
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
SESSION_MODE=""
BTEQ=/opt/devutil/bteq
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
DDL_INPUT_COMPLETE="$1"
test $# = 2 && SESSION_MODE="$2"

DDL_INPUT_FILE=`basename $DDL_INPUT_COMPLETE`
DDL_TMP_PATH=$HOME/tmp
DDL_LOG_PATH=$HOME/tmp

DDL_TMP_FILE=${DDL_TMP_PATH}/${DDL_INPUT_FILE}.${SCRIPT_START_DATE}.tmp
DDL_LOG_FILE=${DDL_LOG_PATH}/${DDL_INPUT_FILE}.${SCRIPT_START_DATE}.log

cat /dev/null >${DDL_TMP_FILE}
test "$SESSION_MODE" != "" && echo ".SET SESSION TRANSACTION $SESSION_MODE" >>${DDL_TMP_FILE}
echo ".run file $HOME/.db_logon"                            >> ${DDL_TMP_FILE}
echo ".set width 300"                                       >> ${DDL_TMP_FILE}
cat ${DDL_INPUT_COMPLETE} | sed -e "s/\${DB_ENV}/$DB_ENV/g" >> ${DDL_TMP_FILE}
test "$SESSION_MODE" = "ANSI" && echo "COMMIT WORK;" >>${DDL_TMP_FILE}

$BTEQ < ${DDL_TMP_FILE} > ${DDL_LOG_FILE} 2>&1
stat=$?

ERROR_PATTERN='^SPL[0-9][0-9][0-9][0-9]:E'
WARNING_PATTERN='^SPL[0-9][0-9][0-9][0-9]:W'
if grep -i '^.COMPILE' ${DDL_TMP_FILE} >/dev/null
then
        loc_stat=0
        if grep "$WARNING_PATTERN" ${DDL_LOG_FILE} >/dev/null
        then
                echo "-------------- WARNINGS DURING COMPILE OF PROCEDURE --------------" >&2
                loc_stat=2
        fi
        if grep "$ERROR_PATTERN" ${DDL_LOG_FILE} >/dev/null
        then
                echo "************** ERRORS DURING COMPILE OF PROCEDURE **************" >&2
                loc_stat=1
        fi
        if [ $loc_stat -gt 0 ]
        then
                egrep -i "^.COMPILE|$WARNING_PATTERN|$ERROR_PATTERN" ${DDL_LOG_FILE} >&2
        fi
        if [ $loc_stat -eq 1 ]
        then
                stat=5526
        fi
fi

case $stat in
4)
        echo "Warnings during BTEQ execution, please check logfile: ${DDL_LOG_FILE}" >&2
        ;;
8|16)
        echo "Errors during BTEQ execution, please check logfile: ${DDL_LOG_FILE}" >&2
        ;;
esac

exit $stat
