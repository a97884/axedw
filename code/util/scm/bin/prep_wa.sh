#!/usr/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID                   : $Id: prep_wa.sh 31255 2020-04-24 15:52:57Z  $
# Last ChangedBy       : $Author: $
# Last ChangedDate     : $Date: 2020-04-24 17:52:57 +0200 (fre, 24 apr 2020) $
# Last ChangedRevision : $Revision: 31255 $
# Subversion URL       : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/prep_wa.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : Deploy build into test environment from any SVN tag
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2012-06-30 S.Sutter       Initial version
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# Deploy build into test environment from SVN tag
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -p        Path                yes         yes
# -e        Environment         yes         yes
# -r        Revision number     yes         no
# -i        Perform Infa import no          no          Defaults to "no import", if not provided
# -d        Apply DB changes    no          no          Defaults to "don't apply changes", if not provided
# -s        Restart             no          no
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
#  1.) Initialization
#  2.) Initialize other variables
#  3.) Check for correct syntax
#  4.) Display parameters and variables
#  5.) Check if source path parameter is a valid SVN URL
#  6.) Get SVN URL previously deployed to work area
#  7.) Check if new deployment path is different from previous deployment
#  8.) Get difference between previous and current deployment
#  9.) Get code from Subversion build tag and update work area
# 10.) get list of changed DDLs and display them for information purpose
#      execute the changes in the database
# 11.) get a list of changed Informatica objects and import them 
# 12.) make shure the Informatica parameter file contains an entry for the database environment 
#
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.)
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialization
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
#Prereq: the following two lines must go into .profile for the user:
#. $HOME/.scm_profile
#. $HOME/.infascm_profile
. basefunc.sh
. infafunc.sh

typeset -l ENVNAME


#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh -p <sourcepath> -e <environment> [-r <revision>] [-i] [-d] [-s] [-h]"
    echo "Deyploy code from any SVN path"
    echo "      -p <sourcepath>  : SVN path to be deployed, starting with branches, tags, or trunk"
    echo "      -e <environment> : environment name (uv0, st2, ...)"
    echo "      [-r <revision>]  : SVN revision of sourcepath"
    echo "      [-i]             : import Informatica objects - defaults to \"no import\", if not provided"
    echo "      [-d]             : apply DB changes - defaults to \"don't apply changes\", if not provided"
    echo "      [-s]             : Restart the script and skip steps already performed"
    echo "      [-h]             : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured.
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialize other variables
#---------------------------------------------------------------------------

RUN_INFA_IMPORT="false"
RUN_DB_APPLY="false"
RESTART_FLAG="false"
RESTART_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.restart.txt

# get parameters
USAGE="p:r:e:idsh"
while getopts "$USAGE" opt; do
    case $opt in
        p  ) SOURCE_PATH="$OPTARG"
             ;;
        r  ) REV_NO="$OPTARG"
             REVISION="@$REV_NO"
             OPT_ISSET_REVISION="true"
             ;;
        e  ) ENVNAME="$OPTARG"
             ;;
        i  ) RUN_INFA_IMPORT="true"
             ;;
        d  ) RUN_DB_APPLY="true"
             ;;
        s  ) if [ -e "$RESTART_FILE" ] ; then
                 RESTART_FLAG="true"
             fi
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# some standard variables required to run this script
FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$SCRIPT_START_DATE.log
STATUS_CHANGED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$SCRIPT_START_DATE.changed_items.txt
INFA_CHANGED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$SCRIPT_START_DATE.infa_changed.txt
INFA_DELETED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$SCRIPT_START_DATE.infa_deleted.txt
DB_CHANGED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$SCRIPT_START_DATE.db_changed.txt
DB_DELETED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$SCRIPT_START_DATE.db_deleted.txt
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt
touch $LOG_MSG_FILE


# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

if [ ! "$SOURCE_PATH" ] ; then
        echo "Error: Parameter -p (source path) not provided!"
        ERROR_CODE=2
fi
if [ ! "$ENVNAME" ] ; then
        echo "Error: Parameter -e (environment name) not provided!"
        ERROR_CODE=2
fi

if [ $ERROR_CODE -ne 0 ] ; then
    echo ""
    print_help
    check_error $ERROR_CODE
fi


#---------------------------------------------------------------------------
# 4.) Display parameters and variables
#---------------------------------------------------------------------------

echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""

if [ "$RESTART_FLAG" == "true" ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Restart parameter provided - will use restart file $RESTART_FILE:"
    echo "###############################################################################"
    cat $RESTART_FILE
    echo "###############################################################################"
    echo ""
    f_get_par_from_restart_file $RESTART_FILE SOURCE_PATH
    SOURCE_PATH=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE REV_NO
    REV_NO=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE REVISION
    REVISION=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE ENVNAME
    ENVNAME=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE RUN_INFA_IMPORT
    RUN_INFA_IMPORT=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE RUN_DB_APPLY
    RUN_DB_APPLY=$GLOBAL_RETURN_VALUE

    f_get_variable_from_restart_file "$RESTART_FILE" SVNSOURCEURL
    SVNSOURCEURL=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" CUR_BUILD_NO
    CUR_BUILD_NO=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" CUR_BUILD_ENV
    CUR_BUILD_ENV=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PREV_SVNSOURCEURL
    PREV_SVNSOURCEURL=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PRV_BUILD_NO
    PRV_BUILD_NO=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PRV_BUILD_ENV
    PRV_BUILD_ENV=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PREV_BUILD_CNT
    PREV_BUILD_CNT=$GLOBAL_RETURN_VALUE

else
    rm -f $RESTART_FILE
fi

echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -p (Source URL)       : $SOURCE_PATH"
echo "Parameter -r (Source revision)  : $REV_NO"
echo "Parameter -e (Environment)      : $ENVNAME"
echo "Parameter -i (Run Infa import)  : $RUN_INFA_IMPORT"
echo "Parameter -d (Apply DB changes) : $RUN_DB_APPLY"
echo "Parameter -s (Restart)          : $RESTART_FLAG"
echo ""
echo ""
echo "SVN repository base path        : $SVN_BASE_URL"
echo "Local logging path              : $BUILD_LOG_PATH"
echo "Final log file                  : $FINAL_LOG_FILE"
echo ""


# The initial checks are not required if the script is called with the restart parameter
# in that case the required variable values will be taken from the restart file
f_check_restart_point "$RESTART_FILE" INITIALCHECK
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then


#---------------------------------------------------------------------------
# 5.) Check if source path parameter is a valid SVN URL
#---------------------------------------------------------------------------

    echo ""
    echo "###############################################################################"
    echo "# Check if source path parameter is a valid SVN URL"
    echo "###############################################################################"
    echo ""

    # if 'code' is not provided in source path paramater, add it to url path 
    if [ `basename "${SOURCE_PATH}"` == "code" ] ; then
        SVNSOURCEURL="${SVN_BASE_URL}/${SOURCE_PATH}"
    else
        SVNSOURCEURL="${SVN_BASE_URL}/${SOURCE_PATH}/code"
    fi

    # Check if source path parameter is a valid SVN URL
    if [ $( $SVN info "${SVNSOURCEURL}${REVISION}" 2>&1 | grep -i "Not a valid URL" | wc -l ) -ne 0 ] ; then
        echo "${SVNSOURCEURL}${REVISION} not found!"
        echo "Cannot update to this URL."
        echo "Exiting ..."
        exit 1
    else
        echo "OK. URL exists"
        echo ""
    fi

    if [ ! "$OPT_ISSET_REVISION" ] ; then
        echo "Determining latest revision, since no revision was specified ..."
        REV_NO=`$SVN info $SVNSOURCEURL | grep Revision | cut -d" " -f2`
        REVISION="@${REV_NO}"
        echo "... latest revision for $SVNSOURCEURL is $REV_NO"
        echo ""
    fi


#---------------------------------------------------------------------------
# 6.) Get SVN URL previously deployed to work area
#---------------------------------------------------------------------------

    echo ""
    echo "###############################################################################"
    echo "# Get SVN URL previously deployed to work area"
    echo "###############################################################################"
    echo ""

    # Use directory $INFA_SHARED_DIR as reference and get SVN URL which is currently deployed.
    # First, check if directory exists at all
    if [ -d "$INFA_SHARED_DIR" ] ; then
        # get URL from work area
        PREV_DEPLOY_URL=`$SVN info $INFA_SHARED_DIR | grep URL:  | cut -d" " -f2`
        # get URL parent directory, since infa_xml is just a sub directory
        PREV_DEPLOY_URL=`dirname $PREV_DEPLOY_URL`
        # also get revision number
        PREV_DEPLOY_REV=`$SVN info $INFA_SHARED_DIR | grep Revision:  | cut -d" " -f2`
        PREV_SVNSOURCEURL="${PREV_DEPLOY_URL}@${PREV_DEPLOY_REV}"

        # check if URL found is still an actual URL
        if [ $( $SVN info "${PREV_SVNSOURCEURL}"  2>&1 | grep -i "Not a valid URL" | wc -l ) -ne 0 ] ; then
            echo "Previous deployment URL ${PREV_DEPLOY_URL}@${PREV_DEPLOY_REV} cannot be found!"
            echo "Thus considering all items as new."
            PREV_BUILD_CNT=0
        else
            echo "Previous deployment URL ${PREV_DEPLOY_URL}@${PREV_DEPLOY_REV} found."
            PREV_BUILD_CNT=1
        fi

    else
        PREV_BUILD_CNT=0
    fi

    # get build number and environment from SVN URL
    f_extract_build_number $SVNSOURCEURL
    CUR_BUILD_NO=$R_BUILD_NO
    CUR_BUILD_ENV=$R_ENV_MAIN

    # get build number and environment previously deployed from SVN URL
    f_extract_build_number $PREV_SVNSOURCEURL
    PRV_BUILD_NO=$R_BUILD_NO
    PRV_BUILD_ENV=$R_ENV_MAIN


    echo ""
    echo "SVN deploy URL            : $SVNSOURCEURL"
    echo "Build no. to be deployed  : $CUR_BUILD_NO"
    echo "Build env to be deployed  : $CUR_BUILD_ENV"
    echo ""
    echo "Previous SVN deploy URL   : $PREV_SVNSOURCEURL"
    echo "Previous build no.        : $PRV_BUILD_NO"
    echo "Previous build env        : $PRV_BUILD_ENV"
    echo ""


#---------------------------------------------------------------------------
# 7.) Check if new deployment path is different from previous deployment
#---------------------------------------------------------------------------

    echo ""
    echo "###############################################################################"
    echo "# Check if new deployment path is different from previous deployment"
    echo "###############################################################################"
    echo ""

    # if previous and current build are identical then do nothing and exit
    if [ "${PREV_SVNSOURCEURL}" = "${SVNSOURCEURL}${REVISION}" ] ; then
        echo "Currently installed URL ${PREV_SVNSOURCEURL}"
        echo " and new deployment URL ${SVNSOURCEURL}${REVISION} are identical!"
        echo "No further action required by this script."
        echo "If really necessary, use standard SVN methods to update/revert/cleanup your work area."
        echo "Exiting ..."
        echo ""
        exit 0
    else
        echo "... OK. "
        echo ""
    fi

    # if revision parameter was not provided, the revision variables can now be unset again,
    # since they are not relevant for further processing (SVN uses -rHEAD by default).
    if [ ! "$OPT_ISSET_REVISION" ] ; then
        REV_NO=""
        REVISION=""
    fi


    # write parameters in case of a restart
    f_write_par_to_restart_file "$RESTART_FILE" "SOURCE_PATH" "$SOURCE_PATH"
    f_write_par_to_restart_file "$RESTART_FILE" "REV_NO" "$REV_NO"
    f_write_par_to_restart_file "$RESTART_FILE" "REVISION" "$REVISION"
    f_write_par_to_restart_file "$RESTART_FILE" "ENVNAME" "$ENVNAME"
    f_write_par_to_restart_file "$RESTART_FILE" "RUN_INFA_IMPORT" "$RUN_INFA_IMPORT"
    f_write_par_to_restart_file "$RESTART_FILE" "RUN_DB_APPLY" "$RUN_DB_APPLY"

    f_write_variable_to_restart_file "$RESTART_FILE" "SVNSOURCEURL" "$SVNSOURCEURL"
    f_write_variable_to_restart_file "$RESTART_FILE" "CUR_BUILD_NO" "$CUR_BUILD_NO"
    f_write_variable_to_restart_file "$RESTART_FILE" "CUR_BUILD_ENV" "$CUR_BUILD_ENV"
    f_write_variable_to_restart_file "$RESTART_FILE" "PREV_SVNSOURCEURL" "$PREV_SVNSOURCEURL"
    f_write_variable_to_restart_file "$RESTART_FILE" "PRV_BUILD_NO" "$PRV_BUILD_NO"
    f_write_variable_to_restart_file "$RESTART_FILE" "PRV_BUILD_ENV" "$PRV_BUILD_ENV"
    f_write_variable_to_restart_file "$RESTART_FILE" "PREV_BUILD_CNT" "$PREV_BUILD_CNT"

    f_write_restart_point $RESTART_FILE INITIALCHECK

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping initial checks"
    echo "###############################################################################"
    echo ""
fi


#---------------------------------------------------------------------------
# 8.) Get difference between previous and current deployment
#---------------------------------------------------------------------------

f_check_restart_point "$RESTART_FILE" GETDIFFPREVCURR
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo "###############################################################################"
    echo "# Get difference between previous and current deployment"
    echo "###############################################################################"
    echo ""

    echo "Generating filelist $STATUS_CHANGED_FILE"
    echo "    containing new/changed items ..."
    echo ""

    # if a previous build exists, list only differences
    if [ $PREV_BUILD_CNT -eq 1 ] ; then
        MOD_URL=$(echo $PREV_SVNSOURCEURL | sed -e 's/\//\\\//'g | cut -f 1 -d @ )
        # echo "original PREV_SVNSOURCEURL : $PREV_SVNSOURCEURL"
        # echo "modified PREV_SVNSOURCEURL : $MOD_URL"
        # perform diff, filter URL path, write to file
        echo "performing diff between"
        echo "        $PREV_SVNSOURCEURL"
        echo "    and ${SVNSOURCEURL}${REVISION} ..."
        echo ""
        $SVN diff --summarize $PREV_SVNSOURCEURL ${SVNSOURCEURL}${REVISION} \
            | grep -E "^A|^M|^D" \
            | sort \
            | sed -e "s/$MOD_URL\///g" \
              > $STATUS_CHANGED_FILE 
        check_error $?
    echo "CHANGED_FILES:"
    cat $STATUS_CHANGED_FILE
    # if no previous build exists, list everything
    # and add a leading 'A' to emulate the svn diff result
    else
        $SVN list --depth infinity ${SVNSOURCEURL}${REVISION} \
            | sed -e "s/\(^.*\)/A       \1/" \
              > $STATUS_CHANGED_FILE 
    fi
    echo "" 
    echo "... done"
    echo ""

    # write restart information
    f_write_variable_to_restart_file "$RESTART_FILE" STATUS_CHANGED_FILE $STATUS_CHANGED_FILE
    f_write_restart_point $RESTART_FILE GETDIFFPREVCURR

    f_write_date
    echo ""

else

    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping getting difference between previous and current deployment"
    echo "###############################################################################"
    echo ""
    # although this step can be skipped, the result of this step from the previous run is still required
    # get name of change list from previous script run (writes into variable GLOBAL_RETURN_VALUE)
    # write contents of old change list into current change list
    f_get_variable_from_restart_file "$RESTART_FILE" STATUS_CHANGED_FILE
    cat $GLOBAL_RETURN_VALUE > $STATUS_CHANGED_FILE
    check_error $?

fi


#---------------------------------------------------------------------------
# 9.) Get code from Subversion build tag and update work area
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get code from Subversion build tag"
echo "###############################################################################"
echo ""

f_check_restart_point "$RESTART_FILE" UPDATEINFASHARED
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo "Updating Informatica run area:"
    echo ""
    # update Informatica run environment
    td_updwa.sh \
            -w "$INFA_SHARED_DIR" \
            -u "${SVNSOURCEURL}/infa_shared${REVISION}" \
            -l "$LOG_FILE" \
            -q
    check_error $?
    echo ""

    f_write_restart_point $RESTART_FILE UPDATEINFASHARED

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping update of Informatica run area"
    echo "###############################################################################"
    echo ""
fi

f_check_restart_point "$RESTART_FILE" UPDATEDBADMIN
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo "Updating database DDL area:"
    echo ""
    # update database ddl area - required to apply the database changes
    td_updwa.sh \
            -w "$HOME/dbadmin" \
            -u "${SVNSOURCEURL}/dbadmin${REVISION}" \
            -l "$LOG_FILE" \
            -q
    check_error $?
    echo ""

    f_write_restart_point $RESTART_FILE UPDATEDBADMIN

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping update of database DDL area"
    echo "###############################################################################"
    echo ""
fi

f_check_restart_point "$RESTART_FILE" UPDATEINFAXML
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo "Updating Informatica XML files:"
    echo ""
    # update Informatica objects - required for import into Informatica repository
    td_updwa.sh \
            -w "$HOME/infa_xml" \
            -u "${SVNSOURCEURL}/infa_xml${REVISION}" \
            -l "$LOG_FILE" \
            -q
    check_error $?
    echo ""

    f_write_restart_point $RESTART_FILE UPDATEINFAXML

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping update of Informatica XML file area"
    echo "###############################################################################"
    echo ""
fi


#---------------------------------------------------------------------------
# 10.) get list of changed DDLs and display them for information purpose
#      execute the changes in the database
#---------------------------------------------------------------------------

if [ "$RUN_DB_APPLY" == "true" ] ; then

    f_check_restart_point "$RESTART_FILE" APPLYDDL
    if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

        DDL_PATTERN="........dbadmin"

        # checking for deleted database objects
        NUM_OF_CHANGED_DDL=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_DDL -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following database object definition files have been deleted."
            echo "# Please check if they can be dropped from the database."
            echo "###############################################################################"
            echo ""

            cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep "^D" > $DB_DELETED_FILE
            cat $DB_DELETED_FILE
            echo ""
            echo "This info can also be found in $DB_DELETED_FILE"
            echo ""
        fi

        NUM_OF_CHANGED_DDL=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep -v "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_DDL -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following DDL scripts have also changed."
            echo "# They will now be applied accordingly to the environment's databases."
            echo "###############################################################################"
            echo ""

            cd $HOME
            cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep -v "^D" > $DB_CHANGED_FILE
            cat $DB_CHANGED_FILE
            echo ""
            apply_db_chg.sh $DB_CHANGED_FILE $HOME/dbadmin $CUR_BUILD_NO $ENVNAME
            echo ""
            cd -
        fi

        f_write_restart_point $RESTART_FILE APPLYDDL

        echo ""
        f_write_date
        echo ""

    else
        echo ""
        echo "###############################################################################"
        echo "# Found restart point - skipping apply of database changes"
        echo "###############################################################################"
        echo ""
    fi
fi


#---------------------------------------------------------------------------
# 11.) get a list of changed Informatica objects and import them 
#---------------------------------------------------------------------------

if [ "$RUN_INFA_IMPORT" == "true" ] ; then

    #
    # make sure all folders, connections etc. are in place in Informatica
    #
    echo ""
    echo "###############################################################################"
    echo "# Run install_stage.sh for environment $ENVNAME"
    echo "###############################################################################"
    echo ""
    $SCM_BASE/infa/install/install_stage.sh "$ENVNAME"

    f_check_restart_point "$RESTART_FILE" IMPORTINFAXML
    if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

        INFA_XML_PATTERN="........infa_xml"

        # checking for deleted Informatica objects
        NUM_OF_CHANGED_XML=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$INFA_XML_PATTERN" | grep "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_XML -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following Informatica objects have been deleted."
            echo "# Please check if they can be deleted from the Informatica target repository."
            echo "###############################################################################"
            echo ""

            cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$INFA_XML_PATTERN" | grep "^D" > $INFA_DELETED_FILE
            cat $INFA_DELETED_FILE
            echo ""
            echo "This info can also be found in $INFA_DELETED_FILE"
            echo ""
        fi

        # checking for new or changed Informatica objects
        NUM_OF_CHANGED_XML=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$INFA_XML_PATTERN" | grep -v "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_XML -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following Informatica objects have changed."
            echo "# They will now be imported into the Informatica target repository."
            echo "###############################################################################"
            echo ""

            cat $STATUS_CHANGED_FILE | grep -v "\/$" \
                                     | grep "$INFA_XML_PATTERN" \
                                     | grep -v "^D" \
                                     | sed -e "s/^$INFA_XML_PATTERN\///" \
                                       > $INFA_CHANGED_FILE
            cat $INFA_CHANGED_FILE
            echo ""

            # run the import of XML files into the Informatica repository
            impxml.sh \
                    -l "$INFA_CHANGED_FILE" \
                    -p "$HOME/infa_xml" \
                    -e "$ENVNAME" \
                    -b "$CUR_BUILD_NO" \
                    -o "$PRV_BUILD_NO"
        fi

        f_write_restart_point $RESTART_FILE IMPORTINFAXML

        echo ""
        f_write_date
        echo ""

    else
        echo ""
        echo "###############################################################################"
        echo "# Found restart point - skipping import of Informatica objects"
        echo "###############################################################################"
        echo ""
    fi
fi


#---------------------------------------------------------------------------
# 12.) make shure the Informatica parameter file contains an entry for the database environment 
#---------------------------------------------------------------------------

f_check_restart_point "$RESTART_FILE" CHANGEINFAPARFILES
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# make shure the Informatica parameter file contains an entry for the database environment"
    echo "###############################################################################"
    echo ""

    sed -e 's/\${DB_ENV}'/$ENVNAME/ <$INFA_SHARED_DIR/par/axedwparameters.prm >$INFA_SHARED_DIR/par/axedwparameters.prm.new
    mv $INFA_SHARED_DIR/par/axedwparameters.prm $INFA_SHARED_DIR/par/axedwparameters.prm.orig && \
    mv $INFA_SHARED_DIR/par/axedwparameters.prm.new $INFA_SHARED_DIR/par/axedwparameters.prm

    #
    # make shure the logon information for batch scripts are correct
    #
    export DB_ENV=$ENVNAME

    for file in $INFA_SHARED_DIR/bin/*.var
    do
        sed -e "s/UserName.*=.*'/UserName = '${DB_ENV}_Common_Trf_00001'/" -e "s/UserPassword.*=.*'/UserPassword = '${DB_ENV}_Common_Trf'/" <$file >$file.new
        mv $file $file.orig && mv $file.new $file
    done

    f_write_restart_point $RESTART_FILE CHANGEINFAPARFILES

    echo ""
    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping change of Informatica parameter files"
    echo "###############################################################################"
    echo ""
fi


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0

