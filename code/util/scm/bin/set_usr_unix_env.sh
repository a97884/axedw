#!/bin/ksh
############################################################################
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID                   : $Id: set_usr_unix_env.sh 128 2011-09-07 08:23:46Z k9106728 $
# Last Changed By      : $Author: k9106728 $
# Last Changed Date    : $Date: 2011-09-07 10:23:46 +0200 (ons, 07 sep 2011) $
# Last Changed Revision: $Revision: 128 $
# URL                  : $URL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/set_usr_unix_env.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# PURPOSE: Create Informatica Linux user including INFA/SVN/ORA environment
# 
# REVISION HISTORY
# ==========================================================================
# INITIALS   DATE         COMMENT
# --------------------------------------------------------------------------
# SSU (TD)   2009-06-26   Initial version
# JEI (TD)   2009-06-29   Added parameter 1-4
#
# OBJECT REFERENCE INFORMATION
# ==========================================================================
# TYPE    ACTION    OBJECT NAME
# --------------------------------------------------------------------------
# 
############################################################################

mkuserdir ()
    {
        if [ -e "$1" ] ; then
            echo "Directory $1 already exists."
        else
            echo "creating directory $1"
            mkdir $1
            # chown -v $NEWUSR:$NEWGRP $1
            # chmod -v 770 $1
        fi
    }

    
TEMPLATE_PATH=/opt/devutil/scm
SCRIPT_VERSION=1
umask 002

if [ $(cat  $HOME/.profile | grep "DEV_ENV_VERSION=$SCRIPT_VERSION" | wc -l) -eq 1 ] ; then
	exit
else
	DEV_ENV_VERSION=`cat  $HOME/.profile | grep "DEV_ENV_VERSION | cut -d = -f 2`
	echo "Your current development environment is not up to date! Will update to version $SCRIPT_VERSION ..."
fi

echo "Home directory: 			    $HOME"
echo "Current environment version:  $DEV_ENV_VERSION"
echo "Template Path:                $TEMPLATE_PATH"
echo ""

if [ "$DEV_ENV_VERSION" -lt "$SCRIPT_VERSION" ] ; then

	# Create Informatica shared directory for new user
	echo "Creating Informatica shared directory ..."
	mkuserdir $HOME/infa_shared
	echo "... done"
	echo ""


	# Create Subversion scripting directories for new user
	echo "Creating Subversion scripting directories ..."
	mkuserdir $HOME/svn
	mkuserdir $HOME/svn/exp
	mkuserdir $HOME/svn/mrg
	mkuserdir $HOME/svn/log
	mkuserdir $HOME/svn/tmp
	mkuserdir $HOME/svn/code
	echo "... done"
	echo ""


	# Set SVN standard configuration
	echo "Setting SVN standard configuration ..."
	mkuserdir $HOME/.subversion
	if [ ! -e $HOME/.subversion/config ] ; then
		ln -s $TEMPLATE_PATH/config/svn_config $HOME/.subversion/config
	fi
	echo "... done"
	echo ""


	# Set link to SCM script standard configuration file
	if [ ! -e $HOME/.scm_profile ] ; then
		echo "Setting link $HOME/.scm_profile to SCM script configuration file $TEMPLATE_PATH/config/.scm_profile ..."
		ln -s $TEMPLATE_PATH/config/.scm_profile $HOME/.scm_profile
		echo "... done"
		echo ""
	fi

	# Set link to Informatica scripting standard configuration file
	if [ ! -e $HOME/.infascm_profile ] ; then
		echo "Setting link $HOME/.infascm_profile to SCM script configuration file $TEMPLATE_PATH/config/.infascm_profile ..."
		ln -s $TEMPLATE_PATH/config/.infascm_profile $HOME/.infascm_profile
		echo "... done"
		echo ""
	fi

	# check if additional entries in .profile alredy exist and add if required
	if [ $(cat $HOME/.profile | grep ".infa_profile" | wc -l) -eq 0 ] ; then
	   cat $TEMPLATE_PATH/template/add_infa2profile >> $HOME/.profile
	fi
	if [ $(cat $HOME/.profile | grep "/opt/devutil/scm/bin" | wc -l) -eq 0 ] ; then
		cat $TEMPLATE_PATH/template/add_devpath2profile >> $HOME/.profile
	fi

	# place this script into .profile to run with each login
	if [ $(cat $HOME/.profile | grep "set_usr_unix_env.sh" | wc -l) -eq 0 ] ; then
		cat $TEMPLATE_PATH/template/add_setusrunixenv2profile >> $HOME/.profile
	fi

fi

echo "... done"
echo ""

if [ $(cat  $HOME/.profile | grep "DEV_ENV_VERSION=" | wc -l) -eq 0 ] ; then
	echo "DEV_ENV_VERSION=$SCRIPT_VERSION" >> $HOME/.profile
fi

exit

