#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID                   : $Id: 11_tag_hfx.sh 5540 2012-09-11 02:01:09Z k9108499 $
# Last ChangedBy       : $Author: k9108499 $
# Last ChangedDate     : $Date: 2012-09-11 04:01:09 +0200 (tis, 11 sep 2012) $
# Last ChangedRevision : $Rev: 5540 $
# Subversion URL       : $URL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/11_tag_hfx.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : tag hotfix branch
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2009-01-06 S.Sutter       Initial version 
# 2011-07-05 S.Sutter       Adapt to Axfood specifics
# 2012-06-30 S.Sutter       Switch to getopts parameter handling; change 
#                           add creation of build results & packages
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# Tag hotfix branch
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -n        Hotfix number       yes         no          existing hotfix release number in branches/hfx; with only one hotfix branch, this parameter can be omitted
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Check if there is a hotfix branch for tagging
# 5.) Determine file names and hotfix paths
# 6.) Determine previous release number and path
# 7.) Create hotfix result info: file lists, scripts & packages
# 8.) Move hotfix branch to release tags
# 
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

function print_help
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-n <hotfixnumber>] [-h]"
    echo "Tag hotfix and move to release tag folder"
    echo "       [-n <hotfixnumber>] : existing hotfix release number in branches/hfx;"
    echo "                             with only one hotfix branch, this parameter can be omitted"
    echo "       [-h]                : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.rel-$HOTFIX_NO.$SCRIPT_START_DATE.error
        fi
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialization
#---------------------------------------------------------------------------

# get parameters
USAGE="n:h"
while getopts "$USAGE" opt; do
    case $opt in
        n  ) HOTFIX_NO="$OPTARG"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo "###############################################################################"
echo "START ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Para -n (Hotfix no.)  : $HOTFIX_NO"
echo ""
echo "SVN repository URL    : $SVN_BASE_URL"
echo "Local temp path       : $BUILD_TMP_PATH"
echo "Local logging path    : $BUILD_LOG_PATH"
echo ""

#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0
# Check if hotfix number complies with standard release number format
if [ "$HOTFIX_NO" ] ; then
    f_check_release_number_format "$HOTFIX_NO"
    if [ $? -eq 0 ] ; then
        echo "Error: Parameter -n (hotfix number) does not comply with standard format '2nnnRnn.xxx.yyy.zzz'!"
        echo "Please provide the parameter in the correct format."
        echo "Exiting ..."
        echo ""
        ERROR_CODE=2
    fi
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


#---------------------------------------------------------------------------
# 4.) Check if there is a hotfix branch for tagging
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "Checking if hotfix branch exists ..."
echo "###############################################################################"
echo ""

if [ "$HOTFIX_NO" ] ; then
    UNTAGGED_CNT=`$SVN list $SVN_BASE_URL/branches/hfx | grep "$HOTFIX_NO" | wc -l` 2>/dev/null
    if [ $UNTAGGED_CNT -eq 0 ] ; then
        echo "There is no hotfix branch that could be tagged!"
        echo "... exiting."
        check_error 2
    elif [ $UNTAGGED_CNT -eq 1 ] ; then
        echo "Found one hotfix branch:"
        HOTFIX_BRANCH=`$SVN list $SVN_BASE_URL/branches/hfx | grep "$HOTFIX_NO" | sed -e "s/\/$//g"`
        echo $HOTFIX_BRANCH
    else
        echo "Found more than one hotfix branch!"
        echo ""
        $SVN list $SVN_BASE_URL/branches/hfx | grep "$HOTFIX_NO"
        echo ""
        echo "Clean up before restarting the script or use distinctive hotfix number!"
        echo "... exiting."
        check_error 2
    fi
else
    UNTAGGED_CNT=`$SVN list $SVN_BASE_URL/branches/hfx | grep "^rel-" | wc -l` 2>/dev/null
    if [ $UNTAGGED_CNT -eq 0 ] ; then
        echo "There is no hotfix branch that could be tagged!"
        echo "... exiting."
        check_error 2
    elif [ $UNTAGGED_CNT -eq 1 ] ; then
        echo "Found one hotfix branch:"
        HOTFIX_BRANCH=`$SVN list $SVN_BASE_URL/branches/hfx | grep "$HOTFIX_NO" | sed -e "s/\/$//g"`
        HOTFIX_NO=`echo $HOTFIX_BRANCH | cut -c 5-23`
        echo $HOTFIX_BRANCH
    else
        echo "Found more than one hotfix branch!"
        echo ""
        $SVN list $SVN_BASE_URL/branches/hfx | grep "$HOTFIX_NO"
        echo ""
        echo "Clean up before restarting the script or use distinctive hotfix number!"
        echo "... exiting."
        check_error 2
    fi


fi
echo "... done"
echo ""


#---------------------------------------------------------------------------
# 5.) Determine file names and hotfix paths
#---------------------------------------------------------------------------

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$HOTFIX_NO.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$HOTFIX_NO.$SCRIPT_START_DATE.log_msg.txt

echo "Final log file        : $FINAL_LOG_FILE"
echo "Temp log message file : $LOG_MSG_FILE"
echo ""

MOVE_SOURCE_URL=$SVN_BASE_URL/branches/hfx/$HOTFIX_BRANCH
MOVE_TARGET_URL=$SVN_BASE_URL/tags/rel

echo "Source                : $MOVE_SOURCE_URL"
echo "Target                : $MOVE_TARGET_URL"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 6.) Determine previous release number and path
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Determine previous release number and path"
echo "###############################################################################"
echo ""

typeset -i PREV_REL_CNT
# Check release tag directory for latest release
PREV_REL_CNT=`$SVN list $SVN_BASE_URL/tags/rel | sort | tail -1 | sed -e "s/\/$//g" | wc -l`
check_error $?
# if this is the first release no previous value can exist
if  [ $PREV_REL_CNT -ne 0 ] ; then
    PREV_REL_NAME=`$SVN list $SVN_BASE_URL/tags/rel | grep 'rel-2[0-9][0-9][0-9]R[0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]' | sort | tail -1 | sed -e "s/\/$//g"`
    PREV_REL_NO=`echo $PREV_REL_NAME | cut -c 5-23`
    PREV_REL_TAG=tags/rel/$PREV_REL_NAME
fi

PREV_REL_URL=$SVN_BASE_URL/$PREV_REL_TAG

echo "Previous release no   : $PREV_REL_NO"
echo "Previous release tag  : $PREV_REL_TAG"
echo "Previous release URL  : $PREV_REL_URL"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 7.) Create hotfix result info: file lists, scripts & packages
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Calling td_build_result.sh to create build results"
echo "###############################################################################"
echo ""

td_build_result.sh \
        -m "$THIS_SCRIPT" \
        -e "hfx" \
        -s "$SCRIPT_START_DATE" \
        -n "$HOTFIX_NO" \
        -o "$PREV_REL_NO" \
        -t "$MOVE_SOURCE_URL" \
        -p "$PREV_REL_URL" \
        -c "$PREV_REL_CNT" \
        -l "$LOG_FILE"

echo ""
f_write_date
echo ""

echo ""
echo "###############################################################################"
echo "# Calling td_package.sh to create packages"
echo "###############################################################################"
echo ""

td_package.sh \
        -e "hfx" \
        -b "$HOTFIX_NO" \
        -c "delta"

echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 8.) Move hotfix branch to release tags
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Archive/move hotfix branch to release tags"
echo "###############################################################################"
echo ""

# perform move from branches to tags
echo "Moving hotfix branch $HOTFIX_BRANCH from branches/hfx to tags/rel" > $LOG_MSG_FILE
cat $LOG_MSG_FILE

$SVN move $MOVE_SOURCE_URL $MOVE_TARGET_URL --parents --file $LOG_MSG_FILE 2>&1
check_error $?
echo "... done."


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
