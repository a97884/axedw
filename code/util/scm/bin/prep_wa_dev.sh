#!/usr/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: prep_wa_dev.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/prep_wa_dev.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Update SAMBA development environment from SVN stage
# Project     :
# Subproject  : all
# --------------------------------------------------------------------------
# Change History
# Date       Author         Description
# 2010-07-28 S.Sutter       Initial version
# --------------------------------------------------------------------------
# Description
#   Update SAMBA development environment from SVN stage
#
# Dependencies
#   none
#
# Parameters
#
# Variables
#
# --------------------------------------------------------------------------
# Processing Steps
# 1.) Initialization
# 2.) Check for correct syntax
# 3.) Display parameters and variables
# 4.) Get currently deployed revision from work area
#     and most current revision from repository
# 5.) Get code from Subversion staging branch and update work area
# 6. get list of changed DDLs and display them for information purpose
#
# --------------------------------------------------------------------------
# Open points
# 1.)
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialization
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh


#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: `basename $0`"
    echo "Create/update complete development directory structure from SVN repository"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured.
        if [ -z $FINAL_LOG_FILE ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi

        mv $LOG_FILE $FINAL_LOG_FILE
        echo "Log file can be found under $FINAL_LOG_FILE"
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# some standard variables required to run this script
FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.log
STATUS_CHANGED_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.changed_items.txt
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt
touch $LOG_MSG_FILE


# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 2.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

# if [ "$LOGNAME" != "coronadev" ] ; then
#    echo "This user cannot deploy into the DEV environment!"
#    echo "Use user coronadev instead. Exiting ..."
#    echo ""
#    ERROR_CODE=1
# fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


#---------------------------------------------------------------------------
# 3.) Display parameters and variables
#---------------------------------------------------------------------------

echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Variables used:"
echo "###############################################################################"
echo ""
echo "SVN repository base path      : $SVN_BASE_URL"
echo "Local logging path            : $BUILD_LOG_PATH"
echo "Final log file                : $FINAL_LOG_FILE"
echo ""
echo ""
SVNSOURCEURL=$SVN_BASE_URL/trunk/code
############################ change!!!
SVNTARGETDIR=$HOME
############################
PREV_SVNSOURCEURL=$PREV_SVN_BUILD_TAG
echo "SVN source URL             : $SVNSOURCEURL"
echo "Work area target dir      : $SVNTARGETDIR"
echo ""


#---------------------------------------------------------------------------
# 4.) Get currently deployed revision from work area
#     and most current revision from repository
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get currently deployed revision from work area"
echo "# and most current revision from repository"
echo "###############################################################################"
echo ""

PREV_REV_NO=`$SVN info $SVNTARGETDIR/infa_xml | grep ^Revision | cut -d" " -f2`
if [ -z $PREV_REV_NO ] ; then
    echo "No currently installed revision was found. Considering all items as new."
    echo ""
else
    echo "Currently installed revision : $PREV_REV_NO"
fi

CURR_REV_NO=`$SVN info $SVNSOURCEURL | grep ^Revision | cut -d" " -f2`
if [ -z $CURR_REV_NO ] ; then
    echo "Cannot determine current revision from $SVNSOURCEURL!!!"
    echo "Exiting ..."
    echo ""
    exit 1
else
    echo "Most current source revision : $CURR_REV_NO"
fi


#---------------------------------------------------------------------------
# 4.) Get difference between deployed and latest revision
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get difference between deployed and latest revision"
echo "###############################################################################"
echo ""

echo "Generating filelist $STATUS_CHANGED_FILE"
echo "    containing new/changed items ..."

# no deployed revision exists, so list everything
# and add a leading 'A' to emulate the svn diff result
if [ -z "$PREV_REV_NO" ] ; then
    $SVN list --depth infinity $SVNSOURCEURL \
        | sed -e "s/\(^.*\)/A       \1/" \
          > $STATUS_CHANGED_FILE 
# if a deployed revision exists, list only differences
else
    MOD_URL=$(echo $SVNSOURCEURL | sed -e 's/\//\\\//'g )
    # echo "modified SVNSOURCEURL : $MOD_URL"
    # perform diff, filter URL path, write to file
    echo "performing diff between"
    echo "        $SVNSOURCEURL at revision $PREV_REV_NO"
    echo "    and $SVNSOURCEURL at revision $CURR_REV_NO ..."
    $SVN diff --summarize "$SVNSOURCEURL@$PREV_REV_NO" "$SVNSOURCEURL@$CURR_REV_NO" \
        | grep -E "^A|^M|^D" \
        | sort \
        | sed -e "s/$MOD_URL\///g" \
          > $STATUS_CHANGED_FILE 
    check_error $?
fi
echo "" 
echo "... done"
echo ""


#---------------------------------------------------------------------------
# 5.) Get code from Subversion staging branch and update work area
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get code from Subversion staging branch"
echo "###############################################################################"
echo ""

echo "Updating development work area to latest revision $CURR_REV_NO ..."
echo ""


# get complete Informatica environment
td_updwa.sh -w $INFA_SHARED_DIR -u $SVNSOURCEURL/infa_shared -l $LOG_FILE
check_error $?
echo ""

# get Informatica xml file
td_updwa.sh -w $SVNTARGETDIR/infa_xml -u $SVNSOURCEURL/infa_xml -l $LOG_FILE
check_error $?
echo ""

# get ddl code
td_updwa.sh -w $SVNTARGETDIR/dbadmin -u $SVNSOURCEURL/dbadmin -l $LOG_FILE
check_error $?
echo ""

echo "... done."
echo ""


#---------------------------------------------------------------------------
# 6. get list of changed DDLs and display them for information purpose
#---------------------------------------------------------------------------

DDL_PATTERN="........dbadmin"
NUM_OF_CHANGED_DDL=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | wc -l`
if  [ $NUM_OF_CHANGED_DDL -ne 0 ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Please be aware that the following DDL scripts have also changed."
    echo "# They should be applied accordingly to the environment's databases."
    echo "###############################################################################"
    echo ""

    cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN"
    echo ""
fi


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0

