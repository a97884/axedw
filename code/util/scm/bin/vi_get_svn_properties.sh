#
# prepare a comment for the 'svn ci' command
# get a list of svn-properties 
# and add the full name for the user
#
# to use it with a function key in vi add the following command to .exrc
#
#"F2 for svn ci:  get svn-properties and add comment
# map ^[[[B :r!propget.all %^Mkkddjf:llC
#
# map the keystroke for F2 to the vi-commands
# :r!propget.all %  = insert the result from  executing the command at actual position
# ^M = Carriage Return
# kkdd = go up two lines and delte that line
# jf:  = go down one line and find the character ':'
# llC  = go right two characters and change the rest of the line

Property=td:streamname
PropertyName=Streamname
ListStart='-- Diese und die folgenden Zeilen werden ignoriert --'
file=$1

# get a list off svn-properties for all files to be checked in
Properties=$(
for Object in $( awk '/$ListStart/{start=NR} { if (NR > start + 2 ) {print $2 }}' $file ) 
do
	svn propget $Property $Object
done | sort -u
)
# prepare komma separated list
for Prop in $Properties
do
	PropertyList="$PropertyList$Sep$Prop"
	Sep=', '
done

#  get the full name for the user from password-file
UserName=$(grep "^${USER:-$LOGIN}:" /etc/passwd | cut -d ':' -f 5)

echo "$PropertyName: $PropertyList"
echo "$UserName: add your comment here "

