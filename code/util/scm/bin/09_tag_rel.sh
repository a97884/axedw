#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: 09_tag_rel.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author:  
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/09_tag_rel.sh $
#---------------------------------------------------------------------------
# SVN Info END
#
# --------------------------------------------------------------------------
# Purpose     : tag current release branch
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2008-11-21 S.Sutter       Initial version 
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# Tag current release branch
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps :
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
# 2.) Initialize other variables
# 3.) Check for correct syntax
# 4.) Check if there is a release branch for tagging
# 5.) Move release from branches to tags
# 
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

function print_help
    {
    echo "Usage: ${THIS_SCRIPT}.sh  [-h]"
    echo "Tag current release branch"
    echo "       [-h]   : Print script syntax help"
    echo ""
    }


#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z $FINAL_LOG_FILE ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialize other variables
#---------------------------------------------------------------------------

# get parameters
USAGE="h"
while getopts "$USAGE" opt; do
    case $opt in
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo "###############################################################################"
echo "START ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Variables used:"
echo "###############################################################################"
echo ""
echo "SVN repository URL    : $SVN_BASE_URL"
echo "Local temp path       : $BUILD_TMP_PATH"
echo "Local logging path    : $BUILD_LOG_PATH"
echo ""


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

#---------------------------------------------------------------------------
# 4.) Check if there is a release branch for tagging
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Check if there is a release branch for tagging ..."
echo "###############################################################################"
echo ""

UNTAGGED_CNT=`$SVN list $SVN_BASE_URL/branches/rel | grep -v ^stage | wc -l`
if [ $UNTAGGED_CNT -eq 0 ] ; then
    echo "There is no release branch that could be tagged!"
    echo "... exiting."
    echo ""
    exit 0
elif [ $UNTAGGED_CNT -eq 1 ] ; then
    $SVN list $SVN_BASE_URL/branches/rel | grep -v ^stage
    REL_BRANCH=`$SVN list $SVN_BASE_URL/branches/rel | grep -v ^stage | sed -e "s/\/$//g"`
    TARGET_RELEASE=`echo $REL_BRANCH | cut -c 5-23`
    echo "Found one release branch"
    echo "Release no            : $TARGET_RELEASE"
    echo "Release branch        : $REL_BRANCH"
    echo ""
else
    echo "Found more than one release branch!"
    $SVN list $SVN_BASE_URL/branches/rels | grep -v ^stage
    echo "Clean up before restarting the script!"
    echo "... exiting."
    echo ""
    exit 0
fi

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$TARGET_RELEASE.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$TARGET_RELEASE.$SCRIPT_START_DATE.log_msg.txt

echo "Final log file        : $FINAL_LOG_FILE"
echo "Temp log message file : $LOG_MSG_FILE"
echo ""
echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 5.) Move release from branches to tags
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Move release from branches to tags ..."
echo "###############################################################################"
echo ""

MOVE_SOURCE_URL=$SVN_BASE_URL/branches/rel/$REL_BRANCH
MOVE_TARGET_URL=$SVN_BASE_URL/tags/rel

echo "Source                : $MOVE_SOURCE_URL"
echo "Target                : $MOVE_TARGET_URL"

# set SVN log message
echo "Moving release branch $REL_BRANCH from branches/rel to tags/rel" > $LOG_MSG_FILE
cat $LOG_MSG_FILE

#perform actual move
$SVN move $MOVE_SOURCE_URL $MOVE_TARGET_URL --file $LOG_MSG_FILE
check_error $?
echo ""
echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
