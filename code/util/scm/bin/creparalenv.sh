#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: creparalenv.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/creparalenv.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#---------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. $HOME/.infascm_profile
. basefunc.sh

typeset -l ENVNAME

#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-e <envname>] [-p <projname>] [-h]"
    echo "Creates new system test build tag from system test staging branch."
    echo "       [-e <envname>] : Environment to build. Default is uv3"
    echo "       [-p <projname>]: Name of project, used as name in Subversion. Default is EDW2"
    echo "       [-b <baseline>]: URL of baseline to use, points to where the code folder is. Default is trunk (main trunk)"
    echo "       [-r <step>]    : Step to restart from"
    echo "                      : Step 1 = create subversion branch"
    echo "                      : Step 2 = create subversion tags"
    echo "                      : Step 3 = copy code from selected baseline"
    echo "                      : Step 4 = create subversion stage area"
    echo "                      : Step 5 = create initial system build"
    echo "                      : Step 6 = cleanup working area"
    echo "       [-h]           : Print script syntax help."
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured.

        echo "Log file can be found under $LOG_FILE"

        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1
PROJNAME="EDW2"
ENVNAME="uv3"
RESTART="no"
RESTART_FROM="1"
BASELINE="trunk"

USAGE="b:e:p:r:h"
while getopts "$USAGE" opt; do
    case $opt in
        b  ) BASELINE="$OPTARG"
             ;;
        e  ) ENVNAME="$OPTARG"
             ;;
        p  ) PROJNAME="$OPTARG"
             ;;
        r  ) RESTART_FROM="$OPTARG"
		RESTART="yes"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

#
# create project structure
#
if [ "$RESTART" = "no" -o "$RESTART_FROM" = "1" ]
then
	echo "create project structure for $proj (stage)"
	$SVN mkdir --parents -m "New branch for project ${PROJNAME}" http://axprsv01.axfood.se/svn/axedw/"${PROJNAME}"/branches/sys/stage
	test $? -ne 0 exit 1
fi
if [ "$RESTART" = "no" -o "$RESTART_FROM" = "2" ]
then
	echo "create project structure for $proj (tags)"
	$SVN mkdir --parents -m "New branch for project ${PROJNAME}" http://axprsv01.axfood.se/svn/axedw/"${PROJNAME}"/tags/sys
	test $? -ne 0 exit 1
fi
if [ "$RESTART" = "no" -o "$RESTART_FROM" = "3" ]
then
	echo "create project structure for $proj (trunk)"
	$SVN mkdir --parents -m "New branch for project ${PROJNAME}" http://axprsv01.axfood.se/svn/axedw/"${PROJNAME}"/trunk
	test $? -ne 0 exit 1
	echo "create project structure for $proj (content from $BASELINE)"
	$SVN copy -m "New branch for project ${PROJNAME}" http://axprsv01.axfood.se/svn/axedw/$BASELINE/code http://axprsv01.axfood.se/svn/axedw/"${PROJNAME}/trunk"
	test $? -ne 0 exit 1
fi

#
# create stage area
#
if [ "$RESTART" = "no" -o "$RESTART_FROM" = "4" ]
then
	echo "create stage area, this will be the whole trunk the first time to get a comlete/full system build stage"
	$SVN copy -m "New stage for project ${PROJNAME}" http://axprsv01.axfood.se/svn/axedw/"${PROJNAME}"/trunk/code http://axprsv01.axfood.se/svn/axedw/"${PROJNAME}/branches/sys/stage"
	test $? -ne 0 exit 1
fi

#
# create initial system build
#
if [ "$RESTART" = "no" -o "$RESTART_FROM" = "5" ]
then
	echo "create initial system build"
	03_create_sys_build.sh -e "$ENVNAME"
	test $? -ne 0 exit 1
fi

#
# update working area, can only be done by environment owner so must be run separately by a restart
#
if [ "$RESTART" = "yes" -o "$RESTART_FROM" = "6" ]
then
	echo "cleanup working area"
	top="`dirname "$INFA_SHARED_DIR"`"
	for dir in "$top/infa_shared" "$top/infa_xml" "$top/util" "$top/dbadmin"
	do
		echo "$dir -> $dir.$SCRIPT_START_DATE"
		test -d "$dir" && mv "$dir" "$dir"."$SCRIPT_START_DATE"
	done
fi

exit 0

