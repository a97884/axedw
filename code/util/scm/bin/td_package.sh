#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID                   : $Id: td_package.sh 5540 2012-09-11 02:01:09Z k9108499 $
# Last ChangedBy       : $Author: k9108499 $
# Last ChangedDate     : $Date: 2012-09-11 04:01:09 +0200 (tis, 11 sep 2012) $
# Last ChangedRevision : $Rev: 5540 $
# Subversion URL       : $URL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/td_package.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : create packages from SVN URL path
# Project     : EDW
# Subproject  : all
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2008-07-29 S.Sutter       Initial version
# 2011-07-07 S.Sutter       Adaptions
# 2011-12-05 A.Bagge        Adapt to Axfood specifics
# 2012-06-30 S.Sutter       Switch to getopts parameter handling;
#                           generalize script to be called from other scripts
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# create installation packages
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -e        Environment         yes         yes
# -r        Build/release no.   yes         yes
# -d        Scope               yes         yes         (f)ull or (d)elta
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Export build
# 5.) Create packages using tar and compress them
# 6.) Create release letter from changed files
# 7.) Commit everything to build branch
#
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.)
# 2.)
# --------------------------------------------------------------------------

#----------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
#----------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

typeset -l PACKAGE_ENV
typeset -l SCOPE

# currently, there is only one subject area
SUBJECT_A="all"


#----------------------------------------------------------------------------
# Print script syntax and help
#----------------------------------------------------------------------------

print_help ()
    {
    echo "Usage : $THIS_SCRIPT -e <environment> -b <build_number> -c <scope> [-h]"
    echo "Create packages from test or hotfix branch"
    echo "        -e <environment>  : environment"
    echo "        -b <build_number> : latest integration test build number"
    echo "        -c <scope>        : scope - (f)ull or (d)elta"
    echo "        [-h]              : print script syntax help"
    echo ""
    }
#----------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#----------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#----------------------------------------------------------------------------
#  Create package using tar and compress them
#----------------------------------------------------------------------------

create_package ()
    {
    
#   deleting everything in BUILD_EXP_PATH (fresh export)
    rm -rf $BUILD_EXP_PATH/*

    PREVIOUS_RELEASE=`cat $BUILD_RESULT_DIR/filelists/previous_releasenumber.txt`
    NEXT_RELEASE=`cat $BUILD_RESULT_DIR/filelists/current_releasenumber.txt`
    echo "CURRENT RELEASE NUMBER                : $PREVIOUS_RELEASE"
    echo "NEXT RELEASE NUMBER                   : $NEXT_RELEASE"
    echo ""

#   export relevant parts from branch
    TAR_CONFIG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.2tar.txt
    echo ""
    echo "###############################################################################"
    echo "# Exporting relevant parts according to $PACKAGE_CONFIG_FILE ..."
    echo "###############################################################################"
    echo ""

    awk -F":" '{print $1,$2}' $PACKAGE_CONFIG_FILE | while read path1 path2
    do
        echo "Exporting $BRANCH_MAIN_PATH/$SOURCE_BUILD/$path1"
        echo "       to $BUILD_EXP_PATH/$path2."
        $SVN export $BRANCH_MAIN_PATH/$SOURCE_BUILD/$path1 $BUILD_EXP_PATH/$path2 --quiet --force 2>&1  
        check_error $?
    done
    echo "... done."
    echo ""


    echo "Getting list of files that will be in tar file ..."
    CONT_WITH_PACKAGE=0
    if [ $SCOPE == "delta" ] ; then
#       create delta package

        TAR_BASE=${PREVIOUS_RELEASE}.${NEXT_RELEASE}
        TRG_ADD_FILE=$BUILD_RESULT_DIR/filelists/$PREVIOUS_RELEASE.${NEXT_RELEASE}.trg_add_files.txt
        TRG_UPD_FILE=$BUILD_RESULT_DIR/filelists/$PREVIOUS_RELEASE.${NEXT_RELEASE}.trg_upd_files.txt
        TRG_DEL_FILE=$BUILD_RESULT_DIR/filelists/$PREVIOUS_RELEASE.${NEXT_RELEASE}.trg_del_files.txt
        echo "TRG_ADD_FILE      : $TRG_ADD_FILE"
        echo "TRG_UPD_FILE      : $TRG_UPD_FILE"
        echo "TRG_DEL_FILE      : $TRG_DEL_FILE"
        if [ -f $TRG_DEL_FILE ]; then
            CONT_WITH_PACKAGE=1
        fi
        if [ -f $TRG_ADD_FILE ]; then
            cat $TRG_ADD_FILE >> $TAR_CONFIG_FILE.tmp
            CONT_WITH_PACKAGE=1
        fi
        if [ -f $TRG_UPD_FILE ]; then
            cat $TRG_UPD_FILE >> $TAR_CONFIG_FILE.tmp
            CONT_WITH_PACKAGE=1
        fi
        if [ -f $TAR_CONFIG_FILE.tmp ]; then
            cat $TAR_CONFIG_FILE.tmp | sort -u >> $TAR_CONFIG_FILE
        fi
    else
#       create full package
        TRG_ALL_FILE=$BUILD_RESULT_DIR/filelists/$PREVIOUS_RELEASE.${NEXT_RELEASE}.trg_all_files.txt
        echo $TRG_ALL_FILE
        
        TAR_BASE=full.${NEXT_RELEASE}
        if [ -f $TRG_ALL_FILE ]; then
            CONT_WITH_PACKAGE=1
            cp $TRG_ALL_FILE $TAR_CONFIG_FILE
        fi

    fi

    if [ CONT_WITH_PACKAGE -eq 1 ]; then

#       One entry for deploy script must be added manually.
#       And one entry for file lists must be added manually.
#       These directory is although in package cfg file but does not belong to any module.

        echo "... Creating package"
        cd $BUILD_EXP_PATH 
        tar -cvf $TAR_BASE.tar -L $TAR_CONFIG_FILE

        echo "Update package with deploy_scripts and filelists"
        tar -Ruvf $TAR_BASE.tar deploy_scripts filelists

        echo "... Package $TAR_BASE.tar created" 
        echo ""

        gzip $TAR_BASE.tar
        echo "... Package $TAR_BASE.tar compressed" 
        echo ""

#       move tar result to working copy
        # $SVN update ${SVN_WORKDIR}	
        mv $TAR_BASE.tar.gz $BUILD_BRANCH_PATH/packages #change to WA path

#       add packages to build branch
        $SVN add $BUILD_BRANCH_PATH/packages/$TAR_BASE.tar.gz
        echo "... added file $TAR_BASE.tar" 
        echo "... done."
        echo ""

#       cleanup temp files
        rm -f $TAR_CONFIG_FILE
        rm -f $TAR_CONFIG_FILE.tmp
    else
        echo "found nothing to package!!!"
    fi
    }


#----------------------------------------------------------------------------
# 2.) Initialization
#----------------------------------------------------------------------------

USAGE="e:b:c:h"
while getopts "$USAGE" opt; do
    case $opt in
        e  ) PACKAGE_ENV="$OPTARG"
             OPT_ISSET_ENV="true"
             # echo "$PACKAGE_ENV"
             ;;
        b  ) P_BUILD_NUMBER="$OPTARG"
             OPT_ISSET_BUILDNO="true"
             # echo "$P_BUILD_NUMBER"
             ;;
        c  ) SCOPE="$OPTARG"
             OPT_ISSET_SCOPE="true"
             # echo "$SCOPE"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars


FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.build-$BUILD_NUMBER.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt
touch $LOG_MSG_FILE


# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#----------------------------------------------------------------------------
# 3.) Check for correct syntax
#----------------------------------------------------------------------------

ERROR_CODE=0

if [ ! "$OPT_ISSET_ENV" ] ; then
        echo "Error: Parameter -e (environment) not provided!"
        ERROR_CODE=2
fi
if [ ! "$OPT_ISSET_BUILDNO" ] ; then
        echo "Error: Parameter -b (build/release number) not provided!"
        ERROR_CODE=2
fi
if [ ! "$OPT_ISSET_SCOPE" ] ; then
        echo "Error: Parameter -c (scope) not provided!"
        ERROR_CODE=2
fi

case `echo $SCOPE | cut -c1` in
    f|d)
        ;;
    *)
        echo "Error: Parameter -c (scope) must be either (f)ull or (d)elta!"
        ERROR_CODE=2
        ;;
esac

case "$PACKAGE_ENV" in
    it|int)
        BRANCH_MAIN_PATH="${SVN_BASE_URL}/branches/int"
        typeset -Z6 BUILD_NUMBER=${P_BUILD_NUMBER}
        BUILD_PATTERN="int-build.${BUILD_NUMBER}"
        CUT_BORDERS="11-16"
        ;;
    st|sys)
        BRANCH_MAIN_PATH="${SVN_BASE_URL}/tags/sys"
        typeset -Z6 BUILD_NUMBER=${P_BUILD_NUMBER}
        BUILD_PATTERN="sys-build.${BUILD_NUMBER}"
        CUT_BORDERS="11-16"
        ;;
    hf|hfx)
        BRANCH_MAIN_PATH="${SVN_BASE_URL}/branches/hfx"
        BUILD_NUMBER=${P_BUILD_NUMBER}
        BUILD_PATTERN="rel-${BUILD_NUMBER}"
        CUT_BORDERS="5-23"
        ;;
    *)
        echo "Error: Parameter -e (environment) does not have a recognized value!"
        echo "       Possible values are: it/int, st/sys, hf/hfx"
        ERROR_CODE=2
        ;;
esac

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    check_error $ERROR_CODE
fi

echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -e (environment)    : $PACKAGE_ENV"
echo "Parameter -b (build/rel no.)  : $BUILD_NUMBER"
echo "Parameter -s (scope)          : $SCOPE"
echo ""
echo "SVN repository URL            : $SVN_BASE_URL"
echo "Local build work path         : $BUILD_WA_PATH"
echo "Local temp path               : $BUILD_TMP_PATH"
echo "Local logging path            : $BUILD_LOG_PATH"
echo "Final log file                : $FINAL_LOG_FILE"
echo "Temp log message file         : $LOG_MSG_FILE"
echo ""


#----------------------------------------------------------------------------
# 4.) Check if build number provided as parameter actually exists
#----------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Check if build number provided as parameter actually exists"
echo "###############################################################################"
echo ""

# echo "$SVN list $BRANCH_MAIN_PATH | grep $BUILD_PATTERN | cut -c $CUT_BORDERS"
BUILD_NO_CHECK=`$SVN list $BRANCH_MAIN_PATH | grep $BUILD_PATTERN | cut -c $CUT_BORDERS`
check_error $?
# echo $BUILD_NO_CHECK
if [[ $BUILD_NO_CHECK != "$BUILD_NUMBER" ]] ; then
	echo "${BUILD_PATTERN}  does not exist in ${BRANCH_MAIN_PATH}!"
    echo "Check the build number again!" 
    echo "Exiting"
	check_error 3
else
	SOURCE_BUILD=`$SVN list $BRANCH_MAIN_PATH | grep $BUILD_PATTERN` 
fi

echo "OK. Will use build ${BRANCH_MAIN_PATH}${SOURCE_BUILD}"
echo ""


#----------------------------------------------------------------------------
# 6.) Prepare environment to create package result directories
#----------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Creating package result directories ..."
echo "###############################################################################"
echo ""

# the package result directory will be located under the build root
# it contains all file lists required for packaging and automatic install scripts
BUILD_BRANCH_PATH=$BUILD_TMP_PATH/package.$BUILD_NUMBER.$SCRIPT_START_DATE
mkdir -p $BUILD_BRANCH_PATH

$SVN checkout ${BRANCH_MAIN_PATH}/${SOURCE_BUILD}/build $BUILD_BRANCH_PATH
check_error $?

if [ ! -d $BUILD_BRANCH_PATH/packages ] ; then
	$SVN mkdir $BUILD_BRANCH_PATH/packages
	check_error $?
fi

BUILD_RESULT_DIR=$BUILD_BRANCH_PATH/results
echo "... done"


#----------------------------------------------------------------------------
# 7.) Create packages 
#----------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Creating package itself ..."
echo "###############################################################################"
echo ""

create_package


#----------------------------------------------------------------------------
# 8.) Commit everything to branch 
#----------------------------------------------------------------------------


echo ""
echo "###############################################################################"
echo "# Committing changes ..."
echo "###############################################################################"
echo ""

echo "Adding integration test packages of build $SOURCE_BUILD" >  $LOG_MSG_FILE
cat $LOG_MSG_FILE
$SVN commit $BUILD_BRANCH_PATH/packages --file $LOG_MSG_FILE  2>&1
check_error $?
echo "... done." 
echo ""


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
