#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: infafunc.sh 6546 2012-10-07 20:57:20Z k9108499 $
# Last Changed By  : $Author: k9108499 $
# Last Change Date : $Date: 2012-10-07 22:57:20 +0200 (sön, 07 okt 2012) $
# Last Revision    : $Revision: 6546 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/infafunc.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Base functions to be included by CM scripts
# Project     : SWoT
# Subproject  : all
# --------------------------------------------------------------------------
# Change History
# Date       Author         Description
# 2011-03-15 S.Sutter       Initial version 
#
# --------------------------------------------------------------------------
# Description
#   Functions that are to be included by all Informatica automation scripts
#
# Dependencies
#   parent  : Called by all Informatica automation scripts
#
# Parameters: none
#
# Variables (please add any other variables that you may use):
#   Variables that are used here are defined by the calling script
#   
#
# --------------------------------------------------------------------------
# Processing Steps 
# function get_infa_repository_name
# function set_infa_repository_name
# function reset_infa_repository_name
# function reset_infa_uuid
# function get_infa_folder_name
# function set_infa_folder_name
# function get_infa_object_version
# function reset_infa_version_number
# function reset_infa_session_props
# function reset_infa_export_date
# function reset_infa_folder_permission
# function sort_xml_file
# function check_if_import_necessary
# function write_import_control_file_specific
# 
# --------------------------------------------------------------------------
# Open points
# --------------------------------------------------------------------------


###########################################################################
# function set_infa_repository_name
# Replace repository name in xml file with new one
# Almost identical to reset_infa_repository_name
###########################################################################

function set_infa_integration_service1 {
    typeset xml_file=$1
    typeset target_env=$2
    typeset old_integration_service
    typeset new_integration_service="IntServ_bix_$target_env"
    typeset sedstr=""
    typeset runfile=""
    old_integration_service=`grep '<WORKFLOW DESCRIPTION =\"' $xml_file \
     | grep  'SERVERNAME =\"' \
     | sed -e 's/\(.*\) SERVERNAME =\"//; s/\" SERVER_DOMAINNAME\(.*\)//'`
    echo "Integration service $old_integration_service will be replaced by $new_integration_service."
    sedstr="sed -e '/<WORKFLOW DESCRIPTION =\"/s/$old_integration_service/$new_integration_service/g'"
    runfile=$xml_file.change_is.sh
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function set_infa_integration_service
# Replace development user specific integration service name in xml file with new one
###########################################################################

set_infa_integration_service () {
    typeset xml_file=$1
    typeset new_is_name=$2
    cat $xml_file | sed -e 's/is_$USER/$new_is_name/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function get_infa_repository_name
# Extract Informatica repository name from xml file; write to global var
###########################################################################

get_infa_repository_name () {
    typeset xml_file=$1
    R_INFA_REP_NAME=`grep "<REPOSITORY NAME=\"" $xml_file \
        | sed -e 's/<REPOSITORY NAME=\"//; s/\" VERSION=\(.*\)//'`
#    echo $R_INFA_REP_NAME
}


###########################################################################
# function get_infa_refobject_name
# Extract name of object referenced by shortcut from xml file; write to global var
###########################################################################

get_infa_refobject_name () {
    typeset xml_file=$1
    typeset obj_type=$2

    R_INFA_REFOBJECT_NAME=`grep "<SHORTCUT COMMENTS =\"" $xml_file \
        | sed -e 's/\(.*\)REFOBJECTNAME =\"//; s/\" REPOSITORYNAME =\(.*\)//'`
    # with sources, the DBD name is also relevant and will be extracted, too
    if [ "$obj_type" = "source" ] ; then
        R_INFA_REFOBJECT_DBD=`grep "<SHORTCUT COMMENTS =\"" $xml_file \
            | sed -e 's/\(.*\)REFERENCEDDBD =\"//; s/\" REFERENCETYPE =\(.*\)//'`
    else
        R_INFA_REFOBJECT_DBD=""
    fi
#    echo $R_INFA_REFOBJECT_DBD
#    echo $R_INFA_REFOBJECT_NAME
}


###########################################################################
# function set_infa_repository_name
# Replace repository name in xml file with new one
# Almost identical to reset_infa_repository_name
###########################################################################

function set_infa_repository_name {
    typeset xml_file=$1
    typeset new_repository_name=$2
    typeset old_repository_name
    typeset sedstr=""
    typeset runfile=""
    old_repository_name=`grep "<REPOSITORY NAME=\"" $xml_file \
        | sed -e 's/<REPOSITORY NAME=\"//; s/\" VERSION=\(.*\)//'`
    # echo $old_repository_name
    sedstr="sed -e '/<REPOSITORY NAME=\"/s/<REPOSITORY NAME=\"$old_repository_name\(.*\)/<REPOSITORY NAME=\"$new_repository_name\1/'"
    runfile=$xml_file.change_rn.sh
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_repository_name
# Replace repository name in xml file with new one
# Almost identical to set_infa_repository_name
###########################################################################

reset_infa_repository_name () {
    typeset xml_file=$1
    typeset new_repository_name=$2
    typeset old_repository_name
    typeset sedstr=""
    typeset runfile=""
    old_repository_name=`grep "<REPOSITORY NAME=\"" $xml_file \
        | sed -e 's/<REPOSITORY NAME=\"//; s/\" VERSION=\(.*\)//'`
    # echo $old_repository_name
    sedstr="sed -e 's/$old_repository_name/$new_repository_name/'"
    # echo $sedstr
    runfile=$xml_file.reset_rn.sh
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function set_infa_repository_name_shortcut
# Replace any repository name in xml file with spedified value
###########################################################################

function set_infa_repository_name_shortcut {
    typeset xml_file=$1
    typeset new_repository_name=$2
    typeset sedstr=""
    typeset runfile=""

    sedstr="sed -e '/<SHORTCUT COMMENTS =\"/s/\(.*\)REPOSITORYNAME =\"\(.*\)\" VERSIONNUMBER =\"\(.*\)/\1REPOSITORYNAME= \"${new_repository_name}\" VERSIONNUMBER =\"\3/'"
    runfile=$xml_file.change_rn.sh
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_uuid
# Reset Informatica uuid in xml file with to value
###########################################################################

reset_infa_uuid () {
    typeset xml_file=$1
    typeset sedstr=""
    typeset runfile=""
    sedstr="sed -e '/UUID=\"/s/\(.*\) UUID=\"\(.*\)\">/\1 UUID=\"0\">/'"
    runfile=$xml_file.reset_uuid.sh
    # echo $sedstr
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function get_infa_folder_name
# Extract Informatica folder name from xml file; write to global var
# Only works on most granular level where xml contains only one object
###########################################################################

get_infa_folder_name () {
    typeset xml_file=$1
    R_INFA_FOLDER_NAME=`grep "<FOLDER NAME=\"" $xml_file \
                    | sed -e 's/<FOLDER NAME=\"//; s/\" GROUP=\(.*\)//'`
#    echo $R_INFA_FOLDER_NAME
}


###########################################################################
# function set_infa_folder_name
# Replace one folder name in xml file with new one
# Only works on most granular level where xml contains only one object
###########################################################################

set_infa_folder_name () {
    typeset xml_file=$1
    typeset new_folder_name=$2
    typeset old_folder_name
    typeset sedstr=""
    typeset runfile=""
    old_folder_name=`grep "<FOLDER NAME=\"" $xml_file \
        | sed -e 's/<FOLDER NAME=\"//; s/\" VERSION=\(.*\)//'`
    sedstr="sed -e '/<FOLDER NAME=\"/s/<FOLDER NAME=\"$old_folder_name\(.*\)/<FOLDER NAME=\"$new_folder_name\1/'"
    runfile=$xml_file.change_fn.sh
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function get_infa_object_version
# Extract Informatica version info from xml file; write to global var
# Only works on most granular level where xml contains only one object
###########################################################################

get_infa_object_version () {
    typeset xml_file=$1
    typeset -u object_type=$2
    R_INFA_OBJECT_VERSION=`grep "<$object_type " $xml_file \
        | grep  "VERSIONNUMBER =\"" \
        | sed -e 's/\(.*\) VERSIONNUMBER =\"//; s/\">//'`
#    echo "Infa object version =$R_INFA_OBJECT_VERSION"
#    return $R_INFA_OBJECT_VERSION
}


###########################################################################
# function set_infa_folder_name
# Replace Informatica object version info in xml file with new one
# Only works on most granular level where xml contains only one object
###########################################################################

set_infa_object_version () {
    typeset xml_file=$1
    typeset -u object_type=$2
    typeset new_object_version=$3
    typeset old_object_version
    typeset sedstr=""
    typeset runfile=""
    old_object_version=`grep "<$object_type DESCRIPTION =\"" $xml_file \
        | grep  "VERSIONNUMBER =\"" \
        | sed -e 's/\(.*\) VERSIONNUMBER =\"//; s/\">//'`
    sedstr="sed -e '/<SESSTRANSFORMATIONINST/s/\(.*\) VERSIONNUMBER =\"$old_object_version\(.*\)/\1 VERSIONNUMBER =\"$new_object_version\2/'"
    runfile=$xml_file.change_ov.sh
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_version_number
# Reset Informatica object version info in xml file to default value
###########################################################################

reset_infa_version_number () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/VERSIONNUMBER =\"/s/\(.*\) VERSIONNUMBER =\"[0-9]*\(.*\)/\1 VERSIONNUMBER =\"0\2/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_integration_service
# Reset Informatica integration service info in xml file to default value
###########################################################################

reset_infa_integration_service () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/ SERVERNAME =\"/s/\(.*\) SERVERNAME =\"\(.*\)\" SERVER_DOMAINNAME\(.*\)/\1 SERVERNAME =\"\" SERVER_DOMAINNAME\3/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_domain
# Reset Informatica domain info in xml file to default value
###########################################################################

reset_infa_domain () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/ SERVER_DOMAINNAME =\"/s/\(.*\) SERVER_DOMAINNAME =\"\(.*\)\" SUSPEND_ON_ERROR\(.*\)/\1 SERVER_DOMAINNAME =\"\" SUSPEND_ON_ERROR\3/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function set_infa_domain
# Set Informatica domain info in xml file to given value
###########################################################################

set_infa_domain () {
    typeset xml_file=$1
    typeset domain_to_set=$2
    cat $xml_file | sed -e '/ SERVER_DOMAINNAME =\"/s/\(.*\) SERVER_DOMAINNAME =\"\(.*\)\" SUSPEND_ON_ERROR\(.*\)/\1 SERVER_DOMAINNAME =\"XXXdomain_to_setXXX\" SUSPEND_ON_ERROR\3/' | sed -e "s/XXXdomain_to_setXXX/${domain_to_set}/"> $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function add_query_banding_info
# add default query banding info t
###########################################################################

add_query_banding_info () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/<ATTRIBUTE NAME =\"Query Band Expression\" VALUE =\"\"\/>/s/<ATTRIBUTE NAME =\"Query Band Expression\" VALUE =\"\"\/>/<ATTRIBUTE NAME =\"Query Band Expression\" VALUE =\"ApplicationName=Informatica;RepositoryServiceName=\$PMRepositoryServiceName;IntegrationServiceName=\$PMIntegrationServiceName;Source=\$PMFolderName;Action=\$PMWorkflowName;SessionName=\$PMSessionName;MappingName=\$PMMappingName;\"\/>/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_session_props
# Reset Informatica session property info in xml file to default value
###########################################################################

reset_infa_session_props () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/<SESSTRANSFORMATIONINST /s/\(.*\) PIPELINE =\"[0-9]*\(.*\) STAGE =\"[0-9]*\(.*\)/\1 PIPELINE =\"0\2 STAGE =\"0\3/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_export_date
# Reset Informatica export date info in xml file to default value
###########################################################################

reset_infa_export_date () {
    typeset xml_file=$1
    typeset pc_date_pattern="[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]"
    typeset pc_date_default="01\/01\/2011 00:00:00"

    cat $xml_file | sed -e "s/POWERMART CREATION_DATE=.${pc_date_pattern}/POWERMART CREATION_DATE=\"${pc_date_default}/" > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_folder_permission
# Reset Informatica folder permission info in xml file to default value
###########################################################################

reset_infa_folder_permission () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/PERMISSIONS=\"/s/\(.*\) PERMISSIONS=\".........\(.*\)/\1 PERMISSIONS=\"---------\2/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_folder_owner
# Reset Informatica folder permission info in xml file to default value
###########################################################################

reset_infa_folder_owner () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/<FOLDER NAME=/s/OWNER=\"\(.*\)\" SHARED=\"/OWNER=\"\" SHARED=\"/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function reset_infa_folder_description
# Reset Informatica domain info in xml file to default value
###########################################################################

reset_infa_folder_description () {
    typeset xml_file=$1
    cat $xml_file | sed -e '/ SERVER_DOMAINNAME =\"/s/\(.*\) SERVER_DOMAINNAME =\"\(.*\)\" SUSPEND_ON_ERROR\(.*\)/\1 SERVER_DOMAINNAME =\"\" SUSPEND_ON_ERROR\3/' > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function sort_xml_file
# Sorts any file
###########################################################################

sort_xml_file () {
    typeset xml_file=$1
    cat $xml_file | sort > $xml_file.tmp
    mv $xml_file.tmp $xml_file
}


###########################################################################
# function check_if_import_necessary
#     Check if an Informatica xml file really needs to be imported
#     Steps:
#     a) export target Infa object and reset some specific entries to default values
#     b) 
###########################################################################


check_if_import_necessary ()
    {
    typeset infa_changed_object=$1
    typeset reference_object=$2
    typeset compare_object=$3
    typeset obj_name=$4
    typeset obj_type=$5
    typeset obj_folder=$6
    
    # export the object from the target folder for comparison
    # check if object exists in target repository - this avoids unnecessary error & log messages
    NEEDS_IMPORT="false"
    if [ $($PMREP listobjects -o $obj_type -f $obj_folder | grep $obj_name | wc -l  ) -gt 0 ] ; then
        $PMREP ObjectExport -n $obj_name -o $obj_type -f $obj_folder -b -u $reference_object

        ################################################################
        # deal with the reference object just epxorted
        # apply a few changes to reference XML file to enable comparison
        ################################################################
        # reset export date to default value
        reset_infa_export_date $reference_object
        # reset all version numbers to 0
        reset_infa_version_number $reference_object
        # set repository specific uuid to default value
        reset_infa_uuid $reference_object
        # reset folder permissions
        reset_infa_folder_permission $reference_object
        # reset folder owner
        reset_infa_folder_owner $reference_object
        # reset folder description
        reset_infa_folder_description $reference_object

        if [ "$obj_type" = "workflow" -o "$obj_type" = "worklet" ] ; then
            # reset partitioning info
            reset_infa_session_props $reference_object
            # reset integration service name
            reset_infa_integration_service $reference_object
            # reset security domain name
            reset_infa_domain $reference_object
        fi 
        if [ "$obj_type" = "workflow" -o "$obj_type" = "worklet"  -o "$obj_type" = "mapping" ] ; then
            # sort the file, since the order of elements is completely arbitrary
            sort_xml_file $reference_object
        fi

        # extract repository information from target object
        get_infa_repository_name $reference_object
        target_repository_name=$R_INFA_REP_NAME

        
        ################################################################
        # deal with the object to import
        # copy import object to temp location, since some changes are necessary for comparison
        ################################################################
        cp $infa_changed_object $compare_object
        # apply a few changes to import XML file to enable comparison
        
        # set repository name to target repository
        reset_infa_repository_name $compare_object $target_repository_name
        # reset all version numbers to 0
        reset_infa_version_number $compare_object
        # set repository specific uuid to default value
        reset_infa_uuid $compare_object
        # reset folder permissions
        reset_infa_folder_permission $compare_object
        # reset folder owner
        reset_infa_folder_owner $compare_object
        # reset folder description
        reset_infa_folder_description $compare_object

        if [ "$obj_type" = "workflow" -o "$obj_type" = "worklet" ] ; then
            # reset partitioning info
            reset_infa_session_props $compare_object
            # reset integration service name
            reset_infa_integration_service $compare_object
            # reset security domain name
            reset_infa_domain $compare_object
        fi 
        if [ "$obj_type" = "workflow" -o "$obj_type" = "worklet"  -o "$obj_type" = "mapping" ] ; then
            # sort the file, since the order of elements is completely arbitrary
            sort_xml_file $compare_object
        fi

        # perform the diff between existing target object and import object
        if [ $( diff $compare_object $reference_object | wc -l ) -ne 0 ] ; then
            echo "Folder $obj_folder / $obj_type object $obj_name is different - will import"
            NEEDS_IMPORT="true"
        else
            echo "Folder $obj_folder / $obj_type object $obj_name is unchanged - no import"
            NEEDS_IMPORT="false"
        fi
    else
        echo "Folder $obj_folder / $obj_type object $obj_name is new - will import"
        NEEDS_IMPORT="true"
    fi

    }


###########################################################################
# function write_import_control_file_specific
# 
###########################################################################

write_import_control_file_specific ()
    {
    typeset xml_ctrl_file=$1
    typeset import_object=$2
    typeset src_repos=$3
    typeset tgt_repos=$4
    typeset checkin_yes_no=$5
    typeset checkin_comments=$6
    typeset label_name=$7

    # control file starts here
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"                                        > $xml_ctrl_file
    echo "<!DOCTYPE IMPORTPARAMS SYSTEM \"${INFA_HOME}/server/bin/impcntl.dtd\">"           >> $xml_ctrl_file
    echo "<IMPORTPARAMS CHECKIN_AFTER_IMPORT=\"${checkin_yes_no}\""                         >> $xml_ctrl_file
    echo "              CHECKIN_COMMENTS=\"${checkin_comments}\""                           >> $xml_ctrl_file
    echo "              APPLY_LABEL_NAME=\"${label_name}\""                                 >> $xml_ctrl_file
    echo "              RETAIN_GENERATED_VALUE=\"YES\">"                                    >> $xml_ctrl_file
# 
    grep -E "FOLDER NAME=\"|FOLDERNAME =\"" $import_object \
        | sed -e 's/<FOLDER NAME=\"//; s/\" GROUP\(.*\)//; s/\(.*\)FOLDERNAME =\"//; s/\" NAME\(.*\)//' \
        | sort -u \
        | while read SOURCE_FOLDER
            do
                echo "<FOLDERMAP SOURCEFOLDERNAME=\"${SOURCE_FOLDER}\" SOURCEREPOSITORYNAME=\"${src_repos}\""  >> $xml_ctrl_file
                echo "           TARGETFOLDERNAME=\"${SOURCE_FOLDER}\" TARGETREPOSITORYNAME=\"${tgt_repos}\"/>" >> $xml_ctrl_file
            done
    echo "<RESOLVECONFLICT>"                                                                >> $xml_ctrl_file
    echo "<TYPEOBJECT OBJECTTYPENAME=\"All\" RESOLUTION=\"REPLACE\"/>"                      >> $xml_ctrl_file
    echo "</RESOLVECONFLICT>"                                                               >> $xml_ctrl_file
    echo "</IMPORTPARAMS>"                                                                  >> $xml_ctrl_file
    # control file ends here
    # cat $xml_ctrl_file
}


###########################################################################
# function scan_pmrep_log_for_error
#     
###########################################################################

scan_pmrep_log_for_error ()
    {
    typeset logfile=$1
    ERROR_CODE="0"
    ERROR_TEXT=""
    WARNING_TEXT=""

    # Warning only
    if [ $(grep -E "(WARNING)|<Warning>" $logfile | wc -l) -ne 0 ] ; then
        ERROR_CODE="4"
        WARNING_TEXT=`grep -E "(WARNING)|<Warning>" $logfile`
    fi

    # The single log for specific object could not be extracted from large import log
    if [ $(grep "Cannot determine status of log file!" $logfile | wc -l) -ne 0 ] ; then
        ERROR_CODE="2"
        WARNING_TEXT="Cannot determine status of log file!"
    fi

    # Object already exists and cannot be overwritten
    if [ $(grep " already exists" $logfile | wc -l) -ne 0 ] ; then
        ERROR_CODE="5"
        ERROR_TEXT=`grep " already exists" $logfile`
    fi

    # Serious import error
    if [ $(grep "Failed" $logfile | wc -l) -ne 0 ] ; then
        ERROR_CODE="8"
        ERROR_TEXT=`grep -E "<Error> :|Invalid" $logfile`
    fi

    # Object is skipped and not imported
    if [ $(grep "Skipping" $logfile | wc -l) -ne 0 ] ; then
        ERROR_CODE="6"
        ERROR_TEXT=`grep "Skipping" $logfile`
    fi


    }

###########################################################################
# function not used
#     
###########################################################################

write_import_control_file ()
    {
    typeset xml_ctrl_file=$1
    typeset src_repos=$2
    typeset tgt_repos=$3
    typeset folder_list=$4
    typeset src_env=$5
    typeset tgt_env=$6
    typeset checkin_comment=$7
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > $xml_ctrl_file
    echo "<!DOCTYPE IMPORTPARAMS SYSTEM \"/var/opt/informatica/powercenter861/server/bin/impcntl.dtd\">" >> $xml_ctrl_file
    echo "<IMPORTPARAMS CHECKIN_AFTER_IMPORT=\"YES\" CHECKIN_COMMENTS=\"$checkin_comment\">" >> $xml_ctrl_file
    cat $folder_list | while read folder_name
    do
        echo "<FOLDERMAP SOURCEFOLDERNAME=\"${src_env}_${folder_name}\"" >> $xml_ctrl_file
        echo "           SOURCEREPOSITORYNAME=\"$src_repos\"" >> $xml_ctrl_file
        echo "           TARGETFOLDERNAME=\"${tgt_env}_${folder_name}\"" >> $xml_ctrl_file
        echo "           TARGETREPOSITORYNAME=\"$tgt_repos\"/>" >> $xml_ctrl_file
        echo "<RESOLVECONFLICT>" >> $xml_ctrl_file
    done
    echo "<TYPEOBJECT OBJECTTYPENAME=\"All\" RESOLUTION=\"REPLACE\"/>" >> $xml_ctrl_file
    echo "</RESOLVECONFLICT>" >> $xml_ctrl_file
    echo "</IMPORTPARAMS>" >> $xml_ctrl_file
    }


###########################################################################
# function write_import_control_file_wrk2dev
#     
###########################################################################

write_import_control_file_wrk2dev () {
    typeset xml_ctrl_file=$1
    typeset src_repos=$2
    typeset tgt_repos=$3
    typeset src_folder=$4
    typeset tgt_folder=$5
    typeset checkin_yes_no=$6
    typeset checkin_comments=$7

    # control file starts here
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"                                        > $xml_ctrl_file
    echo "<!DOCTYPE IMPORTPARAMS SYSTEM \"${INFA_HOME}/server/bin/impcntl.dtd\">"           >> $xml_ctrl_file
    echo "<IMPORTPARAMS CHECKIN_AFTER_IMPORT=\"${checkin_yes_no}\""                         >> $xml_ctrl_file
    echo "              CHECKIN_COMMENTS=\"${checkin_comments}\""                           >> $xml_ctrl_file
    echo "              RETAIN_GENERATED_VALUE=\"YES\">"                                    >> $xml_ctrl_file
    echo "<FOLDERMAP SOURCEFOLDERNAME=\"${src_folder}\" SOURCEREPOSITORYNAME=\"${src_repos}\""  >> $xml_ctrl_file
    echo "           TARGETFOLDERNAME=\"${tgt_folder}\" TARGETREPOSITORYNAME=\"${tgt_repos}\"/>" >> $xml_ctrl_file
    echo "<RESOLVECONFLICT>"                                                                >> $xml_ctrl_file
    echo "<TYPEOBJECT OBJECTTYPENAME=\"All\" RESOLUTION=\"REPLACE\"/>"                      >> $xml_ctrl_file
    echo "</RESOLVECONFLICT>"                                                               >> $xml_ctrl_file
    echo "</IMPORTPARAMS>"                                                                  >> $xml_ctrl_file
    # control file ends here
    # cat $xml_ctrl_file
}
    

set_connection_from_wrk2dev () {
    typeset xml_file=$1
    typeset runfile
    sedstr="sed -e 's/ZTDC_${USER}/TDC_/g; s/ZTPT_${USER}/TPT_/g'"
    runfile=$XMLFILE.set_connection_from_wrk2dev.sh
    echo "cat $xml_file | $sedstr > $xml_file.tmp" > $runfile
    chmod 755 $runfile
    . $runfile
    rm $runfile

    mv $xml_file.tmp $xml_file
}


###########################################################################
# functions to extract information from transformation xml
#     
###########################################################################

transformation_xml_get_description () {
    typeset xml_file=$1
    R_INFA_TRANSFORMATION_DESCRIPTION=`grep "<TRANSFORMATION DESCRIPTION =\"" $xml_file \
                    | sed -e 's/\(.*\)<TRANSFORMATION DESCRIPTION =\"//; s/\" NAME =\"\(.*\)//'`
}

transformation_xml_get_name () {
    typeset xml_file=$1
    R_INFA_TRANSFORMATION_NAME=`grep "<TRANSFORMATION DESCRIPTION =\"" $xml_file \
                    | sed -e 's/\(.*\)\" NAME =\"//; s/\" OBJECTVERSION =\"\(.*\)//'`
}

transformation_xml_get_type () {
    typeset xml_file=$1
    R_INFA_TRANSFORMATION_TYPE=`grep "<TRANSFORMATION DESCRIPTION =\"" $xml_file \
                    | sed -e 's/\(.*\)\" TYPE =\"//; s/\" VERSIONNUMBER =\"\(.*\)//'`
}




###########################################################################
# functions to extract information from source xml
#     
###########################################################################

source_xml_get_dbdname () {
    typeset xml_file=$1
    R_INFA_SOURCE_DBDNAME=`grep "<SOURCE BUSINESSNAME =\"" $xml_file \
                    | sed -e 's/\(.*\)\" DBDNAME =\"//; s/\" DESCRIPTION =\"\(.*\)//'`
}

source_xml_get_description () {
    typeset xml_file=$1
    R_INFA_SOURCE_DESCRIPTION=`grep "<SOURCE BUSINESSNAME =\"" $xml_file \
                    | sed -e 's/\(.*\)\" DESCRIPTION =\"//; s/\" NAME =\"\(.*\)//'`
}

source_xml_get_name () {
    typeset xml_file=$1
    R_INFA_SOURCE_NAME=`grep "<SOURCE BUSINESSNAME =\"" $xml_file \
                    | sed -e 's/\(.*\)\" NAME =\"//; s/\" OBJECTVERSION =\"\(.*\)//'`
}


###########################################################################
# functions to extract information from target xml
#     
###########################################################################

target_xml_get_description () {
    typeset xml_file=$1
    R_INFA_TARGET_DESCRIPTION=`grep "<TARGET BUSINESSNAME =\"" $xml_file \
                    | sed -e 's/\(.*\)\" DESCRIPTION =\"//; s/\" NAME =\"\(.*\)//'`
}

target_xml_get_name () {
    typeset xml_file=$1
    R_INFA_TARGET_NAME=`grep "<TARGET BUSINESSNAME =\"" $xml_file \
                    | sed -e 's/\(.*\)\" NAME =\"//; s/\" OBJECTVERSION =\"\(.*\)//'`
}


###########################################################################
# functions to extract information from mapping xml
#     
###########################################################################

mapping_xml_get_description () {
    typeset xml_file=$1
    R_INFA_MAPPING_DESCRIPTION=`grep "<MAPPING DESCRIPTION =\"" $xml_file \
                    | sed -e 's/\(.*\) DESCRIPTION =\"//; s/\" ISVALID =\"\(.*\)//'`
}

mapping_xml_get_name () {
    typeset xml_file=$1
    R_INFA_MAPPING_NAME=`grep "<MAPPING DESCRIPTION =\"" $xml_file \
                    | sed -e 's/\(.*\)\" NAME =\"//; s/\" OBJECTVERSION =\"\(.*\)//'`
}


###########################################################################
# functions to extract information from mapplet xml
#     
###########################################################################

mapplet_xml_get_description () {
    typeset xml_file=$1
    R_INFA_MAPPLET_DESCRIPTION=`grep "<MAPPLET DESCRIPTION =\"" $xml_file \
                    | sed -e 's/\(.*\) DESCRIPTION =\"//; s/\" ISVALID =\"\(.*\)//'`
}

mapplet_xml_get_name () {
    typeset xml_file=$1
    R_INFA_MAPPLET_NAME=`grep "<MAPPLET DESCRIPTION =\"" $xml_file \
                    | sed -e 's/\(.*\)\" NAME =\"//; s/\" OBJECTVERSION =\"\(.*\)//'`
}


###########################################################################
# functions to strip header and footer from xml file
#     
###########################################################################

###########################################################################
# functions to create concatenated xml file
#     
###########################################################################

write_folder_header () {
    typeset xml_input=$1
    typeset xml_output=$2
    typeset obj_folder=$3
    typeset -i open_tag_start_line
    typeset -i open_tag_end_line
    # cat "$xml_input" | grep "<FOLDER NAME=\""               >> $xml_output


    get_folder_start_line "$xml_input" "$obj_folder"
    echo "Folder start line = $R_START_LINE"
    if [ "$R_START_LINE" -gt 0 ] ; then
        get_open_tag_end_line "$xml_input" "$R_START_LINE"
        echo "Folder open tag end line = $R_OPEN_TAG_END_LINE"
        if [ "$R_OPEN_TAG_END_LINE" -gt 0 ] ; then
            extract_file_part "$xml_input" "$R_START_LINE" "$R_OPEN_TAG_END_LINE" "$xml_output"
        fi
    fi

}


write_folder_footer () {
    typeset xml_output=$1
    echo "</FOLDER>"                                         >> $xml_output
}


write_xml_file_header () {
    typeset xml_input=$1
    typeset xml_output=$2
    head -n4 "$xml_input"                                   >> $xml_output
}


write_xml_file_footer () {
    typeset xml_output=$1
    echo "</REPOSITORY>
</POWERMART>"                                               >> $xml_output
}


write_xml_body () {
    typeset xml_input=$1
    typeset xml_output=$2
    typeset -i start_line
    typeset -i open_tag_end_line

    start_line=`grep -n "<FOLDER NAME=\"" $xml_input | cut -f 1 -d :`
    if [ "$start_line" -gt 0 ] ; then
        get_open_tag_end_line "$xml_input" "$start_line"
        if [ "$R_OPEN_TAG_END_LINE" -gt 0 ] ; then
            # extract_file_part "$xml_input" "$R_START_LINE" "$R_OPEN_TAG_END_LINE" "$xml_output"
            ((R_OPEN_TAG_END_LINE=$R_OPEN_TAG_END_LINE + 1))
            tail -n +${R_OPEN_TAG_END_LINE} "$xml_input" | sed -e :a -e '$d;N;2,3ba' -e 'P;D'      >> $xml_output

        fi
    fi
}


###########################################################################
# functions to extract single objects from a larger xml file
#     
###########################################################################

get_object_start_line () {
    typeset xml_input=$1
    typeset -u obj_type=$2
    typeset obj_name=$3
    typeset -i start_line
    typeset is_source_dev=0
    typeset dbdname

    # some object types are named differently in query result file and export file
    # change type to make sure the object can be found
    case "$obj_type" in
        SESSIONCONFIG )
            obj_type="CONFIG"
            # echo "converting obj_type from SESSIONCONFIG to CONFIG"
            ;;
        "USER DEFINED FUNCTION" )
            obj_type="EXPRMACRO"
            # echo "converting obj_type from USER DEFINED FUNCTION to EXPRMACRO"
            ;;
        *)
                ;;
    esac

    R_START_LINE=0
    R_IS_SHORTCUT=0
    if  [ `expr index "$obj_name" .` -gt 0 ] ; then
        is_source_dev=1
        # echo "$obj_name is a source definition"
        dbdname=`echo $obj_name | cut -f 1 -d .`
        obj_name=`echo $obj_name | cut -f 2 -d .`
    fi

    # echo "xml_input :   $xml_input"
    # echo "obj_type  :   $obj_type"
    # echo "obj_name  :   $obj_name"
    # echo "dbdname   :   $dbdname"

    # echo "Ist Source: $is_source_dev"
    # scan for the line where specific object starts
    if [ "$is_source_dev" -eq 1 ] ; then
        # echo "scanning for source object"
        start_line=`grep -n "<$obj_type \(.*\)DBDNAME =\"$dbdname\"\(.*\)NAME =\"$obj_name\" \(.*\)" $xml_input | cut -f 1 -d :  | head -n 1`
    else
        # echo "scanning for other objects"
        start_line=`grep -n "<$obj_type \(.*\)NAME =\"$obj_name\" \(.*\)" $xml_input | cut -f 1 -d :  | head -n 1`
    fi
    if [ "$start_line" -gt 0 ] ; then
        R_START_LINE=$start_line
    else
        # check if the required file is a shortcut and scan again
        if [ "$is_source_dev" -eq 1 ] ; then
            start_line=`grep -n "<SHORTCUT COMMENTS =\"\(.*\)DBDNAME =\"$dbdname\" \(.*\)NAME =\"$obj_name\"\(.*\)OBJECTTYPE =\"$obj_type\" \(.*\)" $xml_input | cut -f 1 -d :  | head -n 1`
        else
            start_line=`grep -n "<SHORTCUT COMMENTS =\"\(.*\)NAME =\"$obj_name\"\(.*\)OBJECTTYPE =\"$obj_type\" \(.*\)" $xml_input | cut -f 1 -d :  | head -n 1`
        fi
        if [ "$start_line" -gt 0 ] ; then
            R_START_LINE=$start_line
            R_IS_SHORTCUT=1
            # echo "$obj_name is a shortcut"
        fi
    fi
}


get_object_end_line () {
    typeset xml_input=$1
    typeset -u obj_type=$2
    typeset -i start_line=$3
    typeset -i end_line

    # some object types are named differently in query result file and export file
    # change type to make sure the object can be found
    case "$obj_type" in
        SESSIONCONFIG )
            obj_type="CONFIG"
            # echo "converting obj_type from SESSIONCONFIG to CONFIG"
            ;;
        "USER DEFINED FUNCTION" )
            obj_type="EXPRMACRO"
            # echo "converting obj_type from USER DEFINED FUNCTION to EXPRMACRO"
            ;;
        *)
                ;;
    esac

    R_END_LINE=0
    # scan for the line where specific object ends
    ((end_line=`tail -n +$start_line $xml_input | grep -n "</$obj_type>" | cut -f 1 -d :  | head -n 1` + $start_line - 1))
    if [ "$end_line" -gt 0 ] ; then
        R_END_LINE=$end_line
    fi
}


get_folder_start_line () {
    typeset xml_input=$1
    typeset folder_name=$2
    typeset -i start_line

    # echo "Input file : $xml_input"
    # echo "Folder name: $folder_name"
    start_line=`grep -n "<FOLDER NAME=\"$folder_name\"" $xml_input | cut -f 1 -d :`
    # echo "Start line: $start_line"
    if [ "$start_line" -gt 0 ] ; then
        R_START_LINE=$start_line
    else
        R_START_LINE=-1
    fi
}

extract_file_part () {
    typeset xml_input=$1
    typeset -i start_line=$2
    typeset -i end_line=$3
    typeset xml_output=$4

    # echo "Start line: $start_line"
    # echo "End line  : $end_line"
    ((end_line=$end_line - $start_line + 1))
    # echo "End line (after subtract) : $end_line"

    tail -n +${start_line} "$xml_input" | head -n ${end_line}                   >> $xml_output
}


get_open_tag_end_line () {
    typeset xml_input=$1
    typeset -i start_line=$2
    typeset -i end_line
    typeset found=0

    R_OPEN_TAG_END_LINE=0
    ((end_line=$start_line - 1))
    while [ "$found" -eq 0 ]
    do
        ((end_line=$end_line + 1))
        if [ `sed "${end_line}q;d" "$xml_input" | sed 's/.*\(..\)$/\1/'` = "\">" ] ; then
            found=1
            R_OPEN_TAG_END_LINE=$end_line
        fi
    done
}


###########################################################################
# function convert_doseol2unix
# this function is required since the dos2unix function does not exist everywhere
###########################################################################

convert_doseol2unix () {
    typeset xml_input=$1

    tr -d '\r' < "$xml_input" > "$xml_input".tmp
    mv "$xml_input".tmp "$xml_input"
}


###########################################################################
# function extract_single_log_from_generic_import_log
# Extract log information for one specific object from the file
# containing the complete import log
# Might not contain all subtypes - please extend as needed
###########################################################################

extract_single_log_from_generic_import_log () {
    typeset cons_log=$1
    typeset single_log=$2
    typeset object_name=$3
    typeset object_type=$4
    typeset -i log_line
    typeset -i start_line
    typeset -i end_line
    typeset -i end_limit
    typeset -i cons_lines
    typeset found
    typeset -l name_prefix

    case "${object_type}" in
        transformation)
            name_prefix=`echo $object_name | cut -f 1 -d _`
            # echo $name_prefix
            case "${name_prefix}" in
                agg)
                    object_type="Aggregator"
                    ;;
                exp)
                    object_type="Expression"
                    ;;
                fil)
                    object_type="Filter"
                    ;;
                jnr)
                    object_type="Joiner"
                    ;;
                lkp)
                    object_type="Lookup Procedure"
                    ;;
                nrm)
                    object_type="Normalizer"
                    ;;
                rnk)
                    object_type="Rank"
                    ;;
                rtr)
                    object_type="Router"
                    ;;
                seq)
                    object_type="Sequence"
                    ;;
                sq)
                    object_type="Source Qualifier"
                    ;;
                srt)
                    object_type="Sorter"
                    ;;
                upd)
                    object_type="Update Strategy"
                    ;;
                *)
                    object_type="Custom Transformation"
                    ;;
            esac
            ;;
        task)
            name_prefix=`echo $object_name | cut -f 1 -d _`
            # echo $name_prefix
            case "${name_prefix}" in
                cmd)
                    object_type="Command"
                    ;;
                *)
                    object_type="Unknown"
                    ;;
            esac
            ;;
        source)
            if  [ `expr index "$object_name" .` -gt 0 ] ; then
                object_name=`echo $object_name | cut -f 2 -d .`
            fi            
            ;;
        *)
            ;;
    esac
    # fi


    # echo "cons_log    : $cons_log"
    # echo "single_log  : $single_log"
    # echo "object_name : $object_name"
    # echo "object_type : $object_type"

    rm -rf $single_log
    cons_lines=`cat $cons_log | wc -l`
    # echo "number of lines in consolidated import file: $cons_lines"

    cat "$cons_log" | grep -n "${object_name}[ \.]" | grep -E -i "Skipping| already exists|${object_type}[ :]" | while read LINE
    do
        # echo "found line:"
        # echo "$LINE"
        echo "$LINE"  | cut -f 2- -d :                                      >> $single_log
        start_line=`echo "$LINE" | cut -f 1 -d :`
        end_line=$start_line
        ((end_limit=$start_line + 1))
        found=0
        # echo "Start line : $start_line"
        # echo "End line   : $end_line"
        # echo "End limit  : $end_limit"
        
        while [ "$found" -eq "0" -a "$end_line" -lt "$cons_lines" ]
        do
            ((end_line=$end_line + 1))
            # echo "scanning for additional info in line $end_line"
            # sed -n "${end_line}p" "$cons_log"
            if [ `sed -n "${end_line}p" "$cons_log" | grep -i "${object_type}[ :]" | wc -l` -eq 1 ] ; then
                # echo "Found next object type entry"
                found=1
            else
                # echo "Found additional info"
                sed -n "${end_line}p" "$cons_log"                           >> $single_log
            fi
        done

    done

    if [ ! -e "$single_log" ] ; then
        echo "Cannot determine status of log file!"                         >> $single_log
    fi

}


###########################################################################
# function get_infa_object_subtype_from_object_name
# For certain object types, the subtype is determined from the object name prefix
# Another reason why naming conventions are so important!
# Might not contain all subtypes - please extend as needed
###########################################################################

get_infa_object_subtype_from_object_name () {
    typeset object_name=$1
    typeset object_type=$2
    typeset object_subtype
    typeset -l name_prefix


    case "${object_type}" in
        transformation)
            name_prefix=`echo $object_name | cut -f 1 -d _`
            # echo $name_prefix
            case "${name_prefix}" in
                agg) object_subtype="Aggregator" ;;
                exp) object_subtype="Expression" ;;
                fil) object_subtype="Filter" ;;
                jnr) object_subtype="Joiner" ;;
                lkp) object_subtype="Lookup Procedure" ;;
                nrm) object_subtype="Normalizer" ;;
                rnk) object_subtype="Rank" ;;
                rtr) object_subtype="Router" ;;
                seq) object_subtype="Sequence" ;;
                 sq) object_subtype="Source Qualifier" ;;
                srt) object_subtype="Sorter" ;;
                upd) object_subtype="Update Strategy" ;;
                  *) object_subtype="Custom Transformation" ;;
            esac
            ;;
        task)
            name_prefix=`echo $object_name | cut -f 1 -d _`
            # echo $name_prefix
            case "${name_prefix}" in
                cmd) object_subtype="Command" ;;
                  *) object_subtype="Unknown" ;;
            esac
            ;;

        *)
            object_subtype="none"
            ;;
    esac
    R_INFA_OBJECT_SUBTYPE="$object_subtype"

}


###########################################################################
# function f_object_is_shortcut
# Check if object xml file contains a shortcut
# This function only works if the xml file contains only a single object
###########################################################################

f_object_is_shortcut () {
    typeset xml_input=$1
    typeset -i line_count

    line_count=$( grep -c "<SHORTCUT COMMENTS =\"" "$xml_input")
    # echo "line_count = $line_count"
    return $line_count
}