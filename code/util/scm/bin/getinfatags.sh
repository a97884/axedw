#!/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: getinfatags.sh 8659 2013-03-26 13:23:06Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-03-26 14:23:06 +0100 (tis, 26 mar 2013) $
# Last Revision    : $Revision: 8659 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/getinfatags.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Read all files specified on the command line and print the SOURCE, TARGET, JOIN and FILTER tags
# The files are supposed to be in Informatica xml format
#
for file in $@
do
	tfile=`echo $file | sed -e 's:/: :g'`
	egrep 'TYPE ="TARGET"|TYPE ="SOURCE"|TABLEATTRIBUTE NAME ="Join Condition"|TABLEATTRIBUTE NAME ="Filter Condition"' $file | \

		sed \
			-e 's/[<>\/]//g' \
			-e "s/^/FILENAME=\"$tfile\"/" \
			-e 's/ *INSTANCE / /' \
			-e 's/ =/=/g' \
			-e 's/$/ /' \
			-e 's/" /"; /g' \
			-e 's/TABLEATTRIBUTE NAME/TABLEATTRIBUTE_NAME/g' \
			| \
	awk '
	{
		printf( "TABLEATTRIBUTE_NAME=undef\n" );
		printf( "%s\n", $0);
		printf( "echo \"$FILENAME \\c\"\n" );
		printf( "case \"$TABLEATTRIBUTE_NAME\" in \n" );
		printf( "	\"Join Condition\") echo JOIN $VALUE\n;;\n" );
		printf( "	\"Filter Condition\") echo FILTER $VALUE\n;;\n" );
		printf( "	*) echo $TYPE $TRANSFORMATION_NAME\n" );
		printf( "esac\n" );
	}' | \
	sh | \
	egrep -v 'Dummy_For_Product_Join' | sort | uniq
done
