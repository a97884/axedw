#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID                   : $Id: impxml.sh 30363 2020-02-21 08:30:16Z k9108499 $
# Last ChangedBy       : $Author: k9108499 $
# Last ChangedDate     : $Date: 2020-02-21 09:30:16 +0100 (fre, 21 feb 2020) $
# Last ChangedRevision : $Revision: 30363 $
# Subversion URL       : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/impxml.sh $
# --------------------------------------------------------------------------
# SVN Info END
#
# --------------------------------------------------------------------------
# Purpose     : Import Informatica objects
# Project     : EDW
# Subproject  : all
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date        Author        Description
# 2011-03-14  S.Sutter      Initial version
# 2012-05-02  S.Sutter      Add consolidated import
# 2012-09-06  S.Sutter      Switch to getopts parameter handling; add folder parameter
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
#   
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -m        File list           yes         yes         File containing list of Infa objects to import
# -p        File path           yes         yes         Path containing Infa objects to import
# -e        Target environment  yes         yes         Informatica target environment, e.g. st1
# -b        Build number        yes         yes         Build/release number containing the objects to import
# -c        Old build number    yes         yes         Build number containing the objects already imported
# -f        Folder              yes         no          Restrict import to colon-separated list of Informatica folders
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#   
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Get failed imports from previous run
# 5.) Connect to Informatica repository
# 6.) Check if label exists - if not, create label
# 7.) If Infa import log file does not exist, create it with header line
# 8.) Perform XML import into Informatica repository
# 9.) Validate impacted objects
#10.) Get invalid objects
#
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
#---------------------------------------------------------------------------


THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. $HOME/.infascm_profile
. basefunc.sh
. infafunc.sh

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh -l <file_list> -p <file_path> -e <target_env> -b <build_no> -o <old_build_no> [-f <folder>] [-h]"
    echo "Import Informatica objects into repository"
    echo "       -l <file_list>    : File containing list of Infa objects to import"
    echo "       -p <file_path>    : Path containing Infa objects to import"
    echo "       -e <target_env>   : Informatica target environment, e.g. st1"
    echo "       -b <build_no>     : Build/release number containing the objects to import"
    echo "       -o <old_build_no> : Build/release number containing the objects already imported"
    echo "       [-f <folder>]     : Restrict import to colon-separated list of Informatica folders (optional)"
    echo "       [-h]              : print script syntax help"
    echo ""
    }


#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.error  # ${0%.sh} ${SVN_BASE_URL##*/}
        fi

        mv $LOG_FILE $FINAL_LOG_FILE
        echo "Log file can be found under $FINAL_LOG_FILE"
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

#---------------------------------------------------------------------------
# 2.) Initialization
#---------------------------------------------------------------------------

# global return values from functions
R_INFA_REP_NAME=""
R_INFA_FOLDER_NAME=""
R_INFA_OBJECT_VERSION=""
R_INFA_OBJECT_SUBTYPE=""
typeset -i R_START_LINE
typeset -i R_END_LINE
typeset -i R_OPEN_TAG_END_LINE

# get parameters
USAGE="l:p:e:b:o:f:h"
while getopts "$USAGE" opt; do
    case $opt in
        l  ) P_INFA_CHANGE_LIST="$OPTARG"
             OPT_ISSET_CHANGELIST="true"
             ;;
        p  ) P_INFA_XML_PATH="$OPTARG"
             OPT_ISSET_XMLPATH="true"
             ;;
        e  ) typeset -l P_TARGET_ENV="$OPTARG"
             OPT_ISSET_ENV="true"
             ;;
        b  ) P_BUILD_NO="$OPTARG"
             OPT_ISSET_BUILDNO="true"
             ;;
        o  ) P_BUILD_OLD="$OPTARG"
             OPT_ISSET_BUILDOLD="true"
             ;;
        f  ) P_FOLDER="$OPTARG"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.log
INFA_IMPORT_REDO_LIST=$BUILD_LOG_PATH/infa_import_redo.$P_TARGET_ENV.txt

# large xml file containing all concatenated single object xml files
INFA_TOTAL_XML_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.infa_total.xml
# generated import control file required by Informatica import
IMPORT_CONTROL_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.infa_import_control.xml

# Check if the Informatica xml directory is under SVN control (sys environment) or not (int & prd environment)
if [ `$SVN info "$P_INFA_XML_PATH" 2>&1 | grep "not a working copy" | wc -l` -eq 0 ] ; then
    INFA_XML_IS_SVN="true"
else
    INFA_XML_IS_SVN="false"
fi

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

if [ ! "$OPT_ISSET_CHANGELIST" ] ; then
        echo "Error: Parameter -l(Infa change list) not provided!"
        ERROR_CODE=2
fi
if [ ! "$OPT_ISSET_XMLPATH" ] ; then
        echo "Error: Parameter -p (Infa XML path) not provided!"
        ERROR_CODE=2
fi
if [ ! "$OPT_ISSET_ENV" ] ; then
        echo "Error: Parameter -e (target environment) not provided!"
        ERROR_CODE=2
fi
if [ ! "$OPT_ISSET_BUILDNO" ] ; then
        echo "Error: Parameter -b (build number) not provided!"
        ERROR_CODE=2
fi
if [ ! "$OPT_ISSET_BUILDOLD" ] ; then
        echo "Error: Parameter -o (old build number) not provided!"
        ERROR_CODE=2
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    check_error $ERROR_CODE
fi


echo "###############################################################################"
echo "START ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -l (Change list)                : $P_INFA_CHANGE_LIST"
echo "Parameter -p (Path of xml import files)   : $P_INFA_XML_PATH"
echo "Parameter -e (Target environment)         : $P_TARGET_ENV"
echo "Parameter -b (Build no. to deploy)        : $P_BUILD_NO"
echo "Parameter -o (Build no. already deployed) : $P_BUILD_OLD"
echo "Parameter -f (Infa folder)                : $P_FOLDER"
echo ""
echo "Local build work area path                : $BUILD_WA_PATH"
echo "Local temp path                           : $BUILD_TMP_PATH"
echo "Local logging path                        : $BUILD_LOG_PATH"
echo "Final log file                            : $FINAL_LOG_FILE"
echo ""


#---------------------------------------------------------------------------
# 4.) Get failed imports from previous run
#---------------------------------------------------------------------------

# failed imports from the previous run will be re-inserted into this run
# sort -u removes duplicates that might be imported with the current run anyway
if [ -e $INFA_IMPORT_REDO_LIST ] ; then
    cat $P_INFA_CHANGE_LIST > $P_INFA_CHANGE_LIST.tmp
    cat $INFA_IMPORT_REDO_LIST >> $P_INFA_CHANGE_LIST.tmp
    cat $P_INFA_CHANGE_LIST.tmp | sort -u > $P_INFA_CHANGE_LIST
    rm $P_INFA_CHANGE_LIST.tmp
    echo ""
    echo "The following objects from the previous import run will also be executed:"
    cat $INFA_IMPORT_REDO_LIST
    echo ""
    rm -f $INFA_IMPORT_REDO_LIST
fi

#---------------------------------------------------------------------------
# 5.) Connect to Informatica repository
#---------------------------------------------------------------------------

# connect to Informatica repository
# echo "$PMREP connect -r $REPOSITORY_NAME -d $DOMAIN_NAME -n $REPOSITORY_USER_NAME -s $SECURITY_DOMAIN_NAME -X REPOSITORY_USER_PASSWORD"
$PMREP connect -r "$REPOSITORY_NAME" -d "$DOMAIN_NAME" -n "$REPOSITORY_USER_NAME" -s "$SECURITY_DOMAIN_NAME" -X REPOSITORY_USER_PASSWORD
check_error $?


#---------------------------------------------------------------------------
# 6.) Check if label exists - if not, create label
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Creating Informatica label $INFA_LABEL_NAME, if not existing"
echo "###############################################################################"
echo ""
                
# define label name and checkin comment which will be applied during import
if [ "$P_TARGET_ENV" = "pr" ] ; then
    INFA_LABEL_NAME="import_${P_TARGET_ENV}_release_${P_BUILD_NO}"
    CHECKIN_COMMENTS="Automatic import of $P_TARGET_ENV release $P_BUILD_NO at $SCRIPT_START_DATE. Previously deployed release was $P_BUILD_OLD"
else
    INFA_LABEL_NAME="import_${P_TARGET_ENV}_build_${P_BUILD_NO}"
    CHECKIN_COMMENTS="Automatic import of $P_TARGET_ENV build no. $P_BUILD_NO at $SCRIPT_START_DATE. Previously deployed build no. was $P_BUILD_OLD"
fi

INFA_LABEL_COMMENT="Automatic creation during import of xml files to $P_TARGET_ENV test environment"
# create label only if it does not exist yet - avoids unnecessary error messages
if [ $($PMREP ListObjects -o Label | grep "$INFA_LABEL_NAME" | wc -l) -ne 0 ] ; then
    echo "Standard label $INFA_LABEL_NAME already exists!"
    echo "Creating additional label ${INFA_LABEL_NAME}_${SCRIPT_START_DATE}"
    echo ""
    INFA_LABEL_NAME="${INFA_LABEL_NAME}_${SCRIPT_START_DATE}"
fi
$PMREP CreateLabel -a "$INFA_LABEL_NAME" -c "$INFA_LABEL_COMMENT"

echo ""
echo "... done."
echo ""


#---------------------------------------------------------------------------
# 7.) If Infa import log file does not exist, create it with header line
#---------------------------------------------------------------------------

if [ ! -e $INFA_IMPORT_LOG_FILE ] ; then
    echo "Source Repository;Source Folder;Target Environment;Target Repository;Target Folder;Object Type;Object Subtype;Object name;Object Version;td:module;Build Number;Label;Return Code;Error Message;Warning Message;Import User;Import Start Timestamp;Import End Timestamp" > $INFA_IMPORT_LOG_FILE
fi


#---------------------------------------------------------------------------
# 8.) Perform XML import into Informatica repository
#     For performance reasons, consolitate by folder & object type and import in one batch
#     Afterwards, split the log file and scan the log
#---------------------------------------------------------------------------

# cat $INFA_IMPORT_FOLDER_LIST
# cat $INFA_IMPORT_OBJECT_ORDER_FILE
# cat $P_INFA_CHANGE_LIST | wc -l
# head $P_INFA_CHANGE_LIST


# loop through list of folders where deployment is possible
cat $INFA_IMPORT_FOLDER_LIST | while read OBJFOLDER
do
    # if -f parameter is set, then only folders listed in that parameter should be processed
    if [ -n "$P_FOLDER" ] ; then
        f_find_value_in_param_list "$P_FOLDER" "$OBJFOLDER" ":"
        if [ $? -eq 1 ] ; then
            continue
        fi
    fi
    
    # for each folder, loop through list of possible object types
    # that way, dependent objects are imported in correct order
    cat $INFA_IMPORT_CONSOLIDATED_OBJECT_ORDER_FILE | while read OBJTYPE
    do
        echo ""
        echo "###############################################################################"
        echo "### Processing folder $OBJFOLDER / object type $OBJTYPE:"
        echo "###############################################################################"
        echo ""
        IS_FIRST_OBJECT=1
        # consolidated Informatica xml file containing all objects of certain object type belonging to specific folderP@
        CONSOLIDATED_XML_FILE="$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.consolidated.xml"
        # log filled by Informatica import and scanned to find errors and warnings.
        TMP_IMPORT_LOG=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.infaimport.log
        # scan for those objects belonging to the required folder and object type
        # with the trailing slash "/" in grep, objects that have folder or object type in their name, are treated as regular objects
        # just in case, avoid folders - they have a trailing slash and can be filtered out using grep -v "\/$"
        cat $P_INFA_CHANGE_LIST | grep "$OBJFOLDER\/" | grep "$OBJTYPE\/" | grep -v "\/$" | while read line
        do
            # echo "line is: $line"
            INFA_CHANGED_OBJECT="${P_INFA_XML_PATH}/${line}"
            OBJ_NAME=`basename $line .xml`

            # echo ""
            # echo "###############################################################################"
            echo "# Consolidating folder $OBJFOLDER / $OBJTYPE object $OBJ_NAME ..."
            # echo "###############################################################################"
            # echo ""
            # XML file that should be imported. Since there will be some changes before import, store in temp folder.
            IMPORT_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.import.xml
            # copy of import XML use for compare/diff.
            COMPARE_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.compare.xml
            # reference XML file from target environment used to determine if an import is really necessary
            REFERENCE_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.reference.xml

            # echo "Checking if import of object is really necessary ..."
            NEEDS_IMPORT="true"
            # check_if_import_necessary "$INFA_CHANGED_OBJECT" "$REFERENCE_OBJECT" "$COMPARE_OBJECT" "$OBJ_NAME" "$OBJTYPE" "$OBJFOLDER"
            # echo "... done"

            if [ "$NEEDS_IMPORT" = "true" ] ; then

                # copy import object to temp location
                cp $INFA_CHANGED_OBJECT $IMPORT_OBJECT
                # change some information in import XML file

                if [ "$IS_FIRST_OBJECT" -eq 1 ] ; then
                    # echo "This is the first object of the consolidated file"
                    IS_FIRST_OBJECT=0
                    write_xml_file_header "$IMPORT_OBJECT" "$CONSOLIDATED_XML_FILE"
                    write_folder_header "$IMPORT_OBJECT" "$CONSOLIDATED_XML_FILE" "$OBJFOLDER"
                    # get repository name parameters
                    # extract repository information from source object and fill global variable R_INFA_REP_NAME
                    get_infa_repository_name "$CONSOLIDATED_XML_FILE"
                    SOURCE_REPOSITORY_NAME=$R_INFA_REP_NAME
                    TARGET_REPOSITORY_NAME="$REPOSITORY_NAME"
                fi

                write_xml_body "$IMPORT_OBJECT" "$CONSOLIDATED_XML_FILE"
            fi
            
            # don't remove the temp objects if debugging
            rm -f $COMPARE_OBJECT $REFERENCE_OBJECT $IMPORT_OBJECT
        done

        if [ -e "$CONSOLIDATED_XML_FILE" ] ; then
            # closing tag of last folder
            write_folder_footer "$CONSOLIDATED_XML_FILE"
            # remaining closing tags
            write_xml_file_footer "$CONSOLIDATED_XML_FILE"

            # Since xml file is consolidatet from single xml files,
            # shortcuts might point to a different repository than the xml header's .
            # Let all shortcuts have the same repository name to avoid import errors.
            set_infa_repository_name_shortcut "$CONSOLIDATED_XML_FILE" "$SOURCE_REPOSITORY_NAME"

            IMPORT_CONTROL_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.control.xml

            # create xml import control file from the information 
            write_import_control_file_specific \
                     "$IMPORT_CONTROL_FILE" \
                     "$CONSOLIDATED_XML_FILE" \
                     "$SOURCE_REPOSITORY_NAME" \
                     "$TARGET_REPOSITORY_NAME" \
                     "YES" \
                     "$CHECKIN_COMMENTS" \
                     "$INFA_LABEL_NAME"

            echo "Importing all object of type $OBJTYPE in folder $OBJFOLDER"
            echo "using control file $IMPORT_CONTROL_FILE"
            # cat $IMPORT_CONTROL_FILE
            echo ""

            IMPORT_START_TMS=`date "+%Y-%m-%d %T"`
            $PMREP ObjectImport -i "$CONSOLIDATED_XML_FILE" -c $IMPORT_CONTROL_FILE -p 2>&1 | tee $TMP_IMPORT_LOG
            IMPORT_END_TMS=`date "+%Y-%m-%d %T"`

            echo ""
            echo "### Parsing consolidated log file of folder $OBJFOLDER / object type $OBJTYPE:"
            echo ""

            cat $P_INFA_CHANGE_LIST | grep "$OBJFOLDER\/" | grep "$OBJTYPE\/" | grep -v "\/$" | while read line
            do
                OBJ_NAME=`basename $line .xml`
                INFA_CHANGED_OBJECT="${P_INFA_XML_PATH}/${line}"
                SINGLE_IMPORT_LOG=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.import.log
                extract_single_log_from_generic_import_log "$TMP_IMPORT_LOG" "$SINGLE_IMPORT_LOG" "$OBJ_NAME" "$OBJTYPE"
            
                scan_pmrep_log_for_error $SINGLE_IMPORT_LOG
            
                # in case of an error put the row into the redo list
                # it will then be imported with the next run
                if [ "$ERROR_CODE" = "8" -o "$ERROR_CODE" = "6" ] ; then
                    echo $line >> $INFA_IMPORT_REDO_LIST
                fi

                # Depending if under SVN control or not, get information for log
                if [ "$INFA_XML_IS_SVN" == "true" ] ; then
                    # Determine properties from SVN object
                    SVNPROP_SRCFOLDER=`$SVN propget infa:folder $INFA_CHANGED_OBJECT`
                    SVNPROP_SUBTYPE=`$SVN propget infa:subtype $INFA_CHANGED_OBJECT`
                    SVNPROP_VERSION=`$SVN propget infa:version $INFA_CHANGED_OBJECT`
                    SVNPROP_MODULE=`$SVN propget td:module $INFA_CHANGED_OBJECT`
                else
                    # Determine properties directly from xml file and object name
                    get_infa_folder_name "$INFA_CHANGED_OBJECT"
                    SVNPROP_SRCFOLDER="$R_INFA_FOLDER_NAME"
                    get_infa_object_version "$INFA_CHANGED_OBJECT" "$OBJTYPE"
                    SVNPROP_VERSION="$R_INFA_OBJECT_VERSION"
                    get_infa_object_subtype_from_object_name "$OBJ_NAME" "$OBJTYPE"
                    SVNPROP_SUBTYPE="$R_INFA_OBJECT_SUBTYPE"
                fi

                # Create log row for later data import
                LOGLINE="${SOURCE_REPOSITORY_NAME};${SVNPROP_SRCFOLDER};${P_TARGET_ENV};${TARGET_REPOSITORY_NAME};${OBJFOLDER};${OBJTYPE};${SVNPROP_SUBTYPE};${OBJ_NAME};${SVNPROP_VERSION};${SVNPROP_MODULE};${P_BUILD_NO};${INFA_LABEL_NAME};${ERROR_CODE};${ERROR_TEXT};${WARNING_TEXT};${USER};${IMPORT_START_TMS};${IMPORT_END_TMS}"
                echo $LOGLINE
                echo $LOGLINE >> $INFA_IMPORT_LOG_FILE
            
                # cleanup temp files
                rm -f "$SINGLE_IMPORT_LOG"
            
            done
            # cleanup temp files
            # rm -f "$CONSOLIDATED_XML_FILE"
            # rm -f "$TMP_IMPORT_LOG"
            # rm -f "IMPORT_CONTROL_FILE"
        fi
    done

done


#---------------------------------------------------------------------------
# 8.) Perform XML import into Informatica repository
#     Same as previous step, but this time with single imports
#     Used for more complex object types - slower, but more stable
#---------------------------------------------------------------------------

# loop through list of folders where deployment is possible
cat $INFA_IMPORT_FOLDER_LIST | while read OBJFOLDER
do

    # for each folder, loop through list of possible object types
    # that way, dependent objects are imported in correct order
    cat $INFA_IMPORT_OBJECT_ORDER_FILE | while read OBJTYPE
    do

        # scan for those objects belonging to the required folder and object type
        # with the trailing slash "/" in grep, objects that have folder or object type in their name, are treated as regular objects
        # deleted objects are filtered out with grep -v "^D"
        cat $P_INFA_CHANGE_LIST | grep "$OBJFOLDER\/" | grep "$OBJTYPE\/" | grep -v "\/$" | while read line
        do
            # echo "line is: $line"
            INFA_CHANGED_OBJECT="${P_INFA_XML_PATH}/${line}"
            OBJ_NAME=`basename $line .xml`
            
            echo ""
            echo "###############################################################################"
            echo "# Processsing folder $OBJFOLDER / $OBJTYPE object $OBJ_NAME ..."
            echo "###############################################################################"
            echo ""
            # XML file that should be imported. Since there will be some changes before import, store in temp folder.
            IMPORT_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.import.xml
            # copy of import XML use for compare/diff.
            COMPARE_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.compare.xml
            # reference XML file from target environment used to determine if an import is really necessary
            REFERENCE_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.reference.xml
            # log filled by Informatica import and scanned to find errors and warnings.
            TMP_IMPORT_LOG=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.infaimport.log

            # echo "Checking if import of object is really necessary ..."
            NEEDS_IMPORT="true"
            # check_if_import_necessary "$INFA_CHANGED_OBJECT" "$REFERENCE_OBJECT" "$COMPARE_OBJECT" "$OBJ_NAME" "$OBJTYPE" "$OBJFOLDER"
            # echo "... done"

            if [ "$NEEDS_IMPORT" = "true" ] ; then
                # copy import object to temp location
                cp $INFA_CHANGED_OBJECT $IMPORT_OBJECT
                # change some information in import XML file
                if [ "$OBJTYPE" = "workflow" -o "$OBJTYPE" = "worklet" ] ; then
                    # set target integration service name
                    # change_is.sh $IMPORT_OBJECT s $ENV_UPPER
                    # set target database connections
                    # change_conn.sh $IMPORT_OBJECT s $ENV_UPPER $OBJFOLDER
                    # set target domain
                    set_infa_domain $IMPORT_OBJECT $DOMAIN_NAME
                fi
                
                # create xml import control file from the information 
                IMPORT_CONTROL_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$OBJFOLDER.$OBJTYPE.$OBJ_NAME.control.xml

                # get repository name parameters
                # extract repository information from source object and fill global variable R_INFA_REP_NAME
                get_infa_repository_name $INFA_CHANGED_OBJECT
                SOURCE_REPOSITORY_NAME=$R_INFA_REP_NAME
                TARGET_REPOSITORY_NAME="$REPOSITORY_NAME"
                
                # Determine comment used during checkin of imported Informatica objects
                CHECKIN_COMMENTS="Automatic import of $P_TARGET_ENV build no. $P_BUILD_NO at $SCRIPT_START_DATE. Previously deployed build no. was $P_BUILD_OLD"
                write_import_control_file_specific \
                 "$IMPORT_CONTROL_FILE" \
                 "$IMPORT_OBJECT" \
                 "$SOURCE_REPOSITORY_NAME" \
                 "$TARGET_REPOSITORY_NAME" \
                 "YES" \
                 "$CHECKIN_COMMENTS" \
                 "$INFA_LABEL_NAME"
                
                echo "Importing object $IMPORT_OBJECT"
                echo "using control file $IMPORT_CONTROL_FILE"
                # cat $IMPORT_CONTROL_FILE
                echo ""
                
                IMPORT_START_TMS=`date "+%Y-%m-%d %T"`
                $PMREP ObjectImport -i $IMPORT_OBJECT -c $IMPORT_CONTROL_FILE -p 2>&1 | tee $TMP_IMPORT_LOG
                IMPORT_END_TMS=`date "+%Y-%m-%d %T"`

                scan_pmrep_log_for_error $TMP_IMPORT_LOG
                
                # in case of an error put the row into the redo list
                # it will then be imported with the next run
                if [ "$ERROR_CODE" = "8" -o "$ERROR_CODE" = "6" ] ; then
                    echo $line >> $INFA_IMPORT_REDO_LIST
                fi
                
                # Depending if under SVN control or not, get information for log
                if [ "$INFA_XML_IS_SVN" == "true" ] ; then
                    # Determine properties from SVN object
                    SVNPROP_SRCFOLDER=`$SVN propget infa:folder $INFA_CHANGED_OBJECT`
                    SVNPROP_SUBTYPE=`$SVN propget infa:subtype $INFA_CHANGED_OBJECT`
                    SVNPROP_VERSION=`$SVN propget infa:version $INFA_CHANGED_OBJECT`
                    SVNPROP_MODULE=`$SVN propget td:module $INFA_CHANGED_OBJECT`
                else
                    # Determine properties directly from xml file and object name
                    get_infa_folder_name "$INFA_CHANGED_OBJECT"
                    SVNPROP_SRCFOLDER="$R_INFA_FOLDER_NAME"
                    get_infa_object_version "$INFA_CHANGED_OBJECT" "$OBJTYPE"
                    SVNPROP_VERSION="$R_INFA_OBJECT_VERSION"
                    get_infa_object_subtype_from_object_name "$OBJ_NAME" "$OBJTYPE"
                    SVNPROP_SUBTYPE="$R_INFA_OBJECT_SUBTYPE"
                fi
                
                LOGLINE="${SOURCE_REPOSITORY_NAME};${SVNPROP_SRCFOLDER};${P_TARGET_ENV};${TARGET_REPOSITORY_NAME};${OBJFOLDER};${OBJTYPE};${SVNPROP_SUBTYPE};${OBJ_NAME};${SVNPROP_VERSION};${SVNPROP_MODULE};${P_BUILD_NO};${INFA_LABEL_NAME};${ERROR_CODE};${ERROR_TEXT};${WARNING_TEXT};${USER};${IMPORT_START_TMS};${IMPORT_END_TMS}"
                echo $LOGLINE
                echo $LOGLINE >> $INFA_IMPORT_LOG_FILE
            fi
            
            # don't remove the temp objects if debugging
            # rm -f $COMPARE_OBJECT $REFERENCE_OBJECT $IMPORT_OBJECT $IMPORT_CONTROL_FILE $TMP_IMPORT_LOG
        done
    done

done


#---------------------------------------------------------------------------
# 9.) Validate impacted objects
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Validating impacted objects"
echo "###############################################################################"
echo ""

INFA_QUERY_NAME="q_SCM_Impacted_Objects"
IMPACTED_OBJECTS_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$INFA_QUERY_NAME.result.$SCRIPT_START_DATE.txt
CHECKIN_COMMENTS="Automatic validation of impacted objects after import of $P_TARGET_ENV build no. $P_BUILD_NO at $SCRIPT_START_DATE"

$PMREP ExecuteQuery -q "$INFA_QUERY_NAME" -u "$IMPACTED_OBJECTS_FILE"
check_error $?

echo ""
if [ -e "$IMPACTED_OBJECTS_FILE" ] ; then
    $PMREP Validate -i "$IMPACTED_OBJECTS_FILE" -s -k -m "$CHECKIN_COMMENTS"
    check_error $?
    rm -f $IMPACTED_OBJECTS_FILE
else
    echo "OK. No impacted objects"
fi
echo ""


#---------------------------------------------------------------------------
#10.) Get invalid objects
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Getting invalid objects"
echo "###############################################################################"
echo ""

INFA_QUERY_NAME="q_SCM_Invalid_Objects"
INVALID_OBJECTS_FILE=$BUILD_LOG_PATH/$THIS_SCRIPT.$INFA_QUERY_NAME.result.$SCRIPT_START_DATE.txt

$PMREP ExecuteQuery -q "$INFA_QUERY_NAME" -u "$INVALID_OBJECTS_FILE"
check_error $?

echo ""
if  [ -e "$INVALID_OBJECTS_FILE" ] ; then
    echo "The following objects have been invalidated by the import:"
    echo ""
    cat "$INVALID_OBJECTS_FILE"
    echo ""
    echo "The list of invalid files can also be found in $INVALID_OBJECTS_FILE"
else
    echo "OK. No invalid objects"
fi
echo ""


#---------------------------------------------------------------------------
# FINISH
#---------------------------------------------------------------------------

exit 0

