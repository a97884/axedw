#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: 01_expinfadev.sh 15 2011-06-16 18:51:30Z k9105599 $
# Last Changed By  : $Author: k9105599 $
# Last Change Date : $Date: 2011-06-16 20:51:30 +0200 (Thu, 16 Jun 2011) $
# Last Revision    : $Revision: 15 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/util/scm/bin/01_expinfadev.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : export Informatica objects
# Project     : CORONA
# Subproject  : 
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2011-03-09  	S.Sutter        Initial version
# --------------------------------------------------------------------------
# Description
#   export Informatica workflow from development area and place under SVN control
#
# Dependencies
#   parent  : 
#
# Parameters:
#
# Variables (please add any other variables that you may use):
#   XYZ     : description
#   
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Initialize all variables from ini files and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Check if optional first parameter is a valid module name
# 5.) Update work area with latest Informatica XML transformations from SVN
# 6.) Run query to find objects that should be exported
# 7.) Export items found by query into separate xml files
# 8.) Send XML files to Subversion
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini files and define base functions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. $HOME/.infascm_profile
. basefunc.sh
. infafunc.sh


#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [<modulename>]"
    echo "Export Informatica objects from work folder and place them under SVN control."
    echo "       [<modulename>]  : Optional module name; if provided,"
    echo "                         use as default value for td:module SVN property."
    echo "Steps:"
    echo "      1. Update work area with latest Informatica XML transformations from SVN"
    echo "      2. Run query to find objects that should be exported"
    echo "      3. Export items found by query into separate xml files"
    echo "      4. Send XML files to Subversion"
    echo ""
    }


#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi

        mv $LOG_FILE $FINAL_LOG_FILE
        echo "Log file can be found under $FINAL_LOG_FILE"
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

#---------------------------------------------------------------------------
# 2.) Initialization
#---------------------------------------------------------------------------


INFA_TARGET_FOLDER=$1

# set variables
INFA_QUERY_NAME=q_Wrk2Dev_${USER}
INFA_XML_AREA=$HOME/tmp/infa_xml

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt
QUERY_RESULT_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$INFA_QUERY_NAME.result.$SCRIPT_START_DATE.txt

touch $LOG_MSG_FILE

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

# if [ $# -gt 1 ] ; then
     # echo "Invalid number of parameters!!!"
     # echo ""
     # ERROR_CODE=1
# fi

# if [ $ERROR_CODE -ne 0 ] ; then
    # print_help
    # exit 1
# fi

echo "###############################################################################"
echo "START $THIS_SCRIPT at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameters and variables used:"
echo ""
echo "SVN repository base path (SVN_BASE_URL)      : $SVN_BASE_URL"
echo "Local build work area path (BUILD_WA_PATH)   : $BUILD_WA_PATH"
echo "Local temp path (BUILD_TMP_PATH)             : $BUILD_TMP_PATH"
echo "Local logging path (BUILD_LOG_PATH)          : $BUILD_LOG_PATH"
echo "Final log file (FINAL_LOG_FILE)              : $FINAL_LOG_FILE"
echo "Temporary log message file (LOG_MSG_FILE)    : $LOG_MSG_FILE"
echo ""




#---------------------------------------------------------------------------
# 6.) Run query to find objects that should be exported
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Getting list of Informatica items to export..."
echo "###############################################################################"
echo ""
echo "Will run query $INFA_QUERY_NAME and write list to $QUERY_RESULT_FILE"
echo ""

# connect to Informatica repository
# echo "$PMREP connect -r $REPOSITORY_NAME -d $DOMAIN_NAME -n $REPOSITORY_USER_NAME -s $SECURITY_DOMAIN_NAME -X REPOSITORY_USER_PASSWORD"
$PMREP connect -r "$REPOSITORY_NAME" -d "$DOMAIN_NAME" -n "$REPOSITORY_USER_NAME" -s "$SECURITY_DOMAIN_NAME" -X REPOSITORY_USER_PASSWORD
check_error $?

# run query to determine changed Informatica objects and write results into file
# echo "$PMREP ExecuteQuery -q $INFA_QUERY_NAME -u $QUERY_RESULT_FILE"
$PMREP "ExecuteQuery" -q "$INFA_QUERY_NAME" -u "$QUERY_RESULT_FILE"
check_error $?


#---------------------------------------------------------------------------
# 7.) Export items found by query into separate xml files
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Exporting changed Informatica items ..."
echo "###############################################################################"
echo ""


# Take result file of pmrep query, read line by line, and get fields from each line.
# Then export each item into SVN work area, add relevant SVN properties and commit to SVN.
cat $QUERY_RESULT_FILE | while read qrline
do
    # echo $qrline
    OBJ_FOLDER=`echo $qrline | cut -d "," -f2`
    OBJ_NAME=`echo $qrline | cut -d "," -f3`
    OBJ_TYPE=`echo $qrline | cut -d "," -f4`
    OBJ_SUBTYPE=`echo $qrline | cut -d "," -f5`
    OBJ_VERSION=`echo $qrline | cut -d "," -f6`
    OBJ_SHRTCUT=`echo $qrline | cut -d "," -f7`

    # there is an error in the persistent output file created by the query:
    # for mapplets, object_type is set to 'transformation' while object_subtype is set to 'mapplet'
    # this must be corrected manually
    
    if [ "$OBJ_TYPE" = "transformation" -a "$OBJ_SUBTYPE" = "mapplet" ] ; then
        OBJ_TYPE="mapplet"
        OBJ_SUBTYPE="none"
    fi
	
	# determine export folder within SVN work area for Informatica object
    EXPORT_PATH=$INFA_XML_AREA/$OBJ_FOLDER/$OBJ_TYPE
	# create non-existing export folder within SVN work area
    if [ ! -d "$EXPORT_PATH" ] ; then
        $SVN mkdir --parents "$EXPORT_PATH"
        check_error $?
    fi

	# determine export file name SVN work area for Informatica object
    EXPORT_TARGET_FILE=$EXPORT_PATH/$OBJ_NAME.xml
	
    echo ""
    echo "Exporting Informatica object to $EXPORT_TARGET_FILE"
    echo ""
	
	# perform the actual Informatica export
    $PMREP ObjectExport -n "$OBJ_NAME" -o "$OBJ_TYPE" -t "$OBJ_SUBTYPE" -f "$OBJ_FOLDER" -v "$OBJ_VERSION" -b -u "$EXPORT_TARGET_FILE"
    check_error $?
    
    # change integration service
    set_infa_integration_service "$EXPORT_TARGET_FILE" "is_axedw"
    
    # change connections
	set_connection_from_wrk2dev "$EXPORT_TARGET_FILE"
    
    echo "$EXPORT_TARGET_FILE" >> INFA_CHANGE_LIST

done
echo "... done."


# loop through list of possible object types
# that way, dependent objects are imported in correct order
cat $INFA_IMPORT_OBJECT_ORDER_FILE | while read OBJ_TYPE
do

    # scan for those objects belonging to the required folder and object type
    # with the trailing slash "/" in grep, objects that have folder or object type in their name, are treated as regular objects
    # deleted objects are filtered out with grep -v "^D"
    cat $INFA_CHANGE_LIST | grep "$OBJ_TYPE\/" | while read line
        do
            # echo "line is: $line"
            INFA_CHANGED_OBJECT=$line
            OBJ_NAME=`basename $line .xml`
            
            echo ""
            echo "###############################################################################"
            echo "# Processsing folder $INFA_TARGET_FOLDER / $OBJ_TYPE object $OBJ_NAME ..."
            echo "###############################################################################"
            echo ""
            # XML file that should be imported. Since there will be some changes before import, store in temp folder.
            IMPORT_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$INFA_TARGET_FOLDER.$OBJ_TYPE.$OBJ_NAME.import.xml
            # copy of import XML use for compare/diff.
            COMPARE_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$INFA_TARGET_FOLDER.$OBJ_TYPE.$OBJ_NAME.compare.xml
            # reference XML file from target environment used to determine if an import is really necessary
            REFERENCE_OBJECT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$INFA_TARGET_FOLDER.$OBJ_TYPE.$OBJ_NAME.reference.xml
            # log filled by Informatica import and scanned to find errors and warnings.
            TMP_IMPORT_LOG=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$INFA_TARGET_FOLDER.$OBJ_TYPE.$OBJ_NAME.infaimport.log

            echo "Checking if import of object is really necessary ..."
            # NEEDS_IMPORT="true"
            check_if_import_necessary "$INFA_CHANGED_OBJECT" "$REFERENCE_OBJECT" "$COMPARE_OBJECT" "$OBJ_NAME" "$OBJ_TYPE" "$INFA_TARGET_FOLDER"
            echo "... done"

            if [ "$NEEDS_IMPORT" = "true" ] ; then
                # copy import object to temp location
                cp $INFA_CHANGED_OBJECT $IMPORT_OBJECT
                # change some information in import XML file
                # if [ "$OBJ_TYPE" = "workflow" -o "$OBJ_TYPE" = "worklet" ] ; then
                    # set target integration service name
                    # change_is.sh $IMPORT_OBJECT s $ENV_UPPER
                    # set target database connections
                    # change_conn.sh $IMPORT_OBJECT s $ENV_UPPER $INFA_TARGET_FOLDER
                # fi
                
                # create xml import control file from the information 
                IMPORT_CONTROL_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.$INFA_TARGET_FOLDER.$OBJ_TYPE.$OBJ_NAME.control.xml

                # get repository name parameters
                # extract repository information from source object and fill global variable R_INFA_REP_NAME
                get_infa_repository_name $INFA_CHANGED_OBJECT
                SOURCE_REPOSITORY_NAME=$R_INFA_REP_NAME
                TARGET_REPOSITORY_NAME="$REPOSITORY_NAME"
                
                # Determine comment used during checkin of imported Informatica objects
                CHECKIN_COMMENTS="Automatic import of $P_TARGET_ENV build no. $P_BUILD_NO at $SCRIPT_START_DATE. Previously deployed build no. was $P_BUILD_OLD"
                write_import_control_file_specific \
                         "$IMPORT_CONTROL_FILE" \
                         "$SOURCE_REPOSITORY_NAME" \
                         "$TARGET_REPOSITORY_NAME" \
                         "z_${USER}" \
                         "$INFA_TARGET_FOLDER" \
                         "YES" \
                         "$CHECKIN_COMMENTS" 
                
                echo "Importing object $IMPORT_OBJECT"
                echo "using control file $IMPORT_CONTROL_FILE"
                # cat $IMPORT_CONTROL_FILE
                echo ""
                
                $PMREP ObjectImport -i $IMPORT_OBJECT -c $IMPORT_CONTROL_FILE -p 2>&1
                
            fi
            
            # don't remove the temp objects if debugging
            # rm -f $COMPARE_OBJECT $REFERENCE_OBJECT $IMPORT_OBJECT $IMPORT_CONTROL_FILE $TMP_IMPORT_LOG
        done
done



# disconnect from Informatica repository
$PMREP cleanup
check_error $?



#---------------------------------------------------------------------------
# FINISH
#---------------------------------------------------------------------------

exit 0