#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: get_files_per_svnprop.sh 4637 2012-08-09 00:17:43Z k9108499 $
# Last Changed By  : $Author: k9108499 $
# Last Change Date : $Date: 2012-08-09 02:17:43 +0200 (tor, 09 aug 2012) $
# Last Revision    : $Revision: 4637 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/get_files_per_svnprop.sh $
#---------------------------------------------------------------------------
# SVN Info END
#---------------------------------------------------------------------------
#
# Purpose     : Get list of files that have specified Subversion property set within a certain path
# Project     : Axfood
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2008-12-01 S.Sutter       Initial version
# 2009-04-07 T.Bachmann     Parameter assignment changed
# 2011-03-08 S.Sutter       Adapt to Axfood specifics
# 2012-07-31 S.Sutter       Switch to getopts parameter handling 
#
# --------------------------------------------------------------------------
# Description
# --------------------------------------------------------------------------
# Get list of files that have specified Subversion property set within a certain path
#
# --------------------------------------------------------------------------
# Dependencies
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -p        Property path       yes         no          SVN path to search; $SVN_BASE_URL by default
# -u        SVN URL             yes         no          Subversion main URL containing the directory to search; "trunk/code" by default
# -n        Property name       yes         no          Name of SVN property to search for; "td:module" by default
# -v        Property value      yes         no          Value of SVN property to search for; if none provided then take all values
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#   
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialization
# 2.) get list of files with certain SVN property value
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialization
#---------------------------------------------------------------------------

. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-u <url>] [-p <proppath>] [-n <propname>] [-v <propval>] [-h]"
    echo "distinct value list of td:module property within specified SVN path"
    echo "       [-u <url>]      : Subversion base URL containing the code to search."
    echo "                         If omitted, use default value SVN_BASE_URL from the .scm_config file:"
    echo "                         ($SVN_BASE_URL)"
    echo "       [-p <proppath>] : Path of code to search within the SVN repository"
    echo "                         If omitted, use default value $PROP_PATH"
    echo "       [-n <propname>] : Name of SVN property to search for."
    echo "                         If omitted, use default value $PROP_NAME."
    echo "       [-v <propval>]  : Name of SVN property value to search for."
    echo "                         If omitted, then take all values."
    echo "       [-h]            : Print script syntax help"
    echo ""
    }

# default values that can be overwritten by parameters
SVN_URL=$SVN_BASE_URL
PROP_PATH="trunk/code"
PROP_NAME="td:module"

# get parameters
USAGE="u:p:n:v:h"
while getopts "$USAGE" opt; do
    case $opt in
        u  ) SVN_URL="$OPTARG"
             ;;
        p  ) PROP_PATH="$OPTARG"
             ;;
        n  ) PROP_NAME="$OPTARG"
             ;;
        v  ) PROP_VAL="$OPTARG"
             OPT_ISSET_PROPVAL="true"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# --------------------------------------------------------------------------
# 2.) get distinct list of SVN property values
# --------------------------------------------------------------------------

# Build sed search string
SRC_PATH_SED=$( echo $SVN_URL/$PROP_PATH | sed -e 's/\//\\\//'g )


if [ ! "$OPT_ISSET_PROPVAL" ]
then
    # This is for all property values
    $SVN propget -R $PROP_NAME $SVN_URL/$PROP_PATH | sed -e "s/$SRC_PATH_SED\///" | sort +2 +0
    # is it possible to sort after columns 2 first and then after column 0???
else
    # This is for one specific property value
    $SVN propget -R $PROP_NAME $SVN_URL/$PROP_PATH | grep $PROP_VAL | sed -e "s/$SRC_PATH_SED\///" | sed -e "s/ - $PROP_VAL$//" | sort
fi