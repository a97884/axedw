#!/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID                    : $Id: 02_mrg2sys.sh 10406 2013-09-23 15:16:53Z k9105194 $
# Last Changed By       : $Author: k9105194 $
# Last Changed Date     : $Date: 2013-09-23 17:16:53 +0200 (mån, 23 sep 2013) $
# Last Changed Revision : $Revision: 10406 $
# Subversion URL        : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/02_mrg2sys.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : merge from trunk to system test stage
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date          Author          Description
# 2009-08-17    S.Sutter        Initial version
# 2011-03-07    S.Sutter        Adapt to Axfood specifics
# 2012-06-30    S.Sutter        Switch to getopts parameter handling; add restart capability
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# merge from trunk to system test stage
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -m        Module name         yes         yes         
# -r        Revision number     yes         no
# -e        Environment         yes         no
# -c        Cleanup work area   no          no
# -s        Restart             no          no
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Verify module name correctness
# 5.) Determine latest revision 
# 6.) Check for uncommitted changes in merge work area and cleanup if required
# 7.) Update merge target work area 
# 8.) Get merge info and perform merge accordingly
# 9.) Cleanup & finish
#
# --------------------------------------------------------------------------
# Open points
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh -m <module> [-r <revno>] [-s] [-h]"
    echo "Merge from development trunk to system test staging area"
    echo "       -m <module>   : module names to merge. If several modules need to be merged"
    echo "                       provide them separated by colon :"
    echo "       [-r <revno>]  : Revision number to merge. Use valid revision number."
    echo "                       If omitted, HEAD revision is used."
    echo "       [-e <envname>]: Environment to merge from. Default is uv2"
    echo "       [-c]          : Cleanup merge work area if uncommited changes are found"
    echo "       [-s]          : Restart the script and skip steps already performed"
    echo "       [-g]          : Do not automatically include module global_shared"
    echo "       [-i]          : Do not automatically include module INFA_COMMON"
    echo "       [-h]          : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE="$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.error"
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv "$LOG_FILE" "$FINAL_LOG_FILE"
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f "$LOG_MSG_FILE"
        rm -f "$CHANGE_FILE"
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

#---------------------------------------------------------------------------
# 2.) Initialization
#---------------------------------------------------------------------------

typeset -l ENVNAME

# get parameters
MODULE_NAME=""
REV_NO=""
ENVNAME="uv2"
REVISION=""
RESTART_FILE="$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.restart.txt"
CLEANUP_FLAG="false"
RESTART_FLAG="false"
AUTO_INCL_GLOBAL_SHARED="true"
AUTO_INCL_INFA_COMMON="true"
USAGE="m:r:e:cshgi"
while getopts "$USAGE" opt; do
    case $opt in
        m  ) MODULE_NAME="$OPTARG"
             ;;
        r  ) REV_NO="$OPTARG"
             REVISION="-r$REV_NO"
             ;;
        e  ) ENVNAME="$OPTARG"
             ;;
        c  ) CLEANUP_FLAG="true"
             ;;
        g  ) AUTO_INCL_GLOBAL_SHARED="false"
             ;;
        i  ) AUTO_INCL_INFA_COMMON="false"
             ;;
        s  ) if [ -e "$RESTART_FILE" ] ; then
                 RESTART_FLAG="true"
             fi
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

#
# setup environment specific variables
#
. $SCM_BASE/scm/config/.scm_profile.$ENVNAME

#
# update restart file name in case we are in a different environment
#
RESTART_FILE="$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.restart.txt"

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

FINAL_LOG_FILE="$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.log"

CHANGE_FILE="$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.changes.txt"
LOG_MSG_FILE="$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt"
touch "$LOG_MSG_FILE"

CODE_ROOT="$SVN_BASE_URL/trunk/code"

# re-route all output to screen and logfile simultaneously
tee "$LOG_FILE" >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

if [ -z "$MODULE_NAME" -a "$RESTART_FLAG" == "false" ] ; then
    echo "No module name provided - at least one module name is required!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""

if [ "$RESTART_FLAG" == "true" ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Restart parameter provided - will use restart file $RESTART_FILE:"
    echo "###############################################################################"
                                                            cat $RESTART_FILE
    echo "###############################################################################"
    echo ""
    f_get_par_from_restart_file "$RESTART_FILE" MODULE_NAME
    MODULE_NAME="$GLOBAL_RETURN_VALUE"
    f_get_par_from_restart_file "$RESTART_FILE REV_NO"
    REV_NO="$GLOBAL_RETURN_VALUE"
    f_get_par_from_restart_file "$RESTART_FILE REVISION"
    REVISION="$GLOBAL_RETURN_VALUE"
else
    rm -f "$RESTART_FILE"
fi

echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -m (Module Name)           : $MODULE_NAME"
echo "Parameter -r (Revision No)           : $REV_NO"
echo "Parameter -e (Environment)           : $ENVNAME"
echo "Parameter -c (Clean Merge Area)      : $CLEANUP_FLAG"
echo "Parameter -g (noauto global_shared)  : $AUTO_INCL_GLOBAL_SHARED"
echo "Parameter -i (noauto INFA_COMMON)    : $AUTO_INCL_INFA_COMMON"
echo "Parameter -s (Restart)               : $RESTART_FLAG"
echo ""
echo "SVN repository base URL              : $SVN_BASE_URL"
echo "SVN directory with code to merge     : $CODE_ROOT"
echo "Local build merge path               : $BUILD_MRG_PATH"
echo "Local temp path                      : $BUILD_TMP_PATH"
echo "Local logging path                   : $BUILD_LOG_PATH"
echo "Final log file                       : $FINAL_LOG_FILE"
echo "List of changed files                : $CHANGE_FILE"
echo "Temporary log message file           : $LOG_MSG_FILE"
echo ""


# The next 2 steps are not required if the script is called with the restart parameter
# in that case the required variable values will be taken from the restart file
if [ "$RESTART_FLAG" == "false" ] ; then

#---------------------------------------------------------------------------
# 4.) Verify module name correctness
#---------------------------------------------------------------------------

    echo ""
    echo "###############################################################################"
    echo "# Verify module name correctness"
    echo "###############################################################################"
    echo ""

    # this strange construct with the newline is necessary,
    # 	since sed on AIX does not understand the newline "\n" character
    echo $MODULE_NAME | sed -e 's/:/\
    /g' | while read READ_MODULE_NAME
    do
        if [ $(cat $MODULENAME_FILE | grep "$READ_MODULE_NAME" | wc -l) -eq 0 ] ; then
            echo "Modulename $READ_MODULE_NAME is not a valid modulename!"
            WRONG_MODULENAME="true"
		elif [ "$READ_MODULE_NAME" == "global_shared" ] ; then
			AUTO_INCL_GLOBAL_SHARED="false"
		elif [ "$READ_MODULE_NAME" == "INFA_COMMON" ] ; then
			AUTO_INCL_INFA_COMMON="false"
        fi
    done

    if [ $WRONG_MODULENAME ] ; then
        echo "Exiting ..."
        echo ""
        exit 1
    else
        echo "OK - all modulenames are valid."
    fi
	
	#
	# if autoinclude of global_shared and/or INFA_COMMON is active add the modules if not already specified on the command line
	#
	if [ "$AUTO_INCL_GLOBAL_SHARED" == "true" ] ; then
		MODULE_NAME="global_shared:$MODULE_NAME" 
    fi
	if [ "$AUTO_INCL_INFA_COMMON" == "true" ] ; then
		MODULE_NAME="INFA_COMMON:$MODULE_NAME" 
    fi
	
    echo ""
    f_write_date
    echo ""



#---------------------------------------------------------------------------
# 5.) Determine latest revision 
#---------------------------------------------------------------------------

    echo ""
    echo "###############################################################################"
    echo "# Determine latest revision"
    echo "###############################################################################"
    echo ""

    if [ -z "$REV_NO" ] ; then
        echo "Determining latest revision, since no revision was specified ..."
        REV_NO=`$SVN info "$CODE_ROOT" | grep Revision | cut -d" " -f2`
        REVISION="-r$REV_NO"
        echo "... latest revision for $CODE_ROOT is $REV_NO"
        echo ""
        f_write_date
        echo ""
    fi

    # write parameters in case of a restart
    f_write_par_to_restart_file "$RESTART_FILE" "MODULE_NAME" "$MODULE_NAME"
    f_write_par_to_restart_file "$RESTART_FILE" "REV_NO" "$REV_NO"
    f_write_par_to_restart_file "$RESTART_FILE" "REVISION" "$REVISION"

fi


#---------------------------------------------------------------------------
# 6.) Check for uncommitted changes in merge work area and cleanup if required
#---------------------------------------------------------------------------

# Removing uncommitted changes always takes place, especially after a restart.
# After all, errors requiring a restart will most likely make this cleanup necessary.
echo ""
echo "###############################################################################"
echo "# Check for uncommitted changes in merge work area"
echo "###############################################################################"
echo ""

if [ $($SVN status "$BUILD_MRG_PATH"  2>&1 | grep -v "not a working copy" | wc -l) -ne 0 ] ; then
    echo "Uncommitted changes in merge work area $BUILD_MRG_PATH!"
    if [ "$CLEANUP_FLAG" == "true" ] ; then
        # This is the faster solution. It does a SVN revert and removes obsolete files.
        # If you want to perform this, just uncomment it and comment the other solution.
        # echo "Will try to clean up ..."
        # f_cleanup_work_area "$BUILD_MRG_PATH"

        # This is the slower solution. It renames the merge work area for later use and creates a new one.
        # Since this means a complete checkout, the following steps can no longer be skipped and the restart flag must be set to false.
        # If you want to perform this, just uncomment it and comment the other solution.
        RESTART_FLAG="false"
        mv "$BUILD_MRG_PATH" "${BUILD_MRG_PATH}.${SCRIPT_START_DATE}"
        check_error $?
        mkdir "$BUILD_MRG_PATH"
        check_error $?
        echo ""
    else
        echo "Please clean up before continuing. Exiting ..."
        exit 1
    fi
fi
echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 7.) Update merge target work area 
#---------------------------------------------------------------------------

f_check_restart_point "$RESTART_FILE" UPDATEMERGEAREA

if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Update merge target work area"
    echo "###############################################################################"
    echo ""

    echo "Updating merge work area ..." 
    echo ""
    td_updwa.sh \
                -w "$BUILD_MRG_PATH" \
                -u "${SVN_BASE_URL}/branches/sys/stage/code" \
                -l "$LOG_FILE" \
                -r "-rHEAD" \
                -q
    check_error $?
    echo ""
    echo "... td_updwa.sh done"
    echo ""
    f_write_date
    echo ""

    f_write_restart_point $RESTART_FILE UPDATEMERGEAREA

else

    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping update of merge target work area"
    echo "###############################################################################"
    echo ""

fi


#---------------------------------------------------------------------------
# 8.) Get merge information and perform merge accordingly
#---------------------------------------------------------------------------

f_check_restart_point "$RESTART_FILE" PERFORMMERGE

if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Get merge information and perform merge accordingly"
    echo "###############################################################################"
    echo ""

    MERGE_TARGET_URL="${SVN_BASE_URL}/branches/sys/stage/code"
    # create merge target URL if it does not exist
    if [ $($SVN info $MERGE_TARGET_URL 2>&1 | grep "Not a valid URL" | wc -l) -ne 0 ] ; then
        echo "Merge target $MERGE_TARGET_URL does not exist - will create."
        $SVN mkdir -m "Automatic creation of merge staging folder" --parents "$MERGE_TARGET_URL"
        check_error $?
        echo ""
    fi

    #   perform the merge
    echo "Calling td_merge.sh ..."
    echo ""

    if [ "$RESTART_FLAG" == "true" ] ; then
        RESTART_OPTION="-s"
    fi

    td_merge2.sh $RESTART_OPTION \
                -c "property" \
                -n "td:module" \
                -v "$MODULE_NAME" \
                -u "$CODE_ROOT" \
                -a "$MERGE_TARGET_URL" \
                -p "$BUILD_MRG_PATH" \
                -l "$LOG_FILE" \
                -m "$LOG_MSG_FILE" \
                -r "$REV_NO"
    check_error $?
    echo ""
    echo "... td_merge.sh done"
    echo ""
    f_write_date
    echo ""

    f_write_restart_point $RESTART_FILE PERFORMMERGE

else

    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping actual merge"
    echo "###############################################################################"
    echo ""

fi


#---------------------------------------------------------------------------
# 9.) Cleanup & finish
#---------------------------------------------------------------------------

rm -f $RESTART_FILE

exit 0
