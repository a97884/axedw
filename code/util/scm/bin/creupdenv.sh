#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: creupdenv.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/creupdenv.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. $HOME/.infascm_profile
. basefunc.sh
. infafunc.sh

typeset -Z6 CUR_BUILD_NO
typeset -Z6 PRV_BUILD_NO
typeset -l ENVNAME
tmp_path=$HOME/tmp

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: `basename $0` [-i] [-d] <environment> <buildno> {full,delta}"
    echo "Create/update SAMBA directory structure from SVN repository"
        echo "      -i              : do not import Informatica objects (default is to import)"
        echo "      -d              : do not apply database changes (default is to apply)"
    echo "      <environment>   : environment name (uv0, st2, ...)"
    echo "      <buildno>       : valid build number from an existing build tag"
    echo "      full or delta   : do full import to Informatica or just deltas"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured.

        echo "Log file can be found under $LOG_FILE"

        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

#---------------------------------------------------------------------------
# 1.) Check for correct syntax
#---------------------------------------------------------------------------
# get parameters
RUN_INFA_IMPORT="true"
RUN_DB_APPLY="true"

USAGE="idh"
while getopts "$USAGE" opt; do
    case $opt in
        i  ) RUN_INFA_IMPORT="false"
             ;;
        d  ) RUN_DB_APPLY="false"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

ENVNAME="$1"
CUR_BUILD_NO="$2"
INSTMODE="$3"

ERROR_CODE=0

if [ $# -ne 3 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    echo ""
    print_help
    exit 1
fi
#
# check that the environment name matches the Informatica repository name
#
REP_NAME="$REPOSITORY_NAME"
typeset -l REP_NAME
#if [ "repository_$ENVNAME" != "$REP_NAME" ]
#then
#       echo "REPOSITORY_NAME ($REPOSITORY_NAME) does not match ENVNAME ($ENVNAME)"
#       echo "Supposed to be: Repository_$ENVNAME"
#       echo "Please make sure they match"
#       exit 1
#fi

env_num="`echo $ENVNAME | cut -c3-5`"
test "$env_num" = "1" && env_num="" # no environment is used in the maintenance environment to make it production like
case "$ENVNAME" in
uv*)
        infa="/opt/axedw/uv/is_axedw$env_num/infa_shared"
        ;;
st*)
        infa="/opt/axedw/st/is_axedw$env_num/infa_shared"
        ;;
it)
        infa="/opt/axedw/it/is_axedw1/infa_shared"
        ;;
pr)
        infa="/opt/axedw/pr/is_axedw1/infa_shared"
        ;;
*)
        infa=$HOME/infa_shared
        ;;
esac
if [ "$INFA_SHARED_DIR" != "$infa" ]
then
        echo "INFA_SHARED_DIR ($INFA_SHARED_DIR) does not match ENVNAME ($ENVNAME)"
    echo "Supposed to be: $infa"
        echo "Please make sure they match"
        exit 1
fi


#---------------------------------------------------------------------------
# 2.) Display parameters and variables
#---------------------------------------------------------------------------

echo "###############################################################################"
echo "START `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter 1 (Environment)     : $ENVNAME"
echo "Parameter 2 (Build number)    : $CUR_BUILD_NO"
echo "Parameter 3 (Install mode)    : $INSTMODE"
echo "Optional parameter 1 (noinfa) : $RUN_INFA_IMPORT"
echo "Optional parameter 2 (nodb)   : $RUN_DB_APPLY"
echo ""

#---------------------------------------------------------------------------
# 3. get list of changed DDLs and display them for information purpose
#    execute the changes in the database
#---------------------------------------------------------------------------
MYHOME="$INFA_SHARED_DIR/.."
#
# the changes.txt is from system test build and needs to be updated
# update the file with the changes in current integration build
#
cd $MYHOME

# some standard variables required to run this script
FINAL_LOG_FILE=$HOME/$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.log
if [ -e filelists/current_releasenumber.txt ] ; then
    CURRENT_RELEASE_NUMBER=`cat filelists/current_releasenumber.txt`
else
    CURRENT_RELEASE_NUMBER="next"
fi

if [ "$INSTMODE" = "delta" ]
then
        added_files="$MYHOME/filelists/`cat filelists/previous_releasenumber.txt`.${CURRENT_RELEASE_NUMBER}.svn_add_ddl.txt"
        changed_files="$MYHOME/filelists/`cat filelists/previous_releasenumber.txt`.${CURRENT_RELEASE_NUMBER}.svn_upd_ddl.txt"
        if [ ! -r "$added_files" ] ; then
                added_files=/dev/null
        fi
        if [ ! -r "$changed_files" ] ; then
                changed_files=/dev/null
        fi
        STATUS_CHANGED_FILE="$added_files $changed_files"
        DB_CHANGED_FILE=$MYHOME/infa_xml/$CUR_BUILD_NO.`date "+%Y%m%d%H%M%S"`.db_changed.txt
elif [ "$INSTMODE" = "full" ]
then
        STATUS_CHANGED_FILE="$MYHOME/filelists/`cat filelists/previous_releasenumber.txt`.${CURRENT_RELEASE_NUMBER}.svn_all_ddl.txt"
        DB_CHANGED_FILE=$MYHOME/dbadmin/$CUR_BUILD_NO.`date "+%Y%m%d%H%M%S"`.db_changed.txt
else
    print_help
    exit 1
fi

DB_STATUS_FILE="$tmp_path/`basename $0`.`date "+%Y%m%d%H%M%S"`.status"
DDL_PATTERN="code.dbadmin"
NUM_OF_CHANGED_DDL=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | wc -l`
if  [ $NUM_OF_CHANGED_DDL -ne 0 -a "$RUN_DB_APPLY" = "true" ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Please be aware that the following DDL scripts have also changed."
    echo "# They will now be applied accordingly to the environment's databases."
    echo "###############################################################################"
    echo ""

    echo "Content of file: $STATUS_CHANGED_FILE"
    cat $STATUS_CHANGED_FILE | grep -v "\/$" \
        | grep "$DDL_PATTERN" \
        | sed -e "s,^$DDL_PATTERN/,dbadmin/," -e "s/^/A /" \
        >$DB_CHANGED_FILE

    cat $DB_CHANGED_FILE
    echo ""
        #
        # start apply_db_chg in background
        #
    (
                apply_db_chg.sh $DB_CHANGED_FILE $MYHOME/dbadmin $CUR_BUILD_NO $ENVNAME
                status=$?
                echo "$status" >$DB_STATUS_FILE
                if [ $status -ne 0 ] ; then
                        echo ""
                        echo ""
                        echo "***********************************************************************************************"
                        echo "************* apply_db_chg.sh returned an ERROR($status),please check the log file ************"
                        echo "***********************************************************************************************"
                        echo ""
                        echo ""
                fi
                echo ""
        ) 2>&1 | while read line; do echo "apply_db_chg.sh: $line"; done &
        db_pid=$!
fi

#---------------------------------------------------------------------------
# 4. get a list of changed Informatica objects and import them
#---------------------------------------------------------------------------
if [ "$RUN_INFA_IMPORT" = "true" ]
then
        INFA_XML_PATTERN="code.infa_xml"
        INFA_XML_PATH="$INFA_SHARED_DIR/../infa_xml"

        if [ "$INSTMODE" = "delta" ]
        then
                        added_files="$MYHOME/filelists/`cat filelists/previous_releasenumber.txt`.${CURRENT_RELEASE_NUMBER}.svn_add_infa.txt"
                        changed_files="$MYHOME/filelists/`cat filelists/previous_releasenumber.txt`.${CURRENT_RELEASE_NUMBER}.svn_upd_infa.txt"
                        if [ ! -r "$added_files" ] ; then
                                        added_files=/dev/null
                        fi
                        if [ ! -r "$changed_files" ] ; then
                                        changed_files=/dev/null
                        fi
                        STATUS_CHANGED_FILE="$added_files $changed_files"
                        INFA_CHANGED_FILE=$MYHOME/infa_xml/$CUR_BUILD_NO.`date "+%Y%m%d%H%M%S"`.infa_changed.txt
        elif [ "$INSTMODE" = "full" ]
        then
                        STATUS_CHANGED_FILE="$MYHOME/filelists/`cat filelists/previous_releasenumber.txt`.${CURRENT_RELEASE_NUMBER}.svn_all_infa.txt"
                        INFA_CHANGED_FILE=$MYHOME/infa_xml/$CUR_BUILD_NO.`date "+%Y%m%d%H%M%S"`.infa_changed.txt
        else
                print_help
                exit 1
        fi

        # checking for new or changed Informatica objects
                echo ""
                echo "###############################################################################"
                echo "# Please be aware that the following Informatica objects have changed."
                echo "# They will now be imported into the Informatica target repository."
                echo "###############################################################################"
                echo ""

                cat $STATUS_CHANGED_FILE | grep -v "\/$" \
                                                                  | grep "$INFA_XML_PATTERN" \
                                                                  | grep -v "^D" \
                                                                  | sed -e "s/^$INFA_XML_PATTERN\///" \
                                                                        > $INFA_CHANGED_FILE
                cat $INFA_CHANGED_FILE
                echo ""

                # run the import of XML files into the Informatica repository

                impxml.sh \
                                -l "$INFA_CHANGED_FILE" \
                                -p "$INFA_XML_PATH" \
                                -e "$ENVNAME" \
                                -b "$CUR_BUILD_NO" \
                                -o "$PRV_BUILD_NO"
fi

#
# wait for the database changes to be finilized
#
if [ "$db_pid" != "" ]
then
        echo "Wait for the database changes ($db_pid) to be finilized..."
        wait $db_pid
        status="`cat "$DB_STATUS_FILE"`"
        echo "Database changes ended with return code $status"
fi

typeset -u ENVNAME
#
# make shure the Informatica parameter file contains an entry for the database environment
#
node_name=edwt
if [ "$ENVNAME" = "PR" ]
then
        node_name=edwp
fi
for file in par/axedwparameters.prm bin/PWD_BteqLogon
do
        sed -e 's/\${DB_ENV}'"/$ENVNAME/g" \
                -e 's,\${INFA_SHARED_DIR}'",$INFA_SHARED_DIR,g" \
                -e 's/\${node_name}'"/$node_name/g" \
        <$INFA_SHARED_DIR/$file >$INFA_SHARED_DIR/$file.new
        mv $INFA_SHARED_DIR/$file $INFA_SHARED_DIR/$file.orig && \
        mv $INFA_SHARED_DIR/$file.new $INFA_SHARED_DIR/$file
done

#
# make shure the logon information for batch scripts are correct
#
export DB_ENV=$ENVNAME

for file in $INFA_SHARED_DIR/bin/*.var
do
        sed -e "s/UserName.*=.*'/UserName = '${DB_ENV}_Common_Trf_00001'/" -e "s/UserPassword.*=.*'/UserPassword = '${DB_ENV}_Common_Trf'/" <$file >$file.new
        mv $file $file.orig && mv $file.new $file
done

sed -e 's/\${DB_ENV}'/$ENVNAME/g -e 's/\${tdpid}/'`if [ "$ENVNAME" = "PR" ]; then echo edwp; else echo edwt; fi`/g <$INFA_SHARED_DIR/par/dbadmin_logon.btq >$INFA_SHARED_DIR/par/dbadmin_logon.btq.new && \
mv $INFA_SHARED_DIR/par/dbadmin_logon.btq $INFA_SHARED_DIR/par/dbadmin_logon.btq.orig && \
mv $INFA_SHARED_DIR/par/dbadmin_logon.btq.new $INFA_SHARED_DIR/par/dbadmin_logon.btq

rm -f "$DB_STATUS_FILE"
#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
