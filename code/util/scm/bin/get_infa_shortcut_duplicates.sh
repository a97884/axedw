#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: get_infa_shortcut_duplicates.sh 6546 2012-10-07 20:57:20Z k9108499 $
# Last Changed By  : $Author: k9108499 $
# Last Change Date : $Date: 2012-10-07 22:57:20 +0200 (sön, 07 okt 2012) $
# Last Revision    : $Revision: 6546 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/get_infa_shortcut_duplicates.sh $
# --------------------------------------------------------------------------
# SVN Info END
#---------------------------------------------------------------------------
#
# Purpose     : Get a distinct value list of specified Subversion property within a certain path
# Project     : Axfood
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2008-12-01 S.Sutter       Initial version
# 2009-04-07 T.Bachmann     Parameter assignment changed
# 2011-03-08 S.Sutter       Adapt to Axfood specifics
# 2012-07-31 S.Sutter       Switch to getopts parameter handling 
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# Get a distinct value list of specified Subversion property within a certain path
#
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -p        Property path       yes         no          SVN path to search; $SVN_BASE_URL by default
# -u        SVN URL             yes         no          Subversion main URL containing the directory to search; "trunk/code" by default
# -n        Property name       yes         no          Name of SVN property to search for; "td:module" by default
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#   
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialization
# 2.) get distinct list of SVN property values
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialization
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh
. infafunc.sh

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-u <url>] [-p <proppath>] [-n <propname>] [-h]"
    echo "Get distinct value list of td:module property within specified SVN path"
    echo "       [-u <url>]      : Subversion base URL containing the code to search."
    echo "                         If omitted, use default value SVN_BASE_URL from the .scm_config file:"
    echo "                         ($SVN_BASE_URL)"
    echo "       [-p <xmlpath>]  : Path of infa xml files to check within the SVN repository"
    echo "                         If omitted, use default value $XML_PATH"
    echo "       [-h]            : Print script syntax help"
    echo ""
    }

# set Defaults
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
LOG_FILE=$BUILD_LOG_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log
touch $LOG_FILE

# default values that can be overwritten by parameters
SVN_URL=$SVN_BASE_URL
XML_PATH="trunk/code/infa_xml"

# get parameters
USAGE="u:p:h"
while getopts "$USAGE" opt; do
    case $opt in
        u  ) SVN_URL="$OPTARG"
             ;;
        p  ) XML_PATH="$OPTARG"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars


# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


echo ""
echo "###############################################################################"
echo "START $THIS_SCRIPT at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameters and variables used:"
echo ""
echo "Parameter -u (SVN base URL)  : $SVN_URL"
echo "Parameter -p (Infa XML Path) : $XML_PATH"
echo ""
echo "SVN repository base path     : $SVN_BASE_URL"
echo "Code work area path          : $BUILD_CODE_PATH"
echo "Local logging path           : $BUILD_LOG_PATH"
echo "Final log file               : $LOG_FILE"
echo ""


#---------------------------------------------------------------------------
# 3.) Check-out / update / switch work area to relevant directory
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Update work area "
echo "###############################################################################"
echo ""

# echo "Checking if there are changes in temp code area ..." 
# $SVN status $BUILD_CODE_PATH
# if [ $($SVN status $BUILD_CODE_PATH | grep -v "not a working copy" | wc -l) -ne 0 ] ; then
#     echo ""
#     echo "Uncommitted changes in merge work area $BUILD_CODE_PATH!!!"
#     echo "Please clean up before continuing. Exiting ..."
#     exit 1
# fi

# remove trailing "infa_xml" directory, since the complete code should be checked out - not just the infa_xml folder
# this avoids problems with other scripts utilizing the same code directory
XML_PARENT=$( echo $XML_PATH | sed -e 's/\/infa_xml$//' )

echo ""
td_updwa.sh \
        -w "$BUILD_CODE_PATH" \
        -u "${SVN_URL}/${XML_PARENT}" \
        -l "$LOG_FILE" \
        -r -rHEAD \
        -q
echo ""
check_error $?



#---------------------------------------------------------------------------
# 3.) Check for duplicate shortcut references
#---------------------------------------------------------------------------

DUPL_SHORTCUT_INFO=$BUILD_LOG_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.duplicate_shortcuts.txt
echo "Timestamp;Folder;ObjectType;RefObject;ShortcutName;DuplicateShortcutName" > "$DUPL_SHORTCUT_INFO"

find "${BUILD_CODE_PATH}/infa_xml" -name "*.xml" | while read filename
do
    f_object_is_shortcut "$filename"
    if [ $? -gt 0 ] ; then

        # echo "checking file $filename"
        FILEPATH=$( dirname "$filename" )
        FILEPATH_SED=$( echo $FILEPATH | sed -e 's/\//\\\//'g )
        OBJ_TYPE=$( basename "$FILEPATH" )
        OBJ_NAME=$( basename "$filename" .xml )
        OBJ_FOLDER=$( basename $( dirname "$FILEPATH" ) )

        # echo "FILEPATH      : $FILEPATH"
        # echo "FILEPATH_SED  : $FILEPATH_SED"
        # echo "OBJ_TYPE      : $OBJ_TYPE"
        # echo "OBJ_FOLDER    : $OBJ_FOLDER"
        # echo "OBJ_NAME      : $OBJ_NAME"

        # count number of xml files in directory
        # if there is just 1, then it's not worth checking (there can't be duplicates anyway)
        # also, with only 1 file, grep will not output the filename and thus the algorithm does not work
        if [ $( ls -l $FILEPATH/*.xml | wc -l ) -gt 1 ] ; then
        
            # fill variables R_INFA_REFOBJECT_NAME and R_INFA_REFOBJECT_DBD (source only) with name of object referenced by shortcut
            get_infa_refobject_name "$filename" "$OBJ_TYPE"

            # object type source needs special handling, since it contains object name & dbd name
            if [ "$OBJ_TYPE" = "source" ] ; then
                DUPL_SHORTCUT_NAME=""
                # 1. get all files that reference the same dbd and source name
                # 2. the file currently examined must of course be filtered out
                # 3. the grep provides the files's name and path - use 'cut' to get it
                # 4. remove the path to get only the file name itself

                DUPL_SHORTCUT_NAME=`grep "<SHORTCUT\(.*\)REFERENCEDDBD =\"$R_INFA_REFOBJECT_DBD\"\(.*\)REFOBJECTNAME =\"$R_INFA_REFOBJECT_NAME\"" $FILEPATH/*.xml \
                  | grep -v "^$FILEPATH_SED\/$OBJ_NAME.xml:" \
                  | cut -f 1 -d ":" \
                  | sed -e "s/$FILEPATH_SED\/\(.*\)\.xml/\1/"`
                if [ \( -n "$DUPL_SHORTCUT_NAME" \) -a \("$OBJ_NAME" != "$DUPL_SHORTCUT_NAME"\) ] ; then
                    # echo "$OBJ_NAME"
                    # echo "$DUPL_SHORTCUT_NAME"
                    DUPL_SHORTCUT_LINE="$SCRIPT_START_DATE;${OBJ_FOLDER};${OBJ_TYPE};${R_INFA_REFOBJECT_NAME};${OBJ_NAME};${DUPL_SHORTCUT_NAME}"
                    echo $DUPL_SHORTCUT_LINE >> $DUPL_SHORTCUT_INFO
                fi

            else
                DUPL_SHORTCUT_NAME=""
                # 1. get all files that reference the same object name
                # 2. the file currently examined must of course be filtered out
                # 3. the grep provides the files's name and path - use 'cut' to get it
                # 4. remove the path to get only the file name itself
                DUPL_SHORTCUT_NAME=`grep "<SHORTCUT\(.*\)REFOBJECTNAME =\"$R_INFA_REFOBJECT_NAME\"" $FILEPATH/*.xml \
                  | grep -v "^$FILEPATH_SED\/$OBJ_NAME.xml:" \
                  | cut -f 1 -d ":" \
                  | sed -e "s/$FILEPATH_SED\/\(.*\)\.xml/\1/"`
                if [ \( -n "$DUPL_SHORTCUT_NAME" \) -a \("$OBJ_NAME" != "$DUPL_SHORTCUT_NAME"\) ] ; then
                    # echo "$OBJ_NAME"
                    # echo "$DUPL_SHORTCUT_NAME"
                    DUPL_SHORTCUT_LINE="$SCRIPT_START_DATE;${OBJ_FOLDER};${OBJ_TYPE};${R_INFA_REFOBJECT_NAME};${OBJ_NAME};${DUPL_SHORTCUT_NAME}"
                    echo $DUPL_SHORTCUT_LINE >> $DUPL_SHORTCUT_INFO
                fi
            fi
        fi
    fi

done


#---------------------------------------------------------------------------
# 4.) Display duplicate shortcut references
#---------------------------------------------------------------------------

# check if duplicate shortcut info file contains more than just the header line
if [ $( cat "$DUPL_SHORTCUT_INFO" | wc -l ) -gt 1 ] ; then
    echo ""
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo ""
    echo "Please be aware that the following files have duplicate shortcut references!"
    echo "This should be fixed ASAP!"
    echo ""
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo ""
    cat $DUPL_SHORTCUT_INFO
    echo ""
    echo "###############################################################################"
    echo "###############################################################################"
fi


#---------------------------------------------------------------------------
# FINISH
#---------------------------------------------------------------------------

echo "###############################################################################"
echo ""
echo "Log file can be found under $LOG_FILE"
echo ""
echo "###############################################################################"
echo "END $THIS_SCRIPT at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""

