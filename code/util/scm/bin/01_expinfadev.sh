#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: 01_expinfadev.sh 7465 2012-11-13 10:02:42Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-11-13 11:02:42 +0100 (tis, 13 nov 2012) $
# Last Revision    : $Revision: 7465 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/01_expinfadev.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : export Informatica objects
# Project     : EDW
# Subproject  : all
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       	Author         	Description
# 2011-03-09  	S.Sutter        Initial version
# 2012-04-18    S.Sutter        Complete re-write of export functionality for performance reason:
#                               Export into on large file then cut this file into pieces.
# 2012-06-30    S.Sutter        Switch to getopts parameter handling; add restart capability
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# export Informatica workflow from development area and place under SVN control
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -m        Module name         yes         no         
# -q        Informatica query   yes         no
# -p        Infa XML path       yes         no
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini files and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Check if value of optional parameter -m is a valid module name
# 5.) Update work area with latest Informatica XML transformations from SVN
# 6.) If Duplicate shortcut file does not exist, create it with header line
# 7.) Run query to find objects that should be exported
# 8.) Export items found by query and split into separate xml files
# 9.) Send XML files to Subversion
# 10.) Display duplicate shortcut references
#
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini files and define base functions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. $HOME/.infascm_profile
. basefunc.sh
. infafunc.sh


#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-m <modulename>] [-q <queryname>] [-p <xmlpath>] [-e <envname>] [-h]"
    echo "Export Informatica objects from work folder and place them under SVN control."
    echo "       [-m <modulename>] : Optional module name; if provided,"
    echo "                           then use it as default value for td:module SVN property."
    echo "       [-q <queryname>]  : Optional name of query to execute in Informatica, default is q_SCM_Exp_Dev2Sys"
    echo "       [-p <xmlpath>]    : Optional SVN path of Informatica XML files, default is trunk/code/infa_xml"
    echo "       [-e <envname>]    : Optional name of source repository to export from, default is to use $REPOSITORY_NAME (REPOSITORY_NAME)"
    echo "       [-h]              : print script syntax help"
    echo ""
    echo "Steps:"
    echo "      1. Update work area with latest Informatica XML transformations from SVN"
    echo "      2. Run query to find objects that should be exported"
    echo "      3. Export items found by query and split into separate xml files"
    echo "      4. Send XML files to Subversion"
    echo "      5. Display duplicate shortcut references"
    echo ""
    }


#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialization
#---------------------------------------------------------------------------
# set variables
INFA_QUERY_NAME=q_SCM_Exp_Dev2Sys
INFA_XML_AREA=$STD_WA_PATH/infa_xml
SVN_INFA_XML_PATH=$SVN_BASE_URL/trunk/code/infa_xml

USAGE="m:q:p:e:h"
while getopts "$USAGE" opt; do
    case $opt in
        m  ) MODULE_NAME="$OPTARG"
             ;;
        q  ) INFA_QUERY_NAME="$OPTARG"
             ;;
        p  ) SVN_INFA_XML_PATH="$SVN_BASE_URL/$OPTARG"
             ;;
        e  ) export REPOSITORY_NAME="Repository_$OPTARG"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt
CLEANUP_SCRIPT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.cleanup.sh
QUERY_RESULT_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$INFA_QUERY_NAME.result.$SCRIPT_START_DATE.txt
TOTAL_EXPORT_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.total_export.$SCRIPT_START_DATE.xml
GLOBAL_HEADER_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.global_header.$SCRIPT_START_DATE.xml
FOLDER_EXPORT_BASE=$BUILD_TMP_PATH/$THIS_SCRIPT.xml_by_folder.$SCRIPT_START_DATE
DUPL_SHORTCUT_FILE=$STD_WA_PATH/infa_xml/duplicate_shortcuts.txt
DUPL_SHORTCUT_INFO=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.duplicate_shortcuts.txt
# global function return values
typeset -i R_START_LINE
typeset -i R_END_LINE
typeset -i R_OPEN_TAG_END_LINE


touch $LOG_MSG_FILE

# fill cleanup script to delete temporary files
touch $CLEANUP_SCRIPT
chmod +x $CLEANUP_SCRIPT
echo "rm -f $QUERY_RESULT_FILE"                                     >> $CLEANUP_SCRIPT
echo "rm -f $TOTAL_EXPORT_FILE"                                     >> $CLEANUP_SCRIPT
echo "rm -f $GLOBAL_HEADER_FILE"                                    >> $CLEANUP_SCRIPT
echo "rm -f $DUPL_SHORTCUT_INFO"                                    >> $CLEANUP_SCRIPT

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

# no error checks required
ERROR_CODE=0
if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

echo ""
echo "###############################################################################"
echo "START $THIS_SCRIPT at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameters and variables used:"
echo ""
echo "Parameter -m (Module Name)   : $MODULE_NAME"
echo "Parameter -q (Infa Query )   : $INFA_QUERY_NAME"
echo "Parameter -p (Infa XML Path) : $SVN_INFA_XML_PATH"
echo "Parameter -e (Infa env)      : $REPOSITORY_NAME"
echo ""
echo "SVN repository base path     : $SVN_BASE_URL"
echo "Local build work area path   : $BUILD_WA_PATH"
echo "Local temp path              : $BUILD_TMP_PATH"
echo "Local logging path           : $BUILD_LOG_PATH"
echo "Final log file               : $FINAL_LOG_FILE"
echo "Temporary log message file   : $LOG_MSG_FILE"
echo ""


#---------------------------------------------------------------------------
# 4.) Check if value of optional parameter -m is a valid module name
#---------------------------------------------------------------------------

if [ "$MODULE_NAME" != "" ] ; then
    if [ $(cat $MODULENAME_FILE | grep "$MODULE_NAME" | wc -l) -eq 0 ] ; then
        echo "Module name $MODULE_NAME is not in reference list!"
        check_error 2
    fi
fi


#---------------------------------------------------------------------------
# 5.) Update work area with latest Informatica XML transformations from SVN
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Updating work area $STD_WA_PATH ..."
echo "###############################################################################"
echo ""

# For the Informatica export, the 'infa_xml' would be sufficient.
# If the work area is also used for other scripts (which is usually the case),
# the complete 'code' folder should be checked out to avoid conflicts
# with the next checkout/update/switch.

# get 'code' parent folder of 'infa_xml' - this is the folder to be checked out
SVN_INFA_XML_PATH_BASE=`dirname $SVN_INFA_XML_PATH`

td_updwa.sh \
            -w "$STD_WA_PATH" \
            -u "$SVN_INFA_XML_PATH_BASE" \
            -l "$LOG_FILE" \
            -r -rHEAD \
            -q
check_error $?
echo "... done"
echo ""


#---------------------------------------------------------------------------
# 6.) If Duplicate shortcut file does not exist, create it with header line
#---------------------------------------------------------------------------

DUPL_SHORTCUT_HEADER="Timestamp;Folder;ObjectType;RefObject;ShortcutName;DuplicateShortcutName"
if [ ! -e "$DUPL_SHORTCUT_FILE" ] ; then
    echo "$DUPL_SHORTCUT_HEADER" > "$DUPL_SHORTCUT_FILE"
    check_error $?
    $SVN add "$DUPL_SHORTCUT_FILE"
    check_error $?
fi
# temp info file will always be created
echo "$DUPL_SHORTCUT_HEADER" > "$DUPL_SHORTCUT_INFO"


#---------------------------------------------------------------------------
# 7.) Run query to find objects that should be exported
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Getting list of Informatica items to export..."
echo "###############################################################################"
echo ""
echo "Will run query $INFA_QUERY_NAME and write list to $QUERY_RESULT_FILE"
echo ""

# connect to Informatica repository
# echo "$PMREP connect -r $REPOSITORY_NAME -d $DOMAIN_NAME -n $REPOSITORY_USER_NAME -s $SECURITY_DOMAIN_NAME -X REPOSITORY_USER_PASSWORD"
$PMREP connect -r "$REPOSITORY_NAME" -d "$DOMAIN_NAME" -n "$REPOSITORY_USER_NAME" -s "$SECURITY_DOMAIN_NAME" -X REPOSITORY_USER_PASSWORD
check_error $?

# run query to determine changed Informatica objects and write results into file
# echo "$PMREP ExecuteQuery -q $INFA_QUERY_NAME -u $QUERY_RESULT_FILE"
$PMREP "ExecuteQuery" -q "$INFA_QUERY_NAME" -u "$QUERY_RESULT_FILE"
check_error $?


#---------------------------------------------------------------------------
# 8.) Export items found by query and split into separate xml files
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Exporting changed Informatica items ..."
echo "###############################################################################"
echo ""
echo "Run export using results from query $INFA_QUERY_NAME"
echo "Exporting to $TOTAL_EXPORT_FILE ..."
echo ""

$PMREP "ObjectExport" -i "$QUERY_RESULT_FILE" -b -u "$TOTAL_EXPORT_FILE"
check_error $?
echo "... done"
echo ""

echo "Removing cr characters in $TOTAL_EXPORT_FILE ..."
convert_doseol2unix $TOTAL_EXPORT_FILE
# tr -d '\r' <"$TOTAL_EXPORT_FILE.tmp" >"$TOTAL_EXPORT_FILE" && rm -f "$TOTAL_EXPORT_FILE.tmp"
echo "... done"
echo ""

echo "Resetting export date of exported xml file ..."
# reset export date in file to default date
# thus only real changes will be discovered by SVN and actually commited
reset_infa_export_date "$TOTAL_EXPORT_FILE"
echo "... done"
echo ""

# extract the header of the exported total xml file
# since the total xml is split into separate files, the header must be written to each file
write_xml_file_header "$TOTAL_EXPORT_FILE" "$GLOBAL_HEADER_FILE"
# echo "Global header file:"
# cat "$GLOBAL_HEADER_FILE"

# extract by folder from the total xml file into separate files
cat $QUERY_RESULT_FILE | cut -d "," -f2 | sort -u | while read OBJ_FOLDER
do
    # to avoid problems with the unix file system, replace spaces in names by underscore
    UNIX_OBJ_FOLDER="`echo "$OBJ_FOLDER" | tr ' ' '_'`"

    # write delete commands to cleanup after the script ist finished
    echo "rm -f $FOLDER_EXPORT_BASE.folder_header.$UNIX_OBJ_FOLDER"     >> $CLEANUP_SCRIPT
    echo "rm -f $FOLDER_EXPORT_BASE.$UNIX_OBJ_FOLDER"                   >> $CLEANUP_SCRIPT

    get_folder_start_line "$TOTAL_EXPORT_FILE" "$OBJ_FOLDER"
    # echo "Folder start line = $R_START_LINE"
    if [ "$R_START_LINE" -gt 0 ] ; then
        get_open_tag_end_line "$TOTAL_EXPORT_FILE" "$R_START_LINE"
        # echo "Folder open tag end line = $R_OPEN_TAG_END_LINE"
        if [ "$R_OPEN_TAG_END_LINE" -gt 0 ] ; then
            extract_file_part "$TOTAL_EXPORT_FILE" "$R_START_LINE" "$R_OPEN_TAG_END_LINE" "$FOLDER_EXPORT_BASE.folder_header.$UNIX_OBJ_FOLDER"
        fi
        get_object_end_line "$TOTAL_EXPORT_FILE" "folder" "$R_START_LINE"
        # echo "Folder end line = $R_END_LINE"
        if [ "$R_END_LINE" -gt 0 ] ; then
            extract_file_part "$TOTAL_EXPORT_FILE" "$R_START_LINE" "$R_END_LINE" "$FOLDER_EXPORT_BASE.$UNIX_OBJ_FOLDER"
        fi
    fi
done

# Take result file of pmrep query, read line by line, and get fields from each line.
# Then export each item into SVN work area, add relevant SVN properties and commit to SVN.
cat $QUERY_RESULT_FILE | while read qrline
do
    # echo $qrline
    OBJ_FOLDER=`echo $qrline | cut -d "," -f2`
    OBJ_NAME=`echo $qrline | cut -d "," -f3`
    OBJ_TYPE=`echo $qrline | cut -d "," -f4`
    OBJ_SUBTYPE=`echo $qrline | cut -d "," -f5`
    OBJ_VERSION=`echo $qrline | cut -d "," -f6`
    OBJ_SHRTCUT=`echo $qrline | cut -d "," -f7`

    # to avoid problems with the unix file system, replace spaces in names by underscore
    UNIX_OBJ_FOLDER="`echo "$OBJ_FOLDER" | tr ' ' '_'`"
    UNIX_OBJ_TYPE="`echo "$OBJ_TYPE" | tr ' ' '_'`"
    UNIX_OBJ_NAME="`echo "$OBJ_NAME" | tr ' ' '_'`"

    # there is an error in the persistent output file created by the query:
    # for mapplets, object_type is set to 'transformation' while object_subtype is set to 'mapplet'
    # this must be corrected manually
    if [ "$OBJ_TYPE" = "transformation" -a "$OBJ_SUBTYPE" = "mapplet" ] ; then
        OBJ_TYPE="mapplet"
        UNIX_OBJ_TYPE="mapplet"
        OBJ_SUBTYPE="none"
    fi

	# determine export folder within SVN work area for Informatica object
    EXPORT_PATH=$INFA_XML_AREA/$UNIX_OBJ_FOLDER/$UNIX_OBJ_TYPE
    EXPORT_PATH_SED=$( echo $EXPORT_PATH | sed -e 's/\//\\\//'g )
	# create non-existing export folder within SVN work area
    if [ ! -d "$EXPORT_PATH" ] ; then
        $SVN mkdir --parents "$EXPORT_PATH"
        check_error $?
    fi

	# determine export file name SVN work area for Informatica object
    EXPORT_TARGET_FILE=$EXPORT_PATH/$UNIX_OBJ_NAME.xml
    FOLDER_XML_FILE="$FOLDER_EXPORT_BASE.$UNIX_OBJ_FOLDER"

    # echo ""
    echo "Creating xml file $EXPORT_TARGET_FILE"
    # echo ""

    get_object_start_line "$FOLDER_XML_FILE" "$OBJ_TYPE" "$OBJ_NAME"
    # echo "Object start line = $R_START_LINE"
    if [ "$R_START_LINE" -gt 0 ] ; then
        if [ "$R_IS_SHORTCUT" -eq 0 ] ; then
            get_object_end_line "$FOLDER_XML_FILE" "$OBJ_TYPE" "$R_START_LINE"
        else
            R_END_LINE=$R_START_LINE
        fi
        # echo "Object end line = $R_END_LINE"
        if [ "$R_END_LINE" -gt 0 ] ; then
            # echo "writing to $EXPORT_TARGET_FILE"
            cat $GLOBAL_HEADER_FILE > $EXPORT_TARGET_FILE
            cat "$FOLDER_EXPORT_BASE.folder_header.$UNIX_OBJ_FOLDER" >> $EXPORT_TARGET_FILE
            extract_file_part "$FOLDER_XML_FILE" "$R_START_LINE" "$R_END_LINE" "$EXPORT_TARGET_FILE"
            write_folder_footer "$EXPORT_TARGET_FILE"
            write_xml_file_footer "$EXPORT_TARGET_FILE"
        else
            echo "Cannot find object $OBJ_NAME in temp file $FOLDER_XML_FILE"
        fi
    else
        echo "Cannot find object $OBJ_NAME in temp file $FOLDER_XML_FILE"
    fi
    # echo "Start position : $R_START_LINE"
    # echo "End position   : $R_END_LINE"

    if [ "$OBJ_TYPE" = "workflow" -o "$OBJ_TYPE" = "worklet" ] ; then
        add_query_banding_info "$EXPORT_TARGET_FILE"
    fi

	# check if export file is already under SVN control - if not then add
    if [ "`$SVN status $EXPORT_TARGET_FILE | cut -c1`" = "?" ] ; then
        # echo "Adding file to Subversion"
        # echo ""
        $SVN add "$EXPORT_TARGET_FILE"
        # echo ""
    fi
    
    # set SVN properties derived from query result
    # echo "Setting Subversion properties"
    $SVN propset --quiet "infa:folder" "$OBJ_FOLDER" "$EXPORT_TARGET_FILE"
    $SVN propset --quiet "infa:type" "$OBJ_TYPE" "$EXPORT_TARGET_FILE"
    $SVN propset --quiet "infa:subtype" "$OBJ_SUBTYPE" "$EXPORT_TARGET_FILE"
    $SVN propset --quiet "infa:version" "$OBJ_VERSION" "$EXPORT_TARGET_FILE"

#    if [ $(echo "$OBJ_SHRTCUT\c" | wc -c) -gt 0 ] ; then
    f_object_is_shortcut $EXPORT_TARGET_FILE
    if [ $? -gt 0 ] ; then
        # set infa:shortcut property only if it exists
        $SVN propset --quiet "infa:shortcut" "yes" "$EXPORT_TARGET_FILE"

        # count number of xml files in directory
        # if there is just 1, then it's not worth checking (there can't be duplicates anyway)
        # also, with only 1 file, grep will not output the filename and thus the algorithm does not work
        if [ $( ls -l $EXPORT_PATH/*.xml | wc -l ) -gt 1 ] ; then

            # check if object referenced by shortcut is already referenced by another shortcut xml file in work area
            # echo "Checking if shortcut has a duplicate ..."

            # fill variables R_INFA_REFOBJECT_NAME and R_INFA_REFOBJECT_DBD (source only) with name of object referenced by shortcut
            get_infa_refobject_name "$EXPORT_TARGET_FILE" "$OBJ_TYPE"

            # object type source needs special handling, since it contains object name & dbd name
            if [ "$OBJ_TYPE" = "source" ] ; then
                DUPL_SHORTCUT_NAME=""
                # 1. get all files that reference the same dbd and source name
                # 2. the file currently examined must of course be filtered out
                # 3. the grep provides the files's name and path - use 'cut' to get it
                # 4. remove the path to get only the file name itself

                DUPL_SHORTCUT_NAME=`grep "<SHORTCUT\(.*\)REFERENCEDDBD =\"$R_INFA_REFOBJECT_DBD\"\(.*\)REFOBJECTNAME =\"$R_INFA_REFOBJECT_NAME\"" $EXPORT_PATH/*.xml \
                  | grep -v "^$EXPORT_PATH_SED\/$OBJ_NAME.xml:" \
                  | cut -f 1 -d ":" \
                  | sed -e "s/$EXPORT_PATH_SED\/\(.*\)\.xml/\1/"`
                if [ \( -n "$DUPL_SHORTCUT_NAME" \) -a \("$OBJ_NAME" != "$DUPL_SHORTCUT_NAME"\) ] ; then
                    # echo "$OBJ_NAME"
                    # echo "$DUPL_SHORTCUT_NAME"
                    DUPL_SHORTCUT_LINE="$SCRIPT_START_DATE;${OBJ_FOLDER};${OBJ_TYPE};${R_INFA_REFOBJECT_NAME};${OBJ_NAME};${DUPL_SHORTCUT_NAME}"
                    echo $DUPL_SHORTCUT_LINE >> $DUPL_SHORTCUT_FILE
                    echo $DUPL_SHORTCUT_LINE >> $DUPL_SHORTCUT_INFO
                fi

            else
                DUPL_SHORTCUT_NAME=""
                # 1. get all files that reference the same object name
                # 2. the file currently examined must of course be filtered out
                # 3. the grep provides the files's name and path - use 'cut' to get it
                # 4. remove the path to get only the file name itself
                DUPL_SHORTCUT_NAME=`grep "<SHORTCUT\(.*\)REFOBJECTNAME =\"$R_INFA_REFOBJECT_NAME\"" $EXPORT_PATH/*.xml \
                  | grep -v "^$EXPORT_PATH_SED\/$OBJ_NAME.xml:" \
                  | cut -f 1 -d ":" \
                  | sed -e "s/$EXPORT_PATH_SED\/\(.*\)\.xml/\1/"`
                if [ \( -n "$DUPL_SHORTCUT_NAME" \) -a \("$OBJ_NAME" != "$DUPL_SHORTCUT_NAME"\) ] ; then
                    # echo "$OBJ_NAME"
                    # echo "$DUPL_SHORTCUT_NAME"
                    DUPL_SHORTCUT_LINE="$SCRIPT_START_DATE;${OBJ_FOLDER};${OBJ_TYPE};${R_INFA_REFOBJECT_NAME};${OBJ_NAME};${DUPL_SHORTCUT_NAME}"
                    echo $DUPL_SHORTCUT_LINE >> $DUPL_SHORTCUT_FILE
                    echo $DUPL_SHORTCUT_LINE >> $DUPL_SHORTCUT_INFO
                fi
            fi
        fi
    fi
    # if optional module parameter is set, provide it as default module name
    if [ $(echo "$MODULE_NAME\c" | wc -c) -gt 0 ] ; then
        #
        # force module name to global_shared for all objects in folder global_shared
        # to avoid that every export changes the module name for shared objects used from folder global_shared
        #
        if [ "$OBJ_FOLDER" = "global_shared" ] ; then
            $SVN propset --quiet "td:module" "global_shared" "$EXPORT_TARGET_FILE"
        else
            $SVN propset --quiet "td:module" "$MODULE_NAME" "$EXPORT_TARGET_FILE"
        fi
    fi

done
echo "... done."

# disconnect from Informatica repository
$PMREP cleanup
check_error $?


#---------------------------------------------------------------------------
# 9.) Send XML files to Subversion
#---------------------------------------------------------------------------


echo ""
echo "###############################################################################"
echo "Committing changes to Subversion ..."
echo "###############################################################################"
echo ""
echo "Exported changed Informatica objects to development trunk" > $LOG_MSG_FILE
check_error $?
cat $LOG_MSG_FILE

$SVN commit $INFA_XML_AREA --file $LOG_MSG_FILE 2>&1
check_error $?
echo "... done."

#---------------------------------------------------------------------------
# 10.) Display duplicate shortcut references
#---------------------------------------------------------------------------

# check if duplicate shortcut info file contains more than just the header line
if [ $( cat "$DUPL_SHORTCUT_INFO" | wc -l ) -gt 1 ] ; then
    echo ""
    echo "###############################################################################"
    echo "Please be aware that the following files have duplicate shortcut references!"
    echo "This should be fixed ASAP!"
    echo "###############################################################################"
    echo ""
    cat $DUPL_SHORTCUT_INFO
    echo ""
fi


#---------------------------------------------------------------------------
# FINISH
#---------------------------------------------------------------------------

# run cleanup script to delete temporary files
# for debugging purpose, just comment the next line
. $CLEANUP_SCRIPT
rm -f $CLEANUP_SCRIPT

exit 0
