#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: 08_create_rel.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/08_create_rel.sh $
#---------------------------------------------------------------------------
# SVN Info END
#
# --------------------------------------------------------------------------
# Purpose     : generate new production release
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2011-03-30 S.Sutter       Initial version 
# 2011-03-30 S.Sutter       Initial version 
# 2012-06-30 S.Sutter       Switch to getopts parameter handling; add rename of build result files
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# generate release tag from latest IT build
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -t        Release type        yes         yes         possible values: 'major', 'minor', 'newpatch' (patch itself is a reserved word)
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
# 2.) Initialize other variables
# 3.) Check for correct syntax
# 4.) Get previous and new release number
# 5.) Use new release number to determine temp & log file names
# 6.) Check if there are any changes at all. If not then exit.
# 7.) Create new release branch
# 8.) Generate diff between old and new release. Write result to temp file
# 
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------


#---------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh


#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo ""
    echo "Usage: `basename $0` -t <release type> [-h]"
    echo "Create new release branch from latest integration test tag."
    echo "       -t <release type> : Type of release. possible values:"
    echo "                                  - major"
    echo "                                  - minor"
    echo "                                  - newpatch (patch itself is a reserved word)"
    echo "       [-h]              : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$TARGET_RELEASE.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialization
#---------------------------------------------------------------------------

# get parameters
typeset -l RELEASE_TYPE

USAGE="t:h"
while getopts "$USAGE" opt; do
    case $opt in
        t  ) RELEASE_TYPE="$OPTARG"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))



# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo "###############################################################################"
echo "START ${THIS_SCRIPT}.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Variables used:"
echo "###############################################################################"
echo ""
echo "SVN repository URL    : $SVN_BASE_URL"
echo "Local build path      : $BUILD_WA_PATH"
echo "Local temp path       : $BUILD_TMP_PATH"
echo "Local logging path    : $BUILD_LOG_PATH"
echo ""


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

if [ -z "$RELEASE_TYPE" ] ; then
    echo "No release type provided! Exiting ..."
    echo ""
    ERROR_CODE=1
else
    case "$RELEASE_TYPE" in
        major|minor|newpatch)
            ;;
        *)
            echo "Invalid value for relase type parameter!!!"
            echo "Please see help syntax for possible values. Exiting ..."
            echo ""
            ERROR_CODE=2
        ;;
    esac
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


#---------------------------------------------------------------------------
# 4.) Check if an uncommitted release branch already exists
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Check if an uncommitted release branch already exists"
echo "###############################################################################"
echo ""

REL_CNT=0
REL_CNT=`$SVN list $SVN_BASE_URL/branches/rel | grep -v stage | wc -l`
if [ $REL_CNT -ne 0 ] ; then
    echo ""
    echo "An untagged release branch already exists in branches/rel!"
    echo "Please archive/tag this branch before continuing"
    echo "... exiting."
    echo ""
    check_error 2
fi

echo "OK"
echo "... done"
echo ""


#---------------------------------------------------------------------------
# 4.) Get previous release, determine new release no and define log file names
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get previous release, determine new release no and define log file names"
echo "###############################################################################"
echo ""

# check SVN repository for release directories and get the latest release
PREV_REL_TAG=`$SVN list $SVN_BASE_URL/tags/rel | grep "^rel-" | sort | tail -1 | sed -e "s/\/$//g"`
PREV_RELEASE_NO=`echo $PREV_REL_TAG | cut -c 5-23`
PREV_RELEASE_URL=$SVN_BASE_URL/tags/rel/$PREV_REL_TAG

if [ -z "$PREV_RELEASE_NO" ] ; then
    IS_FIRST_RELEASE="true"
    generate_new_release_number "initrel"
else
    generate_new_release_number "$RELEASE_TYPE" "$PREV_RELEASE_NO"
fi

TARGET_RELEASE=$NEW_RELEASE_NUMBER
TARGET_RELEASE_TAG="rel-$TARGET_RELEASE.$SCRIPT_START_DATE"
TARGET_RELEASE_URL="$SVN_BASE_URL/branches/rel/$TARGET_RELEASE_TAG"
COPY_TARGET_URL=$TARGET_RELEASE_URL

if [ -z "$TARGET_RELEASE" ] ; then
    echo "Cannot determine new target release!"
    echo "Exiting ..."
    exit 1
fi

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$TARGET_RELEASE.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$TARGET_RELEASE.$SCRIPT_START_DATE.log_msg.txt
CHANGE_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$TARGET_RELEASE.$SCRIPT_START_DATE.changes.txt
BUILD_RESULT_BASE_DIR=$BUILD_TMP_PATH/$THIS_SCRIPT.$TARGET_RELEASE.$SCRIPT_START_DATE.build_tmp


echo ""
echo "Previous release no   : $PREV_RELEASE_NO"
echo "Previous release tag  : $PREV_REL_TAG"
echo "Previous release URL  : $PREV_RELEASE_URL"
echo "New release no        : $TARGET_RELEASE"
echo ""
echo "Final log file        : $FINAL_LOG_FILE"
echo "Temp log message file : $LOG_MSG_FILE"
echo "List of changed files : $CHANGE_FILE"
echo ""


#---------------------------------------------------------------------------
# 4.) Get latest integration test build
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get latest integration test build"
echo "###############################################################################"
echo ""

# check SVN repository for existing integration test build tags
INT_TAG_CNT=`$SVN list $SVN_BASE_URL/tags/int | grep ^int-build | wc -l`
check_error $?

if [ $INT_TAG_CNT -eq 0 ] ; then
    echo "There is no INT tag that could be used for a release!"
    echo "... exiting."
    exit 0
else
    # check SVN repository for latest integration test tag 
    INT_BUILD_TAG=`$SVN list $SVN_BASE_URL/tags/int | grep ^int-build | sort | tail -1 | sed -e "s/\/$//g"`
    check_error $?

    INT_BUILD_NO=`echo $INT_BUILD_TAG | cut -c 11-16`
    check_error $?

    COPY_SOURCE_URL=$SVN_BASE_URL/tags/int/$INT_BUILD_TAG

    echo "INT build number       : $INT_BUILD_NO"
    echo "INT build tag          : $INT_BUILD_TAG"
    echo "INT build (source) URL : $COPY_SOURCE_URL"
fi

echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 6.) Check if there are any changes requiring a new release
#---------------------------------------------------------------------------

# this check is only required if a previous release already exists
# otherwise, everything from the INT build will be used anyway
if [ ! "$IS_FIRST_RELEASE" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Check if there are any changes requiring a new release ..."
    echo "###############################################################################"
    echo ""

    # echo "performing diff ..."
    CHANGED_FILE_CNT=`$SVN diff --summarize ${PREV_RELEASE_URL}/code ${COPY_SOURCE_URL}/code | wc -l`

    if [ $CHANGED_FILE_CNT -eq 0 ] ; then
        echo ""
        echo "No changes in integration build area since previous release $PREV_BUILD_NO was created!"
        echo "... exiting."
        exit 0
    else
        echo "... OK. Will create a new release."
    fi
    echo ""
fi
f_write_date
echo ""


#---------------------------------------------------------------------------
# 7.) Create new release branch
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Create new release branch"
echo "###############################################################################"
echo ""

# set SVN log message
echo "Creating new release $TARGET_RELEASE from integration build $INT_BUILD_TAG" > $LOG_MSG_FILE
cat $LOG_MSG_FILE

# display copy parameters for debugging purpose
echo "COPY_SOURCE_URL       : $COPY_SOURCE_URL"
echo "COPY_TARGET_URL       : $COPY_TARGET_URL"

# perform the copy
$SVN copy $COPY_SOURCE_URL $COPY_TARGET_URL --file $LOG_MSG_FILE
check_error $?

echo ""
echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 8.) Generate diff between old and new release. Write result to change file
#---------------------------------------------------------------------------

if [ ! "$IS_FIRST_RELEASE" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Generate diff between old and new release. Write result to change file."
    echo "###############################################################################"
    echo ""
    
    # get difference between previous and current release
    # diff result contains complete URL path which is not relevant
    # thus get rid of it before writing to file
    
    # URL of prev rel release
    # change all "\" from path to "/" (relevant for windows systems)
    MOD_URL=$(echo $PREV_RELEASE_URL | sed -e 's/\//\\\//'g )
    # echo "modified PREV_RELEASE_URL : $MOD_URL"

    # perform diff, filter URL path, write to file, 
    echo "performing diff between $PREV_RELEASE_URL"
    echo "    and $COPY_TARGET_URL ..."
    $SVN diff --summarize ${PREV_RELEASE_URL}/code ${COPY_SOURCE_URL}/code | sort | sed -e "s/$MOD_URL//g" > $CHANGE_FILE
    check_error $?

    echo ""
    echo "Changes between current release $TARGET_RELEASE and previous release $PREV_RELEASE_NO"
    echo "   can be found in $CHANGE_FILE"
    echo ""
    f_write_date
    echo ""

fi

#---------------------------------------------------------------------------
# 9.) Rename build result files created during IT build creation
#     At build creation time the release number was not yet known
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Rename build result files created during IT build creation"
echo "###############################################################################"
echo ""

echo "Checking out build result work area $BUILD_RESULT_BASE_DIR"
echo ""
mkdir $BUILD_RESULT_BASE_DIR

td_updwa.sh \
    -w "$BUILD_RESULT_BASE_DIR" \
    -u "${COPY_TARGET_URL}/build" \
    -l "$LOG_FILE" \
    -r "-rHEAD" \
    -q

echo ""
f_write_date
echo ""

echo "Get build result files and rename *.next.* part to *.${TARGET_RELEASE}.*"
echo ""
$SVN list -R "$BUILD_RESULT_BASE_DIR" | grep "\.next\." | while read NEXTFILE
do
    RENAME_FILE=`echo "$NEXTFILE" | sed -e "s/\.next\./\.${TARGET_RELEASE}\./g"`
    # echo "Renaming $NEXTFILE to $RENAME_FILE"
    $SVN rename -q ${BUILD_RESULT_BASE_DIR}/${NEXTFILE} ${BUILD_RESULT_BASE_DIR}/${RENAME_FILE}
    check_error $?
done

# file current_releasenumber.txt can now also be changed to contain the actual release number
echo "Write release number to results/filelists/current_releasenumber.txt"
echo ""
echo "${TARGET_RELEASE}" > $BUILD_RESULT_BASE_DIR/results/filelists/current_releasenumber.txt
if [ `$SVN status $BUILD_RESULT_BASE_DIR/results/filelists/current_releasenumber.txt | cut -c1` == "?" ] ; then
    $SVN add $BUILD_RESULT_BASE_DIR/results/filelists/current_releasenumber.txt
    check_error $?
fi

# set SVN log message and commit
echo ""
echo "Rename build result files created during IT build creation" > $LOG_MSG_FILE
cat $LOG_MSG_FILE
echo ""
$SVN commit $BUILD_RESULT_BASE_DIR --file $LOG_MSG_FILE
check_error $?
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 8.) Cleanup & finish
#---------------------------------------------------------------------------

rm -rf $BUILD_RESULT_BASE_DIR

exit 0
        