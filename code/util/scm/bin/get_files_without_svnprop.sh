#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: get_files_without_svnprop.sh 4637 2012-08-09 00:17:43Z k9108499 $
# Last Changed By  : $Author: k9108499 $
# Last Change Date : $Date: 2012-08-09 02:17:43 +0200 (tor, 09 aug 2012) $
# Last Revision    : $Revision: 4637 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/get_files_without_svnprop.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : Get all files within a certain path that have the specified Subversion property not set
# Project     : Axfood
# Subproject  : all
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2008-12-01 S.Sutter       Initial version
# 2009-04-07 T.Bachmann     Parameter assignment changed
# 2011-03-08 S.Sutter       Adapt to Axfood specifics
# 2012-07-31 S.Sutter       Switch to getopts parameter handling 
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# Get all files within a certain path that have the specified Subversion property not set
#
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -p        Property path       yes         no          SVN path to search; $SVN_BASE_URL by default
# -u        SVN URL             yes         no          Subversion main URL containing the directory to search; "trunk/code" by default
# -n        Property name       yes         no          Name of SVN property to search for; "td:module" by default
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#   
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialization
# 2.) Get files that don't have the SVN property set
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialization
#---------------------------------------------------------------------------

. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-u <url>] [-p <proppath>] [-n <propname>] [-h]"
    echo "Get files that don't have a specific Subversion property set"
    echo "       [-u <url>]      : Subversion base URL containing the code to search."
    echo "                         If omitted, use default value SVN_BASE_URL from the .scm_config file:"
    echo "                         ($SVN_BASE_URL)"
    echo "       [-p <proppath>] : Path of code to search within the SVN repository"
    echo "                         If omitted, use default value $PROP_PATH"
    echo "       [-n <propname>] : Name of SVN property to search."
    echo "                         If omitted, use default value $PROP_NAME."
    echo "       [-h]            : Print script syntax help"
    echo ""
    }


# default values that can be overwritten by parameters
SVN_URL=$SVN_BASE_URL
PROP_PATH="trunk/code"
PROP_NAME="td:module"

# get parameters
USAGE="u:p:n:h"
while getopts "$USAGE" opt; do
    case $opt in
        u  ) SVN_URL="$OPTARG"
             ;;
        p  ) PROP_PATH="$OPTARG"
             ;;
        n  ) PROP_NAME="$OPTARG"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# --------------------------------------------------------------------------
# 2.) Get files that don't have the SVN property set
# --------------------------------------------------------------------------

# get all files from SVN path (no matter which SVN property is set), and write result to temporary file
$SVN list --depth infinity $SVN_URL/$PROP_PATH | grep -v /$ | sort > all.txt

# Build sed search string
SRC_PATH_SED=$( echo $SVN_URL/$PROP_PATH | sed -e 's/\//\\\//'g )

# get all files with specified SVN property from SVN path, write result to temporary file
$SVN propget -R $PROP_NAME $SVN_URL/$PROP_PATH | sed -e "s/$SRC_PATH_SED\/\(.*\) - .*/\1/" | sort > prop.txt

# compare temporary files - get entries contained in all.txt, but not in prop.txt
# those are the entries with the specified SVN property NOT set
diff all.txt prop.txt | grep "^<" | cut -c 3- 

# remove temporary files
rm all.txt prop.txt

