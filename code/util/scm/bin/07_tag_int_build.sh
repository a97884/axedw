#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: 07_tag_int_build.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author:  
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/07_tag_int_build.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : generate new integration test build branch
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2009-08-13 S.Sutter       Initial version 
# 2011-03-08 S.Sutter       Adapt to Axfood specifics
# 2012-06-30 S.Sutter       Switch to getopts parameter handling
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# generate new integration test build branch
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
# 2.) Initialize other variables
# 3.) Check for correct syntax
# 4.) Check if there is a integration test branch for tagging 
# 5.) Get build branch and determine file names 
# 6.) Move integration test build from branches to tags 
# 
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------


#---------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

function print_help
    {
    echo "Usage: ${THIS_SCRIPT}.sh  [-h]"
    echo "Tag integration build branch"
    echo "       [-h]   : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialize other variables
#---------------------------------------------------------------------------

# get parameters
USAGE="h"
while getopts "$USAGE" opt; do
    case $opt in
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Variables used:"
echo "###############################################################################"
echo ""
echo "SVN repository URL    : $SVN_BASE_URL"
echo "Local temp path       : $BUILD_TMP_PATH"
echo "Local logging path    : $BUILD_LOG_PATH"
echo ""


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

# this function is currently not relevant, since there are no parameters
ERROR_CODE=0
if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


#--------------------------------------------------------------------------- 
# 4.) Check if there is a integration test branch for tagging 
#--------------------------------------------------------------------------- 

echo ""
echo "###############################################################################"
echo "# Checking for existing system test branch"
echo "###############################################################################"
echo ""

UNTAGGED_CNT=`$SVN list $SVN_BASE_URL/branches/int | grep ^int-build | wc -l` 
if [ $UNTAGGED_CNT -eq 0 ] ; then 
    echo "There is no integration test branch that could be tagged!" 
    echo "... exiting." 
    exit 0 
elif [ $UNTAGGED_CNT -eq 1 ] ; then 
    echo "Found one integration test branch." 
    $SVN list $SVN_BASE_URL/branches/int | grep ^int-build
else 
    echo "Found more than one integration test branch!" 
    list $SVN_BASE_URL/branches/int | grep ^int-build
    echo "Clean up before restarting the script!" 
    echo "... exiting." 
    exit 0 
fi 
echo ""
f_write_date
echo ""


#--------------------------------------------------------------------------- 
# 5.) Get build branch and determine file names 
#--------------------------------------------------------------------------- 

echo ""
echo "###############################################################################"
echo "# Get build branch and determine file names"
echo "###############################################################################"
echo ""

# Get system test branch and build number 
INT_BUILD_BRANCH=`$SVN list $SVN_BASE_URL/branches/int | grep ^int-build | sort | tail -1 | sed -e "s/\/$//g"`
INT_BUILD_NO=`echo $INT_BUILD_BRANCH | cut -c 11-16` 

echo "Build branch          : $INT_BUILD_BRANCH" 
echo "Build number          : $INT_BUILD_NO" 

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$INT_BUILD_NO.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$INT_BUILD_NO.$SCRIPT_START_DATE.log_msg.txt 

echo "Final log file        : $FINAL_LOG_FILE" 
echo "Temp log message file : $LOG_MSG_FILE" 
echo "" 
f_write_date
echo ""


#--------------------------------------------------------------------------- 
# 6.) Move integration test build from branches to tags 
#--------------------------------------------------------------------------- 

echo ""
echo "###############################################################################"
echo "# Move integration test build from branches to tags"
echo "###############################################################################"
echo ""

MOVE_SOURCE_URL=$SVN_BASE_URL/branches/int/$INT_BUILD_BRANCH 
MOVE_TARGET_URL=$SVN_BASE_URL/tags/int 

echo "Source URL            : $MOVE_SOURCE_URL" 
echo "Target URL            : $MOVE_TARGET_URL" 

# set SVN log message and do some logging 
echo "Moving integration test branch $INT_BUILD_BRANCH from branches/int to tags/int" > $LOG_MSG_FILE
cat $LOG_MSG_FILE

# perform the actual SVN move
$SVN move $MOVE_SOURCE_URL $MOVE_TARGET_URL --file $LOG_MSG_FILE
check_error $? 
echo "... done." 
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
