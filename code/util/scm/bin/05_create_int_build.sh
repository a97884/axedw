    #!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: 05_create_int_build.sh 29510 2019-11-18 15:57:08Z  $
# Last Changed By  : $Author:  
# Last Change Date : $Date: 2019-11-18 16:57:08 +0100 (mån, 18 nov 2019) $
# Last Revision    : $Revision: 29510 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/05_create_int_build.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : generate new integration test build branch
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2009-08-13 S.Sutter       Initial version 
# 2011-03-08 S.Sutter       Adapt to Axfood specifics
# 2012-06-30 S.Sutter       Switch to getopts parameter handling;
#                           make creation of build results a separate generalized script
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# generate new integration test build branch
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -f        fast mode           no          no          omit security checks (no consistency check of runorder text files)
# -c        check DDL files     no          no          check if DDL are in runorder text files - only relevant if fast mode is NOT active
# -d        delta mode          no          no          determine delta between previous and new integration build (default is previous release)
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
#  1.) initialize variables from ini file & function definitions
#  2.) Initialize other variables
#  3.) Check for correct syntax
#  4.) Get previous and new build number
#  5.) Checking for previous release
#  6.) Use new build number to determine temp & log file names
#  7.) Check if the last build has already been archived. Otherwise exit. 
#  8.) Check if there are any changes at all. If not then exit.
#  9.) Check runorder file consistency
# 10.) Create new integration test branch
# 11.) Create build results
# 12.) Create packages
# 13.) Cleanup & finish
# 
# --------------------------------------------------------------------------
# Open points:
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------


#---------------------------------------------------------------------------
#  1.) initialize variables from ini file & function definitions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-f] [-h]"
    echo "Creates new integration test build tag from integration test staging branch."
    echo "       [-f]          : Fast mode - omit security checks:"
    echo "                       - no consistency check of runorder text files"
    echo "       [-c]          : Check if DDL are in runorder text files"
    echo "                       - only relevant if fast mode is not active."
    echo "       [-d]          : Delta mode - determine delta between previous and new integration build."
    echo "                       If ommitted, determine delta between previous release and new integration build"
    echo "                       If ommitted, the build will always be created no matter if there were"
    echo "                         changes between previous and new integration build or not."
    echo "       [-h]          : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        rm -rf $RUNORDER_TMP_PATH
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
#  Filter out directories with children from packaging list
#---------------------------------------------------------------------------

filter_package_list ()
    {
    typeset file2scan=$1
    typeset url2test
    typeset name2scan
    typeset is_dir
    typeset has_child
    
    
    # echo "Scanning $file2scan for directories with children"
    # echo "that should thus not be in the tar package input list"
    touch $file2scan.found
    cat $file2scan | while read line2scan
    do
        # scan, if object is directory
        url2test=$COPY_TARGET_URL/`dirname $line2scan`
        # directories have trailing "/"
        name2scan=`basename $line2scan | sed -e 's/$/\//'`
        is_dir=`$SVN list $url2test | grep $name2scan | wc -l`
        # object is file
        if  [ $is_dir -eq 0 ] ; then
            # echo "        FOUND. $line2scan is regular file."
            echo $line2scan >> $file2scan.found
        # object is directory
        else
            # echo "    $line2scan is directory. Doing further checks"
            SEARCH_STR=$COPY_TARGET_URL/$line2scan
            # do any children exist?
            has_child=`$SVN list $SEARCH_STR | wc -l`
            # no children -> keep
            if  [ $has_child -eq 0 ] ; then
                # echo  "        FOUND. Directory $line2scan has no children. Keeping." >> $LOGFILE
                echo $line2scan >> $file2scan.found
            # has children -> ignore
            # else
                # echo "    Directory $line2scan has children. Filtering out." >> $LOGFILE
            fi
        fi
    done
    mv $file2scan.found $file2scan
    # echo "... done."
    }



#---------------------------------------------------------------------------
#  2.) Initialize other variables
#---------------------------------------------------------------------------

# get parameters
FASTMODE_FLAG="false"
DDLRUNORDER_FLAG="false"
DELTA_FLAG="false"

USAGE="fcdh"
while getopts "$USAGE" opt; do
    case $opt in
        f  ) FASTMODE_FLAG="true"
             ;;
        c  ) DDLRUNORDER_FLAG="true"
             ;;
        d  ) DELTA_FLAG="true"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

typeset -i PREV_CNT

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo ""
echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -f (Fast Mode)  : $FASTMODE_FLAG"
echo "Parameter -c (DDL Check)  : $DDLRUNORDER_FLAG"
echo "Parameter -d (Delta Mode) : $DELTA_LAG"
echo ""
echo "SVN repository URL        : $SVN_BASE_URL"
echo "Local temp path           : $BUILD_TMP_PATH"
echo "Local logging path        : $BUILD_LOG_PATH"
echo ""


#---------------------------------------------------------------------------
#  3.) Check for correct syntax
#---------------------------------------------------------------------------

# this function is currently not relevant
ERROR_CODE=0
if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

#---------------------------------------------------------------------------
#  4.) Get previous and new build number
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get previous and new build number"
echo "###############################################################################"
echo ""

PREV_CNT=`$SVN list $SVN_BASE_URL/tags/int | grep "^int-build" | sort | tail -1 | sed -e "s/\/$//g" | wc -l`
# if this is the first build no previous value can exist
if  [ $PREV_CNT -eq 0 ] ; then
    echo "No previous build tag exists. Defaulting to 0."
    PREV_BUILD_NO=0
else
    PREV_BUILD_TAG=`$SVN list $SVN_BASE_URL/tags/int | grep "^int-build" | sort | tail -1 | sed -e "s/\/$//g"`
    PREV_BUILD_NO=`echo $PREV_BUILD_TAG | cut -c 11-16`
    echo "Previous build number     : $PREV_BUILD_NO"
    echo "Previous build tag        : $PREV_BUILD_TAG"
fi

NEW_BUILD_NO=`expr $PREV_BUILD_NO + 1 | awk '{printf "%06d", $1}'`
NEW_INT_BRANCH=branches/int/int-build.$NEW_BUILD_NO.$SCRIPT_START_DATE

echo "New build number          : $NEW_BUILD_NO"
echo "New build branch          : $NEW_INT_BRANCH"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
#  5.) Checking for previous release
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Check for previous release tag"
echo "###############################################################################"
echo ""

# Check release tag directory for latest release
PREV_REL_CNT=`$SVN list $SVN_BASE_URL/tags/rel | sort | tail -1 | sed -e "s/\/$//g" | wc -l`
check_error $?
# if this is the first release no previous value can exist
if  [ $PREV_REL_CNT -eq 0 ] ; then
    echo "No previous release tag exists. Defaulting to 2012R01.000.000.000"
    PREV_REL_NO="2012R01.000.000.000"
    PREV_REL_TAG="tags/rel/rel-$PREV_REL_NO"
else
    PREV_REL_NAME=`$SVN list $SVN_BASE_URL/tags/rel | grep 'rel-2[0-9][0-9][0-9]R[0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]' | sort | tail -1 | sed -e "s/\/$//g"`
    PREV_REL_NO=`echo $PREV_REL_NAME | cut -c 5-23`
    PREV_REL_TAG=tags/rel/$PREV_REL_NAME
fi

PREV_REL_URL=$SVN_BASE_URL/$PREV_REL_TAG

echo "Previous release no       : $PREV_REL_NO"
echo "Previous release tag      : $PREV_REL_TAG"
echo "Previous release URL      : $PREV_REL_URL"
echo ""


#---------------------------------------------------------------------------
#  6.) Use new build number to determine temp & log file names
#---------------------------------------------------------------------------

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$NEW_BUILD_NO.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$NEW_BUILD_NO.$SCRIPT_START_DATE.log_msg.txt

echo "Final log file            : $FINAL_LOG_FILE"
echo "Temp log message file     : $LOG_MSG_FILE"
echo ""
f_write_date
echo ""


#--------------------------------------------------------------------------- 
#  7.) Check if the last build has already been archived. Otherwise exit. 
#--------------------------------------------------------------------------- 

echo ""
echo "###############################################################################"
echo "# Checking if last branch was properly tagged ..."
echo "###############################################################################"
echo ""

# search for an integration build branch that was created but not yet moved to tags
UNTAGGED_CNT=`$SVN list $SVN_BASE_URL/branches/int | grep ^int-build.$NEW_BUILD_NO | wc -l` 
if [ $UNTAGGED_CNT -ne 0 ] ; then 
    echo "Branch for build number $NEW_BUILD_NO already exists!!!" 
    echo "Branch name is " 
    $SVN list $SVN_BASE_URL/branches/int | grep ^int-build.$NEW_BUILD_NO
    echo "Either delete that branch manually or tag (and thus move) it by using 07_tag_int_build.sh!" 
    echo "... exiting." 
    exit 0 
else
    echo "... OK"
    echo ""
fi 
f_write_date
echo ""


#---------------------------------------------------------------------------
#  8.) Check if there are any changes at all. If not then exit.
#---------------------------------------------------------------------------

PREV_BUILD_URL=$SVN_BASE_URL/tags/int/$PREV_BUILD_TAG
COPY_SOURCE_URL=$SVN_BASE_URL/branches/int/stage
echo "PREV_BUILD_URL            : $PREV_BUILD_URL"
echo "STAGE_URL                 : $COPY_SOURCE_URL"

if [ "$DELTA_FLAG" == "true" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Check if there are any changes at all requiring a new build ..."
    echo "###############################################################################"
    echo ""


    # this step is only necessary, if a previous build exists.
    # otherwise it will just take everything from the staging area.
    if  [ $PREV_CNT -ne 0 ] ; then
         echo "performing diff ..."
        # perform diff, and check if there is at least one row that differs
        # diff only within code directory, since other directories were created by the build itself
        CHANGED_FILE_CNT=`$SVN diff --summarize --ignore-properties $PREV_BUILD_URL/code $COPY_SOURCE_URL/code | wc -l`
        if [ $CHANGED_FILE_CNT -eq 0 ] ; then
            echo ""
            echo "No changes in staging area since previous build $PREV_BUILD_NO ($PREV_BUILD_TAG) was created!"
            echo "... exiting."
            exit 0
        else
            echo "... OK. Will create a new build."
            echo ""
        fi
    fi
    f_write_date
    echo ""

fi

#---------------------------------------------------------------------------
#  9.) Check runorder file consistency
#---------------------------------------------------------------------------

if [ "$FASTMODE_FLAG" == "false" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Checking if all entries applying DB changes exist as listed in runorder files"
    echo "###############################################################################"
    echo ""

    RUNORDER_INCONSISTENT_FLAG=0
    RUNORDER_TMP_PATH=$BUILD_TMP_PATH/$THIS_SCRIPT.$NEW_BUILD_NO.$SCRIPT_START_DATE.runorder_check
    mkdir ${RUNORDER_TMP_PATH}
    mkdir ${RUNORDER_TMP_PATH}/code
    mkdir ${RUNORDER_TMP_PATH}/code/dbadmin
    echo "Checking out ${COPY_SOURCE_URL}/code/dbadmin ..."
    $SVN checkout --quiet "${COPY_SOURCE_URL}/code/dbadmin" "${RUNORDER_TMP_PATH}/code/dbadmin"
    RUNORDER_CHECKOUT_SUCCESS=$?
    echo "...  done"
    echo "Checking consistency ..."
    if [ $RUNORDER_CHECKOUT_SUCCESS -eq 0 ] ; then
        f_check_runorder_completeness "${RUNORDER_TMP_PATH}/code/dbadmin" "$RUNORDER_TMP_PATH"
        if [ $? -ne 0 ] ; then
            echo "Problems in SVN path ${COPY_SOURCE_URL}/code/dbadmin"
            echo "Inconsistencies in at least one _runorder.txt file!"
            echo "... exiting."
            check_error 2
        fi

        if [ "$DDLRUNORDER_FLAG" == "true" ] ; then
            f_check_runorder_ddlentry "${RUNORDER_TMP_PATH}/code/dbadmin" "$RUNORDER_TMP_PATH"
            if [ $? -ne 0 ] ; then
                echo "Problems in SVN path ${COPY_SOURCE_URL}/code/dbadmin"
                echo "At least one DDL script does not exist in any _runorder.txt file!"
                echo "... exiting."
                check_error 2
            fi

        fi

    fi

    echo "...  done"
    echo ""
    echo "Consistency of _runorder.txt files OK."
    echo ""
    rm -rf $RUNORDER_TMP_PATH
    f_write_date
    echo ""
fi


#---------------------------------------------------------------------------
# 10.) Create new integration test branch
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Create new integration test branch"
echo "###############################################################################"
echo ""

# set SVN log message and do some logging
echo "Creating integration test branch $NEW_INT_BRANCH for build $NEW_BUILD_NO from integration test staging branch" > $LOG_MSG_FILE
cat $LOG_MSG_FILE

# set copy parameters and display them for debugging purpose
COPY_TARGET_URL=$SVN_BASE_URL/$NEW_INT_BRANCH
echo "COPY_SOURCE_URL           : $COPY_SOURCE_URL/code"
echo "COPY_TARGET_URL           : $COPY_TARGET_URL/code"

# perform the copy
$SVN copy $COPY_SOURCE_URL/code $COPY_TARGET_URL/code --parents --file $LOG_MSG_FILE
check_error $?

echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 11.) Create build results
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Calling td_build_result.sh to create build results"
echo "###############################################################################"
echo ""
#
# Now there is a possibility to either do a delta (to previous IT build) or a full build (towards previous release)
#
if [ "$DELTA_FLAG" == "true" ] ; then

    td_build_result.sh \
            -m "$THIS_SCRIPT" \
            -e "it" \
            -s "$SCRIPT_START_DATE" \
            -n "$NEW_BUILD_NO" \
            -o "$PREV_BUILD_NO" \
            -t "$COPY_TARGET_URL" \
            -p "$PREV_BUILD_URL" \
            -c "$PREV_CNT" \
            -l "$LOG_FILE" \
            -d
    check_error $?

else

    td_build_result.sh \
            -m "$THIS_SCRIPT" \
            -e "it" \
            -s "$SCRIPT_START_DATE" \
            -n "$NEW_BUILD_NO" \
            -o "$PREV_REL_NO" \
            -t "$COPY_TARGET_URL" \
            -p "$PREV_REL_URL" \
            -c "$PREV_CNT" \
            -l "$LOG_FILE"
    check_error $?

fi

echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 12.) Create packages
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Calling td_package.sh to create packages"
echo "###############################################################################"
echo ""

td_package.sh \
        -e "it" \
        -b "$NEW_BUILD_NO" \
        -c "delta"
check_error $?

echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 13.) Cleanup & finish
#---------------------------------------------------------------------------

exit 0

