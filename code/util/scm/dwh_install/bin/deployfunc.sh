#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID                        : $Id: deployfunc.sh 1385 2011-12-05 13:57:08Z k9105194 $
# Last ChangedBy            : $Author: k9105194 $
# Last ChangedDate          : $Date: 2011-12-05 14:57:08 +0100 (mån, 05 dec 2011) $
# Last ChangedRevision      : $Revision: 1385 $
# URL                       : $URL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/dwh_install/bin/deployfunc.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Base functions to be included by CM scripts
# Project     : AxEDW
# Subproject  : all
# --------------------------------------------------------------------------
# Change History
# Date       Author         Description
# 2008-08-17 S.Sutter       Initial version 
# 2011-07-07 S.Sutter       Adapted requirements 
#
# --------------------------------------------------------------------------
# Description
#   Base functions that are to be included by all deploy scripts
#
# Dependencies
#   parent  : Called by deploy scripts
#
# Parameters: none
#
# Variables (please add any other variables that you may use):
#   Variables that are used here are defined by the calling script
#   
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) function check_error
# 2.) function f_copy_with_path
# 
# --------------------------------------------------------------------------
# Open points
# --------------------------------------------------------------------------


###########################################################################
# 1.) function check_error
#     Check if last command performed with error, if yes then exit
###########################################################################

check_error ()
    {
    if [ $1 -ne 0 ] ; then
        FINAL_LOG_FILE=`echo $FINAL_LOG_FILE | sed 's/log$/error/'`
        wlog "An error occured! Error code is $1. Exiting ..."
        exit $1
    fi
    }


###########################################################################
# 2.) function f_copy_with_path
#     Copy file from $1 to $2. Create parents if necessary.
###########################################################################

f_copy_with_path ()
    {
    if [ -z "$2" -o -z "$1" ] ; then
        echo "Wrong number of parameters!"
        echo "f_copy_with_path <source-file> <target-file>"
        return 2
    fi
    if [ -e $1 ] ; then
        TARGET_DIR=`dirname $2`
        if [ ! -d $TARGET_DIR ] ; then
            mkdir -p $TARGET_DIR 2>&1
        fi
        cp -p $1 $2 2>&1
        return $?
    else
        echo "Copy source $1 does not exist!"
        return 1
    fi
    }
