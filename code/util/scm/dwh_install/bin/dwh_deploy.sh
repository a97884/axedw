#!/bin/ksh
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID                        : $Id: dwh_deploy.sh 1385 2011-12-05 13:57:08Z k9105194 $
# Last ChangedBy            : $Author: k9105194 $
# Last ChangedDate          : $Date: 2011-12-05 14:57:08 +0100 (mån, 05 dec 2011) $
# Last ChangedRevision      : $Revision: 1385 $
# URL                       : $URL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/dwh_install/bin/dwh_deploy.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# 
# Purpose     : Deploy 
# Project     : AxEDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History
# Date       Author         Description
# 2008-11-24 S.Sutter       Initial version
# 2011-07-07 S.Sutter       Adapt to specifics
#
# --------------------------------------------------------------------------
# Description
#
# Processing Steps 
#  1.) Initialize variables from ini file & function definitions
#  2.) Check for correct syntax
#  3.) Initialize other variables
#  4.) Check for correct parameters and existing files
#  5.) Copy, unzip and untar the package into temporary build directory
#  6.) Move install scripts not part of the application to their correct location
#  7.) Re-tar with changed directory names
#  8.) Run script to backup files replaced or deleted during installation
#  9.) Run script to delete files that are obsolete with this release
# 10.) Unpack to final tar location
# 11.) Cleanup & finish
# 
# --------------------------------------------------------------------------
#
# Parameters:
#   $1      : Target environment
#   $2      : Release number
#   $3      : Release number
#   $4      : tarfile (full path)
#
# Functions used:
#   check_error 
#
# included files:
#   .deploy_profile
#   deployfunc.sh
#
# Variables
#   @variables@
#
# --------------------------------------------------------------------------
# Open points
# .
# .
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
#  1.) Initialize variables from ini file & function definitions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. ./.deploy_profile
. deployfunc.sh


#---------------------------------------------------------------------------
#      Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: `basename $0` <targetenv> <relno> <prev_relno> <tarfile>"
    echo "Install package tarfile to target environment"
    echo "       <targetenv>        : Target environment as defined in DWH_ENV variable"
    echo "       <relno>            : Release number to deploy; format mm.nnn.ppp.hhh"
    echo "       <prev_relno>       : Either previous release number already deployed (format mm.nnn.ppp.hhh) or 'full'"
    echo "       <tarfile>          : Full name AND PATH of the zipped tar file containing the package"
    echo "       <target_type>      : Target environment type, possible values: 'prd' or 'int'"
    echo ""
    }


#---------------------------------------------------------------------------
#      Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#    Cleanup procedures at exit
#    rm -rf $DEPLOY_TMP_PATH
#    rm -f $DEPLOY_TMP_BASE_PATH/$TAR_NAME 2>&1
#    rm -f $DEPLOY_TMP_BASE_PATH/$TAR_RENAMED 2>&1
    echo ""
    echo "Log file can be found under $LOGFILE"
    echo ""
    echo "###############################################################################"
    echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
    echo "###############################################################################"
    echo
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
#  2.) Check for correct syntax
#---------------------------------------------------------------------------

if [ $# -ne 5 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
    print_help
    exit $ERROR_CODE
fi


#---------------------------------------------------------------------------
#  3.) Initialize other variables
#---------------------------------------------------------------------------

SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
export LOGFILE=$DEPLOY_LOG_BASE_PATH/$THIS_SCRIPT.$2.$SCRIPT_START_DATE.log
touch $LOGFILE

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"

# Parameters used    
export TARGET_ENV=$1
export RELEASE_NO=$2
export PREV_REL_NO=$3
export ZIP_ORIG=$4
export INT_OR_PRD=$5

if [ "$INT_OR_PRD" = "prd" ] ; then
    RELEASE_STRING=${PREV_REL_NO}_${RELEASE_NO}
else
    RELEASE_STRING=${PREV_REL_NO}.next
fi

if [ "$PREV_REL_NO" = "full" ] ; then
    IS_FULL_RELEASE="true"
else
    IS_FULL_RELEASE="false"
fi



echo ""
echo "Parameter 1 (Target env)          : $TARGET_ENV"
echo "Parameter 2 (Release no)          : $RELEASE_NO"
echo "Parameter 3 (Previous release no) : $PREV_REL_NO"
echo "Parameter 4 (Source file)         : $ZIP_ORIG"
echo "Parameter 5 (int or prd)          : $INT_OR_PRD"

# File names
export ZIP_NAME=`basename $ZIP_ORIG`
export TAR_NAME=`echo $ZIP_NAME | sed -e 's/\(.*\)\.gz$/\1/'`
export TAR_TMP_DIR=`basename $TAR_NAME .tar`
export TAR_RENAMED=`echo $TAR_NAME | sed -e 's/\(.*\)\.tar$/\1.renamed.tar/'`
echo ""
echo "File names:"
echo "LOGFILE                           : $LOGFILE"
echo "ZIP_NAME                          : $ZIP_NAME"
echo "TAR_NAME                          : $TAR_NAME"
echo "TAR_TMP_DIR                       : $TAR_TMP_DIR"
echo "TAR_RENAMED                       : $TAR_RENAMED"

# Path variables from .deploy_profile
echo ""
echo "Path variables from .deploy_profile:"
echo "DEPLOY_TMP_BASE_PATH              : $DEPLOY_TMP_BASE_PATH"
echo "DEPLOY_BACKUP_BASE_PATH           : $DEPLOY_BACKUP_BASE_PATH"
echo "DEPLOY_LOG_BASE_PATH              : $DEPLOY_LOG_BASE_PATH"
echo "DEPLOY_SCRIPT_PATH                : $DEPLOY_SCRIPT_PATH"
echo "DEPLOY_TARGET_PATH                : $DEPLOY_TARGET_PATH"
echo "DEPLOY_ARCHIVE_PATH               : $DEPLOY_ARCHIVE_PATH"

# Deploy script directories
export SCRIPT_MOVE_SOURCE=$DEPLOY_TMP_BASE_PATH/$TAR_TMP_DIR/build_results
export SCRIPT_MOVE_TARGET=$DEPLOY_SCRIPT_PATH/$RELEASE_NO
echo ""
echo "Deploy script directories:"
echo "SCRIPT_MOVE_SOURCE                : $SCRIPT_MOVE_SOURCE"
echo "SCRIPT_MOVE_TARGET                : $SCRIPT_MOVE_TARGET"

# Deploy scripts
export TRG_BACKUP_SCRIPT=$SCRIPT_MOVE_TARGET/scripts/$RELEASE_STRING.trg_backup_script.sh
export TRG_RESTORE_SCRIPT=$SCRIPT_MOVE_TARGET/scripts/$RELEASE_STRING.trg_restore_script.sh
export TRG_RENAME_SCRIPT=$SCRIPT_MOVE_TARGET/scripts/$RELEASE_STRING.trg_rename_script.sh
export TRG_REMOVE_SCRIPT=$SCRIPT_MOVE_TARGET/scripts/$RELEASE_STRING.trg_remove_script.sh

echo ""
echo "Deploy scripts:"
echo "TRG_BACKUP_SCRIPT                 : $TRG_BACKUP_SCRIPT"
echo "TRG_RESTORE_SCRIPT                : $TRG_RESTORE_SCRIPT"
echo "TRG_RENAME_SCRIPT                 : $TRG_RENAME_SCRIPT"
echo "TRG_REMOVE_SCRIPT                 : $TRG_REMOVE_SCRIPT"
echo ""
echo ""

# File lists
export SVN_ALL_INFA=$SCRIPT_MOVE_TARGET/filelists/$RELEASE_STRING.svn_all_infa.txt
export SVN_ADD_INFA=$SCRIPT_MOVE_TARGET/filelists/$RELEASE_STRING.svn_add_infa.txt
export SVN_DEL_INFA=$SCRIPT_MOVE_TARGET/filelists/$RELEASE_STRING.svn_del_infa.txt
export SVN_UPD_INFA=$SCRIPT_MOVE_TARGET/filelists/$RELEASE_STRING.svn_upd_infa.txt
if [ "$IS_FULL_RELEASE" = "true" ] ; then
    export IMPORT_INFA_FILELIST=$SCRIPT_MOVE_TARGET/filelists/01.000.001.001_${RELEASE_NO}.infa_import_filelist.txt
else
    export IMPORT_INFA_FILELIST=$SCRIPT_MOVE_TARGET/filelists/01.000.001.001_${RELEASE_NO}.infa_import_all_list.txt
fi
echo ""
echo "Deploy scripts:"
echo "SVN_ALL_INFA                      : $SVN_ALL_INFA"
echo "SVN_ADD_INFA                      : $SVN_ADD_INFA"
echo "SVN_DEL_INFA                      : $SVN_DEL_INFA"
echo "SVN_UPD_INFA                      : $SVN_UPD_INFA"
echo "IMPORT_INFA_FILELIST              : $IMPORT_INFA_FILELIST"
echo ""
echo ""


#---------------------------------------------------------------------------
#  4.) Check for correct parameters and existing paths
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Check for correct parameters and existing paths"
echo "###############################################################################"
echo ""

ERROR_CODE=0

# Check if release number is correct
if [ $(echo $ZIP_ORIG | grep $RELEASE_NO | wc -l ) -eq 0 ]; then
    echo "ERROR!!!"
    echo "Package file $ZIP_ORIG and release number $RELEASE_NO do not match!"
    ERROR_CODE=1
fi
# Check if target environment is correct
# if [ "$TARGET_ENV" != "$DWH_ENV" ]; then
    # echo "ERROR!!!"
    # echo "Target environment $TARGET_ENV and user's database environment $DWH_ENV do not match!"
    # ERROR_CODE=1
# fi

# Check if parameters have reasonable values
case $INT_OR_PRD in
    int|prd)
        ;;
    *)
        echo "ERROR!!!"
        echo "Parameter 5 must be either 'int' or 'prd'"
        ERROR_CODE=1
esac


# Check if necessary variables from .deploy_profile exist
if [ -z $DEPLOY_TMP_BASE_PATH ] ; then
    echo "ERROR!!!"
    echo "Variable DEPLOY_TMP_BASE_PATH not defined in .deploy_profile!"
    ERROR_CODE=1
fi
if [ -z $DEPLOY_BACKUP_BASE_PATH ] ; then
    echo "ERROR!!!"
    echo "Variable DEPLOY_BACKUP_BASE_PATH not defined in .deploy_profile!"
    ERROR_CODE=1
fi
if [ -z $DEPLOY_LOG_BASE_PATH ] ; then
    echo "ERROR!!!"
    echo "Variable DEPLOY_LOG_BASE_PATH not defined in .deploy_profile!"
    ERROR_CODE=1
fi
if [ -z $DEPLOY_SCRIPT_PATH ] ; then
    echo "ERROR!!!"
    echo "Variable DEPLOY_SCRIPT_PATH not defined in .deploy_profile!"
    ERROR_CODE=1
fi
if [ -z $DEPLOY_TARGET_PATH ] ; then
    echo "ERROR!!!"
    echo "Variable DEPLOY_TARGET_PATH not defined in .deploy_profile!"
    ERROR_CODE=1
fi
if [ -z $DEPLOY_ARCHIVE_PATH ] ; then
    echo "ERROR!!!"
    echo "Variable DEPLOY_ARCHIVE_PATH not defined in .deploy_profile!"
    ERROR_CODE=1
fi
if [ $(echo $DEPLOY_TARGET_PATH | grep "\/$" | wc -l) -eq 0 ] ; then
    echo "ERROR!!!"
    echo "Variable DEPLOY_TARGET_PATH needs to contain a trailing slash (/)"
    ERROR_CODE=1
fi

if [ ERROR_CODE -ne 0 ] ; then
    check_error 99
fi

# Check if necessary directories exist - if not then create.
if [ ! -d $DEPLOY_TMP_BASE_PATH ] ; then
    echo "$DEPLOY_TMP_BASE_PATH does not exist. Creating ..."
    mkdir -p $DEPLOY_TMP_BASE_PATH 2>&1
    check_error $?
    echo "... done"   
    echo ""
fi
if [ ! -d $DEPLOY_BACKUP_BASE_PATH ] ; then
    echo "$DEPLOY_BACKUP_BASE_PATH does not exist. Creating ..."
    mkdir -p $DEPLOY_BACKUP_BASE_PATH 2>&1
    check_error $?
    echo "... done"   
    echo ""
fi
if [ ! -d $DEPLOY_LOG_BASE_PATH ] ; then
    echo "$DEPLOY_LOG_BASE_PATH does not exist. Creating ..."
    mkdir -p $DEPLOY_LOG_BASE_PATH 2>&1
    check_error $?
    echo "... done"   
    echo ""
fi
if [ ! -d $DEPLOY_SCRIPT_PATH ] ; then
    echo "$DEPLOY_SCRIPT_PATH does not exist. Creating ..."
    mkdir -p $DEPLOY_SCRIPT_PATH 2>&1
    check_error $?
    echo "... done"   
    echo ""
fi
if [ ! -d $DEPLOY_ARCHIVE_PATH ] ; then
    echo "$DEPLOY_ARCHIVE_PATH does not exist. Creating ..."
    mkdir -p $DEPLOY_ARCHIVE_PATH 2>&1
    check_error $?
    echo "... done"   
    echo ""
fi


#---------------------------------------------------------------------------
#  5.) Copy, unzip and untar the package into temporary build directory
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Copy, unzip and untar the package into temporary build directory"
echo "###############################################################################"
echo ""

echo "Copying source file $ZIP_ORIG"
echo "    to temp location $DEPLOY_TMP_BASE_PATH ..."
cp $ZIP_ORIG $DEPLOY_TMP_BASE_PATH 2>&1
check_error $?
echo "... done"   
echo ""

echo "Unzipping $DEPLOY_TMP_BASE_PATH/$ZIP_NAME ..."
gunzip $DEPLOY_TMP_BASE_PATH/$ZIP_NAME 2>&1
check_error $?
echo "... done"   
echo ""
                                                          
export DEPLOY_TMP_PATH=$DEPLOY_TMP_BASE_PATH/$TAR_TMP_DIR
echo "DEPLOY_TMP_PATH : $DEPLOY_TMP_PATH" 
echo ""

echo "Removing old temporary build directory, if it exists ..."
rm -rf $DEPLOY_TMP_PATH 2>&1
check_error $?
echo "... done"
echo ""

echo "Creating temporary build directory ..."
mkdir -p $DEPLOY_TMP_PATH 2>&1
check_error $?
echo "... done"
echo ""

echo "Unpacking $DEPLOY_TMP_BASE_PATH/$TAR_NAME ..."
cd $DEPLOY_TMP_PATH
tar -xvf $DEPLOY_TMP_BASE_PATH/$TAR_NAME 2>&1
check_error $?
echo "... done"
echo ""
cd -


#---------------------------------------------------------------------------
#  6.) Move install scripts not part of the application
#      to their correct location
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Move install scripts not part of the application to their correct location"
echo "###############################################################################"
echo ""

echo "Moving install scripts from $SCRIPT_MOVE_SOURCE to $SCRIPT_MOVE_TARGET ..."
# Create release dir for deploy scripts. Ignore errors if dir exists
mkdir -p $DEPLOY_SCRIPT_PATH/$RELEASE_NO 2>/dev/null
# delete deploy script target dir
       rm -rf $SCRIPT_MOVE_TARGET 2>&1
       check_error $?
# do the actual move
       mv $SCRIPT_MOVE_SOURCE $SCRIPT_MOVE_TARGET 2>&1
       check_error $?
echo "... done"
echo ""


#---------------------------------------------------------------------------
#  7.) Re-tar without install scripts
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Re-tar without install scripts"
echo "###############################################################################"
echo ""

echo "Re-packing to $DEPLOY_TMP_BASE_PATH/$TAR_RENAMED ..."
cd $DEPLOY_TMP_PATH
tar -cvf $DEPLOY_TMP_BASE_PATH/$TAR_RENAMED .
check_error $?
echo ""
cd -
echo "... done"
echo ""


#---------------------------------------------------------------------------
#  8.) Run script to backup files replaced or deleted during installation
#---------------------------------------------------------------------------

if [ $IS_FULL_RELEASE = "false" ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Run script to backup files replaced or deleted during installation"
    echo "###############################################################################"
    echo ""

    echo "Checking if backup script for release $RELEASE_NO exists ..."
    #ls -l $TRG_BACKUP_SCRIPT
    if [ -e $TRG_BACKUP_SCRIPT ] ; then
        echo "Backup script TRG_BACKUP_SCRIPT exists."
        echo "Creating backup directories ..."
        mkdir -p $DEPLOY_BACKUP_BASE_PATH/$RELEASE_NO 2>&1
        echo "... done"
        echo "Calling $TRG_BACKUP_SCRIPT ..."
        $TRG_BACKUP_SCRIPT 2>&1
        # check_error $?
        echo "... done"
        echo "PLEASE NOTE: there is a script to restore the saved files under $TRG_RESTORE_SCRIPT!"
    else
        echo "Backup script $TRG_BACKUP_SCRIPT does not exist. Nothing to execute."
    fi
    echo ""
fi


#---------------------------------------------------------------------------
#  9.) Run script to delete files that are obsolete with this release
#---------------------------------------------------------------------------

if [ $IS_FULL_RELEASE = "false" ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Run script to delete files that are obsolete with this release"
    echo "###############################################################################"
    echo ""

    echo "Checking if script to remove obsolete files for release $RELEASE_NO exists ..."
    #ls -l $TRG_REMOVE_SCRIPT
    if [ -e $TRG_REMOVE_SCRIPT ] ; then
        echo "Remove script TRG_REMOVE_SCRIPT exists."
        echo "Calling $TRG_REMOVE_SCRIPT ..."
        $TRG_REMOVE_SCRIPT 2>&1
        #check_error $?
        echo "... done"
    else
        echo "Remove script $TRG_REMOVE_SCRIPT does not exist. Nothing to execute."
    fi
    echo ""
fi


#---------------------------------------------------------------------------
# 10.) Unpack to final tar location
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Unpack to final tar location"
echo "###############################################################################"
echo ""

echo "Installing $DEPLOY_TMP_BASE_PATH/$TAR_RENAMED to its final location $DEPLOY_TARGET_PATH ..."

cd $DEPLOY_TARGET_PATH  2>&1
tar -xvf $DEPLOY_TMP_BASE_PATH/$TAR_RENAMED  2>&1
# no check_error, since there are always error messages due to missing rights
cd -
echo "... done"
echo ""


#---------------------------------------------------------------------------
# 8. import new and changed Informatica objects and import them 
#---------------------------------------------------------------------------

if  [ -e $IMPORT_INFA_FILELIST ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Please be aware that the following Informatica objects have changed."
    echo "# They will now be imported into the Informatica target repository."
    echo "###############################################################################"
    echo ""

    cat $IMPORT_INFA_FILELIST
    echo ""
    
    # run the import of XML files into the Informatica repository
    impxml.sh "$IMPORT_INFA_FILELIST" "$DEPLOY_TARGET_PATH" "$INT_OR_PRD" "$RELEASE_NO" "$PRV_BUILD_NO"
    check_error $?
fi


#---------------------------------------------------------------------------
# 11.) Cleanup & finish
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Cleanup & finish"
echo "###############################################################################"
echo ""

echo "Removing temporary build directory ..."
rm -rf $DEPLOY_TMP_PATH 2>&1
check_error $?
echo "... done"
echo ""

echo "Removing tar files $TAR_NAME and $TAR_RENAMED ..."
rm $DEPLOY_TMP_BASE_PATH/$TAR_NAME 2>&1
check_error $?
rm $DEPLOY_TMP_BASE_PATH/$TAR_RENAMED 2>&1
check_error $?
echo "... done"
echo ""

echo "Moving original file $ZIP_ORIG"
echo " to archive location $DEPLOY_ARCHIVE_PATH ..."
mv $ZIP_ORIG $DEPLOY_ARCHIVE_PATH
check_error $?
echo "... done"
echo ""


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
