/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Set_TargetExtract_Start.btq 4029 2012-06-25 10:02:49Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-06-25 12:02:49 +0200 (mån, 25 jun 2012) $
# Last Revision    : $Revision: 4029 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/MetaData/procedure/Set_TargetExtract_Start.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-06-07 Teradata  Initial version, replaces macro with same name
# --------------------------------------------------------------------------
# Description
#   Loggs start-of-extract timestamp in metadata
#   NOTE. Parallel runs is currently not supported (requires that JobRunID is included as parameter)
# Dependencies
#   ${DB_ENV}MetadataT.TargetExtract (Table)
#   ${DB_ENV}MetadataT.SystemTarget (Table)
#
# Required Parameters:
#	 p_TargetID	- Target identifier as specified in Metadata.Target table
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}MetadataVIN.Set_TargetExtract_Start
(	IN	p_TargetID	INTEGER
--	,IN	p_JobRunID	INTEGER
)
P0: BEGIN
/******************************************************************************
Required Parameters:
 p_TargetID	- Target identifier as specified in Metadata.Target table
Description: Loggs start-of-extract timestamp in metadata
NOTE. Parallel runs is currently not supported (requires that JobRunID is included as parameter)
*****************************************************************************/
	INSERT INTO ${DB_ENV}MetadataT.TargetExtract
	(	TargetExtractID
		,SystemTargetID
		--,JobRunID
		,TargetExtractStart
	)
	SELECT te0.next_TargetExtractID	AS TargetExtractID
		,st0.SystemTargetID
		--,:JobRunID
		,CURRENT_TIMESTAMP	AS TargetExtractStart
	FROM ${DB_ENV}MetadataVOUT.SystemTarget AS st0
	CROSS JOIN (
		SELECT ZEROIFNULL(MAX(te01.TargetExtractID))+1 AS next_TargetExtractID
		FROM ${DB_ENV}MetadataT.TargetExtract AS te01
	) AS te0
	WHERE st0.TargetID = :p_TargetID
	;
END P0
;
