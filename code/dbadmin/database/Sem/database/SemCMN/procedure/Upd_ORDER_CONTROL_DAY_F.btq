/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_ORDER_CONTROL_DAY_F.btq 11992 2014-01-03 15:16:22Z K9105286 $
# Last Changed By  : $Author: K9105286 $
# Last Change Date : $Date: 2014-01-03 16:16:22 +0100 (fre, 03 jan 2014) $
# Last Revision    : $Revision: 11992 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_ORDER_CONTROL_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_ORDER_CONTROL_DAY_F
(	 INOUT p_TranDt	DATE
	,INOUT p_Days	SMALLINT
)
P0: BEGIN
/*****************************************************************************
Optional parameters:
 p_TranDt	- Date to update, if NULL THEN use (current-date - 1)
 p_Days	- Number of additional days following p_TranDt
 Description:	Updates SALES_TRAN_TOTALS_DAY_F
*****************************************************************************/
DECLARE v_TranDt	DATE;
DECLARE v_Days	SMALLINT;
DECLARE v_StartDt	DATE;
DECLARE v_EndDt		DATE;
DECLARE v_NegCount	INTEGER;
-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;

/*
** v_NegCount is set to 1 if the p_Days is negative. v_NegCount is then used for selecting the date to use as where condition:
**     v_NegCount <= 0 then use Trans_Dt to select records (load records from history)
**     v_NegCount > 0 then use InsertDttm (daily load)
*/
SET v_NegCount = 0
;
IF Coalesce(p_Days,-1) > 0
THEN
	SET v_NegCount = 0;
ELSE
	SET v_NegCount = 1;
END IF
;

SET v_TranDt = COALESCE(p_TranDt,CURRENT_DATE-1)
;
SET v_Days = CASE WHEN p_TranDt IS NULL OR p_Days IS NULL THEN 0 ELSE ABS(p_Days) END
;
SET v_StartDt = v_TranDt + 1 -- order controls are loaded the same day in target as aggregates are loaded into semantic database
;
SET v_EndDt = v_TranDt + v_Days
;

-- Check if dynamic partitioning on target table needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(MONTH FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(MONTH FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.ORDER_CONTROL_DAY_F TO CURRENT WITH DELETE;')
	;
END IF
;
-- Check if dynamic partitioning on AJI needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(YEAR FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(YEAR FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_ORDER_CONTROL_WEEK TO CURRENT;')
	;
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_ORDER_CONTROL_MONTH TO CURRENT;')
	;
END IF
;

MERGE INTO ${DB_ENV}SemCMNT.ORDER_CONTROL_DAY_F as td
   USING 
   (
    Select	
		o.Article_Seq_Num as Article_Seq_Num,
		o.Store_Seq_Num as Store_Seq_Num,
		o.Order_Dt as Order_Dt,
		o.Fixed_Order_Ind as Fixed_Order_Ind,
		c.Calendar_Week_Id as Calendar_Week_Id,
		c.Calendar_Month_Id as Calendar_Month_Id,
		SUM(o.Decreased_Cnt) as Decreased_Cnt,
		SUM(o.Increased_Cnt) as Increased_Cnt,
		SUM(o.Inv_Request_Cnt) as Inv_Request_Cnt,
		SUM(Order_Proposed_Item_Qty) as Order_Proposed_Item_Qty,
		SUM(Order_Item_Qty) as Order_Item_Qty
	From ${DB_ENV}SemCMNVOUT.ORDER_CONTROL_F o
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		On Order_Dt = c.Calendar_Dt
		
Where 
		Case When v_NegCount > 0
			Then o.Order_Dt
			Else Cast(o.InsertDttm as DATE)
		End
		Between :v_StartDt And :v_EndDt
		
Group by

	Article_Seq_Num,
	Store_Seq_Num,
	Order_Dt,
	Fixed_Order_Ind,
	Calendar_Week_Id,
    Calendar_Month_Id 
	
	) AS st

On td.Article_Seq_Num = st.Article_Seq_Num
And td.Store_Seq_num = st.Store_Seq_num
And td.Order_Dt = st.Order_Dt
And td.Fixed_Order_Ind = st.Fixed_Order_Ind

WHEN MATCHED THEN UPDATE SET 

	Decreased_Cnt = td.Decreased_Cnt + st.Decreased_Cnt,
	Increased_Cnt = td.Increased_Cnt + st.Increased_Cnt,
	Inv_Request_Cnt = td.Inv_Request_Cnt + st.Inv_Request_Cnt,
	Order_Proposed_Item_Qty = td.Order_Proposed_Item_Qty + st.Order_Proposed_Item_Qty,
	Order_Item_Qty = td.Order_Item_Qty + st.Order_Item_Qty
	
WHEN NOT MATCHED THEN INSERT( 
	
	Article_Seq_Num,
	Store_Seq_Num,
	Order_Dt,
	Fixed_Order_Ind,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Decreased_Cnt,
	Increased_Cnt,
	Inv_Request_Cnt,
	Order_Proposed_Item_Qty,
	Order_Item_Qty
	)   
VALUES (

	st.Article_Seq_Num,
	st.Store_Seq_Num,
	st.Order_Dt,
	st.Fixed_Order_Ind,
	st.Calendar_Week_Id,
	st.Calendar_Month_Id,
	st.Decreased_Cnt,
	st.Increased_Cnt,
	st.Inv_Request_Cnt,
	st.Order_Proposed_Item_Qty,
	st.Order_Item_Qty

	)
;

-- reload 4 days before current date
IF v_NegCount = 0
THEN

DELETE FROM ${DB_ENV}SemCMNT.ORDER_CONTROL_DAY_F where Order_Dt Between (current_date - 4) And (current_date - 1);

MERGE INTO ${DB_ENV}SemCMNT.ORDER_CONTROL_DAY_F as td
   USING 
   (
    Select	
		o.Article_Seq_Num as Article_Seq_Num,
		o.Store_Seq_Num as Store_Seq_Num,
		o.Order_Dt as Order_Dt,
		o.Fixed_Order_Ind as Fixed_Order_Ind,
		c.Calendar_Week_Id as Calendar_Week_Id,
		c.Calendar_Month_Id as Calendar_Month_Id,
		SUM(o.Decreased_Cnt) as Decreased_Cnt,
		SUM(o.Increased_Cnt) as Increased_Cnt,
		SUM(o.Inv_Request_Cnt) as Inv_Request_Cnt,
		SUM(Order_Proposed_Item_Qty) as Order_Proposed_Item_Qty,
		SUM(Order_Item_Qty) as Order_Item_Qty
	From ${DB_ENV}SemCMNVOUT.ORDER_CONTROL_F o
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		On Order_Dt = c.Calendar_Dt
		
Where 
o.Order_Dt Between (current_date - 4) And (current_date - 1)
		
Group by

	Article_Seq_Num,
	Store_Seq_Num,
	Order_Dt,
	Fixed_Order_Ind,
	Calendar_Week_Id,
    Calendar_Month_Id 
	
	) AS st

On td.Article_Seq_Num = st.Article_Seq_Num
And td.Store_Seq_num = st.Store_Seq_num
And td.Order_Dt = st.Order_Dt
And td.Fixed_Order_Ind = st.Fixed_Order_Ind

WHEN MATCHED THEN UPDATE SET 

	Decreased_Cnt = st.Decreased_Cnt,
	Increased_Cnt = st.Increased_Cnt,
	Inv_Request_Cnt = st.Inv_Request_Cnt,
	Order_Proposed_Item_Qty = st.Order_Proposed_Item_Qty,
	Order_Item_Qty = st.Order_Item_Qty
	
WHEN NOT MATCHED THEN INSERT( 
	
	Article_Seq_Num,
	Store_Seq_Num,
	Order_Dt,
	Fixed_Order_Ind,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Decreased_Cnt,
	Increased_Cnt,
	Inv_Request_Cnt,
	Order_Proposed_Item_Qty,
	Order_Item_Qty
	)   
VALUES (

	st.Article_Seq_Num,
	st.Store_Seq_Num,
	st.Order_Dt,
	st.Fixed_Order_Ind,
	st.Calendar_Week_Id,
	st.Calendar_Month_Id,
	st.Decreased_Cnt,
	st.Increased_Cnt,
	st.Inv_Request_Cnt,
	st.Order_Proposed_Item_Qty,
	st.Order_Item_Qty

	)
;

END IF
;

END P0
;

