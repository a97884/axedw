/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_SALES_TRANSACTION_DAY_F.btq 22705 2017-08-18 09:47:34Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-08-18 11:47:34 +0200 (fre, 18 aug 2017) $
# Last Revision    : $Revision: 22705 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_SALES_TRANSACTION_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_SALES_TRANSACTION_DAY_F
(	 INOUT p_TranDt	DATE
	,INOUT p_Days	SMALLINT
	,INOUT p_TargetPrevExtractTime TIMESTAMP(6)
	,INOUT p_TargetExtractTime	TIMESTAMP(6)
) SQL SECURITY INVOKER
P0: BEGIN
/*****************************************************************************
Normal loading and restart loading through Informatica:
Mandatory parameters
 p_TranDt	- Date to update 
 p_Days	- Number of additional days following p_TranDt 
 p_TargetPrevExtractTime  - Last loadingtime
 p_TargetExtractTime - Current loadingtime 

Manually (call procedure) historyloading: 
Mandatory parameters 
 p_TranDt	- Date to update
 p_Days	- Number of additional days, p_Days is negative
 p_TargetExtractTime 
Optional parameters  
 p_TargetPrevExtractTime  - Last loadingtime

 Description:	Updates SALES_TRANSACTION_DAY_F
*****************************************************************************/
DECLARE v_TranDt	DATE;
DECLARE v_Days	SMALLINT;
DECLARE v_StartDt	DATE;
DECLARE v_EndDt		DATE;
DECLARE v_NegCount	INTEGER;
DECLARE v_TargetPrevExtractTime	TIMESTAMP(6);
DECLARE v_TargetExtractTime	TIMESTAMP(6);
DECLARE v_OpStart	TIMESTAMP(6);

-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;

/*
** v_NegCount is set to 1 if the p_Days is negative. v_NegCount is then used for selecting the date to use as where condition:
**     v_NegCount <= 0 then use Trans_Dt to select records (load records from history)
**     v_NegCount > 0 then use Insert_Dttm (daily load)
*/
SET v_NegCount = 0
;
IF Coalesce(p_Days,-1) > 0
THEN
	SET v_NegCount = 0;
ELSE
	SET v_NegCount = 1;
END IF
;

SET v_TranDt = COALESCE(p_TranDt,CURRENT_DATE-1)
;
SET v_Days = CASE WHEN p_TranDt IS NULL OR p_Days IS NULL THEN 0 ELSE ABS(p_Days) END
;
SET v_StartDt = v_TranDt
;
SET v_EndDt = v_TranDt + v_Days
;
SET v_TargetPrevExtractTime = p_TargetPrevExtractTime
;
SET v_TargetExtractTime = p_TargetExtractTime
;


-- Check if dynamic partitioning on target table needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(MONTH FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(MONTH FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.SALES_TRANSACTION_DAY_F TO CURRENT WITH DELETE;')
	;
END IF
;
-- Check if dynamic partitioning on AJI needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(YEAR FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(YEAR FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_SALES_TRANSACTION_WEEK TO CURRENT;')
	;
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_SALES_TRANSACTION_MONTH TO CURRENT;')
	;
END IF
;

/*
** get stores and dates that will be calculated and recalculated (late arrived transactions and loyalty contact relation to transaction)
*/
CREATE VOLATILE TABLE Tran_Day#Volatile, NO LOG
as
(
	Select st_loc.Store_Seq_Num, st_loc.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION st_loc
	Where
	      (
--	Historical load, just look at transaction date
           :v_NegCount = 1 And
           st_loc.Tran_Dt_DD Between :v_StartDt And :v_EndDt
           )
           OR
		   (
--	Online load, load transactions that arrived into target between last run and this run
           :v_NegCount = 0 And
           st_loc.InsertDttm >= :v_TargetPrevExtractTime
		   and
           st_loc.InsertDttm < 	:v_TargetExtractTime
		   and
		   st_loc.Tran_Dt_DD < CURRENT_DATE
			)
	UNION
	Select stlc_loc.Store_Seq_Num, stlc_loc.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT stlc_loc
	Where
	       (
--	Historical load, just look at transaction date
           :v_NegCount = 1 And
           stlc_loc.Tran_Dt_DD Between :v_StartDt And :v_EndDt
           )
           OR
		   (
--	Online load, load transactions that arrived into target between last run and this run
           :v_NegCount = 0 And
           stlc_loc.InsertDttm >= :v_TargetPrevExtractTime
		   and
           stlc_loc.InsertDttm < :v_TargetExtractTime
		   and
		   stlc_loc.Tran_Dt_DD < CURRENT_DATE
			)
)
with data
Primary index(Store_Seq_Num, Tran_Dt_DD)
on commit preserve rows
;
Collect statistics
index(Store_Seq_Num, Tran_Dt_DD)
On Tran_Day#Volatile
;

-- Delete operation
SET v_OpStart = CURRENT_TIMESTAMP(6)
;

/*
** Remove all old stores,dates that will be recalculated
*/
DELETE FROM ${DB_ENV}SemCMNT.SALES_TRANSACTION_DAY_F
	WHERE (Store_Seq_Num, Tran_Dt ) in
	(
		Select Store_Seq_Num, Tran_Dt_DD
		From Tran_Day#Volatile
	)
;

CALL ${DB_ENV}MetadataVIN.UpdTargetLoad ('SemCMNT.SALES_TRANSACTION_DAY_F'
	,'DELETE',PERIOD(:v_OpStart,CURRENT_TIMESTAMP(6)),:ACTIVITY_COUNT, NULL)
;

-- Insert operation
SET v_OpStart = CURRENT_TIMESTAMP(6)
;
/*
** Calculate new and recalculate old stores,dates selected earlier
*/
INSERT INTO ${DB_ENV}SemCMNT.SALES_TRANSACTION_DAY_F
(
	Store_Seq_Num,
	Store_Department_Seq_Num,
	Tran_Dt,
	Contact_Account_Seq_Num,
	Customer_Seq_Num,
	Calendar_Week_Id,
    Calendar_Month_Id, 
	Junk_Seq_Num,
	Delivery_Method_Cd,
	Empl_Sales_Ind,
	Return_Ind,
	Receipt_Cnt
)   
	Select 
		st.Store_Seq_Num as Store_Seq_Num,
		st.Store_Department_Seq_Num as Store_Department_Seq_Num,
		st.Tran_Dt_DD as Tran_Dt,
		Coalesce(stlc.Contact_Account_Seq_Num,-1) as Contact_Account_Seq_Num,
		Coalesce(stp.Party_Seq_Num,-1) as Customer_Seq_Num,
		c.Calendar_Week_Id as Calendar_Week_Id,
		c.Calendar_Month_Id as Calendar_Month_Id,
		Coalesce(j1.Junk_Seq_Num,-1) as Junk_Seq_Num,
		Coalesce(stdlmd.Circumstance_Value_Cd,'-1') as Delivery_Method_Cd,	
		Case When stt1.Sales_Tran_Seq_Num Is Not Null Then 1 Else 0 End As Empl_Sales_Ind,
		Case When stl3.Sales_Tran_Seq_Num Is Not Null Then 1 Else 0 End As Return_Ind,		
		
		SUM(
			Case When st.Tran_Status_Cd = 'VO' Then -1 
			When st.Tran_Status_Cd = 'CT' Then 1
			Else 0 
			End
		) as Receipt_Cnt
		
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
	  On st.Tran_Dt_DD = c.Calendar_Dt
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY As stp
		On st.Sales_Tran_Seq_Num = stp.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stp.Store_Seq_Num 
		And st.Tran_Dt_DD = stp.Tran_Dt_DD
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As stlc 
		On st.Sales_Tran_Seq_Num = stlc.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stlc.Store_Seq_Num 
		And st.Tran_Dt_DD = stlc.Tran_Dt_DD
		And stlc.InsertDttm < :v_TargetExtractTime
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stc
		On st.Sales_Tran_Seq_Num = stc.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stc.Store_Seq_Num 
		And st.Tran_Dt_DD = stc.Tran_Dt_DD
		And stc.Circumstance_Cd = 'COTP'
	LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
		On st.Store_Seq_Num = std1.Store_Seq_Num and st.Tran_Dt_DD = std1.Calendar_Dt
	LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
		on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
		and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
		and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
		and j1.Loyalty_Ind = 0	
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stdlmd
		On st.Sales_Tran_Seq_Num = stdlmd.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stdlmd.Store_Seq_Num 
		And st.Tran_Dt_DD = stdlmd.Tran_Dt_DD
		And stdlmd.Circumstance_Cd = 'DLMD'
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As stt1
		On st.Sales_Tran_Seq_Num = stt1.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stt1.Store_Seq_Num 
		And st.Tran_Dt_DD = stt1.Tran_Dt_DD
		And stt1.Tran_Type_Cd = 'PD' 			
	Left Outer Join (
		Select 
		stl1.Sales_Tran_Seq_Num,
		stl1.Store_Seq_Num,
		stl1.Tran_Dt_DD
		From ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		Where stl1.Tran_Line_Status_Cd = 'CT'
		And stl1.Return_Reason_Cd <> '-1'
		Group by 
		stl1.Sales_Tran_Seq_Num,
		stl1.Store_Seq_Num,
		stl1.Tran_Dt_DD) stl3		
		On st.Sales_Tran_Seq_Num = stl3.Sales_Tran_Seq_Num
		And st.Store_Seq_Num =  stl3.Store_Seq_Num
		And st.Tran_Dt_DD = stl3.Tran_Dt_DD
		
	Inner Join Tran_Day#Volatile as aloc
	on st.Store_Seq_Num = aloc.Store_Seq_Num 
	And st.Tran_Dt_DD = aloc.Tran_Dt_DD

	Where st.Tran_Type_Cd = 'POS'
	And st.Tran_Status_Cd in ( 'CT', 'VO')
	And (st.Sales_Tran_Seq_Num, st.Store_Seq_Num, st.Tran_Dt_DD) In
	(Select 
		stl2.Sales_Tran_Seq_Num
		,stl2.Store_Seq_Num 
		,stl2.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE stl2
	Where stl2.Tran_Line_Status_Cd = 'CT')

	And st.InsertDttm < :v_TargetExtractTime

	Group by 1,2,3,4,5,6,7,8,9,10,11
;

CALL ${DB_ENV}MetadataVIN.UpdTargetLoad ('SemCMNT.SALES_TRANSACTION_DAY_F'
	,'INSERT',PERIOD(:v_OpStart,CURRENT_TIMESTAMP(6)),:ACTIVITY_COUNT, PERIOD(:v_TargetPrevExtractTime,:v_TargetExtractTime))
;

DROP TABLE Tran_Day#Volatile
;

END P0
;
