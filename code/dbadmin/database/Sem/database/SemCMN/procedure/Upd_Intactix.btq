/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_Intactix.btq 27514 2019-03-25 14:33:56Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2019-03-25 15:33:56 +0100 (mån, 25 mar 2019) $
# Last Revision    : $Revision: 27514 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_Intactix.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_INTACTIX() SQL SECURITY INVOKER

P0: BEGIN

-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;


DELETE FROM ${DB_ENV}SemCMNT.TMP_PLANOGRAM_VERSION_D
;

INSERT INTO ${DB_ENV}SemCMNT.TMP_PLANOGRAM_VERSION_D(
Planogram_Version_Id, Planogram_Id, Planogram_Status_Cd,
LM_Version_Seq_Num, Planogram_Version_Name, Version_Valid_From_Dt,
Version_Valid_To_Dt, Capacity_Unstricted, Number_Of_Fixtures,
Number_Of_Segments, Number_Of_Articles_Allocated
)
select 
t1.Planogram_Version_Id, 
t1.Space_Planogram_Id as Planogram_Id, 
t1.Planogram_Status_Cd, 
coalesce(t1.LM_Version_Seq_Num,-1) as LM_Version_Seq_Num,
t1.Planogram_Version_Name, 
t1.Version_Valid_From_Dt, 
coalesce(next_Valid_From_Dt,t1.Version_Valid_To_Dt,date '9999-12-31') as Version_Valid_To_Dt,
t1.Capacity_Unstricted,
t1.Number_Of_Fixtures, 
t1.Number_Of_Segments, 
t1.Number_Of_Articles_Allocated
from
(
 SELECT spv1.Planogram_Version_Id AS x_Planogram_Version_Id
  ,spv1.Version_Valid_From_Dt AS x_Version_Valid_From_Dt
  ,MAX(spv1.Version_Valid_From_Dt)OVER(PARTITION BY Space_Planogram_Id
   ORDER BY spv1.Version_Valid_From_Dt,spv1.Planogram_Version_Id ASC
   ROWS BETWEEN 1 FOLLOWING AND 1 FOLLOWING) - 1 AS next_Valid_From_Dt
 FROM 
 (select * FROM  ${DB_ENV}TgtVOUT.SPACE_PLANOGRAM_VERSION
 where coalesce(Version_Valid_From_Dt, date '1999-01-01') > '2000-01-01'
 QUALIFY ROW_NUMBER() OVER(PARTITION BY Space_Planogram_Id,Version_Valid_From_Dt ORDER BY Version_Valid_To_Dt DESC,Planogram_Version_Id desc) = 1
) as spv1
) AS lo1
INNER JOIN ${DB_ENV}TgtVOUT.SPACE_PLANOGRAM_VERSION as t1
on lo1.x_Planogram_Version_Id = t1.Planogram_Version_Id

where coalesce(next_Valid_From_Dt,t1.Version_Valid_To_Dt,date '9999-12-31') >= '2018-01-01'
and coalesce(next_Valid_From_Dt,t1.Version_Valid_To_Dt,date '9999-12-31') >=  t1.Version_Valid_From_Dt
;

COLLECT STATISTICS ON ${DB_ENV}SemCMNT.TMP_PLANOGRAM_VERSION_D;

END P0
;
