/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_SALES_TRAN_D_L_ITEM_DAY_F.btq 24038 2018-01-23 10:04:36Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-01-23 11:04:36 +0100 (tis, 23 jan 2018) $
# Last Revision    : $Revision: 24038 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_SALES_TRAN_D_L_ITEM_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_SALES_TRAN_D_L_ITEM_DAY_F
(	 INOUT p_TranDt	DATE
	,INOUT p_Days	SMALLINT
	,INOUT p_TargetPrevExtractTime TIMESTAMP(6)
	,INOUT p_TargetExtractTime	TIMESTAMP(6)
) SQL SECURITY INVOKER
P0: BEGIN
/*****************************************************************************
Normal loading and restart loading through Informatica:
Mandatory parameters
 p_TranDt	- Date to update 
 p_Days	- Number of additional days following p_TranDt 
 p_TargetPrevExtractTime  - Last loadingtime
 p_TargetExtractTime - Current loadingtime 

Manually (call procedure) historyloading: 
Mandatory parameters 
 p_TranDt	- Date to update
 p_Days	- Number of additional days, p_Days is negative
 p_TargetExtractTime 
Optional parameters  
 p_TargetPrevExtractTime  - Last loadingtime
 
 Description:	Updates SALES_TRAN_D_L_ITEM_DAY_F
*****************************************************************************/
DECLARE v_TranDt	DATE;
DECLARE v_Days	SMALLINT;
DECLARE v_StartDt	DATE;
DECLARE v_EndDt		DATE;
DECLARE v_NegCount	INTEGER;
DECLARE v_TargetPrevExtractTime	TIMESTAMP(6);
DECLARE v_TargetExtractTime	TIMESTAMP(6);
DECLARE v_OpStart	TIMESTAMP(6);

-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;

/*
** v_NegCount is set to 1 if the p_Days is negative. v_NegCount is then used for selecting the date to use as where condition:
**     v_NegCount <= 0 then use Trans_Dt to select records (load records from history)
**     v_NegCount > 0 then use Insert_Dttm (daily load)
*/
SET v_NegCount = 0
;
IF Coalesce(p_Days,-1) > 0
THEN
	SET v_NegCount = 0;
ELSE
	SET v_NegCount = 1;
END IF
;

SET v_TranDt = COALESCE(p_TranDt,CURRENT_DATE-1)
;
SET v_Days = CASE WHEN p_TranDt IS NULL OR p_Days IS NULL THEN 0 ELSE ABS(p_Days) END
;
SET v_StartDt = v_TranDt
;
SET v_EndDt = v_TranDt + v_Days
;
SET v_TargetPrevExtractTime = p_TargetPrevExtractTime
;
SET v_TargetExtractTime = p_TargetExtractTime
;

-- Check if dynamic partitioning on target table needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(MONTH FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(MONTH FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F TO CURRENT WITH DELETE;')
	;
END IF
;
-- Check if dynamic partitioning on AJI needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(YEAR FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(YEAR FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_D_L_ITEM_WEEK TO CURRENT;')
	;
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_D_L_ITEM_MONTH TO CURRENT;')
	;
END IF
;

/*
** get stores and dates that will be calculated and recalculated (late arrived transactions and loyalty contact relation to transaction)
*/
CREATE VOLATILE TABLE Tran_Line_Day#Volatile, NO LOG
as
(
		Select st_loc.Store_Seq_Num, st_loc.Tran_Dt_DD
		From ${DB_ENV}TgtVOUT.SALES_TRANSACTION st_loc
		Where
	       (
--	Historical load, just look at transaction date
           :v_NegCount = 1 And
           st_loc.Tran_Dt_DD Between :v_StartDt And :v_EndDt
           )
           OR
		   (
--	Online load, load transactions that arrived into target between last run and this run
           :v_NegCount = 0 And
           st_loc.InsertDttm >= :v_TargetPrevExtractTime
		   and
           st_loc.InsertDttm < 	:v_TargetExtractTime
		   and
		   st_loc.Tran_Dt_DD < CURRENT_DATE
			)
		UNION
			Select stlc_loc.Store_Seq_Num, stlc_loc.Tran_Dt_DD
			From ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT stlc_loc
			Where
	       (
--	Historical load, just look at transaction date
           :v_NegCount = 1 And
           stlc_loc.Tran_Dt_DD Between :v_StartDt And :v_EndDt
           )
           OR
		   (
--	Online load, load transactions that arrived into target between last run and this run
           :v_NegCount = 0 And
           stlc_loc.InsertDttm >= :v_TargetPrevExtractTime
		   and
           stlc_loc.InsertDttm < :v_TargetExtractTime
		   and
		   stlc_loc.Tran_Dt_DD < CURRENT_DATE
			)
)
with data
Primary index(Store_Seq_Num, Tran_Dt_DD)
on commit preserve rows
;
Collect statistics
index(Store_Seq_Num, Tran_Dt_DD)
On Tran_Line_Day#Volatile
;

-- Delete operation
SET v_OpStart = CURRENT_TIMESTAMP(6)
;

/*
** Remove all old stores,dates that will be recalculated
*/
DELETE FROM ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F
	WHERE (Store_Seq_Num, Tran_Dt) IN
	(
		Select Store_Seq_Num, Tran_Dt_DD
		From Tran_Line_Day#Volatile
	)
;

CALL ${DB_ENV}MetadataVIN.UpdTargetLoad ('SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F'
	,'DELETE',PERIOD(:v_OpStart,CURRENT_TIMESTAMP(6)),:ACTIVITY_COUNT, NULL)
;

-- Insert operation
SET v_OpStart = CURRENT_TIMESTAMP(6)
;

/*
** Calculate new and recalculate old stores,dates selected earlier
*/
INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Agreement_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Agreement_Seq_Num as Agreement_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Hist_Article_Seq_Num,
		st.Junk_Seq_Num,
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

	From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		stl0.Agreement_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num,
		stl0.Junk_Seq_Num,
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_LINE_X As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

		Inner Join Tran_Line_Day#Volatile as aloc
		on st.Store_Seq_Num = aloc.Store_Seq_Num 
		And st.Tran_Dt = aloc.Tran_Dt_DD
		And st.InsertDttm < :v_TargetExtractTime
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Agreement_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Hist_Article_Seq_Num,
		st.Junk_Seq_Num
;

CALL ${DB_ENV}MetadataVIN.UpdTargetLoad ('SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F'
	,'INSERT',PERIOD(:v_OpStart,CURRENT_TIMESTAMP(6)),:ACTIVITY_COUNT, PERIOD(:v_TargetPrevExtractTime,:v_TargetExtractTime))
;

DROP TABLE Tran_Line_Day#Volatile
;

END P0
;

