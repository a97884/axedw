/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRAN_LINE_DAY_F.btq 25331 2018-08-13 07:33:26Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-08-13 09:33:26 +0200 (mån, 13 aug 2018) $
# Last Revision    : $Revision: 25331 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/table/SALES_TRAN_LINE_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}SemCMNT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRAN_LINE_DAY_F','PRE','IO',1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE ${DB_ENV}SemCMNT.SALES_TRAN_LINE_DAY_F
(
	Scan_Code_Seq_Num INTEGER NOT NULL DEFAULT -1,
	Sales_Tran_Dt DATE FORMAT 'yyyy-mm-dd' NOT NULL DEFAULT DATE '0001-01-01',
	Day_Of_Week_Num BYTEINT NOT NULL DEFAULT 0 COMPRESS (0,1,2,3,4,5,6,7),
	Calendar_Week_Id INTEGER NOT NULL DEFAULT -1,
	Calendar_Month_Id INTEGER NOT NULL DEFAULT -1,
	Calendar_Year_Id INTEGER NOT NULL DEFAULT -1,
	Receipt_Type_Cd CHAR(2) NOT NULL DEFAULT '-1' COMPRESS ('-1','0 ','2 ','5 ','1 ','3 '),
	Sales_Location_Cd CHAR(2) NOT NULL DEFAULT '-1' COMPRESS ('-1','1','10','11','2','3','4','5','6','7','9','99'),
	Store_Seq_Num INTEGER NOT NULL DEFAULT -1,
	Store_Department_Seq_Num BYTEINT NOT NULL DEFAULT -1 COMPRESS (1,2,3,4,5),
	Campaign_Sales_Type_Cd CHAR(1) NOT NULL DEFAULT '-1' COMPRESS ('N','C','L'),
	Contact_Account_Seq_Num INTEGER NOT NULL DEFAULT -1 COMPRESS -1,
	Customer_Seq_Num INTEGER NOT NULL DEFAULT -1 COMPRESS -1 ,
	AO_Ind BYTEINT NOT NULL DEFAULT -1 COMPRESS (0 ,1 ),
	UOM_Category_Cd CHAR(3) NOT NULL DEFAULT 'UNT' COMPRESS('-1', 'N/A', 'OTH', 'UNK', 'UNT', 'WGH', 'VOL'),
	Preferred_Activity_Code_Id CHAR(3) NOT NULL DEFAULT '-1' COMPRESS ('-1','C01','C03','C04','C07','C08','C09','C12','C14','C15','C21','C24','C36','C37','C38','C39','C68','C71','L09'),
	Pref_Promotion_Offer_Seq_Num INTEGER NOT NULL DEFAULT -1 COMPRESS -1 ,
	Article_Seq_Num INTEGER NOT NULL,
	Junk_Seq_Num INTEGER NOT NULL,
	Delivery_Method_Cd VARCHAR(5) NOT NULL COMPRESS ('-1','C&C','HEM'),
	Empl_Sales_Ind BYTEINT NOT NULL COMPRESS (0,1),
	Return_Ind BYTEINT NOT NULL COMPRESS (0,1),
	GP_Unit_Selling_Price_Amt DECIMAL(18,4) COMPRESS (0.0000,8.9300,1.6000,17.7700,1.7900,1.2000,0.8900,4.4600,17.8100,13.3500,13.3000,13.3900,14.2000),
	Unit_Cost_Amt DECIMAL(18,4) COMPRESS (0.0000,0.8900,1.7900,0.4300,0.4200,8.0000,10.7900,9.6300,7.0000,0.7900),
	Unit_Selling_Price_Amt DECIMAL(18,4) COMPRESS (8.9300,-0.0100,1.6000,17.7700,1.7900,0.0100,4.4600,1.2000,0.8900,17.8100,13.3500,13.3000,13.3900),
	Item_Qty DECIMAL(18,4) COMPRESS (1.00000000,2.00000000,0.00000000,3.00000000,4.00000000,5.00000000,6.00000000),
	Receipt_Line_Cnt DECIMAL(18,4) COMPRESS 1,
	Tax_Amt DECIMAL(18,4) COMPRESS (1.0700,1.6000,2.1400,0.0100,0.4000,2.1300,1.9200,2.6700,0.2100,-0.0100,0.3000,0.5400,0.1100,1.3900,1.1700,1.8200,1.4900,1.6100,1.7000,2.3500),
	Additional_Tax_Amt DECIMAL(18,4) COMPRESS 0.0000,
	SemInsertDttm TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
	Agreement_Seq_Num INTEGER NOT NULL DEFAULT -1 COMPRESS -1,
	PL_Unit_Selling_Price_Amt DECIMAL(18,4),
	PL_Unit_Cost_Amt DECIMAL(18,4)
)
PRIMARY INDEX PISALES_TRANS_LINE_DAY_F ( Scan_Code_Seq_Num, Store_Seq_Num, Sales_Tran_Dt )
 PARTITION BY (RANGE_N ( Sales_Tran_Dt 
	-- Dynamic monthly partitioning moving 36 months (from -36 to +1)
	-- Run alter to current on 1st of month
	BETWEEN  -- Always set start range to 1st of month
    ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(-36 )) 
    AND 
    (ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(1 )))- INTERVAL '1' DAY 
    EACH INTERVAL '1' MONTH ) 
    ,RANGE_N(Store_Seq_Num BETWEEN 1 AND 1000 EACH 1 , NO RANGE OR UNKNOWN)
)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

--CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRAN_LINE_DAY_F','POST','IO',1);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNT.SALES_TRAN_LINE_DAY_F IS '$Revision: 25331 $ - $Date: 2018-08-13 09:33:26 +0200 (mån, 13 aug 2018) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
