/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRANSACTION_LINE_X_2.btq 23957 2018-01-17 05:05:30Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-01-17 06:05:30 +0100 (ons, 17 jan 2018) $
# Last Revision    : $Revision: 23957 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/table/SALES_TRANSACTION_LINE_X_2.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}SemCMNT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRANSACTION_LINE_X_2','PRE','OI',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_2
(	Sales_Tran_Seq_Num	BIGINT NOT NULL,
	Sales_Tran_Line_Num	SMALLINT NOT NULL,
	Store_Seq_Num	INTEGER NOT NULL,
	Tran_Dt_DD	DATE NOT NULL,
	Tran_End_Dttm_DD	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' NOT NULL,
	Scan_Code_Seq_Num	INTEGER NOT NULL,
	Store_Department_Seq_Num	BYTEINT NOT NULL COMPRESS (1,2,3),
	Sold_To_Party_Seq_Num	INTEGER NOT NULL COMPRESS -1,
	Agreement_Seq_Num INTEGER NOT NULL COMPRESS -1,
	Receipt_Type_Cd	CHAR(2) NOT NULL COMPRESS ('-1','0 ','2 ','5 ','1 ','3 '),
	Loyalty_Identifier_Seq_Num	INTEGER NOT NULL COMPRESS -1,
	Sales_Location_Cd	CHAR(2) NOT NULL COMPRESS ('-1','0 ','99'),
	Campaign_Sales_Type_Cd	CHAR(1) NOT NULL COMPRESS ('N','C','L'),
	Preferred_Activity_Code_Id	CHAR(3) NOT NULL COMPRESS ('-1 ','C04','L09','C13','C08','   ','C15'),
	Preferred_Discount_Type_Cd	CHAR(2) NOT NULL COMPRESS ('-1','MA','CO'),
	Pref_Promotion_Offer_Seq_Num BIGINT NOT NULL DEFAULT -1 ,
	RankDiscountType	BYTEINT NOT NULL,
	InsertDttm	TIMESTAMP(6) NOT NULL,
	CONSTRAINT PKSALES_TRANSACTION_LINE_X_2 PRIMARY KEY (Sales_Tran_Seq_Num,Sales_Tran_Line_Num)
)
PRIMARY INDEX PPISALES_TRANSACTION_LINE_X_2(
	Sales_Tran_Seq_Num
) PARTITION BY ( -- Match PPI of Tgt SALES_TRANS... tables
	RANGE_N(Tran_Dt_DD  BETWEEN 
	DATE '2010-01-01' AND DATE '2011-12-31' EACH INTERVAL '3' MONTH ,
	DATE '2012-01-01' AND DATE '2013-12-31' EACH INTERVAL '1' MONTH ,
	DATE '2014-01-01' AND (((ADD_MONTHS((DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(2 ))))( TITLE 'Day')( TITLE 'Day')))- (((((ADD_MONTHS((DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(2 ))))( TITLE 'Day')( TITLE 'Day'))(DATE))- (10106 (DATE))) MOD  7 )+ 1 ( TITLE 'DayOfWeek()')))+ 1  EACH INTERVAL '7' DAY ,
	NO RANGE OR UNKNOWN),
	RANGE_N(Store_Seq_Num  BETWEEN 1  AND 900  EACH 1 ,
	NO RANGE OR UNKNOWN)
);
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRANSACTION_LINE_X_2','POST','OI',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_2 IS '$Revision: 23957 $ - $Date: 2018-01-17 06:05:30 +0100 (ons, 17 jan 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/*
-- Define additional/sample Statistics 
COLLECT STATISTICS USING SAMPLE INDEX PKSALES_TRANSACTION_LINE_X_2
ON ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_2
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COLLECT STATISTICS COLUMN Scan_Code_Seq_Num
ON ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_2
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE*/
