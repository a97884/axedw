/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRAN_TOTALS_DAY_F.btq 20639 2016-12-23 11:29:31Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2016-12-23 12:29:31 +0100 (fre, 23 dec 2016) $
# Last Revision    : $Revision: 20639 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/table/SALES_TRAN_TOTALS_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}SemCMNT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRAN_TOTALS_DAY_F','PRE','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE ${DB_ENV}SemCMNT.SALES_TRAN_TOTALS_DAY_F
(
Contact_Account_Seq_Num INTEGER NOT NULL DEFAULT -1,
Store_Seq_Num INTEGER NOT NULL DEFAULT -1,
Tran_Dt DATE FORMAT 'yyyy-mm-dd' NOT NULL DEFAULT DATE '0001-01-01',
Calendar_Week_Id INTEGER NOT NULL,
Calendar_Month_Id INTEGER NOT NULL,
Junk_Seq_Num INTEGER NOT NULL,
Grand_Amt DECIMAL(18,4) COMPRESS (0.0000,10.0000,20.0000,30.0000,60.0000,55.0000,14.9500,19.9500,46.0000,13.0000,12.9500),
Tot_Discount_Amt DECIMAL(18,4) COMPRESS 0.0000,
Employee_Discount_Based_Amt DECIMAL(18,4) COMPRESS 0.0000,
Tot_Loy_Discount_Amt DECIMAL(18,4) COMPRESS 0.0000,
SemInsertDttm TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
)
PRIMARY INDEX PISALES_TRAN_TOTALS_DAY_F ( Contact_Account_Seq_Num, Store_Seq_Num, Tran_Dt
) PARTITION BY (RANGE_N ( Tran_Dt 
	-- Dynamic monthly partitioning moving 36 months (from -36 to +1)
	-- Run alter to current on 1st of month
	BETWEEN  -- Always set start range to 1st of month
    ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(-36 )) 
    AND 
    (ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(1 )))- INTERVAL '1' DAY 
    EACH INTERVAL '1' MONTH ) 
    ,RANGE_N(Store_Seq_Num BETWEEN 1 AND 1000 EACH 1 , NO RANGE OR UNKNOWN)
)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRAN_TOTALS_DAY_F','POST','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNT.SALES_TRAN_TOTALS_DAY_F IS '$Revision: 20639 $ - $Date: 2016-12-23 12:29:31 +0100 (fre, 23 dec 2016) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
