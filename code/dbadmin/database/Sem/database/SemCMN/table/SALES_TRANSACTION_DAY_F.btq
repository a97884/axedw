/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRANSACTION_DAY_F.btq 22704 2017-08-18 09:26:50Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-08-18 11:26:50 +0200 (fre, 18 aug 2017) $
# Last Revision    : $Revision: 22704 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/table/SALES_TRANSACTION_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}SemCMNT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRANSACTION_DAY_F','PRE','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE ${DB_ENV}SemCMNT.SALES_TRANSACTION_DAY_F
(
	Store_Seq_Num INTEGER NOT NULL DEFAULT -1,
	Store_Department_Seq_Num BYTEINT NOT NULL DEFAULT -1 COMPRESS (1,2,3,4,5),
	Tran_Dt DATE FORMAT 'yyyy-mm-dd' NOT NULL,
	Contact_Account_Seq_Num INTEGER NOT NULL DEFAULT -1,
	Customer_Seq_Num INTEGER NOT NULL DEFAULT -1 COMPRESS -1 ,
	Calendar_Week_Id INTEGER NOT NULL,
	Calendar_Month_Id INTEGER NOT NULL,
	Junk_Seq_Num INTEGER NOT NULL,
	Delivery_Method_Cd VARCHAR(5) NOT NULL COMPRESS ('-1','C&C','HEM'),
	Empl_Sales_Ind BYTEINT NOT NULL COMPRESS (0,1),
	Return_Ind BYTEINT NOT NULL COMPRESS (0,1),	
	Receipt_Cnt INTEGER NOT NULL DEFAULT 0 COMPRESS(-1,1,2,3,4,5),
	SemInsertDttm TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
)
PRIMARY INDEX PISALES_TRANSACTION_DAY_F ( Contact_Account_Seq_Num, Store_Seq_Num, Tran_Dt )
PARTITION BY (RANGE_N ( Tran_Dt 
	-- Dynamic monthly partitioning moving 36 months (from -36 to +1)
	-- Run alter to current on 1st of month
	BETWEEN  -- Always set start range to 1st of month
    ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(-36 )) 
    AND 
    (ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(1 )))- INTERVAL '1' DAY 
    EACH INTERVAL '1' MONTH ) 
    ,RANGE_N(Store_Seq_Num BETWEEN 1 AND 1000 EACH 1 , NO RANGE OR UNKNOWN)
)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRANSACTION_DAY_F','POST','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNT.SALES_TRANSACTION_DAY_F IS '$Revision: 22704 $ - $Date: 2017-08-18 11:26:50 +0200 (fre, 18 aug 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
