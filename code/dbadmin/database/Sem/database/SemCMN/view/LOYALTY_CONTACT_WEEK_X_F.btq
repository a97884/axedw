/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_CONTACT_WEEK_X_F.btq 13764 2014-09-08 15:03:42Z K9105286 $
# Last Changed By  : $Author: K9105286 $
# Last Change Date : $Date: 2014-09-08 17:03:42 +0200 (mån, 08 sep 2014) $
# Last Revision    : $Revision: 13764 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_CONTACT_WEEK_X_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

/* New contacts is implemented in a separate view since this fact could be aggregated over time */


-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.LOYALTY_CONTACT_WEEK_X_F 
( 
	Contact_Account_Seq_Num,
	Calendar_Week_Id,
	Member_Account_Seq_Num, 
	Member_Contact_Type_Cd,
	Contact_Ind,
--	Active_Contact_Ind,
	Contact_Cnt
)
As
Lock row for access
Select	
	lca.Contact_Account_Seq_Num
	,calw.Calendar_Week_Id  
	,lma.Member_Account_Seq_Num
	,aar.Member_Contact_Type_Cd
	,Case 
		When ( lcdb_hist.Contact_Status_Val = 'Active' 
		Or lcdb_hist.Contact_Status_Val = 'Inactive')
		Then 1 
		Else 0 
	End As Contact_Ind
--	, lmdf.Active_Contact_Ind
 , 1 (SMALLINT) As Contact_Cnt
/* Get all contact accounts */
From	${DB_ENV}TgtVOUT.ACCT As ca 
Inner Join ${DB_ENV}TgtVOUT.LOYALTY_CONTACT_ACCOUNT As lca
	On	ca.Account_Seq_Num = lca.Contact_Account_Seq_Num 
/* And the validity time for each contact */
Inner Join ${DB_ENV}TgtVOUT.ACCOUNT_DETAILS_B As cadb 
	On	ca.Account_Seq_Num = cadb.Account_Seq_Num 
	And	cadb.Valid_To_Dttm = SYSLIB.HighTSVal()	
/* Join in calender in order to get a correct fact by day, from start date until end of current week */
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_WEEK As calw
	On	calw.Calendar_Week_End_Dt Between cadb.Account_Open_Dt And CURRENT_DATE + INTERVAL '7' DAY
/* Join in the contact status of the contact */
Inner Join ${DB_ENV}TgtVOUT.LOY_CONTACT_DETAILS_B As lcdb_hist
	On	lca.Contact_Account_Seq_Num = lcdb_hist.Contact_Account_Seq_Num 
	And	calw.Calendar_Week_End_Dttm Between lcdb_hist.Valid_From_Dttm And lcdb_hist.Valid_To_Dttm 
/* Join in the membership */
Inner Join ${DB_ENV}TgtVOUT.ACCT_ACCT_RELATIONSHIP As aar
	On	ca.Account_Seq_num = aar.Account_Seq_Num 
	And  aar.Valid_To_Dttm = SYSLIB.HighTSVal()	
Inner Join ${DB_ENV}TgtVOUT.ACCT As ma
	On	aar.Parent_Account_Seq_Num = ma.Account_Seq_Num 
	And	ma.Account_Type_Cd = 'MEM'
Inner Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_ACCOUNT As lma
	On	ma.Account_Seq_Num =  lma.Member_Account_Seq_Num 
/* Short term solution: Re-use semantic table for Active contact resulted in bad performance.  
Left Outer Join ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F as lmdf
	On lca.Contact_Account_Seq_num = lmdf.Contact_Account_Seq_num
	And calw.Calendar_Week_End_Dt = lmdf.Calendar_Dt */
/* Filter to limit the date multiply and account selection. Current week should be included in the result */
Where	ca.Account_Type_Cd = 'CON'
	And	calw.Calendar_Week_End_Dt Between  '2012-01-01' And CURRENT_DATE + INTERVAL '7' DAY
    And coalesce(lcdb_hist.Contact_Status_Val,'')  Not In ('Candidate', 'Candidate - Removed')
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_CONTACT_WEEK_X_F IS '$Revision: 13764 $ - $Date: 2014-09-08 17:03:42 +0200 (mån, 08 sep 2014) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE