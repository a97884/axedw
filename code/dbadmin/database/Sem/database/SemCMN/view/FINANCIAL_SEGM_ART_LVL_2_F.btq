/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: FINANCIAL_SEGM_ART_LVL_2_F.btq 21370 2017-02-07 08:54:20Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-02-07 09:54:20 +0100 (tis, 07 feb 2017) $
# Last Revision    : $Revision: 21370 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/FINANCIAL_SEGM_ART_LVL_2_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemCMNSecVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNSecVOUT.FINANCIAL_SEGM_ART_LVL_2_F
( 
	Concept_Cd,
	Art_Hier_Lvl_2_Seq_Num,
	Corp_Affiliation_Type_Cd,
	Calendar_Dt,
	Trait_Id,
	Financial_Plan_Type_Cd,
	Fin_Lvl2_Value
) 
As 
	Select	
		fshc1.Concept_Cd,
		fshc1.Node_Seq_Num As Art_Hier_Lvl_2_Seq_Num,
		'-1 ' As Corp_Affiliation_Type_Cd,
		cal.Calendar_Dt,
		fshc1.Trait_Id,
		fp1.Financial_Plan_Type_Cd,
		fshc1.Financial_Value As Fin_Lvl2_Value
	From ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGM_HIER_CONC as fshc1
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal
		On cal.Calendar_Dt between fshc1.Financial_Segment_Start_Dt And fshc1.Financial_Segment_End_Dt
	Inner Join ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGMENT as fs1
		On fshc1.Financial_Plan_Seq_Num = fs1.Financial_Plan_Seq_Num
		And fshc1.Financial_Seg_Type_Cd = fs1.Financial_Seg_Type_Cd
		And fshc1.Valid_From_Dttm = fs1.Valid_From_Dttm
		And fs1.Valid_To_Dttm = SYSLIB.HighTSVal()
	Inner Join ${DB_ENV}TgtSecVOUT.FINANCIAL_PLAN as fp1
		On fs1.Financial_Plan_Seq_Num = fp1.Financial_Plan_Seq_Num
	Inner Join ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D As ahn1
		On	 fshc1.Node_Seq_Num = ahn1.Art_Hier_Lvl_2_Seq_Num
		
	Union All 

	
	Select	
		fshcr1.Concept_Cd,
		fshcr1.Node_Seq_Num As Art_Hier_Lvl_2_Seq_Num,
		fshcr1.Corp_Affiliation_Type_Cd,
		cal2.Calendar_Dt,
		fshcr1.Trait_Id,
		fp2.Financial_Plan_Type_Cd,
		fshcr1.Financial_Value As Fin_Lvl2_Value
	From ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGM_HIER_CONC_REL as fshcr1
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal2
		On cal2.Calendar_Dt between fshcr1.Financial_Segment_Start_Dt And fshcr1.Financial_Segment_End_Dt
	Inner Join ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGMENT as fs2
		On fshcr1.Financial_Plan_Seq_Num = fs2.Financial_Plan_Seq_Num
		And fshcr1.Financial_Seg_Type_Cd = fs2.Financial_Seg_Type_Cd
		And fshcr1.Valid_From_Dttm = fs2.Valid_From_Dttm
		And fs2.Valid_To_Dttm = SYSLIB.HighTSVal()
	Inner Join ${DB_ENV}TgtSecVOUT.FINANCIAL_PLAN as fp2
		On fs2.Financial_Plan_Seq_Num = fp2.Financial_Plan_Seq_Num
	Inner Join ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D As ahn2
		On	 fshcr1.Node_Seq_Num = ahn2.Art_Hier_Lvl_2_Seq_Num		

/* Removed from production 2016-08-24 		
	union all
	
	Select	
		fshcr1.Concept_Cd,
		ahn2.Art_Hier_Lvl_2_Seq_Num As Art_Hier_Lvl_2_Seq_Num,
		fshcr1.Corp_Affiliation_Type_Cd,
		cal2.Calendar_Dt,
		fshcr1.Trait_Id,
		fp2.Financial_Plan_Type_Cd,
		sum(fshcr1.Financial_Value) As Fin_Lvl3_Value
	From ${DB_ENV}TgtVOUT.FINANCIAL_SEGM_HIER_CONC_REL as fshcr1
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal2
		On cal2.Calendar_Dt between fshcr1.Financial_Segment_Start_Dt And fshcr1.Financial_Segment_End_Dt
	Inner Join ${DB_ENV}TgtVOUT.FINANCIAL_SEGMENT as fs2
		On fshcr1.Financial_Plan_Seq_Num = fs2.Financial_Plan_Seq_Num
		And fshcr1.Financial_Seg_Type_Cd = fs2.Financial_Seg_Type_Cd
		And fshcr1.Valid_From_Dttm = fs2.Valid_From_Dttm
		And fs2.Valid_To_Dttm = SYSLIB.HighTSVal()
	Inner Join ${DB_ENV}TgtVOUT.FINANCIAL_PLAN as fp2
		On fs2.Financial_Plan_Seq_Num = fp2.Financial_Plan_Seq_Num
	Inner Join ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_3_D As ahn2
		On	 fshcr1.Node_Seq_Num = ahn2.Art_Hier_Lvl_3_Seq_Num
	Where (fshcr1.Financial_Plan_Seq_Num,fshcr1.Financial_Seg_Type_Cd, ahn2.Art_Hier_Lvl_2_Seq_Num, fshcr1.Concept_Cd, fshcr1.Corp_Affiliation_Type_Cd, fshcr1.Trait_Id, fshcr1.Financial_Segment_Start_Dt, fshcr1.Financial_Segment_End_Dt) Not In 
	( 
		Select 
			t1.Financial_Plan_Seq_Num,t1.Financial_Seg_Type_Cd, t1.Node_Seq_Num, t1.Concept_Cd, t1.Corp_Affiliation_Type_Cd, t1.Trait_Id, t1.Financial_Segment_Start_Dt, t1.Financial_Segment_End_Dt
		From ${DB_ENV}TgtVOUT.FINANCIAL_SEGM_HIER_CONC_REL as t1
	) 
	Group By 
		fshcr1.Concept_Cd,
		ahn2.Art_Hier_Lvl_2_Seq_Num,
		fshcr1.Corp_Affiliation_Type_Cd,
		cal2.Calendar_Dt,
		fshcr1.Trait_Id,
		fp2.Financial_Plan_Type_Cd
*/
	;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* Update the statement with the corresponding view name */
COMMENT ON ${DB_ENV}SemCMNSecVOUT.FINANCIAL_SEGM_ART_LVL_2_F IS '$Revision: 21370 $ - $Date: 2017-02-07 09:54:20 +0100 (tis, 07 feb 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE