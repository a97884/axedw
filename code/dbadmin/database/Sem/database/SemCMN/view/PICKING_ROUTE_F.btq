/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PICKING_ROUTE_F.btq 32435 2020-06-15 15:46:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-15 17:46:55 +0200 (mån, 15 jun 2020) $
# Last Revision    : $Revision: 32435 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/PICKING_ROUTE_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
-- The default database is set.
Database ${DB_ENV}SemCMNVOUT
;
  
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.PICKING_ROUTE_F 
(
Route_Seq_Num
, Store_Seq_Num
, Route_Dt
, Picking_Route_User_Id
, Associate_Seq_Num
, Picking_Route_Section_Cd
, Location_Zone_Seq_Num
, Picking_Type_Cd
, Picking_Order_Type_Cd
, Inventory_Ind
, Nbr_Of_Orders
, Ordered_Qty
, Picked_Qty
, Skipped_Qty
, Replacement_Qty
, Pick_Line_Cnt
, Pick_Time_In_Sec
, Picking_Route_Start_Time_Dttm
, Picking_Route_End_Time_Dttm
, StartUp_Time_In_Sec
, Close_Time_In_Sec
, Number_Of_Bags
, Number_Of_Parcel
, Sales_Order_Id
)
AS 
Lock Row For Access
--Plockeffektivitet per plockrutt 
SELECT Pr.Route_Seq_Num
, Prl.Store_Seq_Num
, Pr.Route_Dt_DD AS Route_Dt
, Pr.Picking_Route_User_Id
, coalesce(a1.Associate_Seq_Num,-1) as Associate_Seq_Num
, coalesce(Pr.Picking_Route_Section_Cd,'-1') as Picking_Route_Section_Cd
, Prl.Location_Zone_Seq_Num
, Prl.Picking_Type_Cd
, Prl.Picking_Order_Type_Cd
, coalesce(Prl.Inventory_Ind,0) as Inventory_Ind
, COUNT( DISTINCT(sol.Sales_Order_Seq_Num)) AS Nbr_Of_Orders
, SUM( CEILING( Prl.Ordered_Qty)) AS Ordered_Qty
, SUM( CEILING( Prl.Picked_Qty)) AS Picked_Qty
, SUM(CEILING( Case When Prl.Picked_Qty = 0 And Prl.Ordered_Qty > 0 Then Prl.Ordered_Qty Else 0 End )) AS Skipped_Qty
, SUM(CEILING( Case When Prl.Picked_Qty > 0 And Prl.Ordered_Qty = 0 Then Prl.Picked_Qty Else 0 End )) AS Replacement_Qty
, COUNT(1) AS Pick_Line_Cnt
, Case 
 When (coalesce(Pr.Picking_Route_Section_Cd,'-1') <> 'Pappersplock') Then Extract(day from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second)) * 24*60*60 + Extract(hour from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second)) *60*60 + Extract(minute from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second)) *60 + Extract(second from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second))
 Else Extract(day from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) * 24*60*60 + Extract(hour from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) *60*60 + Extract(minute from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) *60 + Extract(second from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) 
 End AS Pick_Time_In_Sec
, COALESCE(Pr.Picking_Route_Start_Time_Dttm, Timestamp '0001-01-01 00:00:00.000000') AS Picking_Route_Start_Time_Dttm
, COALESCE(Pr.Picking_Route_End_Time_Dttm, Timestamp '0001-01-01 00:00:00.000000') AS Picking_Route_End_Time_Dttm
, Case 
 When (coalesce(Pr.Picking_Route_Section_Cd,'-1') <> 'Pappersplock') Then Extract(day from ((MIN(Prl.Picked_Dttm) - Pr.Route_Started_Dttm) day(4) to second)) * 24*60*60 + Extract(hour from ((MIN(Prl.Picked_Dttm) -Pr.Route_Started_Dttm) day(4) to second)) *60*60 + Extract(minute from ((MIN(Prl.Picked_Dttm) - Pr.Route_Started_Dttm) day(4) to second)) *60 + Extract(second from ((MIN(Prl.Picked_Dttm) - Pr.Route_Started_Dttm) day(4) to second))
 Else 0 
 End AS StartUp_Time_In_Sec
, Case 
 When (coalesce(Pr.Picking_Route_Section_Cd,'-1') <> 'Pappersplock' And Pr.Route_Finished_Dttm is not null) Then Extract(day from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second)) * 24*60*60 + Extract(hour from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second)) *60*60 + Extract(minute from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second)) *60 + Extract(second from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second))
 Else 0 
 End AS Close_Time_In_Sec
, max(Pr.Number_Of_Bags) as Number_Of_Bags
, max(Pr.Number_Of_Freight_Carrier) as Number_Of_Parcel
, max(So.Sales_Order_Id) as  Sales_Order_Id

FROM ${DB_ENV}TgtVOUT.INTERNAL_PICKING_ROUTE_LINES AS Prl

INNER  JOIN ${DB_ENV}MetaDataVOUT.MAP_PICKING_ROUTE AS Mpr
ON Prl.Route_Seq_Num = Mpr.Route_Seq_Num

INNER  JOIN ${DB_ENV}MetaDataVOUT.Source AS Src 
ON Mpr.SourceID = Src.SourceID

INNER  JOIN ${DB_ENV}TgtVOUT.INTERNAL_PICKING_ROUTE AS Pr 
ON Prl.Route_Seq_Num = Pr.Route_Seq_Num

LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.SALES_ORDER_LINE AS Sol 
on Sol.Sales_Order_Seq_Num = Prl.Sales_Order_Seq_Num
AND Sol.Sales_Order_Line_Id = Prl.Sales_Order_Line_Id
AND Sol.Store_Seq_Num = Prl.Store_Seq_Num

LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.SALES_ORDER AS So 
on Sol.Sales_Order_Seq_Num = So.Sales_Order_Seq_Num
AND Sol.Store_Seq_Num = So.Store_Seq_Num 

LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.ASSOCIATE_D as a1
on a1.Associate_Id = Pr.Picking_Route_User_Id

WHERE  Prl.Picked_Dttm is not null 
AND (coalesce(Pr.Picking_Route_Status_Cd,'') = '2' or Prl.Picking_Type_Cd <> '-1')
AND (coalesce(sol.Sales_Order_Seq_Num,-1) <> -1 or Prl.Picking_Type_Cd <> '-1')
AND Src.SourceName = 'CUB'

GROUP BY
 Pr.Route_Seq_Num
 , Prl.store_seq_num
 , pr.Route_Dt_DD 
 , Pr.Picking_Route_User_Id
 , coalesce(a1.Associate_Seq_Num,-1)
 , coalesce(Pr.Picking_Route_Section_Cd,'-1')
 , Prl.Location_Zone_Seq_Num
 , Prl.Picking_Type_Cd
 , Prl.Picking_Order_Type_Cd
 , Prl.Inventory_Ind 
 , Pr.Route_Started_Dttm
 , Pr.Route_Finished_Dttm
 , Pr.Picking_Route_Start_Time_Dttm
 , Pr.Picking_Route_End_Time_Dttm
 --, So.Sales_Order_Id
 
 UNION ALL
 
 SELECT Pr.Route_Seq_Num
, Prl.Store_Seq_Num
, Prl.Route_Dt_DD AS Route_Dt
, Pr.Picking_Route_User_Id
, coalesce(a1.Associate_Seq_Num,-1) as Associate_Seq_Num
, coalesce(Pr.Picking_Route_Section_Cd,'-1') as Picking_Route_Section_Cd
, Prl.Location_Zone_Seq_Num
, Prl.Picking_Type_Cd
, Prl.Picking_Order_Type_Cd
, coalesce(Prl.Inventory_Ind,0) as Inventory_Ind
, COUNT( DISTINCT(sol.Sales_Order_Seq_Num)) AS Nbr_Of_Orders
, SUM( CEILING( Prl.Ordered_Qty)) AS Ordered_Qty
, SUM( CEILING( Prl.Picked_Qty)) AS Picked_Qty
, SUM(CEILING( Case When Prl.Picked_Qty = 0 And Prl.Ordered_Qty > 0 Then Prl.Ordered_Qty Else 0 End )) AS Skipped_Qty
, SUM(CEILING( Case When Prl.Picked_Qty > 0 And Prl.Ordered_Qty = 0 Then Prl.Picked_Qty Else 0 End )) AS Replacement_Qty
, COUNT(1) AS Pick_Line_Cnt
, Case 
 When (coalesce(Pr.Picking_Route_Section_Cd,'-1') <> 'Pappersplock') Then Extract(day from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second)) * 24*60*60 + Extract(hour from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second)) *60*60 + Extract(minute from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second)) *60 + Extract(second from ((MAX(Prl.Picked_Dttm) - MIN(Prl.Picked_Dttm)) day(4) to second))
 Else Extract(day from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) * 24*60*60 + Extract(hour from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) *60*60 + Extract(minute from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) *60 + Extract(second from ((MAX(Pr.Route_Last_Updated_Dttm) - MIN(Pr.Route_Created_Dttm)) day(4) to second)) 
 End AS Pick_Time_In_Sec
, COALESCE(Pr.Picking_Route_Start_Time_Dttm, Timestamp '0001-01-01 00:00:00.000000') AS Picking_Route_Start_Time_Dttm
, COALESCE(Pr.Picking_Route_End_Time_Dttm, Timestamp '0001-01-01 00:00:00.000000') AS Picking_Route_End_Time_Dttm
, Case 
 When (coalesce(Pr.Picking_Route_Section_Cd,'-1') <> 'Pappersplock') Then Extract(day from ((MIN(Prl.Picked_Dttm) - Pr.Route_Started_Dttm) day(4) to second)) * 24*60*60 + Extract(hour from ((MIN(Prl.Picked_Dttm) -Pr.Route_Started_Dttm) day(4) to second)) *60*60 + Extract(minute from ((MIN(Prl.Picked_Dttm) - Pr.Route_Started_Dttm) day(4) to second)) *60 + Extract(second from ((MIN(Prl.Picked_Dttm) - Pr.Route_Started_Dttm) day(4) to second))
 Else 0 
 End AS StartUp_Time_In_Sec
, Case 
 When (coalesce(Pr.Picking_Route_Section_Cd,'-1') <> 'Pappersplock' And Pr.Route_Finished_Dttm is not null) Then Extract(day from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second)) * 24*60*60 + Extract(hour from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second)) *60*60 + Extract(minute from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second)) *60 + Extract(second from ((Pr.Route_Finished_Dttm - MAX(Prl.Picked_Dttm)) day(4) to second))
 Else 0 
 End AS Close_Time_In_Sec
, max(Pr.Number_Of_Bags) as Number_Of_Bags
, max(Pr.Number_Of_Freight_Carrier) as Number_Of_Parcel
, So.Sales_Order_Id as  Sales_Order_Id

FROM ${DB_ENV}TgtVOUT.INTERNAL_PICKING_ROUTE_LINES AS Prl

INNER  JOIN ${DB_ENV}MetaDataVOUT.MAP_PICKING_ROUTE AS Mpr
ON Prl.Route_Seq_Num = Mpr.Route_Seq_Num

INNER  JOIN ${DB_ENV}MetaDataVOUT.Source AS Src 
ON Mpr.SourceID = Src.SourceID

INNER  JOIN ${DB_ENV}TgtVOUT.INTERNAL_PICKING_ROUTE AS Pr 
ON Prl.Route_Seq_Num = Pr.Route_Seq_Num

LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.SALES_ORDER_LINE AS Sol 
on Sol.Sales_Order_Seq_Num = Prl.Sales_Order_Seq_Num
AND Sol.Sales_Order_Line_Id = Prl.Sales_Order_Line_Id
AND Sol.Store_Seq_Num = Prl.Store_Seq_Num

LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.SALES_ORDER AS So 
on Sol.Sales_Order_Seq_Num = So.Sales_Order_Seq_Num
AND Sol.Store_Seq_Num = So.Store_Seq_Num 

LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.ASSOCIATE_D as a1
on a1.Associate_Id = Pr.Picking_Route_User_Id

WHERE  Prl.Picked_Dttm is not null 
AND (coalesce(Pr.Picking_Route_Status_Cd,'') = '2' or Prl.Picking_Type_Cd <> '-1')
AND (coalesce(sol.Sales_Order_Seq_Num,-1) <> -1 or Prl.Picking_Type_Cd <> '-1')
AND Src.SourceName = 'Watson Pickdata'

GROUP BY
 Pr.Route_Seq_Num
 , Prl.store_seq_num
 , Prl.Route_Dt_DD 
 , Pr.Picking_Route_User_Id
 , coalesce(a1.Associate_Seq_Num,-1)
 , coalesce(Pr.Picking_Route_Section_Cd,'-1')
 , Prl.Location_Zone_Seq_Num
 , Prl.Picking_Type_Cd
 , Prl.Picking_Order_Type_Cd
 , Prl.Inventory_Ind 
 , Pr.Route_Started_Dttm
 , Pr.Route_Finished_Dttm
 , Pr.Picking_Route_Start_Time_Dttm
 , Pr.Picking_Route_End_Time_Dttm
 , So.Sales_Order_Id
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.PICKING_ROUTE_F IS '$Revision: 32435 $ - $Date: 2020-06-15 17:46:55 +0200 (mån, 15 jun 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
