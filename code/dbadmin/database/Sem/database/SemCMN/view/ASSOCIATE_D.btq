/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ASSOCIATE_D.btq 30305 2020-02-19 18:51:37Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2020-02-19 19:51:37 +0100 (ons, 19 feb 2020) $
# Last Revision    : $Revision: 30305 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ASSOCIATE_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

Database ${DB_ENV}SemCMNVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.ASSOCIATE_D
(
	Associate_Seq_Num
	, Employeed_In_Loc_Seq_Num
	, Associate_Id
	, Associate_Name
	, Birth_Date
	, Gender_Type_Cd
	, Current_Manager_Seq_Num
	, Hire_Dt
	, Termination_Dt
	, Business_Unit_Cd
	, Department_Cd
	, Division_Cd
	, Job_Title_Cd
	, Job_Position_Cd
	, Job_Location_Cd
	, Contract_Type_Cd
	, Employment_Type_Cd
	, Employment_Status_Cd
	, Employee_Type_Cd
	, Employee_Form_Cd
	, Employee_Class_Cd
	, Inc_In_Budget_Ind
	, Inc_In_History_Ind
	, Inc_In_Salary_Calc_Ind
	, Inc_In_Planning_Ind
	, Inc_In_Adj_Actual_Ind
	, Inc_In_Prints_Ind
	, Inc_In_Salary_Base_Ind
	, Inc_In_Export_Ind
	, Employee_Age_DD
	, Employeed_Number_Of_Year
	, Employee_Active_Ind
) As
Lock Row For Access
select 
	a.Associate_Seq_Num
	,COALESCE(lo.Location_Seq_Num,-1) As Employeed_In_Loc_Seq_Num
	,a.N_Associate_Id As Associate_Id
	,Coalesce(adb.Associate_Name,'N/A') as Associate_Name
	,COALESCE(adb.Birth_Dt,DATE '1900-01-01') As Birth_Date
	,COALESCE(Gender_Type_Cd,'-1') as Gender_Type_Cd
	,Coalesce(am.Associate_Manager_Seq_Num,-1) As Current_Manager_Seq_Num
	,COALESCE(adb.Hire_Dt, DATE '1900-01-01') As Hire_Dt
	,COALESCE(adb.Termination_Dt, DATE '9999-12-31') as Termination_Dt
	,Coalesce(aeb.Business_Unit_Cd,'-1') As Business_Unit_Cd
	,CASE WHEN Coalesce(aeb.Department_Cd,'-1') = '' THEN '-1' ELSE Coalesce(aeb.Department_Cd,'-1') END As Department_Cd
	,CASE WHEN Coalesce(aeb.Division_Cd,'-1') = '' THEN '-1' ELSE Coalesce(aeb.Division_Cd,'-1') END As Division_Cd
	,Coalesce(aeb.Job_Title_Cd,'-1') As Job_Title_Cd
	,CASE WHEN Coalesce(aeb.Job_Position_Cd,'-1') = '' THEN '-1' ELSE Coalesce(aeb.Job_Position_Cd,'-1') END As Job_Position_Cd
	,Coalesce(aeb.Job_Location_Cd,'-1') As Job_Location_Cd
	,Coalesce(aeb.Contract_Type_Cd,'-1') As Contract_Type_Cd
	,Coalesce(aeb.Employment_Type_Cd,'-1') As Employment_Type_Cd
	,Coalesce(aeb.Employment_Status_Cd,'-1') As Employment_Status_Cd
	,Coalesce(aeb.Employee_Type_Cd,'-1') As Employee_Type_Cd
	,Coalesce(etd.Employee_Form_Cd,'-1') As Employee_Form_Cd
	,Coalesce(aeb.Employee_Class_Cd, '-1') As Employee_Class_Cd
	, adb.Inc_In_Budget_Ind
	, adb.Inc_In_History_Ind
	, adb.Inc_In_Salary_Calc_Ind
	, adb.Inc_In_Planning_Ind
	, adb.Inc_In_Adj_Actual_Ind
	, adb.Inc_In_Prints_Ind
	, adb.Inc_In_Salary_Base_Ind
	, adb.Inc_In_Export_Ind
	,((current_date-COALESCE(adb.Birth_Dt,DATE '1900-01-01')) / 365.25) (DECIMAL (6,2)) as Employee_Age_DD
	,((current_date-COALESCE(adb.Hire_Dt, DATE '1900-01-01')) / 365.25) (DECIMAL (6,2)) as Employeed_Number_Of_Year
	,case when COALESCE(adb.Termination_Dt, DATE '9999-12-31') = date '9999-12-31' then 1 else 0 end as Employee_Active_Ind

from ${DB_ENV}TgtVOUT.ASSOCIATE a
left outer join ${DB_ENV}TgtVOUT.ASSOCIATE_DETAILS_B adb
on adb.Associate_Seq_Num = a.Associate_Seq_Num
and adb.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
--left outer join ${DB_ENV}TgtVOUT.LABOR_POOL lp
--on lp.Associate_Seq_Num = a.Associate_Seq_Num
--and lp.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
left outer join ${DB_ENV}TgtVOUT.ASSOCIATE_EMPLOYMENT_B aeb
on aeb.Associate_Seq_Num = a.Associate_Seq_Num
and aeb.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
left outer join ${DB_ENV}TgtVOUT.ASSOCIATE_MANAGER am
on am.Associate_Seq_Num = a.Associate_Seq_Num
and am.Valid_To_Dt = Date '9999-12-31'
left outer join ${DB_ENV}SemCMNVOUT.EMPLOYEE_TYPE_D etd
on aeb.Employee_Type_Cd = etd.Employee_Type_Cd

left outer join ${DB_ENV}TgtVOUT.ASSOCIATE_COST_CENTER_REL accr
on a.Associate_Seq_Num = accr.Associate_Seq_Num and accr.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
left outer join ${DB_ENV}TgtVOUT.COST_CENTER_LOCATION_ZONE cclz
on cclz.Cost_Center_Seq_Num = accr.Cost_Center_Seq_Num and cclz.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
left outer join ${DB_ENV}TgtVOUT.LOCATION_ZONE lz
on cclz.Location_Zone_Seq_Num = lz.Location_Zone_Seq_Num
left outer join ${DB_ENV}TgtVOUT.LOCATION lo
on lo.N_Location_Id = lz.N_Location_Id

where a.N_Location_Id in (0,-1)

UNION ALL

Select	d0.Dummy_Seq_Num As Associate_Seq_Num
, -1 As Employeed_In_Loc_Seq_Num
, d0.Dummy_Id	As Associate_Id
, 'N/A' As Associate_Name
, DATE '1900-01-01' As Birth_Date
, '-1' as Gender_Type_Cd
, -1 As Current_Manager_Seq_Num
, DATE '1900-01-01' As Hire_Dt
, DATE '9999-12-31' as Termination_Dt
, '-1' As Business_Unit_Cd
, '-1' As Department_Cd
, '-1' As Division_Cd
, '-1' As Job_Title_Cd
, '-1' As Job_Position_Cd
, '-1' As Job_Location_Cd
, '-1' As Contract_Type_Cd
, '-1' As Employment_Type_Cd
, '-1' As Employment_Status_Cd
, '-1' As Employee_Type_Cd
, '-1' As Employee_Form_Cd
, '-1' As Employee_Class_Cd
, null As Inc_In_Budget_Ind
, null As Inc_In_History_Ind
, null As Inc_In_Salary_Calc_Ind
, null As Inc_In_Planning_Ind
, null As Inc_In_Adj_Actual_Ind
, null As Inc_In_Prints_Ind
, null As Inc_In_Salary_Base_Ind
, null As Inc_In_Export_Ind
, 0.00 (DECIMAL (5,2)) as Employee_Age_DD
, 0.00 (DECIMAL (5,2)) as Employeed_Number_Of_Year
, 0 as Employee_Active_Ind
From ${DB_ENV}SemCMNT.DUMMY As d0

;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ASSOCIATE_D IS '$Revision: 30305 $ - $Date: 2020-02-19 19:51:37 +0100 (ons, 19 feb 2020) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE