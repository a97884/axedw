/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: TOBACCO_TRACEABILITY_FCI120b.btq 1 2020-05-04  a97884 $
# Last Changed By  : $Author: a97884 $
# Last Change Date : $Date: 2020-05-04  a$
# Last Revision    : $Revision: 1 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/TOBACCO_TRACEABILITY_SCI365b.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemCMNVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.TOBACCO_TRACEABILITY_FCI120b
(
 ASYNC_EXTERNAL_ID,
 Sales_Tran_Seq_Num       ,
 Sales_Tran_Line_Num,
 eoId,
 eventTime,
 invoiceType1,
 invoiceType2,
 invoiceNumber,
 invoiceDate,
 invoiceSeller,
 invoiceBuyer2,
 invoiceNet,
 invoiceCurrency,
 gtin,
 serialNumber,
 invoiceComment
)

As 

SELECT 	
   'FCI120b_'||trim(stl1.Sales_Tran_Seq_Num) ||'_'||trim(stl1.Sales_Tran_Line_Num) ||'_'|| trim( ttl1.Tendering_Method_Cd)
                                        as ASYNC_EXTERNAL_ID,
    stl1.Sales_Tran_Seq_Num             as Sales_Tran_Seq_Num ,
    stl1.Sales_Tran_Line_Num            as Sales_Tran_Line_Num,
    bdb2.Tobacco_Economic_Operator_Id 	as  eoId,
    stl1.Sales_Tran_Line_End_Dttm   	as eventTime,
    '3'									as invoiceType1,
    ttl1.Tendering_Method_Cd 			as invoiceType2,
    st1.N_Sales_Tran_Id					as invoiceNumber,
    stl1.Sales_Tran_Line_End_Dttm		as invoiceDate,
    bdb2.Tobacco_Economic_Operator_Id 	as invoiceSeller,
    bdb1.Tobacco_Economic_Operator_Id 	as invoiceBuyer2,
    stl1.Unit_Org_Selling_Price_Amt		as invoiceNet,
    'SEK' 								as invoiceCurrency,
    sc1.N_Scan_Cd                       as gtin,
    stlt.Traceability_Id                as serialNumber,
    'CustomerPickupAtStore'				as invoiceComment
	
FROM ${DB_ENV}TgtVOUT.SALES_TRANSACTION AS st1	
            	
INNER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE AS stl1	
	ON 	stl1.Sales_Tran_Seq_Num = st1.Sales_Tran_Seq_Num	
	AND stl1.Store_Seq_Num = st1.Store_Seq_Num	
	AND stl1.Tran_Dt_DD = st1.Tran_Dt_DD	
	AND stl1.Tran_Line_Status_Cd = 'CT'	
--  AND stl1.Return_Reason_Cd <> '-1'	
	
INNER JOIN (Select  Sales_Tran_Seq_Num,Store_Seq_Num,Tran_Dt_DD
               ,min(Tendering_Method_Cd)   as      Tendering_Method_Cd             
              from ${DB_ENV}TgtVOUT.TENDER_TRANSACTION_LINE
              where Tendering_Method_Cd <> 5	
                AND Tendering_Method_Cd <> 100
                AND Tender_Amt     <> '-1' 
                AND TO_NUMBER(Tendering_Method_Cd) is not null
             group by Tran_Dt_DD,Store_Seq_Num,Sales_Tran_Seq_Num
            ) AS ttl1
        ON ttl1.Sales_Tran_Seq_Num = st1.Sales_Tran_Seq_Num
	   AND ttl1.Store_Seq_Num      = st1.Store_Seq_Num
	   AND ttl1.Tran_Dt_DD         = st1.Tran_Dt_DD
		
		
INNER JOIN ${DB_ENV}TgtVOUT.SALES_TRAN_LINE_TRACEABILITY AS stlt	
	ON stl1.Sales_Tran_Seq_Num = stlt.Sales_Tran_Seq_Num	
	AND stl1.Sales_Tran_Line_Num = stlt.Sales_Tran_Line_Num	
	AND stl1.Tran_Dt_DD = stlt.Tran_Dt_DD        	
	AND stl1.Store_Seq_Num = stlt.Store_Seq_Num	
  	
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY AS stp1	
	ON stp1.Sales_Tran_Seq_Num = st1.Sales_Tran_Seq_Num	
	AND stp1.Store_Seq_Num = st1.Store_Seq_Num	
	AND stp1.Tran_Dt_DD = st1.Tran_Dt_DD	
	
LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.BUSINESS_DETAILS_B bdb1	
	ON stp1.Party_Seq_Num = bdb1.Org_Party_Seq_Num	
	AND bdb1.valid_to_dttm = syslib.hightsval()	
	
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.STORE AS s1	
	ON s1.Store_Seq_Num = st1.Store_Seq_Num	
	
 LEFT OUTER JOIN ${DB_ENV}TgtVOUT.PARTY AS p1	
	ON p1.N_Party_Id = cast(s1.N_Store_Id as varchar(50)) 	
	AND p1.Party_Type_Cd =  'ORG'	
	
INNER JOIN ${DB_ENV}TgtVOUT.ORGANIZATION As org	
	ON p1.Party_Seq_Num = org.Org_Party_Seq_Num	
	AND org.Org_Is_Business_Ind = 1	
	AND org.Org_Type_Cd = 'EXT'	
	
LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.BUSINESS_DETAILS_B bdb2	
	ON p1.Party_Seq_Num = bdb2.Org_Party_Seq_Num	
	AND bdb2.valid_to_dttm = syslib.hightsval()	
	
INNER JOIN ${DB_ENV}TgtVOUT.MU_SCAN_CODE AS sc1	
	ON sc1.Scan_Code_Seq_num = stl1.Scan_Code_Seq_Num	
	
-------------------------------------	
INNER JOIN ${DB_ENV}SemCMNVOUT.SCAN_CODE_D AS s2	
	ON s2.Scan_Code_Seq_Num = stl1.Scan_Code_Seq_Num	
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D AS a1	
	ON s2.ARTICLE_SEQ_NUM = a1.ARTICLE_SEQ_NUM	
INNER JOIN ${DB_ENV}TgtVOUT.ARTICLE_TRACEABILITY atm	
	ON a1.article_seq_num = atm.article_seq_num	
	AND atm.Valid_To_Dttm = TIMESTAMP '9999-12-31 23:59:59.999999'	
	AND atm.Traceability_Type_Cd = 'TO'	
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.TOBACCO_TRACEABILITY_FCI120b IS '$Revision: 1  $ - $Date: 2019-12-18  $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

