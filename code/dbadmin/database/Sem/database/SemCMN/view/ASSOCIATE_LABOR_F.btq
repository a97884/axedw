/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ASSOCIATE_LABOR_F.btq 23880 2017-12-21 10:09:30Z a54632 $
# Last Changed By  : $Author: a54632 $
# Last Change Date : $Date: 2017-12-21 11:09:30 +0100 (tor, 21 dec 2017) $
# Last Revision    : $Revision: 23880 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ASSOCIATE_LABOR_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name if needed
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- We create the view

Replace View ${DB_ENV}SemCMNVOUT.ASSOCIATE_LABOR_F
(
 Associate_Seq_Num
 , Location_Seq_Num
 , Store_Seq_Num
 , DC_Seq_Num
 , Junk_Seq_Num
 , Labor_Dt
 , Associate_Manager_Seq_Num
 , Actual_Labor_Minute_Qty
 , Scheduled_Labor_Minute_Qty
 , Actual_Non_Labor_Minute_Qty
 , Scheduled_Non_Labor_Minute_Qty
 , Actual_OT_Labor_Minute_Qty
 , Scheduled_OT_Labor_Minute_Qty
 , Actual_Labor_Cost_Amt
 , Scheduled_Labor_Cost_Amt
 , Actual_OT_Labor_Cost_Amt
 , Scheduled_OT_Labor_Cost_Amt
--	, Actual_Derived_Hourly_Labor_Cost_Amt
--	, Scheduled_Derived_Hourly_Labor_Cost_Amt
) As
Lock Row For Access
select 
 Coalesce(als.Associate_Seq_Num, ala.Associate_Seq_Num) as Associate_Seq_Num
 , Coalesce(als.Location_Seq_Num, ala.Location_Seq_Num) as Location_Seq_Num
 , Coalesce(als.Location_Seq_Num, ala.Location_Seq_Num) as Store_Seq_Num
 , Coalesce(als.Location_Seq_Num, ala.Location_Seq_Num) as DC_Seq_Num
 , j1.junk_seq_num
 , Coalesce(als.Labor_Dt, ala.Labor_Dt) as Labor_Dt
 , Coalesce(Associate_Manager_Seq_Num, -1) as Associate_Manager_Seq_Num
 , ala.Actual_Labor_Minute_Qty
 , als.Scheduled_Labor_Minute_Qty
 , ala.Actual_Non_Labor_Minute_Qty
 , als.Scheduled_Non_Labor_Minute_Qty
 , ala.Actual_OT_Labor_Minute_Qty
 , als.Scheduled_OT_Labor_Minute_Qty
 , ala.Actual_Labor_Cost_Amt
 , als.Scheduled_Labor_Cost_Amt
 , ala.Actual_OT_Labor_Cost_Amt
 , als.Scheduled_OT_Labor_Cost_Amt 
--	, ala.Derived_Hourly_Labor_Cost_Amt as Actual_Derived_Hourly_Labor_Cost_Amt
--	, als.Derived_Hourly_Labor_Cost_Amt as Scheduled_Derived_Hourly_Labor_Cost_Amt
from (
 Select 
 Associate_Seq_Num
 , Location_Seq_Num 
 , Scheduled_Labor_Dt as Labor_Dt
 , Coalesce(Scheduled_Labor_Minute_Qty,0)-Coalesce(Scheduled_OT_Labor_Minute_Qty,0) As Scheduled_Labor_Minute_Qty
 , Scheduled_Non_Labor_Minute_Qty
 , Scheduled_OT_Labor_Minute_Qty
 , Scheduled_Labor_Cost_Amt
 , Scheduled_OT_Labor_Cost_Amt
 From ${DB_ENV}TgtVOUT.ASSOCIATE_LABOR_SCHEDULE 
 ) als 
full outer join (
 Select
  Associate_Seq_Num
 , Location_Seq_Num 
 , Actual_Labor_Dt as Labor_Dt
 , Coalesce(Actual_Labor_Minute_Qty,0)-Coalesce(Actual_OT_Labor_Minute_Qty,0) As Actual_Labor_Minute_Qty
 , Actual_Non_Labor_Minute_Qty
 , Actual_OT_Labor_Minute_Qty
 , Actual_Labor_Cost_Amt
 , Actual_OT_Labor_Cost_Amt
 From ${DB_ENV}TgtVOUT.ASSOCIATE_LABOR_ACTUAL 
) ala
on ala.Associate_Seq_Num = als.Associate_Seq_Num 
and ala.Location_Seq_Num = als.Location_Seq_Num 
and ala.Labor_Dt = als.Labor_Dt
Left Outer Join ${DB_ENV}TgtVOUT.ASSOCIATE_MANAGER am
on Coalesce(ala.Associate_Seq_Num, als.Associate_Seq_Num) = am.Associate_Seq_Num 
and Coalesce(ala.Labor_Dt, als.Labor_Dt) Between am.Valid_From_Dt And am.Valid_To_Dt
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
	On als.Location_Seq_Num = std1.Store_Seq_Num and als.Labor_Dt = std1.Calendar_Dt
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
	on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
	and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
	and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
	and j1.Loyalty_Ind = 0	
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ASSOCIATE_LABOR_F IS '$Revision: 23880 $ - $Date: 2017-12-21 11:09:30 +0100 (tor, 21 dec 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT TOP 1 * FROM ${DB_ENV}SemCMNVOUT.ASSOCIATE_LABOR_F 
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
