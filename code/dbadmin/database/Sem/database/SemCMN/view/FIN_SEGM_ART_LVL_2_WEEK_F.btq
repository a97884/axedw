/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: FIN_SEGM_ART_LVL_2_WEEK_F.btq 27690 2019-04-03 12:36:36Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-04-03 14:36:36 +0200 (ons, 03 apr 2019) $
# Last Revision    : $Revision: 27690 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/FIN_SEGM_ART_LVL_2_WEEK_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemCMNSecVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNSecVOUT.FIN_SEGM_ART_LVL_2_WEEK_F
( 
	Concept_Cd,
	Art_Hier_Lvl_2_Seq_Num,
	Corp_Affiliation_Type_Cd,
	Calendar_Week_End_Dt,
	Calendar_Week_Id,
	Trait_Id,
	Financial_Plan_Type_Cd,
	Fin_Lvl2_Value
) 
As 
    SELECT 
        fshc1.Concept_Cd,
        fshc1.Node_Seq_Num AS Art_Hier_Lvl_2_Seq_Num,
        '-1 ' AS Corp_Affiliation_Type_Cd,
        calw.Calendar_Week_End_Dt,
        cal.Calendar_Week_Id,
        fshc1.Trait_Id,
        fp1.Financial_Plan_Type_Cd,
        fshc1.Financial_Value AS Fin_Lvl2_Value
        FROM ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGM_HIER_CONC AS fshc1
        INNER JOIN ${DB_ENV}TgtVOUT.CALENDAR_DATE AS cal
        ON cal.Calendar_Dt BETWEEN fshc1.Financial_Segment_Start_Dt AND fshc1.Financial_Segment_End_Dt
        INNER JOIN ${DB_ENV}TgtVOUT.CALENDAR_WEEK calw
        ON calw.Calendar_Week_Id = cal.Calendar_Week_Id
        INNER JOIN ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGMENT AS fs1
        ON fshc1.Financial_Plan_Seq_Num = fs1.Financial_Plan_Seq_Num
        AND fshc1.Financial_Seg_Type_Cd = fs1.Financial_Seg_Type_Cd
        AND fshc1.Valid_From_Dttm = fs1.Valid_From_Dttm
        AND fs1.Valid_To_Dttm = SYSLIB.HighTSVal()
        INNER JOIN ${DB_ENV}TgtSecVOUT.FINANCIAL_PLAN AS fp1
        ON fs1.Financial_Plan_Seq_Num = fp1.Financial_Plan_Seq_Num
        INNER JOIN ${DB_ENV}TgtSecVOUT.FINANCIAL_TRAIT AS ft1
        ON fshc1.Trait_Id = ft1.Trait_Id  
          --And ft1.Trait_UOM_Cd = 2
        INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D AS ahn1
        ON  fshc1.Node_Seq_Num = ahn1.Art_Hier_Lvl_2_Seq_Num
      
    UNION ALL
    SELECT 
        fshcr1.Concept_Cd,
        fshcr1.Node_Seq_Num AS Art_Hier_Lvl_2_Seq_Num,
        fshcr1.Corp_Affiliation_Type_Cd,
        calw.Calendar_Week_End_Dt,
        cal2.Calendar_Week_Id,
        fshcr1.Trait_Id,
        fp2.Financial_Plan_Type_Cd,
        fshcr1.Financial_Value AS Fin_Lvl2_Value
        FROM ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGM_HIER_CONC_REL AS fshcr1
        INNER JOIN ${DB_ENV}TgtVOUT.CALENDAR_DATE AS cal2
        ON cal2.Calendar_Dt BETWEEN fshcr1.Financial_Segment_Start_Dt AND fshcr1.Financial_Segment_End_Dt
        INNER JOIN ${DB_ENV}TgtVOUT.CALENDAR_WEEK calw
        ON calw.Calendar_Week_Id = cal2.Calendar_Week_Id
        INNER JOIN ${DB_ENV}TgtSecVOUT.FINANCIAL_SEGMENT AS fs2
        ON fshcr1.Financial_Plan_Seq_Num = fs2.Financial_Plan_Seq_Num
        AND fshcr1.Financial_Seg_Type_Cd = fs2.Financial_Seg_Type_Cd
        AND fshcr1.Valid_From_Dttm = fs2.Valid_From_Dttm
        AND fs2.Valid_To_Dttm = SYSLIB.HighTSVal()
        INNER JOIN ${DB_ENV}TgtSecVOUT.FINANCIAL_PLAN AS fp2
        ON fs2.Financial_Plan_Seq_Num = fp2.Financial_Plan_Seq_Num
        INNER JOIN ${DB_ENV}TgtSecVOUT.FINANCIAL_TRAIT AS ft2
        ON fshcr1.Trait_Id = ft2.Trait_Id  
          --And ft2.Trait_UOM_Cd = 2		
        INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D AS ahn2
        ON  fshcr1.Node_Seq_Num = ahn2.Art_Hier_Lvl_2_Seq_Num  	
	
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* Update the statement with the corresponding view name */
COMMENT ON ${DB_ENV}SemCMNSecVOUT.FIN_SEGM_ART_LVL_2_WEEK_F IS '$Revision: 27690 $ - $Date: 2019-04-03 14:36:36 +0200 (ons, 03 apr 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE