/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PROMOTION_OFFER_D.btq 28850 2019-09-02 10:41:56Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $
# Last Revision    : $Revision: 28850 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/PROMOTION_OFFER_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.PROMOTION_OFFER_D
(
	Promotion_Offer_Seq_Num
	,Promotion_Offer_Id
	,Promotion_Offer_Name
	,Promotion_Offer_Desc
	,Campaign_Seq_Num
	,Promotion_Offer_Start_Dt
	,Promotion_Offer_End_Dt
	,Promotion_Type_Cd
	,Promotion_Offer_Type_Cd
	,Location_Hierachy_Description
	,Packaged_Offer_Name
	,Packaged_Offer_Ind
	,Customer_In_Segment
	,Bonus_Buy_Description
	,Promotion_Business_Status_Cd
	,Promotion_Offer_Status_Cd
	,Promotion_Transfer_Status_Cd
	,Transfer_Dttm
	,Bonuscard_Ind
	,Membercard_Ind
	,Loy_Segment_Ind
	,Subtotal_Ind
	,Minimum_Spend_Amt
	,Stores_In_Offer_Cnt
	,Promo_Offer_Cnt
	,Concept_Cd
	,Concept_Top_Cd
	,Order_End_Dt
) 
As
Select
	po1.Promotion_Offer_Seq_Num
	,po1.N_Promotion_Offer_Id 		As Promotion_Offer_Id
	,Coalesce(ponb1.Promotion_Name, 'SAKNAS')	As Promotion_Offer_Name
	,Coalesce(podb1.Promotion_Offer_Desc, 'SAKNAS') As  Promotion_Offer_Desc
	,Coalesce(cpo1.Campaign_Seq_Num, -1) 	As Campaign_Seq_Num
	,pocdb1.Campaign_Promotion_Start_Dt	As Promotion_Offer_Start_Dt
	,pocdb1.Campaign_Promotion_End_Dt As Promotion_Offer_End_Dt
	,Coalesce(pocdb1.Promotion_Type_Cd, '-1') As Promotion_Type_Cd
	,Coalesce(podb1.Promotion_Offer_Type_Cd, '-1') As Promotion_Offer_Type_Cd
	,Coalesce(podb1.Location_Hierachy_Description, 'SAKNAS') As Location_Hierachy_Description
	,podb1.Packaged_Offer_Name
	,Coalesce(podb1.Packaged_Offer_Ind, '0') As Packaged_Offer_Ind
	,podb1.Customer_In_Segment
	,podb1.Bonus_Buy_Description
	,Coalesce(posb1.Promotion_Business_Status_Cd, '-1') As Promotion_Business_Status_Cd
	,Coalesce(posb1.Promotion_Offer_Status_Cd, -1) As Promotion_Offer_Status_Cd
	,Coalesce(posb1.Promotion_Transfer_Status_Cd, -1) As Promotion_Transfer_Status_Cd
	,posb1.Transfer_Dttm
	,Case 
		When pop1.Promotion_Offer_Seq_Num Is Not Null And pop1.Art_1 = 1 Then '1'
		Else '0' End As Bonuscard_Ind
	,Case 
		When pop1.Promotion_Offer_Seq_Num Is Not Null And pop1.Art_7 = 1 Then '7'
		Else '0' End As Membercard_Ind
	,Case 
		When pop1.Promotion_Offer_Seq_Num Is Not Null And pop1.Art_8 = 1 Then '8'
		Else '0' End As Loy_Segment_Ind
	,Coalesce(pop99.N_Article_Id, '0') As Subtotal_Ind
	,pop99.Minimum_Spend_Amt
	,pos1.Stores_In_Offer_Cnt
	,1 (SMALLINT) As Promo_Offer_Cnt
	,po1.Concept_Cd
	,po1.Concept_Top_Cd
	,Case 
		When podb1.Order_End_Dt is Null  Then  pocdb1.Campaign_Promotion_Start_Dt - 28
		Else podb1.Order_End_Dt End As Order_End_Dt		
/* Common logic for several views is collected in a separate SemCMNVIN view */
From	${DB_ENV}SemCMNVIN.PROMOTION_OFFER As po1
Left Outer Join ${DB_ENV}TgtVOUT.PROMOTION_OFFER_NAME_B As ponb1
	On po1.Promotion_Offer_Seq_Num = ponb1.Promotion_Offer_Seq_Num
	And ponb1.Valid_To_Dttm = SYSLIB.HighTSVal()	
Inner Join ${DB_ENV}TgtVOUT.PROMOTION_OFFER_DETAILS_B As podb1
	On po1.Promotion_Offer_Seq_Num = podb1.Promotion_Offer_Seq_Num
	And podb1.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join ${DB_ENV}TgtVOUT.CAMPAIGN_PROMOTION_OFFER As cpo1
	On po1.Promotion_Offer_Seq_Num = cpo1.Promotion_Offer_Seq_Num
	And cpo1.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join	${DB_ENV}TgtVOUT.PROMO_OFFER_CAMPAIGN_DETAILS_B As pocdb1
	On po1.Promotion_Offer_Seq_Num = pocdb1.Promotion_Offer_Seq_Num
	And pocdb1.Valid_To_Dttm = SYSLIB.HighTSVal()	
Left Outer Join	${DB_ENV}TgtVOUT.PROMOTION_OFFER_STATUS_B As posb1
	On po1.Promotion_Offer_Seq_Num = posb1.Promotion_Offer_Seq_Num
	And posb1.Valid_To_Dttm = SYSLIB.HighTSVal()	
-- Create Ind for different prereq articles in Offer
Left Outer Join 
	(Select 
		pop1.Promotion_Offer_Seq_Num
		,Max(Case when N_Article_Id = '1' Then 1 End) As Art_1 	/* BONUSKORT */
		,Max(Case when N_Article_Id = '7' Then 1 End) As Art_7 	/* MEDLEMSKORT */
		,Max(Case when N_Article_Id = '8' Then 1 End) As Art_8	/* LOJALITET SEGMENTERAD */
	From ${DB_ENV}TgtVOUT.PROMOTION_OFFER_PREREQUISITE As pop1
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE As a1 
		On pop1.Article_Seq_Num = a1.Article_Seq_Num
		And a1.N_Article_Id In ('1', '7', '8') 
	Where pop1.Valid_To_Dttm = SYSLIB.HighTSVal()
		Group by 1	) As pop1
	On po1.Promotion_Offer_Seq_Num = pop1.Promotion_Offer_Seq_Num
-- Create Ind for different prereq articles in Offer
Left Outer Join 
	(Select 
		pop99a.Promotion_Offer_Seq_Num
		,a99.N_Article_Id
		,pop99a.Minimum_Spend_Amt
	From ${DB_ENV}TgtVOUT.PROMOTION_OFFER_PREREQUISITE As pop99a
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE As a99 
		On pop99a.Article_Seq_Num = a99.Article_Seq_Num
		And a99.N_Article_Id In ('99') /* SUBTOTAL */
	Where pop99a.Valid_To_Dttm = SYSLIB.HighTSVal()) As pop99
	On po1.Promotion_Offer_Seq_Num = pop99.Promotion_Offer_Seq_Num
-- Count Nbr of stores in offer, for reporting
Left Outer Join
	(Select 
		pos2.Promotion_Offer_Seq_Num,
		Count(pos2.Store_Seq_Num) As Stores_In_Offer_Cnt
	From ${DB_ENV}TgtVOUT.PROMOTION_OFFER_STORE as pos2
	Where pos2.Valid_To_Dttm = SYSLIB.HighTSVal()
	Group by pos2.Promotion_Offer_Seq_Num) As pos1
	On po1.Promotion_Offer_Seq_Num = pos1.Promotion_Offer_Seq_Num

Union All -- Add record for missing masterdata
Select -1 (BIGINT) As Promotion_Offer_Seq_Num,
d0.Dummy_Id As Promotion_Offer_Id,
'SAKNAS' As Promotion_Offer_Name,
'SAKNAS' Promotion_Offer_Desc,
-1 as Campaign_Seq_Num,
(Date '2000-01-01')  as Promotion_Offer_Start_Dt,
(Date '9999-12-31') as Promotion_Offer_End_Dt,
'-1' as Promotion_Type_Cd,
'-1' as Promotion_Offer_Type_Cd,
'SAKNAS' As Location_Hierachy_Description,
'SAKNAS' As Packaged_Offer_Name,
'0' as Packaged_Offer_Ind,
'-1' as Customer_In_Segment,
'SAKNAS' as Bonus_Buy_Description,
'-1' As Promotion_Business_Status_Cd,
-1 As Promotion_Offer_Status_Cd,
-1 As Promotion_Transfer_Status_Cd,
SYSLIB.HighTSVal() as Transfer_Dttm,
'0' As Bonuscard_Ind,
'0' As Membercard_Ind,
'0' As Loy_Segment_Ind,
'0' As Subtotal_Ind,
0.0 (DECIMAL(19,5)) as Minimum_Spend_Amt,
0 as Stores_In_Offer_Cnt,
0 (SMALLINT) as Promo_Offer_Cnt,
'-1' as Concept_Cd,
'-1' as Concept_Top_Cd,
(Date '9999-12-31') as Order_End_Dt
From ${DB_ENV}SemCMNT.DUMMY As d0
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.PROMOTION_OFFER_D IS '$Revision: 28850 $ - $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
