/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ARTICLE_COST_PRICE_COND_F.btq 26449 2018-11-29 07:12:58Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-11-29 08:12:58 +0100 (tor, 29 nov 2018) $
# Last Revision    : $Revision: 26449 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ARTICLE_COST_PRICE_COND_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.ARTICLE_COST_PRICE_COND_F
(
Sales_Org_Seq_Num,
Cost_Price_List_Cd,
Article_Seq_Num,
Vendor_Party_Seq_Num,
Vendor_Subrange_Cd,
Currency_Cd,
Cost_Price_UOM_Cd,
Price_Condition_UOM_Cd,
Price_Condition_Type_Cd,
Price_Calculation_Type_Cd,
Calendar_Dt,
Price_Condition_Rate,
Price_Condition_Val,
Price_Condition_Amt
)
As
Select 
	org.Org_Party_Seq_Num As Sales_Org_Seq_Num --(always Dagab for transfer price documents)
	, cond.Price_List_Cd As Cost_Price_List_Cd
	, cond.Article_Seq_Num
	, price.Vendor_Party_Seq_Num
	, price.Vendor_Subrange_Cd
	, price.Currency_Cd
	, price.Cost_Price_UOM_Cd
	, cond.Price_Condition_UOM_Cd
	, cond.Price_Condition_Type_Cd
	, cond.Price_Calculation_Type_Cd
	, cal.Calendar_Dt
	, cond.Price_Condition_Rate
	, cond.Price_Condition_Val
	, Case When ((cond.Price_Condition_UOM_Cd = 'KG' And cond.Price_Calculation_Type_Cd = 'C') Or cond.Price_Condition_Type_Cd in ('ZPB1'))
		Then cond.Price_Condition_Rate 
		Else cond.Price_Condition_Val 
		End As Price_Condition_Amt
/* For each day */
From ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal
/* fetch the valid documents */	
Inner Join ${DB_ENV}TgtVOUT.ARTICLE_COST_PRICE_SCHEDULE As price_s
On cal.Calendar_Dt between price_s.Valid_From_Dt And price_s.Valid_To_Dt
/* fetch the price document */	
Inner Join ${DB_ENV}TgtVOUT.ARTICLE_COST_PRICE As price
On price.Org_Party_Seq_Num =  price_s.Org_Party_Seq_Num
And price.Distribution_Channel_Cd =  price_s.Distribution_Channel_Cd
And price.Price_List_Cd =  price_s.Price_List_Cd
And price.Article_Seq_Num =  price_s.Article_Seq_Num
And price.Valid_From_Dt =  price_s.Valid_From_Dt
And price.Cost_Price_Doc_Version_Num =  price_s.Cost_Price_Doc_Version_Num
/* Each condition applied */
Inner Join ${DB_ENV}TgtVOUT.ARTICLE_COST_PRICE_COND As cond
On price.Org_Party_Seq_Num = cond.Org_Party_Seq_Num
And price.Distribution_Channel_Cd = cond.Distribution_Channel_Cd
And price.Price_List_Cd = cond.Price_List_Cd
And price.Article_Seq_Num = cond.Article_Seq_Num
And price.Valid_From_Dt = cond.Valid_From_Dt
And price.Cost_Price_Doc_Version_Num =  cond.Cost_Price_Doc_Version_Num
/* For what sales organization */	
Inner Join ${DB_ENV}TgtVOUT.ORGANIZATION As org
On org.Org_Party_Seq_Num = price.Org_Party_Seq_Num 
/* Fetch price documents for Sales Org DAGAB and from analytical day 1 uptil now */	
Where price.Distribution_Channel_Cd = '20'
And org.N_Org_Party_Id = 'S011' And org.Internal_Org_Type_Cd = 'SORG'
And cal.Calendar_Dt between DATE '2011-01-01' and ADD_MONTHS(CURRENT_DATE,13)
And price_s.Valid_Price_Schedule_Ind = 1
And price_s.Recorded_End_Dt = Date '9999-12-31'
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ARTICLE_COST_PRICE_COND_F IS '$Revision: 26449 $ - $Date: 2018-11-29 08:12:58 +0100 (tor, 29 nov 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE