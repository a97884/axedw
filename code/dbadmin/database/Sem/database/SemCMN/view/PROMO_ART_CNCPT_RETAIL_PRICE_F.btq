/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PROMO_ART_CNCPT_RETAIL_PRICE_F.btq 28850 2019-09-02 10:41:56Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $
# Last Revision    : $Revision: 28850 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/PROMO_ART_CNCPT_RETAIL_PRICE_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.PROMO_ART_CNCPT_RETAIL_PRICE_F
(
 Promotion_Offer_Seq_Num
 ,Article_Seq_Num
 ,Concept_Cd
 ,Concept_Top_Cd
 ,Promotion_Offer_Start_Dt 
 ,Promo_Cost_Price_Amt_Excl_VAT
 ,Promo_Rtl_Price_Amt_Excl_VAT
 ,Promo_Rtl_Price_Amt_Incl_VAT
) 
As
Lock row for access
/* The lowest price in the concept shall be used when follow up promotions */
Select 
 po.Promotion_Offer_Seq_Num
 ,po.Article_Seq_Num 
 ,po.Concept_Cd
 ,po.Concept_Top_Cd
 ,po.Promotion_Offer_Start_Dt As Promotion_Offer_Start_Dt
 ,Min(po.Promo_Cost_Price_Amt_Excl_VAT) As Promo_Cost_Price_Amt_Excl_VAT
 ,Min(po.Promo_Rtl_Price_Amt_Excl_VAT) As Promo_Rtl_Price_Amt_Excl_VAT
 ,Min(po.Promo_Rtl_Price_Amt_Incl_VAT) As Promo_Rtl_Price_Amt_Incl_VAT
/* Fetch the lowest retailprice that is valid on the first day of the promotion offer */
From
(
	Select
	poa3.Promotion_Offer_Seq_Num
	,poa3.Article_Seq_Num 
	,poa3.Concept_Cd
	,poa3.Concept_Top_Cd
	,poa3.Campaign_Promotion_Start_Dt As Promotion_Offer_Start_Dt
	,arps1.Valid_From_Dt
	,arp1.Applied_Cost_Price_Amt As Promo_Cost_Price_Amt_Excl_VAT
	,arp1.Net_Retail_Price_Amt As Promo_Rtl_Price_Amt_Excl_VAT
	,arp1.Final_Retail_Price_Amt As Promo_Rtl_Price_Amt_Incl_VAT
	
	From 
		( Select poa1.Promotion_Offer_Seq_Num
				,poa1.Article_Seq_Num
				,poa1.Concept_Cd
				,poa1.Concept_Top_Cd
				,pocdb1.Campaign_Promotion_Start_Dt
				,lo1.Org_Party_Seq_Num
				,lrpg1.Retail_Price_List_Cd
		 From (Select
				poa2.Promotion_Offer_Seq_Num
				,poa2.Article_In_Offer_Ind
				,poa2.Article_Seq_Num
				,poa2.Concept_Cd
				,po1.Concept_Top_Cd
				From ${DB_ENV}SemCMNVIN.PROMOTION_OFFER_ARTICLE poa2
				Inner Join ${DB_ENV}SemCMNVIN.PROMOTION_OFFER As po1
				On poa2.Promotion_Offer_Seq_Num = po1.Promotion_Offer_Seq_Num
				And poa2.Concept_Cd = po1.Concept_Cd
			 /* Eliminate duplicates in Vendor Fund */
			 Group by 1, 2, 3, 4, 5) As poa1
		 Inner Join ${DB_ENV}TgtVOUT.PROMO_OFFER_CAMPAIGN_DETAILS_B As pocdb1
			On poa1.Promotion_Offer_Seq_Num = pocdb1.Promotion_Offer_Seq_Num
			And pocdb1.Valid_To_Dttm = SYSLIB.HighTSVal()
	 /* Use stores in the Promotion to select Salesorg and pricelist*/
		 Inner Join ${DB_ENV}TgtVOUT.PROMOTION_OFFER_STORE as pos1
			On poa1.Promotion_Offer_Seq_Num = pos1.Promotion_Offer_Seq_Num
			And pos1.Valid_To_Dttm = SYSLIB.HighTSVal()
		/* Select Salesorg for the stores in the Promotion */
		 Inner Join ${DB_ENV}TgtVOUT.LOCATION_ORGANIZATION As lo1 
			On pos1.Store_Seq_Num = lo1.Location_Seq_Num
			And lo1.Valid_To_Dttm = SYSLIB.HighTSVal()
			And lo1.Location_Org_Role_Cd = 'HOS'  
		 /* Fetch lvl 4 */
		 Inner Join ${DB_ENV}SemCMNT.ARTICLE_D As a1 
			On poa1.Article_Seq_Num = a1.Article_Seq_Num
		 /* Fetch all valid pricelists for the article */
		 Inner Join ${DB_ENV}TgtVOUT.LOCATION_RETAIL_PRICING_GROUP As lrpg1
			On pos1.Store_Seq_Num = lrpg1.Location_Seq_Num
			And a1.Art_Hier_Lvl_4_Seq_Num = lrpg1.Merc_Cat_Node_Seq_Num
			And pocdb1.Campaign_Promotion_Start_Dt Between lrpg1.Valid_From_Dttm And lrpg1.Valid_To_Dttm 
		Group by poa1.Promotion_Offer_Seq_Num
				,poa1.Article_Seq_Num
				,poa1.Concept_Cd
				,poa1.Concept_Top_Cd
				,pocdb1.Campaign_Promotion_Start_Dt
				,lo1.Org_Party_Seq_Num
				,lrpg1.Retail_Price_List_Cd) poa3        
	/* Fetch the price document */ 
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE_RETAIL_PRICE_SCHEDULE As arps1
	 On poa3.Article_Seq_Num = arps1.Article_Seq_Num
	 And poa3.Org_Party_Seq_Num = arps1.Org_Party_Seq_Num
	 And poa3.Retail_Price_List_Cd = arps1.Price_List_Cd
	 And arps1.Valid_Price_Schedule_Ind = 1
	 And arps1.Recorded_End_Dt = Date '9999-12-31'
	 And poa3.Campaign_Promotion_Start_Dt Between arps1.Valid_From_Dt And arps1.Valid_To_Dt 
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE_RETAIL_PRICE As arp1
		On arps1.Org_Party_Seq_Num =  arp1.Org_Party_Seq_Num
		And arps1.Distribution_Channel_Cd =  arp1.Distribution_Channel_Cd
		And arps1.Price_List_Cd =  arp1.Price_List_Cd
		And arps1.Article_Seq_Num =  arp1.Article_Seq_Num
		And arps1.Retail_Price_UOM_Cd =  arp1.Retail_Price_UOM_Cd
		And arps1.Valid_From_Dt =  arp1.Valid_From_Dt
		And arps1.Retail_Price_Doc_Version_Num =  arp1.Retail_Price_Doc_Version_Num 
	/* get the last changed row, for backwards compatibility ignoring Retail_Price_UOM_Cd */
	Qualify  Rank() Over(Partition by poa3.Campaign_Promotion_Start_Dt, poa3.Promotion_Offer_Seq_Num, poa3.Article_Seq_Num, poa3.Concept_Cd  Order by arps1.Valid_From_Dt Desc) = 1
) as po
/* Group by to get the lowest price per Concept_Cd, Concept_Top_Cd */
Group by 
 po.Promotion_Offer_Seq_Num
 ,po.Article_Seq_Num
 ,po.Concept_Cd
 ,po.Concept_Top_Cd
 ,po.Promotion_Offer_Start_Dt
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.PROMO_ART_CNCPT_RETAIL_PRICE_F IS '$Revision: 28850 $ - $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
