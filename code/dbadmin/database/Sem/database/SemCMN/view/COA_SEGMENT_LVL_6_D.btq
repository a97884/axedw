/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: COA_SEGMENT_LVL_6_D.btq 21456 2017-02-13 10:11:16Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2017-02-13 11:11:16 +0100 (mån, 13 feb 2017) $
# Last Revision    : $Revision: 21456 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/COA_SEGMENT_LVL_6_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVIN	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVIN.COA_SEGMENT_LVL_6_D
(
	COA_Segment_Hierarchy_Id		
	, COA_Seg_Lvl_0_Node_Seq_Num
	, COA_Seg_Lvl_1_Node_Seq_Num
	, COA_Seg_Lvl_2_Node_Seq_Num
	, COA_Seg_Lvl_3_Node_Seq_Num
	, COA_Seg_Lvl_4_Node_Seq_Num
	, COA_Seg_Lvl_5_Node_Seq_Num
	, COA_Seg_Lvl_6_Node_Seq_Num
	, COA_Seg_Lvl_6_Node_Id
	, COA_Seg_Lvl_6_Node_Name
)
as
Lock Row For Access
select 
	lvl0.N_Segment_Hierarchy_Id as COA_Segment_Hierarchy_Id		
	, lvl0.COA_Code_Segment_Seq_Num	 as COA_Seg_Lvl_0_Node_Seq_Num
	, Coalesce( lvl0hier.COA_Code_Segment_Seq_Num,  lvl0.COA_Code_Segment_Seq_Num) as COA_Seg_Lvl_1_Node_Seq_Num
	, Coalesce( lvl1hier.COA_Code_Segment_Seq_Num, lvl0hier.COA_Code_Segment_Seq_Num,  lvl0.COA_Code_Segment_Seq_Num) as COA_Seg_Lvl_2_Node_Seq_Num
	, Coalesce( lvl2hier.COA_Code_Segment_Seq_Num,lvl1hier.COA_Code_Segment_Seq_Num, lvl0hier.COA_Code_Segment_Seq_Num,  lvl0.COA_Code_Segment_Seq_Num)  as COA_Seg_Lvl_3_Node_Seq_Num
	, Coalesce( lvl3hier.COA_Code_Segment_Seq_Num,lvl2hier.COA_Code_Segment_Seq_Num,lvl1hier.COA_Code_Segment_Seq_Num, lvl0hier.COA_Code_Segment_Seq_Num,  lvl0.COA_Code_Segment_Seq_Num)  as COA_Seg_Lvl_4_Node_Seq_Num
	, Coalesce( lvl4hier.COA_Code_Segment_Seq_Num,lvl3hier.COA_Code_Segment_Seq_Num,lvl2hier.COA_Code_Segment_Seq_Num,lvl1hier.COA_Code_Segment_Seq_Num, lvl0hier.COA_Code_Segment_Seq_Num,  lvl0.COA_Code_Segment_Seq_Num)  as COA_Seg_Lvl_5_Node_Seq_Num
	, Coalesce( lvl5hier.COA_Code_Segment_Seq_Num,lvl4hier.COA_Code_Segment_Seq_Num,lvl3hier.COA_Code_Segment_Seq_Num,lvl2hier.COA_Code_Segment_Seq_Num,lvl1hier.COA_Code_Segment_Seq_Num, lvl0hier.COA_Code_Segment_Seq_Num,  lvl0.COA_Code_Segment_Seq_Num)  as COA_Seg_Lvl_6_Node_Seq_Num
	, lvlXdet.COA_Code_Segment_Node as COA_Seg_Lvl_6_Node_Id
	, lvlXdet.COA_Code_Segment_Name as COA_Seg_Lvl_6_Node_Name
/* get top level profit center segment */	
from ${DB_ENV}TgtVOUT.COA_CODE_SEGMENT lvl0
inner join ${DB_ENV}TgtVOUT.COA_Code_Segment_DETAILS_B  lvl0det
	on lvl0det.COA_Code_Segment_Seq_Num = lvl0.COA_Code_Segment_Seq_Num
	and lvl0det.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'			
left outer  join ${DB_ENV}TgtVOUT.COA_Segment_HIERARCHY lvl0hier
	on lvl0hier.Rel_COA_CODE_Segment_Seq_Num = lvl0.COA_Code_Segment_Seq_Num
	and lvl0hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'			
left outer  join ${DB_ENV}TgtVOUT.COA_Segment_HIERARCHY lvl1hier
	on lvl1hier.Rel_COA_CODE_Segment_Seq_Num = lvl0hier.COA_Code_Segment_Seq_Num
	and lvl1hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'			
left outer  join ${DB_ENV}TgtVOUT.COA_Segment_HIERARCHY lvl2hier
	on lvl2hier.Rel_COA_CODE_Segment_Seq_Num = lvl1hier.COA_Code_Segment_Seq_Num
	and lvl2hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'			
left outer  join ${DB_ENV}TgtVOUT.COA_Segment_HIERARCHY lvl3hier
	on lvl3hier.Rel_COA_CODE_Segment_Seq_Num = lvl2hier.COA_Code_Segment_Seq_Num
	and lvl3hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'			
left outer  join ${DB_ENV}TgtVOUT.COA_Segment_HIERARCHY lvl4hier
	on lvl4hier.Rel_COA_CODE_Segment_Seq_Num = lvl3hier.COA_Code_Segment_Seq_Num
	and lvl4hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'		
left outer  join ${DB_ENV}TgtVOUT.COA_Segment_HIERARCHY lvl5hier
	on lvl5hier.Rel_COA_CODE_Segment_Seq_Num = lvl4hier.COA_Code_Segment_Seq_Num
	and lvl5hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'		
	/* Get Profit Center Segment Details */
inner join ${DB_ENV}TgtVOUT.COA_Code_Segment_DETAILS_B  lvlXdet
	on lvlXdet.COA_Code_Segment_Seq_Num = Coalesce( lvl5hier.COA_Code_Segment_Seq_Num, lvl4hier.COA_Code_Segment_Seq_Num, lvl3hier.COA_Code_Segment_Seq_Num, lvl2hier.COA_Code_Segment_Seq_Num,lvl1hier.COA_Code_Segment_Seq_Num, lvl0hier.COA_Code_Segment_Seq_Num,  lvl0.COA_Code_Segment_Seq_Num) 
	and lvlXdet.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'			
where lvl0det.Top_Level_Ind = 1

;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVIN.COA_SEGMENT_LVL_6_D IS '$Revision: 21456 $ - $Date: 2017-02-13 11:11:16 +0100 (mån, 13 feb 2017) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
