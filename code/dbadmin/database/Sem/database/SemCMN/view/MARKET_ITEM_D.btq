/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: MARKET_ITEM_D.btq 30012 2020-01-22 14:03:03Z  $
# Last Changed By  : $AuthOR: k9102939 $
# Last Change Date : $Date: 2020-01-22 15:03:03 +0100 (ons, 22 jan 2020) $
# Last Revision    : $Revision: 30012 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/MARKET_ITEM_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


Replace View ${DB_ENV}SemCMNVOUT.MARKET_ITEM_D 
(
	Market_Item_Seq_Num  ,
	Market_Item_Id       ,
	Source_Defining_Org  ,
	Scan_Cd				 ,
	Market_Item_Name     ,
	Market_Item_Desc     ,
	Market_Item_Super_Category_Cd ,
	Market_Item_Category_Cd ,
	Market_Item_Segment_Cd  ,
	Market_Item_Vendor_Name   ,
	Market_Item_BrAND_Name   ,
	Market_Item_Class_Cd ,
	Market_Item_Trait_NYH ,
	Market_Item_Trait_KRA ,
	Market_Item_Trait_EKO ,
	Market_Item_Trait_P_L ,
	Market_Item_Trait_GLU ,
	Market_Item_Trait_FRT ,
	Market_Item_Trait_MSC ,
	Market_Item_Trait_52W ,
	Axfood_Article_Id,
	Axfood_Article_Desc,
	Axfood_Brand_Name,
	Axfood_Article_Business_Expiry_Dt,
	Axfood_Art_Hier_Lvl_2_Seq_Num ,
	Axfood_Art_Hier_Lvl_3_Seq_Num ,
	Axfood_Art_Hier_Lvl_4_Seq_Num ,
	Axfood_Prod_Hier_Lvl1_Seq_Num ,
	Axfood_Prod_Hier_Lvl2_Seq_Num ,
	Axfood_Vendor_Id,
	Axfood_Vendor_Name
) 
AS 

Lock Row FOR Access
SELECT 
	Market_Item_Seq_Num  ,
	Market_Item_Id       ,
	Source_Defining_Org  ,
	Scan_Cd				 ,
	RTRIM(Market_Item_Name,' X')   Market_Item_Name     ,
	Market_Item_Desc     ,
	COALESCE(Market_Item_Super_Category_Cd,'ODEFINIERAD'), 
	COALESCE(Market_Item_Category_Cd,'ODEFINIERAD') ,
	COALESCE(REGEXP_REPLACE( Market_Item_Segment_Cd, '^.*-', '\1',1,0),'ODEFINIERAD')   Market_Item_Segment_Cd  ,
	COALESCE(Market_Item_Vendor_Name,'ODEFINIERAD')   ,
	COALESCE(Market_Item_BrAND_Name,'ODEFINIERAD')   ,
	COALESCE(Market_Item_Class_Cd,'EJ EMV') ,
	COALESCE(Market_Item_Trait_NYH,'EJ NYCKELHÅLSMÄRKT') ,
	COALESCE(Market_Item_Trait_KRA,'EJ EKOLOGISKT') ,
	COALESCE(Market_Item_Trait_EKO,'EJ EKO') ,
	COALESCE(Market_Item_Trait_P_L,'EJ PRIVATE LABEL') ,
	COALESCE(Market_Item_Trait_GLU,'EJ GLUTENFRI') ,
	COALESCE(Market_Item_Trait_FRT,'NON FAIRTRADE') ,
	COALESCE(Market_Item_Trait_MSC,'EJ MSC') ,
	COALESCE(Market_Item_Trait_52W,'YES') ,
	COALESCE(Axfood_Article_Id, '-1'),
	COALESCE(Axfood_Article_Desc, 'Saknas'),
	COALESCE(Axfood_Brand_Name, 'Saknas'),
	COALESCE(Axfood_Article_Business_Expiry_Dt, DATE '9999-12-31')
	Axfood_Article_Business_Expiry_Dt,
	COALESCE(CASE WHEN Axfood_Art_Hier_Lvl_2_Seq_Num = -1 AND micrd.Art_Hier_Lvl_2_Seq_Num IS NOT NULL 
		THEN micrd.Art_Hier_Lvl_2_Seq_Num ELSE Axfood_Art_Hier_Lvl_2_Seq_Num END, -1) AS Axfood_Art_Hier_Lvl_2_Seq_Num,
	COALESCE(CASE WHEN Axfood_Art_Hier_Lvl_3_Seq_Num = -1 AND micrd.Art_Hier_Lvl_3_Seq_Num IS NOT NULL 
		THEN micrd.Art_Hier_Lvl_3_Seq_Num ELSE Axfood_Art_Hier_Lvl_3_Seq_Num END, -1) AS Axfood_Art_Hier_Lvl_3_Seq_Num,
	COALESCE(CASE WHEN Axfood_Art_Hier_Lvl_4_Seq_Num = -1 AND micrd.Art_Hier_Lvl_4_Seq_Num IS NOT NULL 
		THEN micrd.Art_Hier_Lvl_4_Seq_Num ELSE Axfood_Art_Hier_Lvl_4_Seq_Num END, -1) AS Axfood_Art_Hier_Lvl_4_Seq_Num,
	COALESCE(l2.Prod_Hier_Lvl1_Seq_Num, COALESCE(Axfood_Prod_Hier_Lvl1_Seq_Num,-1)) As Axfood_Prod_Hier_Lvl1_Seq_Num ,
	COALESCE(l2.Prod_Hier_Lvl2_Seq_Num, COALESCE(Axfood_Prod_Hier_Lvl2_Seq_Num,-1)) As Axfood_Prod_Hier_Lvl2_Seq_Num , 
	COALESCE(Axfood_Vendor_Id, '-1'),
	COALESCE(Axfood_Vendor_Name, 'Saknas i SAP Masterdata')	
FROM (
	SELECT 
		  mi.Market_Item_Seq_Num 
		, mi.N_Market_Item_Id As Market_Item_Id
		, mi.Market_Item_Def_Party As Source_Defining_Org
		, COALESCE(scd.Scan_Cd, -1) As Scan_Cd
		, midb.Item_Name As Market_Item_Name
		, midb.Item_Desc As Market_Item_Desc
		, MAX(misg.Market_Item_Super_GROUP_Cd) As Market_Item_Super_Category_Cd 	--SUPERIOR Category
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'Category' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Category_Cd	--Category
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'SEGMENT' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Segment_Cd	--AXFOOD_SEGMENT 		
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'MANUFACTURER' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Vendor_Name	--MANUFACTURER 	
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'BRAND' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_BrAND_Name	--BRAND 	
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'KLASS' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Class_Cd	--KLASS 	
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'NYCKELHÅLSMÄRKT' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_NYH	--NYCKELHALSMARKT 		
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'KRAV/ÖVRIGT EKO' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_KRA	--KRAV_OVRIGT_EKO 	
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'TOTALT EKOLOGISKT' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_EKO	--TOTALT_EKOLOGISKT 		
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'PRIVATE LABEL' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_P_L	--PRIVATE_LABEL 	
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'GLUTEN' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_GLU	--GLUTEN 	
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'FAIRTRADE' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_FRT	--FAIRTRADE 	
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'MSC/EJ MSC' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_MSC	--MSC 		
		, MAX(CASE WHEN mic.Market_Item_Trait_GROUP_Cd = 'ACTIVE(52,WEEKS)' 
			THEN mic.Market_Item_Trait_Cd ELSE Null End) As Market_Item_Trait_52W	--ACTIVE_52_WEEKS 	
		, ad.Article_Id As Axfood_Article_Id
		, ad.Article_Desc As Axfood_Article_Desc
		, ad.Brand_Name As Axfood_Brand_Name
		, ad.Article_Business_Expiry_Dt As Axfood_Article_Business_Expiry_Dt
		, COALESCE(ad.Art_Hier_Lvl_2_Seq_Num,-1) as Axfood_Art_Hier_Lvl_2_Seq_Num 
		, COALESCE(ad.Art_Hier_Lvl_3_Seq_Num,-1) as Axfood_Art_Hier_Lvl_3_Seq_Num 
		, COALESCE(ad.Art_Hier_Lvl_4_Seq_Num,-1) as Axfood_Art_Hier_Lvl_4_Seq_Num 
		, COALESCE(agsp.Parent_Article_GROUP_Seq_Num,-1) as Axfood_Prod_Hier_Lvl1_Seq_Num 
		, COALESCE(ags.Parent_Article_GROUP_Seq_Num,-1) As Axfood_Prod_Hier_Lvl2_Seq_Num 
		, COALESCE(ad.Prod_Hier_Lvl3_Seq_Num,-1) as Axfood_Prod_Hier_Lvl3_Seq_Num 
		, ven.Vendor_Id As Axfood_Vendor_Id
		, ven.Vendor_Name As Axfood_Vendor_Name
	FROM ${DB_ENV}TgtVout.MARKET_ITEM mi
	INNER JOIN ${DB_ENV}TgtVout.MARKET_ITEM_XREF mixr 
		ON mixr.Market_Child_Item_Seq_Num = mi.Market_Item_Seq_Num
	INNER JOIN ${DB_ENV}TgtVout.MARKET_ITEM_DETAILS_B midb 
		ON midb.Market_Item_Seq_Num = mi.Market_Item_Seq_Num
	LEFT OUTER JOIN ${DB_ENV}TgtVout.MARKET_ITEM_TRAITS_B mic 
		ON mic.Market_Item_Seq_Num = mi.Market_Item_Seq_Num 
		AND mic.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
	LEFT OUTER JOIN ${DB_ENV}TgtVout.MARKET_ITEM_SUPER_GROUP misg 
		ON misg.Market_Item_Trait_GROUP_Cd = mic.Market_Item_Trait_GROUP_Cd 
		AND misg.Market_Item_Trait_Cd = mic.Market_Item_Trait_Cd
	LEFT OUTER JOIN ${DB_ENV}TgtVout.MARKET_ITEM_SCAN_CODE misc 
		ON misc.Market_Item_Seq_Num = mi.Market_Item_Seq_Num 
		AND misc.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
	LEFT OUTER JOIN ${DB_ENV}SemCmnT.SCAN_CODE_D scd 
		ON misc.Scan_Code_Seq_Num = scd.Scan_Code_Seq_Num 
	LEFT OUTER JOIN ${DB_ENV}SemCmnT.ARTICLE_D ad 
		ON scd.Article_Seq_Num = ad.Article_Seq_Num 
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_GROUP_STRUCTURE ags 
		ON ad.Prod_Hier_Lvl3_Seq_Num = ags.Article_GROUP_Seq_Num 
		AND ags.Article_GROUP_Level_Num = 3 
		AND ags.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_GROUP_STRUCTURE agsp 
		ON ags.Parent_Article_GROUP_Seq_Num = agsp.Article_GROUP_Seq_Num 
		AND agsp.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
	LEFT OUTER JOIN ${DB_ENV}SemCmnVouT.Vendor_D ven
	ON ad.Vendor_Seq_Num = ven.Vendor_Seq_Num	
	WHERE mi.Market_Item_Def_Party = 'Nielsen'
	AND midb.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
	GROUP BY 	mi.Market_Item_Seq_Num 
		, mi.N_Market_Item_Id 
		, mi.Market_Item_Def_Party 
		, scd.Scan_Cd
		, midb.Item_Name 
		, midb.Item_Desc
		, ad.Article_Id  
		, ad.Article_Desc  
		, ad.Brand_Name  
		, ad.Article_Business_Expiry_Dt
		, COALESCE(ad.Art_Hier_Lvl_2_Seq_Num,-1) 
		, COALESCE(ad.Art_Hier_Lvl_3_Seq_Num,-1) 
		, COALESCE(ad.Art_Hier_Lvl_4_Seq_Num,-1) 
		, COALESCE(agsp.Parent_Article_GROUP_Seq_Num,-1) 
		, COALESCE(ags.Parent_Article_GROUP_Seq_Num,-1) 
		, COALESCE(ad.Prod_Hier_Lvl3_Seq_Num,-1)  
		, ven.Vendor_Id  
		, ven.Vendor_Name 
) T
LEFT OUTER JOIN ${DB_ENV}SemCmnVOUT.MARKET_ITEM_CLASS_RANK_D micrd 
	ON micrd.Market_Item_Trait_Cd = T.Market_Item_Segment_Cd
LEFT OUTER JOIN ${DB_ENV}SemCmnVout.PROD_HIER_LVL_2_D l2 
	ON l2.Prod_Hier_Lvl2_Id = 
	CASE 
		WHEN Market_Item_Class_Cd = 'EJ EMV' THEN '0000000000'
		WHEN Market_Item_Class_Cd = 'MERVARDE' THEN '0000100010'
		WHEN Market_Item_Class_Cd = 'STANDARD' THEN '0000100020'
		WHEN Market_Item_Class_Cd = 'LAGPRIS' THEN '0000100030'
		ELSE -1		
	END
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.MARKET_ITEM_D IS '$Revision: 30012 $ - $Date: 2020-01-22 15:03:03 +0100 (ons, 22 jan 2020) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
