/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STORE_LOCAL_SCAN_CODE_D.btq 26146 2018-10-23 08:29:53Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-10-23 10:29:53 +0200 (tis, 23 okt 2018) $
# Last Revision    : $Revision: 26146 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/STORE_LOCAL_SCAN_CODE_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/


Database ${DB_ENV}SemCMNVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.STORE_LOCAL_SCAN_CODE_D
(
Scan_Code_Seq_Num,
store_Seq_Num,
Store_Id,
Store_Name,
Concept_Cd,
Scan_Cd,
Scan_Code_Desc,
Art_Hier_Lvl_2_Id,
Art_Hier_Lvl_2_Desc,
Art_Hier_Lvl_3_Id,
Art_Hier_Lvl_3_Desc,
Art_Hier_Lvl_4_Id,
Art_Hier_Lvl_4_Desc
)
As
SELECT 
mu2.Scan_code_seq_num
,s1.store_Seq_Num
,s1.Store_Id
,s1.Store_Name
,s1.Concept_Cd
,mu2.N_Scan_Cd as Scan_Cd
,coalesce(sd.MU_Scan_Code_Desc,'Beskrivning saknas') as Scan_Code_Desc
,ah2.Art_Hier_Lvl_2_Id
,ah2.Art_Hier_Lvl_2_Desc
,ah3.Art_Hier_Lvl_3_Id
,ah3.Art_Hier_Lvl_3_Desc
,ah4.Art_Hier_Lvl_4_Id
,ah4.Art_Hier_Lvl_4_Desc

FROM  ${DB_ENV}TgtVOUT.MU_SCAN_CODE as mu2

INNER JOIN  ${DB_ENV}TgtVOUT.ARTICLE_AH_ALLOC_LOCAL alloc
ON alloc.Scan_Code_Seq_Num = mu2.scan_code_seq_num
and alloc.ah_level_num_DD = 4
AND alloc.Valid_To_Dttm = '9999-12-31 23:59:59.999999'

INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D as ah4
on alloc.Node_Seq_Num = ah4.Art_Hier_Lvl_4_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_3_D as ah3
on ah4.Art_Hier_Lvl_3_Seq_Num = ah3.Art_Hier_Lvl_3_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D as ah2
on ah3.Art_Hier_Lvl_2_Seq_Num = ah2.Art_Hier_Lvl_2_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.STORE_D as s1
ON s1.store_Seq_Num= alloc.Hier_Defining_Loc_Seq_Num

LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.MU_SCAN_CODE_LOC_DESC_B sd
ON sd.scan_code_seq_num = mu2.Scan_Code_Seq_Num
and sd.Location_Seq_Num = alloc.Hier_Defining_Loc_Seq_Num
and sd.Valid_To_Dttm = '9999-12-31 23:59:59.999999'

LEFT OUTER JOIN (SELECT	Scan_Code_Seq_Num FROM ${DB_ENV}SemCMNVOUT.SCAN_CODE_D where Article_Seq_Num <> -1) As scm
on mu2.Scan_Code_Seq_Num = scm.Scan_Code_Seq_Num

where scm.Scan_Code_Seq_Num is null
;  

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.STORE_LOCAL_SCAN_CODE_D IS '$Revision: 26146 $ - $Date: 2018-10-23 10:29:53 +0200 (tis, 23 okt 2018) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
