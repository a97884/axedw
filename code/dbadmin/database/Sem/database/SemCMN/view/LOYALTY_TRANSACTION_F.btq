/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_TRANSACTION_F.btq 32665 2020-06-22 13:22:49Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-22 15:22:49 +0200 (mån, 22 jun 2020) $
# Last Revision    : $Revision: 32665 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_TRANSACTION_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# ---------------------------------------------------------------------------
*/


REPLACE VIEW ${DB_ENV}SemCMNVOUT.LOYALTY_TRANSACTION_F
(	Loyalty_Transaction_Id
	,Tran_Type_Cd
	,Sales_Tran_Seq_Num
	,Loyalty_Program_Seq_Num
	,Contact_Account_Seq_Num
	,Member_Account_Seq_Num
	,Partner_Seq_Num
	,Store_Seq_Num
	,Transaction_Dt
	,Tran_Status_Cd
	,Loyalty_Trans_Sub_Type_Cd
	,Loyalty_Trans_Channel_Cd
	,SRC_Siebel_Product_Id
	,Transaction_Num
	,Processing_Dt
	,Sales_Trans_Dt
	,Expiration_Dt
	,Sales_Transaction_Type_Cd
	,Sales_Associate_Id
	,Total_Loyalty_Amt
	,Bonus_Applicable_Amt
	,Employee_Discount_Amt
	,Member_Discount_Amt
	,General_Discount_Amt
	,Post_Registered_Ind
	,Offline_Transaction_Ind
	,Downgraded_Amt
	,Purchase_Amt
	,Expired_Amt
	,Remittance_Amt
	,Accrual_Amt 
	--,Grand_Amt ------------HOW IS THIS SUPPOSED TO BE CALCULATED?
) AS
SELECT	ltr1.Loyalty_Trans_Id AS Loyalty_Transaction_Id
	,ltr1.Tran_Type_Cd
	,ltr1.Sales_Tran_Seq_Num
	,ltr1.Loyalty_Program_Seq_Num
	,ltr1.Contact_Account_Seq_Num
	,COALESCE(ltmh1.Member_Account_Seq_Num,-1) AS Member_Account_Seq_Num
	,COALESCE(elt1.Org_Party_Seq_Num,-1) AS Partner_Seq_Num
	,CASE WHEN ltr1.Store_Seq_Num IS NULL AND lp1.N_Loyalty_Program_Id IN ('1-CI30ORN', '1-NDZL') THEN -2 
	WHEN ltr1.Store_Seq_Num IS NULL AND lp1.N_Loyalty_Program_Id IN ('1-MQ7V', '1-UUDVTCN') THEN -12 
	WHEN ltr1.Store_Seq_Num IS NULL AND lp1.N_Loyalty_Program_Id NOT IN ('1-CI30ORN', '1-NDZL', '1-MQ7V', '1-UUDVTCN') THEN -1 
	WHEN ltr1.Store_Seq_Num IS NULL AND lp1.N_Loyalty_Program_Id IS NULL THEN -1
	ELSE ltr1.Store_Seq_Num END AS Store_Seq_Num
	,CAST(ltr1.Transaction_Dt AS DATE) AS Transaction_Dt
	,ltr1.Tran_Status_Cd
	,ltr1.Loyalty_Tran_Sub_Type_Cd AS Loyalty_Trans_Sub_Type_Cd
	,ltr1.Enrollment_Channel_Cd
	,ltr1.SRC_Siebel_Product_Id
	,ltr1.Transaction_Num
	,CAST(ltr1.Processing_Dt AS DATE) AS Processing_Dt
	,CAST(ltr1.Sales_Trans_Dt AS DATE) AS Sales_Trans_Dt
	,lri1.Expiration_Dt
	,ltr1.Sales_Transaction_Type_Cd
	,Coalesce(st1.Sales_Associate_Id, -1) As Sales_Associate_Id
	,ltr1.Total_Loyalty_Amt
	,CASE WHEN ltr1.Loyalty_Tran_Sub_Type_Cd = 'Purchase' 
		THEN ltr1.Bonus_Applicable_Amt ELSE NULL END	AS Bonus_Applicable_Amt
	,ltr1.Employee_Discount_Amt
	,ltr1.Member_Discount_Amt
	,ltr1.General_Discount_Amt
	,ltr1.Post_Registered_Ind
	,ltr1.Offline_Transaction_Ind
	,lri1.Downgraded_Amt
	,lri1.Purchase_Amt
	,lri1.Expired_Amt
	,lri1.Remittance_Amt
	,lai1.Accrual_Amt
FROM ${DB_ENV}TgtVOUT.LOYALTY_TRANSACTION AS ltr1

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOYALTY_PROGRAM lp1
ON ltr1.Loyalty_Program_Seq_Num = lp1.Loyalty_Program_Seq_Num

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.EXTERNAL_LOYALTY_TRANSACTION AS elt1
	ON ltr1.Loyalty_Trans_Id = elt1.Loyalty_Trans_Id
	
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED AS st1
	ON ltr1.Sales_Tran_Seq_Num = st1.Sales_Tran_Seq_Num	
	And ltr1.Store_Seq_Num = st1.Store_Seq_Num
	And ltr1.Sales_Trans_Dt = st1.Tran_Dt_DD

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOYALTY_TRAN_MEMBER_HIST AS ltmh1
	ON ltr1.Loyalty_Trans_Id = ltmh1.Loyalty_Trans_Id
	AND ltmh1.Valid_To_Dttm = SYSLIB.HighTSVal()

LEFT OUTER JOIN (
	SELECT	lri0.Loyalty_Trans_Id
		,MAX(CASE WHEN lri0.Redemption_Type_Cd = 'Expired'
			THEN CAST(lai_exp0.Expiration_Dt AS DATE) ELSE NULL END) AS Expiration_Dt
		,SUM(CASE WHEN lri0.Redemption_Type_Cd = 'Downgrade'
			THEN lri0.Redemption_Amt ELSE NULL END) AS Downgraded_Amt
		,SUM(CASE WHEN lri0.Redemption_Type_Cd = 'Purchase'
			THEN lri0.Redemption_Amt ELSE NULL END) AS Purchase_Amt
		,SUM(CASE WHEN lri0.Redemption_Type_Cd = 'Expired'
			THEN lri0.Redemption_Amt ELSE NULL END) AS Expired_Amt
		,SUM(CASE WHEN lri0.Redemption_Type_Cd = 'Remittance'
			THEN lri0.Redemption_Amt ELSE NULL END) AS Remittance_Amt
	FROM ${DB_ENV}TgtVOUT.LOYALTY_REDEMPTION_ITEM AS lri0
	INNER JOIN ${DB_ENV}TgtVOUT.LOY_ATTIBUTE_DEFINTION_B AS lad_lri0
		ON lad_lri0.Loyalty_Attribute_Seq_Num = lri0.Loyalty_Attribute_Seq_Num
		AND lad_lri0.Internal_Field_Name = 'Point 1 Value'
		AND lad_lri0.Valid_To_Dttm = SYSLIB.HighTSVal()
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOYALTY_ACCRUAL_ITEM AS lai_exp0 -- to get Expiration_Dt for the expired amount
		ON lai_exp0.Loyalty_Accrual_Id = lri0.Loyalty_Accrual_Id
		AND lri0.Redemption_Type_Cd = 'Expired'
	WHERE lri0.Redemption_Type_Cd IN ('Downgrade','Purchase','Expired','Remittance')
	GROUP BY 1
	) AS lri1
	ON lri1.Loyalty_Trans_Id = ltr1.Loyalty_Trans_Id
	
LEFT OUTER JOIN (
	SELECT	lai0.Loyalty_Trans_Id
		,SUM(lai0.Accrual_Amt) AS Accrual_Amt
	FROM ${DB_ENV}TgtVOUT.LOYALTY_ACCRUAL_ITEM AS lai0
	INNER JOIN ${DB_ENV}TgtVOUT.LOY_ATTIBUTE_DEFINTION_B AS lad_lai0
		ON lad_lai0.Loyalty_Attribute_Seq_Num = lai0.Loyalty_Attribute_Seq_Num
		AND lad_lai0.Internal_Field_Name = 'Point 1 Value'
		AND lad_lai0.Valid_To_Dttm = SYSLIB.HighTSVal()
	GROUP BY 1
	) AS lai1
	ON lai1.Loyalty_Trans_Id = ltr1.Loyalty_Trans_Id
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_TRANSACTION_F IS '$Revision: 32665 $ - $Date: 2020-06-22 15:22:49 +0200 (mån, 22 jun 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
