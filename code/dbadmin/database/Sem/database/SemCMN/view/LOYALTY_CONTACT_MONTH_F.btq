/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_CONTACT_MONTH_F.btq 11817 2013-12-15 19:14:54Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-12-15 20:14:54 +0100 (sön, 15 dec 2013) $
# Last Revision    : $Revision: 11817 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_CONTACT_MONTH_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.LOYALTY_CONTACT_MONTH_F
(
	Member_Account_Seq_Num,
	Contact_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Month_Id,
	Home_Store_Seq_Num,
	Contact_Ind,
	Active_Contact_Ind,
	Contact_Cnt,
	Contact_Sum_Cnt,
	Contact_Ord_Cnt,
	Contact_Period_Cnt
)
As
Lock row for access
Select
	Member_Account_Seq_Num,
	Contact_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Month_Id,
	Month_Home_Store_Seq_Num,
	
	Max(Contact_Ind) as Contact_Ind,
	Max(Active_Contact_Ind) as Active_Contact_Ind,
	
	Contact_Cnt,
	Contact_Sum_Cnt,
	Contact_Ord_Cnt_loc,
	Contact_Period_Cnt_loc
From
(
	Select 
		lmd.Member_Account_Seq_Num,
		lmd.Contact_Account_Seq_Num,
		lmd.Loyalty_Program_Seq_Num,
		lmd.Calendar_Month_Id,
		lmd.Month_Home_Store_Seq_Num,
		lmd.Active_Contact_Ind,
		lmd.Contact_Ind,

		Count(lmd.Contact_Account_Seq_Num) as Contact_Cnt,
		Cast(1 as INTEGER) as Contact_Sum_Cnt,

		Rank() over (partition by 
			lmd.Member_Account_Seq_Num,
			lmd.Contact_Account_Seq_Num,
			lmd.Calendar_Month_Id
		order by
			lmd.Member_Account_Seq_Num +
			lmd.Contact_Account_Seq_Num +
			lmd.Loyalty_Program_Seq_Num +
			lmd.Calendar_Month_Id +
			lmd.Month_Home_Store_Seq_Num +
			lmd.Contact_Ind +
			lmd.Active_Contact_Ind
		desc
		) as Contact_Ord_Cnt_loc,
		Case when Contact_Ord_Cnt_loc = 1 Then 1 else 0 end as Contact_Period_Cnt_loc

	From ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F as lmd

	group by 
		lmd.Member_Account_Seq_Num,
		lmd.Contact_Account_Seq_Num,
		lmd.Loyalty_Program_Seq_Num,
		lmd.Calendar_Month_Id,
		lmd.Month_Home_Store_Seq_Num,
		lmd.Contact_Ind,
		lmd.Active_Contact_Ind
) as a
Group by
	Member_Account_Seq_Num,
	Contact_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Month_Id,
	Month_Home_Store_Seq_Num,
	
	Contact_Cnt,
	Contact_Sum_Cnt,
	Contact_Ord_Cnt_loc,
	Contact_Period_Cnt_loc
Where Contact_Ord_Cnt_loc = 1 -- show only the row with most ones
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','LOYALTY_CONTACT_MONTH_F','POST','I',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_CONTACT_MONTH_F IS '$Revision: 11817 $ - $Date: 2013-12-15 20:14:54 +0100 (sön, 15 dec 2013) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
