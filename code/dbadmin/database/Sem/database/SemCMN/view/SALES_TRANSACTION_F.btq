/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRANSACTION_F.btq 29912 2020-01-16 16:39:25Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 17:39:25 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29912 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/SALES_TRANSACTION_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_F
(	
	Sales_Tran_Seq_Num,
	Sales_Tran_Id,
	Store_Seq_Num,
	Junk_Seq_Num,
	Tran_Dt,
	Tran_End_Dttm,
	Tran_Tm,
	Hour_Of_Day,
	Store_Department_Seq_Num,
	Customer_Seq_Num,
	Loyalty_Account_Id,
	Contact_Account_Seq_Num,
	Loyalty_Identifier_Seq_Num,
	Receipt_Type_Cd,
	Delivery_Method_Cd,
	POS_Register_Seq_Num,
	Sales_Associate_Id,
	Reported_As_Dttm,
	Training_Mode_Status,
	Related_Sales_Tran_Seq_Num,
	Submit_Dttm,
	Empl_Sales_Ind,
	Target_Ind,
	Return_Ind,
	Suspended_Ind,
	Suspended_EOD_Ind,
	Resumed_Ind,
	Voided_Ind,
	Receipt_Cnt,
	OpenJobRunId,
	Insert_Dttm,
	Contact_Insert_Dttm
) As 
	Select	
	st1.Sales_Tran_Seq_Num,
	st1.N_Sales_Tran_Id As Sales_Tran_Id,
	st1.Store_Seq_Num,
	j1.Junk_Seq_Num,
	st1.Tran_Dt_DD As Tran_Dt,
	st1.Tran_End_Dttm_DD As Tran_End_Dttm,
	CAST(st1.Tran_End_Dttm_DD As TIME(6)) As Tran_Tm,
	CAST(CAST(Tran_Tm As Char(2))||':00:00' As Time(0)) as Hour_Of_Day,
	st1.Store_Department_Seq_Num,
	Coalesce(stp.Party_Seq_Num,-1) As Customer_Seq_Num,
	Coalesce(stla.Loyalty_Account_Id,'-1') As Loyalty_Account_Id,
	Coalesce(stlc.Contact_Account_Seq_Num,-1) As Contact_Account_Seq_Num,
	Coalesce(stlim.Loyalty_Identifer_Seq_Num,-1) As Loyalty_Identifier_Seq_Num, --FELSTAVAT Identifer
	Coalesce(stc.Circumstance_Value_Cd,'-1')	As Receipt_Type_Cd, /* Provar med outer join för prestanda  */
	Coalesce(stdlmd.Circumstance_Value_Cd,'-1') As Delivery_Method_Cd,
	st1.POS_Register_Seq_Num,
	st1.Sales_Associate_Id,
	st1.Reported_As_Dttm,
	st1.Training_Mode_Status,
	st1.Related_Sales_Tran_Seq_Num,
	st1.Submit_Dttm,
	Case  
		When stt1.Sales_Tran_Seq_Num Is Not Null Then 1
		Else 0 
	End As Empl_Sales_Ind,
	1 (SMALLINT) As Target_Ind,
	Case	
		When stl2.Sales_Tran_Seq_Num Is Not Null Then 1
		Else 0 
	End As Return_Ind						/* De kvitton som har returer */,
	0 (SMALLINT) As Suspended_Ind			/*  Alla  kvitton hör till ej Parkerade kvitton*/,
	0 (SMALLINT) As Suspended_EOD_Ind		/*  Alla kvitton hör till ej Parkerade kvitton*/,
	Case	
		When  st1.Tran_Status_Cd = 'CT' 
		And st1.Related_Sales_Tran_Seq_Num Is Not Null Then 1
		Else 0 
	End As Resumed_Ind				/* återupptaget kvitto*. Tillagd av Fraud */,
	Case	
		When stlc2.Sales_Tran_Seq_Num Is Not Null
		Then 1
		Else 0 
	End As Voided_Ind				/* Kvitton med korrigeringar. Tillagd av Fraud */,
	Case	
		When Tran_Status_Cd = 'VO' Then -1 
		When	Tran_Status_Cd = 'CT' Then 1
		Else	0 
	End	As Receipt_Cnt,
	st1.OpenJobRunId,
	st1.InsertDttm,
	stlc.InsertDttm Contact_Insert_Dttm
	From ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED As st1
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As stt1
	On st1.Sales_Tran_Seq_Num = stt1.Sales_Tran_Seq_Num
	And st1.Store_Seq_Num = stt1.Store_Seq_Num 
	And st1.Tran_Dt_DD = stt1.Tran_Dt_DD
	And stt1.Tran_Type_Cd = 'PD' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY As stp
 	On st1.Sales_Tran_Seq_Num = stp.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stp.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stp.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOYALTY_ACCOUNT As stla
 	On st1.Sales_Tran_Seq_Num = stla.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stla.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stla.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As stlc 
	On	st1.Sales_Tran_Seq_Num = stlc.Sales_Tran_Seq_Num
	And	st1.Store_Seq_Num = stlc.Store_Seq_Num 
	And	st1.Tran_Dt_DD = stlc.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_ID_METHOD As stlim 
	On	st1.Sales_Tran_Seq_Num = stlim.Sales_Tran_Seq_Num
	And	st1.Store_Seq_Num = stlim.Store_Seq_Num 
	And	st1.Tran_Dt_DD = stlim.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stc
 	On st1.Sales_Tran_Seq_Num = stc.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stc.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stc.Tran_Dt_DD
 	And stc.Circumstance_Cd = 'COTP'
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stdlmd
 	On st1.Sales_Tran_Seq_Num = stdlmd.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stdlmd.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stdlmd.Tran_Dt_DD
 	And stdlmd.Circumstance_Cd = 'DLMD'	
 LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
	On st1.Store_Seq_Num = std1.Store_Seq_Num and st1.Tran_Dt_DD = std1.Calendar_Dt
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
	on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
	and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
	and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
	and j1.Loyalty_Ind = 0		

Left Outer Join (
	Select 
		stlc1.Sales_Tran_Seq_Num,
		stlc1.Store_Seq_Num,
		stlc1.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRAN_LINE_CIRCUMSTANCE As stlc1
		Where 	stlc1.Circumstance_Cd='VOID' 
		And	stlc1.Circumstance_Value_Cd In ('PA','LI') 
		Group by 
		stlc1.Sales_Tran_Seq_Num,
		stlc1.Store_Seq_Num,
		stlc1.Tran_Dt_DD
		) 	stlc2
	On	st1.Sales_Tran_Seq_Num = stlc2.Sales_Tran_Seq_Num
	And	st1.Store_Seq_Num =  stlc2.Store_Seq_Num
	And	st1.Tran_Dt_DD = stlc2.Tran_Dt_DD
Left Outer Join (
	Select 
		stl1.Sales_Tran_Seq_Num,
		stl1.Store_Seq_Num,
		stl1.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
	Where stl1.Tran_Line_Status_Cd = 'CT'
	And stl1.Return_Reason_Cd <> '-1'
	Group by 
		stl1.Sales_Tran_Seq_Num,
		stl1.Store_Seq_Num,
		stl1.Tran_Dt_DD) stl2		
	On st1.Sales_Tran_Seq_Num = stl2.Sales_Tran_Seq_Num
	And st1.Store_Seq_Num =  stl2.Store_Seq_Num
	And st1.Tran_Dt_DD = stl2.Tran_Dt_DD
Where st1.Tran_Type_Cd = 'POS'
 	And st1.Tran_Status_Cd in ( 'CT', 'VO')
	And (st1.Sales_Tran_Seq_Num, st1.Store_Seq_Num, st1.Tran_Dt_DD) In
	(Select 
		stl3.Sales_Tran_Seq_Num
		,stl3.Store_Seq_Num 
		,stl3.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE stl3
	Where stl3.Tran_Line_Status_Cd = 'CT');

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_F IS '$Revision: 29912 $ - $Date: 2020-01-16 17:39:25 +0100 (tor, 16 jan 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE