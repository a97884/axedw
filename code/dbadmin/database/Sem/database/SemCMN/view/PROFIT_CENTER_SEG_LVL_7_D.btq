/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PROFIT_CENTER_SEG_LVL_7_D.btq 21464 2017-02-13 13:25:11Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2017-02-13 14:25:11 +0100 (mån, 13 feb 2017) $
# Last Revision    : $Revision: 21464 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/PROFIT_CENTER_SEG_LVL_7_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVIN	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVIN.PROFIT_CENTER_SEG_LVL_7_D
(
	Segment_Hierarchy_Id		
	, Segment_Lvl_0_Node_Seq_Num
	, Segment_Lvl_1_Node_Seq_Num
	, Segment_Lvl_2_Node_Seq_Num
	, Segment_Lvl_3_Node_Seq_Num
	, Segment_Lvl_4_Node_Seq_Num
	, Segment_Lvl_5_Node_Seq_Num
	, Segment_Lvl_6_Node_Seq_Num
	, Segment_Lvl_7_Node_Seq_Num
	, Segment_Lvl_7_Node_Id
	, Segment_Lvl_7_Node_Name
)
as
Lock Row For Access
select 
	lvl0.N_Segment_Hierarchy_Id as Segment_Hierarchy_Id		
	, lvl0.Profit_Center_Seg_Seq_Num	 as Segment_Lvl_0_Node_Seq_Num
	, coalesce(lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num) as Segment_Lvl_1_Node_Seq_Num
	, coalesce(lvl1hier.Profit_Center_Seg_Seq_Num, lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num) as Segment_Lvl_2_Node_Seq_Num
	, coalesce(lvl2hier.Profit_Center_Seg_Seq_Num, lvl1hier.Profit_Center_Seg_Seq_Num, lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num) as Segment_Lvl_3_Node_Seq_Num
	, coalesce(lvl3hier.Profit_Center_Seg_Seq_Num, lvl2hier.Profit_Center_Seg_Seq_Num, lvl1hier.Profit_Center_Seg_Seq_Num, lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num) as Segment_Lvl_4_Node_Seq_Num
	, coalesce(lvl4hier.Profit_Center_Seg_Seq_Num, lvl3hier.Profit_Center_Seg_Seq_Num, lvl2hier.Profit_Center_Seg_Seq_Num, lvl1hier.Profit_Center_Seg_Seq_Num, lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num) as Segment_Lvl_5_Node_Seq_Num
	, coalesce(lvl5hier.Profit_Center_Seg_Seq_Num, lvl4hier.Profit_Center_Seg_Seq_Num, lvl3hier.Profit_Center_Seg_Seq_Num, lvl2hier.Profit_Center_Seg_Seq_Num, lvl1hier.Profit_Center_Seg_Seq_Num, lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num) as Segment_Lvl_6_Node_Seq_Num
	, coalesce(lvl6hier.Profit_Center_Seg_Seq_Num, lvl5hier.Profit_Center_Seg_Seq_Num, lvl4hier.Profit_Center_Seg_Seq_Num, lvl3hier.Profit_Center_Seg_Seq_Num, lvl2hier.Profit_Center_Seg_Seq_Num, lvl1hier.Profit_Center_Seg_Seq_Num, lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num) as Segment_Lvl_7_Node_Seq_Num
	, lvlXdet.Segment_Node as Segment_Lvl_7_Node_Id
	, lvlXdet.Segment_Name as Segment_Lvl_7_Node_Name
/* get top level profit center segment */	
from ${DB_ENV}TgtVOUT.PROFIT_CENTER_SEGMENT lvl0
inner join ${DB_ENV}TgtVOUT.PROFIT_CENTER_SEG_DETAILS_B  lvl0det
	on lvl0det.Profit_Center_Seg_Seq_Num = lvl0.Profit_Center_Seg_Seq_Num
	and lvl0det.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
left outer  join ${DB_ENV}TgtVOUT.PROFIT_CENTER_HIERARCHY lvl0hier
	on lvl0hier.Rel_Profit_Center_Seg_Seq_Num = lvl0.Profit_Center_Seg_Seq_Num
	and lvl0hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'			
/* get 1st level profit center segment */	
left outer  join ${DB_ENV}TgtVOUT.PROFIT_CENTER_HIERARCHY lvl1hier
	on lvl1hier.Rel_Profit_Center_Seg_Seq_Num = lvl0hier.Profit_Center_Seg_Seq_Num
	and lvl1hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'				
/* get 2nd level profit center segment */	
left outer  join ${DB_ENV}TgtVOUT.PROFIT_CENTER_HIERARCHY lvl2hier
	on lvl2hier.Rel_Profit_Center_Seg_Seq_Num = lvl1hier.Profit_Center_Seg_Seq_Num
	and lvl2hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'				
/* get 3rd level profit center segment */	
left outer  join ${DB_ENV}TgtVOUT.PROFIT_CENTER_HIERARCHY lvl3hier
	on lvl3hier.Rel_Profit_Center_Seg_Seq_Num = lvl2hier.Profit_Center_Seg_Seq_Num
	and lvl3hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'					
/* get 4th level profit center segment */	
left outer  join ${DB_ENV}TgtVOUT.PROFIT_CENTER_HIERARCHY lvl4hier
	on lvl4hier.Rel_Profit_Center_Seg_Seq_Num = lvl3hier.Profit_Center_Seg_Seq_Num
	and lvl4hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'					
/* get 5th level profit center segment */	
left outer  join ${DB_ENV}TgtVOUT.PROFIT_CENTER_HIERARCHY lvl5hier
	on lvl5hier.Rel_Profit_Center_Seg_Seq_Num = lvl4hier.Profit_Center_Seg_Seq_Num
	and lvl5hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'					
/* get 6th level profit center segment */	
left outer  join ${DB_ENV}TgtVOUT.PROFIT_CENTER_HIERARCHY lvl6hier
	on lvl6hier.Rel_Profit_Center_Seg_Seq_Num = lvl5hier.Profit_Center_Seg_Seq_Num
	and lvl6hier.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'				
/* Get Profit Center Segment Details */
inner join ${DB_ENV}TgtVOUT.PROFIT_CENTER_SEG_DETAILS_B  lvlXdet
	on lvlXdet.Profit_Center_Seg_Seq_Num = coalesce(lvl6hier.Profit_Center_Seg_Seq_Num, lvl5hier.Profit_Center_Seg_Seq_Num, lvl4hier.Profit_Center_Seg_Seq_Num, lvl3hier.Profit_Center_Seg_Seq_Num, lvl2hier.Profit_Center_Seg_Seq_Num, lvl1hier.Profit_Center_Seg_Seq_Num, lvl0hier.Profit_Center_Seg_Seq_Num, lvl0.Profit_Center_Seg_Seq_Num)
	and lvlXdet.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
where lvl0det.Top_Level_Ind = 1

;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVIN.PROFIT_CENTER_SEG_LVL_7_D IS '$Revision: 21464 $ - $Date: 2017-02-13 14:25:11 +0100 (mån, 13 feb 2017) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
