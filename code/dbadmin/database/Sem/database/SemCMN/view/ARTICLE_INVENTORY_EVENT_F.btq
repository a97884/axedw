/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ARTICLE_INVENTORY_EVENT_F.btq 21578 2017-02-16 11:27:30Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-02-16 12:27:30 +0100 (tor, 16 feb 2017) $
# Last Revision    : $Revision: 21578 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ARTICLE_INVENTORY_EVENT_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name if needed
;


.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.ARTICLE_INVENTORY_EVENT_F
(
Store_Seq_Num,
Item_Inv_Dt,
Inv_Reason_Cd,
Scan_Code_Seq_Num,
Local_Scan_Cd,
Local_Scan_Code_Desc,
Article_Seq_Num,
Inv_Event_Id,
Inv_Name,
Supplier_Item_Id,
Junk_seq_num,
Local_Art_Hier_Lvl_4_Seq_Num,
Item_Unit_Qty,
Unit_Retail_Price_Amt,
Unit_Retail_Value_Amt,
Unit_Retail_Type_Cd,
Unit_Cost_Amt,
Unit_Cost_Value_Amt,
Unit_Cost_Type_Cd,
Unit_Profit_Margin_Perc,
Unit_Tax_Perc,
Unit_Additional_Tax_Amt,
Unit_Additional_Tax_Value_Amt
)
As
select 
f1.Store_Seq_Num,
f1.Item_Inv_Dt,
f1.Inv_Reason_Cd,
f1.Scan_Code_Seq_Num,
f1.Local_Scan_Cd,
max(scDesc.MU_Scan_Code_Desc) As Local_Scan_Code_Desc,
max(coalesce(scm.Article_Seq_Num,-1)) as Article_Seq_Num,
f1.Inv_Event_Id,
f1.Inv_Name,
f1.Supplier_Item_Id,
max(coalesce(j1.Junk_seq_num,-1)) as Junk_seq_num,
max(ahLvl4.Node_Seq_Num) As Local_Art_Hier_Lvl_4_Seq_Num,
f1.Item_Unit_Qty,
f1.Unit_Retail_Price_Amt,
f1.Unit_Retail_Value_Amt,
f1.Unit_Retail_Type_Cd,
f1.Unit_Cost_Amt,
f1.Unit_Cost_Value_Amt,
f1.Unit_Cost_Type_Cd,
f1.Unit_Profit_Margin_Perc,
f1.Unit_Tax_Perc,
f1.Unit_Additional_Tax_Amt,
f1.Unit_Additional_Tax_Value_Amt
from
(Select	
iie.Location_Seq_Num As Store_Seq_Num,
max(ii.Item_Inv_Dt) as Item_Inv_Dt,
iie.Inv_Reason_Cd,
ii.Scan_Code_Seq_Num,
max(sc.N_Scan_Cd) As Local_Scan_Cd,
iie.Inv_Event_Id As Inv_Event_Id,
max(iie.Inv_Name) as Inv_Name,
ii.Supplier_Item_Id,
sum(ii.Item_Unit_Qty) as Item_Unit_Qty,
max(ii.Unit_Retail_Price_Amt) as Unit_Retail_Price_Amt,
sum(ii.Unit_Retail_Value_Amt) as Unit_Retail_Value_Amt,
max(ii.Unit_Retail_Type_Cd) as Unit_Retail_Type_Cd,
max(ii.Unit_Cost_Amt) as Unit_Cost_Amt,
sum(ii.Unit_Cost_Value_Amt) as Unit_Cost_Value_Amt,
max(ii.Unit_Cost_Type_Cd) as Unit_Cost_Type_Cd,
max(ii.Unit_Profit_Margin_Perc) As Unit_Profit_Margin_Perc,
max(ii.Unit_Tax_Perc) As Unit_Tax_Perc,
max(ii.Unit_Additional_Tax_Amt) As Unit_Additional_Tax_Amt,
sum(ii.Unit_Additional_Tax_Value_Amt) as Unit_Additional_Tax_Value_Amt

From (SELECT Location_Seq_Num, Scan_Code_Seq_Num, Supplier_Item_Id, Inv_Reason_Cd,
Inv_Event_Id, Item_Inv_Line_Num, Item_Inv_Dt, Item_Unit_Qty,
Unit_Retail_Price_Amt, Unit_Retail_Type_Cd, Unit_Retail_Value_Amt,
Unit_Cost_Amt, Unit_Cost_Type_Cd, Unit_Cost_Value_Amt, Unit_Tax_Perc,
Unit_Profit_Margin_Perc, Unit_Additional_Tax_Amt,
CASE WHEN Item_Unit_Qty = 0 or Unit_Additional_Tax_Amt = 0 THEN 0
ELSE(Unit_Additional_Tax_Amt / (Unit_Retail_Price_Amt / (Unit_Retail_Value_Amt/Item_Unit_Qty))) *  Item_Unit_Qty
END as Unit_Additional_Tax_Value_Amt
FROM ${DB_ENV}TgtVOUT.ITEM_INVENTORY) AS ii

Join ${DB_ENV}TgtVOUT.ITEM_INVENTORY_EVENT As iie
	On	ii.Location_Seq_Num = iie.Location_Seq_Num
	And	ii.Inv_Reason_Cd = iie.Inv_Reason_Cd
	And	ii.Inv_Event_Id = iie.Inv_Event_Id
Left Outer Join ${DB_ENV}TgtVOUT.MU_SCAN_CODE_LOCATION AS scLoc
	On	ii.Scan_Code_Seq_Num = scLoc.Scan_Code_Seq_Num
	And	iie.Location_Seq_Num = scLoc.Location_Seq_Num
Inner Join ${DB_ENV}TgtVOUT.MU_SCAN_CODE As sc
	On	ii.Scan_Code_Seq_Num = sc.Scan_Code_Seq_Num
group by iie.Location_Seq_Num, 
iie.Inv_Reason_Cd,
ii.Scan_Code_Seq_Num,
ii.Supplier_Item_Id,
iie.Inv_Event_Id
) as f1

Left Outer Join ${DB_ENV}TgtVOUT.MU_SCAN_CODE_INV_DESC_B AS scDesc
	On	f1.Scan_Code_Seq_Num = scDesc.Scan_Code_Seq_Num
	And	f1.Store_Seq_Num = scDesc.Location_Seq_Num
    And  Cast(Cast(f1.Item_Inv_Dt  AS CHAR(10)) || ' ' || '23:59:59' as timestamp) Between scDesc.Valid_From_Dttm And scDesc.Valid_To_Dttm 
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_AH_ALLOC_LOCAL AS ahLvl4
	On	f1.Scan_Code_Seq_Num = ahLvl4.Scan_Code_Seq_Num
	And	f1.Store_Seq_Num = ahLvl4.Hier_Defining_Loc_Seq_Num
	And	ahLvl4.AH_Level_Num_DD = 4
    And  Cast(Cast(f1.Item_Inv_Dt  AS CHAR(10)) || ' ' || '23:59:59' as timestamp) Between ahLvl4.Valid_From_Dttm And ahLvl4.Valid_To_Dttm

Left Outer Join ${DB_ENV}SemCMNVOUT.SCAN_CODE_DAY_D As scm	
	On	f1.Scan_Code_Seq_Num = scm.Scan_Code_Seq_num 
	And CAST(f1.Item_Inv_Dt AS TIMESTAMP(6)) between scm.Valid_From_Dttm And scm.Valid_To_Dttm
	
Left Outer Join ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
on f1.Store_Seq_Num = std1.Store_Seq_Num and f1.Item_Inv_Dt = std1.Calendar_Dt

Left Outer Join ${DB_ENV}SemCMNVOUT.JUNK_D as j1
on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') and j1.Loyalty_Ind = 0		
	
group by
f1.Store_Seq_Num,
f1.Item_Inv_Dt,
f1.Inv_Reason_Cd,
f1.Scan_Code_Seq_Num,
f1.Local_Scan_Cd,
f1.Inv_Event_Id,
f1.Inv_Name,
f1.Supplier_Item_Id,
f1.Item_Unit_Qty,
f1.Unit_Retail_Price_Amt,
f1.Unit_Retail_Value_Amt,
f1.Unit_Retail_Type_Cd,
f1.Unit_Cost_Amt,
f1.Unit_Cost_Value_Amt,
f1.Unit_Cost_Type_Cd,
f1.Unit_Profit_Margin_Perc,
f1.Unit_Tax_Perc,
f1.Unit_Additional_Tax_Amt,
f1.Unit_Additional_Tax_Value_Amt
 ;
 
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ARTICLE_INVENTORY_EVENT_F IS '$Revision: 21578 $ - $Date: 2017-02-16 12:27:30 +0100 (tor, 16 feb 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
