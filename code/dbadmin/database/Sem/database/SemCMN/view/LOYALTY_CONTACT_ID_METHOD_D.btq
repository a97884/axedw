/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_CONTACT_ID_METHOD_D.btq 16430 2015-05-27 08:59:35Z K9105286 $
# Last Changed By  : $Author: K9105286 $
# Last Change Date : $Date: 2015-05-27 10:59:35 +0200 (ons, 27 maj 2015) $
# Last Revision    : $Revision: 16430 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_CONTACT_ID_METHOD_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.LOYALTY_CONTACT_ID_METHOD_D 
( 
Loyalty_Identifier_Seq_Num,
Loyalty_Identifier_Id,
Defining_Org_Party_Id,
Contact_Account_Seq_Num, 
Identification_Number,
Identification_Type_Cd,
Card_Number_Masked,
Name_On_Card,
Card_Status_Cd,
Siebel_Card_Id,
Request_To_Production_Dt,
Sent_To_Production_Dt,
Active_Ind
) 
As LOCKING ROW FOR ACCESS --Dirty read needed as table db is used
Select	
li.Loyalty_Identifer_Seq_Num As Loyalty_Identifier_Seq_Num,
li.N_Loyalty_Idenfier_Id As Loyalty_Identifier_Id,
li.N_Defining_Org_Id As Defining_Org_Party_Id,
Coalesce(lci.Contact_Account_Seq_Num,-1) As Contact_Account_Seq_Num,
li.N_Loyalty_Idenfier_Id As Identification_Number,
li.Identifier_Type_Cd As Identification_Type_Cd,
Coalesce(lcdb.Card_Number_Masked,'Saknas') As Card_Number_Masked,
Coalesce(lcdb.Name_on_Card,'Saknas') As Name_on_Card,
Coalesce(lcdb.Card_Status_Cd,'-1') As Card_Status_Cd,
Coalesce(lcdb.SRC_Siebel_Card_Id,'-1') As SRC_Siebel_Card_Id,
lcdb.Request_To_Production_Dt, 
lcdb.Sent_To_Production_Dt,
Coalesce(lcdb.Active_Ind, -1) As Active_Ind
From ${DB_ENV}TgtVOUT.LOYALTY_IDENTIFIER As li
Left Outer Join ${DB_ENV}TgtVOUT.LOYALTY_CONTACT_IDENTIFER As lci
	On	li.Loyalty_Identifer_Seq_Num = lci.Loyalty_Identifer_Seq_Num
	And	lci.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
Left Outer Join ${DB_ENV}TgtVOUT.LOYALTY_CARD_DETAILS_B As lcdb
	On	li.Loyalty_Identifer_Seq_Num = lcdb.Loyalty_Identifer_Seq_Num
    And	lcdb.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'

	Union All -- Add record for missing masterdata	
Select	
	d0.Dummy_Seq_Num As Loyalty_Identifier_Seq_Num,
	d0.Dummy_Id As Loyalty_Identifier_Id,
	d0.Dummy_Id As Defining_Org_Party_Id,
	d0.Dummy_Seq_Num As Contact_Account_Seq_Num, 
	'-1' As Identification_Number,
	'-1' As Identification_Type_Cd,
	'Saknas' As Card_Number_Masked,
	'Saknas' As Name_On_Card,
	'-1' As Card_Status_Cd,
	'-1' As Siebel_Card_Id,
	Null (TIMESTAMP) As Request_To_Production_Dt,
	Null (TIMESTAMP) As Sent_To_Production_Dt,
	-1 As Active_Ind
From ${DB_ENV}SemCMNT.DUMMY As d0
	;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_CONTACT_ID_METHOD_D  IS '$Revision: 16430 $ - $Date: 2015-05-27 10:59:35 +0200 (ons, 27 maj 2015) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE