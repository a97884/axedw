/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: FINANCIAL_PLAN_BALANCE_F.btq 28467 2019-05-29 12:01:59Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-05-29 14:01:59 +0200 (ons, 29 maj 2019) $
# Last Revision    : $Revision: 28467 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/FINANCIAL_PLAN_BALANCE_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNSecVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNSecVOUT.FINANCIAL_PLAN_BALANCE_F
(
	 Set_Of_Books_Cd
	, Company_Seq_Num
	, Junk_Seq_Num
	, Accounting_Period_Id
	, Calendar_Year_Id
	, Calendar_Month_Id 
	, Calendar_Dt
	, Financial_Plan_Id
	, Financial_Plan_Line_Num
	, Profit_Center_Seq_Num
	, Cost_Center_Seq_Num
	, COA_Code_Seq_Num	
	, Art_Hier_Lvl_2_Seq_Num
	, Internal_Elimination_Ind
	, Credit_Ind
	, Financial_Plan_Val
	, Currency_Cd
	)
as
LOCK ROW FOR ACCESS
SELECT 
 fpl1.Set_Of_Books_Cd
 , fpl1.Internal_Org_Party_Seq_Num As Company_Seq_Num
 , j1.Junk_Seq_Num
 , fpl1.Accounting_Period_Id As Accounting_Period_Id
 , fpl1.Accounting_Period_Id As Calendar_Year_Id
 , cast(substr(cast(fpl1.Accounting_Period_Id as char(10)),1,4) || '00' as integer) + cast(substr(cast(fpl1.Accounting_Period_Id as char(10)),5,6) as integer) As Calendar_Month_Id
 , cast(trim(substr(cast(fpl1.Accounting_Period_Id as char(10)),1,4)) || '-' || trim(substr(cast(fpl1.Accounting_Period_Id as char(10)),6,6))  || '-01' as Date) As Calendar_Dt
 , fpl1.FP_Header_Id As Financial_Plan_Id
 , fpl1.FP_Line_Num  As Financial_Plan_Line_Num
 , fpl1.Profit_Center_Seq_Num
 , fpl1.Cost_Center_Seq_Num
 , fpl1.COA_Code_Seq_Num
 , Coalesce(ahn1.Node_Seq_Num,-1)  As Art_Hier_Lvl_2_Seq_Num
 , fpl1.Internal_Elimination_Ind
 , fpl1.Credit_Ind
 , fpl1.Financial_Val As Financial_Plan_Val
 , fpl1.Currency_Cd
 FROM ${DB_ENV}TgtSecVOUT.FINANCIAL_PLAN_BALANCE fpl1
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_HIERARCHY_NODE ahn1 
ON ahn1.N_Node_Id = Coalesce(fpl1.Financial_Entry_Segment_Cd,'-1') 
AND ahn1.Article_Hierarchy_Id = 'GG'
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_SALES_ORGANIZATION_D as sts1
    On fpl1.Profit_Center_Seq_Num = sts1.Profit_Center_Seq_Num
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
	On sts1.Store_Seq_Num = std1.Store_Seq_Num 
 and cast(trim(substr(cast(fpl1.Accounting_Period_Id as char(10)),1,4)) || '-' || trim(substr(cast(fpl1.Accounting_Period_Id as char(10)),6,6))  || '-01' as Date) = std1.Calendar_Dt
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
	on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
	and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
	and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
	and j1.Loyalty_Ind = 0 ;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNSecVOUT.FINANCIAL_PLAN_BALANCE_F IS '$Revision: 28467 $ - $Date: 2019-05-29 14:01:59 +0200 (ons, 29 maj 2019) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
