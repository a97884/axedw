/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: AGREEMENT_TERMS_F.btq 29484 2019-11-15 11:41:04Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:41:04 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29484 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/AGREEMENT_TERMS_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.AGREEMENT_TERMS_F
(
Agreement_Seq_Num
,Calendar_Dt
,Article_Seq_Num
,Vendor_Seq_Num
,Art_Hier_Lvl_2_Seq_Num
,Art_Hier_Lvl_4_Seq_Num
,Currency_Cd
,Tender_Amt
,Price_Out_Precent
,Price_In_Precent
,Price_In
,Price_Out
)
As LOCKING ROW FOR ACCESS
select 
a1.Agreement_Seq_Num
,cd1.Calendar_Dt
,coalesce(ap1.Article_Seq_Num,-1) as Article_Seq_Num
,coalesce(ap1.Tender_Party_Seq_Num,-1) as Vendor_Seq_Num
,coalesce(ah2.Art_Hier_Lvl_2_Seq_Num,-1) as Art_Hier_Lvl_2_Seq_Num
,coalesce(ah4.Art_Hier_Lvl_4_Seq_Num,-1) as Art_Hier_Lvl_4_Seq_Num
,coalesce(ap1.Currency_Cd,'-1') as Currency_Cd
,coalesce(ap1.Tender_Amt,0) as Tender_Amt
,coalesce(atPOP.Agreement_Term_Val,0) as Price_Out_Precent
,coalesce(atPIP.Agreement_Term_Val,0) as Price_In_Precent
,coalesce(atPI.Agreement_Term_Val,0) as Price_In
,coalesce(atPO.Agreement_Term_Val,0) as Price_Out

FROM ${DB_ENV}TgtVOUT.AGREEMENT as a1

INNER JOIN ${DB_ENV}TgtVOUT.AGREEMENT_DETAILS_B As ad1
on a1.Agreement_Seq_Num = ad1.agreement_seq_num
and ad1.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'

INNER JOIN ${DB_ENV}TgtVOUT.CALENDAR_DATE as cd1
on cd1.Calendar_Dt between ad1.Agreement_Effective_Dt and ad1.Agreement_Final_Expiration_Dt

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.AGREEMENT_POSITION as ap1
on ap1.Agreement_Seq_Num = a1.Agreement_Seq_Num
AND cd1.Calendar_Dt between cast(ap1.Valid_From_Dttm as date) and cast(ap1.Valid_To_Dttm as date)

LEFT OUTER JOIN (
SELECT 
at1.Agreement_Position_Seq_Num,
at1.Agreement_Seq_Num,
cast(at1.Valid_From_Dttm as date) as Valid_From_Dttm,
cast(at1.Valid_To_Dttm as date) as Valid_To_Dttm,
at2.Agreement_Term_Val
FROM ${DB_ENV}TgtVOUT.AGREED_TERM as at1
INNER JOIN ${DB_ENV}TgtVOUT.AGREEMENT_TERM as at2
on at2.Agreement_Term_Seq_Num = at1.Agreement_Term_Seq_Num and at2.Agreement_Term_Type_Cd = 'POP'
) as atPOP
on ap1.Agreement_Position_Seq_Num = atPOP.Agreement_Position_Seq_Num
and ap1.Agreement_Seq_Num = atPOP.Agreement_Seq_Num
and cd1.Calendar_Dt between atPOP.Valid_From_Dttm and atPOP.Valid_To_Dttm

LEFT OUTER JOIN (
SELECT 
at1.Agreement_Position_Seq_Num,
at1.Agreement_Seq_Num,
cast(at1.Valid_From_Dttm as date) as Valid_From_Dttm,
cast(at1.Valid_To_Dttm as date) as Valid_To_Dttm,
at2.Agreement_Term_Val
FROM ${DB_ENV}TgtVOUT.AGREED_TERM as at1
INNER JOIN ${DB_ENV}TgtVOUT.AGREEMENT_TERM as at2
on at2.Agreement_Term_Seq_Num = at1.Agreement_Term_Seq_Num and at2.Agreement_Term_Type_Cd = 'PIP'
) as atPIP
on ap1.Agreement_Position_Seq_Num = atPIP.Agreement_Position_Seq_Num
and ap1.Agreement_Seq_Num = atPIP.Agreement_Seq_Num
and cd1.Calendar_Dt between atPIP.Valid_From_Dttm and atPIP.Valid_To_Dttm

LEFT OUTER JOIN (
SELECT 
at1.Agreement_Position_Seq_Num,
at1.Agreement_Seq_Num,
cast(at1.Valid_From_Dttm as date) as Valid_From_Dttm,
cast(at1.Valid_To_Dttm as date) as Valid_To_Dttm,
at2.Agreement_Term_Val
FROM ${DB_ENV}TgtVOUT.AGREED_TERM as at1
INNER JOIN ${DB_ENV}TgtVOUT.AGREEMENT_TERM as at2
on at2.Agreement_Term_Seq_Num = at1.Agreement_Term_Seq_Num and at2.Agreement_Term_Type_Cd = 'PO'
) as atPO
on ap1.Agreement_Position_Seq_Num = atPO.Agreement_Position_Seq_Num
and ap1.Agreement_Seq_Num = atPO.Agreement_Seq_Num
and cd1.Calendar_Dt between atPO.Valid_From_Dttm and atPO.Valid_To_Dttm

LEFT OUTER JOIN (
SELECT 
at1.Agreement_Position_Seq_Num,
at1.Agreement_Seq_Num,
cast(at1.Valid_From_Dttm as date) as Valid_From_Dttm,
cast(at1.Valid_To_Dttm as date) as Valid_To_Dttm,
at2.Agreement_Term_Val
FROM ${DB_ENV}TgtVOUT.AGREED_TERM as at1
INNER JOIN ${DB_ENV}TgtVOUT.AGREEMENT_TERM as at2
on at2.Agreement_Term_Seq_Num = at1.Agreement_Term_Seq_Num and at2.Agreement_Term_Type_Cd = 'PI'
) as atPI
on ap1.Agreement_Position_Seq_Num = atPI.Agreement_Position_Seq_Num
and ap1.Agreement_Seq_Num = atPI.Agreement_Seq_Num
and cd1.Calendar_Dt between atPI.Valid_From_Dttm and atPI.Valid_To_Dttm

LEFT OUTER JOIN  ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D as ah2
on ah2.Art_Hier_Lvl_2_Seq_Num = ap1.Node_Seq_Num

LEFT OUTER JOIN  ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D as ah4
on ah4.Art_Hier_Lvl_4_Seq_Num = ap1.Node_Seq_Num
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.AGREEMENT_TERMS_F IS '$Revision: 29484 $ - $Date: 2019-11-15 12:41:04 +0100 (fre, 15 nov 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE