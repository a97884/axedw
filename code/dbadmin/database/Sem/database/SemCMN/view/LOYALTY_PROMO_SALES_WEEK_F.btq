/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_PROMO_SALES_WEEK_F.btq 28850 2019-09-02 10:41:56Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $
# Last Revision    : $Revision: 28850 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_PROMO_SALES_WEEK_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.LOYALTY_PROMO_SALES_WEEK_F
(
	Promotion_Offer_Seq_Num,
	Concept_Cd,
	Concept_Top_Cd,
	Store_Seq_Num,
	Article_Seq_Num,
	Calendar_Week_Id,
	Member_Acc_Seq_Num_Offer,
	Member_Acc_Seq_Num_NonOffer,	
	Member_Offer_Redemp_Cnt,
	Member_Offer_Redemp_Art_Qty,
	Member_Offer_Redemp_Price_Amt,
	Member_Offer_Redemp_Cost_Amt,
	Member_NonOffer_Redemp_Cnt,
	Member_NonOffer_Redemp_Art_Qty,
	Member_NonOffer_Redemp_Price_Amt,
	Member_NonOffer_Redemp_Cost_Amt
)
as
Lock row for access
SELECT poaw1.Promotion_Offer_Seq_Num,
	s1.Concept_Cd,
	po1.Concept_Top_Cd,
	s1.Store_Seq_Num,
	stl1.Article_Seq_Num,
	stl1.Calendar_Week_Id,
	(Case When (poaw1.Promotion_Offer_Seq_Num = Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num And Coalesce(stl1.Item_Qty,0) > 0) then c1.Member_Account_Seq_Num else null End) as Member_Acc_Seq_Num_Offer,
	(Case When (poaw1.Promotion_Offer_Seq_Num <> Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num And Coalesce(stl1.Item_Qty,0) > 0) then c1.Member_Account_Seq_Num else null End) as Member_Acc_Seq_Num_NonOffer,
	Sum(Case When (poaw1.Promotion_Offer_Seq_Num = Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num And Coalesce(stl1.Item_Qty,0) > 0 ) Then 1 Else 0 End) As Member_Offer_Redemp_Cnt,
	Sum(Case When poaw1.Promotion_Offer_Seq_Num = Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num Then (Case When stl1.UOM_Category_Cd = 'WGH' Then 1 Else Coalesce(stl1.Item_Qty,0) End) Else 0 End) As Member_Offer_Redemp_Art_Qty,
	--Sum(Case When poaw1.Promotion_Offer_Seq_Num = Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num  Then Coalesce(stl1.Item_Qty,0) Else 0 End) As Member_Offer_Redemp_Art_Qty,
	Sum(Case When poaw1.Promotion_Offer_Seq_Num = Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num  Then Coalesce(stl1.Unit_Selling_Price_Amt,0) Else 0 End) As Member_Offer_Redemp_Price_Amt,
	Sum(Case When poaw1.Promotion_Offer_Seq_Num = Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num  Then Coalesce(stl1.Unit_Cost_Amt,0) Else 0 End) As Member_Offer_Redemp_Cost_Amt,
    Sum(Case When (poaw1.Promotion_Offer_Seq_Num <> Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num And Coalesce(stl1.Item_Qty,0) > 0) Then 1 Else 0 End) As Member_NonOffer_Redemp_Cnt,
	Sum(Case When poaw1.Promotion_Offer_Seq_Num <> Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num  Then (Case When stl1.UOM_Category_Cd = 'WGH' Then 1 Else Coalesce(stl1.Item_Qty,0) End) Else 0 End) As Member_NonOffer_Redemp_Art_Qty,
    --Sum(Case When poaw1.Promotion_Offer_Seq_Num <> Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num  Then Coalesce(stl1.Item_Qty,0) Else 0 End) As Member_NonOffer_Redemp_Art_Qty,
	Sum(Case When poaw1.Promotion_Offer_Seq_Num <> Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num  Then Coalesce(stl1.Unit_Selling_Price_Amt,0) Else 0 End) As Member_NonOffer_Redemp_Price_Amt,
	Sum(Case When poaw1.Promotion_Offer_Seq_Num <> Coalesce(stl1.Pref_Promotion_Offer_Seq_Num,-1) And poaw1.Article_Seq_Num = stl1.Article_Seq_Num  Then Coalesce(stl1.Unit_Cost_Amt,0) Else 0 End) As Member_NonOffer_Redemp_Cost_Amt
FROM ${DB_ENV}SemCMNVOUT.SALES_TRAN_LINE_WEEK_F as stl1
INNER JOIN ${DB_ENV}SemCMNVOUT.STORE_D as s1
on  (s1.Store_Seq_Num = stl1.Store_Seq_Num)	
INNER JOIN ${DB_ENV}SemCMNVOUT.PROMO_OFFER_ARTICLE_WEEK_B poaw1
on  (
		poaw1.Article_Seq_Num = stl1.Article_Seq_Num and 
		poaw1.Concept_Cd = s1.Concept_Cd and 
		poaw1.Calendar_Week_Id = stl1.Calendar_Week_Id 
	)
LEFT OUTER JOIN ${DB_ENV}SemCMNVIN.PROMOTION_OFFER As po1
 on poaw1.Promotion_Offer_Seq_Num = po1.Promotion_Offer_Seq_Num
 and poaw1.Concept_Cd = po1.Concept_Cd

INNER JOIN 	(SELECT Contact_Account_Seq_Num,Member_Account_Seq_Num FROM ${DB_ENV}SemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D) as c1
on stl1.Contact_Account_Seq_Num = c1.Contact_Account_Seq_Num
where stl1.Contact_Account_Seq_Num <> -1
group by 1,2,3,4,5,6,7,8;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','LOYALTY_PROMO_SALES_WEEK_F','POST','I',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_PROMO_SALES_WEEK_F IS '$Revision: 28850 $ - $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
