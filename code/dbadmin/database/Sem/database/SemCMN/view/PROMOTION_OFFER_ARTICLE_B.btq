/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PROMOTION_OFFER_ARTICLE_B.btq 28850 2019-09-02 10:41:56Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $
# Last Revision    : $Revision: 28850 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/PROMOTION_OFFER_ARTICLE_B.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.PROMOTION_OFFER_ARTICLE_B
(
	Promotion_Offer_Seq_Num
	,Article_Seq_Num
	,Promoted_UOM_Cd
	,Preferred_Tactic_Type_Cd
	,Min_Tactic_Type_Cd
	,Free_Text
	,Promotion_Discount_Value
	,Promotion_Discount_Type_Cd
	,Quantity
	,Operator_Cd
	,Promotion_Offer_Term_Cd
	,Promotion_Offer_Limit
	,VF_Scanback_Type
	,Product_Dimension_Type_Cd
	,Forecast_on_Purchased_Qty
	,Forecast_on_Sold_Qty
	,Tax_Pct
	,Concept_Cd
	,Concept_Top_Cd
) 
As
Select
	poa1.Promotion_Offer_Seq_Num
	,Coalesce(poa1.Article_Seq_Num, -1) As Article_Seq_Num
	,Coalesce(poa3.UOM_Cd, '-1') As Promoted_UOM_Cd
	,Coalesce(poat1.Tactic_Type_Cd, '-1') As Preferred_Tactic_Type_Cd
	,Coalesce(poat2.Min_Tactic_Type_Cd, '-1') As Min_Tactic_Type_Cd 
	,poac1.Free_Text
	,poac1.Discount_Value As Promotion_Discount_Value
	,Coalesce(poac1.Promotion_Discount_Type_Cd, '-1') As Promotion_Discount_Type_Cd
	,poac1.Quantity
	,poac1.Operator_Cd
	,Coalesce(poac1.Promotion_Offer_Term_Cd, -1) As Promotion_Offer_Term_Cd
	,poac1.Promotion_Offer_Limit
	,poac1.VF_Scanback_Type
	,Coalesce(poac1.Product_Dimension_Type_Cd, '-1') As Product_Dimension_Type_Cd
	,poaf1.Forecast_on_Purchased_Qty
	,poaf1.Forecast_on_Sold_Qty
	,tg1.Tax_Group_Percentage/100 (FLOAT) As Tax_Pct
	,poa1.Concept_Cd
	,poa1.Concept_Top_Cd
/* Common logic for several views is collected in a separate SemCMNVIN view */
From	(Select
			poa2.Promotion_Offer_Seq_Num
			,poa2.Article_In_Offer_Ind
			,poa2.Article_Seq_Num
			,poa2.Concept_Cd
			,po1.Concept_Top_Cd
		From ${DB_ENV}SemCMNVIN.PROMOTION_OFFER_ARTICLE poa2
		Inner Join ${DB_ENV}SemCMNVIN.PROMOTION_OFFER As po1
		On poa2.Promotion_Offer_Seq_Num = po1.Promotion_Offer_Seq_Num
		And poa2.Concept_Cd = po1.Concept_Cd
		/* Eliminate duplicates in Vendor Fund */
		Group by 1, 2, 3, 4, 5) As poa1
Left Outer Join ${DB_ENV}TgtVOUT.PROMOTION_OFFER_ARTICLE As poa3
	On poa1.Promotion_Offer_Seq_Num = poa3.Promotion_Offer_Seq_Num
	And poa1.Article_Seq_Num = poa3.Article_Seq_Num
	And poa3.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_DETAILS_B As adt1
	On poa1.Article_Seq_Num = adt1.Article_Seq_Num
	And adt1.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join	${DB_ENV}TgtVOUT.TAX_GROUP as tg1
On adt1.Tax_Group_Cd = tg1.Tax_Group_Cd
-- Preferred Tactic is delivered from ECC
Left Outer Join ${DB_ENV}TgtVOUT.PROMOTION_OFFER_ARTICLE_TACTIC As poat1
	On poa1.Promotion_Offer_Seq_Num = poat1.Promotion_Offer_Seq_Num
	And poa1.Article_Seq_Num = poat1.Article_Seq_Num
	And poat1.Preferred_Tactic_Type_Ind = 1
	And poat1.Valid_To_Dttm = SYSLIB.HighTSVal()	
-- Normally there is only one tactic per article but in PMR several could be registered
-- We replicate the logic that is used when a promotion is transferred to ECC - choose min tactic
Left Outer Join 
	(Select poat3.Promotion_Offer_Seq_Num
			,poat3.Article_Seq_Num
			,Min(poat3.Tactic_Type_Cd) As Min_Tactic_Type_Cd
	From ${DB_ENV}TgtVOUT.PROMOTION_OFFER_ARTICLE_TACTIC poat3
	Where poat3.Valid_To_Dttm = SYSLIB.HighTSVal()	
	Group by 
		poat3.Promotion_Offer_Seq_Num
		,poat3.Article_Seq_Num )As poat2
	On poa1.Promotion_Offer_Seq_Num = poat2.Promotion_Offer_Seq_Num
	And poa1.Article_Seq_Num = poat2.Article_Seq_Num
Left Outer Join ${DB_ENV}TgtVOUT.PROMO_OFFER_ARTICLE_CONSTR As poac1
	On poa1.Promotion_Offer_Seq_Num = poac1.Promotion_Offer_Seq_Num
	And poa1.Article_Seq_Num = poac1.Article_Seq_Num
	And poac1.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join ${DB_ENV}TgtVOUT.PROMO_OFFER_ARTICLE_FORECAST As poaf1
	On poa1.Promotion_Offer_Seq_Num = poaf1.Promotion_Offer_Seq_Num
	And poa1.Article_Seq_Num = poaf1.Article_Seq_Num
	And poaf1.Valid_To_Dttm = SYSLIB.HighTSVal()	
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.PROMOTION_OFFER_ARTICLE_B IS '$Revision: 28850 $ - $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
