/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PROMO_OFFER_ARTICLE_WEEK_B.btq 28850 2019-09-02 10:41:56Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $
# Last Revision    : $Revision: 28850 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/PROMO_OFFER_ARTICLE_WEEK_B.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.PROMO_OFFER_ARTICLE_WEEK_B
(
 Promotion_Offer_Seq_Num
 ,Calendar_Week_Id
 ,Article_In_Offer_Ind
 ,Article_Seq_Num
 ,ScanBack_Value_Pct
 ,ScanBack_Value_Bel
 ,Concept_Cd
 ,Concept_Top_Cd
) 
As
Select
 poa1.Promotion_Offer_Seq_Num
 ,cw1.Calendar_Week_Id
 ,poa1.Article_In_Offer_Ind
 ,poa1.Article_Seq_Num
 ,poa1.ScanBack_Value_Pct
 ,poa1.ScanBack_Value_Bel
 ,poa1.Concept_Cd
 ,poa1.Concept_Top_Cd
/* Common logic for several views is collected in a separate SemCMNVIN view */
From (Select
   poa2.Promotion_Offer_Seq_Num
   ,poa2.Article_In_Offer_Ind
   ,poa2.Article_Seq_Num
   ,poa2.Concept_Cd
   ,po1.Concept_Top_Cd
   ,Sum(Case When poa2.Scanback_Type_Cd = 1
    Then ScanBack_Value 
    Else NULL End) As ScanBack_Value_Pct
   ,Sum(Case When poa2.Scanback_Type_Cd = 2
    Then ScanBack_Value 
    Else NULL End) As ScanBack_Value_Bel
  From ${DB_ENV}SemCMNVIN.PROMOTION_OFFER_ARTICLE poa2
  /* Eliminate duplicates in Vendor Fund */
  Inner Join ${DB_ENV}SemCMNVIN.PROMOTION_OFFER As po1
   On poa2.Promotion_Offer_Seq_Num = po1.Promotion_Offer_Seq_Num
   And poa2.Concept_Cd = po1.Concept_Cd
  Group by 
  poa2.Promotion_Offer_Seq_Num
  , poa2.Article_In_Offer_Ind
  , poa2.Article_Seq_Num
  , poa2.Concept_Cd
  , po1.Concept_Top_Cd) As poa1
Inner Join ${DB_ENV}TgtVOUT.PROMO_OFFER_CAMPAIGN_DETAILS_B As pocdb1
 On poa1.Promotion_Offer_Seq_Num = pocdb1.Promotion_Offer_Seq_Num
 And pocdb1.Valid_To_Dttm = SYSLIB.HighTSVal() 
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_WEEK As cw1
 On (cw1.Calendar_Week_End_Dt Between pocdb1.Campaign_Promotion_Start_Dt And pocdb1.Campaign_Promotion_End_Dt
 Or cw1.Calendar_Week_Start_Dt Between pocdb1.Campaign_Promotion_Start_Dt And pocdb1.Campaign_Promotion_End_Dt
 Or (pocdb1.Campaign_Promotion_Start_Dt > cw1.Calendar_Week_Start_Dt And pocdb1.Campaign_Promotion_End_Dt < cw1.Calendar_Week_End_Dt))
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.PROMO_OFFER_ARTICLE_WEEK_B IS '$Revision: 28850 $ - $Date: 2019-09-02 12:41:56 +0200 (mån, 02 sep 2019) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
