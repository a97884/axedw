/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ASSOCIATE_LABOR_TASK_SCHED_F.btq 25154 2018-06-13 10:20:57Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-06-13 12:20:57 +0200 (ons, 13 jun 2018) $
# Last Revision    : $Revision: 25154 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ASSOCIATE_LABOR_TASK_SCHED_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name if needed
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.ASSOCIATE_LABOR_TASK_SCHED_F
(
	Associate_Seq_Num
	, Location_Seq_Num
	, Location_Zone_Seq_Num 
	, Store_Seq_Num
	, DC_Seq_Num
	, Cost_Center_Seq_Num
	, Junk_seq_Num
	, Work_Shift_Cd
	, Labor_Dt
	, Labor_Hour_Of_Day_Tm
	, Work_Shift_Class_Seq_Num	
	, Associate_Manager_Seq_Num
	, Scheduled_Labor_Minute_Qty
	, Scheduled_Labor_Cost_Amt
	, Scheduled_OT_Labor_Cost_Amt
	, Scheduled_OT_Labor_Minute_Qty
	, Scheduled_QOT_Labor_Minute_Qty
	, Wage_Rate_Type_Cd
	, Overtime_Wage_Rate_Type_Cd
	, Labor_Task_Status_Cd
) As
Lock Row For Access
select 
	schedtask.Associate_Seq_Num
	, schedtask.Location_Seq_Num
	, schedtask.Location_Zone_Seq_Num 
	, schedtask.Location_Seq_Num As Store_Seq_Num
	, schedtask.Location_Seq_Num As DC_Seq_Num
	, schedtask.Cost_Center_Seq_Num
	, j1.Junk_seq_Num
	, schedtask.Work_Shift_Cd
	, schedtask.Labor_Dt
	, schedtask.Labor_Hour_Of_Day_Tm
	, Coalesce(usr.M_Seq_Num,-1) As Work_Shift_Class_Seq_Num 
	, am.Associate_Manager_Seq_Num
	, Case When schedtask.Overtime_Wage_Rate_Type_Cd is null then schedtask.Sch_Task_Min_Qty else 0 end as Scheduled_Labor_Minute_Qty
	, schedtask.Sch_Task_Min_Qty*schedtask.CostPerMinute as Scheduled_Labor_Cost_Amt
	, schedtask.Sch_Task_Min_Qty*schedtask.OTCostPerMinute as Scheduled_OT_Labor_Cost_Amt
	, Case When schedtask.Overtime_Wage_Rate_Type_Cd is not null then schedtask.Sch_Task_Min_Qty else 0 end as Scheduled_OT_Labor_Minute_Qty
	, cast((Coalesce(wrt.Wage_Rate_Type_Percent,0)/100)*(Case When schedtask.Overtime_Wage_Rate_Type_Cd is not null then schedtask.Sch_Task_Min_Qty else 0 end) as DECIMAL(9,3)) as Scheduled_QOT_Labor_Minute_Qty
	, schedtask.Wage_Rate_Type_Cd
	, schedtask.Overtime_Wage_Rate_Type_Cd
	, Case When schedtask.Labor_Task_Status_Cd = -1 Then -3 Else 20+schedtask.Labor_Task_Status_Cd End As Labor_Task_Status_Cd
from (
--Lowest grain facts
	select 
		alts.Associate_Seq_Num
		, alts.Location_Seq_Num as Location_Seq_Num 
		, alts.Location_Zone_Seq_Num as Location_Zone_Seq_Num
		, alts.Cost_Center_Seq_Num
		, alts.Work_Shift_Cd
		, alts.Schedule_Task_Status_Cd As Labor_Task_Status_Cd 
		, alts.Scheduled_Task_Dt_DD as Labor_Dt
		, hod.Time_Of_Day_Tm as Labor_Hour_Of_Day_Tm
		, Sum(case 
   		    when Cast(alts.Scheduled_Task_Start_Dttm as Time) >=  hod.Time_Of_Day_Tm And Extract(Hour from (alts.Scheduled_Task_End_Dttm)) =  Extract(Hour from (hod.Time_Of_Day_Tm))  Then (Cast(Extract(Minute from (alts.Scheduled_Task_End_Dttm)) as INTEGER) - Cast(Extract(Minute from (alts.Scheduled_Task_Start_Dttm)) as INTEGER))		
  		    when Cast(alts.Scheduled_Task_Start_Dttm as Time)>=  hod.Time_Of_Day_Tm And Extract(Hour from (alts.Scheduled_Task_End_Dttm)) <>  Extract(Hour from (hod.Time_Of_Day_Tm)) Then Cast(60 as INTEGER) - Cast(Extract(Minute from (alts.Scheduled_Task_Start_Dttm)) as INTEGER)
			when Cast(alts.Scheduled_Task_Start_Dttm as Time) <  hod.Time_Of_Day_Tm and Extract(Hour from (hod.Time_Of_Day_Tm)) < Extract(Hour from (alts.Scheduled_Task_End_Dttm)) Then Cast(60 as INTEGER)
			when Cast(alts.Scheduled_Task_Start_Dttm as Time) <=  hod.Time_Of_Day_Tm and Extract(Hour from (hod.Time_Of_Day_Tm)) = Extract(Hour from (alts.Scheduled_Task_End_Dttm)) Then Cast(Extract(Minute from (alts.Scheduled_Task_End_Dttm)) as INTEGER)
			else Cast(0 as INTEGER)
			end) as Sch_Task_Min_Qty
		, Sum(alts.Scheduled_Task_Cost_Amt) as Scheduled_Task_Cost_Amt
		, Sum(alts.Scheduled_Task_OT_Cost_Amt) as Scheduled_Task_Overtime_Cost_Amt
		, alts.Wage_Rate_Type_Cd
		, alts.OT_Wage_Rate_Type_Cd as Overtime_Wage_Rate_Type_Cd
		, Sum(((alts.Scheduled_Task_Cost_Amt) /  Case When alts.Scheduled_Task_End_Dttm > alts.Scheduled_Task_Start_Dttm Then ((Extract(Hour from (alts.Scheduled_Task_End_Dttm))*60+Extract(Minute from (alts.Scheduled_Task_End_Dttm)))- (Extract(Hour from (alts.Scheduled_Task_Start_Dttm))*60+Extract(Minute from (alts.Scheduled_Task_Start_Dttm)))) Else 1 End )) /count(*)   as CostPerMinute
		, Sum(((alts.Scheduled_Task_OT_Cost_Amt) / Case When alts.Scheduled_Task_End_Dttm > alts.Scheduled_Task_Start_Dttm Then ((Extract(Hour from (alts.Scheduled_Task_End_Dttm))*60+Extract(Minute from (alts.Scheduled_Task_End_Dttm)))- (Extract(Hour from (alts.Scheduled_Task_Start_Dttm))*60+Extract(Minute from (alts.Scheduled_Task_Start_Dttm)))) Else 1 End )) /count(*)   as OTCostPerMinute
	from ${DB_ENV}TgtVOUT.ASSOCIATE_LABOR_TASK_SCHEDULE alts
	join ${DB_ENV}TgtVOUT.HOUR_OF_DAY hod on extract(Hour from (hod.Time_Of_Day_Tm)) between Extract(Hour from (alts.Scheduled_Task_Start_Dttm)) and Extract(Hour from (alts.Scheduled_Task_End_Dttm))
	group by 
		alts.Associate_Seq_Num
		, alts.Location_Seq_Num
		, alts.Location_Zone_Seq_Num
		, alts.Cost_Center_Seq_Num
		, alts.Work_Shift_Cd
		, alts.Schedule_Task_Status_Cd
		, alts.Scheduled_Task_Dt_DD
		, hod.Time_Of_Day_Tm 
		, alts.Wage_Rate_Type_Cd
		, alts.OT_Wage_Rate_Type_Cd 
) schedtask
left outer join ${DB_ENV}TgtVOUT.WAGE_RATE_TYPE wrt on wrt.Wage_Rate_Type_Cd = schedtask.Overtime_Wage_Rate_Type_Cd
left outer join ${DB_ENV}TgtVOUT.COST_CENTER cc on cc.Cost_Center_Seq_Num = schedtask.Cost_Center_Seq_Num
left outer join ${DB_ENV}SemMetadataVOUT.MSTR_LABOR_SHIFT_CLASS_M usr on cc.N_Cost_Center_Id = usr.N_Cost_Center_Id and schedtask.Work_Shift_Cd = usr.Work_Shift_Cd
Left Outer Join ${DB_ENV}TgtVOUT.ASSOCIATE_MANAGER am
on schedtask.Associate_Seq_Num = am.Associate_Seq_Num 
and schedtask.Labor_Dt Between am.Valid_From_Dt And am.Valid_From_Dt  
--where schedtask.Sch_Task_Min_Qty > 0 --NOT SURE WHY THIS WAS HERE; BUT NOW IT HAS BEEN REMOVED

LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
	On schedtask.Location_Seq_Num = std1.Store_Seq_Num and schedtask.Labor_Dt = std1.Calendar_Dt
	LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
	on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
	and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
	and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
	and j1.Loyalty_Ind = 0;


.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ASSOCIATE_LABOR_TASK_SCHED_F IS '$Revision: 25154 $ - $Date: 2018-06-13 12:20:57 +0200 (ons, 13 jun 2018) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
