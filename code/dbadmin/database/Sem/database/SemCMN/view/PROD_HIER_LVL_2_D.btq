/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PROD_HIER_LVL_2_D.btq 22619 2017-08-08 12:34:32Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-08-08 14:34:32 +0200 (tis, 08 aug 2017) $
# Last Revision    : $Revision: 22619 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/PROD_HIER_LVL_2_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.PROD_HIER_LVL_2_D
(	Prod_Hier_Lvl2_Seq_Num,
	Prod_Hier_Lvl2_Id,
	Prod_Hier_Lvl2_Desc,
	Prod_Hier_Lvl1_Seq_Num,
	EMV_Ind
)
As LOCKING ROW FOR ACCESS --Dirty read needed as table db is used
Select	ag1.Article_Group_Seq_Num As Prod_Hier_Lvl2_Seq_Num,
	ag1.N_Article_Group_Id As Prod_Hier_Lvl2_Id,
	agn.Article_Group_Name As Prod_Hier_Lvl2_Desc,
	ags.Parent_Article_Group_Seq_Num As Prod_Hier_Lvl1_Seq_Num,
	Case When ag1.N_Article_Group_Id In ('0000100010', '0000100020', '0000100030') Then 'Ja' 
	Else 'Nej'
	End As EMV_Ind
From ${DB_ENV}TgtVOUT.ARTICLE_GROUP_STRUCTURE As ags
Inner Join ${DB_ENV}TgtVOUT.ARTICLE_GROUP As ag1
	On	ags.Article_Group_Seq_Num = ag1.Article_Group_Seq_Num
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_GROUP_NAME_B As agn
	On	ag1.Article_Group_Seq_Num = agn.Article_Group_Seq_Num
	And	agn.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
Where	ags.Article_Group_Level_Num = 2
  And	ags.Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999'
  
Union All -- Add record for missing masterdata
Select	d0.Dummy_Seq_Num	As Prod_Hier_Lvl2_Seq_Num,
	d0.Dummy_Id	As Prod_Hier_Lvl2_Id,
	d0.Dummy_SAP_Desc	As Prod_Hier_Lvl2_Desc,
	d0.Dummy_Seq_Num	As Prod_Hier_Lvl1_Seq_Num,
	'Nej' as EMV_Ind
From ${DB_ENV}SemCMNT.DUMMY d0
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.PROD_HIER_LVL_2_D IS '$Revision: 22619 $ - $Date: 2017-08-08 14:34:32 +0200 (tis, 08 aug 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
