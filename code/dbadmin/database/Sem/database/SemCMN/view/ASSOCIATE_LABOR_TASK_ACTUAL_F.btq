/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ASSOCIATE_LABOR_TASK_ACTUAL_F.btq 24973 2018-05-29 07:30:52Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-05-29 09:30:52 +0200 (tis, 29 maj 2018) $
# Last Revision    : $Revision: 24973 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ASSOCIATE_LABOR_TASK_ACTUAL_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name if needed
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.ASSOCIATE_LABOR_TASK_ACTUAL_F
(
	Associate_Seq_Num
	, Location_Seq_Num
	, Location_Zone_Seq_Num 
	, Store_Seq_Num
	, DC_Seq_Num
	, Cost_Center_Seq_Num
	, Junk_Seq_Num
	, Work_Shift_Cd
	, Labor_Dt
	, Labor_Hour_Of_Day_Tm
	, Work_Shift_Class_Seq_Num
	, Associate_Manager_Seq_Num
	, Actual_Labor_Minute_Qty
	, Actual_Labor_Cost_Amt
	, Actual_OT_Labor_Cost_Amt
	, Actual_OT_Labor_Minute_Qty	
	, Actual_QOT_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Minute_Qty	
	, ReAlloc_Actual_QOT_Labor_Minute_Qty
	, OB_Minute_Qty
	, ReAlloc_OB_Minute_Qty	
	, Wage_Rate_Type_Cd
	, Overtime_Wage_Rate_Type_Cd
	, Labor_Task_Status_Cd
	, Source_System_Cd
	) As
    LOCK ROW FOR ACCESS  
	
/*  TILDTID DATA  from table ASSOCIATE_LABOR_TASK_ACTUAL */
SELECT
	  Associate_Seq_Num
	, Location_Seq_Num
	, Location_Zone_Seq_Num 
	, t1.Store_Seq_Num
	, DC_Seq_Num
	, Cost_Center_Seq_Num
	, j1.Junk_seq_Num
	, Work_Shift_Cd
	, t1.Labor_Dt
	, Labor_Hour_Of_Day_Tm
	, Work_Shift_Class_Seq_Num
	, Associate_Manager_Seq_Num
	, Actual_Labor_Minute_Qty
	, Actual_Labor_Cost_Amt
	, Actual_OT_Labor_Cost_Amt
	, Actual_OT_Labor_Minute_Qty	
	, Actual_QOT_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Minute_Qty	
	, ReAlloc_Actual_QOT_Labor_Minute_Qty
	, OB_Minute_Qty
	, ReAlloc_OB_Minute_Qty	
	, Wage_Rate_Type_Cd
	, Overtime_Wage_Rate_Type_Cd
	, Labor_Task_Status_Cd
	, Source_System_Cd
		FROM ${DB_ENV}SemCMNVin.ASSOCIATE_LABOR_TASK_TILTID as t1
	LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
	On t1.Store_Seq_Num = std1.Store_Seq_Num and t1.Labor_Dt = std1.Calendar_Dt
    LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
	on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
	and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
	and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
	and j1.Loyalty_Ind = 0	

	UNION ALL
	
	/*  QUINYX DATA  from table ASSOCIATE_LABOR_RECORD */
	SELECT
	  Associate_Seq_Num
	, Location_Seq_Num
	, Location_Zone_Seq_Num 
	, t2.Store_Seq_Num
	, DC_Seq_Num
	, Cost_Center_Seq_Num
	, 1 as Junk_seq_Num
	, Work_Shift_Cd
	, t2.Labor_Dt
	, Labor_Hour_Of_Day_Tm
	, Work_Shift_Class_Seq_Num
	, Associate_Manager_Seq_Num
	, Actual_Labor_Minute_Qty
	, Actual_Labor_Cost_Amt
	, Actual_OT_Labor_Cost_Amt
	, Actual_OT_Labor_Minute_Qty	
	, Actual_QOT_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Minute_Qty	
	, ReAlloc_Actual_QOT_Labor_Minute_Qty
	, OB_Minute_Qty
	, ReAlloc_OB_Minute_Qty		
	, Wage_Rate_Type_Cd
	, Overtime_Wage_Rate_Type_Cd
	, Labor_Task_Status_Cd
	, Source_System_Cd
	FROM ${DB_ENV}SemCMNVin.ASSOCIATE_LABOR_TASK_QUINYX as t2 
	;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ASSOCIATE_LABOR_TASK_ACTUAL_F IS '$Revision: 24973 $ - $Date: 2018-05-29 09:30:52 +0200 (tis, 29 maj 2018) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
