/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STORE_AREA_MONTH_F.btq 25084 2018-06-12 07:28:55Z a54632 $
# Last Changed By  : $Author: a54632 $
# Last Change Date : $Date: 2018-06-12 09:28:55 +0200 (tis, 12 jun 2018) $
# Last Revision    : $Revision: 25084 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/STORE_AREA_MONTH_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemCMNVOUT	 -- Change db name if needed
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- We create the view

Replace	View ${DB_ENV}SemCMNVOUT.STORE_AREA_MONTH_F

(
Store_Seq_Num,
Junk_Seq_Num,
Calendar_Month_Id,
Total_Area_EOM,
Sales_Area_EOM
)
As LOCKING ROW FOR ACCESS --Dirty read needed as table db is used
SELECT DISTINCT s0.Store_Seq_Num
    ,j1.Junk_Seq_Num
    ,cm0.Calendar_Month_Id
    ,NULLIFZERO(CAST(stv01.Location_Trait_Val AS INTEGER)) AS Total_Area_EOM
    ,NULLIFZERO(CAST(stv02.Location_Trait_Val AS INTEGER)) AS Sales_Area_EOM
FROM ${DB_ENV}TgtVOUT.STORE AS s0
CROSS JOIN ${DB_ENV}SemCMNVOUT.CALENDAR_MONTH_D AS cm0
LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.STORE_TRAIT_VALUE_B AS stv01
    ON stv01.Store_Seq_num = s0.Store_Seq_Num
    AND stv01.Location_Trait_Cd = 'TAE'
    AND cm0.Calendar_Month_End_Dttm BETWEEN stv01.Valid_From_Dttm AND stv01.Valid_To_Dttm
LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.STORE_TRAIT_VALUE_B AS stv02
    ON  stv02.Store_Seq_num = s0.Store_Seq_Num
    AND stv02.Location_Trait_Cd = 'SAE'
    AND cm0.Calendar_Month_End_Dttm BETWEEN stv02.Valid_From_Dttm AND stv02.Valid_To_Dttm
LEFT OUTER  JOIN ${DB_ENV}TgtVOUT.LOCATION_STATUS_B lsb
	ON s0.Store_Seq_Num = lsb.Location_Seq_Num
	AND Location_Status_Type_Cd = 'OPE'
	AND lsb.valid_to_dttm = '9999-12-31 23:59:59.999999'  
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
	On stv01.Store_Seq_num = std1.Store_Seq_Num 
	and cm0.Calendar_Month_Start_Dt = std1.Calendar_Dt
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
	on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
	and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
	and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
    and j1.Loyalty_Ind = 0  	
WHERE cast(cast(current_timestamp as format'YYYY-MM-DD') as CHAR(10)) >= cm0.Calendar_Month_Start_Dt
	AND stv01.Location_Trait_Val is not null
    AND cm0.Calendar_Month_Start_Dt between Location_Status_Start_Dt and Location_Status_End_Dt
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.STORE_AREA_MONTH_F IS '$Revision: 25084 $ - $Date: 2018-06-12 09:28:55 +0200 (tis, 12 jun 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
