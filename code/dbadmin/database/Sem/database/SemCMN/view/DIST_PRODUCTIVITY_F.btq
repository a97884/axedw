/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DIST_PRODUCTIVITY_F.btq 24287 2018-02-14 12:11:46Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-02-14 13:11:46 +0100 (ons, 14 feb 2018) $
# Last Revision    : $Revision: 24287 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/DIST_PRODUCTIVITY_F.btq $
# ---------------------------------------------------------------------------
# SVN Info END
# ---------------------------------------------------------------------------
*/

Database ${DB_ENV}SemCMNVOUT
;



.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.DIST_PRODUCTIVITY_F
(
  Location_Seq_Num
 ,Location_Zone_Seq_Num 
 ,Associate_Seq_Num 
 ,Calendar_Dt 
 ,Hour_Of_Day 
 ,Associate_Manager_Seq_Num 
 ,User_Group_Cd
 ,Calendar_Week_id
 ,Calendar_Month_id 
 ,Threshold_Val_Seq_Num
 
 ,Total_Actual_Minute_Qty
 ,Total_Productivity_Val
 ,Actual_Minute_Qty
 ,Parcel_Qty
 ,Pallet_Qty
 
 ,Picking_Productivity_Val
 ,Picking_Actual_Minute_Qty
 ,Parcel_Pick_Qty
 ,Parcel_Pick_Weighting_Qty 
 ,Pallet_Pick_Qty
 
 ,Lift_Productivity_Val
 ,Lift_Actual_Minute_Qty
 ,Lift_Mission_Qty
 ,Lift_Pallet_In_Qty
 ,Lift_Pallet_Out_Qty
 ,Lift_Refill_Qty
 ,Lift_Pallet_Transfer_Qty
 ,Lift_Pallet_HighBay_Out_Qty
 
 ,Undefined_Parcel_Qty
 ,Undefined_Pallet_Qty
 ,Undefined_Actual_Minute_Qty
 
 ,Parcel_Pick_Row_Cnt
 ,Parcel_Pick_Filter_Row_Cnt 
 ,Pallet_Weighting_Qty

) As

LOCK ROW FOR ACCESS
SELECT 
Location_Seq_Num,
Location_Zone_Seq_Num,
Associate_Seq_Num,
Calendar_Dt,
Hour_Of_Day,
coalesce(Associate_Manager_Seq_Num,-1) Associate_Manager_Seq_Num,
User_Group_Cd,
Calendar_Week_id,
Calendar_Month_id,
-1 As Threshold_Val_Seq_Num,
	
sum(Total_Actual_Minute_Qty) as Total_Actual_Minute_Qty,
sum(Total_Productivity_Val) as Total_Productivity_Val,
sum(Picking_Actual_Minute_Qty+Lift_Actual_Minute_Qty+Admin_Actual_Minute_Qty) As Actual_Minute_Qty,
sum(Parcel_Pick_Qty) as Parcel_Qty,
sum(Lift_Pallet_Out_Qty+Lift_Pallet_HighBay_Out_Qty) as Pallet_Qty,
 
sum(Picking_Productivity_Val) as Picking_Productivity_Val,
sum(Picking_Actual_Minute_Qty) as Picking_Actual_Minute_Qty,
sum(Parcel_Pick_Qty) as Parcel_Pick_Qty,
sum(Parcel_Pick_Weighting_Qty) as Parcel_Pick_Weighting_Qty, 
sum(Pallet_Pick_Qty) as Pallet_Pick_Qty,
 
sum(Lift_Productivity_Val) as Lift_Productivity_Val,
sum(Lift_Actual_Minute_Qty) as Lift_Actual_Minute_Qty,
sum(Lift_Pallet_In_Qty+Lift_Pallet_Out_Qty+Lift_Refill_Qty+Lift_Pallet_Transfer_Qty+Lift_Pallet_HighBay_Out_Qty) as Lift_Mission_Qty,
sum(Lift_Pallet_In_Qty) as Lift_Pallet_In_Qty,
sum(Lift_Pallet_Out_Qty) as Lift_Pallet_Out_Qty,
sum(Lift_Refill_Qty) as Lift_Refill_Qty,
sum(Lift_Pallet_Transfer_Qty) as Lift_Pallet_Transfer_Qty,
sum(Lift_Pallet_HighBay_Out_Qty) as Lift_Pallet_HighBay_Out_Qty,
  
sum(Undefined_Parcel_Qty) as Undefined_Parcel_Qty,
sum(Undefined_Pallet_Qty) as Undefined_Pallet_Qty,
sum(Undefined_Actual_Minute_Qty) as Undefined_Actual_Minute_Qty,
 
sum(Parcel_Pick_Row_Cnt) as Parcel_Pick_Row_Cnt,
sum(Parcel_Pick_Filter_Row_Cnt) as Parcel_Pick_Filter_Row_Cnt,
sum((Lift_Pallet_Out_Qty+Lift_Pallet_HighBay_Out_Qty)*Weighting_Val_Pallet_Qty) as Pallet_Weighting_Qty

FROM 
(
	SELECT 
    de.Supply_Location_Seq_Num as Location_Seq_Num
  , de.Supply_Location_Zone_Seq_Num  as Location_Zone_Seq_Num
  , de.Associate_Seq_Num
  , cd1.Calendar_Dt
  , Cast(substr('00'||Cast(de.Event_Hour as Char(2)),Length(Cast(de.Event_Hour as varChar(2)))+1,2)||':00:00' As Time(0)) As Hour_Of_Day
  , COALESCE(am.Associate_Manager_Seq_Num,-1) AS Associate_Manager_Seq_Num
  , Trim(Coalesce(deg.User_Group_Cd,'')) As User_Group_Cd
  , cd1.Calendar_Week_id
  , cd1.Calendar_Month_id  
  
  , null As Total_Actual_Minute_Qty
  , 0 As Total_Productivity_Val
  , null As Picking_Actual_Minute_Qty
  , null As Lift_Actual_Minute_Qty
  , null As Admin_Actual_Minute_Qty
  , null As Undefined_Actual_Minute_Qty
  
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then Actual_Qty Else 0 End) As Parcel_Pick_Qty
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then (Actual_Qty * Weighting_Val) Else 0 End) As Parcel_Pick_Weighting_Qty   
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 1 And coalesce(To_Storage_Location_ID,'') <> '' Then Row_Cnt Else 0 End) As Pallet_Pick_Qty
  , (Case When Dist_Mission_Cd = 'INFACKNING' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And Associate_User_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Row_Cnt Else 0 End) As Lift_Pallet_In_Qty
  , (Case When Dist_Mission_Cd = 'HELPALL' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And Associate_User_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020'  Then Row_Cnt Else 0 End) As Lift_Pallet_Out_Qty
  , (Case When Dist_Mission_Cd = 'PAFYLLNING' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And Associate_User_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Row_Cnt Else 0 End) As Lift_Refill_Qty
  , (Case When Dist_Mission_Cd = 'FLYTT' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And Associate_User_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Row_Cnt Else 0 End) As Lift_Pallet_Transfer_Qty
  , (Case When Dist_Mission_Cd = 'HBAY' And coalesce(To_Storage_Location_ID,'') <> '' Then Row_Cnt Else 0 End) As Lift_Pallet_HighBay_Out_Qty
  , (Case When Dist_Mission_Cd = '' Then Actual_Qty Else 0 End) As Undefined_Parcel_Qty
  , (Case When Dist_Mission_Cd = '' Then Row_Cnt Else 0 End) As Undefined_Pallet_Qty  
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then Row_Cnt Else 0 End) As Parcel_Pick_Row_Cnt
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') And Actual_Qty <> 0 Then Row_Cnt Else 0 End) As Parcel_Pick_Filter_Row_Cnt
  , 0 As Picking_Productivity_Val
  , 0 As Lift_Productivity_Val
  , de.Weighting_Val_Pallet_Qty
		
	FROM ${DB_ENV}SemCmnVOUT.DIST_EVENT_F de
	LEFT OUTER JOIN ${DB_ENV}SemCmnVOUT.DIST_EVENT_GROUP_D deg
	ON de.Dist_Event_Group_Seq_Num = deg.Dist_Event_Group_Seq_Num
		
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ASSOCIATE_MANAGER am
    ON de.Associate_Seq_Num = am.Associate_Seq_Num and am.Valid_To_Dt = '9999-12-31'

	INNER JOIN ${DB_ENV}SemCmnVOUT.CALENDAR_DAY_D cd1
	ON de.Event_Dt = cd1.Calendar_Dt		
	
	UNION ALL
	
	SELECT 
    lb.Location_Seq_Num
  , lb.Location_Zone_Seq_Num 
  , Case When Shift_Type_Cd = 'Medarbetare' Then lb.Associate_Seq_Num Else -1 End As Associate_Seq_Num
  , cd2.Calendar_Dt
  , lb.Labor_Hour_Of_Day_Tm As Hour_Of_Day
  , Associate_Manager_Seq_Num
  , Trim(Coalesce(wsc.User_Group_Cd,'')) As User_Group_Cd
  , cd2.Calendar_Week_id
  , cd2.Calendar_Month_id  
  
  , Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty As Total_Actual_Minute_Qty
  , 0 As Total_Productivity_Val
  , Case When Shift_Class_Cd = 'Plock' Then Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty Else 0 End As Picking_Actual_Minute_Qty
  , Case When Shift_Class_Cd = 'Lyft' Then Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty Else 0 End As Lift_Actual_Minute_Qty
  , Case When Shift_Class_Cd = 'Lager' Then Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty Else 0 End As Admin_Actual_Minute_Qty
  , Case When Shift_Class_Cd Not In ( 'Lager','Lyft','Plock' ) Then Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty Else 0 End As Undefined_Actual_Minute_Qty
  
  , null As Parcel_Pick_Qty
  , null As Parcel_Pick_Weighting_Qty  
  , null As Pallet_Pick_Qty
  , null As Lift_Pallet_In_Qty
  , null As Lift_Pallet_Out_Qty
  , null As Lift_Refill_Qty
  , null As Lift_Pallet_Transfer_Qty
  , null As Lift_Pallet_HighBay_Out_Qty
  , null As Undefined_Parcel_Qty
  , null As Undefined_Pallet_Qty  
  , null As Parcel_Pick_Row_Cnt
  , null As Parcel_Pick_Filter_Row_Cnt  
  
  , 0 As Picking_Productivity_Val
  , 0 As Lift_Productivity_Val
  , 0 as Weighting_Val_Pallet_Qty
		
	FROM ${DB_ENV}SemCmnVOUT.ASSOCIATE_LABOR_TASK_ACTUAL_F lb
	INNER JOIN ${DB_ENV}SemCmnVOUT.WORK_SHIFT_CLASS_D wsc
		ON lb.Work_Shift_Class_Seq_Num = wsc.Work_Shift_Class_Seq_Num
	INNER JOIN ${DB_ENV}SemCmnVOUT.CALENDAR_DAY_D cd2
	ON lb.Labor_Dt = cd2.Calendar_Dt			
	WHERE Source_System_Cd = 'QUINYX'
	
) as x

group by 1,2,3,4,5,6,7,8,9,10
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.DIST_PRODUCTIVITY_F IS '$Revision: 24287 $ - $Date: 2018-02-14 13:11:46 +0100 (ons, 14 feb 2018) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
