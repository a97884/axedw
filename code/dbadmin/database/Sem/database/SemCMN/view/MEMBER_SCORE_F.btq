/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: MEMBER_SCORE_F.btq 16748 2015-08-10 09:36:54Z K9105286 $
# Last Changed By  : $Author: K9105286 $
# Last Change Date : $Date: 2015-08-10 11:36:54 +0200 (mån, 10 aug 2015) $
# Last Revision    : $Revision: 16748 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/MEMBER_SCORE_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
Database ${DB_ENV}SemCMNVOUT
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.MEMBER_SCORE_F
(
	Model_Apply_Dt,
	Calendar_Month_Id,
	Member_Account_Seq_Num,
	RFM_Lvl,
	Revenue_Lvl,
	SoW_Lvl,
	Kluster_Lvl,
	Kluster_Name,
	Matris9_Lvl,
	Matris9_Name,
	Top_Btm_100000_Name,
	Specgrupp_Gluten_Name,
	Specgrupp_Laktos_Name,
	Specgrupp_EKO_Name,
	Specgrupp_Barn_Name,
	Specgrupp_Master_Name,
	Top_Rank_Lvl,
	Grand_Amt_3000_3mths_Ind_Name,
	Sales_inc_Tax_1Mths_Val,
	Recpts_1Mths_Val,
	Avg_Purchase_inc_Tax_1Mths_Val,
	BV_1Mths_Val,
	BV_pct_1Mths_Val,
	InsertDttm
	) 
As

Select
	lms.Model_Apply_End_Dt as Model_Apply_Dt,
	c.Calendar_Month_Id as Calendar_Month_Id,
	lms.Member_Account_Seq_Num as Member_Account_Seq_Num,
 
	Max(
	Case When  lms.Model_Id In(1001,1002)
	Then  lms.Member_Lvl
	Else null
	End
	) as RFM_Lvl,

	Max(
	Case When  lms.Model_Id In(1003,1004)
	Then  lms.Member_Lvl
	Else null
	End
	) as Revenue_Lvl,

	Max(
	Case When  lms.Model_Id In(1005,1006)
	Then  lms.Member_Lvl
	Else null
	End
	) as Sow_Lvl,

	Max(
	Case When  lms.Model_Id In(1007,1008)
	Then  lms.Member_Lvl
	Else null
	End
	) as Kluster_Lvl,

	Max(
	Case When  lms.Model_Id In(1007,1008)
	Then  lms.Member_Desc
	Else null
	End
	) as Kluster_Name,

	Max(
	Case When  lms.Model_Id In(1009,1010)
	Then  lms.Member_Lvl
	Else null
	End
	) as Matris9_Lvl,

	Max(
	Case When  lms.Model_Id In(1009,1010)
	Then  lms.Member_Desc
	Else null
	End
	) as Matris9_Name,

	Max(
	Case When  lms.Model_Id In(1011,1012)
	Then  lms.Member_Desc
	Else null
	End
	) as Top_Btm_100000_Name,

	Max(
	Case When  lms.Model_Id In(1013,1014)
	Then  lms.Member_Desc
	Else null
	End
	) as Specgrupp_Gluten_Name,

	Max(
	Case When  lms.Model_Id In(1015,1016)
	Then  lms.Member_Desc
	Else null
	End
	) as Specgrupp_Laktos_Name,

	Max(
	Case When  lms.Model_Id In(1017,1018) 
	Then  lms.Member_Desc
	Else null
	End
	) as Specgrupp_EKO_Name,

	Max(
	Case When  lms.Model_Id In(1019,1020)
	Then  lms.Member_Desc
	Else null
	End
	) as Specgrupp_Barn_Name,

	Max(
	Case When  lms.Model_Id In(1021,1022)
	Then  lms.Member_Desc
	Else null
	End
	) as Specgrupp_Master_Name,

	Max(
	Case When  lms.Model_Id In(1023,1024)
	Then  lms.Member_Lvl
	Else null
	End
	) as Top_Rank_Lvl,

	Max(
	Case When  lms.Model_Id In(1025,1026)
	Then  lms.Member_Desc
	Else null
	End
	) as Grand_Amt_3000_3mths_Name,

	Max(
	Case When  lms.Model_Id In(1027,1028)
	Then  lms.Member_Val
	Else null
	End
	) as Sales_inc_Tax_1Mths_Score,

	Max(
	Case When  lms.Model_Id In(1029,1030)
	Then  lms.Member_Val
	Else null
	End
	) as Recpts_1Mths_Score,

	Max(
	Case When  lms.Model_Id In(1031,1032)
	Then  lms.Member_Val
	Else null
	End
	) as Avg_Pur_inc_Tax_1Mths_Score,
	
	Max(	
	Case When  lms.Model_Id In(1033,1034)
	Then  lms.Member_Val
	Else null
	End
	) as BV_1Mths_Score,
		
	Max(	
	Case When  lms.Model_Id In(1035,1036)
	Then  lms.Member_Val
	Else null
	End
	) as BV_pct_1Mths_Score,
	
--Get last run per model and model data period		
	Max(lms.InsertDttm) as InsertDttm

From  ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_SCORE as lms
Inner Join ${DB_ENV}TgtVOUT.ANALYTICAL_MODEL as am
	On lms.Model_Id = am.Model_Id
	And am.Model_Id between 1001 and 2000
	And am.Model_Status_Cd='Active'
	
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
	On lms.Model_Apply_End_Dt = c.Calendar_Dt
	
Group by 1,2,3
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.MEMBER_SCORE_F IS '$Revision: 16748 $ - $Date: 2015-08-10 11:36:54 +0200 (mån, 10 aug 2015) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

