/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DC_D.btq 26462 2018-12-13 07:16:50Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-12-13 08:16:50 +0100 (tor, 13 dec 2018) $
# Last Revision    : $Revision: 26462 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/DC_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemCMNVOUT.DC_D
(	DC_Seq_Num
	,DC_Id
	,DC_Name
	,Operational_From_Dt
	,Operational_To_Dt
	,GLN_Id
	,Locator_Seq_Num
	,Address_Seq_Num
) AS
LOCKING ROW FOR ACCESS
	SELECT	l1.Location_Seq_Num As DC_Seq_Num
		,l1.N_Location_Id	AS DC_Id
		,COALESCE(ln1.Location_Name, 'SAKNAS') AS DC_Name
		,COALESCE(ls1.Location_Status_Start_Dt, SYSLIB.LowDTVal())	AS Operational_From_Dt
		,COALESCE(ls1.Location_Status_End_Dt, SYSLIB.HighDTVal())	AS Operational_To_Dt
		,lg1.GLN_Id
		,Coalesce(ll1.Locator_Seq_Num,-1 ) As Locator_Seq_Num
		,Coalesce(la1.Address_Seq_Num, -1) As Address_Seq_Num
	FROM ${DB_ENV}TgtVOUT.LOCATION AS l1
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION_NAME_B AS ln1
		ON	ln1.Location_Seq_Num = l1.Location_Seq_Num
		AND	ln1.Valid_To_Dttm = SYSLIB.HighTSVal()
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION_ADDRESS AS la1
		ON	la1.Location_Seq_Num = l1.Location_Seq_Num
		AND	la1.Address_Usage_Cd = 'OFF'
		AND	la1.Valid_To_Dttm = SYSLIB.HighTSVal()
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION_LOCATOR AS ll1
		ON	ll1.Location_Seq_Num = l1.Location_Seq_Num
		AND	ll1.Valid_To_Dttm = SYSLIB.HighTSVal()
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION_GLN_B AS lg1
		ON	lg1.Location_Seq_Num = l1.Location_Seq_Num
		AND	lg1.Valid_To_Dttm = SYSLIB.HighTSVal()
	LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION_STATUS_B AS ls1
		ON	ls1.Location_Seq_Num = l1.Location_Seq_Num
		AND	ls1.Location_Status_Type_Cd = 'OPE'
		AND	ls1.Valid_To_Dttm = SYSLIB.HighTSVal()
	WHERE l1.Location_Type_Cd = 'DC'

UNION ALL

SELECT	
d0.Dummy_Seq_Num AS DC_Seq_Num
,d0.Dummy_Id (INTEGER) AS DC_Id
,d0.Dummy_Desc AS DC_Name
,SYSLIB.LowDTVal() AS Operational_From_Dt
,SYSLIB.HighDTVal() AS Operational_To_Dt
,d0.Dummy_Id AS GLN_Id
,d0.Dummy_Seq_Num AS Locator_Seq_Num
,d0.Dummy_Seq_Num AS Address_Seq_Num
FROM ${DB_ENV}SemCMNT.DUMMY d0
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.DC_D IS '$Revision: 26462 $ - $Date: 2018-12-13 08:16:50 +0100 (tor, 13 dec 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT TOP 1 * FROM ${DB_ENV}SemCMNVOUT.DC_D
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE