/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ER_SAP_STORE_ART_W_SALES_F.btq 17532 2015-11-09 09:50:50Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2015-11-09 10:50:50 +0100 (mån, 09 nov 2015) $
# Last Revision    : $Revision: 17532 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/ER_SAP_STORE_ART_W_SALES_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemEXPVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemEXPVOUT.ER_SAP_STORE_ART_W_SALES_F 
( 
	Article_Id
	, UOM_Cd
	, Store_Id
	, Calendar_Week_Id	
	, Calendar_Week_End_Dt
	, Campaign_Sales_Ind
	, Unit_Selling_Price_Amt
	, Unit_Cost_Price_Amt
	, Item_Qty
	, Store_Margin_Amt
	, Unit_Inv_Cnt
) 
As
Lock row for access
Select 
	Substr('000000000000000000' || s.Article_Id,character_length(s.Article_Id)+1,18) as Article_Id
	, s.UOM_Cd
	, s.Store_Id
	, s.Calendar_Week_Id	
	, calw.Calendar_Week_End_Dt
	, s.Campaign_Sales_Ind
	, Sum(s.Unit_Selling_Price_Amt) as Unit_Selling_Price_Amt
	, Sum(s.Unit_Cost_Amt) as Unit_Cost_Price_Amt
	, Sum(s.Item_Qty) as Item_Qty
	, Sum(s.Store_Margin_Amt) as Store_Margin_Amt
	, Max(Perpetual_Inv_Unit_Qty) as Unit_Inv_Cnt
From
(	
	select 
		f.Article_Seq_Num
		, f.Store_Seq_Num
		, c.Calendar_Week_Id
		, f.Perpetual_Inv_Unit_Qty As Perpetual_Inv_Unit_Qty
	From ${DB_ENV}SemCMNVOUT.PERPETUAL_INVENTORY_F f
	Join ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D c On c.Calendar_Dt = f.Perpetual_Inv_Dt
	--Where c.Calendar_Dt BETWEEN current_date - INTERVAL '42' DAY  AND current_date
	Where c.Calendar_Dt BETWEEN current_date - INTERVAL '365' DAY  AND current_date --added for early testing purposes
	And c.Day_Of_Week_Num = 7 
	
	Union All
	/* Senaste veckans senaste saldo */
	select 
		f.Article_Seq_Num
		, f.Store_Seq_Num
		, c.Calendar_Week_Id
		, f.Perpetual_Inv_Unit_Qty As Perpetual_Inv_Unit_Qty
	From ${DB_ENV}SemCMNVOUT.PERPETUAL_INVENTORY_F f
	Join ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D c On c.Calendar_Dt = f.Perpetual_Inv_Dt
	Where (f.Article_Seq_Num, f.Store_Seq_Num, f.Perpetual_Inv_Dt) 
	In
	(
		select 
			f.Article_Seq_Num
			, f.Store_Seq_Num
			, Max(f.Perpetual_Inv_Dt) as Perpetual_Inv_Dt
		From ${DB_ENV}SemCMNVOUT.PERPETUAL_INVENTORY_F f
		Where f.Perpetual_Inv_Dt In 
		( 
			Select c1.Calendar_Dt 
			From ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D c1
			Join ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D c2 On c1.Calendar_Week_Id = c2.Calendar_Week_Id And c1.Day_Of_Week_Num <= c2.Day_Of_Week_Num
			Where c1.Calendar_Dt BETWEEN current_date - INTERVAL '7' DAY  AND current_date
			And c2.Calendar_Dt = current_date
		)	
	Group by 
		f.Article_Seq_Num
		, f.Store_Seq_Num
	)
) i 
Inner Join 
(
	Select 
		 scd.Article_Seq_Num,
		 ad.Article_Id,    
		 mud.MU_UOM_Cd as UOM_Cd,
		 stld.Store_Seq_Num,
		 sd.Store_Id,
		 stld.Calendar_Week_Id,		 
		 stld.Campaign_Sales_Ind,
		 stld.Unit_Selling_Price_Amt,
		 stld.Unit_Cost_Amt,
		 stld.Item_Qty,
		 stld.Store_Margin_Amt
	From 
	(
		select 
			Scan_Code_Seq_Num
			, Store_Seq_Num
			, Calendar_Week_Id			
			, Case When Campaign_Sales_Type_Cd <> 'N' Then 1 Else 0 End As Campaign_Sales_Ind
			, Sum(Unit_Selling_Price_Amt) As Unit_Selling_Price_Amt
		 	, Sum(Unit_Cost_Amt) As Unit_Cost_Amt
		 	, Sum(Item_Qty) As Item_Qty
		 	, Sum(GP_Unit_Selling_Price_Amt) - Sum(Unit_Cost_Amt) AS Store_Margin_Amt
		From ${DB_ENV}SemCMNVOUT.SALES_TRAN_LINE_DAY_F
		--Where Calendar_Week_Id in (Select distinct Calendar_Week_Id from ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D Where Calendar_Dt BETWEEN current_date - INTERVAL '42' DAY  AND current_date )
		Where Calendar_Week_Id in (Select distinct Calendar_Week_Id from ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D Where Calendar_Dt BETWEEN current_date - INTERVAL '365' DAY  AND current_date ) --added for early testing purposes
		Group By 
			Scan_Code_Seq_Num
			, Calendar_Week_Id
			, Store_Seq_Num
			, Case When Campaign_Sales_Type_Cd <> 'N' Then 1 Else 0 End 
	)  stld
	Join ${DB_ENV}SemCMNVOUT.SCAN_CODE_D scd 
	On stld.Scan_Code_Seq_Num = scd.Scan_Code_Seq_Num
	Join ${DB_ENV}SemCMNVOUT.MEASURING_UNIT_D  mud 
	On scd.Measuring_Unit_Seq_Num = mud.Measuring_Unit_Seq_Num
	Join ${DB_ENV}SemCMNVOUT.ARTICLE_D  ad 
	On ad.Article_Seq_Num = mud.Article_Seq_Num
	Join ${DB_ENV}SemCMNVOUT.STORE_D sd 
	On sd.Store_Seq_Num = stld.Store_Seq_Num
	Where mud.Mu_Id <> '-1' 
) s 
On i.Article_Seq_Num = s.Article_Seq_Num
And i.Store_Seq_Num = s.Store_Seq_Num
And i.Calendar_Week_Id = s.Calendar_Week_Id
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_WEEK calw on calw.Calendar_Week_Id = i.Calendar_Week_Id
Group By 
	s.Article_Id
	, s.UOM_Cd
	, s.Store_Id
	, s.Calendar_Week_Id	
	, calw.Calendar_Week_End_Dt
	, s.Campaign_Sales_Ind;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* Update the statement with the corresponding view name */
COMMENT ON ${DB_ENV}SemEXPVOUT.ER_SAP_STORE_ART_W_SALES_F  IS '$Revision: 17532 $ - $Date: 2015-11-09 10:50:50 +0100 (mån, 09 nov 2015) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE