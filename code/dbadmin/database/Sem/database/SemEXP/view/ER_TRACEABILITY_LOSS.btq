/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ER_TRACEABILITY_LOSS.btq 29321 2019-10-17 16:29:43Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-10-17 18:29:43 +0200 (tor, 17 okt 2019) $
# Last Revision    : $Revision: 29321 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/ER_TRACEABILITY_LOSS.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemEXPVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVOUT.ER_TRACEABILITY_LOSS ( 
Sales_Tran_Seq_Num,
Party_Seq_Num,
Store_Seq_Num,
Source_Own_Party,
Source_Location,
Destination_Own_Party,
Destination_Location,
Destination_Name,
Destination_Address,
Destination_Postal_Cd,
Destination_City,
Tran_Dt_DD,
Tran_End_Dttm_DD,
InsertDttm,
Scan_Code_Seq_Num,
Scan_Cd,
Traceability_Type_Cd,
Item_Qty,
NetWeight,
UOM_Category_Cd,
LGTIN,
EventID,
EventType,
Action 
) AS
SELECT 
Sales_Tran_Seq_Num,
Party_Seq_Num,
Store_Seq_Num,
Source_Own_Party,
Source_Location,
Destination_Own_Party,
Destination_Location,
Destination_Name,
Destination_Address,
Destination_Postal_Cd,
Destination_City,
Tran_Dt_DD,
Tran_End_Dttm_DD,
InsertDttm,
Scan_Code_Seq_Num,
Scan_Cd,
Traceability_Type_Cd,
Item_Qty,
NetWeight,
UOM_Category_Cd,
LGTIN,
EventID,
EventType,
Action
FROM
(
SELECT	st1.Inv_Reg_Batch_Id AS Sales_Tran_Seq_Num,
NULL AS Party_Seq_Num,
st1.Location_Seq_Num Store_Seq_Num,
'732024.080006.0' Source_Own_Party,
lgb.Source_GTIN Source_Location,
'N/A' Destination_Own_Party,
'N/A' Destination_Location,
'N/A' Destination_name,
'N/A' Destination_Address,
'N/A' Destination_Postal_Cd,
'N/A' Destination_City,
st1.Adjustment_Dt Tran_Dt_DD,
st1.Adjustment_Dt Tran_End_Dttm_DD,
st1.InsertDttm,
st1.Scan_Code_Seq_Num,
s2.scan_cd scan_cd,
atm.traceability_type_cd,
st1.Adjust_Qty AS Item_Qty,
COALESCE(sclu1.UOM_Category_Cd,'-1') AS UOM_Category_Cd,
oreplace(COALESCE(prfx.LGTIN, s2.scan_cd),' ','')||'.'||Traceability_Id AS LGTIN,
CASE WHEN a1.Article_Type_Cd = 'ZHAW' AND COALESCE(ad1.Net_Weight,0) <> 0 AND ad1.Net_Qty_UOM_Cd = 'KG' THEN 
st1.Adjust_Qty * (ad1.Net_Weight/1000) ELSE st1.Adjust_Qty END (DECIMAL (18,4))  AS NetWeight,
'ADD' AS Action,
'Decommissioning' AS EventType,
CAST(st1.Inv_Reg_Batch_Id AS VARCHAR(50)) || '.' || CAST(st1.Scan_Code_Seq_Num AS VARCHAR(10)) || '.' || CAST(st1.Location_Seq_Num AS VARCHAR(10))   
|| '.' || CAST(st1.Adjustment_Dt AS VARCHAR(10)) || '.' || st1.Known_Stock_Loss_Adj_Reason_Cd AS EventId

FROM ${DB_ENV}TgtVOUT.KNOWN_STOCK_LOSS AS st1

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.MU_SCAN_CODE_LOC_UOM_B AS sclu1
ON	sclu1.Scan_Code_Seq_num = st1.Scan_Code_Seq_Num
AND	sclu1.Location_Seq_Num = st1.Location_Seq_Num
AND	st1.Adjustment_Dt BETWEEN sclu1.Valid_From_Dttm AND sclu1.Valid_To_Dttm

INNER JOIN ${DB_ENV}SemCMNVOUT.SCAN_CODE_D AS s2
ON s2.Scan_Code_Seq_Num = st1.Scan_Code_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.MEASURING_UNIT_D AS mu1
ON s2.Measuring_Unit_Seq_Num = mu1.Measuring_Unit_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D AS a1
ON s2.ARTICLE_SEQ_NUM = a1.ARTICLE_SEQ_NUM

INNER JOIN (
select at1.article_seq_num,sc1.scan_code_seq_num,sc1.scan_cd
from
(SELECT article_seq_num
FROM ${DB_ENV}TgtVOUT.ARTICLE_TRACEABILITY
) as at1
INNER JOIN ${DB_ENV}SEMCMNVOUT.SCAN_CODE_D as sc1
on at1.article_seq_num = sc1.article_seq_num
INNER JOIN ${DB_ENV}SEMCMNVOUT.MEASURING_UNIT_D as mu1
on mu1.Measuring_Unit_Seq_Num = sc1.Measuring_Unit_Seq_Num
QUALIFY ROW_NUMBER() OVER (PARTITION BY  at1.article_seq_num ORDER BY mu1.Ordering_MU_Ind DESC) = 1
) as scan_code_Rank
on scan_code_Rank.article_seq_num = a1.article_seq_num

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_DETAILS_B AS ad1
ON s2.ARTICLE_SEQ_NUM = ad1.ARTICLE_SEQ_NUM
AND ad1.Valid_To_Dttm = syslib.hightsval()

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_TRACEABILITY atm
ON s2.article_seq_num = atm.article_seq_num
AND atm.Valid_To_Dttm = syslib.hightsval()

INNER JOIN (
SELECT 
comp_prefix||'.'||item_ref||'.'||'0' AS Source_GTIN,
GLN_Id,prefix_,gcplength,
Location_Seq_Num,
store_name,store_id,
concept_cd
FROM
(
SELECT SUBSTRING(pre_comp_prefix
FROM 1 FOR gcplength ) AS comp_prefix,
SUBSTRING(pre_comp_prefix FROM gcplength + 1  FOR length(pre_comp_prefix) - 1 ) AS  item_ref,
CASE WHEN length(GLN_Id) = 14 THEN SUBSTRING( GLN_Id FROM 1 FOR 1 ) ELSE '0' END AS Pack_Ind,
GLN_Id,prefix_,gcplength,
Location_Seq_Num,
store_name,store_id,
concept_cd
FROM
(
SELECT 
GLN_Id,
Store_Seq_Num AS Location_seq_num,
store_name,store_id,
concept_cd,
CASE WHEN length(GLN_Id) = 14 THEN SUBSTRING(GLN_Id FROM 2 FOR length(GLN_Id) - 2)
ELSE SUBSTRING(GLN_Id FROM 1 FOR length(GLN_Id) - 1) END AS pre_comp_prefix,
prefix_,
gcplength
FROM ${DB_ENV}SemCMNVOUT.STORE_D sourc
INNER JOIN ${DB_ENV}SemEXPVOUT.ER_GTIN gc
ON 1 = 1
WHERE instr (GLN_Id,prefix_) > 0
) AS x
QUALIFY ROW_NUMBER() OVER (PARTITION BY  GLN_Id ORDER BY gcplength) = 1
) AS z
) lgb
ON  st1.Location_Seq_Num = lgb.Location_Seq_Num

INNER JOIN (
SELECT comp_prefix||'.'||Pack_Ind||item_ref AS LGTIN,
scan_cd,Scan_Code_Seq_Num
FROM
(
SELECT SUBSTRING(pre_comp_prefix
FROM 1 FOR gcplength ) AS comp_prefix,
SUBSTRING(pre_comp_prefix FROM gcplength + 1  FOR length(pre_comp_prefix) - 1 ) AS  item_ref,
CASE WHEN length(scan_cd) = 14 THEN SUBSTRING( scan_cd FROM 1 FOR 1 ) ELSE '0' END AS Pack_Ind,
scan_cd,Scan_Code_Seq_Num
FROM
(
SELECT n_scan_cd AS scan_cd,
Scan_Code_Seq_Num,
CASE WHEN length(scan_cd) = 14 THEN SUBSTRING(n_scan_cd FROM 2 FOR length(n_scan_cd) - 2)
ELSE SUBSTRING(n_scan_cd FROM 1 FOR length(n_scan_cd) - 1) END AS pre_comp_prefix,
prefix_,gcplength 
FROM ${DB_ENV}TgtVOUT.MU_SCAN_CODE sc
INNER JOIN ${DB_ENV}SemEXPVOUT.ER_GTIN gc
ON 1 = 1
WHERE instr (sc.n_scan_cd,prefix_) > 0
) AS x
) AS z
) prfx
ON scan_code_Rank.Scan_Code_Seq_Num = prfx.Scan_Code_Seq_Num

WHERE coalesce(st1.Traceability_Id,'-1') <> '-1'

) as  x

;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemEXPVOUT.ER_TRACEABILITY_LOSS  IS '$Revision: 29321 $ - $Date: 2019-10-17 18:29:43 +0200 (tor, 17 okt 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

