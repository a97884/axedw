/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: GFK_PRODUCTS.btq 21700 2017-02-22 13:33:06Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-02-22 14:33:06 +0100 (ons, 22 feb 2017) $
# Last Revision    : $Revision: 21700 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/GFK_PRODUCTS.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemEXPVIN
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVIN.GFK_PRODUCTS
( TIME_KEY
 ,PROD_LEVEL
 ,PROD_KEY
 ,PROD_NAME
 ,PROD_PARENT
 ,PROD_PARENT_NAME
 ,Scan_Code_Seq_Num
 ,Measuring_Unit_Seq_Num
 ,Article_Seq_Num
 ,Node_Seq_Num
 ,Calendar_Week_Start_Dt
 ,Calendar_Week_End_Dt
 ,Calendar_Week_Start_Dttm
 ,Calendar_Week_End_Dttm
) AS LOCKING ROW FOR ACCESS
SELECT gfkt0.TIME_KEY
 ,7 (BYTEINT) AS PROD_LEVEL
 ,(SUBSTRING('0000000000000' FROM 1 FOR 13-CHARACTER_LENGTH(TRIM(mus0.N_Scan_Cd)))||TRIM(mus0.N_Scan_Cd)) (VARCHAR(13)) AS PROD_KEY
 ,ad0.Article_Desc (VARCHAR(250)) AS PROD_NAME
 ,TRIM(ahn0.N_Node_Id) (VARCHAR(22)) AS PROD_PARENT
 ,ahnd0.Node_Desc AS PROD_PARENT_NAME
 ,mus0.Scan_Code_Seq_Num
 ,scm0.Measuring_Unit_Seq_Num
 ,ma0.Article_Seq_Num
 ,aha0.Node_Seq_Num
 ,gfkt0.Calendar_Week_Start_Dt
 ,gfkt0.Calendar_Week_End_Dt
 ,gfkt0.Calendar_Week_Start_Dttm
 ,gfkt0.Calendar_Week_End_Dttm
FROM (select * from ${DB_ENV}TgtVOUT.MU_SCAN_CODE 
WHERE SYSLIB.IS_INTEGER(N_Scan_Cd) <> 0
AND length(TRIM(LEADING '0' FROM trim(N_Scan_Cd))) < 14) AS mus0
CROSS JOIN ${DB_ENV}SemEXPT.ER_GFK_TIME AS gfkt0
INNER JOIN ${DB_ENV}TgtVOUT.SCAN_CODE_MU AS scm0
ON scm0.Scan_Code_Seq_Num = mus0.Scan_Code_Seq_Num
AND scm0.Valid_From_Dttm <= gfkt0.Calendar_Week_End_Dttm
AND scm0.Valid_To_Dttm > gfkt0.Calendar_Week_End_Dttm
INNER JOIN ${DB_ENV}TgtVOUT.MEASURING_UNIT AS mu0
ON mu0.Measuring_Unit_Seq_Num = scm0.Measuring_Unit_Seq_Num
INNER JOIN ${DB_ENV}TgtVOUT.MU_ARTICLE AS ma0
ON ma0.Measuring_Unit_Seq_Num = mu0.Measuring_Unit_Seq_Num
AND ma0.Valid_From_Dttm <= gfkt0.Calendar_Week_End_Dttm
AND ma0.Valid_To_Dttm > gfkt0.Calendar_Week_End_Dttm
INNER JOIN ${DB_ENV}TgtVOUT.ARTICLE AS a0
ON a0.Article_Seq_Num = ma0.Article_Seq_Num
INNER JOIN ${DB_ENV}TgtVOUT.ARTICLE_AH_ALLOC_CENTRAL AS aha0
ON aha0.Article_Seq_Num = a0.Article_Seq_Num
AND aha0.Valid_From_Dttm <= gfkt0.Calendar_Week_End_Dttm
AND aha0.Valid_To_Dttm > gfkt0.Calendar_Week_End_Dttm
INNER JOIN ${DB_ENV}TgtVOUT.ARTICLE_HIERARCHY_NODE AS ahn0
ON ahn0.Node_Seq_Num = aha0.Node_Seq_Num
AND ahn0.Article_Hierarchy_Id = 'GG'
INNER JOIN ${DB_ENV}SemEXPT.ER_GFK_PRODUCTS_FILTER AS gfkpf0
ON POSITION(TRIM(gfkpf0.PROD_PARENT_PATTERN) IN ahn0.N_Node_Id) = 1
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_DESC_B AS ad0
ON ad0.Article_Seq_Num = a0.Article_Seq_Num
AND ad0.Valid_From_Dttm <= gfkt0.Calendar_Week_End_Dttm
AND ad0.Valid_To_Dttm > gfkt0.Calendar_Week_End_Dttm
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_HIER_NODE_DESC_B AS ahnd0
ON ahnd0.Node_Seq_Num = ahn0.Node_Seq_Num
AND ahnd0.Valid_From_Dttm <= gfkt0.Calendar_Week_End_Dttm
AND ahnd0.Valid_To_Dttm > gfkt0.Calendar_Week_End_Dttm
WHERE gfkt0.TIME_KEY = gfkt0.Calendar_Week_Start_Dt
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemEXPVIN.GFK_PRODUCTS IS '$Revision: 21700 $ - $Date: 2017-02-22 14:33:06 +0100 (ons, 22 feb 2017) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

