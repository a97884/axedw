/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ER_SAP_STORE_ART_W_PRICE_F.btq 17563 2015-11-10 11:41:01Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2015-11-10 12:41:01 +0100 (tis, 10 nov 2015) $
# Last Revision    : $Revision: 17563 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/ER_SAP_STORE_ART_W_PRICE_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemEXPVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemEXPVOUT.ER_SAP_STORE_ART_W_PRICE_F 
( 
	Article_Id
	, UOM_Cd
	, Store_Id
	, Calendar_Week_Id	
	, Calendar_Week_End_Dt
	, Transfer_Price_Amt
	, Transfer_Price_Currency_Cd
	, List_Price_Amt
	, List_Price_Currency_Cd
	, Market_Discount_Amt
) 
As
Lock row for access
Select 
	Substr('000000000000000000' || a.N_Article_Id,character_length(a.N_Article_Id)+1,18) as Article_Id
	, Price.Cost_Price_UOM_Cd As UOM_Cd
	, Price.Store_Id As Store_Id
	, price.Calendar_Week_Id
	, calw.Calendar_Week_End_Dt
	, price.Transfer_Price_Amt
	, price.Transfer_Price_Currency_Cd
	, price.List_Price_Amt
	, price.List_Price_Currency_Cd
	, price.Market_Discount_Amt	
From 
(
	Select 
	 s.Store_Id As Store_Id 
	 , price.Price_List_Cd As Cost_Price_List_Cd
	 , price.Article_Seq_Num
	 , price.Vendor_Party_Seq_Num
	 , price.Vendor_Subrange_Cd
	 , price.Cost_Price_UOM_Cd
	 , cal.Calendar_Week_Id 
	 , cal.Calendar_Dt
	 , price.Final_Cost_Price_Amt as Transfer_Price_Amt
	 , price.Currency_Cd as Transfer_Price_Currency_Cd
	 , Case 
			When pb00.Price_Condition_UOM_Cd = 'KG' And pb00.Price_Calculation_Type_Cd = 'C' Then pb00.Price_Condition_Rate 
			Else pb00.Price_Condition_Val 
		End As List_Price_Amt
	 ,  pb00.Price_Condition_Currency_Cd as List_Price_Currency_Cd
	 , Case 
			When z13x.Price_Condition_UOM_Cd = 'KG' And z13x.Price_Calculation_Type_Cd = 'C' Then z13x.Price_Condition_Rate 
			Else z13x.Price_Condition_Val 
		End As Market_Discount_Amt		
	/* For each day */
	From ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal
	/* fetch the valid documents */ 
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE_COST_PRICE_SCHEDULE As price_s
	On cal.Calendar_Dt between price_s.Valid_From_Dt And price_s.Valid_To_Dt
	/* fetch the price document */ 
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE_COST_PRICE As price
	On price.Org_Party_Seq_Num =  price_s.Org_Party_Seq_Num
	And price.Distribution_Channel_Cd =  price_s.Distribution_Channel_Cd
	And price.Price_List_Cd =  price_s.Price_List_Cd
	And price.Article_Seq_Num =  price_s.Article_Seq_Num
	And price.Valid_From_Dt =  price_s.Valid_From_Dt
	And price.Cost_Price_Doc_Version_Num =  price_s.Cost_Price_Doc_Version_Num
	/* Listpris (PB00) condition applied */
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE_COST_PRICE_COND As pb00
	On price_s.Org_Party_Seq_Num = pb00.Org_Party_Seq_Num
	And price_s.Distribution_Channel_Cd = pb00.Distribution_Channel_Cd
	And price_s.Price_List_Cd = pb00.Price_List_Cd
	And price_s.Article_Seq_Num = pb00.Article_Seq_Num
	And price_s.Valid_From_Dt = pb00.Valid_From_Dt
	And price_s.Cost_Price_Doc_Version_Num =  pb00.Cost_Price_Doc_Version_Num
	And pb00.Price_Condition_Type_Cd = 'PB00'
	/* Marknadsrabatt (Z130,Z131) condition applied */
	Inner Join ${DB_ENV}TgtVOUT.ARTICLE_COST_PRICE_COND As z13x
	On price_s.Org_Party_Seq_Num = z13x.Org_Party_Seq_Num
	And price_s.Distribution_Channel_Cd = z13x.Distribution_Channel_Cd
	And price_s.Price_List_Cd = z13x.Price_List_Cd
	And price_s.Article_Seq_Num = z13x.Article_Seq_Num
	And price_s.Valid_From_Dt = z13x.Valid_From_Dt
	And price_s.Cost_Price_Doc_Version_Num =  z13x.Cost_Price_Doc_Version_Num
	And z13x.Price_Condition_Type_Cd in ('Z130','Z131')
	/* For what stores*/ 
	Inner Join 
	(
	  SELECT s1.N_Store_Id As Store_Id
	   ,lo2.Cost_Price_List_Cd
	  FROM ${DB_ENV}TgtVOUT.LOCATION_ORGANIZATION AS lo2
	  INNER JOIN ${DB_ENV}TgtVOUT.STORE AS s1 
	   ON s1.Store_Seq_Num = lo2.Location_Seq_Num
	  WHERE lo2.Valid_To_Dttm = SYSLIB.HighTSVal()
		AND lo2.Location_Org_Role_Cd =  'ICS'
		AND lo2.Org_Party_Seq_Num = 10
	) s On s.Cost_Price_List_Cd = price_s.Price_List_Cd
	/* Fetch price documents for Sales Org DAGAB and from analytical day 1 uptil now */ 
	Where price_s.Distribution_Channel_Cd = '20'
	And price_s.Org_Party_Seq_Num = 10 
	--And cal.Calendar_Dt between ADD_MONTHS(CURRENT_DATE,-3) and ADD_MONTHS(CURRENT_DATE,1)
	And cal.Calendar_Dt = CURRENT_DATE --Only last known price! 
	And price_s.Valid_Price_Schedule_Ind = 1
	And price_s.Recorded_End_Dt = Date '9999-12-31'
	AND price_s.Price_List_Cd in 
	(
			  SELECT DISTINCT lo2.Cost_Price_List_Cd
	  		  FROM ${DB_ENV}TgtVOUT.LOCATION_ORGANIZATION AS lo2
	  		  INNER JOIN ${DB_ENV}TgtVOUT.STORE AS s1 
	   			ON s1.Store_Seq_Num = lo2.Location_Seq_Num
	  		  WHERE lo2.Valid_To_Dttm = SYSLIB.HighTSVal()
				AND lo2.Location_Org_Role_Cd =  'ICS'
				AND lo2.Org_Party_Seq_Num = 10
	)
) As price --On calw.Calendar_Week_End_Dt = price.Calendar_Dt
Inner Join ${DB_ENV}TgtVOUT.ARTICLE a On a.Article_Seq_Num = price.Article_Seq_Num
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_WEEK calw on calw.Calendar_Week_Id = price.Calendar_Week_Id
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* Update the statement with the corresponding view name */
COMMENT ON ${DB_ENV}SemEXPVOUT.ER_SAP_STORE_ART_W_PRICE_F  IS '$Revision: 17563 $ - $Date: 2015-11-10 12:41:01 +0100 (tis, 10 nov 2015) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE