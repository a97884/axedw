/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ER_TRACEABILITY.btq 29892 2020-01-16 13:31:18Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29892 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/ER_TRACEABILITY.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemEXPVOUT
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVOUT.ER_TRACEABILITY ( 
Sales_Tran_Seq_Num,
Party_Seq_Num,
Store_Seq_Num,
Source_Own_Party,
Source_Location,
Destination_Own_Party,
Destination_Location,
Destination_Name,
Destination_Address,
Destination_Postal_Cd,
Destination_City,
Tran_Dt_DD,
Tran_End_Dttm_DD,
InsertDttm,
Scan_Code_Seq_Num,
Scan_Cd,
Traceability_Type_Cd,
Item_Qty,
NetWeight,
UOM_Category_Cd,
LGTIN,
EventID,
EventType,
Action,
Article_Seq_Num,
scan_cd_rank
) AS

SELECT Sales_Tran_Seq_Num,
Party_Seq_Num,
Store_Seq_Num,
Source_Own_Party,
Source_Location,
Destination_Own_Party,
Destination_Location,
Destination_Name,
Destination_Address,
Destination_Postal_Cd,
Destination_City,
Tran_Dt_DD,
Tran_End_Dttm_DD,
InsertDttm,
Scan_Code_Seq_Num,
Scan_Cd,
Traceability_Type_Cd,
Item_Qty,
NetWeight,
UOM_Category_Cd,
LGTIN,
CAST(Sales_Tran_Seq_Num AS VARCHAR(19))||'_'|| CAST(Sales_Tran_Line_Num AS VARCHAR(10))||'_'|| OREPLACE(LGTIN,'.','') AS EventID,
EventType,
Action,
Article_Seq_Num,
scan_cd_rank
FROM
(
SELECT st1.Sales_Tran_Seq_Num,
stp1.Party_Seq_Num Party_Seq_Num,
st1.Store_Seq_Num,
'732024.080006.0' Source_Own_Party,
lgb.Source_GTIN Source_Location,
bdb.Corporate_Identity_Num Destination_Own_Party,
bdb.Corporate_Identity_Num Destination_Location,
onb.Org_Name Destination_name,
adr.Address_Line_1 as Destination_Address,
adr.Postal_Cd as Destination_Postal_Cd,
adr.Postal_Locality_Name Destination_City,
st1.Tran_Dt_DD,
st1.Tran_End_Dttm_DD,
st1.InsertDttm,
stl1.Scan_Code_Seq_Num,
s2.scan_cd,
atm.traceability_type_cd,
(CASE WHEN stla1.Actual_Item_Qty IS NOT NULL THEN stla1.Actual_Item_Qty * stl1.Item_Qty ELSE stl1.Item_Qty END) AS Item_Qty,
COALESCE(sclu1.UOM_Category_Cd,'-1') AS UOM_Category_Cd,
oreplace(COALESCE(prfx.LGTIN, scan_code_Rank.scan_cd),' ','')||'.'||stlt.Traceability_Id AS LGTIN,

CASE WHEN sclu1.UOM_Category_Cd = 'UNT' AND a1.Article_Type_Cd = 'ZHAW' AND COALESCE(ad1.Net_Weight,0) <> 0 AND ad1.Net_Qty_UOM_Cd = 'KG' THEN 
((CASE WHEN stla1.Actual_Item_Qty IS NOT NULL THEN stla1.Actual_Item_Qty * stl1.Item_Qty ELSE stl1.Item_Qty END) * (ad1.Net_Weight/1000))
WHEN sclu1.UOM_Category_Cd = 'WGH' THEN (
CASE WHEN stla1.Actual_Item_Qty IS NOT NULL THEN stla1.Actual_Item_Qty * stl1.Item_Qty ELSE stl1.Item_Qty END)
ELSE 0
END (DECIMAL (18,4)) AS NetWeight,

'ADD' AS Action,
'Shipping' AS EventType,
mst.TransactionID AS InstanceIdentifier,
a1.Article_Seq_Num,
scan_code_Rank.scan_cd as scan_cd_rank,
stl1.Sales_Tran_Line_Num

FROM ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED AS st1
            
INNER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE AS stl1
ON stl1.Sales_Tran_Seq_Num = st1.Sales_Tran_Seq_Num
AND stl1.Store_Seq_Num = st1.Store_Seq_Num
AND stl1.Tran_Dt_DD = st1.Tran_Dt_DD

INNER JOIN ${DB_ENV}TgtVOUT.SALES_TRAN_LINE_TRACEABILITY AS stlt
ON stl1.Sales_Tran_Seq_Num = stlt.Sales_Tran_Seq_Num
AND stl1.Sales_Tran_Line_Num = stlt.Sales_Tran_Line_Num
AND stl1.Tran_Dt_DD = stlt.Tran_Dt_DD        
AND stl1.Store_Seq_Num = stlt.Store_Seq_Num

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL AS stla1
ON stla1.Sales_Tran_Seq_Num = stl1.Sales_Tran_Seq_Num
AND stla1.Sales_Tran_Line_Num = stl1.Sales_Tran_Line_Num
AND stla1.Store_Seq_Num = stl1.Store_Seq_Num
AND stla1.Tran_Dt_DD = stl1.Tran_Dt_DD
  
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY AS stp1
ON stp1.Sales_Tran_Seq_Num = st1.Sales_Tran_Seq_Num
AND stp1.Store_Seq_Num = st1.Store_Seq_Num
AND stp1.Tran_Dt_DD = st1.Tran_Dt_DD

INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_SALES_TRANS mst
ON st1.Sales_Tran_Seq_Num = mst.Sales_Tran_Seq_Num
 
LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.PARTY_ADDRESS  pad
ON stp1.Party_Seq_Num = pad.Party_Seq_Num
AND pad.Valid_To_Dttm = syslib.hightsval()
AND pad.Address_Usage_Cd = 'OFF'

LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.ADDRESS_D adr
ON adr.Address_Seq_Num = pad.Address_Seq_Num

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.MU_SCAN_CODE_LOC_UOM_B AS sclu1
ON sclu1.Scan_Code_Seq_num = stl1.Scan_Code_Seq_Num
AND sclu1.Location_Seq_Num = stl1.Store_Seq_Num
AND st1.Tran_End_Dttm_DD BETWEEN sclu1.Valid_From_Dttm AND sclu1.Valid_To_Dttm

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.STORE_TRAIT_VALUE_B AS stv1
ON st1.Store_Seq_Num =  stv1.Store_Seq_Num
AND  stv1.Location_Trait_Cd = 'TST'

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ORGANIZATION_NAME_B onb
ON onb.Org_Party_Seq_Num = stp1.Party_Seq_Num
AND onb.Valid_To_Dttm = syslib.hightsval()
AND onb.Name_Type_Cd = 'LEG'

INNER JOIN ${DB_ENV}SemCMNVOUT.SCAN_CODE_D AS s2
ON s2.Scan_Code_Seq_Num = stl1.Scan_Code_Seq_Num

LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.SCAN_CODE_D AS s2a
ON s2a.Scan_Code_Seq_Num = stla1.Actual_Scan_Code_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.MEASURING_UNIT_D AS mu1
ON s2.Measuring_Unit_Seq_Num = mu1.Measuring_Unit_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D AS a1
ON s2.ARTICLE_SEQ_NUM = a1.ARTICLE_SEQ_NUM

INNER JOIN (
select at1.article_seq_num,sc1.scan_code_seq_num,sc1.scan_cd
from
(SELECT article_seq_num
FROM ${DB_ENV}TgtVOUT.ARTICLE_TRACEABILITY
) as at1
INNER JOIN ${DB_ENV}SEMCMNVOUT.SCAN_CODE_D as sc1
on at1.article_seq_num = sc1.article_seq_num
INNER JOIN ${DB_ENV}SEMCMNVOUT.MEASURING_UNIT_D as mu1
on mu1.Measuring_Unit_Seq_Num = sc1.Measuring_Unit_Seq_Num
QUALIFY ROW_NUMBER() OVER (PARTITION BY  at1.article_seq_num ORDER BY mu1.Ordering_MU_Ind DESC) = 1
) as scan_code_Rank
on scan_code_Rank.article_seq_num = a1.article_seq_num

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_DETAILS_B AS ad1
ON s2.ARTICLE_SEQ_NUM = ad1.ARTICLE_SEQ_NUM
AND ad1.Valid_To_Dttm = TIMESTAMP '9999-12-31 23:59:59.999999'
            
INNER JOIN ${DB_ENV}TgtVOUT.ARTICLE_TRACEABILITY atm
ON a1.article_seq_num = atm.article_seq_num
AND atm.Valid_To_Dttm = TIMESTAMP '9999-12-31 23:59:59.999999'

LEFT OUTER JOIN  ${DB_ENV}TgtVOUT.BUSINESS_DETAILS_B bdb
ON stp1.Party_Seq_Num = bdb.Org_Party_Seq_Num
AND bdb.valid_to_dttm = syslib.hightsval()

LEFT OUTER JOIN (
SELECT 
comp_prefix||'.'||item_ref||'.'||'0' AS Source_GTIN,
GLN_Id,prefix_,gcplength,
Location_Seq_Num,
store_name,store_id,
concept_cd
FROM
(
SELECT SUBSTRING(pre_comp_prefix
FROM 1 FOR gcplength ) AS comp_prefix,
SUBSTRING(pre_comp_prefix FROM gcplength + 1  FOR length(pre_comp_prefix) - 1 ) AS  item_ref,
CASE WHEN length(GLN_Id) = 14 THEN SUBSTRING( GLN_Id FROM 1 FOR 1 ) ELSE '0' END AS Pack_Ind,
GLN_Id,prefix_,gcplength,
Location_Seq_Num,
store_name,store_id,
concept_cd
FROM
(
SELECT 
GLN_Id,
Store_Seq_Num AS Location_seq_num,
store_name,store_id,
concept_cd,
CASE WHEN length(GLN_Id) = 14 THEN SUBSTRING(GLN_Id FROM 2 FOR length(GLN_Id) - 2)
ELSE SUBSTRING(GLN_Id FROM 1 FOR length(GLN_Id) - 1) END AS pre_comp_prefix,
prefix_,
gcplength
FROM ${DB_ENV}SemCMNVOUT.STORE_D sourc
INNER JOIN ${DB_ENV}SemEXPVOUT.ER_GTIN gc
ON 1 = 1
WHERE instr (GLN_Id,prefix_) > 0
) AS x
QUALIFY ROW_NUMBER() OVER (PARTITION BY  GLN_Id ORDER BY gcplength) = 1
) AS z
) lgb
ON  stl1.Store_Seq_Num = lgb.Location_Seq_Num

INNER JOIN (
SELECT comp_prefix||'.'||Pack_Ind||item_ref AS LGTIN,
scan_cd,Scan_Code_Seq_Num
FROM
(
SELECT SUBSTRING(pre_comp_prefix
FROM 1 FOR gcplength ) AS comp_prefix,
SUBSTRING(pre_comp_prefix FROM gcplength + 1  FOR length(pre_comp_prefix) - 1 ) AS  item_ref,
CASE WHEN length(scan_cd) = 14 THEN SUBSTRING( scan_cd FROM 1 FOR 1 ) ELSE '0' END AS Pack_Ind,
scan_cd,Scan_Code_Seq_Num
FROM
(
SELECT n_scan_cd AS scan_cd,
Scan_Code_Seq_Num,
CASE WHEN length(scan_cd) = 14 THEN SUBSTRING(n_scan_cd FROM 2 FOR length(n_scan_cd) - 2)
ELSE SUBSTRING(n_scan_cd FROM 1 FOR length(n_scan_cd) - 1) END AS pre_comp_prefix,
prefix_,gcplength 
FROM ${DB_ENV}TgtVOUT.MU_SCAN_CODE sc
INNER JOIN ${DB_ENV}SemEXPVOUT.ER_GTIN gc
ON 1 = 1
WHERE instr (sc.n_scan_cd,prefix_) > 0
) AS x
) AS z
) prfx
ON scan_code_Rank.Scan_Code_Seq_Num = prfx.Scan_Code_Seq_Num
WHERE st1.Tran_Status_Cd IN ( 'CT', 'VO')
AND  st1.Tran_Type_Cd = 'POS'
AND  stv1.Store_Seq_Num IS NULL
AND  stl1.Tran_Line_Status_Cd = 'CT'
AND  atm.valid_to_dttm = syslib.hightsval()
) AS f1
QUALIFY ROW_NUMBER() OVER (PARTITION BY  EventID ORDER BY Tran_Dt_DD DESC) = 1
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemEXPVOUT.ER_TRACEABILITY  IS '$Revision: 29892 $ - $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

