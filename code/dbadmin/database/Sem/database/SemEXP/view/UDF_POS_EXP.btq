/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: UDF_POS_EXP.btq 33360 2020-08-21 12:14:25Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-08-21 14:14:25 +0200 (fre, 21 aug 2020) $
# Last Revision    : $Revision: 33360 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/UDF_POS_EXP.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemEXPVOUT	; 

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVOUT.UDF_POS_EXP
(
	ProductID
	,LocationID
	,OrderChannel
	,BusinessDate
	,OfferID
	,PriceTypeCode
	,ReturnIndicator
	,SalesQty
	,SalesUoM 
	,TransactionCount
	,NetRevenue
	,GrossRevenue
	,Cost
	,Currency
	)
AS LOCKING ROW FOR ACCESS 
SELECT	
	a14.SAP_Article_Id As ProductID
	,a15.Store_Id As LocationID
	,sd.Store_Department_Id As OrderChannel
	,a11.Tran_Dt As BusinessDate
	,a16.Promotion_Offer_Id As OfferID
	,CASE 
		WHEN a11.Preferred_Discount_Type_Cd = '-1' 
			THEN NULL 
			ELSE a11.Preferred_Discount_Type_Cd 
		END AS PriceTypeCode
	,CASE WHEN a11.Return_Reason_Cd <> '-1' 
		THEN 1 
		ELSE 0 
	END As ReturnIndicator
	,SUM(a11.Item_Qty)  As SalesQty
	,a11.UOM_Category_Cd As SalesUoM 
	,COUNT(Distinct a11.Sales_Tran_Seq_Num)  As TransactionCount
	,SUM(a11.Unit_Selling_Price_Amt)  As NetRevenue
	,SUM(a11.Unit_Selling_Price_Amt + a11.Tax_Amt)  As GrossRevenue
	,SUM(a11.Unit_Cost_Amt)  As Cost
	,'SEK' As Currency
FROM	${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_LINE_X_F	a11
	INNER JOIN	${DB_ENV}SemCMNVOUT.SCAN_CODE_D	a12
	  ON 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	INNER JOIN	${DB_ENV}SemCMNVOUT.ARTICLE_D	a14
	  ON 	(a12.Article_Seq_Num = a14.Article_Seq_Num)
	INNER JOIN	${DB_ENV}SemCMNVOUT.STORE_D	a15
	  ON 	(a11.Store_Seq_Num = a15.Store_Seq_Num)
	INNER JOIN	${DB_ENV}SemCMNVOUT.PROMOTION_OFFER_D	a16
	  ON 	(a11.Pref_Promotion_Offer_Seq_Num = a16.Promotion_Offer_Seq_Num)
	INNER JOIN	${DB_ENV}SemCMNVOUT.CAMPAIGN_SALES_TYPE_D	a17
	  ON 	(a11.Campaign_Sales_Type_Cd = a17.Campaign_Sales_Type_Cd)
	INNER JOIN  	${DB_ENV}SemCMNVOUT.STORE_DEPARTMENT_D sd
	ON a11.Store_Department_Seq_Num = sd.Store_Department_Seq_Num
GROUP BY	
	a14.SAP_Article_Id
	,a15.Store_Id
	,sd.Store_Department_Id
	,a11.Tran_Dt
	,a16.Promotion_Offer_Id
	,a11.Preferred_Discount_Type_Cd
	,CASE WHEN a11.Return_Reason_Cd <> '-1' 
		THEN 1 
		ELSE 0 
	 END 
	,a11.UOM_Category_Cd;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* Update the statement with the corresponding view name */
COMMENT ON ${DB_ENV}SemEXPVOUT.UDF_POS_EXP IS '$Revision: 33360 $ - $Date: 2020-08-21 14:14:25 +0200 (fre, 21 aug 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
