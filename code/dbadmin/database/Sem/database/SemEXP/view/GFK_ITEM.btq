/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: GFK_ITEM.btq 29892 2020-01-16 13:31:18Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29892 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/GFK_ITEM.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemEXPVIN
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVIN.GFK_ITEM
( TIME_KEY
 ,FASCIA_PARENT
 ,FASCIA_PARENT_NAME
 ,PROD_KEY
 ,PROD_NAME
 ,PROD_PARENT
 ,PROD_PARENT_NAME
 ,ANTAL_VARDE
 ,BELOPP_INKL_MOMS
 ,Scan_Code_Seq_Num
 ,Calendar_Week_Start_Dt
 ,Calendar_Week_End_Dt
 ,Calendar_Week_Start_Dttm
 ,Calendar_Week_End_Dttm
) AS
SELECT gfkg0.TIME_KEY
 ,gfkg0.FASCIA_PARENT
 ,gfkg0.FASCIA_PARENT_NAME
 ,gfkp0.PROD_KEY
 ,gfkp0.PROD_NAME
 ,gfkp0.PROD_PARENT
 ,gfkp0.PROD_PARENT_NAME
 ,SUM(COALESCE(stla0.Actual_Item_Qty,1.0000)
  * stl0.Item_Qty) (DECIMAL (10,2)) AS ANTAL_VARDE
 ,SUM(stl0.Unit_Selling_Price_Amt
  + stl0.Tax_Amt) (DECIMAL (10,2)) AS BELOPP_INKL_MOMS
 ,MAX(stl0.Scan_Code_Seq_Num) AS Scan_Code_Seq_Num
 ,MAX(gfkg0.Calendar_Week_Start_Dt) AS Calendar_Week_Start_Dt
 ,MAX(gfkg0.Calendar_Week_End_Dt) AS Calendar_Week_End_Dt
 ,MAX(gfkg0.Calendar_Week_Start_Dttm) AS Calendar_Week_Start_Dttm
 ,MAX(gfkg0.Calendar_Week_End_Dttm) AS Calendar_Week_End_Dttm
FROM ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED st0
INNER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE AS stl0
ON st0.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
INNER JOIN ${DB_ENV}SemEXPT.ER_GFK_TIME AS gfkt0 -- has two years of calendar-dates
ON gfkt0.TIME_KEY = stl0.Tran_Dt_DD
INNER JOIN ${DB_ENV}SemEXPT.ER_GFK_GEOGRAPHY AS gfkg0
ON gfkg0.Seq_Num = stl0.Store_Seq_Num
INNER JOIN ${DB_ENV}SemEXPT.ER_GFK_PRODUCTS AS gfkp0
ON gfkp0.Scan_Code_Seq_Num = stl0.Scan_Code_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As stla0
ON stla0.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
AND stla0.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
AND stla0.Store_Seq_Num = stl0.Store_Seq_Num 
AND stla0.Tran_Dt_DD = stl0.Tran_Dt_DD
WHERE st0.Tran_Status_Cd in ( 'CT', 'VO')
AND st0.Tran_Type_Cd = 'POS'
AND stl0.Tran_Line_Status_Cd = 'CT'
AND gfkt0.TIME_KEY BETWEEN gfkt0.Calendar_Week_Start_Dt AND gfkt0.Calendar_Week_End_Dt
AND gfkp0.Scan_Code_Seq_Num IS NOT NULL
AND gfkg0.FASCIA_LEVEL = 2
GROUP BY 1,2,3,4,5,6,7
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemEXPVIN.GFK_ITEM IS '$Revision: 29892 $ - $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

