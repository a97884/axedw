/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ACN_PRODUCTS.btq 21698 2017-02-22 11:51:54Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-02-22 12:51:54 +0100 (ons, 22 feb 2017) $
# Last Revision    : $Revision: 21698 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/ACN_PRODUCTS.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemEXPVIN
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVIN.ACN_PRODUCTS
( TIME_KEY
 ,PROD_LEVEL
 ,PROD_KEY
 ,PROD_NAME
 ,PROD_PARENT
 ,PROD_LEVERANTOR_ID
 ,PROD_LEVERANTOR_NAMN
 ,Scan_Code_Seq_Num
 ,Measuring_Unit_Seq_Num
 ,Article_Seq_Num
 ,Node_Seq_Num
 ,Calendar_Week_Start_Dt
 ,Calendar_Week_End_Dt
 ,Calendar_Week_Start_Dttm
 ,Calendar_Week_End_Dttm
) AS LOCKING ROW FOR ACCESS
SELECT acnt0.TIME_KEY
 ,9 (BYTEINT) AS PROD_LEVEL
 ,(SUBSTRING('0000000000000000000000' FROM 1 FOR 22-CHARACTER_LENGTH(TRIM(mus0.N_Scan_Cd)))||TRIM(mus0.N_Scan_Cd)) (VARCHAR(22)) AS PROD_KEY
,coalesce(ad0.Article_Desc,'Saknar beskrivning') (VARCHAR(250)) AS PROD_NAME
 ,coalesce(TRIM(ahn0.N_Node_Id),'0')  (VARCHAR(22)) AS PROD_PARENT
 ,coalesce(v0.PROD_LEVERANTOR_ID,'0') as PROD_LEVERANTOR_ID
 ,coalesce(v0.PROD_LEVERANTOR_NAMN,'Saknar beskrivning') as PROD_LEVERANTOR_NAMN
 ,mus0.Scan_Code_Seq_Num
 ,scm0.Measuring_Unit_Seq_Num
 ,ma0.Article_Seq_Num
 ,aha0.Node_Seq_Num
 ,acnt0.Calendar_Week_Start_Dt
 ,acnt0.Calendar_Week_End_Dt
 ,acnt0.Calendar_Week_Start_Dttm
 ,acnt0.Calendar_Week_End_Dttm
FROM (select * from ${DB_ENV}TgtVOUT.MU_SCAN_CODE 
WHERE SYSLIB.IS_INTEGER(N_Scan_Cd) <> 0
AND length(TRIM(LEADING '0' FROM trim(N_Scan_Cd))) < 14
AND length(TRIM(LEADING '0' FROM trim(N_Scan_Cd))) <> 3) AS mus0
CROSS JOIN ${DB_ENV}SemEXPT.ER_ACN_TIME AS acnt0
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.SCAN_CODE_MU AS scm0
ON scm0.Scan_Code_Seq_Num = mus0.Scan_Code_Seq_Num
AND scm0.Valid_From_Dttm <= acnt0.Calendar_Week_End_Dttm
AND scm0.Valid_To_Dttm > acnt0.Calendar_Week_End_Dttm
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.MEASURING_UNIT AS mu0
ON mu0.Measuring_Unit_Seq_Num = scm0.Measuring_Unit_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.MU_ARTICLE AS ma0
ON ma0.Measuring_Unit_Seq_Num = mu0.Measuring_Unit_Seq_Num
AND ma0.Valid_From_Dttm <= acnt0.Calendar_Week_End_Dttm
AND ma0.Valid_To_Dttm > acnt0.Calendar_Week_End_Dttm
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE AS a0
ON a0.Article_Seq_Num = ma0.Article_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_AH_ALLOC_CENTRAL AS aha0
ON aha0.Article_Seq_Num = a0.Article_Seq_Num
AND aha0.Valid_From_Dttm <= acnt0.Calendar_Week_End_Dttm
AND aha0.Valid_To_Dttm > acnt0.Calendar_Week_End_Dttm
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_HIERARCHY_NODE AS ahn0
ON ahn0.Node_Seq_Num = aha0.Node_Seq_Num
AND ahn0.Article_Hierarchy_Id = 'GG'
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_DESC_B AS ad0
ON ad0.Article_Seq_Num = a0.Article_Seq_Num
AND ad0.Valid_From_Dttm <= acnt0.Calendar_Week_End_Dttm
AND ad0.Valid_To_Dttm > acnt0.Calendar_Week_End_Dttm
LEFT OUTER JOIN (
 SELECT va0.Article_Seq_Num
  ,MAX(TRIM(LEADING '0' FROM o0.N_Org_Party_Id)) (VARCHAR(50)) AS PROD_LEVERANTOR_ID
  ,MAX(on0.Org_Name) (VARCHAR(100)) AS PROD_LEVERANTOR_NAMN
 FROM ${DB_ENV}TgtVOUT.ORGANIZATION AS o0
 CROSS JOIN ${DB_ENV}SemEXPT.ER_ACN_TIME AS acnt02
 INNER JOIN ${DB_ENV}TgtVOUT.VENDOR_ARTICLE AS va0
 ON va0.Dist_Org_Party_Seq_Num = o0.Org_Party_Seq_Num
 AND va0.Valid_From_Dttm <= acnt02.Calendar_Week_End_Dttm
 AND va0.Valid_To_Dttm > acnt02.Calendar_Week_End_Dttm
 LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ORGANIZATION_NAME_B AS on0
 ON on0.Org_Party_Seq_Num = o0.Org_Party_Seq_Num
 AND on0.Valid_From_Dttm <= acnt02.Calendar_Week_End_Dttm
 AND on0.Valid_To_Dttm > acnt02.Calendar_Week_End_Dttm
 AND on0.Name_Type_Cd = 'LEG'
 WHERE acnt02.TIME_KEY = acnt02.Calendar_Week_Start_Dt
 AND o0.Org_Is_Vendor_Ind = 1
 AND va0.Preferred_Dist_Vendor_Ind = 1
 GROUP BY 1
) AS v0
ON v0.Article_Seq_Num = a0.Article_Seq_Num
WHERE acnt0.TIME_KEY = acnt0.Calendar_Week_Start_Dt
AND mus0.Scan_Code_Seq_Num IN ( --only active articles (has transactions in last 2 years)
 SELECT stl0.Scan_Code_Seq_Num
 FROM ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE AS stl0
 INNER JOIN ${DB_ENV}SemEXPT.ER_ACN_TIME AS acnt00 -- has two years of calendar-dates
 ON acnt00.TIME_KEY = stl0.Tran_Dt_DD
 INNER JOIN ${DB_ENV}SemEXPT.ER_ACN_STORES AS acns00
 ON acns00.Store_Seq_Num = stl0.Store_Seq_Num
 GROUP BY 1
 )
UNION ALL
SELECT acnt0.TIME_KEY
 ,ahs0.AH_Level_Num AS PROD_LEVEL
 ,TRIM(ahn0.N_Node_Id) AS PROD_KEY
 ,ahnd0.Node_Desc AS PROD_NAME
 ,TRIM(ahn1.N_Node_Id) AS PROD_PARENT
 ,NULL (VARCHAR(50))  AS PROD_LEVERANTOR_ID
 ,NULL (VARCHAR(100)) AS PROD_LEVERANTOR_NAMN
 ,NULL (INTEGER) AS Scan_Code_Seq_Num
 ,NULL (INTEGER) AS Measuring_Unit_Seq_Num
 ,NULL (INTEGER) AS Article_Seq_Num
 ,ahn0.Node_Seq_Num
 ,acnt0.Calendar_Week_Start_Dt
 ,acnt0.Calendar_Week_End_Dt
 ,acnt0.Calendar_Week_Start_Dttm
 ,acnt0.Calendar_Week_End_Dttm
FROM ${DB_ENV}TgtVOUT.ARTICLE_HIERARCHY_NODE AS ahn0
CROSS JOIN ${DB_ENV}SemEXPT.ER_ACN_TIME AS acnt0
INNER JOIN ${DB_ENV}TgtVOUT.ARTICLE_HIERARCHY_STRUCTURE AS ahs0
ON ahs0.Child_Node_Seq_Num = ahn0.Node_Seq_Num
AND ahs0.Valid_From_Dttm <= acnt0.Calendar_Week_End_Dttm
AND ahs0.Valid_To_Dttm > acnt0.Calendar_Week_End_Dttm
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_HIERARCHY_NODE AS ahn1
ON ahn1.Node_Seq_Num = ahs0.Parent_Node_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE_HIER_NODE_DESC_B AS ahnd0
ON ahnd0.Node_Seq_Num = ahn0.Node_Seq_Num
AND ahnd0.Valid_From_Dttm <= acnt0.Calendar_Week_End_Dttm
AND ahnd0.Valid_To_Dttm > acnt0.Calendar_Week_End_Dttm
WHERE acnt0.TIME_KEY = acnt0.Calendar_Week_Start_Dt
AND ahn0.Article_Hierarchy_Id = 'GG'
AND PROD_KEY <> 'GG'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemEXPVIN.ACN_PRODUCTS IS '$Revision: 21698 $ - $Date: 2017-02-22 12:51:54 +0100 (ons, 22 feb 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

