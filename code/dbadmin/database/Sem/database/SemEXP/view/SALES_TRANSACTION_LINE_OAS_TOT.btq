/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRANSACTION_LINE_OAS_TOT.btq 19486 2016-07-27 06:09:52Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2016-07-27 08:09:52 +0200 (ons, 27 jul 2016) $
# Last Revision    : $Revision: 19486 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/SALES_TRANSACTION_LINE_OAS_TOT.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemEXPVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


Replace View ${DB_ENV}SemEXPVIN.SALES_TRANSACTION_LINE_OAS_TOT
(
Sales_Tran_Seq_Num,
Sales_Tran_Line_Num,
Store_Seq_Num,
Store_Id,
AO_Store_Ind,
Article_Seq_Num,
Article_Id,
Component_Article_Seq_Num,
Component_Article_Id,
Tran_Dt,
SRC_Activity_Code_Id,
SRC_Discount_Type_Cd,
OASRank,
Item_Qty,
OpenJobRunId,
Insert_Dttm
)
As
Select 
st.Sales_Tran_Seq_Num,
st.Sales_Tran_Line_Num,
st.Store_Seq_Num,
std.N_Store_Id as Store_id,
Coalesce(stv.Location_Trait_Val, '-1')  as AO_Store_Ind,
a0.Article_Seq_Num,
a0.N_Article_Id,
Coalesce(a1.Article_Seq_Num, -1) AS Component_Article_Seq_Num,
Coalesce(a1.N_Article_Id, '-1') AS Component_Article_Id,
st.Tran_Dt,
sro.SRC_Activity_Code_Id,
sro.SRC_Discount_Type_Cd,
sro.OASRank,
st.Item_Qty,
st.OpenJobRunId,
st.InsertDttm
From ${DB_ENV}SemEXPVIN.SALES_TRANSACTION_LINE_OAS As st
Left Outer Join ${DB_ENV}SemEXPVIN.SALES_TRAN_DISC_LINE_RANK_OAS sro
ON sro.Sales_Tran_Seq_Num = st.Sales_Tran_Seq_Num
AND sro.Sales_Tran_Line_Num =st.Sales_Tran_Line_Num
JOIN ${DB_ENV}TgtVOUT.STORE AS std
 ON std.Store_Seq_Num = st.Store_Seq_Num
Left Outer Join  ${DB_ENV}TgtVOUT.STORE_TRAIT_VALUE_B As stv
 On stv.Store_Seq_num = std.Store_Seq_Num
 And stv.Location_Trait_Cd = 'AO'
 And stv.Valid_To_Dttm =  '9999-12-31 23:59:59.999999'
 JOIN ${DB_ENV}TgtVOUT.SCAN_CODE_MU AS scm0
ON scm0.Scan_Code_Seq_Num = st.Scan_Code_Seq_Num
AND st.Sales_Tran_Line_End_Dttm between scm0.Valid_From_Dttm and scm0.Valid_To_Dttm
JOIN ${DB_ENV}TgtVOUT.MEASURING_UNIT AS mu0
ON mu0.Measuring_Unit_Seq_Num = scm0.Measuring_Unit_Seq_Num
JOIN ${DB_ENV}TgtVOUT.MU_ARTICLE AS ma0
ON ma0.Measuring_Unit_Seq_Num = mu0.Measuring_Unit_Seq_Num
AND st.Sales_Tran_Line_End_Dttm between ma0.Valid_From_Dttm and ma0.Valid_To_Dttm
JOIN ${DB_ENV}TgtVOUT.ARTICLE AS a0
ON a0.Article_Seq_Num = ma0.Article_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.MULTI_ARTICLE_CONTENT mac
ON a0.Article_Seq_Num = mac.Multi_Article_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ARTICLE AS a1
ON a1.Article_Seq_Num = mac.Article_Seq_Num
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
