/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRANSACTION_TOTALS_F.btq 29892 2020-01-16 13:31:18Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29892 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/SALES_TRANSACTION_TOTALS_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemEXPVIN.SALES_TRANSACTION_TOTALS_F 
( 
Sales_Tran_Seq_Num,
Store_Seq_Num,
Tran_End_Dttm_DD,
Sales_Tran_Id,
Tran_Dt_DD,
Loyalty_Based_Amt,
Employee_Discount_Based_Amt,
Sales_Amt,
Taxable_Amt,
Tax_Amt,
Grand_Amt,
Total_Cost_Amt,
Gross_Profit_Amt,
Item_Cnt,
Scanned_Item_Cnt,
OpenJobRunId,
Insert_Dttm
) 
As
Select	
st.Sales_Tran_Seq_Num,
st.Store_Seq_Num,
st.Tran_End_Dttm_DD As Tran_End_Dttm,
st.N_Sales_Tran_Id,
st.Tran_Dt_DD,
Sum(Case	When stt.Tran_Type_Cd = 'LA' Then stt.Total_Amt Else	0 End	) As Total_Amt_LA,
Sum(Case	When stt.Tran_Type_Cd = 'PD' Then stt.Total_Amt Else	0 End	) AS Total_Amt_PD,
Sum(Case	When stt.Tran_Type_Cd = 'SA' Then stt.Total_Amt Else	0 End	) AS Total_Amt_SA,
Sum(Case	When stt.Tran_Type_Cd = 'TT' Then stt.Total_Amt Else	0 End	) AS Total_Amt_TT,
Sum(Case	When stt.Tran_Type_Cd = 'TA' Then stt.Total_Amt Else	0 End	) AS Total_Amt_TA,
Sum(Case	When stt.Tran_Type_Cd = 'GA' Then stt.Total_Amt Else	0 End	) AS Total_Amt_GA,
Sum(Case	When stt.Tran_Type_Cd = 'CA' Then stt.Total_Amt Else	0 End	) AS Total_Amt_CA,
Sum(Case	When stt.Tran_Type_Cd = 'CS' Then stt.Total_Amt Else	0 End	) AS Total_Amt_CS,
Sum(Case	When stt.Tran_Type_Cd = 'IC' Then stt.Total_Amt Else	0 End	) AS Total_Amt_IC,
Sum(Case	When stt.Tran_Type_Cd = 'SC' Then stt.Total_Amt Else	0 End	) AS Total_Amt_SC,
stt.OpenJobRunID,
st.InsertDttm AS Insert_Dttm
From	${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED As st
Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As stt
	On	st.Sales_Tran_Seq_Num = stt.Sales_Tran_Seq_Num 
Where	st.Tran_Status_Cd in ( 'CT', 'VO')
	And	st.Tran_Type_Cd = 'POS'
Group	By     
st.Sales_Tran_Seq_Num,
st.Store_Seq_Num,
st.Tran_End_Dttm_DD,
st.N_Sales_Tran_Id,
st.Tran_Dt_DD,
stt.OpenJobRunID,
st.InsertDttm ;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* Update the statement with the corresponding view name */
COMMENT ON ${DB_ENV}SemEXPVIN.SALES_TRANSACTION_TOTALS_F  IS '$Revision: 29892 $ - $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE