/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_SemEXP_Picturebox.btq 12547 2014-03-10 16:09:42Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2014-03-10 17:09:42 +0100 (mån, 10 mar 2014) $
# Last Revision    : $Revision: 12547 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/procedure/Load_SemEXP_Picturebox.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}SemEXPVIN.Load_SemEXP_Picturebox
()
P0: BEGIN

 -- Refresh ER_PICTUREBOX_TIME
 DELETE FROM ${DB_ENV}SemEXPT.ER_PICTUREBOX_TIME
 ;
 INSERT INTO ${DB_ENV}SemEXPT.ER_PICTUREBOX_TIME
 SELECT * FROM ${DB_ENV}SemEXPVIN.PICTUREBOX_TIME
 ;
 COLLECT STATISTICS UNIQUE INDEX ( TIME_KEY )
 ,INDEX ( Calendar_Week_Start_Dttm )
 ,INDEX ( Calendar_Week_End_Dttm )
 ,INDEX ( Calendar_Week_Start_Dt )
 ,INDEX ( Calendar_Week_End_Dt )
 ON ${DB_ENV}SemEXPT.ER_PICTUREBOX_TIME
 ;

 -- Refresh ER_PICTUREBOX_STORES
 DELETE FROM ${DB_ENV}SemEXPT.ER_PICTUREBOX_STORES
 ;
 INSERT INTO ${DB_ENV}SemEXPT.ER_PICTUREBOX_STORES
 SELECT * FROM ${DB_ENV}SemEXPVIN.PICTUREBOX_STORES
 ;
 COLLECT STATISTICS UNIQUE INDEX ( STORE_NO )
 ,UNIQUE INDEX ( Store_Seq_Num )
 ON ${DB_ENV}SemEXPT.ER_PICTUREBOX_STORES
 ;
 
 -- Refresh ER_PICTUREBOX_GEOGRAPHY
 DELETE FROM ${DB_ENV}SemEXPT.ER_PICTUREBOX_GEOGRAPHY
 ;
 INSERT INTO ${DB_ENV}SemEXPT.ER_PICTUREBOX_GEOGRAPHY
 SELECT * FROM ${DB_ENV}SemEXPVIN.PICTUREBOX_GEOGRAPHY
 ;
 COLLECT STATISTICS UNIQUE INDEX ( FASCIA_LEVEL, FASCIA_KEY )
 ON ${DB_ENV}SemEXPT.ER_PICTUREBOX_GEOGRAPHY
 ;

 -- Refresh ER_PICTUREBOX_PRODUCTS
 DELETE FROM ${DB_ENV}SemEXPT.ER_PICTUREBOX_PRODUCTS
 ;
 INSERT INTO ${DB_ENV}SemEXPT.ER_PICTUREBOX_PRODUCTS
 SELECT * FROM ${DB_ENV}SemEXPVIN.PICTUREBOX_PRODUCTS
 ;
 COLLECT STATISTICS UNIQUE INDEX ( PROD_LEVEL, PROD_KEY )
 ,INDEX ( Scan_Code_Seq_Num )
 ,INDEX ( Measuring_Unit_Seq_Num )
 ,INDEX ( Article_Seq_Num )
 ,INDEX ( Node_Seq_Num )
 ON ${DB_ENV}SemEXPT.ER_PICTUREBOX_PRODUCTS
 ;

 -- Refresh ER_PICTUREBOX_ITEM
 DELETE FROM ${DB_ENV}SemEXPT.ER_PICTUREBOX_ITEM
 ;
 INSERT INTO ${DB_ENV}SemEXPT.ER_PICTUREBOX_ITEM
 SELECT * FROM ${DB_ENV}SemEXPVIN.PICTUREBOX_ITEM
 ;
 COLLECT STATISTICS UNIQUE INDEX ( GEOG_KEY, PROD_KEY )
 ,INDEX ( Scan_Code_Seq_Num )
 ,INDEX ( Store_Seq_Num )
 ON ${DB_ENV}SemEXPT.ER_PICTUREBOX_ITEM
 ;

 -- Increment TargetExtractPeriod by 1 week for PictureBox
UPDATE ${DB_ENV}MetaDataT.TargetExtractPeriod SET From_Tran_Date = From_Tran_Date + INTERVAL '7' DAY WHERE TargetID = 11;

END P0
;
