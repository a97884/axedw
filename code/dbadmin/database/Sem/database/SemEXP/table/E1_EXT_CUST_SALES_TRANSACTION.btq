/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: E1_EXT_CUST_SALES_TRANSACTION.btq 30268 2020-02-13 09:06:32Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-02-13 10:06:32 +0100 (tor, 13 feb 2020) $
# Last Revision    : $Revision: 30268 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/table/E1_EXT_CUST_SALES_TRANSACTION.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}SemEXPT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemEXPT','E1_EXT_CUST_SALES_TRANSACTION','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE SET TABLE ${DB_ENV}SemEXPT.E1_EXT_CUST_SALES_TRANSACTION ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      Transaction_Code CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC NOT NULL,
      Sales_Tran_Seq_Num BIGINT NOT NULL,
      Sales_Tran_Line_Num SMALLINT NOT NULL,
      Tran_Dt DATE FORMAT 'yyyy-mm-dd' NOT NULL,
	  Store_Seq_Num INTEGER NOT NULL,
      CardNo VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      ReceiptNo VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      EAN BIGINT NOT NULL,
      ItemGrpId INTEGER,
      RetailerId INTEGER NOT NULL,
      SalesDate TIMESTAMP(6) NOT NULL,
      TransType CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC NOT NULL,
      QTY DECIMAL(9,3),
      PaidPrice DECIMAL(11,3),
      InkPrice DECIMAL(11,3),
      PaymType CHAR(2) NOT NULL,
      Weight DECIMAL(9,3),
      Unit DECIMAL(9,3),
      VatId VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      ArticleName 	VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
	  Store_Department_Id 	VARCHAR(50),
	  Store_Department_Name	VARCHAR(100),
	  POS_Register_Id		VARCHAR(50),
	  Workstation_Id		VARCHAR(50),
	  Sales_Associate_Id	VARCHAR(20),
	  Tendering_Method_Desc	VARCHAR(80),
	  Tendering_Type_Desc   VARCHAR(80), 
      Insert_Dttm TIMESTAMP(6),
      TS TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6))
UNIQUE PRIMARY INDEX ( Transaction_Code ,Sales_Tran_Seq_Num ,
Sales_Tran_Line_Num );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemEXPT','E1_EXT_CUST_SALES_TRANSACTION','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
