/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Read_LP_POS_4_1.btq 27772 2019-04-12 06:38:50Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-04-12 08:38:50 +0200 (fre, 12 apr 2019) $
# Last Revision    : $Revision: 27772 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/macro/Read_LP_POS_4_1.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: export sales to Leverantorsportal
#           : nollforsaljning
# Project	: 
# Subproj	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    	Description
# 2018-12-18 18249  	Initial version
# --------------------------------------------------------------------------
# Description
# Dependencies
#
# --------------------------------------------------------------------------
*/

Replace Macro ${DB_ENV}SemEXPT.Read_LP_POS_4_1(
 Vendor_Id VARCHAR(5000),
 Calendar_Week_Id_start INTEGER,
 Calendar_Week_Id_end INTEGER,
 PGRP VARCHAR(5000),
 VGRP VARCHAR(5000), 
 Concept_CD VARCHAR(5000),
 Store_id VARCHAR(5000),
 GTIN VARCHAR(5000)
 )
as
(
Lock row for access

select 
sd1.Concept_Cd
--,cd1.Concept_Name as Concept_Name
,sd1.store_id
--,sd1.store_name
,sd2.Corp_Affiliation_Type_Cd
,sd2.Corp_Affiliation_Type_Desc
,a1.Vendor_Id
--,a1.Vendor_Name
,a1.Scan_cd
--,a1.Scan_Code_Desc
--,a1.article_id
--,a1.article_desc
--,a1.Art_Hier_Lvl_2_Id as Hgrp_Id
--,a1.Art_Hier_Lvl_2_Desc as Hgrp_Desc
--,a1.Art_Hier_Lvl_3_Id as Pgrp_Id
--,a1.Art_Hier_Lvl_3_Desc as Pgrp_Desc
--,a1.Art_Hier_Lvl_4_Id as Vgrp_Id
--,a1.Art_Hier_Lvl_4_Desc as Vgrp_Desc
--,a1.Brand_Name
,a1.MU_UOM_Cd as UOM_Cd
,sa1.Article_Display_Type_Cd as expo_kod
,:Calendar_Week_Id_start
,:Calendar_Week_Id_end

FROM
(
select 
scd.Scan_Code_Seq_Num
,scd.Scan_Cd
,scd.Scan_Code_Desc
,vd.vendor_id
,vd.Vendor_Name
,ah4.Art_Hier_Lvl_4_Id
,ah4.Art_Hier_Lvl_4_Desc
,ah3.Art_Hier_Lvl_3_Id
,ah3.Art_Hier_Lvl_3_Desc
,ah2.Art_Hier_Lvl_2_Id
,ah2.Art_Hier_Lvl_2_Desc
,ad.Base_Unit_UOM_Cd
,ad.Brand_Name
,ad.Article_Seq_Num
,ad.article_id
,ad.article_desc
,mu.MU_UOM_Cd
from
(SELECT	* FROM ${DB_ENV}SemCMNVOUT.SCAN_CODE_D
where Article_Seq_Num <> -1
) as scd
INNER JOIN ${DB_ENV}SemCMNVOUT.MEASURING_UNIT_D as mu
on scd.Measuring_Unit_Seq_Num = mu.Measuring_Unit_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D as ad
on scd.Article_Seq_Num = ad.Article_Seq_Num
INNER JOIN (select Vendor_Seq_Num,TRIM(LEADING '0' FROM vendor_id) as vendor_id,Vendor_Name,vendor_id as vendor_id_org from ${DB_ENV}SemCMNVOUT.VENDOR_D) as vd
on vd.Vendor_Seq_Num = ad.Pref_Vendor_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D as ah4
on ad.Art_Hier_Lvl_4_Seq_Num = ah4.Art_Hier_Lvl_4_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_3_D as ah3
on ad.Art_Hier_Lvl_3_Seq_Num = ah3.Art_Hier_Lvl_3_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D as ah2
on ad.Art_Hier_Lvl_2_Seq_Num = ah2.Art_Hier_Lvl_2_Seq_Num
where mu.Base_MU_Ind = 1
) as a1

INNER JOIN 
(select 
t1.Store_Seq_Num,t1.Article_Seq_Num,--Article_Display_Type_Cd,
count(distinct ca1.Calendar_Dt) as antal_dagar_i_sortiment
from
(
Select  as1.Store_Seq_Num
, aa1.Article_Seq_Num
,aa1.Article_Display_Type_Cd
, Case When as1.Valid_From_Dt > aa1.Valid_From_Dt Then as1.Valid_From_Dt Else aa1.Valid_From_Dt End As Corrected_Valid_From_Dt
, Case When as1.Valid_To_Dt < aa1.Valid_To_Dt Then as1.Valid_To_Dt Else aa1.Valid_To_Dt End As Corrected_Valid_To_Dt
From ${DB_ENV}TgtVOUT.ASSORTMENT_ARTICLE  aa1
Inner Join ${DB_ENV}TgtVOUT.ASSORTMENT_STORE as1 
On as1.Assortment_Seq_Num = aa1.Assortment_Seq_Num
And (aa1.Valid_From_Dt Between as1.Valid_From_Dt and as1.Valid_To_Dt or aa1.Valid_To_Dt Between as1.Valid_From_Dt and as1.Valid_To_Dt)
 ) t1 

INNER JOIN (SELECT Calendar_Dt,Calendar_Week_Id
FROM ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D where Calendar_Week_Id between :Calendar_Week_Id_start and :Calendar_Week_Id_end) as ca1
on ca1.Calendar_Dt between t1.Corrected_Valid_From_Dt and t1.Corrected_Valid_To_Dt

where t1.Store_Seq_Num in (select store_seq_num from ${DB_ENV}SEMEXPVOUT.ER_LP_SALES_STORE_ART_F group by 1)

group by 1,2--,3
) as s1
on a1.Article_Seq_Num = s1.Article_Seq_Num

INNER JOIN (SELECT count(1) as antal_dagar_i_perioden
FROM ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D where Calendar_Week_Id between :Calendar_Week_Id_start and :Calendar_Week_Id_end) as cd2
on s1.antal_dagar_i_sortiment = cd2.antal_dagar_i_perioden

INNER JOIN ${DB_ENV}SemCMNVOUT.STORE_D as sd1
on s1.Store_Seq_Num = sd1.Store_Seq_Num and sd1.Store_Position_Type_Cd <> 'VB'

INNER JOIN ${DB_ENV}SemCMNVOUT.CONCEPT_D cd1
on sd1.Concept_Cd = cd1.Concept_Cd

INNER JOIN
(SELECT 
sdd1.Store_Seq_Num
,sdd1.Corp_Affiliation_Type_Cd
,cat1.Corp_Affiliation_Type_Desc
FROM ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as sdd1
INNER JOIN ${DB_ENV}SemCMNVOUT.CORP_AFFILIATION_TYPE_D as cat1
on sdd1.Corp_Affiliation_Type_Cd = cat1.Corp_Affiliation_Type_Cd
where sdd1.Calendar_Dt = current_date
) as sd2
on sd1.Store_Seq_Num = sd2.Store_Seq_Num

INNER JOIN ${DB_ENV}SemEXPVOUT.ER_LP_SALES_STORE_ART_F as sa1
on sa1.Scan_Code_Seq_Num = a1.Scan_Code_Seq_Num and sa1.Store_Seq_Num = s1.Store_Seq_Num and sa1.Article_Display_Type_Cd <> '-1'

LEFT OUTER JOIN
(
SELECT
Store_Seq_Num
,Article_Seq_Num
FROM ${DB_ENV}SemCMNVOUT.SALES_TRAN_LINE_WEEK_F
where Calendar_Week_Id between :Calendar_Week_Id_start and :Calendar_Week_Id_end
group by 1,2
) as f1
on a1.Article_Seq_Num = f1.Article_Seq_Num and s1.Store_Seq_Num = f1.Store_Seq_Num
  
where f1.Store_Seq_Num is null

AND 
(
Position(a1.Vendor_Id in coalesce(:Vendor_Id,'') ) > 0
Or a1.Vendor_Id = coalesce(:Vendor_Id,a1.Vendor_Id)
)

AND (
Position(a1.Art_Hier_Lvl_3_Id in coalesce(:PGRP,'') ) > 0
Or a1.Art_Hier_Lvl_3_Id = coalesce(:PGRP,a1.Art_Hier_Lvl_3_Id)
)

AND (
Position(a1.Art_Hier_Lvl_4_Id in coalesce(:VGRP,'') ) > 0
Or a1.Art_Hier_Lvl_4_Id = coalesce(:VGRP,a1.Art_Hier_Lvl_4_Id)
)

AND (
Position(sd1.Concept_CD in coalesce(:Concept_CD,'') ) > 0
Or sd1.Concept_CD = coalesce(:Concept_CD,sd1.Concept_CD)
)

AND (
Position(cast(sd1.store_id as varchar(20)) in coalesce(:Store_id,'') ) > 0
Or cast(sd1.store_id as varchar(20)) = coalesce(:Store_id,cast(sd1.store_id as varchar(20)))
)

AND (
Position(a1.Scan_Cd in coalesce(:GTIN,'') ) > 0
Or a1.Scan_Cd = coalesce(:GTIN,a1.Scan_Cd)
)


order by a1.Vendor_Id,a1.article_id,a1.Scan_cd,sd1.Concept_Cd,sd1.store_id

;

);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
