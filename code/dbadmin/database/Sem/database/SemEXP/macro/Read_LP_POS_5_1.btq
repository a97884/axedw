/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Read_LP_POS_5_1.btq 27773 2019-04-12 06:39:02Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-04-12 08:39:02 +0200 (fre, 12 apr 2019) $
# Last Revision    : $Revision: 27773 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/macro/Read_LP_POS_5_1.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: export sales to Leverantorsportal
#           : nyheter
# Project	: 
# Subproj	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    	Description
# 2018-12-18 18249  	Initial version
# --------------------------------------------------------------------------
# Description
# Dependencies
#
# --------------------------------------------------------------------------
*/

Replace Macro ${DB_ENV}SemEXPT.Read_LP_POS_5_1(
 Vendor_Id VARCHAR(50),
 Calendar_Week_Id_start INTEGER,
 Calendar_Week_Id_end INTEGER,
 PGRP VARCHAR(5000),
 VGRP VARCHAR(5000), 
 Concept_CD VARCHAR(5000),
 Store_id VARCHAR(5000),
 GTIN VARCHAR(5000)
 )
as
(
Lock row for access

SELECT 
f1.min_Sales_Tran_Dt as Sales_Start_Date
,a1.Vendor_Id
--,a1.Vendor_Name
,a1.Scan_cd
--,a1.Scan_Code_Desc
--,a1.article_id
--,a1.article_desc
--,a1.Art_Hier_Lvl_2_Id as Hgrp_Id
--,a1.Art_Hier_Lvl_2_Desc as Hgrp_Desc
--,a1.Art_Hier_Lvl_3_Id as Pgrp_Id
--,a1.Art_Hier_Lvl_3_Desc as Pgrp_Desc
--,a1.Art_Hier_Lvl_4_Id as Vgrp_Id
--,a1.Art_Hier_Lvl_4_Desc as Vgrp_Desc
--,a1.Brand_Name
,a1.MU_UOM_Cd as UOM_Cd
,sd1.Concept_Cd
--,cd1.Concept_Name as Concept_Name
,sd1.store_id
--,sd1.store_name
,f2.Item_Qty(DECIMAL (18,2)) as Item_Qty
,f2.Unit_Selling_Price_Amt (DECIMAL (18,2)) as Selling_Amt_Excl_Tax
,f2.Calendar_Week_Id
,f1.Article_Display_Type_Cd as expo_kod
,:Calendar_Week_Id_start
,:Calendar_Week_Id_end

FROM
(SELECT min(Calendar_Dt) as min_Calendar_Dt,max(Calendar_Dt) as max_Calendar_Dt
FROM ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D where Calendar_Week_Id between :Calendar_Week_Id_start and :Calendar_Week_Id_end) as ca1

INNER JOIN ${DB_ENV}SEMEXPVOUT.ER_LP_SALES_STORE_ART_F as f1
on f1.min_Sales_Tran_Dt between ca1.min_Calendar_Dt and ca1.max_Calendar_Dt

INNER JOIN
(
select 
scd.Scan_Code_Seq_Num
,scd.Scan_Cd
,scd.Scan_Code_Desc
,vd.vendor_id
,vd.Vendor_Name
,ah4.Art_Hier_Lvl_4_Id
,ah4.Art_Hier_Lvl_4_Desc
,ah3.Art_Hier_Lvl_3_Id
,ah3.Art_Hier_Lvl_3_Desc
,ah2.Art_Hier_Lvl_2_Id
,ah2.Art_Hier_Lvl_2_Desc
,ad.Base_Unit_UOM_Cd
,ad.Brand_Name
,ad.Article_Seq_Num
,ad.article_id
,ad.article_desc
,mu.MU_UOM_Cd
from
(SELECT	* FROM ${DB_ENV}SemCMNVOUT.SCAN_CODE_D
where Article_Seq_Num <> -1
) as scd
INNER JOIN ${DB_ENV}SemCMNVOUT.MEASURING_UNIT_D as mu
on scd.Measuring_Unit_Seq_Num = mu.Measuring_Unit_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D as ad
on scd.Article_Seq_Num = ad.Article_Seq_Num
INNER JOIN (select Vendor_Seq_Num,TRIM(LEADING '0' FROM vendor_id) as vendor_id,Vendor_Name,vendor_id as vendor_id_org from ${DB_ENV}SemCMNVOUT.VENDOR_D) as vd
on vd.Vendor_Seq_Num = ad.Pref_Vendor_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D as ah4
on ad.Art_Hier_Lvl_4_Seq_Num = ah4.Art_Hier_Lvl_4_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_3_D as ah3
on ad.Art_Hier_Lvl_3_Seq_Num = ah3.Art_Hier_Lvl_3_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D as ah2
on ad.Art_Hier_Lvl_2_Seq_Num = ah2.Art_Hier_Lvl_2_Seq_Num
) as a1
on a1.Scan_Code_Seq_Num = f1.Scan_Code_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.STORE_D as sd1
on f1.Store_Seq_Num = sd1.Store_Seq_Num and sd1.Store_Position_Type_Cd <> 'VB'

INNER JOIN ${DB_ENV}SemCMNVOUT.CONCEPT_D cd1
on sd1.Concept_Cd = cd1.Concept_Cd

INNER JOIN 
(SELECT
Scan_Code_Seq_Num, Store_Seq_Num,Calendar_Week_Id, 
sum(Item_Qty) as Item_Qty,
sum(Unit_Selling_Price_Amt) as Unit_Selling_Price_Amt
FROM ${DB_ENV}SemCMNVOUT.SALES_TRAN_LINE_WEEK_F 
where Calendar_Week_Id between :Calendar_Week_Id_start and :Calendar_Week_Id_end
group by 1,2,3
) as f2
on f1.Scan_Code_Seq_Num = f2.Scan_Code_Seq_Num and f1.Store_Seq_Num = f2.Store_Seq_Num

where
a1.Vendor_Id = :Vendor_Id

AND (
Position(a1.Art_Hier_Lvl_3_Id in coalesce(:PGRP,'') ) > 0
Or a1.Art_Hier_Lvl_3_Id = coalesce(:PGRP,a1.Art_Hier_Lvl_3_Id)
)

AND (
Position(a1.Art_Hier_Lvl_4_Id in coalesce(:VGRP,'') ) > 0
Or a1.Art_Hier_Lvl_4_Id = coalesce(:VGRP,a1.Art_Hier_Lvl_4_Id)
)

AND (
Position(sd1.Concept_CD in coalesce(:Concept_CD,'') ) > 0
Or sd1.Concept_CD = coalesce(:Concept_CD,sd1.Concept_CD)
)

AND (
Position(a1.Scan_Cd in coalesce(:GTIN,'') ) > 0
Or a1.Scan_Cd = coalesce(:GTIN,a1.Scan_Cd)
)

AND (
Position(cast(sd1.store_id as varchar(20)) in coalesce(:Store_id,'') ) > 0
Or cast(sd1.store_id as varchar(20)) = coalesce(:Store_id,cast(sd1.store_id as varchar(20)))
)

AND f2.Item_Qty <> 0

;

);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
