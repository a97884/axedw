/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Read_LP_POS_Week_Agg.btq 27768 2019-04-12 06:37:53Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-04-12 08:37:53 +0200 (fre, 12 apr 2019) $
# Last Revision    : $Revision: 27768 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/macro/Read_LP_POS_Week_Agg.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: export sales to Leverantorsportal
#           : sales-week
# Project	: 
# 			:
# --------------------------------------------------------------------------
# Change History
# Date       Author    	Description
# 2019-01-08 18249  	Initial version
# --------------------------------------------------------------------------
# Description
# Dependencies
#
# --------------------------------------------------------------------------
*/

Replace Macro ${DB_ENV}SemEXPT.Read_LP_POS_Week_Agg(
 Vendor_Id VARCHAR(50),
 Calendar_Week_Id INTEGER,
 VGRP VARCHAR(5000)
 )
as
(
Lock row for access
select 
Week
,Vendor_Id
--,Vendor_Name
,Scan_cd
--,Scan_Code_Desc
--,Hgrp_Id
--,Hgrp_Desc
--,Pgrp_Id
--,Pgrp_Desc
--,Vgrp_Id
--,Vgrp_Desc
--,Brand_Name
,Concept_Cd
--,Concept_Name
,Store_id
--,Store_name
,UOM_Cd
,Item_Qty
,Selling_Amt_Excl_Tax
FROM

(select 
f1.Calendar_Week_Id as Week
,a1.Vendor_Id
,a1.Vendor_Name
,a1.Scan_cd
,a1.Scan_Code_Desc
,a1.Art_Hier_Lvl_2_Id as Hgrp_Id
,a1.Art_Hier_Lvl_2_Desc as Hgrp_Desc
,a1.Art_Hier_Lvl_3_Id as Pgrp_Id
,a1.Art_Hier_Lvl_3_Desc as Pgrp_Desc
,a1.Art_Hier_Lvl_4_Id as Vgrp_Id
,a1.Art_Hier_Lvl_4_Desc as Vgrp_Desc
,a1.Brand_Name
,s1.Concept_Cd
,cd1.Concept_Name as Concept_Name
,s1.Store_id
,s1.Store_name
,a1.MU_UOM_Cd as UOM_Cd
,sum(f1.Item_Qty) (DECIMAL (18,2)) as Item_Qty
,sum(f1.Unit_Selling_Price_Amt) (DECIMAL (18,2)) as Selling_Amt_Excl_Tax

FROM

(
select 
scd.Scan_Code_Seq_Num
,scd.Scan_Cd
,scd.Scan_Code_Desc
,vd.vendor_id
,vd.Vendor_Name
,ah4.Art_Hier_Lvl_4_Id
,ah4.Art_Hier_Lvl_4_Desc
,ah3.Art_Hier_Lvl_3_Id
,ah3.Art_Hier_Lvl_3_Desc
,ah2.Art_Hier_Lvl_2_Id
,ah2.Art_Hier_Lvl_2_Desc
,ad.Base_Unit_UOM_Cd
,ad.Brand_Name
,mu.MU_UOM_Cd
from
(SELECT	* FROM ${DB_ENV}SemCMNVOUT.SCAN_CODE_D
where Article_Seq_Num <> -1
) as scd
INNER JOIN ${DB_ENV}SemCMNVOUT.MEASURING_UNIT_D as mu
on scd.Measuring_Unit_Seq_Num = mu.Measuring_Unit_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D as ad
on scd.Article_Seq_Num = ad.Article_Seq_Num
INNER JOIN (select Vendor_Seq_Num,TRIM(LEADING '0' FROM vendor_id) as vendor_id,Vendor_Name,vendor_id as vendor_id_org from ${DB_ENV}SemCMNVOUT.VENDOR_D) as vd
on vd.Vendor_Seq_Num = ad.Pref_Vendor_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D as ah4
on ad.Art_Hier_Lvl_4_Seq_Num = ah4.Art_Hier_Lvl_4_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_3_D as ah3
on ad.Art_Hier_Lvl_3_Seq_Num = ah3.Art_Hier_Lvl_3_Seq_Num
INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D as ah2
on ad.Art_Hier_Lvl_2_Seq_Num = ah2.Art_Hier_Lvl_2_Seq_Num
) as a1

INNER JOIN 
(SELECT
Scan_Code_Seq_Num, Store_Seq_Num,Calendar_Week_Id,
UOM_Category_Cd, 
Item_Qty,Unit_Selling_Price_Amt
FROM ${DB_ENV}SemCMNVOUT.SALES_TRAN_LINE_WEEK_F 
) as f1
on a1.Scan_Code_Seq_Num = f1.Scan_Code_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.STORE_D as s1
on s1.Store_Seq_Num = f1.Store_Seq_Num and s1.Store_Position_Type_Cd <> 'VB'

INNER JOIN ${DB_ENV}SemCMNVOUT.CONCEPT_D cd1
on s1.Concept_Cd = cd1.Concept_Cd
 
Where 
(
Position(a1.Vendor_Id in coalesce(:Vendor_Id,'') ) > 0
Or a1.Vendor_Id = coalesce(:Vendor_Id,a1.Vendor_Id)
)

and f1.Calendar_Week_Id = :Calendar_Week_Id

AND (
Position(a1.Art_Hier_Lvl_4_Id in coalesce(:VGRP,'') ) > 0
Or a1.Art_Hier_Lvl_4_Id = coalesce(:VGRP,a1.Art_Hier_Lvl_4_Id)
)

group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
) as r1
where Item_Qty <> 0
order by 1,2,Scan_cd,Store_id

;

);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
