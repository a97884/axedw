/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: MSTR_PRODUCTIVITY.btq 24553 2018-03-29 13:44:31Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-03-29 15:44:31 +0200 (tor, 29 mar 2018) $
# Last Revision    : $Revision: 24553 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemMetadata/table/MSTR_PRODUCTIVITY.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

.SET MAXERROR 0;

DATABASE ${DB_ENV}SemMetadataT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemMetadataT','MSTR_PRODUCTIVITY','PRE','IO',1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY
     (
      Calendar_Month_Id INTEGER NOT NULL DEFAULT -1 ,
      Calendar_Year_Id INTEGER NOT NULL DEFAULT -1 ,
      N_Location_Id INTEGER NOT NULL DEFAULT -1 ,
      N_Location_Zone_Id VARCHAR(100) CHARACTER SET LATIN NOT CASESPECIFIC NOT NULL,
      Target_AP_Value DECIMAL(18,6),
      Number_of_Packages DECIMAL(18,4),
      Number_Of_Hours DECIMAL(18,4) DEFAULT -1.0000,
	  CONSTRAINT PKMSTR_PRODUCTIVITY PRIMARY KEY (Calendar_Month_Id ,N_Location_Id ,N_Location_Zone_Id)
	  )
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemMetadataT','MSTR_PRODUCTIVITY','POST','IO',1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY IS '$Revision: 24553 $ - $Date: 2018-03-29 15:44:31 +0200 (tor, 29 mar 2018) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COLLECT STATISTICS 
	COLUMN(Calendar_Month_Id),
	COLUMN(Calendar_Year_Id),
	COLUMN(Calendar_Month_Id,Calendar_Year_Id),
	COLUMN(N_Location_Id),
	COLUMN(N_Location_Zone_Id)
ON ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY
AS LOCKING ROW FOR ACCESS 
SELECT 
 m1.Calendar_Month_Id, 
 m1.Calendar_Year_Id,
 m1.N_Location_Id, 
 m1.N_Location_Zone_Id, 
 coalesce(d1.Loc_Seq_Num,-1) as Location_Seq_Num,
 coalesce(d2.Location_Zone_Seq_Num,-1) as Location_Zone_Seq_Num,
 Case When m1.Number_Of_Hours = 0 then 0 else (m1.Number_of_Packages/Number_Of_Hours) End as Target_AP_Value,
 m1.Number_of_Packages, 
 m1.Number_Of_Hours
 FROM ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY m1
 LEFT OUTER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOC as d1
 on m1.N_Location_Id = d1.N_Loc_Id
 LEFT OUTER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOCATION_ZONE as d2
 on m1.N_Location_Id = d2.N_Loc_Id and  m1.N_Location_Zone_Id = d2.N_Location_Zone_Id
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
