/*
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: RunRModel.btq 25425 2018-08-21 15:12:11Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2018-08-21 17:12:11 +0200 (tis, 21 aug 2018) $
# Last Revision    : $Revision: 25425 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/ADS/procedure/RunRModel.btq $
# --------------------------------------------------------------------------
# SCM Info END
*/
REPLACE PROCEDURE $#DB_ENV#$#ADS_DB#T.RunRModel(
	 Model_Id INTEGER
	,Source_DB_Name VARCHAR(128)
	,Source_TABLE_Name VARCHAR(128)
	,Source_WHERE_Condition VARCHAR(1024)
	,Hash_Partition_By VARCHAR(1024)
	,Target_DB_Name VARCHAR(128)
	,Target_TABLE_Name VARCHAR(128)
	,Debug INTEGER
	,OUT ErrorStr VARCHAR(255)
	,OUT Generated_SQL_Text VARCHAR(30000)
)
BEGIN
	DECLARE SQL_Text VARCHAR(30000);
	DECLARE Contract_txt VARCHAR(1000);
	DECLARE Operator_txt VARCHAR(30000);
	DECLARE KeepLog INTEGER;
	DECLARE CONTINUE HANDLER FOR SQLSTATE VALUE '23505' SET ErrorStr = 'Duplicate Row during insert';
	
	SET KeepLog = 0;
	IF Debug <> 0
	THEN
		SET KeepLog = 1;
	END IF
	;

	SELECT R_Contract, R_Operator
	INTO :Contract_txt, :Operator_txt
	FROM $#DB_ENV#$#TARGET_DB#VOUT.ANALYTICAL_MODEL_R_SCRIPT Where Model_Id = :Model_Id
	;
	
	SET SQL_Text = '
	INSERT INTO ' || Target_DB_Name || '.' || Target_TABLE_Name || '
	SELECT * FROM TD_SYSGPL.ExecR (
	ON (SELECT * FROM ' || Source_DB_Name || '.' || Source_TABLE_Name || ' Where ' || Source_WHERE_Condition || ') ' ||
	Hash_Partition_By || '
	ON ( SELECT Model_Id, R_Binary FROM $#DB_ENV#$#TARGET_DB#VOUT.ANALYTICAL_MODEL_R_BINARY Where Model_Id = ' || Model_Id || '
	     AND InsertDttm = (SELECT MAX(InsertDttm) FROM $#DB_ENV#$#TARGET_DB#VOUT.ANALYTICAL_MODEL_R_BINARY Where Model_Id = ' || Model_Id || ') ) DIMENSION ' || '
	USING
/* The following does not work, use variables instead. Need investigation.
	CONTRACT( ' || '
	SELECT R_Contract FROM $#DB_ENV#$#TARGET_DB#VOUT.ANALYTICAL_MODEL_R_SCRIPT Where Model_Id = ' || Model_Id || ')' || '
	OPERATOR( ' || '
	SELECT R_Operator FROM $#DB_ENV#$#TARGET_DB#VOUT.ANALYTICAL_MODEL_R_SCRIPT Where Model_Id = ' || Model_Id || ')' || '
*/
	CONTRACT( ''' || Contract_txt || ''')' || '
	OPERATOR( ''' || Operator_txt || ''')' || '
	keeplog(' || KeepLog || ')
	) as D
	' ;

	SET Generated_SQL_Text = SQL_Text;

	IF Debug = 0
	THEN
		CALL DBC.SysExecSQL( SQL_Text );
	END IF
	;
END;
