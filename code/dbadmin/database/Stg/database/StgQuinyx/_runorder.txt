---------- Tables
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/database/StgQuinyx.btq

-- STG Tables
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/Stg_Quinyx_Payroll.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/Stg_Quinyx_PayrollQ.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/Stg_Quinyx_Schedules.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/Stg_Quinyx_SchedulesQ.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/Stg_Quinyx_APIResponse.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/Stg_Quinyx_APIResponseQ.btq
--
-- S1 Tables
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/S1_Quinyx_Payroll.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/S1_Quinyx_Schedules.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/S1_Quinyx_APIResponse.btq

-- S2 Tables
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/S2_Quinyx_Payroll.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/S2_Quinyx_Schedules.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/table/S2_Quinyx_APIResponse.btq
-- Views
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/view/Stg_Quinyx_Payroll_vc.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/view/Stg_Quinyx_Schedules_vc.btq
BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/view/Stg_Quinyx_APIResponse_vc.btq
-- Procedures
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Quinyx_Payroll_ReadQ.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Quinyx_Payroll_PurgeDataFrmStg.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Load_Stg_Quinyx_Payroll.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Quinyx_Schedules_ReadQ.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Quinyx_Schedules_PurgeDataFrmStg.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Load_Stg_Quinyx_Schedules.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Quinyx_APIResponse_ReadQ.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Quinyx_APIResponse_PurgeDataFrmStg.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgQuinyx/procedure/Load_Stg_Quinyx_GetRestaurantAPI.btq

-- Triggers
--BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/trigger/Load_Stg_Quinyx_PayrollQ.btq
--BTQRUN code/dbadmin/database/Stg/database/StgQuinyx/trigger/Load_Stg_Quinyx_PayrollQ.btq
