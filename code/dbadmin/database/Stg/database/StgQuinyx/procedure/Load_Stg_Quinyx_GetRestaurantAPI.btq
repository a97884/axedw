/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_Quinyx_GetRestaurantAPI.btq 21053 2017-01-17 10:10:11Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-01-17 11:10:11 +0100 (tis, 17 jan 2017) $
# Last Revision    : $Revision: 21053 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgQuinyx/procedure/Load_Stg_Quinyx_GetRestaurantAPI.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this procedure for insert in stg-table
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Sonic call this procedure for insert in stg-table
# Dependencies
#	${DB_ENV}StgSAPT.Stg_Quinyx_APIResponse
#	${DB_ENV}StgSAPT.Stg_Quinyx_APIResponseQ
#	
# --------------------------------------------------------------------------
*/



REPLACE PROCEDURE ${DB_ENV}StgQuinyxT.Load_Stg_Quinyx_GetRestaurantAPI
(	IN SequenceId	VARCHAR(100)
	,IN ContentType	VARCHAR(30)
	,IN SubContentType	VARCHAR(30)
	,IN TransactionTS	VARCHAR(26)
	,IN ExtractionTS	VARCHAR(26)
	,IN IEReceivedTS	VARCHAR(26)
	,IN IEDeliveryTS	VARCHAR(26)
	,IN SourceVersion	VARCHAR(10)
	,IN XmlData	CLOB(6000000) CHARACTER SET UNICODE
)
BEGIN

INSERT INTO ${DB_ENV}StgQuinyxT.Stg_Quinyx_APIResponse
(	SequenceId
	,ContentType
	,SubContentType
	,TransactionTS
	,ExtractionTS
	,IEReceivedTS
	,IEDeliveryTS
	,SourceVersion
	,ArchiveKey
	,XmlData
) VALUES (
	:SequenceId
	,:ContentType
	,:SubContentType
	,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,:SourceVersion
	,(	TRIM(:SequenceId)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
		)
	,:XmlData 
);

	INSERT INTO ${DB_ENV}StgQuinyxT.Stg_Quinyx_APIResponseQ
	(	SequenceId
		,ReferenceTS
	) VALUES (
		:SequenceId
		,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	);

END;
