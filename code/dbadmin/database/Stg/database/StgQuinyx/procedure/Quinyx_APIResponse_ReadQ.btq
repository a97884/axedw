/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Quinyx_APIResponse_ReadQ.btq 20983 2017-01-13 07:39:03Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-01-13 08:39:03 +0100 (fre, 13 jan 2017) $
# Last Revision    : $Revision: 20983 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgQuinyx/procedure/Quinyx_APIResponse_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this procedure for insert in stg-table
# Project	: EDW1
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-02-06 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Sonic call this procedure for insert in stg-table
# Dependencies
#	
#	
#	
# --------------------------------------------------------------------------
*/



REPLACE PROCEDURE ${DB_ENV}StgQuinyxT.Quinyx_APIResponse_ReadQ(IN WorkflowRunId VARCHAR(255))

BEGIN

DECLARE RecordCount INT; 					-- To keep frequency Limit
DECLARE Counter INT; 						-- To keep loop counter
DECLARE QCount INT; 						-- To keep count of records present in queue table

DECLARE QITS TIMESTAMP(6);					-- To keep QITS read from queue     
DECLARE SequenceId INT; 					-- To keep SequenceId read from queue
DECLARE SAPCustomerID INT; 					-- To keep SAPCustomerID read from queue
DECLARE ContentType VARCHAR(30); 			--  To keep ContentType read from queue
DECLARE SubContentType	VARCHAR(30);		--  To keep SubContentType read from queue	
DECLARE TransactionTS TIMESTAMP(6);			--  To keep TransactionTS read from queue	
DECLARE IEDeliveryTS TIMESTAMP(6);			--  To keep IEDeliveryTS read from queue	  


INSERT INTO ${DB_ENV}UtilT.Quinyx_APIResponse_Load_Cache (  -- insert in cache table          
		 TS
		,SequenceId    
		,TransactionTS 
)
SELECT AND CONSUME TOP 1
		 TS
		,SequenceId    
		,ReferenceTS 
FROM 
		${DB_ENV}StgQuinyxT.Stg_Quinyx_APIResponseQ;
		
LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgQuinyxT.Stg_Quinyx_APIResponseQ; -- Get current row count of Queue table 

BT;
WHILE QCount > 0 -- Loop until queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.Quinyx_APIResponse_Load_Cache (  -- insert in cache table          
		 TS
		,SequenceId    
		,TransactionTS 
)
SELECT AND CONSUME TOP 1
		 TS
		,SequenceId    
		,ReferenceTS 
FROM 
		${DB_ENV}StgQuinyxT.Stg_Quinyx_APIResponseQ;


SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop


ET;



END;