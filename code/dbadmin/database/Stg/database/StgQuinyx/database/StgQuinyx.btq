/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StgQuinyx.btq 21455 2017-02-13 09:26:19Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-02-13 10:26:19 +0100 (mån, 13 feb 2017) $
# Last Revision    : $Revision: 21455 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgQuinyx/database/StgQuinyx.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgQuinyx
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgQuinyx';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgQuinyx
FROM ${DB_ENV}Stg AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgQuinyx AS
'Parent Database for source data from Quinyx'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgQuinyx TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgQuinyxT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgQuinyxT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgQuinyxT
FROM ${DB_ENV}StgQuinyx AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgQuinyxT AS
'Database holding stagingtables from Quinyx'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgQuinyxT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgQuinyxVIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgQuinyxVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgQuinyxVIN
FROM ${DB_ENV}StgQuinyx AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgQuinyxVIN AS
'Database holding views for loading stagingtables from Quinyx'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgQuinyxVIN TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}StgQuinyxT TO ${DB_ENV}StgQuinyxVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgQuinyxVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgQuinyxVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgQuinyxVOUT
FROM ${DB_ENV}StgQuinyx AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgQuinyxVOUT AS
'Database holding views for reading stagingtables from Quinyx'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgQuinyxVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgQuinyxT TO ${DB_ENV}StgQuinyxVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT ON Sys_Calendar TO ${DB_ENV}StgQuinyxVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* ===============================================================================
These grants are required to support usage of dynamically (runtime) created views  
within Informatica PowerCenter (push-down optimization)
=============================================================================== */
-- Read Access on MetaData for stage dbs (update when adding new sources)
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}MetaDataVOUT TO ${DB_ENV}StgQuinyxT,${DB_ENV}StgQuinyxVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read & Delete Access on UtilT for stage dbs
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}UtilT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}UtilT TO ${DB_ENV}StgQuinyxT,${DB_ENV}StgQuinyxVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Execute functions in SYSLIB
SELECT * FROM DBC.Databases WHERE DatabaseName = 'SYSLIB';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT EXECUTE FUNCTION ON SYSLIB TO ${DB_ENV}StgQuinyxT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgQuinyx to Cntl
SELECT * FROM DBC.Databases WHERE DatabaseName IN ( '${DB_ENV}CntlCleanseT', '${DB_ENV}CntlCleanseVIN');
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 2 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgQuinyxT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgQuinyxVOUT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT DROP TABLE ON ${DB_ENV}StgQuinyxT TO ${DB_ENV}CntlCleanseT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgQuinyx to MetadataVIN
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgQuinyxT TO ${DB_ENV}MetaDataVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on Achv to StgQuinyx 
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}Achv';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}Achv TO ${DB_ENV}StgQuinyxT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on Cntl to StgQuinyx
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}CntlCleanseT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}CntlCleanseT TO ${DB_ENV}StgQuinyxT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on Util to StgQuinyx
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}UtilT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}UtilT TO ${DB_ENV}StgQuinyxT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT