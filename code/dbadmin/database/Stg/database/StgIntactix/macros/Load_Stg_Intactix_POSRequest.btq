/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_Intactix_POSRequest.btq 10262 2013-09-13 11:33:36Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2013-09-13 13:33:36 +0200 (fre, 13 sep 2013) $
# Last Revision    : $Revision: 10262 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgIntactix/macros/Load_Stg_Intactix_POSRequest.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this macro for INSERT IN stg-table Stg_Intactix_POSRequest
# Project	: AxEDW
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2013-06-13 Sabel  Initial version
# --------------------------------------------------------------------------
# Description
# Dependencies
#	${DB_ENV}StgAxBOT.Stg_OAS_POSRequest
# --------------------------------------------------------------------------
*/

REPLACE MACRO ${DB_ENV}StgIntactixT.Load_Stg_Intactix_POSRequest
( 		  QID INTEGER
		, DBKey INTEGER
		, LMID INTEGER
		, ArticleID INTEGER
		, SAPCustomerID INTEGER
		, FromDttm TIMESTAMP(6) 
		, ToDttm TIMESTAMP(6) 
		, Flag1 BYTEINT
		, Flag2 BYTEINT
		, IEReceivedTS TIMESTAMP(6) 
		, IEDeliveryTS TIMESTAMP(6) 
) AS (

INSERT INTO ${DB_ENV}StgIntactixT.Stg_Intactix_POSRequest
(		QID, 
		DBKey, 
		LMID, 
		ArticleID, 
		SAPCustomerID, 
		FromDttm, 
		ToDttm,
		Flag1, 
		Flag2, 
		ContentType, 
		SubContentType, 
		ExtractionTS, 
		IEReceivedTS,
		IEDeliveryTS, 
		SourceVersion, 
		ArchiveKey)
 VALUES (
	 :QID
	,:DBKey
	,:LMID
	,:ArticleID
	,:SAPCustomerID
	,CAST(:FromDttm AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:ToDttm AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,:Flag1
	,:Flag2
	,'INTACTIX'
	,NULL
	,NULL
	,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,'1.0'
	,(	:QID
		|| '_' || :DBKey
		|| '_' || :LMID
		|| '_' || :ArticleID
		|| '_' || :SAPCustomerID
		|| '_' || SUBSTRING(CAST(:IEDeliveryTS AS VARCHAR(26)) FROM 1 FOR 10)
 		|| '_' || SUBSTRING(CAST(:IEDeliveryTS AS VARCHAR(26)) FROM 12 FOR 15)
		)
	 );
);
