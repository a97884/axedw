/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Intactix_POS_ReadQ.btq 10388 2013-09-18 13:10:47Z K9105806 $
# Last Changed By  : $Author: K9105806 $
# Last Change Date : $Date: 2013-09-18 15:10:47 +0200 (ons, 18 sep 2013) $
# Last Revision    : $Revision: 10388 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgIntactix/procedure/Intactix_POS_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for HistoricPOS Reading Queue
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for HistoricPOS Reading Queue
# Dependencies
#	${DB_ENV}StgOAST.Stg_OAS_POSRequestQ
#	${DB_ENV}StgOAST.OAS_HistoricPOS_Load_Cache
# --------------------------------------------------------------------------
*/

--dummy change2 130913

REPLACE PROCEDURE ${DB_ENV}StgIntactixT.Intactix_POS_ReadQ(INOUT WorkflowRunId VARCHAR(255))
BEGIN
DECLARE QCount INT; -- To keep count of records present in queue table

LOCK ROW FOR ACCESS 
SELECT COUNT(*) 
	   INTO :QCount 
 FROM ${DB_ENV}StgIntactixT.Stg_Intactix_POSRequestQ; -- Get current row count of Queue table 

BT;

WHILE QCount > 0 -- Loop until queue is emtpy
DO
-- Select and Consume top row from queue table and insert in cache table
	INSERT INTO ${DB_ENV}UtilT.Intactix_POS_Load_Cache (  
	TS,
	QID,
	WorkflowRunId
	)
	SELECT AND CONSUME TOP 1
	  TS,
	  QID,
	  :WorkflowRunId
	FROM
	 ${DB_ENV}StgIntactixT.Stg_Intactix_POSRequestQ
	;

	SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END;
