/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_OAS_SeasonProposal.btq 17570 2015-11-10 11:53:22Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2015-11-10 12:53:22 +0100 (tis, 10 nov 2015) $
# Last Revision    : $Revision: 17570 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgOAS/macros/Load_Stg_OAS_SeasonProposal.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this macro for INSERT IN stg-table OAS_SeasonProposal
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2015-06-15 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Trigger that inserts new reccords in queue-table
# Dependencies
#	${DB_ENV}StgAxBOT.Stg_OAS_SeasonProposal
#	${DB_ENV}StgAxBOT.Stg_OAS_SeasonProposalQ
# --------------------------------------------------------------------------
*/


REPLACE MACRO ${DB_ENV}StgOAST.Load_Stg_OAS_SeasonProposal
( SequenceId  VARCHAR(40)
 ,ContentType VARCHAR(30)
 ,SubContentType VARCHAR(30)
 ,TransactionTS CHAR(26)
 ,ExtractionTS CHAR(26)
 ,IEReceivedTS CHAR(26)
 ,IEDeliveryTS CHAR(26)
 ,SourceVersion VARCHAR(10)
 ,XmlData CLOB(5242880)
) AS (

 INSERT INTO ${DB_ENV}StgOAST.Stg_OAS_SeasonProposal
 ( SequenceId 
  ,ContentType
  ,SubContentType
  ,TransactionTS
  ,ExtractionTS
  ,IEReceivedTS
  ,IEDeliveryTS
  ,SourceVersion
  ,TS
  ,ArchiveKey
  ,XmlData
 ) VALUES (
  :SequenceId 
  ,:ContentType
  ,:SubContentType
  ,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,:SourceVersion
  ,CURRENT_TIMESTAMP
  ,( TRIM(:SequenceId)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
  )
  ,:XmlData
 );
);


