/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_OAS_OnOrder.btq 4435 2012-07-23 08:40:20Z k9105777 $
# Last Changed By  : $Author: k9105777 $
# Last Change Date : $Date: 2012-07-23 10:40:20 +0200 (mån, 23 jul 2012) $
# Last Revision    : $Revision: 4435 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgOAS/macros/Load_Stg_OAS_OnOrder.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this macro for INSERT IN stg-table OAS_OnOrder
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-02-02 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Trigger that inserts new reccords in queue-table
# Dependencies
#	${DB_ENV}StgAxBOT.Stg_OAS_OnOrder
#	${DB_ENV}StgAxBOT.Stg_OAS_OnOrderQ
# --------------------------------------------------------------------------
*/

REPLACE MACRO ${DB_ENV}StgOAST.Load_Stg_OAS_OnOrder
( SequenceId  VARCHAR(40)
 ,SAPCustomerId VARCHAR(15) 
 ,ContentType VARCHAR(30)
 ,SubContentType VARCHAR(30)
 ,TransactionTS CHAR(26)
 ,ExtractionTS CHAR(26)
 ,IEReceivedTS CHAR(26)
 ,IEDeliveryTS CHAR(26)
 ,SourceVersion VARCHAR(10)
 ,XmlData CLOB(5242880)
) AS (

 INSERT INTO ${DB_ENV}StgOAST.Stg_OAS_OnOrder
 ( SequenceId 
  ,SAPCustomerId
  ,ContentType
  ,SubContentType
  ,TransactionTS
  ,ExtractionTS
  ,IEReceivedTS
  ,IEDeliveryTS
  ,SourceVersion
  ,TS
  ,ArchiveKey
  ,XmlData
 ) VALUES (
  :SequenceId 
  ,:SAPCustomerId
  ,:ContentType
  ,:SubContentType
  ,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,:SourceVersion
  ,CURRENT_TIMESTAMP
  ,( TRIM(:SequenceId)
  || '_' || TRIM(:SAPCustomerId)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
  )
  ,:XmlData
 );
);