/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: OASCampaignProposal_ReadQ.btq 13157 2014-06-03 08:52:08Z k9107640 $
# Last Changed By  : $Author: k9107640 $
# Last Change Date : $Date: 2014-06-03 10:52:08 +0200 (tis, 03 jun 2014) $
# Last Revision    : $Revision: 13157 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgOAS/procedure/OASCampaignProposal_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for OrderProposal Reading Queue
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for CampaignProposal Reading Queue
# Dependencies
#	${DB_ENV}StgOAST.Stg_OAS_CampaignProposalQ
#	${DB_ENV}StgOAST.OAS_CampaignProposal_Load_Cache
# --------------------------------------------------------------------------
*/




REPLACE PROCEDURE ${DB_ENV}StgOAST.OASCampaignProposal_ReadQ(IN WorkflowRunId VARCHAR(255))
BEGIN
DECLARE QCount INT; -- To keep count of records present in queue table


INSERT INTO ${DB_ENV}UtilT.OAS_CampaignProposal_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
    ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalQ; -- Get current row count of Queue table 

BT;

WHILE QCount > 0 -- Loop until queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.OAS_CampaignProposal_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
    ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalQ
;

SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END;

