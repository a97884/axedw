/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SAPCustomerAgreement_ReadQ.btq 22877 2017-08-25 05:51:33Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-08-25 07:51:33 +0200 (fre, 25 aug 2017) $
# Last Revision    : $Revision: 22877 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/procedure/SAPCustomerAgreement_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for Customer Reading Queue
# Project	: EDW1
# Subproject:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for Customer Reading Queue aggrement
# Dependencies
#	${DB_ENV}StgSAPT.Stg_CustomerQ
#	${DB_ENV}UtilT.SAP_Customer_Load_Cache
# --------------------------------------------------------------------------
*/
REPLACE PROCEDURE ${DB_ENV}StgSAPT.SAPCustomerAgreement_ReadQ(IN WorkflowRunId VARCHAR(255))
BEGIN

DECLARE RecordCount INT;      -- To keep frequency Limit
DECLARE Counter INT;       -- To keep loop counter
DECLARE QCount INT;       -- To keep count of records present in queue table

SET Counter = 1;       -- Set Counter to 1

SELECT coalesce(RecordCount,-1) INTO :RecordCount FROM ${DB_ENV}StgSAPT.Stg_SAP_Load_Frequency WHERE SourceID = 80; -- Getting Frequency Limit

SET RecordCount = coalesce(RecordCount,-1);

IF RecordCount = -1 THEN

BT;

INSERT INTO ${DB_ENV}UtilT.SAP_CustomerAgreement_Load_Cache (  -- Select all records from queue table and insert in cache table
  SequenceId
 ,TransactionTS
 ,ExtractionTS
 ,WorkflowRunId
 ,TS
)
SELECT
  SequenceId
 ,TransactionTS
 ,ExtractionTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_CustomerAgreementQ
;

DELETE FROM ${DB_ENV}StgSAPT.Stg_CustomerAgreementQ;

ET;

ELSE

BT;

INSERT INTO ${DB_ENV}UtilT.SAP_CustomerAgreement_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,TransactionTS
 ,ExtractionTS
 ,WorkflowRunId
 ,TS
)
SELECT AND CONSUME TOP 1
  SequenceId
 ,TransactionTS
 ,ExtractionTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_CustomerAgreementQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgSAPT.Stg_CustomerAgreementQ; -- Get current row count of Queue table 

WHILE Counter < RecordCount AND QCount > 0 -- Loop until frequeny limit is reached or queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.SAP_CustomerAgreement_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,TransactionTS
 ,ExtractionTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,TransactionTS
 ,ExtractionTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_CustomerAgreementQ
;

SET Counter = Counter + 1; 
SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END IF;

END;
