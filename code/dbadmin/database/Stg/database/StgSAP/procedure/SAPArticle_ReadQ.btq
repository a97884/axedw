/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SAPArticle_ReadQ.btq 4340 2012-07-13 12:12:41Z k9107060 $
# Last Changed By  : $Author: k9107060 $
# Last Change Date : $Date: 2012-07-13 14:12:41 +0200 (fre, 13 jul 2012) $
# Last Revision    : $Revision: 4340 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/procedure/SAPArticle_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for Article Reading Queue
# Project	: EDW1
# Subproject:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for Article Reading Queue
# Dependencies
#	${DB_ENV}StgSAPT.Stg_ArticleQ
#	${DB_ENV}UtilT.SAP_Article_Load_Cache
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}StgSAPT.SAPArticle_ReadQ(IN WorkflowRunId VARCHAR(255))
BEGIN
DECLARE QCount INT; -- To keep count of records present in queue table


INSERT INTO ${DB_ENV}UtilT.SAP_Article_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
    ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_ArticleQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgSAPT.Stg_ArticleQ; -- Get current row count of Queue table 

BT;

WHILE QCount > 0 -- Loop until queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.SAP_Article_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
    ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_ArticleQ
;

SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END;