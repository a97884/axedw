/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_PMR_Promotion.btq 25302 2018-07-27 10:22:24Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-07-27 12:22:24 +0200 (fre, 27 jul 2018) $
# Last Revision    : $Revision: 25302 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/procedure/Load_Stg_PMR_Promotion.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: 
# Project	: 
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2015-01-13 Peter H  Initial version
# --------------------------------------------------------------------------
# Description
#	
# Dependencies
#	
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}StgSAPT.Load_Stg_PMR_Promotion
( IN SequenceId  VARCHAR(40)
 ,IN ContentType VARCHAR(30)
 ,IN SubContentType VARCHAR(30)
 ,IN TransactionTS CHAR(26)
 ,IN ExtractionTS CHAR(26)
 ,IN IEReceivedTS CHAR(26)
 ,IN IEDeliveryTS CHAR(26)
 ,IN SourceVersion VARCHAR(10)
 ,IN XmlData CLOB(5000000) CHARACTER SET UNICODE
)  
BEGIN

 INSERT INTO ${DB_ENV}StgSAPT.Stg_PMR_Promotion
 ( SequenceId 
  ,ContentType
  ,SubContentType
  ,TransactionTS
  ,ExtractionTS
  ,IEReceivedTS
  ,IEDeliveryTS
  ,SourceVersion
  ,TS
  ,ArchiveKey
  ,XmlData
 ) VALUES (
  :SequenceId 
  ,:ContentType
  ,:SubContentType
  ,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
  ,:SourceVersion
  ,CURRENT_TIMESTAMP
  ,( TRIM(:SequenceId)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
  || '_' || SUBSTRING(CAST(:IEReceivedTS AS VARCHAR(26)) FROM 1 FOR 10)
  || '_' || SUBSTRING(CAST(:IEReceivedTS AS VARCHAR(26)) FROM 12 FOR 15)  
  )
  ,clobreplerrc(TRANSLATE(:XmlData USING UNICODE_TO_LATIN WITH ERROR))
 );
 
INSERT INTO ${DB_ENV}StgSAPT.Stg_PMR_PromotionQ
(SequenceId,
ReferenceTS) 
VALUES (
:SequenceId
,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
);

END;
