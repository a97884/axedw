/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SAPSite_ReadQ.btq 5194 2012-08-28 07:19:13Z k9105777 $
# Last Changed By  : $Author: k9105777 $
# Last Change Date : $Date: 2012-08-28 09:19:13 +0200 (tis, 28 aug 2012) $
# Last Revision    : $Revision: 5194 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/procedure/SAPSite_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for Site Reading Queue
# Project	: EDW
# Subproject:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-08-27 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for Customer Reading Queue
# Dependencies
#	${DB_ENV}StgSAPT.Stg_SiteQ
#	${DB_ENV}UtilT.SAP_Site_Load_Cache
# --------------------------------------------------------------------------
*/
REPLACE PROCEDURE ${DB_ENV}StgSAPT.SAPSite_ReadQ(IN WorkflowRunId VARCHAR(255))
BEGIN
DECLARE QCount INT; -- To keep count of records present in queue table

INSERT INTO ${DB_ENV}UtilT.SAP_Site_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)
SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_SiteQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgSAPT.Stg_SiteQ; -- Get current row count of Queue table 

BT;

WHILE QCount > 0 -- Loop until queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.SAP_Site_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)
SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_SiteQ
;

SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END;
