/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: S2_SAP_Pricing_Item.btq 16771 2015-08-11 12:56:04Z K9110860 $
# Last Changed By  : $Author: K9110860 $
# Last Change Date : $Date: 2015-08-11 14:56:04 +0200 (tis, 11 aug 2015) $
# Last Revision    : $Revision: 16771 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/table/S2_SAP_Pricing_Item.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}StgSAPT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}StgSAPT','S2_SAP_Pricing_Item','PRE','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE SET TABLE ${DB_ENV}StgSAPT.S2_SAP_Pricing_Item
     (
      XPK_Item INTEGER NOT NULL,
      DocumentNumber VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC,
      VersionNumber VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC,
      SiteID VARCHAR(4) CHARACTER SET LATIN NOT CASESPECIFIC,
      ArticleID VARCHAR(18) CHARACTER SET LATIN NOT CASESPECIFIC,
      SalesOrg CHAR(4) CHARACTER SET LATIN NOT CASESPECIFIC,
      DistributionChannel CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      MerchandiseCategory VARCHAR(9) CHARACTER SET LATIN NOT CASESPECIFIC,
      SalesUnit CHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC,
      SalesPriceUnit INTEGER,
      PurchasingOrg CHAR(4) CHARACTER SET LATIN NOT CASESPECIFIC,
      VendorID VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC,
      VendorSubrange VARCHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      SalesCurrency VARCHAR(5) CHARACTER SET LATIN NOT CASESPECIFIC,
      BasicPurchasePrice DECIMAL(9,2),
      PurchasePriceNetNet DECIMAL(9,2),
      SalesPriceNet DECIMAL(9,2),
      FinalPrice DECIMAL(9,2),
      ReferenceSite CHAR(4) CHARACTER SET LATIN NOT CASESPECIFIC,
      SalesPriceConditionType CHAR(4) CHARACTER SET LATIN NOT CASESPECIFIC,
      ConditionTable CHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC,
      ValidFrom TIMESTAMP(6),
      ValidTo TIMESTAMP(6),
      AxFoodPriceListDescription VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      PriceList_Type CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      LegacyDescription VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      InksammalGroup VARCHAR(40) CHARACTER SET LATIN NOT CASESPECIFIC,
      InksammalSubGroup VARCHAR(40) CHARACTER SET LATIN NOT CASESPECIFIC,
      VolumeClass CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      LegacyAgreementID VARCHAR(8) CHARACTER SET LATIN NOT CASESPECIFIC,
      SourceLoadID INTEGER NOT NULL,
      SequenceId VARCHAR(40) CHARACTER SET LATIN NOT CASESPECIFIC,
      TransactionTS TIMESTAMP(6),
      ExtractionTS TIMESTAMP(6),
      IEReceivedTS TIMESTAMP(6),
      IEDeliveryTS TIMESTAMP(6),
      SourceVersion VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC,
      TS TIMESTAMP(6),
      ArchiveKey VARCHAR(256) CHARACTER SET LATIN NOT CASESPECIFIC)
UNIQUE PRIMARY INDEX ( XPK_Item );
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}StgSAPT','S2_SAP_Pricing_Item','POST','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
