/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: S1_SAP_SalesOrder_Item.btq 27599 2019-03-27 15:33:59Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-03-27 16:33:59 +0100 (ons, 27 mar 2019) $
# Last Revision    : $Revision: 27599 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/table/S1_SAP_SalesOrder_Item.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}StgSAPT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}StgSAPT','S1_SAP_SalesOrder_Item','PRE','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE ${DB_ENV}StgSAPT.S1_SAP_SalesOrder_Item
     (
      XPK_SAP_Salesorder BIGINT,
      FK_Sci_Onorder BIGINT,
      technicalorigin CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      orderid VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      orderrowid VARCHAR(7) CHARACTER SET LATIN NOT CASESPECIFIC,
      articlenumber INTEGER,
      reforderrowid VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC,
	  ExtOrderId  VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
	  ExtOrderRowId VARCHAR(7) CHARACTER SET LATIN NOT CASESPECIFIC,
	  CustomerId  VARCHAR(200) CHARACTER SET LATIN NOT CASESPECIFIC,
      orderdate TIMESTAMP(6),
	  Pickingdate TIMESTAMP(6),	  
      deliverdate TIMESTAMP(6),
	  onorder_errorcode INTEGER,	  
      shortagecode VARCHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      onorderstatus BYTEINT,
      baseuomcode VARCHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC,
      uomcode VARCHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC,
      orderarticlenumber INTEGER,
      orderquantity DECIMAL(18,4),
      orderquantitybaseuom DECIMAL(18,4),
      delivereduomcode VARCHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC,
      deliveredquantity DECIMAL(18,4),
      deliveredquantitybaseuom DECIMAL(18,4),
      receivedquantity DECIMAL(18,4),
      receivedquantitybaseuom DECIMAL(18,4),
	  confirmedquantity DECIMAL(18,4),
	  confirmedquantitybaseuom DECIMAL(18,4),	  
      partialdelivery VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      reftosubstituteorder VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC,
	  warehouse VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
	  transportationgroup VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
	  ordertype VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
	  longleadtime VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
	  GOODS_ISSUE_DATE DATE FORMAT 'YYYY-MM-DD' DEFAULT DATE '0001-01-01',
	  ARTICLE_SUPPLIER_STATUS_CD VARCHAR(40) CHARACTER SET LATIN NOT CASESPECIFIC,
	  UNLOADING_POINT VARCHAR(40) CHARACTER SET LATIN NOT CASESPECIFIC,
	  LOCATION_ZONE VARCHAR(40) CHARACTER SET LATIN NOT CASESPECIFIC,
	  ReasonForRejection VARCHAR(200) CHARACTER SET LATIN NOT CASESPECIFIC,
	  MatAvailDate DATE FORMAT 'YYYY-MM-DD' DEFAULT DATE '0001-01-01',
	  ReplacementRef VARCHAR(200) CHARACTER SET LATIN NOT CASESPECIFIC,
	  DirectOrder VARCHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC,
	  MatnrExpo INTEGER,
      TS TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
      ArchiveKey VARCHAR(256) CHARACTER SET LATIN NOT CASESPECIFIC
	  )
UNIQUE PRIMARY INDEX ( XPK_SAP_Salesorder );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}StgSAPT','S1_SAP_SalesOrder_Item','POST','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
