/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StgSoftone.btq 28254 2019-05-08 08:59:06Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-05-08 10:59:06 +0200 (ons, 08 maj 2019) $
# Last Revision    : $Revision: 28254 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSoftone/database/StgSoftone.btq $
# ------------------------------------------------------------------------------
# SVN Info END
# ------------------------------------------------------------------------------
*/


-- -----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgSoftone
-- -----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgSoftone';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgSoftone
FROM ${DB_ENV}Stg AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgSoftone AS
'Parent Database for source data from Timesystem SoftOne'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgSoftone TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgSoftoneT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgSoftoneT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgSoftoneT
FROM ${DB_ENV}StgSoftone AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgSoftoneT AS
'Database holding stagingtables from Salesforce Marketing Cloud'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgSoftoneT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgSoftoneVIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgSoftoneVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgSoftoneVIN
FROM ${DB_ENV}StgSoftone AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgSoftoneVIN AS
'Database holding views for loading stagingtables from Salesforce Marketing Cloud'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgSoftoneVIN TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}StgSoftoneT TO ${DB_ENV}StgSoftoneVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgSoftoneVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgSoftoneVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgSoftoneVOUT
FROM ${DB_ENV}StgSoftone AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgSoftoneVOUT AS
'Database holding views for reading stagingtables from Salesforce Marketing Cloud'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgSoftoneVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgSoftoneT TO ${DB_ENV}StgSoftoneVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT ON Sys_Calendar TO ${DB_ENV}StgSoftoneVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* ===============================================================================
These grants are required to support usage of dynamically (runtime) created views  
within Informatica PowerCenter (push-down optimization)
=============================================================================== */
-- Read Access on MetaData for stage dbs (update when adding new sources)
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}MetaDataVOUT TO ${DB_ENV}StgSoftoneT,${DB_ENV}StgSoftoneVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read & Delete Access on UtilT for stage dbs
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}UtilT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}UtilT TO ${DB_ENV}StgSoftoneT,${DB_ENV}StgSoftoneVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Execute functions in SYSLIB
SELECT * FROM DBC.Databases WHERE DatabaseName = 'SYSLIB';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT EXECUTE FUNCTION ON SYSLIB TO ${DB_ENV}StgSoftoneT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgSoftone to Cntl
SELECT * FROM DBC.Databases WHERE DatabaseName IN ( '${DB_ENV}CntlCleanseT', '${DB_ENV}CntlCleanseVIN');
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 2 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgSoftoneT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgSoftoneVOUT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT DROP TABLE ON ${DB_ENV}StgSoftoneT TO ${DB_ENV}CntlCleanseT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgSoftone to MetadataVIN
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgSoftoneT TO ${DB_ENV}MetaDataVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on Achv 
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}Achv';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}Achv TO ${DB_ENV}StgSoftoneT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT


-- Read Access on CntlCleanseT
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}CntlCleanseT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}CntlCleanseT TO ${DB_ENV}StgSoftoneT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT
