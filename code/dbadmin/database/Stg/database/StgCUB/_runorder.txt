BTQRUN	code/dbadmin/database/Stg/database/StgCUB/database/StgCUB.btq
------ Start pre-fix scripts (fixes not included in table-def scripts)
------ End of pre-fix scripts
------ Start of work-tables
------ End of work-tables
------ Start of view definitions (insert new lines between Start and End marker)
------ End of view definitions
------ Start of initialization (loading initial values)
------ End of initialization
------ Start of Table-deletions
------ End of Table-deletions
------ Start of Triggers
------ End of Triggers
------ Start of procedure definitions (insert new lines between Start and End marker)
------ End of procedure definitions
------ Start of Table-stats
------ End of Table-stats
------ Start post-fix scripts (fixes not included in table-def scripts)
------ End of post-fix scripts
------ Tables
--	STG	Tables
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/database/StgCUB.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/STG_CUB_FreightCarrier.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/STG_CUB_PickList.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/STG_CUB_PickList_Item.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/STG_CUB_PickList_Route.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/STG_CUB_PickList_Route_Line.btq
--	S2	Tables
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/S2_CUB_FreightCarrier.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/S2_CUB_PickList.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/S2_CUB_PickList_Item.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/S2_CUB_PickList_Route.btq
BTQRUN	code/dbadmin/database/Stg/database/StgCUB/table/S2_CUB_PickList_Route_Line.btq
------ Views
------ Procedures
------ BTQCOMPILE	code/dbadmin/database/Stg/database/StgOAS/procedure/OASStores_PurgeDataFrmStg.btq
------ Macros
------ Triggers
------ Remove	object	-	drop	procedure	-	do	not	compile
