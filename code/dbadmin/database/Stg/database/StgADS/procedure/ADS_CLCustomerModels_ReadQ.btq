/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ADS_CLCustomerModels_ReadQ.btq 25355 2018-08-20 10:54:42Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2018-08-20 12:54:42 +0200 (mån, 20 aug 2018) $
# Last Revision    : $Revision: 25355 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgADS/procedure/ADS_CLCustomerModels_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for CL Customer Event Reading Queue
# Project	: Data Mining
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-02-06 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for CL Customer Event Reading Queue
# Dependencies
#	${DB_ENV}StgADST.Stg_ADS_CL_Customer_Models
#	${DB_ENV}StgADST.Stg_ADS_CL_Customer_ModelsQ
#	${DB_ENV}UtilT.ADS_CLCustomerModel_Load_Cache
# --------------------------------------------------------------------------
*/


REPLACE PROCEDURE ${DB_ENV}StgADST.ADS_CLCustomerModels_ReadQ(IN WorkflowRunId VARCHAR(255))
BEGIN
DECLARE QCount INT; -- To keep count of records present in queue table

BT;
	  
INSERT INTO ${DB_ENV}UtilT.ADS_CLCustomerModel_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
TS
,Datum
,Calendar_Month_Id
,Member_Account_Seq_Num
,Loyalty_Program_Seq_Num
,InsertDttm
,WorkflowRunId 
)

SELECT 
TS
,Datum
,Calendar_Month_Id
,Member_Account_Seq_Num
,Loyalty_Program_Seq_Num
,InsertDttm
,:WorkflowRunId
FROM
 ${DB_ENV}StgADST.Stg_ADS_CL_Customer_ModelsQ
;DELETE FROM   ${DB_ENV}StgADST.Stg_ADS_CL_Customer_ModelsQ;

ET;
END;