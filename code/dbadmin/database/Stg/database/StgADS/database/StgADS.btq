/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StgADS.btq 17050 2015-08-27 12:01:15Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2015-08-27 14:01:15 +0200 (tor, 27 aug 2015) $
# Last Revision    : $Revision: 17050 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgADS/database/StgADS.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgADS
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgADS';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgADS
FROM ${DB_ENV}Stg AS
   PERM = 1073741824
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgADS AS
'Parent Database for source data from ADS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgADS TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgADST
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgADST';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgADST
FROM ${DB_ENV}StgADS AS
   PERM = 1073741824
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgADST AS
'Database holding stagingtables from ADS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgADST TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgADSVIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgADSVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgADSVIN
FROM ${DB_ENV}StgADS AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgADSVIN AS
'Database holding views for loading stagingtables from ADS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgADSVIN TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}StgADST TO ${DB_ENV}StgADSVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgADSVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgADSVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgADSVOUT
FROM ${DB_ENV}StgADS AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgADSVOUT AS
'Database holding views for reading stagingtables from ADS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgADSVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgADST TO ${DB_ENV}StgADSVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT ON Sys_Calendar TO ${DB_ENV}StgADSVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* ===============================================================================
These grants are required to support usage of dynamically (runtime) created views  
within Informatica PowerCenter (push-down optimization)
=============================================================================== */
-- Read Access on MetaData for stage dbs (update when adding new sources)
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}MetaDataVOUT TO ${DB_ENV}StgADST,${DB_ENV}StgADSVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read & Delete Access on UtilT for stage dbs
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}UtilT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}UtilT TO ${DB_ENV}StgADST,${DB_ENV}StgADSVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgADS to Cntl
SELECT * FROM DBC.Databases WHERE DatabaseName IN ( '${DB_ENV}CntlCleanseT', '${DB_ENV}CntlCleanseVIN');
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 2 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgADST TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgADSVOUT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT DROP TABLE ON ${DB_ENV}StgADST TO ${DB_ENV}CntlCleanseT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgADS to MetadataVIN
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgADST TO ${DB_ENV}MetaDataVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgADS to Achv
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}Achv';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgADSVOUT TO ${DB_ENV}Achv WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

