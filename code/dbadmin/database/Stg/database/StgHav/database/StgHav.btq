﻿/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StgHav.btq 1 2020-01-08 08:45:08Z a97884 $
# Last Changed By  : $Author: a97884 $
# Last Change Date : $Date: 2020-01-08 .. $
# Last Revision    : $Revision: 1 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgHav/database/StgHav.btq $
# --------------------------------------------------------------------------
# SVN   Info END.
# --------------------------------------------------------------------------
*/

-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgHav
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgHav';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgHav
FROM ${DB_ENV}Stg AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgHav AS
'Parent Database for Response data from Hav Traceability'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgHav TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgHavT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgHavT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgHavT
FROM ${DB_ENV}StgHav AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgHavT AS
'Database holding Response data from Hav Traceability'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgHavT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgHavVIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgHavVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgHavVIN
FROM ${DB_ENV}StgHav AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgHavVIN AS
'Database holding views for Response data from Hav Traceability'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgHavVIN TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}StgHavT TO ${DB_ENV}StgHavVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgHavVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgHavVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgHavVOUT
FROM ${DB_ENV}StgHav AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgHavVOUT AS
'Database holding views for reading Response data from Hav Traceability'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgHavVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgHavT TO ${DB_ENV}StgHavVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT ON Sys_Calendar TO ${DB_ENV}StgHavVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* ===============================================================================
These grants are required to support usage of dynamically (runtime) created views  
within Informatica PowerCenter (push-down optimization)
=============================================================================== */
-- Read Access on MetaData for stage dbs (update when adding new sources)
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}MetaDataVOUT TO ${DB_ENV}StgHavT,${DB_ENV}StgHavVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read & Delete Access on UtilT for stage dbs
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}UtilT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}UtilT TO ${DB_ENV}StgHavT,${DB_ENV}StgHavVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Execute functions in SYSLIB
SELECT * FROM DBC.Databases WHERE DatabaseName = 'SYSLIB';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT EXECUTE FUNCTION ON SYSLIB TO ${DB_ENV}StgHavT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgHav to Cntl
SELECT * FROM DBC.Databases WHERE DatabaseName IN ( '${DB_ENV}CntlCleanseT', '${DB_ENV}CntlCleanseVIN');
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 2 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgHavT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgHavVOUT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT DROP TABLE ON ${DB_ENV}StgHavT TO ${DB_ENV}CntlCleanseT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgHav to Metadata
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgHavT TO ${DB_ENV}MetaDataVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}MetadataVOUT TO ${DB_ENV}StgHavVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT
