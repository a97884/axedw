/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: AxBOInv_ReadQ.btq 13220 2014-06-09 08:50:50Z k9105806 $
# Last Changed By  : $Author: k9105806 $
# Last Change Date : $Date: 2014-06-09 10:50:50 +0200 (mån, 09 jun 2014) $
# Last Revision    : $Revision: 13220 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgAxBO/procedure/AxBOInv_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this procedure for insert in stg-table
# Project	: EDW1
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-02-06 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Sonic call this procedure for insert in stg-table
# Dependencies
#	${DB_ENV}StgAxBOT.Stg_AxBO_Inventory
#	${DB_ENV}StgAxBOT.Stg_AxBO_InventoryQ
#	${DB_ENV}UtilT.AxBO_InvEvTot_Load_Cache
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}StgAxBOT.AxBOInv_ReadQ(IN WorkflowRunId VARCHAR(255))

BEGIN

DECLARE QCount INT; -- To keep count of records present in queue table

INSERT INTO ${DB_ENV}UtilT.AxBO_InvEvTot_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
       TS
      ,SAPCustomerID
      ,SequenceId
      ,ReceiptSequenceNr
      ,ReferenceTS
      ,WorkflowRunId
)
SELECT AND CONSUME TOP 1
       CURRENT_TIMESTAMP
   ,SAPCustomerID
      ,SequenceId
      ,ReceiptSequenceNr
      ,ReferenceTS
     ,:WorkflowRunId
FROM
 ${DB_ENV}StgAxBOT.Stg_AxBO_InventoryQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgAxBOT.Stg_AxBO_InventoryQ; -- Get current row count of Queue table 

BT;

WHILE QCount > 0 -- Loop until queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.AxBO_InvEvTot_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
       TS
      ,SAPCustomerID
      ,SequenceId
      ,ReceiptSequenceNr
      ,ReferenceTS
      ,WorkflowRunId
)
SELECT AND CONSUME TOP 1
       CURRENT_TIMESTAMP
   ,SAPCustomerID
      ,SequenceId
      ,ReceiptSequenceNr
      ,ReferenceTS
     ,:WorkflowRunId
FROM
 ${DB_ENV}StgAxBOT.Stg_AxBO_InventoryQ
;

SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END;