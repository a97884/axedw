/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: AxBOPOS_ReadQ.btq 13218 2014-06-09 08:50:35Z k9105806 $
# Last Changed By  : $Author: k9105806 $
# Last Change Date : $Date: 2014-06-09 10:50:35 +0200 (mån, 09 jun 2014) $
# Last Revision    : $Revision: 13218 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgAxBO/procedure/AxBOPOS_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this procedure for insert in stg-table
# Project	: EDW1
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-02-06 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Sonic call this procedure for insert in stg-table
# Dependencies
#	${DB_ENV}StgAxBOT.Stg_AxBO_POS
#	${DB_ENV}StgAxBOT.Stg_AxBO_POSQ
#	${DB_ENV}UtilT.AxBO_POS_Load_Cache
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}StgAxBOT.AxBOPOS_ReadQ(IN WorkflowRunId VARCHAR(255))

BEGIN

DECLARE RecordCount INT; 					-- To keep frequency Limit
DECLARE Counter INT; 						-- To keep loop counter
DECLARE QCount INT; 						-- To keep count of records present in queue table

DECLARE QITS TIMESTAMP(6);					-- To keep QITS read from queue     
DECLARE SequenceId INT; 					-- To keep SequenceId read from queue
DECLARE SAPCustomerID INT; 					-- To keep SAPCustomerID read from queue
DECLARE ContentType VARCHAR(30); 			--  To keep ContentType read from queue
DECLARE SubContentType	VARCHAR(30);		--  To keep SubContentType read from queue	
DECLARE TransactionTS TIMESTAMP(6);			--  To keep TransactionTS read from queue	
DECLARE IEDeliveryTS TIMESTAMP(6);			--  To keep IEDeliveryTS read from queue	  

SET SubContentType = 'TransactionExportList';	-- Set SubContentType to TransactionExportList for looping
SET Counter = 1;								-- Set Counter to 1

SELECT RecordCount INTO :RecordCount FROM ${DB_ENV}StgAxBOT.Stg_AxBO_POS_Load_Frequency WHERE StatusId = 'POS'; -- Getting Frequency Limit

IF RecordCount = -1 THEN

BT;

INSERT INTO ${DB_ENV}UtilT.AxBO_POS_Load_Cache (  -- insert in cache table          
		 QITS
		,SequenceId    
		,SAPCustomerID 
		,ContentType
		,SubContentType		
		,TransactionTS 
		,IEDeliveryTS  
		,WorkflowRunId 
)
SELECT 
		 QITS
		,SequenceId    
		,SAPCustomerID  
		,ContentType
		,SubContentType			
		,TransactionTS 
		,IEDeliveryTS 
		,:WorkflowRunId
FROM 
		${DB_ENV}StgAxBOT.Stg_AxBO_POSQ
WHERE
		SubContentType <> 'TransactionExportList'
;DELETE FROM ${DB_ENV}StgAxBOT.Stg_AxBO_POSQ;

ET;

ELSE

BT;

-- Wait for a record to be inserted into queue table which isn't a TransactionExportList

WHILE SubContentType = 'TransactionExportList'
DO

	SELECT AND CONSUME TOP 1
			 QITS
			,SequenceId    
			,SAPCustomerID  
			,ContentType
			,SubContentType			
			,TransactionTS 
			,IEDeliveryTS  
	FROM
			${DB_ENV}StgAxBOT.Stg_AxBO_POSQ			
	INTO
			 :QITS
			,:SequenceId    
			,:SAPCustomerID  
			,:ContentType
			,:SubContentType			
			,:TransactionTS 
			,:IEDeliveryTS;
			
END WHILE;

-- Insert the transaction read from above looping into Load_Cache

INSERT INTO ${DB_ENV}UtilT.AxBO_POS_Load_Cache (  -- insert in cache table          
		 QITS
		,SequenceId    
		,SAPCustomerID 
		,ContentType
		,SubContentType		
		,TransactionTS 
		,IEDeliveryTS  
		,WorkflowRunId 
)
SELECT 
		 :QITS
		,:SequenceId    
		,:SAPCustomerID  
		,:ContentType
		,:SubContentType			
		,:TransactionTS 
		,:IEDeliveryTS 
		,:WorkflowRunId;
		
-- Now that a transaction other than TransactionExportList has arrived in queue, take count of records in queue	
LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgAxBOT.Stg_AxBO_POSQ; 		


WHILE Counter <= RecordCount AND QCount > 0 -- Loop until frequeny limit is reached or queue is emtpy
DO
		SELECT AND CONSUME TOP 1
				 QITS
				,SequenceId    
				,SAPCustomerID  
				,ContentType
				,SubContentType			
				,TransactionTS 
				,IEDeliveryTS  
		FROM
				${DB_ENV}StgAxBOT.Stg_AxBO_POSQ
		INTO
				 :QITS
				,:SequenceId    
				,:SAPCustomerID  
				,:ContentType
				,:SubContentType			
				,:TransactionTS 
				,:IEDeliveryTS;

		IF SubContentType <> 'TransactionExportList' THEN

			INSERT INTO ${DB_ENV}UtilT.AxBO_POS_Load_Cache (  -- insert in cache table          
					 QITS
					,SequenceId    
					,SAPCustomerID 
					,ContentType
					,SubContentType		
					,TransactionTS 
					,IEDeliveryTS  
					,WorkflowRunId 
			)
			SELECT 
					 :QITS
					,:SequenceId    
					,:SAPCustomerID  
					,:ContentType
					,:SubContentType			
					,:TransactionTS 
					,:IEDeliveryTS 
					,:WorkflowRunId;

			SET Counter = Counter + 1; -- Increment counter by 1
		
		END IF;		
		
SET QCount = QCount - 1; -- Get current row count of Queue table 
		
END WHILE;  -- end loop

ET;

END IF;

END;