/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_AxBO_POSChecksum.btq 12079 2014-01-10 16:30:56Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2014-01-10 17:30:56 +0100 (fre, 10 jan 2014) $
# Last Revision    : $Revision: 12079 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgAxBO/macro/Load_Stg_AxBO_POSChecksum.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this macro for INSERT IN stg-table
# Project	: EDW1
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-01-30 Teradata  Initial version
# 2014-01-10 M.Sabel   Add SAPCustomerID
# --------------------------------------------------------------------------
# Description
#	Sonic call this macro for INSERT IN stg-table for POS Checksum
# Dependencies
#	${DB_ENV}StgAxBOT.Stg_AxBO_POSChecksum
#	${DB_ENV}StgAxBOT.Stg_AxBO_POSChecksumQ
# --------------------------------------------------------------------------
*/
REPLACE MACRO ${DB_ENV}StgAxBOT.Load_Stg_AxBO_POSChecksum
(	SequenceId	INTEGER
	,SAPCustomerID	INTEGER
	,StoreId	INTEGER
	,ContentType	VARCHAR(30)
	,SubContentType	VARCHAR(30)
	,TransactionTS	CHAR(26)
	,ExtractionTS	CHAR(26)
	,IEReceivedTS	CHAR(26)
	,IEDeliveryTS	CHAR(26)
	,SourceVersion	VARCHAR(10)
	,AOFlag	VARCHAR(1)
	,XmlData	CLOB(5242880)
) AS (

	INSERT INTO ${DB_ENV}StgAxBOT.Stg_AxBO_POSChecksum
	(	SequenceId
		,SAPCustomerID
		,StoreId
		,ContentType
		,SubContentType
		,TransactionTS
		,ExtractionTS
		,IEReceivedTS
		,IEDeliveryTS
		,SourceVersion
		,AOFlag
		,TS
		,ArchiveKey
		,XmlData
	) VALUES (
		:SequenceId
		,:SAPCustomerID
		,:StoreId
		,:ContentType
		,:SubContentType
		,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
		,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
		,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
		,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
		,:SourceVersion
		,:AOFlag
		,CURRENT_TIMESTAMP
		,(	TRIM(:SequenceId)
		|| '_' || TRIM(:SAPCustomerID)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
		|| '_' || SUBSTRING(CAST(:IEDeliveryTS AS VARCHAR(26)) FROM 1 FOR 10)
		|| '_' || SUBSTRING(CAST(:IEDeliveryTS AS VARCHAR(26)) FROM 12 FOR 15)
		)
		,:XmlData
	);
);