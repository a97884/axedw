/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_AxBO_Inventory.btq 12746 2014-04-14 13:49:15Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2014-04-14 15:49:15 +0200 (mån, 14 apr 2014) $
# Last Revision    : $Revision: 12746 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgAxBO/macro/Load_Stg_AxBO_Inventory.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this macro for INSERT IN stg-table
# Project	: EDW
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author        Description
# 2014-01-10 M.Sabel       Initial version
# --------------------------------------------------------------------------
# Description
#	Sonic call this macro for INSERT IN stg-table
# Dependencies
#	${DB_ENV}StgAxBOT.Stg_AxBO_Inventory
#	${DB_ENV}StgAxBOT.Stg_AxBO_InventoryQ
# --------------------------------------------------------------------------
*/
REPLACE MACRO ${DB_ENV}StgAxBOT.Load_Stg_AxBO_Inventory
(SequenceId INTEGER
,SAPCustomerID INTEGER
,StoreId INTEGER
,ContentType VARCHAR(30)
,SubContentType VARCHAR(30)
,TransactionTS CHAR(26)
,ExtractionTS CHAR(26)
,IEReceivedTS CHAR(26)
,IEDeliveryTS CHAR(26)
,SourceVersion VARCHAR(10)
,AOFlag VARCHAR(1)
,XmlData CLOB(33554432)
) AS (

INSERT INTO ${DB_ENV}StgAxBOT.Stg_AxBO_Inventory
( SequenceId
 ,SAPCustomerID
 ,StoreId
 ,ContentType
 ,SubContentType
 ,TransactionTS
 ,ExtractionTS
 ,IEReceivedTS
 ,IEDeliveryTS
 ,SourceVersion
 ,AOFlag
 ,ArchiveKey
 ,XmlData
) VALUES (
 :SequenceId
 ,:SAPCustomerID 
 ,:StoreId
 ,:ContentType
 ,:SubContentType
 ,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
 ,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
 ,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
 ,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
 ,:SourceVersion
 ,:AOFlag
 ,( TRIM(:SequenceId)
  || '_' || TRIM(:SAPCustomerID)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
  || '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
  || '_' || SUBSTRING(CAST(:IEDeliveryTS AS VARCHAR(26)) FROM 1 FOR 10)
  || '_' || SUBSTRING(CAST(:IEDeliveryTS AS VARCHAR(26)) FROM 12 FOR 15)
  )
 ,:XmlData
 );
);
