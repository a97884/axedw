BTQRUN	code/dbadmin/database/Stg/database/StgACN/database/StgACN.btq
------ Start pre-fix scripts (fixes not included in table-def scripts)
------ End of pre-fix scripts
------ Start of work-tables
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_Characteristics_Item.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_Characteristics_Level_1.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_Characteristics_Level_2.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_Dim_Market.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_Dim_Period.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_Dim_Product.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_Fact.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S1_ACN_HEA.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_Characteristics_Item.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_Characteristics_Level_1.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_Characteristics_Level_2.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_Dim_Market.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_Dim_Period.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_Dim_Product.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_Fact.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_HEA.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/Stg_ACNQ.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_SESFF_FACT_DATA.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_SESFF_META_DATA.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_SESFF_MKT.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_SESFF_PER.btq
BTQRUN	code/dbadmin/database/Stg/database/StgACN/table/S2_ACN_SESFF_PROD.btq
------ End of work-tables
------ Start of view definitions (insert new lines between Start and End marker)
------ End of view definitions
------ Start of initialization (loading initial values)
------ End of initialization
------ Start of Table-deletions
------ End of Table-deletions
------ Start of Triggers
------ End of Triggers
------ Start of procedure definitions (insert new lines between Start and End marker)
BTQCOMPILE code/dbadmin/database/Stg/database/StgACN/procedure/ACN_PurgeDataFrmStg.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgACN/procedure/ACN_PurgeDataFrmStg_SFF.btq
BTQCOMPILE code/dbadmin/database/Stg/database/StgACN/procedure/ACN_ReadQ.btq
------ End of procedure definitions
------ Start of Table-stats
------ End of Table-stats
------ Start post-fix scripts (fixes not included in table-def scripts)
------ End of post-fix scripts



