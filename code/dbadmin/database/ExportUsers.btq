/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ExportUsers.btq 31543 2020-05-29 10:45:33Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-05-29 12:45:33 +0200 (fre, 29 maj 2020) $
# Last Revision    : $Revision: 31543 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/ExportUsers.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;


-- ============================================================================
-- Database: ${DB_ENV}ExportUsers
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE DATABASE ${DB_ENV}ExportUsers
FROM ${DB_ENV}dbadmin AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

GRANT ALL ON ${DB_ENV}ExportUsers
	TO ${DB_ENV}ExportUsers,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ============================================================================
-- Export user for DL Extracts
-- ============================================================================
-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_DL_Exp_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_DL_Exp_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_DL_Exp_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_DL_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_DL_Exp_00001 IS 'Export user for DL Extracts'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_DL_Exp_00001
	TO ${DB_ENV}_DL_Exp_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_DL_Exp_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_DL_Exp_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR,${DB_ENV}loadR TO ${DB_ENV}_DL_Exp_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ============================================================================
-- Common user for Extracts
-- ============================================================================
-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_Common_Exp_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_Common_Exp_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_Common_Exp_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_Common_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_Common_Exp_00001 IS 'Common user for Extracts'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_Common_Exp_00001
	TO ${DB_ENV}_Common_Exp_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_Common_Exp_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_Common_Exp_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR,${DB_ENV}loadR,${DB_ENV}loadStgR,${DB_ENV}loadADSR TO ${DB_ENV}_Common_Exp_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================
-- ============================================================================
-- Specific Batch/Export users for target system exports
-- ============================================================================
-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_Ext_Exp_SOI003_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_Ext_Exp_SOI003_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_Ext_Exp_SOI003_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_ext_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_Ext_Exp_SOI003_00001 IS 'Specific user for integration SOI003 (Daily Sales Direktmedia)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_Ext_Exp_SOI003_00001
	TO ${DB_ENV}_Ext_Exp_SOI003_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_Ext_Exp_SOI003_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_Ext_Exp_SOI003_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_Ext_Exp_SOI003_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_KKA_Exp_SOI008_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_KKA_Exp_SOI008_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_KKA_Exp_SOI008_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_kka_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_KKA_Exp_SOI008_00001 IS 'Specific user for integration SOI008 (KKA Daily Sales)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_KKA_Exp_SOI008_00001
	TO ${DB_ENV}_KKA_Exp_SOI008_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_KKA_Exp_SOI008_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_KKA_Exp_SOI008_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_KKA_Exp_SOI008_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_KKA_Exp_SOI009_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_KKA_Exp_SOI009_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_KKA_Exp_SOI009_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_kka_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_KKA_Exp_SOI009_00001 IS 'Specific user for integration SOI009 (KKA Masterdata)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_KKA_Exp_SOI009_00001
	TO ${DB_ENV}_KKA_Exp_SOI009_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_KKA_Exp_SOI009_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_KKA_Exp_SOI009_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_KKA_Exp_SOI009_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_OAS_Exp_SCI240_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OAS_Exp_SCI240_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_OAS_Exp_SCI240_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_oas_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_OAS_Exp_SCI240_00001 IS 'Specific user for integration SCI240 (OAS Daily Sales)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_OAS_Exp_SCI240_00001
	TO ${DB_ENV}_OAS_Exp_SCI240_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OAS_Exp_SCI240_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_OAS_Exp_SCI240_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_OAS_Exp_SCI240_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_OAS_Exp_SCI241_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OAS_Exp_SCI241_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_OAS_Exp_SCI241_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_oas_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_OAS_Exp_SCI241_00001 IS 'Specific user for integration SCI241 (OAS Historical Sales)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_OAS_Exp_SCI241_00001
	TO ${DB_ENV}_OAS_Exp_SCI241_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OAS_Exp_SCI241_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_OAS_Exp_SCI241_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_OAS_Exp_SCI241_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SAP_Exp_MDI045a_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SAP_Exp_MDI045a_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SAP_Exp_MDI045a_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_sap_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SAP_Exp_MDI045a_00001 IS 'Specific user for integration MDI045a (SAP DM Weekly Sales Priceoptimization)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SAP_Exp_MDI045a_00001
	TO ${DB_ENV}_SAP_Exp_MDI045a_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SAP_Exp_MDI045a_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SAP_Exp_MDI045a_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_SAP_Exp_MDI045a_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_AxCRM_Exp_MAI004_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_AxCRM_Exp_MAI004_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_AxCRM_Exp_MAI004_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_AxCRM_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_AxCRM_Exp_MAI004_00001 IS 'Specific user for integration MAI004 (Transactions to AxCRM)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_AxCRM_Exp_MAI004_00001
	TO ${DB_ENV}_AxCRM_Exp_MAI004_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_AxCRM_Exp_MAI004_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_AxCRM_Exp_MAI004_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_AxCRM_Exp_MAI004_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_Intactix_Exp_MDI132_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_Intactix_Exp_MDI132_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_Intactix_Exp_MDI132_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	PASSWORD = ${DB_ENV}_intactix_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_Intactix_Exp_MDI132_00001 IS 'Specific user for integration MDI132 (Intactix Historical Sales)'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_Intactix_Exp_MDI132_00001
	TO ${DB_ENV}_Intactix_Exp_MDI132_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_Intactix_Exp_MDI132_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_Intactix_Exp_MDI132_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_Intactix_Exp_MDI132_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_TVAS_Exp_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_TVAS_Exp_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_TVAS_Exp_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_tvas_exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_TVAS_Exp_00001 IS 'Specific user for integration from BI through cube in TVAS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_TVAS_Exp_00001
	TO ${DB_ENV}_TVAS_Exp_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_TVAS_Exp_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_TVAS_Exp_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_TVAS_Exp_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ${DB_ENV}TvasloadR TO ${DB_ENV}_TVAS_Exp_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ${DB_ENV}readSemR TO ${DB_ENV}_TVAS_Exp_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SPSS_Exp_MAI045_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI045_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SPSS_Exp_MAI045_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_SPSS_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SPSS_Exp_MAI045_00001 IS 'Specific user for integration from SPSS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SPSS_Exp_MAI045_00001
	TO ${DB_ENV}_SPSS_Exp_MAI045_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI045_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SPSS_Exp_MAI045_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}readSPSSR TO ${DB_ENV}_SPSS_Exp_MAI045_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SPSS_Exp_MAI046_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI046_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SPSS_Exp_MAI046_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_SPSS_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SPSS_Exp_MAI046_00001 IS 'Specific user for integration from SPSS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SPSS_Exp_MAI046_00001
	TO ${DB_ENV}_SPSS_Exp_MAI046_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI046_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SPSS_Exp_MAI046_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}readSPSSR TO ${DB_ENV}_SPSS_Exp_MAI046_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SPSS_Exp_MAI047_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI047_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SPSS_Exp_MAI047_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_SPSS_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SPSS_Exp_MAI047_00001 IS 'Specific user for integration from SPSS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SPSS_Exp_MAI047_00001
	TO ${DB_ENV}_SPSS_Exp_MAI047_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI047_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SPSS_Exp_MAI047_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}readSPSSR TO ${DB_ENV}_SPSS_Exp_MAI047_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SPSS_Exp_MAI048_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI048_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SPSS_Exp_MAI048_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_SPSS_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SPSS_Exp_MAI048_00001 IS 'Specific user for integration from SPSS'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SPSS_Exp_MAI048_00001
	TO ${DB_ENV}_SPSS_Exp_MAI048_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_MAI048_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SPSS_Exp_MAI048_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}readSPSSR TO ${DB_ENV}_SPSS_Exp_MAI048_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SPSS_Exp_BII114_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_BII114_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SPSS_Exp_BII114_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_OCB_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SPSS_Exp_BII114_00001 IS 'Specific user for integration from OCB'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SPSS_Exp_BII114_00001
	TO ${DB_ENV}_SPSS_Exp_BII114_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_BII114_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SPSS_Exp_BII114_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_SPSS_Exp_BII114_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SPSS_Exp_BII115_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_BII115_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SPSS_Exp_BII115_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_OCB_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SPSS_Exp_BII115_00001 IS 'Specific user for integration from OCB'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SPSS_Exp_BII115_00001
	TO ${DB_ENV}_SPSS_Exp_BII115_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SPSS_Exp_BII115_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SPSS_Exp_BII115_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_SPSS_Exp_BII115_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_OCB_Exp_BII116_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII116_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_OCB_Exp_BII116_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_OCB_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_OCB_Exp_BII116_00001 IS 'Specific user for integration from OCB'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_OCB_Exp_BII116_00001
	TO ${DB_ENV}_OCB_Exp_BII116_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII116_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_OCB_Exp_BII116_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_OCB_Exp_BII116_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_OCB_Exp_BII117_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII117_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_OCB_Exp_BII117_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_OCB_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_OCB_Exp_BII117_00001 IS 'Specific user for integration from OCB'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_OCB_Exp_BII117_00001
	TO ${DB_ENV}_OCB_Exp_BII117_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII117_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_OCB_Exp_BII117_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_OCB_Exp_BII117_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_PMR_Exp_MDI187_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_PMR_Exp_MDI187_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_PMR_Exp_MDI187_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_PMR_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_PMR_Exp_MDI187_00001 IS 'Specific user for integration from PMR'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_PMR_Exp_MDI187_00001
	TO ${DB_ENV}_PMR_Exp_MDI187_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_PMR_Exp_MDI187_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_PMR_Exp_MDI187_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_PMR_Exp_MDI187_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_OCC_Exp_BII326_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCC_Exp_BII326_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_OCC_Exp_BII326_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_OCC_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_OCC_Exp_BII326_00001 IS 'Specific user for integration from OCC'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_OCC_Exp_BII326_00001
	TO ${DB_ENV}_OCC_Exp_BII326_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCC_Exp_BII326_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_OCC_Exp_BII326_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_OCC_Exp_BII326_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_LP_Exp_MDI219_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_LP_Exp_MDI219_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_LP_Exp_MDI219_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_LP_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_LP_Exp_MDI219_00001 IS 'Specific user for integration from lev_portal'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_LP_Exp_MDI219_00001
	TO ${DB_ENV}_LP_Exp_MDI219_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_LP_Exp_MDI219_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_LP_Exp_MDI219_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_LP_Exp_MDI219_00001;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_OCB_Exp_BII336a_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII336a_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_OCB_Exp_BII336a_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_OCB_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_OCB_Exp_BII336a_00001 IS 'Specific user for integration from OCB for Alcohol Tax'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_OCB_Exp_BII336a_00001
	TO ${DB_ENV}_OCB_Exp_BII336a_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII336a_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_OCB_Exp_BII336a_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_OCB_Exp_BII336a_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_OCB_Exp_BII326b_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII326b_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_OCB_Exp_BII326b_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_OCB_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_OCB_Exp_BII326b_00001 IS 'Specific user for integration from OCB for Digital Receipts'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_OCB_Exp_BII326b_00001
	TO ${DB_ENV}_OCB_Exp_BII326b_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_OCB_Exp_BII326b_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_OCB_Exp_BII326b_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_OCB_Exp_BII326b_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ============================================================================

-- ----------------------------------------------------------------------------
-- User: ${DB_ENV}_SAP_Exp_BII342_00001
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SAP_Exp_BII342_00001';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE
CREATE USER ${DB_ENV}_SAP_Exp_BII342_00001
FROM ${DB_ENV}dbadmin
AS	PERM = 0
	SPOOL = 0
	TEMPORARY = 0
	PASSWORD = ${DB_ENV}_SAP_Exp
	PROFILE = ${DB_ENV}exportP
	DEFAULT ROLE = ALL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON USER ${DB_ENV}_SAP_Exp_BII342_00001 IS 'Specific user for integration to export data to SAP PMR'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}_SAP_Exp_BII342_00001
	TO ${DB_ENV}_SAP_Exp_BII342_00001,${DB_ENV}dbadmin,DBADMIN,DBC
	WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}_SAP_Exp_BII342_00001' AND OwnerName <> '${DB_ENV}ExportUsers';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE ${DB_ENV}_SAP_Exp_BII342_00001 TO ${DB_ENV}ExportUsers
;
.LABEL SKIPMV

GRANT ${DB_ENV}exportR TO ${DB_ENV}_SAP_Exp_BII342_00001;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

