/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: FINANCIAL_SEGM_HIER_CONC_REL.btq 21370 2017-02-07 08:54:20Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-02-07 09:54:20 +0100 (tis, 07 feb 2017) $
# Last Revision    : $Revision: 21370 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/FINANCIAL_SEGM_HIER_CONC_REL.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','FINANCIAL_SEGM_HIER_CONC_REL','PRE','O',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.FINANCIAL_SEGM_HIER_CONC_REL
(
	Financial_Plan_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Financial_Seg_Type_Cd	SMALLINT DEFAULT -1 NOT NULL,
	Valid_From_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT TIMESTAMP '0001-01-01 00:00:00.000000' NOT NULL,
	Node_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Concept_Cd	CHAR(3) NOT CASESPECIFIC DEFAULT '-1' CHARACTER SET LATIN NOT NULL,
	Corp_Affiliation_Type_Cd	CHAR(3) NOT CASESPECIFIC DEFAULT '-1' CHARACTER SET LATIN NOT NULL,
	Financial_Segment_Start_Dt	DATE FORMAT 'YYYY-MM-DD' DEFAULT DATE '0001-01-01' NOT NULL,
	Trait_Id	INTEGER DEFAULT -1 NOT NULL,
	Financial_Value	DECIMAL(18,6),
	Financial_Segment_End_Dt	DATE FORMAT 'YYYY-MM-DD' DEFAULT DATE '9999-12-31',
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKFINANCIAL_SEGM_HIER_CONC_REL PRIMARY KEY (Financial_Plan_Seq_Num,Financial_Seg_Type_Cd,Valid_From_Dttm,Node_Seq_Num,Concept_Cd,Corp_Affiliation_Type_Cd,Financial_Segment_Start_Dt,Trait_Id),
	CONSTRAINT FK1FINANCIAL_SEGM_HIER_CONC_RE FOREIGN KEY (Node_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ARTICLE_HIERARCHY_NODE (Node_Seq_Num),
	CONSTRAINT FK2FINANCIAL_SEGM_HIER_CONC_RE FOREIGN KEY (Concept_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.CONCEPT (Concept_Cd),
	CONSTRAINT FK3FINANCIAL_SEGM_HIER_CONC_RE FOREIGN KEY (Corp_Affiliation_Type_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.CORP_AFFILIATION_TYPE (Corp_Affiliation_Type_Cd),
	CONSTRAINT FK4FINANCIAL_SEGM_HIER_CONC_RE FOREIGN KEY (Financial_Plan_Seq_Num, Financial_Seg_Type_Cd, Valid_From_Dttm) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.FINANCIAL_SEGMENT (Financial_Plan_Seq_Num, Financial_Seg_Type_Cd, Valid_From_Dttm),
	CONSTRAINT FK5FINANCIAL_SEGM_HIER_CONC_RE FOREIGN KEY (Trait_Id) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.FINANCIAL_TRAIT (Trait_Id)
)
PRIMARY INDEX PIFINANCIAL_SEGM_HIER_CONC_REL(
	Financial_Plan_Seq_Num, Node_Seq_Num
);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','FINANCIAL_SEGM_HIER_CONC_REL','POST','O',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.FINANCIAL_SEGM_HIER_CONC_REL IS '$Revision: 21370 $ - $Date: 2017-02-07 09:54:20 +0100 (tis, 07 feb 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


