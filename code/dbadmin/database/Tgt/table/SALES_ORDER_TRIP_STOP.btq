/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_ORDER_TRIP_STOP.btq 31368 2020-05-06 06:19:46Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-05-06 08:19:46 +0200 (ons, 06 maj 2020) $
# Last Revision    : $Revision: 31368 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/SALES_ORDER_TRIP_STOP.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','SALES_ORDER_TRIP_STOP','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.SALES_ORDER_TRIP_STOP
(
	Route_Id	INTEGER DEFAULT -1 NOT NULL,
	Trip_Stop_Sequence_Num	INTEGER DEFAULT -1 NOT NULL,
	Sales_Order_Seq_Num	INTEGER NOT NULL,
	Store_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Order_Num	VARCHAR(20),
	Trip_Delivery_Status_Cd	VARCHAR(10) DEFAULT '-1',
	Address_Seq_Num	INTEGER DEFAULT -1,
	Party_Type_Cd	CHAR(3) DEFAULT '-1',
	Loyalty_Identifier_Seq_Num	INTEGER DEFAULT -1,
	Source_System_Customer_Id	VARCHAR(20) DEFAULT '-1',
	Number_Of_Packages	INTEGER,
	Number_Of_Bags	INTEGER,
	Freight_Carrier_Count	INTEGER,
	Scheduled_Expres_Delivery_Ind	BYTEINT DEFAULT 0,
	Start_Tm	TIME(0),
	End_Tm	TIME(0),
	Commit_Delivery_Dttm	TIMESTAMP FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)',
	Picking_Order_Reference_Num	INTEGER,
	Scheduled_Delivery_Dttm	TIMESTAMP FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)',
	Expres_Delivery_Ind	BYTEINT DEFAULT 0,
	Actual_Delivery_Dttm	TIMESTAMP FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)',
	Customer_Sign_Off_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)',
	Distans_Meas	DECIMAL(18,4) COMPRESS (0.0000),
	Time_Since_Last_Stop	INTEGER,
	Service_Time_In_Sec	INTEGER,
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKSALES_ORDER_TRIP_STOP PRIMARY KEY (Route_Id,Trip_Stop_Sequence_Num,Sales_Order_Seq_Num,Store_Seq_Num),
	CONSTRAINT FK12SALES_ORDER_TRIP_STOP FOREIGN KEY (Trip_Delivery_Status_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.TRIP_DELIVERY_STATUS (Trip_Delivery_Status_Cd),
	CONSTRAINT FK1SALES_ORDER_TRIP_STOP FOREIGN KEY (Address_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ADDRESS (Address_Seq_Num),
	CONSTRAINT FK5SALES_ORDER_TRIP_STOP FOREIGN KEY (Party_Type_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.PARTY_TYPE (Party_Type_Cd),
	CONSTRAINT FK11SALES_ORDER_TRIP_STOP FOREIGN KEY (Route_Id) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.SALES_ORDER_SHIPPING_ROUTE (Route_Id),
	CONSTRAINT FK13SALES_ORDER_TRIP_STOP FOREIGN KEY (Store_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.STORE (Store_Seq_Num)
);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','SALES_ORDER_TRIP_STOP','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT','SALES_ORDER_TRIP_STOP','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.SALES_ORDER_TRIP_STOP IS '$Revision: 31368 $ - $Date: 2020-05-06 08:19:46 +0200 (ons, 06 maj 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

