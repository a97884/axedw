/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_ACCRUAL_ITEM.btq 33002 2020-07-06 13:44:19Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2020-07-06 15:44:19 +0200 (mån, 06 jul 2020) $
# Last Revision    : $Revision: 33002 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/LOYALTY_ACCRUAL_ITEM.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','LOYALTY_ACCRUAL_ITEM','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.LOYALTY_ACCRUAL_ITEM
(
	Loyalty_Accrual_Id	VARCHAR(20) DEFAULT '-1' NOT NULL,
	Loyalty_Trans_Id	VARCHAR(20) NOT NULL,
	Loyalty_Attribute_Seq_Num	INTEGER,
	Loyalty_Promotion_Seq_Num	INTEGER,
	Accrual_Status_Cd	VARCHAR(255),
	Accrual_Type_Cd	VARCHAR(255),
	Loyalty_Accrual_Item_Num	VARCHAR(255),
	Expiration_Dt	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)',
	Process_Dt	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)',
	Accrual_Qty DECIMAL(18,4),
	Accrual_Amt	DECIMAL(18,4),
	Claimed_Amt	DECIMAL(18,4),
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	CONSTRAINT PKLOYALTY_ACCRUAL_ITEM PRIMARY KEY (Loyalty_Accrual_Id),
	CONSTRAINT FK1LOYALTY_ACCRUAL_ITEM FOREIGN KEY (Loyalty_Promotion_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOYALTY_PROMOTION (Loyalty_Promotion_Seq_Num),
	CONSTRAINT FK3LOYALTY_ACCRUAL_ITEM FOREIGN KEY (Accrual_Type_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ACCRUAL_ITEM_TYPE (Accrual_Type_Cd),
	CONSTRAINT FK4LOYALTY_ACCRUAL_ITEM FOREIGN KEY (Accrual_Status_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ACCRUAL_STATUS_CODE (Accrual_Status_Cd),
	CONSTRAINT FK5LOYALTY_ACCRUAL_ITEM FOREIGN KEY (Loyalty_Attribute_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOYALTY_ATTRIBUTE (Loyalty_Attribute_Seq_Num),
	CONSTRAINT FK6LOYALTY_ACCRUAL_ITEM FOREIGN KEY (Loyalty_Trans_Id) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOYALTY_TRANSACTION (Loyalty_Trans_Id)
);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','LOYALTY_ACCRUAL_ITEM','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.LOYALTY_ACCRUAL_ITEM IS '$Revision: 33002 $ - $Date: 2020-07-06 15:44:19 +0200 (mån, 06 jul 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


