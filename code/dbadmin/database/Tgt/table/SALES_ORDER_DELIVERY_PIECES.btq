/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_ORDER_DELIVERY_PIECES.btq 30660 2020-03-06 12:30:57Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $
# Last Revision    : $Revision: 30660 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/SALES_ORDER_DELIVERY_PIECES.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','SALES_ORDER_DELIVERY_PIECES','PRE',NULL,1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.SALES_ORDER_DELIVERY_PIECES
(

	Sales_Order_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Delivery_Piece_Id	INTEGER  NOT NULL ,
	Store_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Order_Dt_DD	DATE  FORMAT 'YYYY-MM-DD' NOT NULL ,
	Piece_Amt	DECIMAL(18,4)   COMPRESS (0.0000),
	OpenJobRunId	INTEGER  ,
	CloseJobRunId	INTEGER  ,
	InsertDttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	CONSTRAINT PKSALES_ORDER_DELIVERY_PIECES PRIMARY KEY (Sales_Order_Seq_Num,Delivery_Piece_Id,Store_Seq_Num)
)
	PRIMARY INDEX PPISALES_ORDER_DELIVERY_PIECE
	 (
			Sales_Order_Seq_Num
	 ) PARTITION BY (RANGE_N(Order_Dt_DD  BETWEEN 
DATE '2015-01-01' AND (((ADD_MONTHS((DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(2 ))))( TITLE 'Day')( TITLE 'Day')))- (((((ADD_MONTHS((DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(2 ))))( TITLE 'Day')( TITLE 'Day'))(DATE))- (10106 (DATE))) MOD  7 )+ 1 ( TITLE 'DayOfWeek()')))+ 1  EACH INTERVAL '7' DAY ,
 NO RANGE OR UNKNOWN),RANGE_N(Store_Seq_Num  BETWEEN 1  AND 1000  EACH 1 ,
 NO RANGE OR UNKNOWN)) ;

COMMENT ON TABLE SALES_ORDER_DELIVERY_PIECES IS 'An Sales Order will evovle into the state as a Picking or Packing Slip, which is defined as the order which contains the items to be packed/picked as the fulfillment of the customer order. This is the list of items and the expected quantity as well as the';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.Delivery_Piece_Id IS 'A unique identifier of a specific delivery piece';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.Piece_Amt IS 'The amount of specific delivery pieces used ';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.Sales_Order_Seq_Num IS 'Surrogate key for SALES_ORDER_DELIVERY_PIECES';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.OpenJobRunId IS 'Reference to the (load) process that created the row. This defines "Transaction Start Dttm".';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.CloseJobRunId IS 'Reference to the (load) process that logically deleted the row. This defines "Transaction End Dttm".';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.InsertDttm IS 'Reference to the (load) process that created the row. This defines "Transaction Start Dttm".';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.Order_Dt_DD IS 'Date of Sales Order Placement';

COMMENT ON COLUMN SALES_ORDER_DELIVERY_PIECES.Store_Seq_Num IS 'Surrogate key for SALES_ORDER_DELIVERY_PIECES';

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','SALES_ORDER_DELIVERY_PIECES','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT','SALES_ORDER_DELIVERY_PIECES','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.SALES_ORDER_DELIVERY_PIECES IS '$Revision: 30660 $ - $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


