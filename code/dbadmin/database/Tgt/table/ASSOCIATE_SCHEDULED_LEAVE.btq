/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ASSOCIATE_SCHEDULED_LEAVE.btq 30660 2020-03-06 12:30:57Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $
# Last Revision    : $Revision: 30660 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/ASSOCIATE_SCHEDULED_LEAVE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
--ASSOCIATE_SCHEDULED_LEAVE
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ASSOCIATE_SCHEDULED_LEAVE','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.ASSOCIATE_SCHEDULED_LEAVE
(
	Associate_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Location_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Scheduled_Leave_Start_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT TIMESTAMP '0001-01-01 00:00:00.000000' NOT NULL,
	Scheduled_Leave_End_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT TIMESTAMP '9999-12-31 23:59:59.999999' NOT NULL,
	Scheduled_Leave_Dt_DD	DATE FORMAT 'YYYY-MM-DD' NOT NULL,
	Leave_Type_Cd	CHAR(3) DEFAULT '-1',
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKASSOCIATE_SCHEDULED_LEAVE PRIMARY KEY (Associate_Seq_Num,Location_Seq_Num,Scheduled_Leave_Start_Dttm),
	CONSTRAINT FK1ASSOCIATE_SCHEDULED_LEAVE FOREIGN KEY (Leave_Type_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LEAVE_TYPE (Leave_Type_Cd),
	CONSTRAINT FK3ASSOCIATE_SCHEDULED_LEAVE FOREIGN KEY (Location_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOCATION (Location_Seq_Num),
	CONSTRAINT FK4ASSOCIATE_SCHEDULED_LEAVE FOREIGN KEY (Associate_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ASSOCIATE (Associate_Seq_Num)
)
PRIMARY INDEX PPIASSOCIATE_SCHEDULED_LEAVE
	 (
			Associate_Seq_Num,
			Location_Seq_Num
	 )  PARTITION BY (RANGE_N ( Scheduled_Leave_Dt_DD BETWEEN
    DATE '2014-12-01' AND (ADD_MONTHS((CURRENT_DATE ),(7 )))- (EXTRACT(DAY FROM (ADD_MONTHS((CURRENT_DATE ),(3 ))))( TITLE 'DAY')( TITLE 'DAY')) EACH INTERVAL '7' DAY,
    NO RANGE OR UNKNOWN )
                , RANGE_N ( Location_Seq_Num BETWEEN
                        1 AND 1000 EACH 1,
                        NO RANGE OR UNKNOWN
                )) ;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ASSOCIATE_SCHEDULED_LEAVE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.ASSOCIATE_SCHEDULED_LEAVE IS '$Revision: 30660 $ - $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
