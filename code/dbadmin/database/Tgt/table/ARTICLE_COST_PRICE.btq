/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ARTICLE_COST_PRICE.btq 16696 2015-06-18 12:48:20Z K9102939 $
# Last Changed By  : $Author: K9102939 $
# Last Change Date : $Date: 2015-06-18 14:48:20 +0200 (tor, 18 jun 2015) $
# Last Revision    : $Revision: 16696 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/ARTICLE_COST_PRICE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ARTICLE_COST_PRICE','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.ARTICLE_COST_PRICE
(
	Org_Party_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Distribution_Channel_Cd	CHAR(2) DEFAULT '-1' NOT NULL,
	Price_List_Cd	CHAR(2) DEFAULT '-1' NOT NULL,
	Article_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Valid_From_Dt	DATE FORMAT 'YYYY-MM-DD' NOT NULL,
	Cost_Price_Doc_Version_Num	INTEGER NOT NULL,
	Cost_Price_Doc_Num	INTEGER DEFAULT -1 NOT NULL,
	Purch_Org_Party_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Vendor_Party_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Vendor_Subrange_Cd	CHAR(6) DEFAULT '-1' NOT NULL COMPRESS ('ADMIN', 'FRYDIR', 'FRYEMV', 'FRYST', 'FRYST1', 'FRYST2', 'FRYST3', 'FRYST4', 'KYLDIR', 'KYLEMV', 'KYLT', 'KYLT1', 'KYLT2', 'KYLT3', 'KYLT4', 'TORDIR', 'TOREDR', 'TOREMV', 'TORRT', 'TORRT1', 'TORRT2', 'TORRT3', 'TORRT4', 'TORRT5', 'TORRT6', 'ADMN', 'FR01', 'FR02', 'FR03', 'FR04', 'KY01', 'KY02', 'KY03', 'KY04', 'KY05', 'KY06', 'TR01', 'TR02', 'TR03', 'TR04', 'TR05', 'TR06', 'TR07', 'TR08', 'TR09', 'TR10', 'TR11', 'TR12'),
	Cost_Price_UOM_Cd	CHAR(4) DEFAULT '-1' NOT NULL,
	Currency_Cd	CHAR(3) DEFAULT '-1' NOT NULL,
	Gross_Price_Amt	DECIMAL(9,2),
	Base_Price_Amt	DECIMAL(9,2),
	Final_Cost_Price_Amt	DECIMAL(9,2),
	Valid_To_Dt	DATE FORMAT 'YYYY-MM-DD',
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKARTICLE_COST_PRICE PRIMARY KEY (Org_Party_Seq_Num,Distribution_Channel_Cd,Price_List_Cd,Article_Seq_Num,Valid_From_Dt,Cost_Price_Doc_Version_Num),
	CONSTRAINT FK1ARTICLE_COST_PRICE FOREIGN KEY (Vendor_Party_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.PARTY (Party_Seq_Num),
	CONSTRAINT FK3ARTICLE_COST_PRICE FOREIGN KEY (Vendor_Subrange_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.VENDOR_SUBRANGE (Vendor_Subrange_Cd),
	CONSTRAINT FK5ARTICLE_COST_PRICE FOREIGN KEY (Currency_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.CURRENCY (Currency_Cd),
	CONSTRAINT FK8ARTICLE_COST_PRICE FOREIGN KEY (Distribution_Channel_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.CHANNEL (Channel_Cd),
	CONSTRAINT FK9ARTICLE_COST_PRICE FOREIGN KEY (Article_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ARTICLE (Article_Seq_Num),
	CONSTRAINT FK10ARTICLE_COST_PRICE FOREIGN KEY (Price_List_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.PRICE_LIST (Price_List_Cd),
	CONSTRAINT FK12ARTICLE_COST_PRICE FOREIGN KEY (Cost_Price_UOM_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.UOM (UOM_Cd),
	CONSTRAINT FK13ARTICLE_COST_PRICE FOREIGN KEY (Org_Party_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ORGANIZATION (Org_Party_Seq_Num),
	CONSTRAINT FK14ARTICLE_COST_PRICE FOREIGN KEY (Purch_Org_Party_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ORGANIZATION (Org_Party_Seq_Num)
)
PRIMARY INDEX PPIARTICLE_COST_PRICE(
	Article_Seq_Num
) PARTITION BY (CASE_N(
	Price_List_Cd = 'AA',
	Price_List_Cd = 'AB',
	Price_List_Cd = 'AC',
	Price_List_Cd = 'AD',
	Price_List_Cd = 'AF',
	Price_List_Cd = 'AG',
	Price_List_Cd = 'AI',
	Price_List_Cd = 'AK',
	Price_List_Cd = 'AL',
	Price_List_Cd = 'AM',
	Price_List_Cd = 'AN',
	Price_List_Cd = 'AO',
	Price_List_Cd = 'AP',
	Price_List_Cd = 'AT',
	Price_List_Cd = 'MA',
	Price_List_Cd = 'MB',
	Price_List_Cd = 'MC',
	Price_List_Cd = 'ME',
	Price_List_Cd = 'MF',
	Price_List_Cd = 'MG',
	Price_List_Cd = 'MH',
	Price_List_Cd = 'MI',
	Price_List_Cd = 'ML',
	Price_List_Cd = 'MM',
	Price_List_Cd = 'MN',
	Price_List_Cd = 'MO',
	Price_List_Cd = 'MQ',
	Price_List_Cd = 'MT',
	Price_List_Cd = 'MX',
	Price_List_Cd = 'MZ',
	Price_List_Cd = 'NA',
	Price_List_Cd = 'NC',
	Price_List_Cd = 'ND',
	Price_List_Cd = 'NE',
	Price_List_Cd = 'NF',
	Price_List_Cd = 'NG',
	Price_List_Cd = 'NH',
	Price_List_Cd = 'NI',
	Price_List_Cd = 'NJ',
	Price_List_Cd = 'NK',
	Price_List_Cd = 'PB',
	Price_List_Cd = 'PG',
	Price_List_Cd = 'PH',
	Price_List_Cd = 'PI',
	Price_List_Cd = 'PK',
	Price_List_Cd = 'PL',
	Price_List_Cd = 'PO',
	Price_List_Cd = 'PQ',
	Price_List_Cd = 'PU',
	Price_List_Cd = 'QA',
	Price_List_Cd = 'QH',
	Price_List_Cd = 'QI',
	Price_List_Cd = 'QJ',
	Price_List_Cd = 'QK',
	Price_List_Cd = 'QL',
	NO CASE,
	UNKNOWN
));

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ARTICLE_COST_PRICE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.ARTICLE_COST_PRICE IS '$Revision: 16696 $ - $Date: 2015-06-18 14:48:20 +0200 (tor, 18 jun 2015) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


