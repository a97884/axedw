/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ACTUAL_RETAIL_PRICE.btq 30710 2020-03-11 19:29:17Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2020-03-11 20:29:17 +0100 (ons, 11 mar 2020) $
# Last Revision    : $Revision: 30710 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/ACTUAL_RETAIL_PRICE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ACTUAL_RETAIL_PRICE','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.ACTUAL_RETAIL_PRICE
(
	Location_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Article_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Valid_From_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' NOT NULL,
	Retail_Price_List_Cd	CHAR(2) DEFAULT '-1' NOT NULL,
	Retail_Pricing_Lvl_Name	VARCHAR(100),
	Applied_Cost_Price_Amt	DECIMAL(9,2),
	Net_Retail_Price_Amt	DECIMAL(9,2),
	Final_Retail_Price_Amt	DECIMAL(9,2),
	Currency_Cd	CHAR(3) DEFAULT '-1' NOT NULL,
	Valid_To_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)',
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKACTUAL_RETAIL_PRICE PRIMARY KEY (Location_Seq_Num,Article_Seq_Num,Valid_From_Dttm)
)
PRIMARY INDEX PIACTUAL_RETAIL_PRICE(
	Article_Seq_Num
) PARTITION BY (RANGE_N ( Location_Seq_Num BETWEEN
                        1 AND 2000 EACH 1,
                        NO RANGE OR UNKNOWN
                ));

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ACTUAL_RETAIL_PRICE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.ACTUAL_RETAIL_PRICE IS '$Revision: 30710 $ - $Date: 2020-03-11 20:29:17 +0100 (ons, 11 mar 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT', 'ACTUAL_RETAIL_PRICE', '${DB_ENV}CntlLoadReadyT',null,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
