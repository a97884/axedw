/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: INV_REQ_STATUS_LINE_HIST.btq 23042 2017-09-08 12:41:14Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-09-08 14:41:14 +0200 (fre, 08 sep 2017) $
# Last Revision    : $Revision: 23042 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/INV_REQ_STATUS_LINE_HIST.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT
;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','INV_REQ_STATUS_LINE_HIST','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.INV_REQ_STATUS_LINE_HIST
(

	Order_Technical_Origin_Cd	CHAR(2)  DEFAULT '-1' NOT NULL ,
	Inventory_Req_Order_Id	VARCHAR(20)  NOT NULL ,
	Inventory_Req_Line_Num	INTEGER  NOT NULL ,
	Ref_Order_Row_Id	VARCHAR(30)  DEFAULT '-1' NOT NULL ,
	Transaction_Dttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' NOT NULL ,
	Order_Dt_DD	DATE  FORMAT 'YYYY-MM-DD' ,
	Supply_Location_Seq_Num	INTEGER  DEFAULT -1 ,
	Req_Location_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Req_Party_Seq_Num	INTEGER  DEFAULT -1 ,
	Article_Seq_Num	INTEGER  WITH DEFAULT NOT NULL ,
	Measuring_Unit_Seq_Num	INTEGER  DEFAULT -1 ,
	Order_Dttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' ,
	Pick_Dttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' ,
	Delivery_Dttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' ,
	Order_Type_Cd	CHAR(4)  DEFAULT '-1' NOT NULL ,
	Inventory_Request_Status_Cd	CHAR(2)  DEFAULT '-1' NOT NULL ,
	Exception_Cd	VARCHAR(50)  DEFAULT '-1' NOT NULL ,
	Partial_Delivery_Ind	BYTEINT  DEFAULT 0 ,
	Ordered_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	Ordered_Qty_in_Base_Units	DECIMAL(18,4)   COMPRESS (0.0000),
	Confirmed_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	Confirmed_Qty_in_Base_Units	DECIMAL(18,4)   COMPRESS (0.0000),	
	Delivered_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	Delivered_Qty_in_Base_Units	DECIMAL(18,4)   COMPRESS (0.0000),
	Received_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	Received_Qty_in_Base_Units	DECIMAL(18,4)   COMPRESS (0.0000),
	Delivery_Dt_DD	DATE  FORMAT 'YYYY-MM-DD' ,
	Transportation_Group_Cd	VARCHAR(50)  DEFAULT '-1' ,
	OpenJobRunId	INTEGER  ,
	CloseJobRunId	INTEGER  ,
	InsertDttm	TIMESTAMP(6)  DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	CONSTRAINT PKINV_REQ_STATUS_LINE_HIST PRIMARY KEY (Order_Technical_Origin_Cd,Inventory_Req_Order_Id,Inventory_Req_Line_Num,Ref_Order_Row_Id,Transaction_Dttm),
	 CONSTRAINT FK1INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Order_Technical_Origin_Cd, Inventory_Req_Order_Id, Inventory_Req_Line_Num, Ref_Order_Row_Id) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.INV_REQ_STATUS_LINE (Order_Technical_Origin_Cd, Inventory_Req_Order_Id, Inventory_Req_Line_Num, Ref_Order_Row_Id),
	 CONSTRAINT FK2INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Inventory_Request_Status_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.INVENTORY_REQUEST_STATUS (Inventory_Request_Status_Cd),
	 CONSTRAINT FK3INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Exception_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.EXCEPTION (Exception_Cd),
	 CONSTRAINT FK4INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Req_Location_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOCATION (Location_Seq_Num),
	 CONSTRAINT FK5INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Article_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ARTICLE (Article_Seq_Num),
	 CONSTRAINT FK6INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Order_Technical_Origin_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ORDER_TECHNICAL_ORIGIN (Order_Technical_Origin_Cd),
	 CONSTRAINT FK7INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Transportation_Group_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.TRANSPORTATION_GROUP (Transportation_Group_Cd),
	 CONSTRAINT FK8INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Supply_Location_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOCATION (Location_Seq_Num),
	 CONSTRAINT FK9INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Req_Party_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.PARTY (Party_Seq_Num),
	 CONSTRAINT FK10INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Measuring_Unit_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.MEASURING_UNIT (Measuring_Unit_Seq_Num),
	 CONSTRAINT FK268INV_REQ_STATUS_LINE_HIST  FOREIGN KEY (Order_Type_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ORDER_TYPE (Order_Type_Cd)
)
	PRIMARY INDEX PPIINV_REQ_STATUS_LINE_HIST
	 (
			Inventory_Req_Order_Id,
			Inventory_Req_Line_Num
	 ) PARTITION BY (RANGE_N(Order_Dt_DD  BETWEEN 
((ADD_MONTHS((DATE ),(-24 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(-24 ))))))- ((((ADD_MONTHS((DATE ),(-24 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(-24 ))))))- DATE '0001-01-01') MOD  7 ) 
AND (((ADD_MONTHS((DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(2 ))))))+ 6 )- ((((ADD_MONTHS((DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((DATE ),(2 ))))))- DATE '0001-01-01') MOD  7 ) EACH INTERVAL '7' DAY ,
 NO RANGE OR UNKNOWN),RANGE_N(Req_Location_Seq_Num  BETWEEN 1  AND 1000  EACH 1 ,
 NO RANGE OR UNKNOWN)) ;


.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','INV_REQ_STATUS_LINE_HIST','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.INV_REQ_STATUS_LINE_HIST IS '$Revision: 23042 $ - $Date: 2017-09-08 14:41:14 +0200 (fre, 08 sep 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT', 'INV_REQ_STATUS_LINE_HIST', '${DB_ENV}CntlLoadReadyT',null,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


