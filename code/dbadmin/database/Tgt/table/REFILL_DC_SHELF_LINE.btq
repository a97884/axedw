/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: REFILL_DC_SHELF_LINE.btq 29277 2019-10-14 10:30:06Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-10-14 12:30:06 +0200 (mån, 14 okt 2019) $
# Last Revision    : $Revision: 29277 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/REFILL_DC_SHELF_LINE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','REFILL_DC_SHELF_LINE','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.REFILL_DC_SHELF_LINE
(

	Refill_Order_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Refill_Order_Line	INTEGER  NOT NULL ,
	Location_Zone_Seq_Num	INTEGER  DEFAULT -1 ,
	Refill_By_User_Id	VARCHAR(50)  ,
	Placing_Id	VARCHAR(40)  DEFAULT '-1' NOT NULL ,
	Associate_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Refill_Location_Type_Cd	VARCHAR(10)  DEFAULT '-1' NOT NULL ,
	Article_Seq_Num	INTEGER  DEFAULT -1 ,
	Refill_Qty	INTEGER  ,
	Refill_Dt	DATE  FORMAT 'YYYY-MM-DD' ,
	Refill_Start_Dttm	TIMESTAMP  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' ,
	Refill_End_Dttm	TIMESTAMP  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' ,
	Refill_Time	DECIMAL(18,4)  DEFAULT -1  COMPRESS (0.0000),
	Identification_Type_Cd	VARCHAR(30)  DEFAULT '-1' ,
	Weight	DECIMAL(18,4)   COMPRESS (0.0000),
	OpenJobRunId	INTEGER  ,
	CloseJobRunId	INTEGER  ,
	InsertDttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	CONSTRAINT PKREFILL_DC_SHELF_LINE PRIMARY KEY (Refill_Order_Seq_Num,Refill_Order_Line),
	 CONSTRAINT FK2REFILL_DC_SHELF_LINE  FOREIGN KEY (Article_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ARTICLE (Article_Seq_Num),
	 CONSTRAINT FK3REFILL_DC_SHELF_LINE  FOREIGN KEY (Refill_Order_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.REFILL_DC_SHELF (Refill_Order_Seq_Num),
	 CONSTRAINT FK4REFILL_DC_SHELF_LINE  FOREIGN KEY (Identification_Type_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.REFILL_IDENTIFICATION_TYPE (Identification_Type_Cd),
	 CONSTRAINT FK5REFILL_DC_SHELF_LINE  FOREIGN KEY (Refill_Location_Type_Cd) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.REFILL_LOCATION_TYPE (Refill_Location_Type_Cd),
	 CONSTRAINT FK6REFILL_DC_SHELF_LINE  FOREIGN KEY (Placing_Id) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.REFILL_PLACING (Placing_Id),
	 CONSTRAINT FK7REFILL_DC_SHELF_LINE  FOREIGN KEY (Associate_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ASSOCIATE (Associate_Seq_Num),
	 CONSTRAINT FK8REFILL_DC_SHELF_LINE  FOREIGN KEY (Refill_By_User_Id) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.SALES_ORDER_MEMBER (Sales_Order_Member_Id),
	 CONSTRAINT FK9REFILL_DC_SHELF_LINE  FOREIGN KEY (Location_Zone_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOCATION_ZONE (Location_Zone_Seq_Num)
);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','REFILL_DC_SHELF_LINE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT','REFILL_DC_SHELF_LINE','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.REFILL_DC_SHELF_LINE IS '$Revision: 29277 $ - $Date: 2019-10-14 12:30:06 +0200 (mån, 14 okt 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

