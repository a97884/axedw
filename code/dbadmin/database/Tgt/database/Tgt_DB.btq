/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Tgt_DB.btq 27438 2019-03-18 15:57:55Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2019-03-18 16:57:55 +0100 (mån, 18 mar 2019) $
# Last Revision    : $Revision: 27438 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/database/Tgt_DB.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}Tgt
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}Tgt';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}Tgt
FROM ${DB_ENV}dbadmin AS
   PERM = 500000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}Tgt AS
'Parent Database for the Target Databases'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}Tgt TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}TgtT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}TgtT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}TgtT
FROM ${DB_ENV}Tgt AS
   PERM = 450000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}TgtT AS
'Database holding the Target tables'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}TgtT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}TgtVIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}TgtVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}TgtVIN
FROM ${DB_ENV}Tgt AS
   PERM = 50000000  -- Contains StoredProcedures
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}TgtVIN AS
'Database holding views for loading ${DB_ENV}TgtT'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}TgtVIN TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT,DELETE,INSERT,UPDATE,TABLE ON ${DB_ENV}TgtT TO ${DB_ENV}TgtVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT EXECUTE FUNCTION ON SYSLIB TO ${DB_ENV}TgtVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}TgtVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}TgtVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}TgtVOUT
FROM ${DB_ENV}Tgt AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}TgtVOUT AS
'Database holding 1 to 1 views for reading target tables'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}TgtVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}TgtT TO ${DB_ENV}TgtVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

/* ===============================================================================
PermSpace (re-)allocation (permspace required for StoredProcedures)
=============================================================================== */
SELECT PermSpace FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}TgtVIN'
AND PermSpace >= 49999995;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPMVSP
CREATE DATABASE #${DB_ENV}TgtVIN# FROM ${DB_ENV}dbadmin AS PERM = 50000000;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GIVE #${DB_ENV}TgtVIN# TO ${DB_ENV}TgtVIN;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
DROP DATABASE #${DB_ENV}TgtVIN#;
.LABEL SKIPMVSP

-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}TgtSecVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}TgtSecVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}TgtSecVOUT
FROM ${DB_ENV}Tgt AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}TgtSecVOUT AS
'Database holding 1 to 1 views for reading target tables containing sensitive data'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}TgtSecVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}TgtT TO ${DB_ENV}TgtSecVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE