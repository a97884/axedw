/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRAN_DISCOUNT_LINE.btq 23077 2017-09-13 06:57:01Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-09-13 08:57:01 +0200 (ons, 13 sep 2017) $
# Last Revision    : $Revision: 23077 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/fix/SALES_TRAN_DISCOUNT_LINE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}TgtT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- Verify that target table exist and has correct name
--------------------------------------------------------------------------
Select TableName From dbc.Tablesv Where DatabaseName = '${DB_ENV}TgtT' And TableName = 'SALES_TRAN_DISCOUNT_LINE'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 1 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Verify that preloaded table exist and has correct name
--------------------------------------------------------------------------
Select TableName From dbc.Tablesv Where DatabaseName = '${DB_ENV}TgtT' And TableName = 'SALES_TRAN_DISCOUNT_LINE_17R2'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 1 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Verify that temp-table created by NewTabDef doesn't already exist
--------------------------------------------------------------------------
Select TableName From dbc.Tablesv Where DatabaseName = '${DB_ENV}TgtT' And TableName = '#SALES_TRAN_DISCOUNT_LINE#01'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 0 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Run NewTabDef - PRE - for the table
--------------------------------------------------------------------------
CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','SALES_TRAN_DISCOUNT_LINE','PRE',NULL,1)
;
------------------------------------------------------------------------------------------------------------------------


-- Rename production-table from #SALES_TRAN_DISCOUNT_LINE#01 to #SALES_TRAN_DISCOUNT_LINE_OLDTABLE
--------------------------------------------------------------------------
RENAME TABLE ${DB_ENV}TgtT.#SALES_TRAN_DISCOUNT_LINE#01 AS ${DB_ENV}TgtT.#SALES_TRAN_DISCOUNT_LINE_OLDTABLE
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Rename preloaded table from SALES_TRAN_DISCOUNT_LINE_17R2 to SALES_TRAN_DISCOUNT_LINE
--------------------------------------------------------------------------
RENAME TABLE ${DB_ENV}TgtT.SALES_TRAN_DISCOUNT_LINE_17R2 AS ${DB_ENV}TgtT.SALES_TRAN_DISCOUNT_LINE
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Create empty table for NewTabDef-Procedure
--------------------------------------------------------------------------
CREATE TABLE ${DB_ENV}TgtT.#SALES_TRAN_DISCOUNT_LINE#01 AS ${DB_ENV}TgtT.#SALES_TRAN_DISCOUNT_LINE_OLDTABLE WITH NO DATA AND STATISTICS
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Run NewTabDef - POST - for the table
--------------------------------------------------------------------------
CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','SALES_TRAN_DISCOUNT_LINE','POST','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT','SALES_TRAN_DISCOUNT_LINE','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Comment on the new table
--------------------------------------------------------------------------
COMMENT ON ${DB_ENV}TgtT.SALES_TRAN_DISCOUNT_LINE IS '$Revision: 23077 $ - $Date: 2017-09-13 08:57:01 +0200 (ons, 13 sep 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
------------------------------------------------------------------------------------------------------------------------


-- Load last month from _OLDTABLE
--------------------------------------------------------------------------
DELETE FROM 
	${DB_ENV}TgtT.SALES_TRAN_DISCOUNT_LINE
WHERE
	Tran_Dt_DD between '2017-08-31' AND '2017-10-31'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}TgtT.SALES_TRAN_DISCOUNT_LINE (
	Sales_Tran_Seq_Num,
	Sales_Tran_Line_Num,
	Sales_Tran_Discnt_Line_Num,
	Tran_Dt_DD,
	Store_Seq_Num,
	Promotion_Offer_Seq_Num,
	Coupon_Serial_Seq_Num,
	Discount_Type_Cd,
	Discount_Amt_Incl_Vat,
	Discount_Amt_Excl_Vat,
	Tax_Amt,
	Discounted_Item_Ind,
	SRC_Activity_Code_Id,
	SRC_RPM_Id,
	SRC_Exclude_From_Statistics,
	SRC_Reason_Code,
	OpenJobRunId,
	CloseJobRunId
	)
SELECT
	Sales_Tran_Seq_Num,
	Sales_Tran_Line_Num,
	Sales_Tran_Discnt_Line_Num,
	Tran_Dt_DD,
	Store_Seq_Num,
	Promotion_Offer_Seq_Num,
	Coupon_Serial_Seq_Num,
	Discount_Type_Cd,
	Discount_Amt_Incl_Vat,
	Discount_Amt_Excl_Vat,
	Tax_Amt,
	Discounted_Item_Ind,
	SRC_Activity_Code_Id,
	SRC_RPM_Id,
	SRC_Exclude_From_Statistics,
	SRC_Reason_Code,
	OpenJobRunId,
	CloseJobRunId
FROM 
	${DB_ENV}TgtT.#SALES_TRAN_DISCOUNT_LINE_OLDTABLE
WHERE
	Tran_Dt_DD between '2017-08-31' AND '2017-10-31'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE