/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DC_EXTRNL_SALES_ORDER.btq 25320 2018-08-06 13:34:38Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-08-06 15:34:38 +0200 (mån, 06 aug 2018) $
# Last Revision    : $Revision: 25320 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/init/DC_EXTRNL_SALES_ORDER.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}TgtT;

--DELETE FROM ${DB_ENV}TgtT.DC_EXTRNL_SALES_ORDER where Sales_Order_Date between '2017-09-01' and '2018-01-31';

INSERT INTO ${DB_ENV}TgtT.DC_EXTRNL_SALES_ORDER(Sales_Order_Id,Sales_Order_Date,Order_Type_Cd,Req_Party_Seq_Num,Req_Location_Seq_Num,Manufacturer_Ind,Source_System_Transaction_Id,OpenJobRunId)
SELECT 
x2.Sales_Order_Id,
x2.Sales_Order_Date,
x2.Order_Type_Cd,
x2.Req_Party_Seq_Num,
x2.Req_Location_Seq_Num,
0 as Manufacturer_Ind,
x2.Source_System_Transaction_Id,
-1 as OpenJobRunId
FROM
(select 
x.Sales_Order_Id,
x.Order_Dt_DD as Sales_Order_Date,
x.Order_Type_Cd,
x.Req_Party_Seq_Num,
x.Req_Location_Seq_Num,
max(x.Inventory_Req_Order_Id) as Source_System_Transaction_Id
from
(SELECT	Order_Technical_Origin_Cd, Inventory_Req_Order_Id, Inventory_Req_Line_Num,
		Ref_Order_Row_Id, Transaction_Dttm, Order_Dt_DD, Supply_Location_Seq_Num,
		Req_Location_Seq_Num, Req_Party_Seq_Num, Article_Seq_Num, Measuring_Unit_Seq_Num,
		Order_Dttm, Pick_Dttm, Delivery_Dttm, Order_Type_Cd, Inventory_Request_Status_Cd,
		Exception_Cd, Partial_Delivery_Ind, Ordered_Qty, Ordered_Qty_in_Base_Units,
		Confirmed_Qty, Confirmed_Qty_in_Base_Units, Delivered_Qty, Delivered_Qty_in_Base_Units,
		Received_Qty, Received_Qty_in_Base_Units, Delivery_Dt_DD, Transportation_Group_Cd,
		OpenJobRunId, CloseJobRunId, InsertDttm,
		substr(Ref_Order_Row_Id,1,10) as Sales_Order_Id
FROM ${DB_ENV}TgtVOUT.INV_REQ_STATUS_LINE
where Order_Dt_DD between '2017-09-01' and '2018-01-31'
) as x
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION as y
on x.Req_Location_Seq_Num = y.Location_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION as z
on x.Supply_Location_Seq_Num = z.Location_Seq_Num
where coalesce(y.Location_Type_Cd,'EXT') = 'EXT'
group by 1,2,3,4,5
) as x2
;

--DELETE FROM ${DB_ENV}TgtT.DC_EXTRNL_SALES_ORDER_LINE where Sales_Order_Id in (select Sales_Order_Id FROM ${DB_ENV}TgtVOUT.DC_EXTRNL_SALES_ORDER where Sales_Order_Date between '2017-09-01' and '2018-01-31');

INSERT INTO ${DB_ENV}TgtT.DC_INTRNL_SALES_ORDER_LINE(
Sales_Order_Id, 
Sales_Order_Line_Num, 
Order_Creation_Dt,
Shortage_Code, 
Direct_Order_Ind, 
Delivery_Date, 
Article_Seq_Num,
Ordered_Qty, 
Confirmed_Qty, 
Ordered_Qty_In_Base_Units, 
Confirmed_Qty_In_Base_Units,
Supply_Location_Seq_num, 
Location_Zone_Seq_Num, 
Delivered_Qty,
Delivered_Qty_In_Base_Units, 
Received_Qty_In_Base_Units, 
Order_Technical_Origin_Cd,
Order_Reject_Reason_Cd, 
Ext_Order_Num, 
Ext_Order_Line, 
Long_Lead_Time_Ind,
Partial_Delivery_Ind, 
Expo_Article, 
Article_Engagement_Date,
Pick_Dttm, 
Transportation_Group_Cd, 
Vendor_Subrange_Cd, 
Process_Manager_Id,
Vendor_Id, 
Replacement_Ref, 
Record_Create_Date, 
Source_System_Transaction_Line,
Order_Supplier_Status_Cd, 
Order_Status_Cd, 
Return_Article_Ind,
Sales_Uom, 
Unloading_Point, 
OpenJobRunId
)
SELECT
x.Sales_Order_Id,
cast(substr(x.Ref_Order_Row_Id,12,6) as integer) as Sales_Order_Line_Num,
x.Order_Dt_DD as Order_Creation_Dt,
'-' as Shortage_Code,
0 as Direct_Order_Ind,
Delivery_Dt_DD as Delivery_Date,
x.Article_Seq_Num,
Ordered_Qty ,
Confirmed_Qty ,
Ordered_Qty_In_Base_Units,
Confirmed_Qty_In_Base_Units,
Supply_Location_Seq_num,
loz1.Location_Zone_Seq_Num,
Delivered_Qty,
Delivered_Qty_In_Base_Units,
Received_Qty_In_Base_Units,
Order_Technical_Origin_Cd,
REASON_FOR_REJECTION as Order_Reject_Reason_Cd,
Inventory_Req_Order_Id as Ext_Order_Num,
Inventory_Req_Line_Num as Ext_Order_Line,
0 as Long_Lead_Time_Ind,
Partial_Delivery_Ind,
null as Expo_Article,
null as Article_Engagement_Date,
Pick_Dttm,
x.Transportation_Group_Cd,
coalesce(va1.Vendor_Subrange_Cd,'-1') as Vendor_Subrange_Cd,
coalesce(pmp.Process_Manager_Id,-1) as Process_Manager_Id,
-1 as Vendor_Id,
'-1' as Replacement_Ref,
null as Record_Create_Date,
null as Source_System_Transaction_Line,
ARTICLE_SUPPLIER_STATUS_CD as Order_Supplier_Status_Cd,
Inventory_Request_Status_Cd as Order_Status_Cd,
cast(RETURN_ITEM  as integer) as Return_Article_Ind,
coalesce(mu1.N_MU_ID2,'-1') as Sales_Uom,
UNLOADING_POINT,
-1 as OpenJobRunId
from
(SELECT T1.*,
CASE 
	WHEN T3.LGORT IS NOT NULL AND T3.LGORT <> ' ' THEN T3.LGORT
    WHEN T4.RESLO IS NOT NULL AND T4.RESLO <> ' ' THEN T4.RESLO
	ELSE '-1' 
END AS N_Location_Zone_Id,
T3.ABGRU as REASON_FOR_REJECTION, -- orsak till avslag
  CASE 
    WHEN T3.MMSTA IS NOT NULL AND T3.MMSTA <> ' ' THEN T3.MMSTA
    WHEN T4.MMSTA IS NOT NULL AND T4.MMSTA <> ' ' THEN T4.MMSTA	
    WHEN T4.BSGRU IS NOT NULL AND T4.BSGRU <> ' ' THEN T4.BSGRU
	ELSE '-1' 
  END AS ARTICLE_SUPPLIER_STATUS_CD, --hem och slutkod
  CASE 
    WHEN T3.SHKZG IS NOT NULL AND T3.SHKZG <> ' ' THEN T3.SHKZG
    WHEN T4.RETPO IS NOT NULL AND T4.RETPO <> ' ' THEN T4.RETPO 
	ELSE 0 
  END AS RETURN_ITEM,
  CASE 
    WHEN T2.ABLAD IS NOT NULL AND T2.ABLAD <> ' ' THEN T2.ABLAD
	WHEN T5.ABLAD IS NOT NULL AND T5.ABLAD <> ' ' THEN T5.ABLAD
    ELSE '-1'
  END AS UNLOADING_POINT,
  CASE 
    WHEN T3.UEPOS = '000000' THEN 1
	WHEN T4.UEBPO = '00000' THEN 1
	ELSE 0
  END AS HIGHER_LEVEL_ITEM
FROM
(SELECT	Order_Technical_Origin_Cd, Inventory_Req_Order_Id, Inventory_Req_Line_Num,
		Ref_Order_Row_Id, Transaction_Dttm, Order_Dt_DD, Supply_Location_Seq_Num,
		Req_Location_Seq_Num, Req_Party_Seq_Num, Article_Seq_Num, Measuring_Unit_Seq_Num,
		Order_Dttm, Pick_Dttm, Delivery_Dttm, Order_Type_Cd, Inventory_Request_Status_Cd,
		Exception_Cd, Partial_Delivery_Ind, Ordered_Qty, Ordered_Qty_in_Base_Units,
		Confirmed_Qty, Confirmed_Qty_in_Base_Units, Delivered_Qty, Delivered_Qty_in_Base_Units,
		Received_Qty, Received_Qty_in_Base_Units, Delivery_Dt_DD, Transportation_Group_Cd,
		OpenJobRunId, CloseJobRunId, InsertDttm,
		substr(Ref_Order_Row_Id,1,10) as Sales_Order_Id,
		substr(Ref_Order_Row_Id,12,6) as Sales_Order_Line_Num
FROM ${DB_ENV}TgtVOUT.INV_REQ_STATUS_LINE
where Order_Dt_DD between '2017-09-01' and '2018-01-31'
) as T1

LEFT OUTER JOIN ${DB_ENV}TASRepT.VBAP T3
ON T1.Sales_Order_Id = T3.VBELN AND T1.Sales_Order_Line_Num = T3.POSNR

LEFT OUTER JOIN ${DB_ENV}TASRepT.EKPO T4
ON T1.Sales_Order_Id = T4.EBELN AND T1.Sales_Order_Line_Num = '0'||T4.EBELP

LEFT OUTER JOIN ${DB_ENV}TASRepT.EKPV T2
ON T1.Sales_Order_Id = T2.EBELN AND T1.Sales_Order_Line_Num = '0'||T2.EBELP

LEFT OUTER JOIN ${DB_ENV}TASRepT.VBPA T5
ON T1.Sales_Order_Id = T5.VBELN AND T1.Sales_Order_Line_Num = T5.POSNR AND T5.PARVW = 'WE'

) as x

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION as lo1
on x.Req_Location_Seq_Num = lo1.Location_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION as lo2
on x.Supply_Location_Seq_Num = lo2.Location_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION_ZONE as loz1
on lo2.N_Location_Id = loz1.N_Location_Id and x.N_Location_Zone_Id = loz1.N_Location_Zone_Id

LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D a1
on x.Article_Seq_Num = a1.Article_Seq_Num

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.VENDOR_ARTICLE va1
on x.Article_Seq_Num = va1.Article_Seq_Num and a1.Pref_Vendor_Seq_Num = va1.Dist_Org_Party_Seq_Num and va1.valid_to_dttm = timestamp '9999-12-31 23:59:59.999999'

LEFT OUTER JOIN ${DB_ENV}TgtVOUT.PROCESS_MANAGER_PARTY pmp
on x.Article_Seq_Num = pmp.Article_Seq_Num and x.Supply_Location_Seq_Num = pmp.Location_Seq_Num and pmp.valid_to_dttm = timestamp '9999-12-31 23:59:59.999999'

LEFT OUTER JOIN ${DB_ENV}MetaDataVOUT.MAP_MU as mu1
on x.Measuring_Unit_Seq_Num = mu1.MU_Seq_Num

where coalesce(lo1.Location_Type_Cd,'EXT') = 'EXT' AND HIGHER_LEVEL_ITEM = 1
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

