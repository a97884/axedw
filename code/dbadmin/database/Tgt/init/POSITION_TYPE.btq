/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: POSITION_TYPE.btq 18806 2016-04-05 12:41:42Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2016-04-05 14:41:42 +0200 (tis, 05 apr 2016) $
# Last Revision    : $Revision: 18806 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/init/POSITION_TYPE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

SELECT * FROM DBC.Tables WHERE DatabaseName = '${DB_ENV}TgtT' AND TableName = 'POSITION_TYPE';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPINS

DATABASE ${DB_ENV}TgtT;

CREATE VOLATILE TABLE #POSITION_TYPE#V1
,NO LOG
AS (SELECT * FROM ${DB_ENV}TgtT.POSITION_TYPE)
WITH NO DATA
ON COMMIT PRESERVE ROWS
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO #POSITION_TYPE#V1 VALUES('COA','Position till besöksadress','Contact address geo coordinates',-1,NULL);
INSERT INTO #POSITION_TYPE#V1 VALUES('ULA','Position till varuleveranssadress','Freight unloading geo coordinates',-1,NULL);
INSERT INTO #POSITION_TYPE#V1 VALUES('SHA','Position till varuavhämtningsadress','Freight loading geo coordinates',-1,NULL);
INSERT INTO #POSITION_TYPE#V1 VALUES('BIA','Position till fakturaadress','Billing address geo coordinates',-1,NULL);
INSERT INTO #POSITION_TYPE#V1 VALUES('UNK','Okänd position','Unknown geo coordinates',-1,NULL);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

MERGE INTO ${DB_ENV}TgtT.POSITION_TYPE
AS t1
USING (
	SELECT	s0.POS_TYPE_Cd
		,s0.POS_TYPE_Desc
		,s0.Pos_Type_Alt_Desc
	FROM #POSITION_TYPE#V1 AS s0
	LEFT OUTER JOIN ${DB_ENV}TgtT.POSITION_TYPE AS t0
	ON t0.POS_TYPE_Cd = s0.POS_TYPE_Cd
	WHERE	t0.POS_TYPE_Cd IS NULL
	OR	COALESCE(TRIM(t0.POS_TYPE_Desc),'') IN ('UNKNOWN', 'Saknar beskrivning')
	OR	COALESCE(TRIM(t0.POS_TYPE_Desc),'') <> s0.POS_TYPE_Desc
) AS s1
ON	t1.POS_TYPE_Cd = s1.POS_TYPE_Cd
WHEN MATCHED THEN UPDATE
SET	POS_TYPE_Desc = s1.POS_TYPE_Desc, Pos_Type_Alt_Desc = s1.Pos_Type_Alt_Desc
WHEN NOT MATCHED THEN INSERT
(	POS_TYPE_Cd
	,POS_TYPE_Desc
	,Pos_Type_Alt_Desc
	,OpenJobRunId
) VALUES (
	s1.POS_TYPE_Cd
	,s1.POS_TYPE_Desc
	,s1.Pos_Type_Alt_Desc
	,-1
);
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.LABEL SKIPINS
