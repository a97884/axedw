/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOCATION_TRAIT.btq 29552 2019-11-25 15:31:42Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-25 16:31:42 +0100 (mån, 25 nov 2019) $
# Last Revision    : $Revision: 29552 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/init/LOCATION_TRAIT.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
SELECT * FROM DBC.Tables WHERE DatabaseName = '${DB_ENV}TgtT' AND TableName = 'LOCATION_TRAIT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPINS

DATABASE ${DB_ENV}TgtT;

CREATE VOLATILE TABLE #LOCATION_TRAIT#V1
,NO LOG
AS (SELECT * FROM ${DB_ENV}TgtT.LOCATION_TRAIT)
WITH NO DATA
ON COMMIT PRESERVE ROWS
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO #LOCATION_TRAIT#V1 (Location_Trait_Cd,Location_Trait_Desc,OpenJobRunID,CloseJobRunID) VALUES ('-1','Odefinierad',-1,NULL);
INSERT INTO #LOCATION_TRAIT#V1 (Location_Trait_Cd,Location_Trait_Desc,OpenJobRunID,CloseJobRunID) VALUES ('ACT','Actual Store',-1,NULL);
INSERT INTO #LOCATION_TRAIT#V1 (Location_Trait_Cd,Location_Trait_Desc,OpenJobRunID,CloseJobRunID) VALUES ('AO','Autoorder Store',-1,NULL);
INSERT INTO #LOCATION_TRAIT#V1 (Location_Trait_Cd,Location_Trait_Desc,OpenJobRunID,CloseJobRunID) VALUES ('SAE','Sales Area',-1,NULL);
INSERT INTO #LOCATION_TRAIT#V1 (Location_Trait_Cd,Location_Trait_Desc,OpenJobRunID,CloseJobRunID) VALUES ('TAE','Total Area',-1,NULL);
INSERT INTO #LOCATION_TRAIT#V1 (LOCATION_TRAIT_CD,Location_Trait_Desc,OpenJobRunID,CloseJobRunID) VALUES ('TST','Test Store',-1,NULL);
INSERT INTO #LOCATION_TRAIT#V1 (LOCATION_TRAIT_CD,Location_Trait_Desc,OpenJobRunID,CloseJobRunID) VALUES ('CAV','CAR Validation Activation',-1,NULL);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

MERGE INTO ${DB_ENV}TgtT.LOCATION_TRAIT
AS t1
USING (
	SELECT	
		s0.Location_Trait_Cd,
		s0.Location_Trait_Desc
	FROM #LOCATION_TRAIT#V1 AS s0
	LEFT OUTER JOIN ${DB_ENV}TgtT.LOCATION_TRAIT AS t0
	ON t0.Location_Trait_Cd = s0.Location_Trait_Cd
	WHERE	t0.Location_Trait_Cd IS NULL
	OR	COALESCE(TRIM(t0.LOCATION_TRAIT_Desc),'') IN ('UNKNOWN', 'Saknar beskrivning')
) AS s1
ON	t1.Location_Trait_Cd = s1.Location_Trait_Cd
WHEN MATCHED THEN UPDATE
SET	Location_Trait_Desc = s1.Location_Trait_Desc
WHEN NOT MATCHED THEN INSERT
(	Location_Trait_Cd,
	Location_Trait_Desc,
	OpenJobRunId
) VALUES (
	s1.Location_Trait_Cd,
	s1.Location_Trait_Desc,
	-1
);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.LABEL SKIPINS
