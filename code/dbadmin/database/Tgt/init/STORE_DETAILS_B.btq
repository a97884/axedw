/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STORE_DETAILS_B.btq 12265 2014-01-28 13:28:58Z k9107640 $
# Last Changed By  : $Author: k9107640 $
# Last Change Date : $Date: 2014-01-28 14:28:58 +0100 (tis, 28 jan 2014) $
# Last Revision    : $Revision: 12265 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/init/STORE_DETAILS_B.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

SELECT * FROM DBC.Tables WHERE DatabaseName = '${DB_ENV}TgtT' AND TableName = 'STORE_DETAILS_B'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPINS

DATABASE ${DB_ENV}TgtT
;

CREATE VOLATILE TABLE #STORE_DETAILS_B#V1
AS (
Select 
 cpts.Gen_Seq_Num As Store_Seq_Num,
TIMESTAMP  '0001-01-01 00:00:00.000000' AS Valid_From_Dttm,
 cpts.Store_Type_Cd As Store_Type_Cd,
 cpts.Concept_Cd As Concept_Cd,
 '-1' AS Location_Strategy_Cd,
 '-1' AS Assortment_Profile_Cd,
 TIMESTAMP  '9999-12-31 23:59:59.999999' AS Valid_To_Dttm,
 '-1' As OpenJobrunId,
 NULL as CloseJobrunId
From
(
 Select 
  ROW_NUMBER() OVER (ORDER BY Concept_Cd Desc,Store_Type_Cd Asc)*-1 -1 AS Gen_Seq_Num
  -- starts with -2 since -1 is reserved for missing masterdata
  , Concept_Cd
  , Store_Type_Cd
  , Store_Name
  , Sales_Org_Party_Seq_Num
 From
 (
  Select 
   Concept_Cd
   , Store_Type_Cd
   , Concept_Name || ' ' || Store_Type_Desc As Store_Name
   ,  Case 
     When Concept_Cd = 'EUC'  And Store_Type_Cd = 'FRAN' Then 'S903' 
     When Concept_Cd = 'HEA'  And Store_Type_Cd = 'FRAN' Then 'S901' 
     When Concept_Cd = 'HEM'  And Store_Type_Cd = 'FRAN' Then 'S901' 
     When Concept_Cd = 'HEM'  And Store_Type_Cd = 'CORP' Then 'S005'      
     When Concept_Cd = 'NEX'  And Store_Type_Cd = 'CORP' Then 'S007'  
     When Concept_Cd = 'PRX'  And Store_Type_Cd = 'CORP' Then 'S006'            
     When Concept_Cd = 'SNG'  And Store_Type_Cd = 'CORP' Then 'S008'   
     When Concept_Cd = 'TEM'  And Store_Type_Cd = 'FRAN' Then 'S902' 
     When Concept_Cd = 'WH2'  And Store_Type_Cd = 'FRAN' Then 'S903' 
     When Concept_Cd = 'WH2'  And Store_Type_Cd = 'CORP' Then 'S004'      
     When Concept_Cd = 'WHE'  And Store_Type_Cd = 'FRAN' Then 'S903' 
     When Concept_Cd = 'WHE'  And Store_Type_Cd = 'CORP' Then 'S004'   
     When Concept_Cd = 'WIL'  And Store_Type_Cd = 'FRAN' Then 'S903' 
     When Concept_Cd = 'WIL'  And Store_Type_Cd = 'CORP' Then 'S004'        
    Else '-1'
    End As Sales_Org_Id
  From ${DB_ENV}TgtVOUT.CONCEPT, ${DB_ENV}TgtVOUT.STORE_TYPE
  Where Concept_Top_Cd is not null and Concept_Top_Cd <> '-1'
  And Store_Type_Cd is not null and Store_Type_Cd <> '-1'
 ) genStores
 Inner Join 
 (
  Select 
   Org_Party_Seq_Num As Sales_Org_Party_Seq_Num,
   N_Org_Party_Id As Sales_Org_Id
  From ${DB_ENV}TgtVOUT.ORGANIZATION org
  Where Org_Type_Cd = 'INT' And Internal_Org_Type_Cd = 'SORG'
 ) salesOrgs
 On salesOrgs.Sales_Org_Id = genStores.Sales_Org_Id
) cpts
UNION ALL -- Add record for missing masterdata

SELECT d0.Dummy_Seq_Num AS Store_Seq_Num
 ,TIMESTAMP  '0001-01-01 00:00:00.000000' AS Valid_from_From_Dt
 ,d0.Dummy_Id AS Store_Type_Cd
 ,d0.Dummy_Id AS Concept_Cd
 ,'-1' AS Location_Strategy_Cd
 ,'-1' AS Assortment_Profile_Cd
 ,TIMESTAMP  '9999-12-31 23:59:59.999999' AS Valid_To_Dt
 ,'-1' As OpenJobrunId
 ,NULL as CloseJobrunId
FROM ${DB_ENV}SemCMNT.DUMMY d0
) WITH DATA
ON COMMIT PRESERVE ROWS
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- 
MERGE INTO ${DB_ENV}TgtT.STORE_DETAILS_B
AS t1
USING (
	SELECT	s0.Store_Seq_Num AS Store_Seq_Num
		,s0.Valid_From_Dttm	AS Valid_From_Dttm
		,s0.Store_Type_Cd	AS Store_Type_Cd
		,s0.Concept_Cd	AS Concept_Cd
		,s0.Location_Strategy_Cd AS Location_Strategy_Cd
		,s0.Assortment_Profile_Cd AS Assortment_Profile_Cd
		,s0.Valid_To_Dttm AS Valid_To_Dttm
		,s0.OpenJobRunId AS OpenJobRunId
	FROM #STORE_DETAILS_B#V1 AS s0
	LEFT OUTER JOIN ${DB_ENV}TgtT.STORE_DETAILS_B AS t0
	ON t0.Store_Seq_Num = s0.Store_Seq_Num
	AND t0.Valid_From_Dttm = s0.Valid_From_Dttm
	WHERE	t0.Store_Seq_Num IS NULL
	AND     t0.Valid_From_Dttm IS NULL
) AS s1
ON	t1.Store_Seq_Num = s1.Store_Seq_Num
AND	t1.Valid_From_Dttm = s1.Valid_From_Dttm
WHEN MATCHED THEN UPDATE
SET	Store_Type_Cd	= s1.Store_Type_Cd
	,Concept_Cd	= s1.Concept_Cd
	,Location_Strategy_Cd	= s1.Location_Strategy_Cd
	,Assortment_Profile_Cd	= s1.Assortment_Profile_Cd
WHEN NOT MATCHED THEN INSERT
(	Store_Seq_Num
	,Valid_From_Dttm
	,Store_Type_Cd
	,Concept_Cd
	,Location_Strategy_Cd
	,Assortment_Profile_Cd
	,Valid_To_Dttm
	,OpenJobRunId
	,CloseJobRunId
) VALUES (
	s1.Store_Seq_Num
	,s1.Valid_From_Dttm
	,s1.Store_Type_Cd
	,s1.Concept_Cd
	,s1.Location_Strategy_Cd
	,s1.Assortment_Profile_Cd
	,s1.Valid_To_Dttm
	,s1.OpenJobRunId
	,NULL
)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.LABEL SKIPINS
