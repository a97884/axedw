/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PRICE_CALCULATION_TYPE.btq 6957 2012-10-25 06:24:43Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2012-10-25 08:24:43 +0200 (tor, 25 okt 2012) $
# Last Revision    : $Revision: 6957 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/init/PRICE_CALCULATION_TYPE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

SELECT * FROM DBC.Tables WHERE DatabaseName = '${DB_ENV}TgtT' AND TableName = 'PRICE_CALCULATION_TYPE'
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPINS

DATABASE ${DB_ENV}TgtT
;

CREATE VOLATILE TABLE #PRICE_CALCULATION_TYPE#V1
,NO LOG
AS (SELECT * FROM ${DB_ENV}TgtT.PRICE_CALCULATION_TYPE)
WITH NO DATA
ON COMMIT PRESERVE ROWS
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES(DEFAULT(${DB_ENV}TgtT.PRICE_CALCULATION_TYPE.Price_Calculation_Type_Cd),'Odefinierad',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('A','Percentage',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('B','Fixed amount',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('C','Quantity',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('D','Gross weight',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('E','Net weight',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('F','Volume',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('G','Formula',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('H','Percentage included',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('I','Percentage (travel expenses)',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('K','Per mille',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('J','Per mille',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('L','Points',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('M','Quantity - monthy price',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('N','Quantity - yearly price',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('O','Quantity - daily price',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('P','Quantity - weekly price',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('Q','Commodity Price',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('R','Distance-dependent',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('S','Number of shipping units',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('T','Multi-dimensional',-1,NULL)
;INSERT INTO #PRICE_CALCULATION_TYPE#V1 VALUES('U','Percentage FIN (CRM Only)',-1,NULL)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

MERGE INTO ${DB_ENV}TgtT.PRICE_CALCULATION_TYPE
AS t1
USING (
	SELECT	s0.Price_Calculation_Type_Cd
		,s0.Price_Calculation_Type_Desc
	FROM #PRICE_CALCULATION_TYPE#V1 AS s0
	LEFT OUTER JOIN ${DB_ENV}TgtT.PRICE_CALCULATION_TYPE AS t0
	ON t0.Price_Calculation_Type_Cd = s0.Price_Calculation_Type_Cd
	WHERE	t0.Price_Calculation_Type_Cd IS NULL
	OR	COALESCE(TRIM(t0.Price_Calculation_Type_Desc),'') IN ('UNKNOWN', 'Saknar beskrivning')
) AS s1
ON	t1.Price_Calculation_Type_Cd = s1.Price_Calculation_Type_Cd
WHEN MATCHED THEN UPDATE
SET	Price_Calculation_Type_Desc = s1.Price_Calculation_Type_Desc
WHEN NOT MATCHED THEN INSERT
(	Price_Calculation_Type_Cd
	,Price_Calculation_Type_Desc
	,OpenJobRunId
) VALUES (
	s1.Price_Calculation_Type_Cd
	,s1.Price_Calculation_Type_Desc
	,-1
)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.LABEL SKIPINS
