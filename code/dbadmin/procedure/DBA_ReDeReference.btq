/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DBA_ReDeReference.btq 5671 2012-09-13 18:24:09Z k9106728 $
# Last Changed By  : $Author: k9106728 $
# Last Change Date : $Date: 2012-09-13 20:24:09 +0200 (tor, 13 sep 2012) $
# Last Revision    : $Revision: 5671 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/procedure/DBA_ReDeReference.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Restores/Removes References (ForeignKey constraints) on table(s)
# Project     : EDW1
# Subproject  :
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2011-09-02 Teradata  Initial version
# 2011-11-29 Teradata  Changed to common DBA_ReDeDefData table
# --------------------------------------------------------------------------
# Description
#   Restores/Removes References (ForeignKey constraints) on table(s)
#   If "p_TrgTab" is NULL then all tables in spec database are targeted
#   If "p_TrgTab" parameter ends with a "*" a simple pattern-match is performed
#
# Dependencies
#   ${DB_ENV}dbadmin.DBA_ReDeDefData (Table)
#   ${DB_ENV}dbadmin.DBA_DebugLogData (Table)
#
# Parameters
#	Required Parameters
#	 p_TrgDb	- Target database containing table(s) to de/re-reference
#	 p_Operation	- References TO the table (Parent) are targeted (DE,RE,DR or SV)
#		RE = Re-Reference, re-create refereces to Target from saved FK-def
#		DE = De-Reference, Save & Drop FK-def where Target is Parent
#		DR = Drop-Reference, Drop (no save) FK-def where Target is Parent
#		SV = Save-Reference Save (no drop) FK-def where Target is Parent
#	Optional parameters
#	 p_TrgTab	- Table to de/re-reference.
#	 p_Options	- Options
#		C = Also clear/remove all FK constraints from Target (child)
#	 p_Debug	- If "1" insert generated sql into DBA_DebugLogData table
#		If "99" no exec, only insert generated sql into DBA_DebugLogData table
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}dbadmin.DBA_ReDeReference
(	p_TrgDb	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_TrgTab	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Operation	CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Options	VARCHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Debug	BYTEINT
)
P0: BEGIN
/*****************************************************************************
Required Parameters:
 p_TrgDb	- Target database containing table(s) to de/re-reference
 p_Operation	- References TO the table (Parent) are targeted (DE,RE,DR or SV)
 		RE = Re-Reference, re-create refereces to Target from saved FK-def
 		DE = De-Reference, Save & Drop FK-def where Target is Parent
 		DR = Drop-Reference, Drop (no save) FK-def where Target is Parent
 		SV = Save-Reference Save (no drop) FK-def where Target is Parent
Optional parameters:
 p_TrgTab	- Table to de/re-reference.
 p_Options	- Options
 		C = Also clear/remove all FK constraints from Target (child)
 p_Debug	- If "1" insert generated sql into DBA_DebugLogData table
		If "99" no exec, only insert generated sql into DBA_DebugLogData table

Description:	Restores/Removes References (ForeignKey constraints) on table(s)
	If "p_TrgTab" is NULL then all tables in spec database are targeted
	If "p_TrgTab" parameter ends with a "*" a simple pattern-match is performed

Requires:	Table "DBA_ReDeDefData" for saved Reference-definitions
*****************************************************************************/
DECLARE v_PrcName	VARCHAR(65) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_PrcDttm	TIMESTAMP(3);
DECLARE v_EOL		CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Blank	CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Space	CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_BB	CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_BE	CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_P	CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_SQLstate	CHAR(5);
DECLARE v_dbadmin	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_TrgTab	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Match	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_SDTexists	SMALLINT;
DECLARE v_DelOld	BYTEINT;
DECLARE v_ChkOpt	VARCHAR(20);
DECLARE v_PCdiff	SMALLINT;
DECLARE v_CCdiff	SMALLINT;
DECLARE v_SQLgrant	VARCHAR(500) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_SQLalter	VARCHAR(30000) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_SQLdel	VARCHAR(500) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_SQLsave	VARCHAR(30000) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_ChildCols	VARCHAR(2000) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_ParentCols	VARCHAR(2000) CHARACTER SET LATIN NOT CASESPECIFIC;

SET v_EOL=TRANSLATE('0D'xc USING UNICODE_TO_LATIN WITH ERROR);
SET v_Blank=TRANSLATE('' USING UNICODE_TO_LATIN WITH ERROR);
SET v_Space=TRANSLATE(' ' USING UNICODE_TO_LATIN WITH ERROR);
SET v_BB=TRANSLATE('(' USING UNICODE_TO_LATIN WITH ERROR);
SET v_BE=TRANSLATE(')' USING UNICODE_TO_LATIN WITH ERROR);
SET v_P=TRANSLATE('.' USING UNICODE_TO_LATIN WITH ERROR);
SET v_PrcName = '${DB_ENV}dbadmin.DBA_ReDeReference';
SET v_PrcDttm = CURRENT_TIMESTAMP(3);
SET v_dbadmin='${DB_ENV}dbadmin';

-- Verify parameters
IF COALESCE(p_TrgDb,'')=''
OR POSITION('${DB_ENV}' IN COALESCE(p_TrgDb,''))<>1
OR COALESCE(p_Operation,'') NOT IN ('DE','RE','DR','SV')
THEN
	LEAVE P0;
END IF;

-- Set procedure defaults
SET v_ChkOpt='WITH NO CHECK OPTION' -- currently not be derivable from DBC
;

SET v_TrgTab = TRANSLATE(TRIM(COALESCE(p_TrgTab,v_Blank)) USING UNICODE_TO_LATIN WITH ERROR)
;
IF POSITION('*' IN v_TrgTab) > 0
THEN
	SET v_Match = TRIM(SUBSTRING(v_TrgTab FROM 1 FOR POSITION('*' IN v_TrgTab)-1))
	;
ELSE
	SET v_Match = NULL
	;
END IF;

-- Use all tables/views matching "p_TrgTab" name/pattern in "p_TrgDb" database
F_TL: FOR cur_TL AS cur_TabList CURSOR FOR
	LOCKING ROW FOR ACCESS
	SELECT
		t0.DatabaseName
		,t0.TableName
		,t0.CreatorName
		,r0.x_PC
	FROM DBC.Tables AS t0
	INNER JOIN (
		SELECT	rd0.TrgDbName	AS TargetDB
			,rd0.TrgTabName	AS TargetTable
			,'P'	(CHAR(1)) AS x_PC
		FROM ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
		WHERE	COALESCE(:p_Operation,'') = 'RE'
		AND	rd0.TrgDbName		= :p_TrgDb
		AND	(CASE
			WHEN :p_TrgTab IS NULL
			THEN (CASE WHEN POSITION('#' IN rd0.TrgTabName)=1 THEN 0 ELSE 1 END)
			WHEN COALESCE(:v_Match,'***') = ''
			THEN 1
			WHEN :v_Match IS NOT NULL
			THEN (CASE WHEN POSITION(:v_Match IN rd0.TrgTabName)=1 THEN 1
				ELSE 0 END)
			WHEN TRIM(rd0.TrgTabName) = TRIM(:p_TrgTab)
			THEN 1
			ELSE 0
			END) = 1
		AND	rd0.DefClass = 'RE' --References def
		GROUP BY 1,2
		UNION
		SELECT
			r02.ParentDB	AS TargetDB
			,r02.ParentTable	AS TargetTable
			,'P'	(CHAR(1)) AS x_PC
		FROM DBC.RI_Distinct_Parents AS r02
		WHERE	COALESCE(:p_Operation,'') IN ('DE','DR','SV')
		AND	r02.ParentDB		= :p_TrgDb
		AND	(CASE
			WHEN :p_TrgTab IS NULL
			THEN (CASE WHEN POSITION('#' IN r02.ParentTable)=1 THEN 0 ELSE 1 END)
			WHEN COALESCE(:v_Match,'***') = ''
			THEN 1
			WHEN :v_Match IS NOT NULL
			THEN (CASE WHEN POSITION(:v_Match IN r02.ParentTable)=1 THEN 1
				ELSE 0 END)
			WHEN TRIM(r02.ParentTable) = TRIM(:p_TrgTab)
			THEN 1
			ELSE 0
			END) = 1
		GROUP BY 1,2
		UNION
		SELECT
			r03.ChildDB	AS TargetDB
			,r03.ChildTable	AS TargetTable
			,'C'	(CHAR(1)) AS x_PC
		FROM DBC.RI_Distinct_Children AS r03
		WHERE	COALESCE(:p_Operation,'') IN ('DE','DR')
		AND	POSITION('C' IN COALESCE(:p_Options,''))>0
		AND	r03.ChildDB		= :p_TrgDb
		AND	(CASE
			WHEN :p_TrgTab IS NULL
			THEN (CASE WHEN POSITION('#' IN r03.ChildTable)=1 THEN 0 ELSE 1 END)
			WHEN COALESCE(:v_Match,'***') = ''
			THEN 1
			WHEN :v_Match IS NOT NULL
			THEN (CASE WHEN POSITION(:v_Match IN r03.ChildTable)=1 THEN 1
				ELSE 0 END)
			WHEN TRIM(r03.ChildTable) = TRIM(:p_TrgTab)
			THEN 1
			ELSE 0
			END) = 1
		GROUP BY 1,2
	) AS r0
	ON r0.TargetDB = t0.DatabaseName
	AND r0.TargetTable = t0.TableName
	LEFT OUTER JOIN DBC.ErrorTblsV AS e0
	ON e0.ErrTblDbName = t0.DatabaseName
	AND e0.ErrTblName = t0.TableName
	WHERE	t0.DatabaseName	= :p_TrgDb
	AND	(CASE
		WHEN :p_TrgTab IS NULL
		THEN (CASE WHEN POSITION('#' IN t0.TableName)=1 THEN 0 ELSE 1 END)
		WHEN COALESCE(:v_Match,'***') = ''
		THEN 1
		WHEN :v_Match IS NOT NULL
		THEN (CASE WHEN POSITION(:v_Match IN t0.TableName)=1 THEN 1
			ELSE 0 END)
		WHEN TRIM(t0.TableName) = TRIM(:p_TrgTab)
		THEN 1
		ELSE 0
		END) = 1
	AND	t0.TableKind IN ('T','O')
	AND	t0.QueueFlag = 'N' -- exclude queue tables
	AND	e0.ErrTblName IS NULL -- exclude errortables
	GROUP BY 1,2,3,4
	ORDER BY t0.DatabaseName,t0.TableName
DO
	IF COALESCE(p_Operation,'')='RE'
	AND cur_TL.x_PC = 'P'
	THEN -- Get saved definitions and Re-Reference

		IF cur_TL.CreatorName <> TRIM(v_dbadmin)
		THEN -- Not Creator of Parent, grant reference access to Parent
			SET v_SQLgrant=TRANSLATE((CAST('GRANT REFERENCES ON ' AS CHAR(20))
				||TRIM(cur_TL.DatabaseName)||v_P||TRIM(cur_TL.TableName)
				||CAST(' TO ' AS CHAR(4))
				||TRIM(v_dbadmin)
				||CAST(' WITH GRANT OPTION;' AS CHAR(19))||v_EOL
				) USING UNICODE_TO_LATIN WITH ERROR)
			;
			IF ZEROIFNULL(p_Debug)<>0
			THEN	-- Insert debug-text
				INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
				(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
				) VALUES (
					:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
					,'"'||TRIM(:cur_TL.DatabaseName)||'"."'||TRIM(:cur_TL.TableName)||'"'
					,'${DB_ENV}dbadmin'
					,USER,SESSION,:v_SQLgrant
				);
			END IF;
			IF ZEROIFNULL(p_Debug)<>99
			THEN
				CALL DBC.SysExecSQL(:v_SQLgrant);
			END IF;
		END IF;

		SET v_SQLalter=v_blank;

		F_RR: FOR cur_RR AS cur_ReRef CURSOR FOR
		LOCKING ROW FOR ACCESS
		SELECT	rd0.TrgDbName	AS ParentDb
			,rd0.TrgTabName	AS ParentTable
			,rd0.OthrDbName	AS ChildDb
			,rd0.OthrTabName	AS ChildTable
			,rd0.DefId
			,rd0.DefName
			,rd0.TrgColList	AS ParentCols
			,rd0.OthrColList	AS ChildCols
			,rd0.DefOpt	AS CheckOpt
		FROM ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
		WHERE	rd0.TrgDbName		= :cur_TL.DatabaseName
		AND	rd0.TrgTabName	= :cur_TL.TableName
		AND	rd0.DefClass = 'RE' --References def
		AND	COALESCE(:p_Operation,'')='RE'
		ORDER BY rd0.OthrDbName
			,rd0.OthrTabName
			,rd0.DefId
		DO
			-- RE-Reference
			SET v_SQLalter=TRANSLATE(('ALTER TABLE '
				||TRIM(cur_RR.ChildDB)||v_P||TRIM(cur_RR.ChildTable)||v_EOL
				||CAST(' ADD ' AS CHAR(5))
				||TRIM(CAST((CASE WHEN COALESCE(cur_RR.DefName,'') <> ''
					THEN 'CONSTRAINT ' || TRIM(COALESCE(cur_RR.DefName,''))
					ELSE ''
					END) AS VARCHAR(45)))
				||CAST(' FOREIGN KEY ' AS CHAR(13))
				||v_BB||TRIM(cur_RR.ChildCols)||v_BE||v_EOL
				||CAST(' REFERENCES ' AS CHAR(12))
				||TRIM(cur_RR.CheckOpt)||v_EOL
				||v_Space||TRIM(cur_RR.ParentDB)||v_P||TRIM(cur_RR.ParentTable)
				||v_Space||v_BB||TRIM(cur_RR.ParentCols)||v_BE||v_EOL
				||CAST(';' AS CHAR(1))||v_EOL
				) USING UNICODE_TO_LATIN WITH ERROR);
			IF ZEROIFNULL(p_Debug)<>0
			THEN	-- Insert debug-text
				INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
				(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
				) VALUES (
					:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
					,'"'||TRIM(:cur_TL.DatabaseName)||'"."'||TRIM(:cur_TL.TableName)||'"'
					,'"'||TRIM(:cur_RR.ChildDB)||'"."'||TRIM(:cur_RR.ChildTable)||'"'
					,USER,SESSION,:v_SQLalter
				);
			END IF;
			IF ZEROIFNULL(p_Debug)<>99
			THEN
				B1: BEGIN
					-- ignore errors
					DECLARE CONTINUE HANDLER
					FOR SQLEXCEPTION, SQLWARNING
					SET v_SQLstate=SQLSTATE
					;
					CALL DBC.SysExecSQL(:v_SQLalter)
					;
				END B1;
			END IF;
		END FOR F_RR;
	END IF;

	IF cur_TL.x_PC = 'P'
	THEN -- Delete saved old FK-def
		SET v_SQLdel = TRANSLATE((
			'DELETE FROM ${DB_ENV}dbadmin.DBA_ReDeDefData'||v_EOL
			||' WHERE TrgDbName ='''||TRIM(cur_TL.DatabaseName)||''''||v_EOL
			||' AND TrgTabName ='''||TRIM(cur_TL.TableName)||''''||v_EOL
			||' AND DefClass = ''RE'''||v_EOL --References def
			||';'
			) USING UNICODE_TO_LATIN WITH ERROR);
		IF ZEROIFNULL(p_Debug)<>0
		THEN	-- Insert debug-text
			INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
			(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
			) VALUES (
				:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
				,'"'||TRIM(:cur_TL.DatabaseName)||'"."'||TRIM(:cur_TL.TableName)||'"'
				,'"'||TRIM(:cur_TL.DatabaseName)||'"."'||TRIM(:cur_TL.TableName)||'"'
				,USER,SESSION,:v_SQLdel
			);
		END IF;
		IF ZEROIFNULL(p_Debug)<>99
		THEN
			CALL DBC.SysExecSQL(:v_SQLdel);
		END IF;
	END IF;

	IF COALESCE(p_Operation,'') IN ('DE','DR','SV')
	THEN -- Create definitions and DE-Reference

		SET v_SQLalter = v_blank;
		SET v_SQLsave = v_blank;
		SET v_ChildCols = v_blank;
		SET v_ParentCols = v_blank;

		F_DR: FOR cur_DR AS cur_DeRef CURSOR FOR
		LOCKING ROW FOR ACCESS
		SELECT
			rd0.ParentDB
			,rd0.ParentTable
			,rd0.ParentKeyColumn
			,rd0.ChildDB
			,rd0.ChildTable
			,rd0.ChildKeyColumn
			,rd0.DefId
			,rd0.DefName
			,rd0.DefOpt
			,rd0.x_PC
			,rd0.x_ColNo
			,rd0.x_LastCol
		FROM (
			SELECT -- FK constraints to Target (parent)
				r11.ParentDB
				,r11.ParentTable
				,r11.ParentKeyColumn
				,r11.ChildDB
				,r11.ChildTable
				,r11.ChildKeyColumn
				,r11.IndexID	AS DefId
				,r11.IndexName	AS DefName
				,:v_ChkOpt	AS DefOpt -- currently not be derivable from DBC
				,:cur_TL.x_PC	AS x_PC
				,RANK()OVER(PARTITION BY r11.ChildDB,r11.ChildTable,r11.IndexID
					ORDER BY c11.ColumnId ASC) AS x_ColNo
				,RANK()OVER(PARTITION BY r11.ChildDB,r11.ChildTable,r11.IndexID
					ORDER BY c11.ColumnId DESC) AS x_LastCol
			FROM DBC.All_RI_Parents AS r11
			INNER JOIN DBC.Columns AS c11
			ON	c11.DatabaseName = r11.ChildDB
			AND	c11.TableName = r11.ChildTable
			AND	c11.ColumnName = r11.ChildKeyColumn
			WHERE	r11.ParentDB = :cur_TL.DatabaseName
			AND	r11.ParentTable = :cur_TL.TableName
			AND	:cur_TL.x_PC = 'P'
			UNION ALL
			SELECT	-- FK constraints from Target (child)
				r12.ParentDB
				,r12.ParentTable
				,r12.ParentKeyColumn
				,r12.ChildDB
				,r12.ChildTable
				,r12.ChildKeyColumn
				,r12.IndexID	AS DefId
				,r12.IndexName	AS DefName
				,:v_ChkOpt	AS DefOpt -- currently not be derivable from DBC
				,:cur_TL.x_PC	AS x_PC
				,RANK()OVER(PARTITION BY r12.ChildDB,r12.ChildTable,r12.IndexID
					ORDER BY c12.ColumnId ASC) AS x_ColNo
				,RANK()OVER(PARTITION BY r12.ChildDB,r12.ChildTable,r12.IndexID
					ORDER BY c12.ColumnId DESC) AS x_LastCol
			FROM DBC.All_RI_Children AS r12
			INNER JOIN DBC.Columns AS c12
			ON	c12.DatabaseName = r12.ChildDB
			AND	c12.TableName = r12.ChildTable
			AND	c12.ColumnName = r12.ChildKeyColumn
			WHERE	r12.ChildDB = :cur_TL.DatabaseName
			AND	r12.ChildTable = :cur_TL.TableName
			-- self-ref are captured in the "parent" set
			AND NOT (	r12.ParentDB = r12.ChildDB
				AND	r12.ParentTable = r12.ChildTable
				)
			AND	:cur_TL.x_PC = 'C'
		) AS rd0
		ORDER BY rd0.ChildDB
			,rd0.ChildTable
			,rd0.DefId
			,rd0.x_ColNo
			,rd0.x_PC
		DO
			IF cur_DR.x_ColNo = 1
			THEN
				SET v_ChildCols = TRANSLATE(TRIM(cur_DR.ChildKeyColumn) USING UNICODE_TO_LATIN WITH ERROR);
				SET v_ParentCols = TRANSLATE(TRIM(cur_DR.ParentKeyColumn) USING UNICODE_TO_LATIN WITH ERROR);
			ELSE
				SET v_ChildCols = TRANSLATE((TRIM(v_ChildCols)
					||CAST(',' AS CHAR(1))
					||TRIM(cur_DR.ChildKeyColumn)
					)  USING UNICODE_TO_LATIN WITH ERROR);
				SET v_ParentCols = TRANSLATE((TRIM(v_ChildCols)
					||CAST(',' AS CHAR(1))
					||TRIM(cur_DR.ParentKeyColumn)
					)  USING UNICODE_TO_LATIN WITH ERROR);
			END IF;

			IF cur_DR.x_PC = 'P'
			AND p_Operation IN ('DE','SV')
			AND cur_DR.x_LastCol = 1
			THEN -- Save definition
				SET v_SQLsave = TRANSLATE((
					'INSERT INTO ${DB_ENV}dbadmin.DBA_ReDeDefData'||v_EOL
					||'(TrgDbName,TrgTabName,OthrDbName,OthrTabName,DefClass,DefId,DefOpt,DefName,TrgColList,OthrColList,SaveDttm,SessionId)'||v_EOL
					||' VALUES '
					||'('''||TRIM(cur_DR.ParentDB)||''''
					||','''||TRIM(cur_DR.ParentTable)||''''
					||','''||TRIM(cur_DR.ChildDB)||''''
					||','''||TRIM(cur_DR.ChildTable)||''''
					||',''RE'''
					||','||TRIM(CAST(cur_DR.DefId AS FORMAT'Z(9)9'))
					||','''||TRIM(cur_DR.DefOpt)||''''
					||','''||TRIM(cur_DR.DefName)||''''
					||','''||TRIM(v_ParentCols)||''''
					||','''||TRIM(v_ChildCols)||''''
					||',CURRENT_TIMESTAMP(3),SESSION);'
					) USING UNICODE_TO_LATIN WITH ERROR);
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,'"'||TRIM(:cur_TL.DatabaseName)||'"."'||TRIM(:cur_TL.TableName)||'"'
						,'"'||TRIM(:cur_DR.ChildDB)||'"."'||TRIM(:cur_DR.ChildTable)||'"'
						,USER,SESSION,:v_SQLsave
					);
				END IF;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					CALL DBC.SysExecSQL(:v_SQLsave);
				END IF;
			END IF;


			IF p_Operation IN ('DE','DR')
			AND cur_DR.x_LastCol = 1
			THEN	-- DE-Reference
				SET v_SQLalter=TRANSLATE((CAST('ALTER TABLE ' AS CHAR(12))
					||TRIM(cur_DR.ChildDB)||v_P||TRIM(cur_DR.ChildTable)||v_EOL
					||CAST(' DROP ' AS CHAR(6))
					||TRIM(CAST((CASE WHEN COALESCE(cur_DR.DefName,'') <> ''
						THEN 'CONSTRAINT ' || TRIM(COALESCE(cur_DR.DefName,''))||v_EOL
						ELSE ''
						END) AS VARCHAR(45)))
					||TRIM(CAST((CASE WHEN COALESCE(cur_DR.DefName,'') = ''
						THEN 'FOREIGN KEY (' || TRIM(v_ChildCols)||')'
							||' REFERENCES '||TRIM(cur_DR.ParentDB)||'.'||TRIM(cur_DR.ParentTable)
							||' ('||TRIM(v_ParentCols)||')'
							||v_EOL
						ELSE ''
						END) AS VARCHAR(4100)))
					||CAST(';' AS CHAR(1))||v_EOL
					) USING UNICODE_TO_LATIN WITH ERROR);

				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,'"'||TRIM(:cur_TL.DatabaseName)||'"."'||TRIM(:cur_TL.TableName)||'"'
						,'"'||TRIM(:cur_DR.ChildDB)||'"."'||TRIM(:cur_DR.ChildTable)||'"'
						,USER,SESSION,:v_SQLalter
					);
				END IF;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					/*B1: BEGIN
						-- ignore errors
						DECLARE CONTINUE HANDLER
						FOR SQLEXCEPTION, SQLWARNING
						SET v_SQLstate=SQLSTATE
						;
						CALL DBC.SysExecSQL(:v_SQLalter)
						;
					END B1;*/
					CALL DBC.SysExecSQL(:v_SQLalter)
					;
				END IF;
			END IF;

		END FOR F_DR;
	END IF;
END FOR F_TL;
END P0;
