/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DBA_ReDeTrigger.btq 5671 2012-09-13 18:24:09Z k9106728 $
# Last Changed By  : $Author: k9106728 $
# Last Change Date : $Date: 2012-09-13 20:24:09 +0200 (tor, 13 sep 2012) $
# Last Revision    : $Revision: 5671 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/procedure/DBA_ReDeTrigger.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Restores/Removes Triggers on specified table(s)
# Project     : EDW1
# Subproject  :
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2011-11-28 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#   Restores/Removes Triggers on specified table(s)
#   If "p_TrgTab" is NULL then all tables in spec database are targeted
#   If "p_TrgTab" parameter ends with a "*" a simple pattern-match is performed
#
# Dependencies
#   ${DB_ENV}dbadmin.DBA_ReDeDefData (Table)
#   ${DB_ENV}dbadmin.DBA_DebugLogData (Table)
#	SYSLIB.REPLACES UDF
#
# Parameters
#	Required Parameters
#	 p_TrgDb	- Target database containing table(s) to de/re-trigger
#	 p_Operation	- Operation type (DE,RE,DR or SV)
#	 		DE = De-Trigger(save Trigger-def & drop)
#	 		RE = Re-Trigger(re-create from saved Trigger-def)
#	 		DR = Drop Trigger (no save)
#	 		SV = Save Trigger definition (no drop)
#	Optional parameters:
#	 p_TrgTab	- Table to de/re-trigger.
#	 p_Options	- Options
#	 		E = Executes additional "ALTER TRIGGER ... ENABLED" statement
#	 		D = Executes additional "ALTER TRIGGER ... DISABLED" statement
#	 p_Debug	- If "1" then insert generated sql into the metadata Debug-Log
#		If "99" no exec, only insert generated sql into DBA_DebugLogData table
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}dbadmin.DBA_ReDeTrigger
(	p_TrgDb	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_TrgTab	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Operation	CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Options	VARCHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Debug	BYTEINT
)
P0: BEGIN
/*****************************************************************************
Required Parameters:
 p_TrgDb	- Target database containing table(s) to de/re-trigger
 p_Operation	- Operation type (DE,RE,DR or SV)
 		DE = De-Trigger(save Trigger-def & drop)
 		RE = Re-Trigger(re-create from saved Trigger-def)
 		DR = Drop Trigger (no save)
 		SV = Save Trigger definition (no drop)
Optional parameters:
 p_TrgTab	- Table to de/re-trigger.
 p_Options	- Options
 		E = Executes additional "ALTER TRIGGER ... ENABLED" statement
 		D = Executes additional "ALTER TRIGGER ... DISABLED" statement
 p_Debug	- If "1" then insert generated sql into the metadata Debug-Log
		If "99" no exec, only insert generated sql into DBA_DebugLogData table

Description:	Restores/Removes Triggers on specified table(s)
	If "p_TrgTab" is NULL then all tables in spec database are targeted
	If "p_TrgTab" parameter ends with a "*" a simple pattern-match is performed

Requires:	Table "DBA_ReDeDefData" for saved trigger-definitions
*****************************************************************************/
DECLARE v_SQLstate	CHAR(5);
DECLARE v_PrcName	VARCHAR(65) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_PrcDttm	TIMESTAMP(3);
DECLARE v_EOL		CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Blank	CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_TrgObj	VARCHAR(65) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_OthrObj	VARCHAR(65) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_TrgTab	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Match	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_DelSQL	VARCHAR(250) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_DropSQL	VARCHAR(250) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_InsSQL	VARCHAR(32000) CHARACTER SET LATIN NOT CASESPECIFIC;

SET v_EOL=TRANSLATE('0D'xc USING UNICODE_TO_LATIN WITH ERROR);
SET v_Blank=TRANSLATE('' USING UNICODE_TO_LATIN WITH ERROR);
SET v_PrcName = '${DB_ENV}dbadmin.DBA_ReDeTrigger';
SET v_PrcDttm = CURRENT_TIMESTAMP(3);

-- Verify parameters
IF COALESCE(p_TrgDb,'')=''
OR POSITION('${DB_ENV}' IN COALESCE(p_TrgDb,''))<>1
OR COALESCE(p_Operation,'') NOT IN ('DE','RE','DR','SV')
THEN
	LEAVE P0;
END IF
;
IF POSITION('E' IN COALESCE(p_Options,''))=0
AND POSITION('D' IN COALESCE(p_Options,''))=0
AND TRIM(COALESCE(p_Options,''))<>''
THEN
	LEAVE P0;
END IF
;

SET v_TrgTab = TRANSLATE(TRIM(COALESCE(p_TrgTab,v_Blank)) USING UNICODE_TO_LATIN WITH ERROR)
;
IF POSITION('*' IN v_TrgTab) > 0
THEN
	SET v_Match = TRIM(SUBSTRING(v_TrgTab FROM 1 FOR POSITION('*' IN v_TrgTab)-1))
	;
ELSE
	SET v_Match = NULL
	;
END IF;

IF p_Operation='RE'
THEN	-- Get saved definitions and Re-Trigger
	F_RTT: FOR cur_RTT AS cur_ReTriggerTab CURSOR FOR
		LOCKING ROW FOR ACCESS
		SELECT rd0.TrgDbName,rd0.TrgTabName
		FROM ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
		WHERE	rd0.TrgDbName	= :p_TrgDb
		AND	(CASE
			WHEN :p_TrgTab IS NULL
			THEN (CASE WHEN POSITION('#' IN rd0.TrgTabName)=1 THEN 0 ELSE 1 END)
			WHEN COALESCE(:v_Match,'***') = ''
			THEN 1
			WHEN :v_Match IS NOT NULL
			THEN (CASE WHEN POSITION(:v_Match IN rd0.TrgTabName)=1 THEN 1
				ELSE 0 END)
			WHEN TRIM(rd0.TrgTabName) = TRIM(:p_TrgTab)
			THEN 1
			ELSE 0
			END) = 1
		AND	rd0.DefClass = 'G' --trigger def
		GROUP BY 1,2
		ORDER BY 1,2
	DO
		F_RT: FOR cur_RT AS cur_ReTrigger CURSOR FOR
			LOCKING ROW FOR ACCESS
			SELECT	rd0.TrgDbName,rd0.TrgTabName,rd0.DefName,rd0.OthrDbName,rd0.OthrTabName
				,rd0.DefSQL	AS x_CreSQL
				,(	'DELETE FROM ${DB_ENV}dbadmin.DBA_ReDeDefData'||:v_EOL
					||' WHERE TrgDbName='''||TRIM(rd0.TrgDbName)||''''||:v_EOL
					||' AND TrgTabName='''||TRIM(rd0.TrgTabName)||''''||:v_EOL
					||' AND OthrDbName='''||TRIM(rd0.OthrDbName)||''''||:v_EOL
					||' AND OthrTabName='''||TRIM(rd0.OthrTabName)||''''||:v_EOL
					||' AND DefClass = ''G'''||:v_EOL
					||';'
					)	(VARCHAR(250))	AS x_DelSQL
				,(	'ALTER TRIGGER '||TRIM(rd0.OthrDbName)||'.'||TRIM(rd0.OthrTabName)
					||(CASE WHEN TRIM(COALESCE(:p_Options,''))='D'
						THEN ' DISABLED'
						WHEN TRIM(COALESCE(:p_Options,''))='E'
						THEN ' ENABLED'
						ELSE NULL
						END)
					||';'
					)	(VARCHAR(250))	AS x_AlterSQL
			FROM ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
			WHERE	rd0.TrgDbName	= :cur_RTT.TrgDbName
			AND	rd0.TrgTabName	= :cur_RTT.TrgTabName
			AND	rd0.DefClass = 'G' --index def
			ORDER BY rd0.TrgDbName,rd0.TrgTabName,rd0.DefName
		DO
			SET v_TrgObj='"'||TRIM(cur_RT.TrgDbName)
				||'"."'||TRIM(cur_RT.TrgTabName)||'"'
			;
			SET v_OthrObj=CASE WHEN POSITION('.' IN COALESCE(cur_RT.DefName,''))>0
				THEN '"'||TRIM(cur_RT.OthrDbName)
				||'"."'||TRIM(cur_RT.OthrTabName)||'"'
				ELSE '"'||TRIM(cur_RT.DefName)||'"'
				END
			;
			IF cur_RT.x_CreSQL IS NOT NULL
			THEN	-- Create (Restore) trigger
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:v_OthrObj
						,USER,SESSION
						,:cur_RT.x_CreSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					B1: BEGIN
						-- ignore errors
						DECLARE CONTINUE HANDLER
						FOR SQLEXCEPTION, SQLWARNING
						SET v_sqlstate=SQLSTATE
						;
						CALL DBC.SysExecSQL(:cur_RT.x_CreSQL);
					END B1;
				END IF
				;
			END IF
			;
			IF cur_RT.x_AlterSQL IS NOT NULL
			AND TRIM(COALESCE(p_Options,'')) IN ('D','E')
			THEN	-- Alter trigger to enabled/disabled
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:v_OthrObj
						,USER,SESSION
						,:cur_RT.x_AlterSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					B1: BEGIN
						-- ignore errors
						DECLARE CONTINUE HANDLER
						FOR SQLEXCEPTION, SQLWARNING
						SET v_sqlstate=SQLSTATE
						;
						CALL DBC.SysExecSQL(:cur_RT.x_AlterSQL);
					END B1;
				END IF
				;
			END IF
			;
			IF cur_RT.x_DelSQL IS NOT NULL
			THEN	-- Remove saved definition
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:v_OthrObj
						,USER,SESSION
						,:cur_RT.x_DelSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					CALL DBC.SysExecSQL(:cur_RT.x_DelSQL);
				END IF
				;
			END IF;
		END FOR F_RT;
	END FOR F_RTT;
END IF;

IF p_Operation='DE'
THEN	-- Create definitions and De-Trigger

	F_DTT: FOR cur_DTT AS cur_DeTriggerTab CURSOR FOR
		LOCKING ROW FOR ACCESS
		SELECT	t0.DatabaseName	AS TrgDbName
			,t0.TableName	AS TrgTabName
			,MAX(CASE WHEN g0.SubjectTableDataBaseName IS NOT NULL THEN 1 ELSE 0 END) AS x_TriggerExist
			,MAX(CASE WHEN rd0.TrgDbName IS NOT NULL THEN 1 ELSE 0 END) AS x_DelOld
		FROM DBC.Tables AS t0
		LEFT OUTER JOIN DBC.Triggers  AS g0
		ON g0.SubjectTableDataBaseName = t0.DatabaseName
		AND g0.TableName = t0.TableName
		LEFT OUTER JOIN ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
		ON rd0.TrgDbName = t0.DatabaseName
		AND rd0.TrgTabName = t0.TableName
		AND rd0.DefClass = 'G' --index def
		WHERE	t0.DatabaseName = :p_TrgDb
		AND	(CASE
			WHEN :p_TrgTab IS NULL
			THEN (CASE WHEN POSITION('#' IN t0.TableName)=1 THEN 0 ELSE 1 END)
			WHEN COALESCE(:v_Match,'***') = ''
			THEN 1
			WHEN :v_Match IS NOT NULL
			THEN (CASE WHEN POSITION(:v_Match IN t0.TableName)=1 THEN 1
				ELSE 0 END)
			WHEN TRIM(t0.TableName) = TRIM(:p_TrgTab)
			THEN 1
			ELSE 0
			END) = 1
		AND	(	g0.SubjectTableDataBaseName IS NOT NULL
			OR	rd0.TrgDbName IS NOT NULL
			)
		GROUP BY 1,2
		ORDER BY 1,2
	DO
		SET v_TrgObj=TRANSLATE(('"'||TRIM(cur_DTT.TrgDbName)
			||'"."'||TRIM(cur_DTT.TrgTabName)||'"'
			) USING UNICODE_TO_LATIN WITH ERROR )
		;
		IF cur_DTT.x_DelOld = 1
		THEN --remove all old saved trigger definitions for table
			SET v_DelSQL=TRANSLATE((
				'DELETE FROM ${DB_ENV}dbadmin.DBA_ReDeDefData'
				||' WHERE TrgDbName='''||TRIM(cur_DTT.TrgDbName)||''''||v_EOL
				||' AND TrgTabName='''||TRIM(cur_DTT.TrgTabName)||''''||v_EOL
				||' AND DefClass = ''G'''||v_EOL
				||';'
				) USING UNICODE_TO_LATIN WITH ERROR)
			;
			IF ZEROIFNULL(p_Debug)<>0
			THEN	-- Insert debug-text
				INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
				(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
				) VALUES (
					:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
					,:v_TrgObj,:v_TrgObj
					,USER,SESSION
					,:v_DelSQL
				);
			END IF
			;
			IF ZEROIFNULL(p_Debug)<>99
			THEN
				CALL DBC.SysExecSQL(:v_DelSQL);
			END IF;
		END IF
		;
		IF cur_DTT.x_TriggerExist = 1
		THEN -- Get trigger definition
			F_DT: FOR cur_DT AS cur_DeTrigger CURSOR FOR
				LOCKING ROW FOR ACCESS
				SELECT	g2.OthrDbName
					,g2.OthrTabName
					,g2.DefType
					,g2.DefOpt
					,g2.DefName
					,SYSLIB.REPLACES(SUBSTRING(g2.x_CreateText FROM 1 FOR 30000), '''', '''''')
						(VARCHAR(31000))	AS CreateText1
					,SYSLIB.REPLACES(SUBSTRING(g2.x_CreateText FROM 30001), '''', '''''')
						(VARCHAR(31000))	AS CreateText2
				FROM(
					SELECT	g1.OthrDbName
						,g1.OthrTabName
						,g1.DefType
						,g1.DefOpt
						,TRIM(g1.OthrDbName)||'.'||TRIM(g1.OthrTabName)	AS DefName
						,TRANSLATE(TRIM(COALESCE(tt2.TextString,g1.x_CreateText)
							) USING UNICODE_TO_LATIN WITH ERROR) AS x_CreateText
					FROM (
						SELECT g0.TriggerId
							,CASE WHEN ZEROIFNULL(tt1.x_LineNoCT)=1
								THEN 'C'
								WHEN ZEROIFNULL(tt1.x_LineNoRT)=1
								THEN 'R'
								ELSE NULL
								END	AS x_TextType
							,TRANSLATE(d1.DataBaseName USING UNICODE_TO_LATIN WITH ERROR) AS OthrDbName
							,TRANSLATE(g0.TriggerName USING UNICODE_TO_LATIN WITH ERROR) AS OthrTabName
							,g0.Kind	AS DefType
							,g0.TriggerEnabled	AS DefOpt
							,TRANSLATE(TRIM(CASE
								WHEN COALESCE(g0.CreateTxtOverflow,'')<>'C'
								THEN g0.CreateText
								WHEN COALESCE(t1.RequestTxtOverflow,'')<>'R'
								THEN t1.RequestText
								ELSE NULL
								END) USING UNICODE_TO_LATIN WITH ERROR
									)	(VARCHAR(13000)) AS x_CreateText
						FROM DBC.TVM AS t0
						INNER JOIN DBC.Dbase AS d0
						ON d0.DatabaseId = t0.DatabaseId
						INNER JOIN DBC.TriggersTbl AS g0
						ON g0.TableId = t0.TVMId
						INNER JOIN DBC.Dbase AS d1
						ON d1.DatabaseId = g0.DatabaseId
						INNER JOIN DBC.TVM AS t1
						ON t1.TVMId = g0.TriggerId
						LEFT OUTER JOIN (
							SELECT	tt0.TextID
								,MAX(CASE WHEN tt0.TextType = 'C'
									AND CHARACTER_LENGTH(TRIM(tt0.TextString))>31000
									THEN CHARACTER_LENGTH(TRIM(tt0.TextString))
									WHEN tt0.TextType = 'C'
									THEN tt0.LineNo
									ELSE 0 END)	AS x_LineNoCT
								,MAX(CASE WHEN tt0.TextType = 'R'
									AND CHARACTER_LENGTH(TRIM(tt0.TextString))>31541
									THEN CHARACTER_LENGTH(TRIM(tt0.TextString))
									WHEN tt0.TextType = 'R'
									THEN tt0.LineNo
									ELSE 0 END)	AS x_LineNoRT
							FROM DBC.TextTbl AS tt0
							GROUP BY 1
						) AS tt1
						ON tt1.TextID = g0.TriggerId
						WHERE d0.DatabaseName = :cur_DTT.TrgDbName
						AND t0.TVMName = :cur_DTT.TrgTabName
					) AS g1
					LEFT OUTER JOIN DBC.TextTbl AS tt2
					ON tt2.TextID = g1.TriggerId
					AND tt2.TextType = g1.x_TextType
					AND tt2.LineNo = 1
				) AS g2
				ORDER BY g2.OthrDbName,g2.OthrTabName
			DO
				SET v_DropSQL=TRANSLATE(('DROP TRIGGER '
					||TRIM(cur_DT.OthrDbName)||'.'||TRIM(cur_DT.OthrTabName)
					||';'
					) USING UNICODE_TO_LATIN WITH ERROR)
				;
				SET v_InsSQL=(
					TRANSLATE(TRIM(CAST(('INSERT INTO ${DB_ENV}dbadmin.DBA_ReDeDefData'||v_EOL
						||'(TrgDbName,TrgTabName,OthrDbName,OthrTabName,DefClass,DefType,DefOpt,DefName,DefSQL,SaveDttm,SessionId)'||v_EOL
						) AS VARCHAR(160))) USING UNICODE_TO_LATIN WITH ERROR)
					||TRANSLATE(CAST('VALUES'||v_EOL AS VARCHAR(7)) USING UNICODE_TO_LATIN WITH ERROR)
					||TRANSLATE(TRIM(CAST((
						'('''||TRIM(cur_DTT.TrgDbName)||''''
						||','''||TRIM(cur_DTT.TrgTabName)||''''
						||v_EOL
						||(CASE WHEN cur_DT.OthrDbName IS NOT NULL
							THEN ','''||TRIM(cur_DT.OthrDbName)||''''
							ELSE ',NULL'
							END)
						||(CASE WHEN cur_DT.OthrTabName IS NOT NULL
							THEN ','''||TRIM(cur_DT.OthrTabName)||''''
							ELSE ',NULL'
							END)
						||v_EOL
						||',''G'''
						||','''||TRIM(cur_DT.DefType)||''''
						||','''||TRIM(cur_DT.DefOpt)||''''
						||(CASE WHEN cur_DT.DefName IS NOT NULL
							THEN ','''||TRIM(cur_DT.DefName)||''''
							ELSE ',NULL'
							END)
						||v_EOL
						) AS VARCHAR(220))) USING UNICODE_TO_LATIN WITH ERROR)
					||TRIM(CASE WHEN TRIM(cur_DT.CreateText2)<>''
						THEN TRANSLATE(',TRANSLATE(''' USING UNICODE_TO_LATIN WITH ERROR)
							||cur_DT.CreateText1
							||TRANSLATE(''' USING UNICODE_TO_LATIN WITH ERROR)' USING UNICODE_TO_LATIN WITH ERROR)
							||v_EOL
						ELSE TRANSLATE(',''' USING UNICODE_TO_LATIN WITH ERROR)
							||TRIM(cur_DT.CreateText1)
							||TRANSLATE('''' USING UNICODE_TO_LATIN WITH ERROR)
							||v_EOL
						END)
					||TRIM(CASE WHEN TRIM(cur_DT.CreateText2)<>''
						THEN TRANSLATE('||TRANSLATE(''' USING UNICODE_TO_LATIN WITH ERROR)
							||cur_DT.CreateText2
							||TRANSLATE(''' USING UNICODE_TO_LATIN WITH ERROR)' USING UNICODE_TO_LATIN WITH ERROR)
							||v_EOL
						ELSE ''
						END)
					||TRANSLATE(TRIM(CAST((
						',CURRENT_TIMESTAMP(3),SESSION);'
						) AS VARCHAR(23))) USING UNICODE_TO_LATIN WITH ERROR)
					)
				;
				IF p_Operation IN ('DE','SV')
				THEN	-- Save definition
					IF ZEROIFNULL(p_Debug)<>0
					THEN	-- Insert debug-text
						INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
						(ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
						) VALUES (
							:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
							,:v_TrgObj,:cur_DT.DefName
							,USER,SESSION
							,:v_InsSQL
						);
					END IF
					;
					IF ZEROIFNULL(p_Debug)<>99
					THEN
						CALL DBC.SysExecSQL(:v_InsSQL);
					END IF;
				END IF
				;
				IF p_Operation IN ('DE','DR')
				AND v_DropSQL IS NOT NULL
				THEN	-- DE-Index
					IF ZEROIFNULL(p_Debug)<>0
					THEN	-- Insert debug-text
						INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
						(ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
						) VALUES (
							:v_PrcName,:v_PrcDttm, CURRENT_TIMESTAMP(3)
							,:v_TrgObj,:cur_DT.DefName
							,USER,SESSION
							,:v_DropSQL
						);
					END IF
					;
					IF ZEROIFNULL(p_Debug)<>99
					THEN
						B1: BEGIN
							-- ignore errors
							DECLARE CONTINUE HANDLER
							FOR SQLEXCEPTION, SQLWARNING
							SET v_sqlstate=SQLSTATE
							;
							CALL DBC.SysExecSQL(:v_DropSQL)
							;
						END B1;
					END IF;
				END IF;
			END FOR F_DT;
		END IF;
	END FOR F_DTT;
END IF;
END P0
;
