/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DBA_UpdPartitions.btq 33181 2020-08-12 12:43:37Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2020-08-12 14:43:37 +0200 (ons, 12 aug 2020) $
# Last Revision    : $Revision: 33181 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/procedure/DBA_UpdPartitions.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2015-05-04 A20841    Initial version
# --------------------------------------------------------------------------
# Description
#   Runs the "ALTER TABLE db.tbl TO CURRENT" (with or without "WITH DELETE").
#   If there are Secondary indexes or hash indexes these will be dropped before the
#   operation and recreated afterwards using the existing procedure DBA_ReDeIndex.
#
# Dependencies
#   ${DB_ENV}dbadmin.DBA_ReDeIndex (Procedure)
#   ${DB_ENV}dbadmin.DBA_NewTabDefData (Table)
#   ${DB_ENV}dbadmin.DBA_DebugLogData (Table)
#
# Parameters
#	Required Parameters:
#	 p_TrgDbName	- Target database
#	 p_TrgTabName	- Target table
#	Optional parameters
#    p_WDelete  - If "1" use "with delete", if other run command without this option
#	 p_Debug	- If "1" insert generated sql into DBA_DebugLogData table
#			      If "99" no exec, only insert generated sql into DBA_DebugLogData table
# --------------------------------------------------------------------------
*/
REPLACE PROCEDURE ${DB_ENV}dbadmin.DBA_UpdPartitions
( 
   p_TrgDbName VARCHAR(256)
  ,p_TrgTabName VARCHAR(256)
  ,p_wdelete INTEGER
  ,p_Debug BYTEINT
)
P0: BEGIN
/***********************************************************************************************************
Required parameters:
 p_TrgDbName  - Target database
 p_TrgTabName - Target table
Optionial parameters:
 p_WDelete - If 1 use "with delete", if 0 or none to run command without this option
 p_Debug   - If "1" insert generated sql into DBA_DebugLogData table
             If "99" no exec, only insert generated sql into DBA_DebugLogData table
************************************************************************************************************/

DECLARE v_DbAdmin VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_PrcName VARCHAR(61) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_PrcDttm TIMESTAMP(3);
DECLARE v_StmtDttm TIMESTAMP(3);
DECLARE v_StmtTxt VARCHAR(512);
DECLARE v_SQLstate VARCHAR(5);
DECLARE v_SQLcode SMALLINT;
DECLARE v_SQLmsg VARCHAR(510);
DECLARE v_StopRun BYTEINT;
DECLARE v_WeekPartitioning BYTEINT;
DECLARE v_CalculatedStartDate BYTEINT;
DECLARE v_ResolvedCurrentDate DATE;
DECLARE v_FetchNewDateSQL VARCHAR(1000) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_FetchOldDateSQL VARCHAR(1000) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_ResolvedCurrentDateReplace VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_StartDateOld VARCHAR(10);
DECLARE v_StartDateNew VARCHAR(10);
DECLARE v_StartDateOldDoW SMALLINT;
DECLARE v_StartDateNewDoW SMALLINT;
DECLARE v_Status VARCHAR(128) CHARACTER SET LATIN NOT CASESPECIFIC;




/*****************************************************************************
Capture error-state in debug-logg
*****************************************************************************/
DECLARE CONTINUE HANDLER FOR SQLSTATE '42000'
H1: BEGIN
 SET v_SQLstate=SQLSTATE;
 SET v_SQLcode=SQLCODE;
 --COMMIT WORK;

END H1;

DECLARE EXIT HANDLER
FOR SQLEXCEPTION, SQLWARNING
H0: BEGIN
 SET v_SQLstate=SQLSTATE;
 SET v_SQLcode=SQLCODE;
 
 SELECT ErrorText
 INTO :v_SQLmsg
 FROM DBC.ErrorMsgs
 WHERE ErrorCode=:v_SQLcode
 ;

 --UPDATE ${DB_ENV}dbadmin.DBA_DebugLogData
 --SET Status = 'SQLSTATE: '||TRIM(:v_SQLstate)||' '||TRIM(:v_SQLmsg)
 --WHERE ProcedureName = :v_PrcName
 --AND ProcedureDttm = :v_PrcDttm
 --AND StatementDttm = :v_StmtDttm
 --AND SessionId = SESSION
 --;
 
 MERGE INTO ${DB_ENV}dbadmin.DBA_DebugLogData
 USING (select :v_PrcName AS tbl_PrcName, :v_PrcDttm AS tbl_PrcDttm, :v_StmtDttm AS tbl_StmtDttm, SESSION AS tbl_Session) as tbl_input
 ON ${DB_ENV}dbadmin.DBA_DebugLogData.ProcedureName = tbl_input.tbl_PrcName
 AND ${DB_ENV}dbadmin.DBA_DebugLogData.ProcedureDttm = tbl_input.tbl_PrcDttm
 AND ${DB_ENV}dbadmin.DBA_DebugLogData.StatementDttm = tbl_input.tbl_StmtDttm
 AND ${DB_ENV}dbadmin.DBA_DebugLogData.SessionId = tbl_input.tbl_Session
 WHEN MATCHED THEN UPDATE SET Status = 'SQLSTATE: '|| TRIM(COALESCE(:v_SQLstate,'NoSQLState'))||' '||TRIM(COALESCE(:v_SQLmsg,'NoSQLMsg'))
 WHEN NOT MATCHED THEN INSERT (ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt,Status
    ) VALUES (
      COALESCE(:v_PrcName,'')
	 ,:v_PrcDttm
	 ,:v_StmtDttm
     ,COALESCE(:p_TrgDbName,'') || '.' || COALESCE(:p_TrgTabName,'')
	 ,COALESCE(:p_TrgDbName,'') || '.' || COALESCE(:p_TrgTabName,'')
     ,USER
	 ,SESSION
     ,COALESCE(:v_StmtTxt,'')
	 ,'SQLSTATE: '|| TRIM(COALESCE(:v_SQLstate,'NoSQLState'))||' '||TRIM(COALESCE(:v_SQLmsg,'NoSQLMsg'))
    )
	;
 --COMMIT WORK;
 
 RESIGNAL
 ;
END H0;


SET v_Status = '';
SET v_DbAdmin = '${DB_ENV}dbadmin';
SET v_PrcName = TRIM(v_DbAdmin)||'.DBA_UpdPartitions';
SET v_PrcDttm = CURRENT_TIMESTAMP(3);
SET v_StopRun=0;
SET v_StartDateOld = '';
SET v_StartDateNew = '';

-- Drop volatile table if exists
--SET v_StmtTxt='CALL ' || TRIM(v_DbAdmin) || '.TryDropObject(''volatile_UpdPart'',1);';
SET v_StmtDttm= CURRENT_TIMESTAMP(3);	
SET v_StmtTxt='DROP TABLE volatile_UpdPart;';
CALL DBC.SysExecSQL(:v_StmtTxt);
SET v_StmtTxt='';


-- Check if hardcoded start in partitioning-expression
SELECT
  CASE
    WHEN POSITION('20' IN constrainttext) = 0 THEN 0
    WHEN POSITION('20' IN constrainttext) < POSITION('ADD_MONTHS' IN constrainttext) THEN 1
    ELSE 0
  END AS CaseCheck
INTO
  :v_CalculatedStartDate
FROM
  dbc.indexconstraintsv
WHERE
  databasename = :p_TrgDbName
  AND tablename = :p_TrgTabName
  AND ConstraintType = 'Q'
;

LOCKING ROW FOR ACCESS
SELECT
icv.ResolvedCurrent_Date,
CASE WHEN POSITION('EACH INTERVAL ''7'' DAY' IN icv.constrainttext)>0 THEN 1 ELSE 0 END AS WeekPartitioning
INTO
	:v_ResolvedCurrentDate,
	:v_WeekPartitioning
FROM
	dbc.indexconstraintsv icv
WHERE
	databasename = :p_TrgDbName
	AND tablename = :p_TrgTabName
    AND ConstraintType = 'Q'
;

-- If hardcoded start-date skip all checks
-- If calculated start-date then do all checks
IF v_CalculatedStartDate = 1
	THEN
	IF EXTRACT(YEAR FROM v_ResolvedCurrentDate) || EXTRACT(MONTH FROM v_ResolvedCurrentDate) = EXTRACT(YEAR FROM CURRENT_DATE) || EXTRACT(MONTH FROM CURRENT_DATE)
		THEN
			SET v_StopRun=3;
			SET v_StmtTxt = 'Table already updated this month, will be skipped';
	END IF;
		
ELSE
		SET v_ResolvedCurrentDateReplace = '(DATE ''' || v_ResolvedCurrentDate || ''')'
		;
		


		LOCKING ROW FOR ACCESS
		SELECT
		  FetchNewDateSQL,
		  FetchOldDateSQL
		INTO
		  :v_FetchNewDateSQL,
		  :v_FetchOldDateSQL
		FROM
		( 
		SELECT 
		  POSITION('BETWEEN' IN constrainttext)+8 AS StartPos,
		  POSITION('AND' IN constrainttext) AS EndPos,
		  'INSERT INTO volatile_UpdPart SELECT ''New'',' || SUBSTR(constrainttext, StartPos, EndPos-StartPos) (VARCHAR(10000)) AS FetchNewDateSQL,
		  'INSERT INTO volatile_UpdPart SELECT ''Old'',' ||
				OREPLACE(
					OREPLACE(
						OREPLACE(
							OREPLACE(
								OREPLACE(
									OREPLACE(
										OREPLACE(
											OREPLACE(
												OREPLACE(
													OREPLACE(
														OREPLACE(
															OREPLACE(
																SUBSTR(
																	constrainttext
																	,StartPos
																	,EndPos-StartPos
																)
																,'DATE '''
																,'@@XX@@ '''
															)
															,'(10106 (DATE))'
															,'@@YY@@'
														)
														,'0 )))))(DATE))'
														,'@@TT@@'
													)
													,'( TITLE ''Day''))(DATE))'
													,'@@ZZ@@'
												)
												,')(DATE)'
												,'@@SS@@'
											)
											,'DATE'
											,'DATE ''' || :v_ResolvedCurrentDate || ''''
										)
										,'CURRENT_DATE'
										,'DATE ''' || :v_ResolvedCurrentDate || ''''
									)
									,'@@XX@@ '''
									,'DATE '''
								)
								,'@@YY@@'
								,'(10106 (DATE))'
							)
							,'@@ZZ@@'
							,'( TITLE ''Day''))(DATE))'
						)
						,'@@TT@@'
						,'0 )))))(DATE))'
					)
					,'@@SS@@'
					,')(DATE)'
				)
				(VARCHAR(10000))  AS FetchOldDateSQL

			from
			  dbc.indexconstraintsv
			where 
				Databasename = :p_TrgDbName
			AND Tablename = :p_TrgTabName
			AND	ConstraintType = 'Q'
			AND constrainttext LIKE '%BETWEEN%'
			AND StartPos > 0
			AND EndPos > 0
			AND StartPos < EndPos
		) AS Tbl1
		;


		CREATE VOLATILE TABLE volatile_UpdPart
		(
			WhichOne CHAR(3),
			StartDate VARCHAR(10)
		)
		PRIMARY INDEX (WhichOne)
		ON COMMIT PRESERVE ROWS
		;
			
		CALL DBC.SysExecSQL(:v_FetchNewDateSQL);
		CALL DBC.SysExecSQL(:v_FetchOldDateSQL);

		LOCKING ROW FOR ACCESS
		SELECT
		  volatile_UpdPart.StartDate,
		  cal_New.Day_of_week
		INTO
		  :v_StartDateNew,
		  :v_StartDateNewDoW
		FROM
		  volatile_UpdPart JOIN
		  Sys_Calendar.CALENDAR AS cal_New on volatile_UpdPart.StartDate=cal_New.calendar_date
		where 
		  volatile_UpdPart.WhichOne='New'
		;

		LOCKING ROW FOR ACCESS
		SELECT
		  volatile_UpdPart.StartDate,
		  cal_New.Day_of_week
		INTO
		  :v_StartDateOld,
		  :v_StartDateOldDoW
		FROM
		  volatile_UpdPart JOIN
		  Sys_Calendar.CALENDAR AS cal_New on volatile_UpdPart.StartDate=cal_New.calendar_date
		where 
		  volatile_UpdPart.WhichOne='Old'
		;

		DROP TABLE volatile_UpdPart
		;

		-- x Blir det skillnad i start?
		IF v_StartDateOld = v_StartDateNew
			THEN
				SET v_StmtTxt = 'Will have the same startdate, will not update the table';
				SET v_StopRun=1;
			ELSE
		--   x Är det vecko-partition?
			IF v_WeekPartitioning = 1
			THEN
		--     x Har vecko-start-dag ändrats?
				IF v_StartDateNewDoW <> v_StartDateOldDoW
				THEN
					SET v_StmtTxt = 'Will have a different start-day-of-week, partitioning-expression is wrong, will not update the table. To quick-fix the table use NewTabDef-procedure to recreate table. To fix issue correct the partitioning-expression.';
					SET v_StopRun=2;
					SET v_Status='Table needs to be handled manually';
				END IF;
			END IF;		
		END IF;
END IF;

SET v_StmtTxt = COALESCE(v_StmtTxt,'') || ' -- ALL INFO:, v_StopRun=' || COALESCE(v_StopRun,'x') || ',v_WeekPartitioning=' || v_WeekPartitioning || ',v_ResolvedCurrentDate=' || COALESCE(v_ResolvedCurrentDate, DATE '1900-01-01') || ',v_StartDateOld=' || COALESCE(v_StartDateOld, '1900-01-01') || ',v_StartDateNew=' || COALESCE(v_StartDateNew, '1900-01-01') || ',v_StartDateOldDoW=' || COALESCE(v_StartDateOldDoW,0) || ',v_StartDateNewDoW=' || COALESCE(v_StartDateNewDoW,0) || '.x.';


IF ZEROIFNULL(v_StopRun)=0
THEN	
	IF COALESCE(p_WDelete,'') = 1
	THEN
		SET v_StmtTxt = 'ALTER TABLE ' || p_TrgDbName || '.' || p_TrgTabName || ' TO CURRENT WITH DELETE;';
	ELSE
		SET v_StmtTxt = 'ALTER TABLE ' || p_TrgDbName || '.' || p_TrgTabName || ' TO CURRENT;';
	END IF
	;
END IF
;


SET v_StmtDttm= CURRENT_TIMESTAMP(3);	
INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
  (ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt,Status
    ) VALUES (
      :v_PrcName
	 ,:v_PrcDttm
	 ,:v_StmtDttm
     ,:p_TrgDbName || '.' || :p_TrgTabName
	 ,:p_TrgDbName || '.' || :p_TrgTabName
     ,USER
	 ,SESSION
     ,:v_StmtTxt
	 ,:v_Status
    )
;
 
-- Execute if input-parameter p_Debug is other than 99
IF ZEROIFNULL(p_Debug)<>99
  THEN	
	IF ZEROIFNULL(v_StopRun)=0
	THEN
		CALL DBC.SysExecSQL(:v_StmtTxt);
	END IF
	;
END IF
;


END P0
;
