#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: archive_oas.sh 19479 2016-07-26 09:02:55Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2016-07-26 11:02:55 +0200 (tis, 26 jul 2016) $
# Last Revision    : $Revision: 19479 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/archive_oas.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

DB_ENVAchv="$DB_ENV"Achv
DB_ENVStgOAST="$DB_ENV"StgOAST
DB_ENVMetaDataVIN="$DB_ENV"MetaDataVIN

JobRunID=$2
echo "JobRunID=$JobRunID"

CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS=$CURRENT_TS"

exec >$PMRootDir/log/`basename $0`_"Arch_"$CURRENT_TS"_script".log 2>&1 

echo "writing to log"
echo "PMRootDir = $PMRootDir"
echo "DB_ENV = $DB_ENV"
echo "CURRENT_TS = $CURRENT_TS"
echo "JobRunID = $JobRunID"

echo "DB_ENVAchv = ${DB_ENV}Achv"
echo "DB_ENVStgOAST = ${DB_ENV}StgOAST"
echo "DB_ENVMetaDataVIN = ${DB_ENV}MetaDataVIN"


###################################################################################################
#  Print script syntax and help
###################################################################################################

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <JobRunID>"
    echo ""
    echo "   Base directory is   : PMRootDir"
    echo "   <JobRunID>         : JobRunID to archive"
    echo ""
    }


############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


############################################################################
# Script and log file directories                                          #
############################################################################

# Log Directory
LOG_DIR=$1/log
echo "LOG_DIR = " $LOG_DIR


# Bteq Log File
LOG_FILE=$LOG_DIR/`basename $0 .sh`_$JobRunID"_"$CURRENT_TS"_bteq".log
echo "LOG_FILE = " $LOG_FILE
#chmod 777 $LOG_FILE


# Bteq Directory
SCRIPT_DIR=$1/bin
echo "SCRIPT_DIR = " $SCRIPT_DIR


# Bteq File
#BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/bin/`basename $0 .sh`_$CURRENT_TS.bteq
BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/tmp/`basename $0 .sh`_$CURRENT_TS.bteq
echo "BTEQ_ARCHIVE_SOURCE_SCRIPT = " $BTEQ_ARCHIVE_SOURCE_SCRIPT 
>$BTEQ_ARCHIVE_SOURCE_SCRIPT
#chmod 777 $BTEQ_ARCHIVE_SOURCE_SCRIPT


############################################################################
# TO GET LOGON INFORMATION FOR BTEQ                                        #
############################################################################

PWD_FILE=$SCRIPT_DIR/PWD_BteqLogon
echo "PWD_FILE = " $PWD_FILE

LOGON=`grep $DB_ENV"_"LOGON $PWD_FILE `

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	return -1 
}

#LOGON_FILE=$SCRIPT_DIR/$DB_ENV"_"$JobRunID"_LOGON"
LOGON_FILE=$1/tmp/$DB_ENV"_"$JobRunID"_LOGON"
echo "LOGON_FILE = " $LOGON_FILE
#chmod 777 $LOGON_FILE

LOGON=`echo $LOGON | cut -d "=" -f2`

#Test condition to check if the file already exists

if [ -f $LOGON_FILE ]		
then
	#echo "Logon File Exists"
	EXISTING_LOGON=`head -1 $LOGON_FILE`
	
	if [[ $LOGON = $EXISTING_LOGON ]]
	then
	echo "Same credentials reusing existing file"
	else
	echo $LOGON > $LOGON_FILE
	fi
else 
	#file does not exist, use latest credentials
	echo $LOGON > $LOGON_FILE
fi
chmod 777 $LOGON_FILE

echo "
.SET SESSION TRANSACTION ANSI;

.RUN FILE = $LOGON_FILE" > $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=CreateArchiveTables;ClientUser=$LOGNAME;' FOR SESSION;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "commit;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

############################################################################
# BTEQ                                                                                     
############################################################################

echo "
-- ---------------------------------------------------------------------------------------------------------------------
-- The output may exceed the standard 80 characters, it is expanded to 5000
-- ---------------------------------------------------------------------------------------------------------------------
.width 5000
.set format on
.foldline off" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

start_time=`date`
echo "start_time = " $start_time > $LOG_FILE


echo "


------------------------------------------------------------------------------------------
-- 1) OAS Stores Load Archiving
------------------------------------------------------------------------------------------



INSERT INTO ${DB_ENV}Achv.Arc_OAS_Stores
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS 
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
	    ,XmlData   
        ,JobRunId      
)
SELECT

         Oas.SequenceId        
        ,Oas.SAPCustomerID  
        ,Oas.ContentType         
        ,Oas.SubContentType
		,Oas.TransactionTS 		
        ,Oas.ExtractionTS   
        ,Oas.IEReceivedTS   
        ,Oas.IEDeliveryTS   
        ,Oas.SourceVersion  
        ,Oas.TS             
        ,Oas.ArchiveKey
	    ,Oas.XmlData     
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_Stores Oas

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_StoresQ OasQ
        ON
        Oas.SequenceId   = OasQ.SequenceId
    AND Oas.TransactionTS    = OasQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_Store_Load_Cache OasCache
        ON
        Oas.SequenceId   = OasCache.SequenceId
    AND Oas.TransactionTS    = OasCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_Stores Stores1
        ON
        Oas.ArchiveKey = Stores1.ArchiveKey

LEFT OUTER JOIN
       ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Oas.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_Stores'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Oas.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_Stores'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasQ.SequenceId       IS NULL      AND
        OasCache.SequenceId   IS NULL      AND
        Stores1.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_Stores WHERE ArchiveKey IN
(
SELECT
         Oas.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_Stores Oas

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_StoresQ OasQ
        ON
        Oas.SequenceId   = OasQ.SequenceId
    AND Oas.TransactionTS    = OasQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_Store_Load_Cache OasCache
        ON
        Oas.SequenceId   = OasCache.SequenceId
    AND Oas.TransactionTS    = OasCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_Stores Stores1
        ON
        Oas.ArchiveKey = Stores1.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Oas.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_Stores'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Oas.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_Stores'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasQ.SequenceId       IS NULL      AND
        OasCache.SequenceId   IS NULL      AND
        Stores1.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 2) OAS StockBalanceReq Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_StockBalanceReq
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey
		,XmlData    
        ,JobRunId      
)
SELECT
         OasStockBal.SequenceId        
        ,OasStockBal.SAPCustomerID  
        ,OasStockBal.ContentType         
        ,OasStockBal.SubContentType    
		,OasStockBal.TransactionTS	
        ,OasStockBal.ExtractionTS   
        ,OasStockBal.IEReceivedTS   
        ,OasStockBal.IEDeliveryTS   
        ,OasStockBal.SourceVersion  
        ,OasStockBal.TS             
        ,OasStockBal.ArchiveKey
		,OasStockBal.XmlData    
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_StockBalanceReq OasStockBal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_StockBalanceReqQ OasStockBalQ
        ON
        OasStockBal.SequenceId   = OasStockBalQ.SequenceId
    AND OasStockBal.TransactionTS    = OasStockBalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_SBRequest_Load_Cache OasStockBalCache
        ON
        OasStockBal.SequenceId   = OasStockBalCache.SequenceId
    AND OasStockBal.TransactionTS    = OasStockBalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_StockBalanceRequest S1StockBal
        ON
        OasStockBal.ArchiveKey = S1StockBal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasStockBal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_StockBalanceRequest'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasStockBal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_StockBalanceRequest'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasStockBalQ.SequenceId       IS NULL      AND
        OasStockBalCache.SequenceId   IS NULL      AND
        S1StockBal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_StockBalanceReq WHERE ArchiveKey IN
(
SELECT
         OasStockBal.ArchiveKey

FROM
        ${DB_ENV}StgOAST.Stg_OAS_StockBalanceReq OasStockBal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_StockBalanceReqQ OasStockBalQ
        ON
        OasStockBal.SequenceId   = OasStockBalQ.SequenceId
    AND OasStockBal.TransactionTS    = OasStockBalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_SBRequest_Load_Cache OasStockBalCache
        ON
        OasStockBal.SequenceId   = OasStockBalCache.SequenceId
    AND OasStockBal.TransactionTS    = OasStockBalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_StockBalanceRequest S1StockBal
        ON
        OasStockBal.ArchiveKey = S1StockBal.ArchiveKey
       
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasStockBal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_StockBalanceRequest'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasStockBal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_StockBalanceRequest'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasStockBalQ.SequenceId       IS NULL      AND
        OasStockBalCache.SequenceId   IS NULL      AND
        S1StockBal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 3) OAS AssortmentItem Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_AssortmentItem
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData     
        ,JobRunId      
)
SELECT
         OasAssort.SequenceId        
        ,OasAssort.SAPCustomerID  
        ,OasAssort.ContentType         
        ,OasAssort.SubContentType    
		,OasAssort.TransactionTS	
        ,OasAssort.ExtractionTS   
        ,OasAssort.IEReceivedTS   
        ,OasAssort.IEDeliveryTS   
        ,OasAssort.SourceVersion  
        ,OasAssort.TS             
        ,OasAssort.ArchiveKey 
		,OasAssort.XmlData        
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_AssortmentItem OasAssort

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_AssortmentItemQ OasAssortQ
        ON
        OasAssort.SequenceId   = OasAssortQ.SequenceId
    AND OasAssort.TransactionTS    = OasAssortQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_AssortmentItem_Load_Cache OasAssortCache
        ON
        OasAssort.SequenceId   = OasAssortCache.SequenceId
    AND OasAssort.TransactionTS    = OasAssortCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_AssortmentItem S1Assort
        ON
        OasAssort.ArchiveKey = S1Assort.ArchiveKey
    
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasAssort.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_AssortmentItem'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasAssort.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_AssortmentItem'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasAssortQ.SequenceId       IS NULL      AND
        OasAssortCache.SequenceId   IS NULL      AND
        S1Assort.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_AssortmentItem WHERE ArchiveKey IN
(
SELECT
         OasAssort.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_AssortmentItem OasAssort

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_AssortmentItemQ OasAssortQ
        ON
        OasAssort.SequenceId   = OasAssortQ.SequenceId
    AND OasAssort.TransactionTS    = OasAssortQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_AssortmentItem_Load_Cache OasAssortCache
        ON
        OasAssort.SequenceId   = OasAssortCache.SequenceId
    AND OasAssort.TransactionTS    = OasAssortCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_AssortmentItem S1Assort
        ON
        OasAssort.ArchiveKey = S1Assort.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasAssort.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_AssortmentItem'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasAssort.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_AssortmentItem'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasAssortQ.SequenceId       IS NULL      AND
        OasAssortCache.SequenceId   IS NULL      AND
        S1Assort.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 4) OAS MPLDaysToCover Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_MPLDaysToCover
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData   
        ,JobRunId      
)
SELECT
         OasMPL.SequenceId        
        ,OasMPL.SAPCustomerID  
        ,OasMPL.ContentType         
        ,OasMPL.SubContentType    
		,OasMPL.TransactionTS	
        ,OasMPL.ExtractionTS   
        ,OasMPL.IEReceivedTS   
        ,OasMPL.IEDeliveryTS   
        ,OasMPL.SourceVersion  
        ,OasMPL.TS             
        ,OasMPL.ArchiveKey
		,OasMPL.XmlData   
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_MPLDaysToCover OasMPL

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_MPLDaysToCoverQ OasMPLQ
        ON
        OasMPL.SequenceId   = OasMPLQ.SequenceId
    AND OasMPL.TransactionTS    = OasMPLQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_MPLDaysToCover_Load_Cache OasMPLCache
        ON
        OasMPL.SequenceId   = OasMPLCache.SequenceId
    AND OasMPL.TransactionTS    = OasMPLCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_MPLDaysToCover S1MPL
        ON
        OasMPL.ArchiveKey = S1MPL.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasMPL.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_MPLDaysToCover'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasMPL.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_MPLDaysToCover'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasMPLQ.SequenceId       IS NULL      AND
        OasMPLCache.SequenceId   IS NULL      AND
        S1MPL.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_MPLDaysToCover WHERE ArchiveKey IN
(
SELECT
         OasMPL.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_MPLDaysToCover OasMPL

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_MPLDaysToCoverQ OasMPLQ
        ON
        OasMPL.SequenceId   = OasMPLQ.SequenceId
    AND OasMPL.TransactionTS    = OasMPLQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_MPLDaysToCover_Load_Cache OasMPLCache
        ON
        OasMPL.SequenceId   = OasMPLCache.SequenceId
    AND OasMPL.TransactionTS    = OasMPLCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_MPLDaysToCover S1MPL
        ON
        OasMPL.ArchiveKey = S1MPL.ArchiveKey
 
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasMPL.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_MPLDaysToCover'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasMPL.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_MPLDaysToCover'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasMPLQ.SequenceId       IS NULL      AND
        OasMPLCache.SequenceId   IS NULL      AND
        S1MPL.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 5) OAS OnOrder Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_OnOrder
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData     
        ,JobRunId      
)
SELECT
         OasOnOdr.SequenceId        
        ,OasOnOdr.SAPCustomerID  
        ,OasOnOdr.ContentType         
        ,OasOnOdr.SubContentType    
		,OasOnOdr.TransactionTS	
        ,OasOnOdr.ExtractionTS   
        ,OasOnOdr.IEReceivedTS   
        ,OasOnOdr.IEDeliveryTS   
        ,OasOnOdr.SourceVersion  
        ,OasOnOdr.TS             
        ,OasOnOdr.ArchiveKey  
		,OasOnOdr.XmlData     
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_OnOrder OasOnOdr

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_OnOrderQ OasOnOdrQ
        ON
        OasOnOdr.SequenceId   = OasOnOdrQ.SequenceId
    AND OasOnOdr.TransactionTS    = OasOnOdrQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_OnOrder_Load_Cache OasOnOdrCache
        ON
        OasOnOdr.SequenceId   = OasOnOdrCache.SequenceId
    AND OasOnOdr.TransactionTS    = OasOnOdrCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_OnOrder S1OnOdr
        ON
        OasOnOdr.ArchiveKey = S1OnOdr.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasOnOdr.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_OnOrder'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasOnOdr.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_OnOrder'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasOnOdrQ.SequenceId       IS NULL      AND
        OasOnOdrCache.SequenceId   IS NULL      AND
        S1OnOdr.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_OnOrder WHERE ArchiveKey IN
(
SELECT
         OasOnOdr.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_OnOrder OasOnOdr

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_OnOrderQ OasOnOdrQ
        ON
        OasOnOdr.SequenceId   = OasOnOdrQ.SequenceId
    AND OasOnOdr.TransactionTS    = OasOnOdrQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_OnOrder_Load_Cache OasOnOdrCache
        ON
        OasOnOdr.SequenceId   = OasOnOdrCache.SequenceId
    AND OasOnOdr.TransactionTS    = OasOnOdrCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_OnOrder S1OnOdr
        ON
        OasOnOdr.ArchiveKey = S1OnOdr.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasOnOdr.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_OnOrder'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasOnOdr.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_OnOrder'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasOnOdrQ.SequenceId       IS NULL      AND
        OasOnOdrCache.SequenceId   IS NULL      AND
        S1OnOdr.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 6) OAS OrderProposal Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_OrderProposal
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData      
        ,JobRunId      
)
SELECT
         OasOdrPropsal.SequenceId        
        ,OasOdrPropsal.SAPCustomerID  
        ,OasOdrPropsal.ContentType         
        ,OasOdrPropsal.SubContentType    
		,OasOdrPropsal.TransactionTS	
        ,OasOdrPropsal.ExtractionTS   
        ,OasOdrPropsal.IEReceivedTS   
        ,OasOdrPropsal.IEDeliveryTS   
        ,OasOdrPropsal.SourceVersion  
        ,OasOdrPropsal.TS             
        ,OasOdrPropsal.ArchiveKey   
		,OasOdrPropsal.XmlData      
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_OrderProposal OasOdrPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_OrderProposalQ OasOdrPropsalQ
        ON
        OasOdrPropsal.SequenceId   = OasOdrPropsalQ.SequenceId
    AND OasOdrPropsal.TransactionTS    = OasOdrPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_OrderProposal_Load_Cache OasOdrPropsalCache
        ON
        OasOdrPropsal.SequenceId   = OasOdrPropsalCache.SequenceId
    AND OasOdrPropsal.TransactionTS    = OasOdrPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_OrderProposal_Order S1OdrPropsal
        ON
        OasOdrPropsal.ArchiveKey = S1OdrPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasOdrPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_OrderProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasOdrPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_OrderProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasOdrPropsalQ.SequenceId       IS NULL      AND
        OasOdrPropsalCache.SequenceId   IS NULL      AND
        S1OdrPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_OrderProposal WHERE ArchiveKey IN
(
SELECT
         OasOdrPropsal.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_OrderProposal OasOdrPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_OrderProposalQ OasOdrPropsalQ
        ON
        OasOdrPropsal.SequenceId   = OasOdrPropsalQ.SequenceId
    AND OasOdrPropsal.TransactionTS    = OasOdrPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_OrderProposal_Load_Cache OasOdrPropsalCache
        ON
        OasOdrPropsal.SequenceId   = OasOdrPropsalCache.SequenceId
    AND OasOdrPropsal.TransactionTS    = OasOdrPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_OrderProposal_Order S1OdrPropsal
        ON
        OasOdrPropsal.ArchiveKey = S1OdrPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasOdrPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_OrderProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasOdrPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_OrderProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasOdrPropsalQ.SequenceId       IS NULL      AND
        OasOdrPropsalCache.SequenceId   IS NULL      AND
        S1OdrPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 7) OAS POSRequest Load Archiving
------------------------------------------------------------------------------------------


INSERT INTO ${DB_ENV}Achv.Arc_OAS_POSRequest 
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS 
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData   
        ,JobRunId      
)
SELECT

         Oas.SequenceId        
        ,Oas.SAPCustomerID  
        ,Oas.ContentType         
        ,Oas.SubContentType
		,Oas.TransactionTS 		
        ,Oas.ExtractionTS   
        ,Oas.IEReceivedTS   
        ,Oas.IEDeliveryTS   
        ,Oas.SourceVersion  
        ,Oas.TS             
        ,Oas.ArchiveKey
		,Oas.XmlData     
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_POSRequest Oas

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_POSRequestQ OasQ
        ON
        Oas.SequenceId   = OasQ.SequenceId
    AND Oas.TransactionTS    = OasQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_HistoricPOS_Load_Cache OasCache
        ON
        Oas.SequenceId   = OasCache.SequenceId
    AND Oas.TransactionTS    = OasCache.ReferenceTS
    
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Oas.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_HistoricPOS'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Oas.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_HistoricPOS'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasQ.SequenceId       IS NULL      AND
        OasCache.SequenceId   IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL 
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_POSRequest  WHERE ArchiveKey IN
(
SELECT
         Oas.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_POSRequest Oas

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_POSRequestQ OasQ
        ON
        Oas.SequenceId   = OasQ.SequenceId
    AND Oas.TransactionTS    = OasQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_HistoricPOS_Load_Cache OasCache
        ON
        Oas.SequenceId   = OasCache.SequenceId
    AND Oas.TransactionTS    = OasCache.ReferenceTS
    
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Oas.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_HistoricPOS'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Oas.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_HistoricPOS'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasQ.SequenceId       IS NULL      AND
        OasCache.SequenceId   IS NULL  AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL 
  
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 8) OAS CampaignProposal Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_CampaignProposal
(
         SequenceId       
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData      
        ,JobRunId      
)
SELECT
         OasCmpPropsal.SequenceId        
        ,OasCmpPropsal.ContentType         
        ,OasCmpPropsal.SubContentType    
		,OasCmpPropsal.TransactionTS	
        ,OasCmpPropsal.ExtractionTS   
        ,OasCmpPropsal.IEReceivedTS   
        ,OasCmpPropsal.IEDeliveryTS   
        ,OasCmpPropsal.SourceVersion  
        ,OasCmpPropsal.TS             
        ,OasCmpPropsal.ArchiveKey   
		,OasCmpPropsal.XmlData      
		,$JobRunID AS JobRunID
FROM
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposal OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_CampaignProposal_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_CampaignProposal S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_CampaignProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_CampaignProposal'
        AND XPV.ResolveStatus = 'E'
WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_CampaignProposal WHERE ArchiveKey IN
(
SELECT
         OasCmpPropsal.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposal OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_CampaignProposal_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_CampaignProposal S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_CampaignProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_CampaignProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 9) OAS CampaignRowProposal Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_CampaignProposalRow
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData      
        ,JobRunId      
)
SELECT
         OasCmpPropsal.SequenceId        
        ,OasCmpPropsal.SAPCustomerID  
        ,OasCmpPropsal.ContentType         
        ,OasCmpPropsal.SubContentType    
		,OasCmpPropsal.TransactionTS	
        ,OasCmpPropsal.ExtractionTS   
        ,OasCmpPropsal.IEReceivedTS   
        ,OasCmpPropsal.IEDeliveryTS   
        ,OasCmpPropsal.SourceVersion  
        ,OasCmpPropsal.TS             
        ,OasCmpPropsal.ArchiveKey   
		,OasCmpPropsal.XmlData      
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalRow OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalRowQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_CmpnProposalRow_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_CampaignProposalrow S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_CampaignRowProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_CampaignRowProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalRow WHERE ArchiveKey IN
(
SELECT
         OasCmpPropsal.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalRow OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_CampaignProposalRowQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_CmpnProposalRow_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_CampaignProposalRow S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_CampaignRowProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_CampaignRowProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 10) OAS SeasonProposal Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_SeasonProposal
(
         SequenceId       
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData      
        ,JobRunId      
)
SELECT
         OasCmpPropsal.SequenceId        
        ,OasCmpPropsal.ContentType         
        ,OasCmpPropsal.SubContentType    
		,OasCmpPropsal.TransactionTS	
        ,OasCmpPropsal.ExtractionTS   
        ,OasCmpPropsal.IEReceivedTS   
        ,OasCmpPropsal.IEDeliveryTS   
        ,OasCmpPropsal.SourceVersion  
        ,OasCmpPropsal.TS             
        ,OasCmpPropsal.ArchiveKey   
		,OasCmpPropsal.XmlData      
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposal OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposalQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_SeasonProposal_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_SeasonProposal S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_SeasonProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_SeasonProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_SeasonProposal WHERE ArchiveKey IN
(
SELECT
         OasCmpPropsal.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposal OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposalQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_SeasonProposal_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_SeasonProposal S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_SeasonProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_SeasonProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR



------------------------------------------------------------------------------------------
-- 11) OAS SeasonRowProposal Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_OAS_SeasonProposalRow
(
         SequenceId       
        ,SAPCustomerID 
        ,ContentType        
        ,SubContentType      
        ,TransactionTS 
		,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,TS            
        ,ArchiveKey 
		,XmlData      
        ,JobRunId      
)
SELECT
         OasCmpPropsal.SequenceId        
        ,OasCmpPropsal.SAPCustomerID  
        ,OasCmpPropsal.ContentType         
        ,OasCmpPropsal.SubContentType    
		,OasCmpPropsal.TransactionTS	
        ,OasCmpPropsal.ExtractionTS   
        ,OasCmpPropsal.IEReceivedTS   
        ,OasCmpPropsal.IEDeliveryTS   
        ,OasCmpPropsal.SourceVersion  
        ,OasCmpPropsal.TS             
        ,OasCmpPropsal.ArchiveKey   
		,OasCmpPropsal.XmlData      
		,$JobRunID AS JobRunID

FROM
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposalRow OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposalRowQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_CmpnProposalRow_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_SeasonProposalrow S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_SeasonRowProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_SeasonRowProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgOAST.Stg_OAS_SeasonProposalRow WHERE ArchiveKey IN
(
SELECT
         OasCmpPropsal.ArchiveKey
FROM
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposalRow OasCmpPropsal

LEFT OUTER JOIN
        ${DB_ENV}StgOAST.Stg_OAS_SeasonProposalRowQ OasCmpPropsalQ
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalQ.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.OAS_CmpnProposalRow_Load_Cache OasCmpPropsalCache
        ON
        OasCmpPropsal.SequenceId   = OasCmpPropsalCache.SequenceId
    AND OasCmpPropsal.TransactionTS    = OasCmpPropsalCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgOAST.S1_OAS_SeasonProposalRow S1CmpPropsal
        ON
        OasCmpPropsal.ArchiveKey = S1CmpPropsal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        OasCmpPropsal.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'OAS_SeasonRowProposal'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        OasCmpPropsal.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'OAS_SeasonRowProposal'
        AND XPV.ResolveStatus = 'E'

WHERE
        OasCmpPropsalQ.SequenceId       IS NULL      AND
        OasCmpPropsalCache.SequenceId   IS NULL      AND
        S1CmpPropsal.ArchiveKey  IS NULL      AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 12) JobRun Finish
------------------------------------------------------------------------------------------

UPDATE ${DB_ENV}MetaDataVIN.JobRun
SET JobEnd = CURRENT_TIMESTAMP
WHERE
JobRunID = $JobRunID
;


.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE
COMMIT WORK;
.IF ERRORCODE <> 0 THEN  .QUIT ERRORCODE

.QUIT 0


------------------------------------------------------------------------------------------
-- 11) Error Handling
------------------------------------------------------------------------------------------

.LABEL RETURNERROR

.QUIT ERRORCODE" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT START ----------------" >> $LOG_FILE
echo >> $LOG_FILE

cat $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT END ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS START AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

bteq < $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE 2>&1

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS END AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "start time $start_time" >> $LOG_FILE
echo "end time `date`" >> $LOG_FILE


BTEQ_RETURN_CODE=""
BTEQ_RETURN_CODE=`grep 'RC (return code)' $LOG_FILE `

test "$BTEQ_RETURN_CODE" = "" && {
        echo "Error: Unable to execute $BTEQ_ARCHIVE_SOURCE_SCRIPT"
        echo "Error: Could not find BTEQ_RETURN_CODE from $LOG_FILE"
        echo "Info: Try executing the command task again, check logon user and password"
        return -1
}

BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`
echo "BTEQ_RETURN_CODE ="$BTEQ_RETURN_CODE

if [ "$BTEQ_RETURN_CODE" = "0" ]; then
        rm  -f $BTEQ_ARCHIVE_SOURCE_SCRIPT
        rm  -f $LOG_FILE
        rm -f $LOGON_FILE
        #rm -f $PMRootDir/log/`basename $0`_$JobRunID"_"$CURRENT_TS"_script".log
        return $BTEQ_RETURN_CODE
        

else
        return -1
fi