#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CollectStatisticsSem_5.sh 12010 2014-01-07 17:58:34Z k9108547 $
# Last Changed By  : $Author: k9108547 $
# Last Change Date : $Date: 2014-01-07 18:58:34 +0100 (tis, 07 jan 2014) $
# Last Revision    : $Revision: 12010 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/CollectStatisticsSem_5.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=COLLECT_STAT;' FOR SESSION;

--*********************************************
--SEMCMNT AJI1_SALES_TRAN_LINE_WEEK
--*********************************************
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_LINE_WEEK;
.os date 

SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
