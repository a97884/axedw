#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Purge_InformaticaLogs.sh 22897 2017-08-28 10:14:02Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-08-28 12:14:02 +0200 (mån, 28 aug 2017) $
# Last Revision    : $Revision: 22897 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/Purge_InformaticaLogs.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

#!/usr/bin/ksh
# This script will remove sessions and workflow logs older than 13 days
# Runs from prompt, crontab or via unicenter scheduler
#
# Per Sundman
# 2012-06-25
#

edw_srv=$(hostname -s)
edw_env=''

        if [[ ${edw_srv%??} = dwpret ]]
        then
                        edw_env=pr

        elif [[ ${edw_srv%??} = dwitet ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

# LOGFILE=/usr/axtools/scripts/cleanup_axedw.log
LOGFILE=/opt/axedw/${edw_env}/is_axedw1/infa_shared/log/$(hostname -s)_cleanup_axedw.log

echo $(date) >> $LOGFILE
echo "" >> $LOGFILE
echo "--------------------------------------------------------------------------------------" >> $LOGFILE
echo "Cleanup of /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Direktmedia/Archive (seconds), removed `/usr/bin/find /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Direktmedia/Archive -name ext*.Z -mtime +13|wc -l|awk '{print $1}'` files" >> $LOGFILE
/usr/bin/time /usr/bin/find /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Direktmedia/Archive -name ext*.Z -mtime +13 -exec rm {} \; 2>>$LOGFILE
echo "" >> $LOGFILE
echo "--------------------------------------------------------------------------------------" >> $LOGFILE
echo "Cleanup of /opt/axedw/${edw_env}/is_axedw1/infa_shared/WorkflowLogs (seconds), removed `/usr/bin/find /opt/axedw/${edw_env}/is_axedw1/infa_shared/WorkflowLogs -name "wf_*"  -mtime +13|wc -l|awk '{print $1}'` files" >> $LOGFILE
/usr/bin/time /usr/bin/find /opt/axedw/${edw_env}/is_axedw1/infa_shared/WorkflowLogs -name "wf_*" -mtime +13 -exec rm {} \; 2>>$LOGFILE
echo "" >> $LOGFILE
echo "--------------------------------------------------------------------------------------" >> $LOGFILE
echo "Cleanup of /opt/axedw/${edw_env}/is_axedw1/infa_shared/SessLogs (seconds), removed `/usr/bin/find /opt/axedw/${edw_env}/is_axedw1/infa_shared/SessLogs -name "s_*" -mtime +13|wc -l|awk '{print $1}'` files" >> $LOGFILE
/usr/bin/time /usr/bin/find /opt/axedw/${edw_env}/is_axedw1/infa_shared/SessLogs -name "s_*" -mtime +13 -exec rm {} \; 2>>$LOGFILE
echo "" >> $LOGFILE
echo "--------------------------------------------------------------------------------------" >> $LOGFILE
echo "" >> $LOGFILE