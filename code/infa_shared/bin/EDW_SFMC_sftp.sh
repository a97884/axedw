#!/usr/bin/ksh
##############################################################################
#                                                                            #
# SFTP script to transfer files to Salesforce                                #
#                                                                            #
# 2019-04-01 Erik Kaar                                                       #
##############################################################################
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib
export LD_LIBRARY_PATH
sftphost='mc20y95dfdwr2c835gxfq65rqw3q.ftp.marketingcloudops.com'
fileuser='100012002'
PMRootDir=$1/TgtFiles/SFMC
IntegrationID=$2
Chain=$3
ChainID=$4
ExpFile=$5

sftp -oBatchMode=no -b - ${fileuser}@${sftphost} << !
   cd /Import/${Chain}/${IntegrationID}
   put ${ExpFile}
bye
!