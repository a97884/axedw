#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CollectStatistics_13.sh 16121 2015-04-13 11:29:45Z k9108547 $
# Last Changed By  : $Author: k9108547 $
# Last Change Date : $Date: 2015-04-13 13:29:45 +0200 (mån, 13 apr 2015) $
# Last Revision    : $Revision: 16121 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/CollectStatistics_13.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

 
export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=COLLECT_STAT;' FOR SESSION;

--*********************************************
--TgtT 
--*********************************************

.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.CAMPAIGN;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_STATUS_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_NAME_B;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_CATEGORY;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_COUPON;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.SCANBACK_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_DETAILS_B;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_STORE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_PREREQUISITE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_NAME_B;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_VENDOR_FUND;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.TACTIC_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_ARTICLE_TACTIC;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_ARTICLE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMO_OFFER_ARTL_COST_PRICE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_OPERATOR;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PRODUCT_DIMENSION_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_TERM_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_STATUS_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_TRANSFER_STATUS_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_STATUS_B;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.CAMPAIGN_PROMOTION_OFFER;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.CAMPAIGN_DETAILS_B;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMO_OFFER_ARTICLE_FORECAST;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_BUSINESS_STATUS_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_ARTICLE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMOTION_OFFER_DISCOUNT_TYPE;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMO_OFFER_ARTICLE_CONSTR;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_STATUS_B;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.PROMO_OFFER_CAMPAIGN_DETAILS_B;
.os date 
COLLECT STATISTICS ON ${DB_ENV}TgtT.VENDOR_FUND_DETAILS_B;
.os date 


SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
