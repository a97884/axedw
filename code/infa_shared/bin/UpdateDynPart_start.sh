#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: UpdateDynPart_start.sh 23924 2018-01-10 12:04:14Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2018-01-10 13:04:14 +0100 (ons, 10 jan 2018) $
# Last Revision    : $Revision: 23924 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/UpdateDynPart_start.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#/bin/ksh
edw_srv=$(hostname -s)
edw_env=''
SaveExitCode=0

        if [[ ${edw_srv%??} = dwpret ]]
        then
                        edw_env=pr

        elif [[ ${edw_srv%??} = dwitet ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

export PMRootDir="/opt/axedw/${edw_env}/is_axedw1/infa_shared"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo PMRootDir="${PMRootDir}"


  if [ "${edw_env}" = "pr" ]
  then

      print running in production
      sudo -u etpradm1 /opt/axedw/pr/is_axedw1/infa_shared/bin/UpdateDynPart.sh /opt/axedw/pr/is_axedw1/infa_shared
      if [ "$?" = "1" ] ; then
         exit 1
      fi

  elif [ "${edw_env}" = "it" ]
  then
      print running in it
      sudo -u etitadm1 /opt/axedw/it/is_axedw1/infa_shared/bin/UpdateDynPart.sh /opt/axedw/it/is_axedw1/infa_shared
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi


      print running in uv1
      ssh dwutet04 "sudo -u etuvadm1 /opt/axedw/uv/is_axedw1/infa_shared/bin/UpdateDynPart.sh /opt/axedw/uv/is_axedw1/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi

      print running in uv2
      ssh dwutet04 "sudo -u etuvadm2 /opt/axedw/uv/is_axedw2/infa_shared/bin/UpdateDynPart.sh /opt/axedw/uv/is_axedw2/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi

      print running in st1
      ssh dwutet04 "sudo -u etstadm1 /opt/axedw/st/is_axedw1/infa_shared/bin/UpdateDynPart.sh /opt/axedw/st/is_axedw1/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi

      print running in st2
      ssh dwutet04 "sudo -u etstadm2 /opt/axedw/st/is_axedw2/infa_shared/bin/UpdateDynPart.sh /opt/axedw/st/is_axedw2/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi
	  
	  exit ${SaveExitCode}

  fi

