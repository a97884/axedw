#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CollectStatistics_4.sh 18429 2016-02-04 08:03:59Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2016-02-04 09:03:59 +0100 (tor, 04 feb 2016) $
# Last Revision    : $Revision: 18429 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/CollectStatistics_4.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=COLLECT_STAT;' FOR SESSION;

.label next

COLLECT STATISTICS ON ${DB_ENV}TgtT.J1LOCATION_ORGANIZATION;
COLLECT STATISTICS ON ${DB_ENV}TgtT.J2LOCATION_ORGANIZATION;
COLLECT STATISTICS ON ${DB_ENV}TgtT.STJI1_ACCT_ACCT_RELATIONSHIP;


SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
