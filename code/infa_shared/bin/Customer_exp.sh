#!/bin/ksh

export PMRootDir=$1
export CLOB_DIR="$PMRootDir/SrcFiles/SAP/Customer"
export SCRIPT_DIR="$PMRootDir/bin"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"
export I_SEQUENCE_ID=$2
export I_REFERENCETS=$3
export FILE_LIST_NAME="xmllist"
export FILE_DIR_NAME=$CLOB_DIR

exec >$PMRootDir/log/`basename $0`.log 2>&1 

echo "PMRootDir=$1"
echo "I_SEQUENCE_ID=$2"
echo "I_REFERENCETS=$3"
if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

#  Print script syntax and help

print_help ()
    {
    echo "Usage: `basename $0` <SequenceId>"
    echo ""
    echo "   XML directory is   : $CLOB_DIR"
	echo "   <PMRootDir>        : Value of PMRootDir, parsed from Informatica normally"
    echo "   <SequenceId>       : SequenceId of Xmlfile to export"
    echo "   <ReferenceTS>      : ReferenceTS of Xmlfile"
    echo ""
    }

############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 3 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
#  Run TPT to export Customer-files                                        #
############################################################################
echo ""
echo "###############################################################################"
echo "# Starting TPT job to export Customer-files ...                               #"
echo "###############################################################################"
echo "" 
tbuild -f $SCRIPT_DIR/Customer_exp.tpt -v $SCRIPT_DIR/Customer_exp.var -u "DB_ENV='$DB_ENV', TargetFileName = '$FILE_LIST_NAME', LobDirPath = '$CLOB_DIR', SequenceId = '$I_SEQUENCE_ID', ReferenceTS = '$I_REFERENCETS'"
