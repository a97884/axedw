#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: RefreshStats_2.sh 10509 2013-09-27 11:27:59Z k9108547 $
# Last Changed By  : $Author: k9108547 $
# Last Change Date : $Date: 2013-09-27 13:27:59 +0200 (fre, 27 sep 2013) $
# Last Revision    : $Revision: 10509 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/RefreshStats_2.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
	for FILE in $PMRootDir/bin/CollectStatisticsSem_*
do
	$FILE $PMRootDir &
done

wait

)  2>&1 | tee $logfile
