#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Purge_InformaticaBackup.sh 22897 2017-08-28 10:14:02Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-08-28 12:14:02 +0200 (mån, 28 aug 2017) $
# Last Revision    : $Revision: 22897 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/Purge_InformaticaBackup.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

#!/usr/bin/ksh
#This script will remove backups of the infa_shared repository that are older than 7 days
#set -x
edw_srv=$(hostname -s)
edw_env=''

        if [[ ${edw_srv%??} = dwpret ]]
        then
                        edw_env=pr

        elif [[ ${edw_srv%??} = dwitet ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

LOGFILE=/opt/axedw/${edw_env}/is_axedw1/infa_shared/log/$(hostname -s)_cleanup_infabackup.log


cd /opt/axedw/${edw_env}/is_axedw1/infa_shared/Backup

find . -name "*.rep" -mtime +7 -exec ls -l {} \; >> ${LOGFILE}
find . -name "*.rep" -mtime +7 -exec rm {} \;
