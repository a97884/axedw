#!/usr/bin/ksh
###########################################################################
#                                                                         #
# scbzip - Packa och komprimera alla filer for skick till SCB             #
#                                                                         #
# 2013-07-01  Peter Holmlund                               	              #
###########################################################################

# Satt variabler med biblioteks och filnamn mm.
SCBBaseDir=/opt/axedw/UV1/is_axedw2/infa_shared
SCBBaseDir=$1
SCBUtDir=${SCBBaseDir}/TgtFiles/SCB
SCBArkDir=${SCBUtDir}/Archive
Timefile=time.txt
OutfilePrefix=tmp_Axfood_
OutfilePrefixEnd=KPIAxfood_
Timestamp=`date +%Y%m%d_%H%M%S`
echo "start"
if [ ! -s ${SCBUtDir}/${Timefile} ]
then
  echo "Hittar inte filen ${Timefile}!"
  exit
fi

# Ta bort eventuella gamla flaggfiler.
echo Ta bort gammal flaggfil
rm -f ${SCBUtDir}/*.flg

# Hamta aktuellt datum ur tidsfilen.
CurWeek=`cat ${SCBUtDir}/${Timefile} | awk '{print $6}'`

# Skapa tar-fil och zippa den.
OutTar=${OutfilePrefix}${CurWeek}.tar
cd ${SCBUtDir}
tar cvf ${OutTar} *.txt ; gzip ${SCBUtDir}/$OutTar

# Kontrollera att zipfilen existerar och har innehall.
# Skapa i sa fall flaggfilen, lagg en kopia i arkivet,
# ta bort txt-filerna och returnera ok (0).
if [ -s ${SCBUtDir}/${OutTar}.gz ]
then
  #cp ${SCBUtDir}/${OutTar}.gz ${SCBArkDir}/${OutTar}.gz.${Timestamp}
  rm -f ${SCBUtDir}/*.txt
  touch ${SCBUtDir}/${CurWeek}.flg
  mv ${SCBUtDir}/${OutTar}.gz ${SCBUtDir}/${OutfilePrefixEnd}${CurWeek}.tar.gz
  exit 0
fi

# Returnera fel (1).
exit 1
