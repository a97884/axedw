#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SendMail.sh 1434 2011-12-06 10:32:09Z etuvadm2 $
# Last Changed By  : $Author: etuvadm2 $
# Last Change Date : $Date: 2011-12-06 11:32:09 +0100 (tis, 06 dec 2011) $
# Last Revision    : $Revision: 1434 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/SendMail.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Usage: SendMail to subject text
#
to="$1"
subject="$2"
shift
shift
text="$*"

echo "$text" | mailx -N -s "$subject" "$to"
status=$?

exit $status
