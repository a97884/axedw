#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: UpdateDynPartDaily_start.sh 31144 2020-04-01 15:17:14Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-04-01 17:17:14 +0200 (ons, 01 apr 2020) $
# Last Revision    : $Revision: 31144 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/UpdateDynPartDaily_start.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#/bin/ksh
edw_srv=$(hostname -s)
edw_env=''
SaveExitCode=0

        if [[ ${edw_srv%??} = dwpret ]]
        then
                        edw_env=pr

        elif [[ ${edw_srv%??} = dwitet ]]
        then
                        edw_env=it
       
		elif [[ ${edw_srv%??} = dwutet ]]
        then
                        edw_env=st2
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

export PMRootDir="/opt/axedw/${edw_env}/is_axedw1/infa_shared"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo PMRootDir="${PMRootDir}"

  if [ "${edw_env}" = "pr" ]
  then

      print running in production
      sudo -u etpradm1 /opt/axedw/pr/is_axedw1/infa_shared/bin/UpdateDynPartDaily.sh /opt/axedw/pr/is_axedw1/infa_shared
      if [ "$?" = "1" ] ; then
         exit 1
      fi

  elif [ "${edw_env}" = "it" ]
  then
      print running in it
      sudo -u etitadm1 /opt/axedw/it/is_axedw1/infa_shared/bin/UpdateDynPartDaily.sh /opt/axedw/it/is_axedw1/infa_shared
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi

  elif [ "${edw_env}" = "st2" ]
  then
  
      print running in uv1
      ssh dwutet04 "sudo -u etuvadm1 /opt/axedw/uv/is_axedw1/infa_shared/bin/UpdateDynPartDaily.sh /opt/axedw/uv/is_axedw1/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi

      print running in uv2
      ssh dwutet04 "sudo -u etuvadm2 /opt/axedw/uv/is_axedw2/infa_shared/bin/UpdateDynPartDaily.sh /opt/axedw/uv/is_axedw2/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi

      print running in st1
      ssh dwutet04 "sudo -u etstadm1 /opt/axedw/st/is_axedw1/infa_shared/bin/UpdateDynPartDaily.sh /opt/axedw/st/is_axedw1/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi

      print running in st2
      ssh dwutet04 "sudo -u etstadm2 /opt/axedw/st/is_axedw2/infa_shared/bin/UpdateDynPartDaily.sh /opt/axedw/st/is_axedw2/infa_shared"
      if [ "$?" = "1" ] ; then
         SaveExitCode=1
      fi
	  

  fi
exit ${SaveExitCode}
