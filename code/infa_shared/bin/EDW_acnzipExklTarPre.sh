#!/usr/bin/ksh
###########################################################################
#                                                                         #
# acnzip - Packa och komprimera alla filer for skick till Nielsen         # 
# Skicket ska gå på torsdaga och leverera del av veckans försäljning      #
# 2017-12-01  Kristina Svahn                              	              #
###########################################################################

# Satt variabler med biblioteks och filnamn mm.
AcnBaseDir=/opt/axedw/UV1/is_axedw2/infa_shared
AcnBaseDir=$1
AcnUtDir=${AcnBaseDir}/TgtFiles/ACN
AcnArkDir=${AcnUtDir}/Archive
Timefile=time.txt
OutfilePrefix=tmp_Axfood_
OutfilePrefixEnd=Axfood_
OutfileSuffixEnd=_MIDWEEK
Timestamp=`date +%Y%m%d_%H%M%S`
echo "start"
if [ ! -s ${AcnUtDir}/${Timefile} ]
then
  echo "Hittar inte filen ${Timefile}!"
  exit
fi
echo "start2"
# Ta bort eventuella gamla flaggfiler.
echo Ta bort gammal flaggfil
rm -f ${AcnUtDir}/*.flg

# Hamta aktuellt datum ur tidsfilen.
CurWeek=`cat ${AcnUtDir}/${Timefile} | awk '{print $6}'`

mv ${AcnUtDir}/pre_AxfoodArtikelfil.csv ${AcnUtDir}/AxfoodArtikelfil.csv

# Skapa tar-fil och zippa den.
OutZip=${OutfilePrefix}${CurWeek}${OutfileSuffixEnd}.zip
cd ${AcnUtDir}
zip ${OutZip} *.txt



# Kontrollera att zipfilen existerar och har innehall.
# Skapa i sa fall flaggfilen, lagg en kopia i arkivet,
# ta bort txt-filerna och returnera ok (0).
if [ -s ${AcnUtDir}/${OutZip} ]
then
  #cp ${AcnUtDir}/${OutZip}.gz ${AcnArkDir}/${OutZip}.gz.${Timestamp}
  rm -f ${AcnUtDir}/*.txt
  touch ${AcnUtDir}/${CurWeek}.flg
  mv ${AcnUtDir}/${OutZip} ${AcnUtDir}/${OutfilePrefixEnd}${CurWeek}${OutfileSuffixEnd}.zip
  exit 0
fi

# Returnera fel (1).
exit 1
