#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Customer_exp_full.sh 2744 2012-04-26 15:16:54Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-04-26 17:16:54 +0200 (tor, 26 apr 2012) $
# Last Revision    : $Revision: 2744 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/Customer_exp_full.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export CLOB_DIR="$PMRootDir/SrcFiles/SAP/Customer"
export SCRIPT_DIR="$PMRootDir/bin"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"
export FILE_LIST_NAME="xmllist"
export FILE_DIR_NAME=$CLOB_DIR

exec >$PMRootDir/log/`basename $0`.log 2>&1 

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

#  Print script syntax and help

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir>"
    echo ""
    echo "   XML directory is   : $CLOB_DIR"
    echo "   <PMRootDir>        : Informatica PMRootDir variable"
    echo ""
    }

############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 1 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
#  Run TPT to export POS-files                                             #
############################################################################
echo ""
echo "###############################################################################"
echo "# Starting TPT job to export POS-files ...                                    #"
echo "###############################################################################"
echo "" 
tbuild -f $SCRIPT_DIR/Customer_exp_full.tpt -v $SCRIPT_DIR/Customer_exp_full.var -u "DB_ENV='$DB_ENV', TargetFileName = '$FILE_LIST_NAME', LobDirPath = '$CLOB_DIR'"
