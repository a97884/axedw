#!/bin/sh
##############################################################################
#                                                                            #
# acnget - Get specified SFF files from Nielsen                              #
#                                                                            #
# 2018-02-22 Erik Kaar                                                       #
##############################################################################
HOST='euse.nielsen.com'
USER='AX'
PASSWD='4Xsten'
PMRootDir=$1/SrcFiles/Nielsen
FILE=$2SESFF.*

ftp -n $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
prompt off
lcd $PMRootDir
cd SFF
bin
passive
mget $FILE
quit
END_SCRIPT
exit 0


