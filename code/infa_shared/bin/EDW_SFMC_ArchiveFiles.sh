#!/bin/sh
##############################################################################
#                                                                            #
# Archive SFMC files after loading of data                                   #
#                                                                            #
# 2018-09-06 Erik Kaar                                                       #
##############################################################################
PMRootDir=$1
FILE=$PMRootDir/SrcFiles/SFMC/$2

for f in $(cat $FILE) ; do
  mv "$f" $PMRootDir/SrcFiles/SFMC/Archive
done
exit 0



