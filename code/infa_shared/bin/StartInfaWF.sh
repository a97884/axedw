#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StartInfaWF.sh 1107 2011-11-29 15:08:11Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2011-11-29 16:08:11 +0100 (tis, 29 nov 2011) $
# Last Revision    : $Revision: 1107 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/StartInfaWF.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Usage: StartInfaWF domain service user password folder workflow
#
. $HOME/.infascm_profile

if [ $# -lt 6 ]
then
	echo "Usage:  `basename $0` domain service user password folder workflow [parameter=value]..."
	exit 1
fi
domain="$1"
service="$2"
user="$3"
password="$4"
folder="$5"
workflow="$6"
if [ $# -gt 6 ]
then
	paramfile=/tmp/$$.prm
	parameterfile="-lpf $paramfile"
	shift; shift; shift; shift; shift; shift
	echo "[Service:$service]" >$paramfile
	for par in "$@"
	do
		echo "\$\$$par"
	done >>$paramfile
fi

pmcmd startworkflow -domain "$domain" -service "$service" -user "$user" -pv "$password" -folder "$folder" $parameterfile -wait "$workflow"
status=$?

test "$paramfile" != "" && rm -f $paramfile

exit $status
