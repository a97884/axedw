#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Purge_OASPOS.sh 28585 2019-06-19 12:48:30Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-06-19 14:48:30 +0200 (ons, 19 jun 2019) $
# Last Revision    : $Revision: 28585 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/Purge_OASPOS.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#!/bin/ksh
#This script will remove backups of the infa_shared OAS-POS files that are older than 20 days
#set -x

edw_srv=$(hostname -s)
edw_env=''

        if [[ ${edw_srv%??} = dwpret ]]
        then
                        edw_env=pr

        elif [[ ${edw_srv%??} = dwitet ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi



LOGFILE=/opt/axedw/${edw_env}/is_axedw1/infa_shared/log/$(hostname -s)_cleanup_oaspos.log

touch ${LOGFILE}

DIRDAILY=/opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/OAS/POSDaily/Archive
DIRHIST=/opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/OAS/POSHistoric/Archive

        if [ -d $DIRDAILY ]
                then
                        cd $DIRDAILY
                        find . -mtime +20 -exec rm -f {} \;
                        pwd

                else
                        echo "Warning, ${DIRDAILY} is missing!"
        fi

        if [ -d $DIRHIST ]
                then
                        cd $DIRHIST
                        find . -mtime +20 -exec rm -f {} \;
                        pwd
                else
                        echo "Warning, ${DIRHIST} is missing!"
        fi


