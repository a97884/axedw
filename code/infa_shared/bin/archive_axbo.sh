#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: archive_axbo.sh 21907 2017-03-14 09:56:48Z k9107640 $
# Last Changed By  : $Author: k9107640 $
# Last Change Date : $Date: 2017-03-14 10:56:48 +0100 (tis, 14 mar 2017) $
# Last Revision    : $Revision: 21907 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/archive_axbo.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

DB_ENVAchv="$DB_ENV"Achv
DB_ENVStgAxBOT="$DB_ENV"StgAxBOT
DB_ENVMetaDataVIN="$DB_ENV"MetaDataVIN

JobRunID=$2
echo "JobRunID=$JobRunID"

CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS=$CURRENT_TS"

exec >$PMRootDir/log/`basename $0`_"Arch_"$CURRENT_TS"_script".log 2>&1 

echo "writing to log"
echo "PMRootDir = $PMRootDir"
echo "DB_ENV = $DB_ENV"
echo "CURRENT_TS = $CURRENT_TS"
echo "JobRunID = $JobRunID"

echo "DB_ENVAchv = ${DB_ENV}Achv"
echo "DB_ENVStgAxBOT = ${DB_ENV}StgAxBOT"
echo "DB_ENVMetaDataVIN = ${DB_ENV}MetaDataVIN"



###################################################################################################
#  Print script syntax and help
###################################################################################################

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <JobRunID>"
    echo ""
    echo "   Base directory is   : PMRootDir"
    echo "   <JobRunID>         : JobRunID to archive"
    echo ""
    }


############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


############################################################################
# Script and log file directories                                          #
############################################################################

# Log Directory
LOG_DIR=$1/log
echo "LOG_DIR = " $LOG_DIR


# Bteq Log File
LOG_FILE=$LOG_DIR/`basename $0 .sh`_$JobRunID"_"$CURRENT_TS"_bteq".log
echo "LOG_FILE = " $LOG_FILE
#chmod 777 $LOG_FILE


# Bteq Directory
SCRIPT_DIR=$1/bin
echo "SCRIPT_DIR = " $SCRIPT_DIR


# Bteq File
#BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/bin/`basename $0 .sh`_$CURRENT_TS.bteq
BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/tmp/`basename $0 .sh`_$CURRENT_TS.bteq
echo "BTEQ_ARCHIVE_SOURCE_SCRIPT = " $BTEQ_ARCHIVE_SOURCE_SCRIPT 
>$BTEQ_ARCHIVE_SOURCE_SCRIPT
#chmod 777 $BTEQ_ARCHIVE_SOURCE_SCRIPT


############################################################################
# TO GET LOGON INFORMATION FOR BTEQ                                        #
############################################################################

PWD_FILE=$SCRIPT_DIR/PWD_BteqLogon
echo "PWD_FILE = " $PWD_FILE

LOGON=`grep $DB_ENV"_"LOGON $PWD_FILE `

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	return -1 
}

#LOGON_FILE=$SCRIPT_DIR/$DB_ENV"_"$JobRunID"_LOGON"
LOGON_FILE=$1/tmp/$DB_ENV"_"$JobRunID"_LOGON"
echo "LOGON_FILE = " $LOGON_FILE
#chmod 777 $LOGON_FILE

LOGON=`echo $LOGON | cut -d "=" -f2`

#Test condition to check if the file already exists

if [ -f $LOGON_FILE ]		
then
	#echo "Logon File Exists"
	EXISTING_LOGON=`head -1 $LOGON_FILE`
	
	if [[ $LOGON = $EXISTING_LOGON ]]
	then
	echo "Same credentials reusing existing file"
	else
	echo $LOGON > $LOGON_FILE
	fi
else 
	#file does not exist, use latest credentials
	echo $LOGON > $LOGON_FILE
fi
chmod 777 $LOGON_FILE

echo "
.SET SESSION TRANSACTION ANSI;

.RUN FILE = $LOGON_FILE" > $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=CreateArchiveTables;ClientUser=$LOGNAME;' FOR SESSION;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "commit;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

############################################################################
# BTEQ                                                                                     
############################################################################

echo "
-- ---------------------------------------------------------------------------------------------------------------------
-- The output may exceed the standard 80 characters, it is expanded to 5000
-- ---------------------------------------------------------------------------------------------------------------------
.width 5000
.set format on
.foldline off" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

start_time=`date`
echo "start_time = " $start_time > $LOG_FILE


echo "
------------------------------------------------------------------------------------------
-- 1) AxBO_Loss Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxBO_Loss
(
         SequenceId   
		,SAPCustomerID 		 
        ,StoreId       
        ,ContentType   
        ,SubContentType
        ,TransactionTS 
        ,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,AOFlag        
        ,TS            
        ,ArchiveKey    
        ,XmlData       
        ,JobRunId     
		,ARC_TS	
)

SELECT
         Loss.SequenceId
		,Loss.SAPCustomerID 
        ,Loss.StoreId        
        ,Loss.ContentType    
        ,Loss.SubContentType 
        ,Loss.TransactionTS  
        ,Loss.ExtractionTS   
        ,Loss.IEReceivedTS   
        ,Loss.IEDeliveryTS   
        ,Loss.SourceVersion  
        ,Loss.AOFlag         
        ,Loss.TS             
        ,Loss.ArchiveKey     
        ,Loss.XmlData 
		,$JobRunID AS JobRunID
		,current_timestamp
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_Loss Loss
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_LossQ LossQ
        ON
        Loss.SequenceId      = LossQ.SequenceId
    AND Loss.SAPCustomerID   = LossQ.SAPCustomerID
    AND Loss.TransactionTS   = LossQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_InvAdjust_Load_Cache LossCache
        ON
        Loss.SequenceId      = LossCache.SequenceId
    AND Loss.SAPCustomerID   = LossCache.SAPCustomerID
    AND Loss.TransactionTS   = LossCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_AxBo_InvAdjust_BOTrans InvAdj
        ON
        Loss.ArchiveKey = InvAdj.ArchiveKey

LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Loss.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_Loss'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Loss.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_Loss'
        AND XPV.ResolveStatus = 'E'

WHERE
        InvAdj.ArchiveKey   IS NULL         AND
        LossQ.SequenceId  IS NULL         AND
        LossCache.SequenceId     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgAxBOT.Stg_AxBO_Loss WHERE ArchiveKey IN
(
SELECT
         Loss.ArchiveKey     
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_Loss Loss
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_LossQ LossQ
        ON
        Loss.SequenceId      = LossQ.SequenceId
    AND Loss.SAPCustomerID   = LossQ.SAPCustomerID
    AND Loss.TransactionTS   = LossQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_InvAdjust_Load_Cache LossCache
        ON
        Loss.SequenceId      = LossCache.SequenceId
    AND Loss.SAPCustomerID   = LossCache.SAPCustomerID
    AND Loss.TransactionTS   = LossCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_AxBo_InvAdjust_BOTrans InvAdj
        ON
        Loss.ArchiveKey = InvAdj.ArchiveKey

LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Loss.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_Loss'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Loss.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_Loss'
        AND XPV.ResolveStatus = 'E'

WHERE
        InvAdj.ArchiveKey   IS NULL         AND
        LossQ.SequenceId  IS NULL         AND
        LossCache.SequenceId     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 2) AxBO_Inventory Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxBO_Inventory
(
         SequenceId   
		,SAPCustomerID
        ,StoreId       
        ,ContentType   
        ,SubContentType
        ,TransactionTS 
        ,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,AOFlag        
        ,TS            
        ,ArchiveKey    
        ,XmlData       
        ,JobRunId 
		,ARC_TS	
)

SELECT
         Inv.SequenceId
		,Inv.SAPCustomerID
        ,Inv.StoreId        
        ,Inv.ContentType    
        ,Inv.SubContentType 
        ,Inv.TransactionTS  
        ,Inv.ExtractionTS   
        ,Inv.IEReceivedTS   
        ,Inv.IEDeliveryTS   
        ,Inv.SourceVersion  
        ,Inv.AOFlag         
        ,Inv.TS             
        ,Inv.ArchiveKey     
        ,Inv.XmlData        
        ,$JobRunID AS JobRunID
		,current_timestamp
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_Inventory Inv
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_InventoryQ InvQ
        ON
        Inv.SequenceId      = InvQ.SequenceId
    AND Inv.SAPCustomerID   = InvQ.SAPCustomerID
    AND Inv.TransactionTS   = InvQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_InvEvTot_Load_Cache InvCache
        ON
        Inv.SequenceId      = InvCache.SequenceId
    AND Inv.SAPCustomerID   = InvCache.SAPCustomerID
    AND Inv.TransactionTS   = InvCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_Axbo_InvEvTot_BOTrans InvEvTot
        ON
        Inv.ArchiveKey = InvEvTot.ArchiveKey
        
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Inv.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_Inventory'
                AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Inv.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_Inventory'
         AND XPV.ResolveStatus = 'E'

WHERE
        InvEvTot.ArchiveKey   IS NULL         AND
        InvQ.SequenceId       IS NULL         AND
        InvCache.SequenceId          IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgAxBOT.Stg_AxBO_Inventory WHERE ArchiveKey IN
(
SELECT
         Inv.ArchiveKey
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_Inventory Inv
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_InventoryQ InvQ
        ON
        Inv.SequenceId      = InvQ.SequenceId
    AND Inv.SAPCustomerID   = InvQ.SAPCustomerID
    AND Inv.TransactionTS   = InvQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_InvEvTot_Load_Cache InvCache
        ON
        Inv.SequenceId      = InvCache.SequenceId
    AND Inv.SAPCustomerID   = InvCache.SAPCustomerID
    AND Inv.TransactionTS   = InvCache.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_Axbo_InvEvTot_BOTrans InvEvTot
        ON
        Inv.ArchiveKey = InvEvTot.ArchiveKey
        
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Inv.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_Inventory'
                AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Inv.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_Inventory'
         AND XPV.ResolveStatus = 'E'

WHERE
        InvEvTot.ArchiveKey   IS NULL         AND
        InvQ.SequenceId       IS NULL         AND
        InvCache.SequenceId          IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR



------------------------------------------------------------------------------------------
-- 3) AxBO_Stock Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxBO_Stock
(
         SequenceId
		,SAPCustomerID		 
        ,StoreId       
        ,ContentType   
        ,SubContentType
        ,TransactionTS 
        ,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,AOFlag        
        ,TS            
        ,ArchiveKey    
        ,XmlData       
        ,JobRunId   
		,ARC_TS	
)

SELECT
         Stk.SequenceId
		,Stk.SAPCustomerID		 
        ,Stk.StoreId        
        ,Stk.ContentType    
        ,Stk.SubContentType 
        ,Stk.TransactionTS  
        ,Stk.ExtractionTS   
        ,Stk.IEReceivedTS   
        ,Stk.IEDeliveryTS   
        ,Stk.SourceVersion  
        ,Stk.AOFlag         
        ,Stk.TS             
        ,Stk.ArchiveKey     
        ,Stk.XmlData        
		,$JobRunID AS JobRunID
		,current_timestamp
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_Stock Stk
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_StockQ StkQ
        ON
        Stk.SequenceId      = StkQ.SequenceId
    AND Stk.SAPCustomerID   = StkQ.SAPCustomerID
    AND Stk.TransactionTS   = StkQ.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_InvBal_Load_Cache StkCache
        ON
        Stk.SequenceId      = StkCache.SequenceId
    AND Stk.SAPCustomerID   = StkCache.SAPCustomerID
    AND Stk.TransactionTS   = StkCache.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_AxBO_InvBal_BOTrans InvBal
        ON
        Stk.ArchiveKey = InvBal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Stk.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_Stock'
                AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Stk.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_Stock'
         AND XPV.ResolveStatus = 'E'

WHERE
        InvBal.ArchiveKey     IS NULL         AND
        StkQ.SequenceId       IS NULL         AND
        StkCache.SequenceId          IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgAxBOT.Stg_AxBO_Stock WHERE ArchiveKey IN
(
SELECT
         Stk.ArchiveKey
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_Stock Stk
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_StockQ StkQ
        ON
        Stk.SequenceId      = StkQ.SequenceId
    AND Stk.SAPCustomerID   = StkQ.SAPCustomerID
    AND Stk.TransactionTS   = StkQ.ReferenceTS
	
LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_InvBal_Load_Cache StkCache
        ON
        Stk.SequenceId      = StkCache.SequenceId
    AND Stk.SAPCustomerID   = StkCache.SAPCustomerID
    AND Stk.TransactionTS   = StkCache.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_AxBO_InvBal_BOTrans InvBal
        ON
        Stk.ArchiveKey = InvBal.ArchiveKey
        
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Stk.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_Stock'
                AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Stk.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_Stock'
         AND XPV.ResolveStatus = 'E'

WHERE
        InvBal.ArchiveKey     IS NULL         AND
        StkQ.SequenceId       IS NULL         AND
        StkCache.SequenceId          IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 4) AxBO_POS_CheckSum Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxBO_POSCheckSum
(
         SequenceId
        ,SAPCustomerID		 
        ,StoreId       
        ,ContentType   
        ,SubContentType
        ,TransactionTS 
        ,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,AOFlag        
        ,TS            
        ,ArchiveKey    
        ,XmlData       
        ,JobRunId      
		,ARC_TS
)
SELECT
         Pos.SequenceId
        ,Pos.SAPCustomerID		 
        ,Pos.StoreId        
        ,Pos.ContentType    
        ,Pos.SubContentType 
        ,Pos.TransactionTS  
        ,Pos.ExtractionTS   
        ,Pos.IEReceivedTS   
        ,Pos.IEDeliveryTS   
        ,Pos.SourceVersion  
        ,Pos.AOFlag         
        ,Pos.TS             
        ,Pos.ArchiveKey     
        ,Pos.XmlData        
		,$JobRunID AS JobRunID
		,current_timestamp
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_POSCheckSum Pos
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_POSCheckSumQ PosQ
        ON
        Pos.SequenceId      = PosQ.SequenceId
    AND Pos.SAPCustomerID   = PosQ.SAPCustomerID
    AND Pos.TransactionTS   = PosQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_POSChecksum_Load_Cache POSChecksumCache
        ON
        Pos.SequenceId      = POSChecksumCache.SequenceId
    AND Pos.SAPCustomerID   = POSChecksumCache.SAPCustomerID
    AND Pos.TransactionTS   = POSChecksumCache.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_AxBO_POS_CheckSum chk
        ON
        Pos.ArchiveKey = chk.ArchiveKey
        
     LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Pos.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_CheckSum'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Pos.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_CheckSum'
        AND XPV.ResolveStatus = 'E'

WHERE
        Pos.SubContentType = 'TransactionCheckSums'  AND
        chk.ArchiveKey   IS NULL         AND
        PosQ.SequenceId  IS NULL         AND
        POSChecksumCache.SequenceId     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgAxBOT.Stg_AxBO_POSCheckSum WHERE ArchiveKey IN
(
SELECT
        Pos.ArchiveKey
FROM
        ${DB_ENV}StgAxBOT.Stg_AxBO_POSCheckSum Pos
LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.Stg_AxBO_POSCheckSumQ PosQ
        ON
        Pos.SequenceId      = PosQ.SequenceId
    AND Pos.SAPCustomerID   = PosQ.SAPCustomerID
    AND Pos.TransactionTS   = PosQ.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}UtilT.AxBO_POSChecksum_Load_Cache POSChecksumCache
        ON
        Pos.SequenceId      = POSChecksumCache.SequenceId
    AND Pos.SAPCustomerID   = POSChecksumCache.SAPCustomerID
    AND Pos.TransactionTS   = POSChecksumCache.ReferenceTS

LEFT OUTER JOIN
        ${DB_ENV}StgAxBOT.S1_AxBO_POS_CheckSum chk
        ON
        Pos.ArchiveKey = chk.ArchiveKey
        
     LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Pos.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_CheckSum'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Pos.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_CheckSum'
        AND XPV.ResolveStatus = 'E'

WHERE
        Pos.SubContentType = 'TransactionCheckSums'  AND
        chk.ArchiveKey   IS NULL         AND
        PosQ.SequenceId  IS NULL         AND
        POSChecksumCache.SequenceId     IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
GROUP BY 1
)AND SubContentType = 'TransactionCheckSums'
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

 ------------------------------------------------------------------------------------------
 -- 5) TILTID Employee Archiving
 ------------------------------------------------------------------------------------------

-- INSERT INTO ${DB_ENV}Achv.Arc_TILTID_Employee
-- (
         -- SequenceId   
		-- ,SAPCustomerID 		 
        -- ,ContentType   
        -- ,SubContentType
        -- ,TransactionTS 
        -- ,ExtractionTS  
        -- ,IEReceivedTS  
        -- ,IEDeliveryTS  
        -- ,SourceVersion 
        -- ,TS            
        -- ,ArchiveKey    
        -- ,XmlData       
        -- ,JobRunId
		-- ,ARC_TS	
-- )

-- SELECT
         -- Emp.SequenceId
		-- ,Emp.SAPCustomerID 
        -- ,Emp.ContentType    
        -- ,Emp.SubContentType 
        -- ,Emp.TransactionTS  
        -- ,Emp.ExtractionTS   
        -- ,Emp.IEReceivedTS   
        -- ,Emp.IEDeliveryTS   
        -- ,Emp.SourceVersion  
        -- ,Emp.TS             
        -- ,Emp.ArchiveKey     
        -- ,Emp.XmlData  
		-- ,$JobRunID AS JobRunID
		-- ,current_timestamp

-- FROM
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_Employee Emp
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_EmployeeQ EmpQ
        -- ON
        -- Emp.SequenceId      = EmpQ.SequenceId
    -- AND Emp.SAPCustomerID   = EmpQ.SAPCustomerID
    -- AND Emp.TransactionTS   = EmpQ.ReferenceTS

-- LEFT OUTER JOIN
        -- ${DB_ENV}UtilT.TILTID_Employee_Load_Cache EmpCache
        -- ON
        -- Emp.SequenceId      = EmpCache.SequenceId
    -- AND Emp.SAPCustomerID   = EmpCache.SAPCustomerID
    -- AND Emp.TransactionTS   = EmpCache.TransactionTS
	
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.S1_TILTID_Employee_Header EmpHead
        -- ON
        -- Emp.ArchiveKey = EmpHead.ArchiveKey

-- LEFT OUTER JOIN
        -- ${DB_ENV}CntlCleanseT.R4_SourceError SE
        -- ON
        -- Emp.ArchiveKey = SE.ArchiveKey
        -- AND SE.StreamName = 'TILTID_Employee'
        -- AND SE.ResolveStatus = 'E'
     

-- WHERE
        -- EmpHead.ArchiveKey   IS NULL         AND
        -- EmpQ.SequenceId  IS NULL         AND
        -- EmpCache.SequenceId     IS NULL AND
		-- SE.ArchiveKey   IS NULL
-- ;

-- .IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

-- DELETE FROM ${DB_ENV}StgTILTIDT.Stg_TILTID_Employee WHERE ArchiveKey IN
-- (
-- SELECT
         -- Emp.ArchiveKey     
-- FROM
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_Employee Emp
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_EmployeeQ EmpQ
        -- ON
        -- Emp.SequenceId      = EmpQ.SequenceId
    -- AND Emp.SAPCustomerID   = EmpQ.SAPCustomerID
    -- AND Emp.TransactionTS   = EmpQ.ReferenceTS

-- LEFT OUTER JOIN
        -- ${DB_ENV}UtilT.TILTID_Employee_Load_Cache EmpCache
        -- ON
        -- Emp.SequenceId      = EmpCache.SequenceId
    -- AND Emp.SAPCustomerID   = EmpCache.SAPCustomerID
    -- AND Emp.TransactionTS   = EmpCache.TransactionTS
	
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.S1_TILTID_Employee_Header EmpHead
        -- ON
        -- Emp.ArchiveKey = EmpHead.ArchiveKey

-- LEFT OUTER JOIN
        -- ${DB_ENV}CntlCleanseT.R4_SourceError SE
        -- ON
        -- Emp.ArchiveKey = SE.ArchiveKey
        -- AND SE.StreamName = 'TILTID_Employee'
        -- AND SE.ResolveStatus = 'E'
     

-- WHERE
        -- EmpHead.ArchiveKey   IS NULL         AND
        -- EmpQ.SequenceId  IS NULL         AND
        -- EmpCache.SequenceId     IS NULL AND
		-- SE.ArchiveKey   IS NULL
-- GROUP BY 1
-- )
-- ;

-- .IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
-- COMMIT WORK;
-- .IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

 ------------------------------------------------------------------------------------------
 -- 6) TILTID_Schedule Archiving
 ------------------------------------------------------------------------------------------

-- INSERT INTO ${DB_ENV}Achv.Arc_TILTID_Schedule
-- (
         -- SequenceId   
		-- ,SAPCustomerID 		 
        -- ,ContentType   
        -- ,SubContentType
        -- ,TransactionTS 
        -- ,ExtractionTS  
        -- ,IEReceivedTS  
        -- ,IEDeliveryTS  
        -- ,SourceVersion 
        -- ,TS            
        -- ,ArchiveKey    
        -- ,XmlData       
        -- ,JobRunId     
		-- ,ARC_TS	
-- )

-- SELECT
         -- Sched.SequenceId
		-- ,Sched.SAPCustomerID 
        -- ,Sched.ContentType    
        -- ,Sched.SubContentType 
        -- ,Sched.TransactionTS  
        -- ,Sched.ExtractionTS   
        -- ,Sched.IEReceivedTS   
        -- ,Sched.IEDeliveryTS   
        -- ,Sched.SourceVersion  
        -- ,Sched.TS             
        -- ,Sched.ArchiveKey     
        -- ,Sched.XmlData  
		-- ,$JobRunID AS JobRunID
		-- ,current_timestamp

-- FROM
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_Schedule Sched
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_ScheduleQ SchedQ
        -- ON
        -- Sched.SequenceId      = SchedQ.SequenceId
    -- AND Sched.SAPCustomerID   = SchedQ.SAPCustomerID
    -- AND Sched.TransactionTS   = SchedQ.ReferenceTS

-- LEFT OUTER JOIN
        -- ${DB_ENV}UtilT.TILTID_Schedule_Load_Cache SchedCache
        -- ON
        -- Sched.SequenceId      = SchedCache.SequenceId
    -- AND Sched.SAPCustomerID   = SchedCache.SAPCustomerID
    -- AND Sched.TransactionTS   = SchedCache.TransactionTS
	
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.S1_TILTID_Schedule_Header SchedHead
        -- ON
        -- Sched.ArchiveKey = SchedHead.ArchiveKey

-- LEFT OUTER JOIN
        -- ${DB_ENV}CntlCleanseT.R4_SourceError SE
        -- ON
        -- Sched.ArchiveKey = SE.ArchiveKey
        -- AND SE.StreamName = 'TILTID_Schedule'
        -- AND SE.ResolveStatus = 'E'
     

-- WHERE
        -- SchedHead.ArchiveKey   IS NULL         AND
        -- SchedQ.SequenceId  IS NULL         AND
        -- SchedCache.SequenceId     IS NULL AND
		-- SE.ArchiveKey   IS NULL
-- ;

-- .IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

-- DELETE FROM ${DB_ENV}StgTILTIDT.Stg_TILTID_Schedule WHERE ArchiveKey IN
-- (
-- SELECT
         -- Sched.ArchiveKey     
-- FROM
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_Schedule Sched
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.Stg_TILTID_ScheduleQ SchedQ
        -- ON
        -- Sched.SequenceId      = SchedQ.SequenceId
    -- AND Sched.SAPCustomerID   = SchedQ.SAPCustomerID
    -- AND Sched.TransactionTS   = SchedQ.ReferenceTS

-- LEFT OUTER JOIN
        -- ${DB_ENV}UtilT.TILTID_Schedule_Load_Cache SchedCache
        -- ON
        -- Sched.SequenceId      = SchedCache.SequenceId
    -- AND Sched.SAPCustomerID   = SchedCache.SAPCustomerID
    -- AND Sched.TransactionTS   = SchedCache.TransactionTS
	
-- LEFT OUTER JOIN
        -- ${DB_ENV}StgTILTIDT.S1_TILTID_Schedule_Header SchedHead
        -- ON
        -- Sched.ArchiveKey = SchedHead.ArchiveKey

-- LEFT OUTER JOIN
        -- ${DB_ENV}CntlCleanseT.R4_SourceError SE
        -- ON
        -- Sched.ArchiveKey = SE.ArchiveKey
        -- AND SE.StreamName = 'TILTID_Schedule'
        -- AND SE.ResolveStatus = 'E'
     

-- WHERE
        -- SchedHead.ArchiveKey   IS NULL         AND
        -- SchedQ.SequenceId  IS NULL         AND
        -- SchedCache.SequenceId     IS NULL AND
		-- SE.ArchiveKey   IS NULL
-- GROUP BY 1
-- )
-- ;

-- .IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
-- COMMIT WORK;
-- .IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 7) AxBO_MarkdownLabel
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxBO_MarkdownLabel
(
     SequenceId,
     SAPCustomerID,
     StoreId,
     ContentType,
     SubContentType,
     TransactionTS,
     ExtractionTS,
     IEReceivedTS,
     IEDeliveryTS,
     SourceVersion,
     AOFlag,
     TS,
     ArchiveKey,
     XmlData,
     JobRunId,
	 ARC_TS	
)

SELECT
         md1.SequenceId
		,md1.SAPCustomerID 
        ,md1.StoreId        
        ,md1.ContentType    
        ,md1.SubContentType 
        ,md1.TransactionTS  
        ,md1.ExtractionTS   
        ,md1.IEReceivedTS   
        ,md1.IEDeliveryTS   
        ,md1.SourceVersion  
        ,md1.AOFlag         
        ,md1.TS             
        ,md1.ArchiveKey     
        ,md1.XmlData 
		,$JobRunID AS JobRunID
		,current_timestamp
FROM
${DB_ENV}StgAxBOT.Stg_AxBO_MarkdownLabel md1

LEFT OUTER JOIN ${DB_ENV}StgAxBOT.Stg_AxBO_MarkdownLabelQ md1Q 
ON md1.SequenceId = md1Q.SequenceId AND md1.SAPCustomerID = md1Q.SAPCustomerID 
AND md1.TransactionTS = md1Q.TransactionTS AND md1.ExtractionTS = md1Q.ExtractionTS

LEFT OUTER JOIN ${DB_ENV}UtilT.AxBO_MarkdownLabel_Load_Cache md1Cache
ON md1.SequenceId = md1Cache.SequenceId AND md1.SAPCustomerID = md1Cache.SAPCustomerID 
AND md1.TransactionTS = md1Cache.TransactionTS AND md1.ExtractionTS = md1Cache.ExtractionTS
	
LEFT OUTER JOIN ${DB_ENV}StgAxBOT.S1_AxBO_MarkdownLabel md1s1
ON md1.ArchiveKey = md1s1.ArchiveKey

LEFT OUTER JOIN ${DB_ENV}CntlCleanseT.R4_SourceError md1SE
ON md1.ArchiveKey = md1SE.ArchiveKey AND md1SE.StreamName = 'AxBOMDLabel' AND md1SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation md1XPV
ON md1.ArchiveKey = md1XPV.ArchiveKey AND md1XPV.StreamName = 'AxBOMDLabel' AND md1XPV.ResolveStatus = 'E'

WHERE md1s1.ArchiveKey IS NULL AND md1Q.SequenceId IS NULL AND md1Cache.SequenceId IS NULL 
AND md1SE.ArchiveKey IS NULL AND md1XPV.ArchiveKey IS NULL
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgAxBOT.Stg_AxBO_MarkdownLabel WHERE ArchiveKey IN (
SELECT md1.ArchiveKey 
FROM ${DB_ENV}StgAxBOT.Stg_AxBO_MarkdownLabel md1

LEFT OUTER JOIN ${DB_ENV}StgAxBOT.Stg_AxBO_MarkdownLabelQ md1Q 
ON md1.SequenceId = md1Q.SequenceId AND md1.SAPCustomerID = md1Q.SAPCustomerID 
AND md1.TransactionTS = md1Q.TransactionTS AND md1.ExtractionTS = md1Q.ExtractionTS

LEFT OUTER JOIN ${DB_ENV}UtilT.AxBO_MarkdownLabel_Load_Cache md1Cache 
ON md1.SequenceId = md1Cache.SequenceId AND md1.SAPCustomerID = md1Cache.SAPCustomerID 
AND md1.TransactionTS = md1Cache.TransactionTS AND md1.ExtractionTS = md1Cache.ExtractionTS
	
LEFT OUTER JOIN ${DB_ENV}StgAxBOT.S1_AxBO_MarkdownLabel md1s1 
ON md1.ArchiveKey = md1s1.ArchiveKey

LEFT OUTER JOIN ${DB_ENV}CntlCleanseT.R4_SourceError md1SE 
ON md1.ArchiveKey = md1SE.ArchiveKey 
AND md1SE.StreamName = 'AxBOMDLabel' AND md1SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation md1XPV 
ON md1.ArchiveKey = md1XPV.ArchiveKey 
AND md1XPV.StreamName = 'AxBOMDLabel' AND md1XPV.ResolveStatus = 'E'

WHERE md1s1.ArchiveKey IS NULL AND md1Q.SequenceId  IS NULL AND 
md1Cache.SequenceId IS NULL AND md1SE.ArchiveKey IS NULL AND md1XPV.ArchiveKey IS NULL 
GROUP BY 1
)
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 8) JobRun Finish
------------------------------------------------------------------------------------------

UPDATE ${DB_ENV}MetaDataVIN.JobRun
SET JobEnd = CURRENT_TIMESTAMP
WHERE
JobRunID = $JobRunID
;


.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE
COMMIT WORK;
.IF ERRORCODE <> 0 THEN  .QUIT ERRORCODE

.QUIT 0


------------------------------------------------------------------------------------------
-- 9) Error Handling
------------------------------------------------------------------------------------------

.LABEL RETURNERROR

.QUIT ERRORCODE" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT START ----------------" >> $LOG_FILE
echo >> $LOG_FILE

cat $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT END ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS START AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

bteq < $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE 2>&1

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS END AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "start time $start_time" >> $LOG_FILE
echo "end time `date`" >> $LOG_FILE


BTEQ_RETURN_CODE=""
BTEQ_RETURN_CODE=`grep 'RC (return code)' $LOG_FILE `

test "$BTEQ_RETURN_CODE" = "" && {
        echo "Error: Unable to execute $BTEQ_ARCHIVE_SOURCE_SCRIPT"
        echo "Error: Could not find BTEQ_RETURN_CODE from $LOG_FILE"
        echo "Info: Try executing the command task again, check logon user and password"
        return -1
}

BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`
echo "BTEQ_RETURN_CODE ="$BTEQ_RETURN_CODE

if [ "$BTEQ_RETURN_CODE" = "0" ]; then
        rm  -f $BTEQ_ARCHIVE_SOURCE_SCRIPT
        rm  -f $LOG_FILE
        rm -f $LOGON_FILE
        #rm -f $PMRootDir/log/`basename $0`_$JobRunID"_"$CURRENT_TS"_script".log
        return $BTEQ_RETURN_CODE
        

else
        return -1
fi
