#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: pos_checksum_exp.sh 2744 2012-04-26 15:16:54Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-04-26 17:16:54 +0200 (tor, 26 apr 2012) $
# Last Revision    : $Revision: 2744 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/pos_checksum_exp.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------


export PMRootDir=$1
export FILE_DIR="$PMRootDir/SrcFiles/Axbo/POS_Checksum"
export SCRIPT_DIR="$PMRootDir/bin"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"
export FILE_LIST_NAME="xmllist"
export I_StoreId=$2
export I_SequenceId=$3
export I_ReferenceTS=$4

exec >$PMRootDir/log/`basename $0`.log 2>&1 

echo "PMRootDir=$PMRootDir"
echo "I_StoreId=$I_StoreId"
echo "I_SequenceId=$I_SequenceId"
echo "I_ReferenceTS=$I_ReferenceTS"
if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"
echo "DB_ENV=$DB_ENV"


#  Print script syntax and help

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <StoreId> <SequenceId> <ReferenceTS>"
    echo ""
    echo "   Base directory is : $POS_DIR"
    echo "   <StoreId>         : StoreId"
    echo "   <SequenceId>      : SequenceId"
	echo "   <ReferenceTS>     :Reference Timestamp"
    echo ""
    }

############################################################################
# BEGIN PROCESSING                                                         #
############################################################################

############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 4 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

############################################################################
# Check directory for XML-files                                            #
############################################################################

echo ""
echo "###############################################################################"
echo "# Checking directory for XML-files ...                                        #"
echo "###############################################################################"
echo ""

if [ -e "$FILE_DIR" ] ; then
    echo "   Directory $FILE_DIR exists."
else
	echo "  Directory $FILE_DIR does not exist.		"
	echo "  Exiting..  "
	echo ""

    	exit 1
fi

############################################################################
#  Run TPT to export XML-files                                             #
############################################################################
echo ""
echo "###############################################################################"
echo "# Starting TPT job to export XML-files ...                                    #"
echo "###############################################################################"
echo "" 
tbuild -f $SCRIPT_DIR/pos_checksum_exp.tpt -v $SCRIPT_DIR/tptrun.var -u "DB_ENV='$DB_ENV', TargetFileName = '$FILE_LIST_NAME', LobDirPath = '$FILE_DIR', StoreId = '$I_StoreId', SequenceId = '$I_SequenceId', ReferenceTS = '$I_ReferenceTS'"