#!/bin/sh
##############################################################################
#                                                                            #
# Archive SoftOne files after loading of data                                   #
#                                                                            #
# 2019-06-11 Erik Kaar                                                       #
##############################################################################
PMRootDir=$1
FILE=$PMRootDir/SrcFiles/SoftOne/$2

for f in $(cat $FILE) ; do
  mv "$f" $PMRootDir/SrcFiles/SoftOne/Archive
done
exit 0



