#!/bin/ksh
##############################################################################
#                                                                            #
# acncha - Prepare Nielsen CHA file before load into Informatica DTS         #
#                                                                            #
# 2016-02-03 Erik Kaar                                                       #
##############################################################################

PMRootDir=$1
LoadFile=$2

# Copy file if no hierarchy section in file
if [[ $(grep -c "H,LEVEL," $PMRootDir/SrcFiles/Nielsen/$LoadFile.cha) = 0 ]]; 
then
cp $PMRootDir/SrcFiles/Nielsen/$LoadFile.cha $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac1
fi

# Split file if hierarchy section exists
if [[ $(grep -c "H,LEVEL," $PMRootDir/SrcFiles/Nielsen/$LoadFile.cha) > 0 ]]; 
then
# Split the file into one containing characteristics and the other hierarchy data
csplit -f $PMRootDir/SrcFiles/Nielsen/$LoadFile.cha -s $PMRootDir/SrcFiles/Nielsen/$LoadFile.cha /^H,LEVEL,/

# Change name of characteristics file
mv $PMRootDir/SrcFiles/Nielsen/$LoadFile.cha00 $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac1

# Change name of hierarchy file
mv $PMRootDir/SrcFiles/Nielsen/$LoadFile.cha01 $PMRootDir/SrcFiles/Nielsen/$LoadFile.chah
fi

# Remove the first line in characteristics file
sed '1d' $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac1 > $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac2

# Replace C, in beginning of line with CHARACTERISTICS,
sed 's/^C,/CHARACTERISTICS,/g' $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac2 > $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac3

# Put INCLUDE, in beginning of line ending with ,I,
sed '/,I,/s/^/INCLUDE,/' $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac3 > $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac4

# Replace ,I, with ,INCLUDE_T,
sed 's/,I,/,INCLUDE_T,/g' $PMRootDir/SrcFiles/Nielsen/$LoadFile.chac4 > $PMRootDir/SrcFiles/Nielsen/$LoadFile.chacn

