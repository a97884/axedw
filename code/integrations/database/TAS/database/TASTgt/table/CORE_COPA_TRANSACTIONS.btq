/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CORE_COPA_TRANSACTIONS.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASTgt/table/CORE_COPA_TRANSACTIONS.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASTgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_COPA_TRANSACTIONS','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASTgtT.CORE_COPA_TRANSACTIONS
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
    TAS_SOURCE_ID VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    RECORD_TYPE CHAR(1) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    CO_PLAN_VERSION CHAR(3) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    COPA_DOCUMENT_ID VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    COPA_DOCUMENT_ITEM_ID VARCHAR(6) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    PROFITABILITY_SEGMENT VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    PROF_SUB_SEGMENT CHAR(4) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    REF_DOCUMENT_NUMBER_ID VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC,
    REF_DOC_ITEM_ID VARCHAR(6) CHARACTER SET UNICODE CASESPECIFIC,
    FISCAL_YEAR CHAR(4) CHARACTER SET UNICODE CASESPECIFIC,
    FISCAL_PERIOD CHAR(3) CHARACTER SET UNICODE CASESPECIFIC,
    COPA_TIMESTAMP DECIMAL(16,0),
    TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC,
    PRIMARY KEY (RECORD_TYPE,CO_PLAN_VERSION,COPA_DOCUMENT_ID,COPA_DOCUMENT_ITEM_ID,PROFITABILITY_SEGMENT,PROF_SUB_SEGMENT)
  )
PRIMARY INDEX (RECORD_TYPE,CO_PLAN_VERSION,COPA_DOCUMENT_ID,COPA_DOCUMENT_ITEM_ID);

COMMENT ON TABLE PRTASTgtT.CORE_COPA_TRANSACTIONS IS 'Table containing cost profit analysis transactions.';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.TAS_SOURCE_ID IS 'SAP Source Identification';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.RECORD_TYPE IS 'CE1xxxx.VRGAR - Record Type';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.CO_PLAN_VERSION IS 'CE1xxxx.VERSI - Plan version (CO-PA)';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.COPA_DOCUMENT_ID IS 'CE1xxxx.BELNR - Document number of line item in Profitability Analysis';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.COPA_DOCUMENT_ITEM_ID IS 'CE1xxxx.POSNR - Item no. of CO-PA line item';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.PROFITABILITY_SEGMENT IS 'CE1xxxx.PAOBJNR - Profitability Segment Number (CO-PA)';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.PROF_SUB_SEGMENT IS 'CE1xxxx.PASUBNR - Profitability segment changes (CO-PA)';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.REF_DOCUMENT_NUMBER_ID IS 'CE1xxxx.RBELN - Reference document number for CO-PA line item';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.REF_DOC_ITEM_ID IS 'CE1xxxx.RPOSN - Item number from reference document (CO-PA)';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.FISCAL_YEAR IS 'CE1xxxx.GJAHR - Fiscal Year';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.FISCAL_PERIOD IS 'CE1xxxx.PERDE - Period';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.COPA_TIMESTAMP IS 'CE1S_AL.TIMESTMP - Time created (Greenwich Meantime)';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.TAS_CREATE_DATE IS 'CE1xxxx.HZDAT - Date on Which Record Was Created';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.DWH_CHANGE_DATE IS 'Datawarehouse Change date - Extraction run on date';
COMMENT ON COLUMN PRTASTgtT.CORE_COPA_TRANSACTIONS.DWH_DELETION_FLAG IS 'Active Y or N';

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_COPA_TRANSACTIONS','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASTgtT','CORE_COPA_TRANSACTIONS','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASTgtT.CORE_COPA_TRANSACTIONS IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

