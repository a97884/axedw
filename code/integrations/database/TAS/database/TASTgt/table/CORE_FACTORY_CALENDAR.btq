/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CORE_FACTORY_CALENDAR.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASTgt/table/CORE_FACTORY_CALENDAR.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASTgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_FACTORY_CALENDAR','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASTgtT.CORE_FACTORY_CALENDAR
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
    TAS_SOURCE_ID VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    FACTORY_CALENDAR_ID CHAR(2) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    FACTORY_CALENDAR_NAME VARCHAR(60) CHARACTER SET UNICODE CASESPECIFIC,
    TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC,
    PRIMARY KEY (TAS_SOURCE_ID,FACTORY_CALENDAR_ID)
  )
PRIMARY INDEX (FACTORY_CALENDAR_ID);

COMMENT ON TABLE PRTASTgtT.CORE_FACTORY_CALENDAR IS 'Table containing factory calendar definition.';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.TAS_SOURCE_ID IS 'SAP Source Identification';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.FACTORY_CALENDAR_ID IS 'TFACD.IDENT - Factory Calendar';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.FACTORY_CALENDAR_NAME IS 'TFACT.LTEXT - Default language for Object Text';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.DWH_CHANGE_DATE IS 'Datawarehouse Change date - Extraction run on date';
COMMENT ON COLUMN PRTASTgtT.CORE_FACTORY_CALENDAR.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag';

--DROP TABLE PRTASTgtT.CORE_FACTORY_CALENDAR_L;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_FACTORY_CALENDAR','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASTgtT','CORE_FACTORY_CALENDAR','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASTgtT.CORE_FACTORY_CALENDAR IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


