/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CORE_SHIPPING_TYPE_L.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASTgt/table/CORE_SHIPPING_TYPE_L.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASTgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_SHIPPING_TYPE_L','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASTgtT.CORE_SHIPPING_TYPE_L
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
   TAS_SOURCE_ID VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
   SHIPPING_TYPE_ID CHAR(2) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
   LANGUAGE_ID CHAR(2) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
   SHIPPING_TYPE_DESCRIPTION VARCHAR(20) CHARACTER SET UNICODE CASESPECIFIC,
   TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   DWH_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC,
   PRIMARY KEY(TAS_SOURCE_ID,SHIPPING_TYPE_ID,LANGUAGE_ID) 
  ) 
PRIMARY INDEX(SHIPPING_TYPE_ID);

COLLECT STATISTICS   
  COLUMN (TAS_SOURCE_ID, SHIPPING_TYPE_ID, LANGUAGE_ID),   
  COLUMN (TAS_SOURCE_ID),   
  COLUMN (SHIPPING_TYPE_ID),
  COLUMN (LANGUAGE_ID)  
ON PRTASTgtT.CORE_SHIPPING_TYPE_L;

COMMENT ON TABLE PRTASTgtT.CORE_SHIPPING_TYPE_L IS 'Language Table containing descriptions of shipping types.';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.TAS_SOURCE_ID IS 'SAP Source Identification';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.SHIPPING_TYPE_ID IS 'T173T.VSART - Shipping type';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.LANGUAGE_ID IS 'T002.LAISO - Language according to ISO 639:T173T.SPRAS - Language Key';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.SHIPPING_TYPE_DESCRIPTION IS 'T173T.BEZEI - Description of the Shipping Type';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.DWH_CHANGE_DATE IS 'Datawarehouse Change date - Extraction run on date';
COMMENT ON COLUMN PRTASTgtT.CORE_SHIPPING_TYPE_L.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag - Indicator for deletion';

--DROP TABLE PRTASTgtT.CORE_TRANSPORT_MODE;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_SHIPPING_TYPE_L','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASTgtT','CORE_SHIPPING_TYPE_L','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASTgtT.CORE_SHIPPING_TYPE_L IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


