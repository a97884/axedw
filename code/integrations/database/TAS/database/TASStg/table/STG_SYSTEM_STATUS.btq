/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STG_SYSTEM_STATUS.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASStg/table/STG_SYSTEM_STATUS.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASStgT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SYSTEM_STATUS','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASStgT.STG_SYSTEM_STATUS
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
    SYSTEM_STATUS VARCHAR(5) CHARACTER SET UNICODE CASESPECIFIC,
    SYSTEM_STATUS_SHORT_TEXT CHAR(4) CHARACTER SET UNICODE CASESPECIFIC,
    SYSTEM_STATUS_LONG_TEXT VARCHAR(30) CHARACTER SET UNICODE CASESPECIFIC,
    TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC
  )
PRIMARY INDEX (SYSTEM_STATUS);

COMMENT ON TABLE PRTASStgT.STG_SYSTEM_STATUS IS 'Table containing descriptions of system status.';
COMMENT ON COLUMN PRTASStgT.STG_SYSTEM_STATUS.SYSTEM_STATUS IS 'TJ02.ISTAT - System status';
COMMENT ON COLUMN PRTASStgT.STG_SYSTEM_STATUS.SYSTEM_STATUS_SHORT_TEXT IS 'TJ02T.TXT04 - Individual status of an object (short form)';
COMMENT ON COLUMN PRTASStgT.STG_SYSTEM_STATUS.SYSTEM_STATUS_LONG_TEXT IS 'TJ02T.TXT30 - Object status';
COMMENT ON COLUMN PRTASStgT.STG_SYSTEM_STATUS.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASStgT.STG_SYSTEM_STATUS.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASStgT.STG_SYSTEM_STATUS.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASStgT.STG_SYSTEM_STATUS.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag';

--DROP TABLE PRTASStgT.STG_SYSTEM_STATUS_L;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SYSTEM_STATUS','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASStgT','STG_SYSTEM_STATUS','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASStgT.STG_SYSTEM_STATUS IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


