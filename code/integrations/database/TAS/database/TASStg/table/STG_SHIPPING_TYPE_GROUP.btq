/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STG_SHIPPING_TYPE_GROUP.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASStg/table/STG_SHIPPING_TYPE_GROUP.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASStgT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SHIPPING_TYPE_GROUP','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASStgT.STG_SHIPPING_TYPE_GROUP
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
   SHIPPING_TYPE_GROUP_ID CHAR(4) CHARACTER SET UNICODE CASESPECIFIC,
   SHIP_TYPE_GROUP_DESCRIPTION VARCHAR(40) CHARACTER SET UNICODE CASESPECIFIC,
   TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC) 
PRIMARY INDEX (SHIPPING_TYPE_GROUP_ID);

COMMENT ON TABLE PRTASStgT.STG_SHIPPING_TYPE_GROUP IS 'Table containing shipping type groups.';
COMMENT ON COLUMN PRTASStgT.STG_SHIPPING_TYPE_GROUP.SHIPPING_TYPE_GROUP_ID IS 'TVFCV.VSGRP - Shipping type procedure group';
COMMENT ON COLUMN PRTASStgT.STG_SHIPPING_TYPE_GROUP.SHIP_TYPE_GROUP_DESCRIPTION IS 'TVFCVT.BEZEI - Description';
COMMENT ON COLUMN PRTASStgT.STG_SHIPPING_TYPE_GROUP.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASStgT.STG_SHIPPING_TYPE_GROUP.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASStgT.STG_SHIPPING_TYPE_GROUP.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASStgT.STG_SHIPPING_TYPE_GROUP.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag - Indicator for deletion';

--DROP TABLE PRTASStgT.STG_SHIPPING_TYPE_GROUP_L;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SHIPPING_TYPE_GROUP','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASStgT','STG_SHIPPING_TYPE_GROUP','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASStgT.STG_SHIPPING_TYPE_GROUP IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


