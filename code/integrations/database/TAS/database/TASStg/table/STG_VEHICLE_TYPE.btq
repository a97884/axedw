/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STG_VEHICLE_TYPE.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASStg/table/STG_VEHICLE_TYPE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASStgT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_VEHICLE_TYPE','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASStgT.STG_VEHICLE_TYPE
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
   VEHICLE_TYPE_ID VARCHAR(10) CHARACTER SET UNICODE NOT CASESPECIFIC,
   VEHICLE_TYPE_DESCRIPTION VARCHAR(20) CHARACTER SET UNICODE NOT CASESPECIFIC,
   TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
   DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC
   ) 
PRIMARY INDEX (VEHICLE_TYPE_ID);

COMMENT ON TABLE PRTASStgT.STG_VEHICLE_TYPE IS 'Table containing vehicle types.';
COMMENT ON COLUMN PRTASStgT.STG_VEHICLE_TYPE.VEHICLE_TYPE_ID IS 'T370K.EQART - Equipment Number';
COMMENT ON COLUMN PRTASStgT.STG_VEHICLE_TYPE.VEHICLE_TYPE_DESCRIPTION IS 'T370K_T.EARTX - Text for Object Type';
COMMENT ON COLUMN PRTASStgT.STG_VEHICLE_TYPE.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASStgT.STG_VEHICLE_TYPE.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASStgT.STG_VEHICLE_TYPE.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASStgT.STG_VEHICLE_TYPE.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag - Indicator for deletion';

--DROP TABLE PRTASStgT.STG_VEHICLE_TYPE_L;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_VEHICLE_TYPE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASStgT','STG_VEHICLE_TYPE','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASStgT.STG_VEHICLE_TYPE IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


