/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STG_SALES_OBJECT_L.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASStg/table/STG_SALES_OBJECT_L.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASStgT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SALES_OBJECT_L','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASStgT.STG_SALES_OBJECT_L
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
    SALES_OBJECT VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC,
    LANGUAGE_ID CHAR(2) CHARACTER SET UNICODE CASESPECIFIC,
    SALES_OBJECT_DESCRIPTION VARCHAR(60) CHARACTER SET UNICODE CASESPECIFIC,
    TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC
  )
PRIMARY INDEX (SALES_OBJECT);

COMMENT ON TABLE PRTASStgT.STG_SALES_OBJECT_L IS 'Language table containing descriptions of sales objects.';
COMMENT ON COLUMN PRTASStgT.STG_SALES_OBJECT_L.SALES_OBJECT IS 'DD07T.DOMVALUE_L - Values for Domains: Single Value / Upper Limit';
COMMENT ON COLUMN PRTASStgT.STG_SALES_OBJECT_L.LANGUAGE_ID IS 'T002.LAISO - Language according to ISO 639:DD07T.DDLANGUAGE - Language Key';
COMMENT ON COLUMN PRTASStgT.STG_SALES_OBJECT_L.SALES_OBJECT_DESCRIPTION IS 'DD07T.DDTEXT - Short text for fixed values';
COMMENT ON COLUMN PRTASStgT.STG_SALES_OBJECT_L.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASStgT.STG_SALES_OBJECT_L.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASStgT.STG_SALES_OBJECT_L.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASStgT.STG_SALES_OBJECT_L.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag';

--DROP TABLE PRTASStgT.STG_SALES_OFFICE;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SALES_OBJECT_L','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASStgT','STG_SALES_OBJECT_L','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASStgT.STG_SALES_OBJECT_L IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


