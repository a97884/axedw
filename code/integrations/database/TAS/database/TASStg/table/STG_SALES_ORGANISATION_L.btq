/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: STG_SALES_ORGANISATION_L.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASStg/table/STG_SALES_ORGANISATION_L.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASStgT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SALES_ORGANISATION_L','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASStgT.STG_SALES_ORGANISATION_L
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
    SALES_ORGANISATION_ID CHAR(4) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    LANGUAGE_ID CHAR(2) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    SALES_ORG_DESCRIPTION VARCHAR(20) CHARACTER SET UNICODE CASESPECIFIC,
    TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC
  )
PRIMARY INDEX (SALES_ORGANISATION_ID);

COMMENT ON TABLE PRTASStgT.STG_SALES_ORGANISATION_L IS 'Language table containing descriptions of sales organisations.';
COMMENT ON COLUMN PRTASStgT.STG_SALES_ORGANISATION_L.SALES_ORGANISATION_ID IS 'TVKOT.VKORG - Sales Organisation';
COMMENT ON COLUMN PRTASStgT.STG_SALES_ORGANISATION_L.LANGUAGE_ID IS 'T002.LAISO - Language according to ISO 639:TVKOT.SPRAS - Language Key';
COMMENT ON COLUMN PRTASStgT.STG_SALES_ORGANISATION_L.SALES_ORG_DESCRIPTION IS 'TVKOT.VTEXT - Name';
COMMENT ON COLUMN PRTASStgT.STG_SALES_ORGANISATION_L.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASStgT.STG_SALES_ORGANISATION_L.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASStgT.STG_SALES_ORGANISATION_L.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASStgT.STG_SALES_ORGANISATION_L.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag';

--DROP TABLE PRTASStgT.STG_SAP_USERS;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASStgT','STG_SALES_ORGANISATION_L','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASStgT','STG_SALES_ORGANISATION_L','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASStgT.STG_SALES_ORGANISATION_L IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


