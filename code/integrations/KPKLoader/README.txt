/*
# ----------------------------------------------------------------------------
# SCM Infostamp				(DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID			   : $Id: README.txt 31834 2020-06-04 07:55:49Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-04 09:55:49 +0200 (tor, 04 jun 2020) $
# Last Revision	   : $Revision: 31834 $
# SCM URL		   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/README.txt $
# --------------------------------------------------------------------------
# SCM Info END
*/
Scripts to load transactions from Competitor Price App from s3 into Teradata database.
        The user_id field in the input will be set to NULL (in the script loadCompetitorPrice.tpt) during the three first months in production.
        The convertion of user_id to NULL will be removed after tree months when GDPR reports are deployed (the script loadCompetitiviePrice.tpt muste be changed and redeployed).

bin/startup.sh
        verify that loader is not running and startup the loader with all subprocesses started
bin/shutdown.sh
        verify the status of the loader and request shutdown of all loadCompetitiviePrice jobs
bin/status.sh
		show the status for all subprocesses

bin/mounts3
        mount s3 filesystem on ../$#DB_ENV#.s3.competitorPrice
        uses file .passwd-s3fs-kpk as credentials
bin/loadCompetitiviePrice.sh
        starts TPT to load event files from S3 mounted folder in ../$#DB_ENV#.s3.competitorPrice
bin/loadCompetitiviePrice.tpt
        loads event files from s3.competitorPrice folder into Teradata database
        reads all files in subfolder Product-Transactions-Data ending with .lst which contains a list of names for the files to load
        monitors the s3.competitorPrice folder every 60 seconds and loads maximum 10 files each iteration
        terminates after 60 days and must be restarted every 60 days (parameter VigilElapsedTime = 86400 in loadCompetitiviePrice.tpt)
        if the TPT needs to be stopped used the following command:
        twbstat - displays ongoing jobs
        twbcmd load_competitorPrice-168 job terminate
                where load_competitorPrice-168 is from output from twbstat
        the job can be restarted by executing loadCompetitiviePrice.sh with nohup in background
par/loadCompetitiviePrice.var
        Variables used by TPT scripts
log/loadCompetitiviePrice.log
        log output from loadCompetitiviePrice.tpt
log/lastTimestamp
		timestamp of this file is used for comparision with files in the s3 bucket. All files newer that this file are loaded

Setup and run sequence:
        0. cd bin
        1. verify initial settings
			a. parameters in mounts3.sh
			b. credentials in ~/.passwd-s3fs-kpk - make sure the file has a file permission of 600
			c. timestamp on file ../log/lastTimestamp, will be created by pullData.sh if not exists
        3. verify database StgKPKT that the table stg_KPK_Transaction exists
        4. run ./startup.sh to start the loader
        5. verify content in ../$#DB_ENV#.s3.competitorPrice
        6. verify ../log/loadCompetitiviePrice.log for errors and check that files are loaded
