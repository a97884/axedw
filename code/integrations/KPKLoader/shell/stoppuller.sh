#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: stoppuller.sh 32343 2020-06-12 06:40:10Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-12 08:40:10 +0200 (fre, 12 jun 2020) $
# Last Revision    : $Revision: 32343 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/stoppuller.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
#
# stoppuller.sh - verify the status of the puller and stop if running
#
stopFile="../log/stopPullData"   # stop processing and exit if this file exists
pidFile="../log/pullData.pid"    # file for ProcessID
logFile="../log/pullData.log"    # general log file for errors
s3Folder="$#DB_ENV#.s3.competitorPrice/Product-Transactions-Data"	# name of folder tomount s3 bucket on

echo "Verifying the status of the puller..."
pullerProcess="$(ps -fu`id -un`|grep "/bin/sh .*/bin/pullData.sh .*/${s3Folder}" |grep -v grep)"
if [ "${pullerProcess}" != "" ]
then
        echo "puller is running. Stopping..."
        echo "${pullerProcess}"
        touch $stopFile
        while [ "$(ps -fu`id -un`|grep "/bin/sh .*/bin/pullData.sh .*/${s3Folder}" |grep -v grep)" != "" ]
        do
                echo "Waiting for puller to stop..."
                sleep 10
        done
        echo "puller is stopped"

else
        echo "puller is already stopped"
fi

exit 0
