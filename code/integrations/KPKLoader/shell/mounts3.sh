#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: mounts3.sh 32338 2020-06-11 14:13:48Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-11 16:13:48 +0200 (tor, 11 jun 2020) $
# Last Revision    : $Revision: 32338 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/mounts3.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
echo "Mounting axfood-ttnlab-priskoll-dev-integration on ../$#DB_ENV#.s3.competitorPrice"

s3fs  axfood-ttnlab-priskoll-dev-integration ../$#DB_ENV#.s3.competitorPrice \
	-o nonempty \
	-o no_check_certificate \
	-o passwd_file=~/.passwd-s3fs-kpk \
	-o url=https://s3.amazonaws.com \
	-o endpoint="eu-west-1" \
	-o complement_stat \
	-o uid=$(id -u) \
	-o gid=$(id -g) \
	>../log/mounts3.log 2>&1
stat=$?
if [ $stat -ne 0 ]
then
        echo "mount failed with code $stat"
fi
exit $stat
