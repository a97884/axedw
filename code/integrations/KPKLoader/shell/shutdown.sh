#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: shutdown.sh 31665 2020-06-02 13:48:34Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-02 15:48:34 +0200 (tis, 02 jun 2020) $
# Last Revision    : $Revision: 31665 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/shutdown.sh $
# --------------------------------------------------------------------------
# SCM Info END
#

#
# verify the status of the loader
#
binFolder="../bin"              # where all programs are
jobName=$#DB_ENV#loadCompetitorPrice		# name of the TPT job

stat=0

echo "Verifying the status of the loader..."
statInfo="$(twbstat)"
jobIDs="$(echo "$statInfo" | grep ${jobName})"
if [ "$jobIDs" = "" ]
then
        echo "no loader jobs found" >&2
        echo "$statInfo" >&2
else
        echo "The following loader jobs found and I will send a terminate request for all of them:"
        echo "${jobIDs}"
        for job in ${jobIDs}
        do
                echo "Requesting to terminate job: ${job}"
                twbcmd "${job}" job terminate

				cnt=0
                while [ "$(twbstat | grep ${jobName})" != "" ]
                do
						cnt=$(($cnt + 1))
                        echo "Waiting for the loader (${job}) to stop...(${cnt})"
                        sleep 10
						if [ $(($cnt % 10)) -eq 0 ]
						then
			                twbkill "${job}"
						else
			                twbcmd "${job}" job terminate
						fi

                done
                echo "The loader (${job}) is stopped"
        done
fi

$binFolder/stoppuller.sh

$binFolder/umounts3.sh
stat=$?

exit $stat
