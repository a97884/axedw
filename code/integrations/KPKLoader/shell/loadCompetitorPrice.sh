#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: loadCompetitorPrice.sh 31834 2020-06-04 07:55:49Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-04 09:55:49 +0200 (tor, 04 jun 2020) $
# Last Revision    : $Revision: 31834 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/loadCompetitorPrice.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
envName="$2"
binFolder="../bin"	# where all programs are
parFolder="../par"	# general log file for errors
pidFile="../log/loadCompetitorPrice.pid"	# file for ProcessID
logFile="../log/loadCompetitorPrice.log"	# general log file for errors
jobName=$#DB_ENV#loadCompetitorPrice		# name of the TPT job

test -r "${logFile}" && mv -f "${logFile}"  "${logFile}.old"
echo "$(date): Starting load process for environment ${envName}" | tee -a "${logFile}"
tbuild -f "${binFolder}/loadCompetitorPrice.tpt" -v "${parFolder}/loadCompetitorPrice.var" -j ${jobName} 2>&1 | while read l; do echo "$(date): $l"; done | tee -a "${logFile}"
stat=$?
echo "$(date): Ending load process (status=${stat}) for environment ${envName}" | tee -a "${logFile}"
exit ${stat}
