#!/bin/sh
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: cleanArchive.sh 32490 2020-06-16 14:55:04Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 16:55:04 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32490 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/cleanArchive.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
binFolder="../bin"			# where all programs are
logFolder="../log"			# where all log files are
logFile="${logFolder}/cleanArchive.log"
ArchiveDir="$1"				# where the archive to cleanup is
maxNumOfFilesInArchive="$2"	# maximum number of files to keep in the archive
maxNumOfFilesToClean="$3"	# maximum number of files to remove from the archive, the oldest files are removed
if [ $# -ne 3 ]
then
	echo "Usage: $0 archiveDir maxFiles nrFilesToRemove" >&2
	exit 1
fi

currNrFiles="$(ls $ArchiveDir | wc -l)"
if [ $currNrFiles -ge $maxNumOfFilesInArchive ]
then
	echo "Current number of files (${currNrFiles}) in the archive folder (${ArchiveDir}) exceeds maximum configured (${maxNumOfFilesInArchive})" | tee -a $logFile
	echo "I will remove the oldest ${maxNumOfFilesToClean} files" | tee -a "$logFile"
	ls -t "${ArchiveDir}" | tail -${maxNumOfFilesToClean} | xargs -t rm -f >>"$logFile" 2>&1
fi

exit 0
