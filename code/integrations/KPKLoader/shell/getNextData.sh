#!/bin/sh
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: getNextData.sh 31524 2020-05-27 19:40:38Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-05-27 21:40:38 +0200 (ons, 27 maj 2020) $
# Last Revision    : $Revision: 31524 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/getNextData.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
# getData.sh - get data file into target folder
#
# Usage: getData.sh targetFolder srcFiles...
#
#
# for each file in arguments put a newline after 131'th field
#

targetFolder="$1"
if [ ! -d "${targetFolder}" ]
then
        echo "Unable to access $targetFolder as target folder" >&2
        exit 1
fi
shift

for file in $@
do
        test -f $file || continue
        srcFolder="$(dirname $file)"
        targetFile="$targetFolder/fileList.$(basename $file).lst"
        if [ "$srcFolder" = "$targetFolder" ]
        then
                echo "Source[$srcFolder] and target[$targetFolder] folders are same, unable to continue" >&2
                exit 1
        fi

        #
        # create name file to specify the file to load
        #
        if ! echo -e "$(basename $file)\t${file}" >"$targetFile"; then echo "Unable to create name file for $file ($targetFile)"; exit 1; fi
done

exit 0
