#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: status.sh 31834 2020-06-04 07:55:49Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-04 09:55:49 +0200 (tor, 04 jun 2020) $
# Last Revision    : $Revision: 31834 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/status.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
# status.sh - check the status of the loader
#
jobName=$#DB_ENV#loadCompetitorPrice		# name of the TPT job
s3Folder="$#DB_ENV#.s3.competitorPrice/Product-Transactions-Data"	# name of folder tomount s3 bucket on

stat=0

pullerProcess="$(ps -fu`id -un`|grep "/bin/sh .*/bin/pullData.sh .*/${s3Folder}" |grep -v grep)"
if [ "${pullerProcess}" != "" ]
then
        echo "Message: pullData.sh.sh is running: ${pullerProcess}"
else
        #
        # no it is not running so start the puller and save the PID
        #
        echo "Message: pullData.sh is not running"
        stat=99
fi

statInfo="$(twbstat)"
jobIDs="$(echo "$statInfo" | grep ${jobName})"
if [ "$jobIDs" = "" ]
then
        echo "Message: no loader jobs found"
        stat=98
else
        echo "Message: loader is running with the following jobIds"
        echo "${jobIDs}" | while read line; do echo "Message: $line"; done
fi

var_file=../par/loadCompetitorPrice.var
node="$(grep -i TargetTdpId ${var_file} | while IFS=' =' read name node; do echo $node; done|tr -d \')"
uid="$(grep -i TargetUserName ${var_file} | while IFS=' =' read name uid; do echo $uid; done|tr -d \')"
passwd="$(grep -i TargetUserPassword ${var_file} | while IFS=' =' read name passwd; do echo $passwd; done|tr -d \')"

nr_minutes_idle="$(bteq <<-end
.LOGON $node/$uid,$passwd
.SET TITLEDASHES off
SELECT
        'NRMINUTES_IDLE' (TITLE ''), (CURRENT_TIMESTAMP - MAX(InsertDttm)) MINUTE(4) as MINUTES
FROM    $#DB_ENV#.StgKPKVOUT.stg_KPK_Transaction
;
end
)"
nr_minutes_idle="$(echo "${nr_minutes_idle}" | grep NRMINUTES_IDLE | egrep -v 'CURRENT_TIMESTAMP' | while read name minutes; do echo $minutes; done)"
echo "Message: Number of minutes idle ${nr_minutes_idle}"
if [ "${nr_minutes_idle}" = "" -o "${nr_minutes_idle}" = "?" ]
then
        nr_minutes_idle=0
fi

if [ ${nr_minutes_idle} -gt 180 ]
then
        stat=97
        echo "Statistic: -1"
else
        echo "Statistic: $nr_minutes_idle"
fi
exit ${stat}
