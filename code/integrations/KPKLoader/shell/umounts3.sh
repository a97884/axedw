#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: umounts3.sh 31662 2020-06-02 12:51:19Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-02 14:51:19 +0200 (tis, 02 jun 2020) $
# Last Revision    : $Revision: 31662 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/umounts3.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
echo "Unmounting ../$#DB_ENV#.s3.competitorPrice"
fusermount -u ../$#DB_ENV#.s3.competitorPrice
exit $?
