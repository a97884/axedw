#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: startpuller.sh 31666 2020-06-02 13:55:47Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-02 15:55:47 +0200 (tis, 02 jun 2020) $
# Last Revision    : $Revision: 31666 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/startpuller.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
#
# startpuller.sh - check if puller is running, if not start it
#
binFolder="../bin"                               # where all programs are
stopFile="../log/stopPullData"   # stop processing and exit if this file exists
pidFile="../log/pullData.pid"    # file for ProcessID
logFile="../log/pullData.log"    # general log file for errors
s3Folder="$#DB_ENV#.s3.competitorPrice/Product-Transactions-Data"	# name of folder tomount s3 bucket on

mv -f "$logFile" "$logFile.old"

echo "Checking if puller is running..."
pullerProcess="$(ps -fu`id -un`|grep "/bin/sh .*/bin/pullData.sh .*/${s3Folder}" |grep -v grep)"
if [ "${pullerProcess}" != "" ]
then
        echo "pullData.sh is already running: ${pullerProcess}" >&2
else
        #
        # no it is not running so start the puller and save the PID
        #
        echo "No it is not running so I will start the puller"
        rm -f "$stopFile"

        #
        # monitor the latest Product-Transactions-Data folder
        #
        folder="$(ls -d ../${s3Folder}* | tail -1)"
        if [ -d "$folder" ]
        then
                nohup ${binFolder}/pullData.sh $folder >>"$logFile" 2>&1&
                echo $! >$pidFile
        else
                echo "No such folder exists: $folder" >&2
                exit 1
        fi
fi

exit 0
